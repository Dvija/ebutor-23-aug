package com.google.maps.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebutor.adapters.BuyerInfoAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 09-Jun-16.
 */
public class RetailersModel extends BuyerInfoAdapter.Row implements Parcelable, ClusterItem {

    public static final Creator<RetailersModel> CREATOR = new Creator<RetailersModel>() {
        @Override
        public RetailersModel createFromParcel(Parcel source) {
            return new RetailersModel(source);
        }

        @Override
        public RetailersModel[] newArray(int size) {
            return new RetailersModel[size];
        }
    };
    @SerializedName("customer_id")
    private String customerId;
    private String customerGrpId;
    @SerializedName("customer_token")
    private String customerToken;
    @SerializedName("firstname")
    private String firstName;
    private String lastName;
    private String image;
    private String segmentId;
    private String leWhId;
    @SerializedName("telephone")
    private String telephone;
    @SerializedName("company")
    private String company;
    @SerializedName("address_1")
    private String address1;
    @SerializedName("address2")
    private String address2;
    private String city;
    private String pincode;
    @SerializedName("No_of_shutters")
    private String noOfShutters;
    @SerializedName("volume_class")
    private String volumeClass;
    @SerializedName("business_type_id")
    private String businessTypeId;
    @SerializedName("master_manf")
    private String masterManfIds;
    @SerializedName("buyer_type")
    private String buyerTypeId;
    @SerializedName("legal_entity_id")
    private String legalEntityId;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("check_in")
    private String lastVisit;
    @SerializedName("last_order")
    private String lastOrder;
    @SerializedName("popup")
    private boolean popup;
    private boolean isCheckIn;
    private boolean isBilled;
    private String hub;
    @SerializedName("beat_id")
    private String beatId;
    @SerializedName("beatname")
    private String beatName;
    private int color;
    private float hue;
//    private Marker marker;

    public RetailersModel() {
    }

    protected RetailersModel(Parcel in) {
        this.customerId = in.readString();
        this.customerGrpId = in.readString();
        this.customerToken = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.image = in.readString();
        this.segmentId = in.readString();
        this.leWhId = in.readString();
        this.telephone = in.readString();
        this.company = in.readString();
        this.address1 = in.readString();
        this.address2 = in.readString();
        this.city = in.readString();
        this.pincode = in.readString();
        this.noOfShutters = in.readString();
        this.volumeClass = in.readString();
        this.businessTypeId = in.readString();
        this.masterManfIds = in.readString();
        this.buyerTypeId = in.readString();
        this.legalEntityId = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.lastVisit = in.readString();
        this.lastOrder = in.readString();
        this.popup = in.readByte() != 0;
        this.isCheckIn = in.readByte() != 0;
        this.isBilled = in.readByte() != 0;
        this.hub = in.readString();
        this.beatId = in.readString();
        this.beatName = in.readString();
        this.color = in.readInt();
        this.hue = in.readFloat();
//        this.marker = in.readParcelable(Marker.class.getClassLoader());
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(String customerToken) {
        this.customerToken = customerToken;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getNoOfShutters() {
        return noOfShutters;
    }

    public void setNoOfShutters(String noOfShutters) {
        this.noOfShutters = noOfShutters;
    }

    public String getVolumeClass() {
        return volumeClass;
    }

    public void setVolumeClass(String volumeClass) {
        this.volumeClass = volumeClass;
    }

    public String getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(String businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public String getMasterManfIds() {
        return masterManfIds;
    }

    public void setMasterManfIds(String masterManfIds) {
        this.masterManfIds = masterManfIds;
    }

    public String getBuyerTypeId() {
        return buyerTypeId;
    }

    public void setBuyerTypeId(String buyerTypeId) {
        this.buyerTypeId = buyerTypeId;
    }

    public boolean isPopup() {
        return popup;
    }

    public void setPopup(boolean popup) {
        this.popup = popup;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public String getLeWhId() {
        return leWhId;
    }

    public void setLeWhId(String leWhId) {
        this.leWhId = leWhId;
    }

    public String getBeatId() {
        return beatId;
    }

    public void setBeatId(String beatId) {
        this.beatId = beatId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(String lastVisit) {
        this.lastVisit = lastVisit;
    }

    public String getLastOrder() {
        return lastOrder;
    }

    public void setLastOrder(String lastOrder) {
        this.lastOrder = lastOrder;
    }

    public String getLegalEntityId() {
        return legalEntityId;
    }

    public void setLegalEntityId(String legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public boolean isCheckIn() {
        return isCheckIn;
    }

    public void setIsCheckIn(boolean isCheckIn) {
        this.isCheckIn = isCheckIn;
    }

    public boolean isBilled() {
        return isBilled;
    }

    public void setIsBilled(boolean isBilled) {
        this.isBilled = isBilled;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getCustomerGrpId() {
        return customerGrpId;
    }

    public void setCustomerGrpId(String customerGrpId) {
        this.customerGrpId = customerGrpId;
    }

    public String getBeatName() {
        return beatName;
    }

    public void setBeatName(String beatName) {
        this.beatName = beatName;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getHue() {
        return hue;
    }

    public void setHue(float hue) {
        this.hue = hue;
    }

//    public Marker getMarker() {
//        return marker;
//    }
//
//    public void setMarker(Marker marker) {
//        this.marker = marker;
//    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.customerId);
        dest.writeString(this.customerGrpId);
        dest.writeString(this.customerToken);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.image);
        dest.writeString(this.segmentId);
        dest.writeString(this.leWhId);
        dest.writeString(this.telephone);
        dest.writeString(this.company);
        dest.writeString(this.address1);
        dest.writeString(this.address2);
        dest.writeString(this.city);
        dest.writeString(this.pincode);
        dest.writeString(this.noOfShutters);
        dest.writeString(this.volumeClass);
        dest.writeString(this.businessTypeId);
        dest.writeString(this.masterManfIds);
        dest.writeString(this.buyerTypeId);
        dest.writeString(this.legalEntityId);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.lastVisit);
        dest.writeString(this.lastOrder);
        dest.writeByte(this.popup ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isCheckIn ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isBilled ? (byte) 1 : (byte) 0);
        dest.writeString(this.hub);
        dest.writeString(this.beatId);
        dest.writeString(this.beatName);
        dest.writeInt(this.color);
        dest.writeFloat(this.hue);
//        dest.writeParcelable(this.marker, flags);
    }

    @Override
    public LatLng getPosition() {
        double lat = 0.0, lon = 0.0;
        try {
            lat = Double.parseDouble(latitude);
            lon = Double.parseDouble(longitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new LatLng(lat, lon);
    }

    @Override
    public String getTitle() {
        return company;
    }

    @Override
    public String getSnippet() {
        return company;
    }

    public class Retailers {
        @SerializedName("Status")
        private int status;
        @SerializedName("Message")
        private String message;
        @SerializedName("data")
        private ArrayList<RetailersModel> arrRetailers;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<RetailersModel> getArrRetailers() {
            return arrRetailers;
        }

        public void setArrRetailers(ArrayList<RetailersModel> arrRetailers) {
            this.arrRetailers = arrRetailers;
        }
    }
}
