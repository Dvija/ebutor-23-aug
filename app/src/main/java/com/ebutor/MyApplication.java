package com.ebutor;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.ebutor.models.BannerModel;
import com.ebutor.models.FilterDataModel;
import com.ebutor.models.ProductFilterModel;
import com.ebutor.models.TabModel;
import com.github.mzule.activityrouter.router.RouterCallback;
import com.github.mzule.activityrouter.router.RouterCallbackProvider;
import com.github.mzule.activityrouter.router.SimpleRouterCallback;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by 300024 on 3/14/2016.
 */
public class MyApplication extends MultiDexApplication implements RouterCallbackProvider {
    public static final String TAG = MyApplication.class.getSimpleName();
    private static MyApplication mInstance;
    String filterData;
    ArrayList<ProductFilterModel> arrProductFilters;
    private HashMap<String, String> hashmapSelected = new HashMap<>();
    private ArrayList<FilterDataModel> arrSelectedData = new ArrayList<>();
    private Tracker mTracker;
    private ArrayList<BannerModel> bannersArray;
    private RequestQueue mRequestQueue;
    private ArrayList<TabModel> arrTabs;
    private int cartCount;
    private String selBeats, dcId, selHubIds, selBeatIds, selBeatNames, selHubNames, selSpokeIds, selSpokeNames;
    private boolean isDayChanged = false;

    public static String getTAG() {
        return TAG;
    }

    public static void setmInstance(MyApplication mInstance) {
        MyApplication.mInstance = mInstance;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(150 * 1024 * 1024); // 150 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public Tracker getmTracker() {
        return mTracker;
    }

    public void setmTracker(Tracker mTracker) {
        this.mTracker = mTracker;
    }

    public RequestQueue getmRequestQueue() {
        return mRequestQueue;
    }

    public void setmRequestQueue(RequestQueue mRequestQueue) {
        this.mRequestQueue = mRequestQueue;
    }

    public int getCartCount() {
        return cartCount;
    }

    public void setCartCount(int cartCount) {
        this.cartCount = cartCount;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public ArrayList<BannerModel> getBannersArray() {
        return bannersArray;
    }

    public void setBannersArray(ArrayList<BannerModel> bannersArray) {
        this.bannersArray = bannersArray;
    }

    public String getFilterData() {
        return filterData;
    }

    public void setFilterData(String filterData) {
        this.filterData = filterData;
    }

    public ArrayList<ProductFilterModel> getArrProductFilters() {
        return arrProductFilters;
    }

    public void setArrProductFilters(ArrayList<ProductFilterModel> arrProductFilters) {
        this.arrProductFilters = arrProductFilters;
    }

    public HashMap<String, String> getSelectedFilters() {
        return hashmapSelected;
    }

    public void setSelectedFilters(HashMap<String, String> hashmapSelected) {
        this.hashmapSelected = hashmapSelected;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        initImageLoader(this);
    }



    /*public int addToCart(ProductModel model, String customerId) {

        String cartId = DBHelper.getInstance().getValue(DBHelper.TABLE_CART, DBHelper.KEY_CART_ID,
                model.getProductId(), DBHelper.KEY_PRODUCT_ID, customerId);

        if (cartId != null) {
            // Cart ID found, Delete records from Variant table & Cart Table
            DBHelper.getInstance().deleteRows(DBHelper.TABLE_CART, DBHelper.KEY_CART_ID, cartId);
            DBHelper.getInstance().deleteRows(DBHelper.TABLE_CART_VARIANT, DBHelper.KEY_CART_ID, cartId);

            DBHelper.getInstance().insertProductIntoCart(model, customerId);
            if (model.getSkuModelArrayList() != null && model.getSkuModelArrayList().size() > 0) {
                for (SKUModel skuModel : model.getSkuModelArrayList()) {
                    DBHelper.getInstance().insertCartVariant(skuModel, model.getCartId(), model.getProductId(), customerId);
                }
            }
        } else {
            DBHelper.getInstance().insertProductIntoCart(model, customerId);
            if (model.getSkuModelArrayList() != null && model.getSkuModelArrayList().size() > 0) {
                for (SKUModel skuModel : model.getSkuModelArrayList()) {
                    DBHelper.getInstance().insertCartVariant(skuModel, model.getCartId(), model.getProductId(), customerId);
                }
            }
        }
        exportDatabase("factail_database");
        return (int) DBHelper.getInstance().getRowsCount(DBHelper.TABLE_CART, customerId);
    }*/

    public void exportDatabase(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "Factail.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    /*public void removeCartItem(ProductModel model) {
        DBHelper.getInstance().deleteRows(DBHelper.TABLE_CART_VARIANT, DBHelper.KEY_CART_ID, model.getCartId());
        DBHelper.getInstance().deleteRows(DBHelper.TABLE_CART, DBHelper.KEY_CART_ID, model.getCartId());
    }*/

    @Override
    public RouterCallback provideRouterCallback() {
        return new SimpleRouterCallback() {
            @Override
            public void beforeOpen(Context context, Uri uri) {
//                context.startActivity(new Intent(context, LaunchActivity.class));
            }

            @Override
            public void notFound(Context context, Uri uri) {
//                context.startActivity(new Intent(context, NotFoundActivity.class));
            }
        };
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.app_tracker);
        }
        return mTracker;
    }

    public ArrayList<TabModel> getArrTabs() {
        return arrTabs;
    }

    public void setArrTabs(ArrayList<TabModel> arrTabs) {
        this.arrTabs = arrTabs;
    }

    public ArrayList<FilterDataModel> getArrSelectedData() {
        return arrSelectedData;
    }

    public void setArrSelectedData(ArrayList<FilterDataModel> arrSelectedData) {
        this.arrSelectedData = arrSelectedData;
    }

    public String getSelBeats() {
        return selBeats;
    }

    public void setSelBeats(String selBeats) {
        this.selBeats = selBeats;
    }

    public String getDcId() {
        return dcId;
    }

    public void setDcId(String dcId) {
        this.dcId = dcId;
    }

    public String getSelHubIds() {
        return selHubIds;
    }

    public void setSelHubIds(String selHubIds) {
        this.selHubIds = selHubIds;
    }

    public String getSelBeatIds() {
        return selBeatIds;
    }

    public void setSelBeatIds(String selBeatIds) {
        this.selBeatIds = selBeatIds;
    }

    public String getSelBeatNames() {
        return selBeatNames;
    }

    public void setSelBeatNames(String selBeatNames) {
        this.selBeatNames = selBeatNames;
    }

    public String getSelHubNames() {
        return selHubNames;
    }

    public void setSelHubNames(String selHubNames) {
        this.selHubNames = selHubNames;
    }

    public String getSelSpokeIds() {
        return selSpokeIds;
    }

    public void setSelSpokeIds(String selSpokeIds) {
        this.selSpokeIds = selSpokeIds;
    }

    public String getSelSpokeNames() {
        return selSpokeNames;
    }

    public void setSelSpokeNames(String selSpokeNames) {
        this.selSpokeNames = selSpokeNames;
    }

    public boolean isDayChanged() {
        return isDayChanged;
    }

    public void setDayChanged(boolean dayChanged) {
        isDayChanged = dayChanged;
    }
}
