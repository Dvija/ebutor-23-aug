package com.ebutor.backgroundtask;

/**
 * Created by Srikanth Nama on 13-Jul-16.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.OutletsModel;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.PARSER_TYPE;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class VolleyBackgroundTask extends Request<String> {

    private VolleyHandler<Object> listener;
    private Map<String, String> params;
    private PARSER_TYPE requestType;
    private JSONParser jsonParser;
    private Context context;
    private int method;
    private Gson gson;

    public VolleyBackgroundTask(String url, Map<String, String> params,
                                VolleyHandler<Object> reponseListener, ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }

    public VolleyBackgroundTask(int method, String url, Map<String, String> params,
                                VolleyHandler<Object> reponseListener, ErrorListener errorListener, PARSER_TYPE requestType) {
        super(method, url, errorListener);
        this.method = method;
        jsonParser = new JSONParser();
        this.listener = reponseListener;
        this.params = params;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        if (reponseListener instanceof Fragment) {
            context = ((Fragment) reponseListener).getActivity();
        } else if (reponseListener instanceof AppCompatActivity) {
            context = (AppCompatActivity) reponseListener;
        }
        this.requestType = requestType;
        Log.e("" + requestType, url);

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (this.method == Method.GET)
            return ConstantValues.getRequestHeaders();
        else if (requestType == PARSER_TYPE.DEFAULT)
            return ConstantValues.getRequestHeaders();
        else
            return super.getHeaders();
    }

    @Override
    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        if (params != null) {
            Log.e("" + requestType, params.get("data"));
        }
        return params;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(jsonString,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
            VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
            volleyError = error;
        }

        return volleyError;
    }

    @Override
    protected void deliverResponse(String response) {
        // TODO Auto-generated method stub
        Object object = null;
        String status = "", message = "";
        Log.e("" + requestType, response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (requestType == PARSER_TYPE.APP_UPDATE) {
                object = jsonParser.appUpdate(jsonObject);
                listener.onResponse(object, requestType, status, message);
                return;
            }

            status = jsonObject.optString("status");
            message = jsonObject.optString("message");
            if (status.equalsIgnoreCase("session")) {
                listener.onSessionError(message);
                return;
            }

            if (requestType == PARSER_TYPE.ADD_ORDER && (status.equals("0") || status.equals("-3"))) {
                object = message;
                listener.onResponse(object, requestType, status, message);
                return;
            }

            if (status.equalsIgnoreCase("success") || status.equals("1")) {
                switch (requestType) {
                    /*case VIEW_SHOP_LIST:
                        object = jsonParser.getProducts(jsonObject);
                        break;*/
                    case SHIPMENT_TRACKING:
                        object = jsonParser.getShipmentTrackingData(jsonObject);
                        break;
                    case GET_PRODUCT_DETAIL:
                    case EDIT_CART_DATA:
                        object = jsonParser.getProductDetails(jsonObject);
                        break;
                    case ADD_REVIEW:
                        object = jsonParser.addReview(jsonObject);
                        break;
                    case GET_COUNTRIES:
                        object = jsonParser.getCountries(jsonObject);
                        break;
                    case GET_STATES:
                        object = jsonParser.getStates(jsonObject);
                        break;
                    case ADD_ADDRESS:
                    case EDIT_SHIPPING_ADDRESS:
                        object = jsonParser.addAddress(jsonObject);
                        break;
                    case CANCEL_ORDER:
                    case CANCEL_ENTIRE_ORDER:
                    case RETURN_ORDER:
                    case RETURN_ENTIRE_ORDER:
                        object = jsonParser.cancelRequest(jsonObject);
                        break;
                    case GET_FEATURED_OFFERS:
                        object = jsonParser.getFeaturedOffers(jsonObject);
                        break;
                    case GET_FEATURED_CATEGORIES:
                        object = jsonParser.getRecommendedCategories(jsonObject);
                        break;
                    case GET_RECOMMENDED_PRODUCTS:
                        object = jsonParser.getRecommendedProducts(jsonObject);
                        break;
                    case GET_TOP_BRANDS:
                        object = jsonParser.getTopBrands(jsonObject);
                        break;
                    case GET_BANNER:
                        object = jsonParser.getBanners(jsonObject);
                        break;
                    case ADD_TO_CART:
                    case EDIT_CART:
                        object = jsonParser.addCart(jsonObject);
                        break;
                    case CREATE_WISHLIST:
                    case WISHLIST:
                        object = jsonParser.getWishList(jsonObject);
                        break;
                    case ORDERS_LIST:
                        object = jsonParser.getOrders(jsonObject);
                        break;
                    case ADDTO_WISHLIST:
                        object = message;
                        break;
                    case GET_HOME_PAGE_OFFERS:
                        object = jsonParser.getHomePageDataNew(jsonObject);
                        break;
                    case SORTED_TABS:
                        object = jsonParser.getTabData(jsonObject);
                        break;
                    case ORDER_DETAIL:
                        object = jsonParser.getOrderDetail(jsonObject);
                        break;

                    case RETURN_REASONS:
                        object = jsonParser.getReturnReasons(jsonObject);
                        break;
                    case CANCEL_REASONS:
                        object = jsonParser.getCancelReasons(jsonObject);
                        break;
                    case ADD_REGISTRATION:
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        message = dataObject.optString("message");
                        status = dataObject.optString("status");
                        object = message;
                        break;
                    case GET_ADDRESSES:
                        object = jsonParser.getAddresses(jsonObject);
                        break;
                    case ADD_ORDER:
                        JSONObject dataObj = jsonObject.optJSONObject("data");
                        object = dataObj != null ? dataObj.optDouble("ecash_amount") : 0;
                        break;
                    case VIEW_CART:
                        object = jsonParser.viewCart(jsonObject);
                        break;
                    case GET_MASTER_LOOKUP:
                        object = jsonParser.getMasterLookUp(jsonObject, context);
                        break;
                    case GET_MAIN_CATEGORIES:
                        object = jsonParser.getCategories(jsonObject);
                        break;
                    case GET_SUB_CATEGORIES:
                        object = jsonParser.getSubCategories(jsonObject);
                        break;
                    case CONFIRM_OTP:
                        object = jsonParser.confirmOtp(jsonObject);
                        JSONObject otpObj = jsonObject.optJSONObject("data");
                        status = otpObj.optString("status");
                        message = otpObj.optString("message");
                        break;
                    case RESEND_OTP:
                        JSONObject resendOtpObj = jsonObject.optJSONObject("data");
                        status = resendOtpObj.optString("status");
                        message = resendOtpObj.optString("message");
                        break;
                    case OTP:
                        JSONObject resendObj = jsonObject.optJSONObject("data");
                        object = resendObj.optString("telephone");
                        break;
                    case CART_COUNT:
                        object = jsonObject.optString("data");
                        break;
                    case UPDATE_RETAILER:
                        object = message;
                        break;
                    case APPLY_FILTER:
                    case SEARCH_PRODUCTS:
                    case GET_CATEGORIES:
                    case VIEW_SHOP_LIST:
                    case SUPPLIER_PRODUCTS:
                    case HIGH_MARGIN:
                    case FAST_MOVING:
                    case TOP_RATED:
                    case UNBILLED_SKUS:
                    case GET_MANUFACTURER_PRODUCTS:
                        object = jsonParser.getProductIds(jsonObject, false);
                        break;
                    case OFFLINE_PRODUCTS:
                    case SRM_PRODUCTS:
                        JSONObject offObj = jsonObject.optJSONObject("data");
                        JSONArray offlineArray = offObj.optJSONArray("data");
                        object = jsonParser.getOfflineProducts(offlineArray, context);
                        break;
                    case GET_FILTER_DATA:
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        object = dataArray;
                        break;
                    case GET_FILTER_DATA_NEW:
                        JSONObject filterObj = jsonObject.optJSONObject("data");
                        object = filterObj;
                        break;
                    case GET_SORT_DATA:
                        JSONArray sortArray = jsonObject.optJSONArray("data");
                        object = sortArray;
                        break;
                    case GET_PROFILE:
                        object = jsonParser.getProfile(jsonObject);
                        break;
                    case EDIT_PROFILE:
                        object = jsonParser.editProfile(jsonObject);
                        break;
                    case EDIT_NAME:
                        object = jsonParser.editName(jsonObject);
                        break;
                    case EDIT_EMAIL:
                        object = jsonParser.editEmail(jsonObject);
                        break;
                    case EDIT_MOBILE:
                        object = jsonParser.editMobile(jsonObject);
                        break;
                    case EDIT_ADDRESS:
                        object = jsonParser.editAddress(jsonObject);
                        break;
                    case PINCODE_AVAILABILITY:
                        JSONObject tempObj = jsonObject.optJSONObject("data");
                        object = tempObj.optString("le_wh_id");
                        break;
                    case PURCHASE_SLABS:
                    case SLAB_RATES:
                    case FREEBIE_SLABS:
                        object = jsonParser.getSlabRates(jsonObject);
                        break;
                    case GET_OTP:
                        JSONArray tempObj1 = jsonObject.optJSONArray("data");
                        JSONObject temp = tempObj1.optJSONObject(0);
                        object = temp.optString("otp_number");
                        break;
                    case GET_ALL_RETAILERS:
                        object = jsonParser.getAllRetailers(jsonObject);
                        break;
                    case GET_ALL_SUPPLIERS:
                        object = jsonParser.getAllSuppliers(jsonObject);
                        break;
                    case GET_RETAILER_TOKEN:
                        object = jsonParser.getRetailerToken(jsonObject);
                        break;
//                    case GET_PIN_CODE_AREAS:
//                        object = jsonParser.getPinCodeAreas(jsonObject.optJSONArray("data"));
//                        break;
                    case ADD_CART_1:
                    case CHECK_INVENTORY:
                        object = jsonParser.getAddCart1Response(jsonObject.optJSONObject("data"));
                        break;
                    case FF_COMMENTS:
                        object = jsonObject;
                        break;
                    case IMAGES_DESC:
                        object = jsonParser.getImagesDesc(jsonObject);
                        break;
                    case REVIEW_SPEC:
                        object = jsonParser.getSpecsReview(jsonObject);
                        break;
                    case CHECK_CART_INVENTORY:
                        object = jsonParser.checkCartInventoryResponse(jsonObject, context);
                        break;
                    case RETAILER_DASHBOARD:
                        object = jsonParser.getRetailerDashboardNew(jsonObject.getJSONArray("data"));
                        break;
                    case PO_LIST:
                        object = jsonParser.getPOs(jsonObject);
                        break;
                    case PO_DETAIL:
                        object = jsonParser.getPoDetail(jsonObject);
                        break;
                    case DISABLE_CONTACT:
                        object = message;
                        break;
                    case GET_WH_LIST:
                        object = jsonParser.getwareHouses(jsonObject);
                        break;
                    case MY_TEAM_DASHBOARD:
                        object = jsonParser.getMyTeamDashboard(jsonObject.getJSONArray("data"));
                        break;
                    case CREATE_PO:
                        object = message;
                        break;
                    case GET_INVENTORY:
                        object = jsonParser.getInventory(jsonObject.getJSONArray("data"));
                        break;
                    case PRICE_HISTORY:
                        object = jsonParser.getPriceHistory(jsonObject);
                        break;
                    case APP_UPDATE:
                        object = jsonParser.appUpdate(jsonObject);
                        break;
                    case GET_BEATS_BY_FF_ID:
                        object = jsonParser.getBeatsByFFID(jsonObject);
                        break;
                    case FEEDBACK_REASONS:
                        object = jsonParser.getFeedbackReasons(jsonObject);
                        break;
                    case SAVE_FEEDBACK:
                        object = message;
                        break;
                    case UPDATE_PO:
                        object = message;
                        break;
                    case GET_PINCODE_DATA:
                        object = jsonParser.getPinCodeData(jsonObject);
                        break;
                    case GET_SUPPLIER_MASTER_LOOKUP_DATA:
                        object = jsonParser.getSupplierMasterLookupData(jsonObject.optJSONObject("data"));
                        break;
                    case CREATE_SUPPLIER:
                        object = jsonParser.getCreateSupplierResponse(jsonObject.optJSONObject("data"));
                        break;
                    case GET_MANUFACTURERS:
                        object = jsonParser.getManufacturers(jsonObject.optJSONArray("data"));
                        break;
                    case PO_MASTER_LOOKUP:
                        object = jsonParser.getPOMasterLookUp(jsonObject);
                        break;
                    case FF_LIST:
                        object = jsonParser.getFFList(jsonObject);
                        break;
                    case GET_SO_DASHBOARD_FILTERS:
                        object = jsonParser.getSODashboardFilters(jsonObject);
                        break;
                    case GET_BEATS:
                        object = jsonParser.getBeats(jsonObject);
                        break;
                    case GET_EXPENSES:
                        object = jsonParser.getExpenses(jsonObject);
                        break;
                    case GET_EXPENSE_DETAILS:
                        object = jsonParser.getExpenseDetails(jsonObject);
                        break;
                    case GET_EXPENSE_TYPES:
                        object = jsonParser.getExpenseMasterLookupData(jsonObject);
                        break;
                    case SO_DASHBOARD:
                        object = jsonParser.getSODashboardNew(jsonObject.optJSONArray("data"));
                        break;
                    case SO_DASHBOARD_PICKER:
                        object = jsonParser.getSODashboardPickerNew(jsonObject.optJSONArray("data"));
                        break;
                    case SO_DASHBOARD_DELIVERY:
                        object = jsonParser.getSODashboardDeliveryNew(jsonObject.optJSONArray("data"));
                        break;
                    case SO_RETURN_REASONS:
                        object = jsonParser.getSOReturnReasonsNew(jsonObject.optJSONArray("data"));
                        break;
                    case UPDATE_GEO:
                        object = message;
                        break;
                    case GET_APPROVALS_DATA:
                        object = jsonParser.getApprovalDataResponse(jsonObject);
                        break;
                    case UPDATE_BEAT:
                        object = jsonParser.updateBeat(jsonObject);
//                        object = jsonObject.optJSONObject("data").optString("hub_id");
                        break;
                    case SEND_MAIL_TO_FF:
                        object = message;
                        break;
                    case GET_APPROVAL_HISTORY:
                        object = jsonParser.getApprovalHistoryResponse(jsonObject);
                        break;
                    case OUTLETS_MAPS:
                        object = gson.fromJson(jsonObject.toString(), OutletsModel.Retailers.class);
                        break;
                    case GET_ALL_MANUFACTURER_PRODUCTS:
                        object = jsonParser.getAllManufacturerProducts(jsonObject);
                        break;
                    case MANAGE_BEATS:
//                        object = jsonParser.getBeatsData(jsonObject);
                        object = jsonParser.getManageBeats(jsonObject);
                        break;
                    case GET_ALL_DATA:
                        object = jsonParser.getAllData(jsonObject);
                        break;
                    case ADD_BEAT:
                        object = message;
                        break;
                    case RM_LIST:
                        object = jsonParser.getRMList(jsonObject);
                        break;
                    case GET_BEATS_DATA:
                        object = jsonParser.getBeatsData(jsonObject);
                        break;
                    case ADD_SPOKE:
                        object = jsonParser.addSpoke(jsonObject);
                        break;
                    case PO_PENDING_APPROVALS:
                        object = jsonParser.getPendingPOs(jsonObject);
                        break;
                    case GENERATE_ORDER_REF:
                        object = jsonObject.optString("data");
                        break;
                    case GET_PO_APPROVAL_HISTORY:
                        object = jsonParser.getApprovalHistoryResponse(jsonObject);
                        break;
                    case PO_APPROVAL_OPTIONS:
                        object = jsonParser.getPOApprovalOptions(jsonObject);
                        break;
                    case SUBMIT_APPROVAL:
                        object = status;
                        break;
                    case GET_TALLY_LEDGERS:
                        object = jsonParser.getTallyLedgerData(jsonObject.optJSONArray("data"));
                        break;
                    case GENERATE_OTP:
                        object = status;
                        break;
                    case ORDER_OTP_CONFIRM:
                        object = status;
                        break;
                    case GET_ALL_EXPENSES:
                        object = jsonParser.getAllExpenses(jsonObject);
                        break;
                    case PO_STATUS_LIST:
                        object = jsonParser.getPOStatusList(jsonObject);
                        break;
                    case FILTER_ORDER_STATUS:
                        object = jsonParser.getFilterOrderStatus(jsonObject);
                        break;
                    case GET_ORDER_DATA:
                        object = jsonParser.getOrderData(jsonObject);
                        break;
                    case GET_NOTIFICATIONS:
                        object = jsonParser.getNotifications(jsonObject);
                        break;
                    case UNCLAIMED_EXPENSES:
                        object = jsonParser.getUnclaimedExpenses(jsonObject);
                        break;
                    case GET_MASTER_LOOKUP_DISCOUNTS:
                        object = jsonParser.getMasterLookUpDiscounts(jsonObject);
                        break;
                    case ORDER_CASHBACK:
                        object=jsonParser.getOrderLevelCashBack(jsonObject);
                        break;
                    case CASHBACK_HISTORY:
                        object=jsonParser.getCashBackHistory(jsonObject);
                        break;
                    default:
                        object = response;
                        break;
                }
            } else {
                object = status;
            }
            listener.onResponse(object, requestType, status, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}