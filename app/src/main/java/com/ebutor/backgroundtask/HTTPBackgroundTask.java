package com.ebutor.backgroundtask;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Window;

import com.ebutor.R;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.models.BrandProductsModel;
import com.ebutor.utils.APIREQUEST_TYPE;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class HTTPBackgroundTask extends AsyncTask<HashMap<String, String>, Integer, Integer> {

    static int RESCODE = 0;
    boolean isGson;
    private boolean isSwipeDownRefresh;
    private String progressType = "";
    private ResultHandler handler;
    private Context context = null;
    private PARSER_TYPE parserType = PARSER_TYPE.DEFAULT;
    private APIREQUEST_TYPE requestId;
    private Dialog dialog;
    private Object obj = null;
    private InputStream inputStream = null;
    private HttpURLConnection conn = null;
    private boolean showDialog = true;
    private boolean isLoadMore = false;
    private int resdId = R.layout.toolbar_progress;

    public HTTPBackgroundTask(ResultHandler callback, Context context,
                              PARSER_TYPE parserType, APIREQUEST_TYPE requestId) {
        this.handler = callback;
        this.context = context;
        this.parserType = parserType;
        this.requestId = requestId;
    }

    public HTTPBackgroundTask(ResultHandler callback, Context context,
                              PARSER_TYPE parserType, APIREQUEST_TYPE requestId, boolean isLoadMore, String progressType) {
        this.handler = callback;
        this.context = context;
        this.parserType = parserType;
        this.requestId = requestId;
        this.isLoadMore = isLoadMore;
        this.progressType = progressType;
    }

    public HTTPBackgroundTask(ResultHandler callback, Context context,
                              PARSER_TYPE parserType, APIREQUEST_TYPE requestId, boolean isLoadMore, String progressType, boolean isSwipeDownRefresh) {
        this.handler = callback;
        this.context = context;
        this.parserType = parserType;
        this.requestId = requestId;
        this.isLoadMore = isLoadMore;
        this.isSwipeDownRefresh = isSwipeDownRefresh;
        this.progressType = progressType;
    }

    static public HttpURLConnection getHTTPConnection(String url) throws Exception {
        HttpURLConnection _conn = null;
        URL serverAddress = null;
        URL hostAddress = null;
        int socketExepCt = 0;
        int ExepCt = 0;
        int numRedirect = 0;

        hostAddress = new URL(url);
        String host = "http://" + hostAddress.getHost() + "/";

        for (int i = 0; i <= 2; i++) {

            try {

                Log.e("request url :::", url + "");

                serverAddress = new URL(url);

                _conn = (HttpURLConnection) serverAddress.openConnection();
                if (_conn != null) {
                    _conn.setRequestMethod("GET");
                    _conn.addRequestProperty("Content-Type", "application/json");
                    _conn.setRequestProperty("api_key", " testkey");
                    _conn.setRequestProperty("api_secret", "testsecret");
                    _conn.setDoOutput(false);
                    _conn.setReadTimeout(50000);
                    _conn.setConnectTimeout(15000);
                    _conn.setInstanceFollowRedirects(false);

					/*if (item != null) {
                        Hashtable requestProperty = item.getAllAttributes();
						Enumeration keys = requestProperty.keys();
						while (keys.hasMoreElements()) {
							String key = keys.nextElement().toString();
							String value = requestProperty.get(key).toString();
							_conn.setRequestProperty(key, value);

						}
					}*/

                    RESCODE = 0;
                    _conn.connect();

                    RESCODE = _conn.getResponseCode();
                    if (RESCODE == HttpURLConnection.HTTP_OK) {
                        return _conn;
                    } else if (RESCODE == HttpURLConnection.HTTP_MOVED_TEMP
                            || RESCODE == HttpURLConnection.HTTP_MOVED_PERM) {
                        if (numRedirect > 15) {
                            _conn.disconnect();
                            _conn = null;
                            break;
                        }

                        numRedirect++;
                        i = 0;
                        url = _conn.getHeaderField("Location");
                        if (!url.startsWith("http")) {
                            url = host + url;
                        }
                        // Log.e("redirection url : ", url);
                        _conn.disconnect();
                        _conn = null;
                        continue;

                    } else {
                        _conn.disconnect();
                        _conn = null;
                    }
                }
            } catch (MalformedURLException me) {
                me.printStackTrace();
//                throw me;
            } catch (SocketTimeoutException se) {
                se.printStackTrace();
                _conn = null;
                if (i >= 5)
                    throw se;
            } catch (SocketException s) {
                s.printStackTrace();
//                if (socketExepCt > 10) {
//                    _conn = null;
//                    throw s;
//                }
//                socketExepCt++;
//                i = 0;
//                continue;
                return null;
            } catch (Exception e) {
                e.printStackTrace();

                if (ExepCt > 10) {
                    _conn = null;
                    throw e;
                }
                ExepCt++;
                i = 0;
                continue;

            }
        }
        return null;
    }

    public void setDialog(int id, String url) {

        dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);

        switch (progressType) {

            case ConstantValues.SPLASH_PROGRESS:
                resdId = R.layout.splash_progress;
                break;
            case ConstantValues.PROGRESS:
                resdId = R.layout.progress;
                break;
            case ConstantValues.TOOLBAR_PROGRESS:
                resdId = R.layout.toolbar_progress;
                break;
            case ConstantValues.TABS_PROGRESS:
                resdId = R.layout.tabs_progress;
                break;
            default:
                resdId = R.layout.toolbar_progress;
                break;
        }
        dialog.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        dialog.setTitle("");
//        dialog.setMessage("Please wait..");
        dialog.setContentView(resdId);
        dialog.setCancelable(false);

//        dialog = new ProgressDialog(context);
//        dialog.setTitle("");
//        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        dialog.setMessage("Please wait..");
//        dialog.setIndeterminate(true);
//        dialog.setCancelable(false);
    }

    public void setShowDialog(boolean show) {
        this.showDialog = show;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showDialog && !isLoadMore && !isSwipeDownRefresh) {
            if (dialog != null)
                dialog.show();
            else {
                setDialog(0, "");
                dialog.show();
            }
        }

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Integer doInBackground(HashMap<String, String>... params) {
        // TODO Auto-generated method stub

        String requestUrl = "";

        int timeoutConnection = 70000;
        int timeoutSocket = 70000;


        try {


            if (!isNetworkAvailable()) {
                return -5;
            }

            if (requestId == APIREQUEST_TYPE.HTTP_POST) {

                requestUrl = ConstantValues.NEW_BASE_URL_AUTH;

                switch (parserType) {
                    case APPID:
                        requestUrl = requestUrl.concat("common/registeration");
                        break;
                    case GET_CUSTOMER_TYPE:
                        requestUrl = requestUrl.concat("customer/customers/customerType");
                        break;
                    case ADD_REGISTRATION:
//                        requestUrl = requestUrl.concat("common/updatefile/addregistration");
                        requestUrl = requestUrl.concat("common/updatefile/addregistrationSales");
                        break;
                    case CONFIRM_OTP:
//                        requestUrl = requestUrl.concat("common/updatefile/otpconfirm");
                        requestUrl = requestUrl.concat("common/updatefile/otpconfirmSales");
                        break;
                    case CONFIRM_PASSWORD:
                        requestUrl = requestUrl.concat("common/updatefile/password");
                        break;
                    case SORTED_TABS:
                        requestUrl = requestUrl.concat("common/sortingDataTabs");
                        break;
                    case LOGIN:
                        requestUrl = requestUrl.concat("account/login");
                        break;
                    case PINCODE_AVAILABILITY:
                        requestUrl = requestUrl.concat("common/products/pinCodeAvailablity");
                        break;
                    case GET_SEGMENTS:
                        requestUrl = requestUrl.concat("common/segment/getsegment");
                        break;
                    case RESEND_OTP:
//                        requestUrl = requestUrl.concat("common/updatefile/resendotp");
                        requestUrl = requestUrl.concat("common/updatefile/resendotpSales");
                        break;
                    case GET_HOME_PAGE_OFFERS:
                        requestUrl = requestUrl.concat("common/home/homepage");
                        break;
                    case GET_RECOMMENDED_PRODUCTS:
                        requestUrl = requestUrl.concat("common/home/recomendedProduct");
                        break;
                    case GET_CATEGORIES:
                        requestUrl = requestUrl.concat("product/getcategories");
                        break;
                    case GET_TOP_BRANDS:
                        requestUrl = requestUrl.concat("common/home/topBrands");
                        break;
                    case GET_FEATURED_CATEGORIES:
                        requestUrl = requestUrl.concat("common/home/recommendedCategories");
                        break;
                    case GET_FEATURED_OFFERS:
                        requestUrl = requestUrl.concat("common/home/featuredOffer");
                        break;
                    case GET_PRODUCT_DETAIL:
                        requestUrl = requestUrl.concat("product/productDetails");
                        break;
                    case HIGH_MARGIN:
                        requestUrl = requestUrl.concat("product/highMargin");
                        break;
                    case FAST_MOVING:
                        requestUrl = requestUrl.concat("product/fastmoving/fastMovingProducts");
                        break;
                    case GET_BRAND_PRODUCTS:
                        requestUrl = requestUrl.concat("product/brandproduct/getBrandProducts");
//                        isGson = true;
                        break;
                    case SEARCH_PRODUCTS:
                        requestUrl = requestUrl.concat("product/search/getSearchProducts");
                        break;
                    case TOP_RATED:
                        requestUrl = requestUrl.concat("common/products/topRated");
                        break;
                    case GET_SUB_CATEGORIES:
                        requestUrl = requestUrl.concat("product/getcategories/categoryChild");
                        break;
                    case GET_PRODUCT_SKU:
                        requestUrl = requestUrl.concat("product/getcategories/getProducts");
                        break;
                    case ADD_REVIEW:
                        requestUrl = requestUrl.concat("product/review/addReview");
                        break;
                    case VIEW_REVIEW_RATING:
                        requestUrl = requestUrl.concat("product/review/viewReview");
                        break;
                    case ADD_TO_CART:
                        requestUrl = requestUrl.concat("checkout/cart/addcart");
                        break;
                    case CART_COUNT:
                        requestUrl = requestUrl.concat("checkout/cart/countcart");
                        break;
                    case VIEW_CART:
                        requestUrl = requestUrl.concat("checkout/cart/viewcart2");
                        break;
                    case DELETE_CART:
                        requestUrl = requestUrl.concat("checkout/cart/deletecart2");
                        break;
                    case EDIT_CART:
                        requestUrl = requestUrl.concat("checkout/cart/addcart");
                        break;
                    case GET_ADDRESSES:
                        requestUrl = requestUrl.concat("common/checkout/getAddress");
                        break;
                    case GET_STATES:
                        requestUrl = requestUrl.concat("common/checkout/getState");
                        break;
                    case GET_COUNTRIES:
                        requestUrl = requestUrl.concat("common/checkout/getcountries");
                        break;
                    case ADD_ADDRESS:
                        requestUrl = requestUrl.concat("common/checkout/saveAddress");
                        break;
                    case GET_PROFILE:
                        requestUrl = requestUrl.concat("account/profile");
                        break;
                    case ADD_ORDER:
                        requestUrl = requestUrl.concat("checkout/orders/addOrder");
                        break;
                    case GET_FILTER_DATA:
                        requestUrl = requestUrl.concat("product/filterdata/getFilterdata");
                        break;
                    case FORGOT_PASSWORD:
                        requestUrl = requestUrl.concat("common/updatefile/forgotpassword");
                        break;
                    case GET_BANNER:
                        requestUrl = requestUrl.concat("common/home/getBanner");
                        break;
                    case APPLY_FILTER:
//                        requestUrl = requestUrl.concat("product/search/getSearchProducts");
                        requestUrl = requestUrl.concat("product/search/getSearchProductsNewFilter");
                        break;
                    case ORDER_DETAIL:
                        requestUrl = requestUrl.concat("checkout/orders/orderDetails");
                        break;
                    case EDIT_PROFILE:
                        requestUrl = requestUrl.concat("account/profile/updateProfile");
                        break;
                    case EDIT_MOBILE:
//                        requestUrl = requestUrl.concat("account/profile/Telephone");
                        requestUrl = requestUrl.concat("account/profile/TelephoneSales");
                        break;
                    case EDIT_EMAIL:
                        requestUrl = requestUrl.concat("account/profile/updateEmail");
                        break;
                    case EDIT_ADDRESS:
                        requestUrl = requestUrl.concat("account/profile/updateAddress");
                        break;
                    case ORDERS_LIST:
                        requestUrl = requestUrl.concat("checkout/orders/getCustomerOrder");
                        break;
                    case OTP:
                        requestUrl = requestUrl.concat("account/profile/updateTelephone");
                        break;
                    case CREATE_WISHLIST:
                        requestUrl = requestUrl.concat("product/shoppingList/Createlist");
                        break;
                    case WISHLIST:
                        requestUrl = requestUrl.concat("product/shoppingList/shoppinglist");
                        break;
                    case CANCEL_ENTIRE_ORDER:
                        requestUrl = requestUrl.concat("checkout/orders/cancelOrder");
                        break;
                    case RETURN_REASONS:
                        requestUrl = requestUrl.concat("checkout/orders/returnReasons");
                        break;
                    case RETURN_ORDER:
                        requestUrl = requestUrl.concat("checkout/orders/returnOrderVariant");
                        break;
                    case RETURN_ENTIRE_ORDER:
                        requestUrl = requestUrl.concat("checkout/orders/returnOrder");
                        break;
                    case CANCEL_REASONS:
                        requestUrl = requestUrl.concat("checkout/orders/cancelReasons");
                        break;
                    case CANCEL_ORDER:
                        requestUrl = requestUrl.concat("checkout/orders/cancelOrderVariant");
                        break;
                    case ADDTO_WISHLIST:
                        requestUrl = requestUrl.concat("product/shoppingList/addproducttolist");
                        break;
                    case VIEW_SHOP_LIST:
                        requestUrl = requestUrl.concat("product/shoppingList/viewlist_shopping");
                        break;
                    case DELETE_PRODUCTFROM_WISHLIST:
                        requestUrl = requestUrl.concat("product/shoppingList/deleteproduct_list");
                        break;
                    case SHIPMENT_TRACKING:
                        requestUrl = requestUrl.concat("checkout/tracking");
                        break;
                    case EDIT_SHIPPING_ADDRESS:
                        requestUrl = requestUrl.concat("common/checkout/editaddess");
                        break;
                    case GET_ALL_SEGMENTS:
                        requestUrl = requestUrl.concat("common/segment/getCompleteSegments");
                        break;
                    case GET_ALL_RETAILERS:
                        requestUrl = requestUrl.concat("retailers");
                        break;
                    case GET_RETAILER_TOKEN:
                        requestUrl = requestUrl.concat("retailertoken");
                        break;
                    default:
                        break;

                }
                if (isGson) {
                    requestUrl = Utils.urlEncode(requestUrl);
                    HashMap<String, String> map = (HashMap<String, String>) params[0];
                    this.obj = performGsonPostCall(requestUrl, getPostDataString(map));
                } else {
                    requestUrl = Utils.urlEncode(requestUrl);
                    this.obj = performPostCall(requestUrl, getPostDataString(params[0]));
                }
                return 0;
            } else if (requestId == APIREQUEST_TYPE.HTTP_GET) {

            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return -1;
        } catch (IOException e) {
            e.printStackTrace();
            return -3;
        } catch (Exception e) {
            e.printStackTrace();
            return -4;
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return -99;

    }

    public Object performGsonPostCall(String requestURL, String postDataParams) throws IOException {

        URL url;
        String response = "";
        Object model = null;

        url = new URL(requestURL);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10680500);
        conn.setConnectTimeout(10680500);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);


        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(postDataParams);

        writer.flush();
        writer.close();
        os.close();
        int responseCode = conn.getResponseCode();
        BufferedReader br = null;
        if (responseCode == HttpsURLConnection.HTTP_OK) {
            String line;
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
           /* while ((line = br.readLine()) != null) {
                response += line;
            }*/
        } else {
            br = null;

        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        final Gson gson = gsonBuilder.create();
        switch (parserType) {
            case GET_BRAND_PRODUCTS:
                BrandProductsModel.MenuFieldHolder brandProductsModel = gson.fromJson(br, BrandProductsModel.MenuFieldHolder.class);
                model = brandProductsModel;
                break;
        }
        return model;
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);

        try {

            if (result != 0) {
                if (result == -1)
                    handler.onError("UnsupportedEncodingException.!", parserType);
                if (result == -2)
                    handler.onError("ClientProtocolException.!", parserType);
                if (result == -3)
                    handler.onError("IOException.!!", parserType);
                if (result == 4)
                    handler.onError("Invalid URL", parserType);
                if (result == -4)
                    handler.onError("Connection Error", parserType);
                if (result == -5)
                    handler.onError(
                            "No Internet, Please check your network settings",
                            parserType);
                return;
            }

            if (obj != null) {
                handler.onFinish(obj, parserType);
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (!isLoadMore && !isSwipeDownRefresh && dialog != null && showDialog && dialog.isShowing())
                dialog.dismiss();
        }
    }

    public String performGetCall(String requestURL, String postDataParams) throws IOException {

        HttpURLConnection conn = null;
        String response = "";
        try {
            requestURL = requestURL.concat("?");
            requestURL = requestURL.concat(postDataParams);
            conn = getHTTPConnection(requestURL);
            int responseCode = conn.getResponseCode();
            Log.e("response msg", "" + conn.getResponseMessage());

            response = new String();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                    Log.e("response", "" + response.toString());
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public String performPostCall(String requestURL,
                                  String postDataParams) throws IOException {

        URL url;
        String response = "";

        url = new URL(requestURL);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(9000000);
        conn.setConnectTimeout(9000000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);


        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(postDataParams);

        writer.flush();
        writer.close();
        os.close();
        int responseCode = conn.getResponseCode();

        if (responseCode == HttpsURLConnection.HTTP_OK) {
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }
        } else {
            response = "";

        }


        return response;
    }

    /**
     * checkConnectivity - Checks Whether Internet connection is available or
     * not.
     */
    private boolean isNetworkAvailable() {

        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }

        NetworkInfo net = manager.getActiveNetworkInfo();
        if (net != null) {
            if (net.isConnected()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private Object parseData(PARSER_TYPE parserType) throws Exception {


        Reader reader = new InputStreamReader(inputStream, "UTF-8");
        Object resObj = null;

        switch (parserType) {

            case DEFAULT:
                resObj = Utils.convertStreamToString(inputStream);
                break;


            default:
                break;
        }


        return resObj;
    }

}
