package com.ebutor.backgroundtask;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.ebutor.R;
import com.ebutor.database.DBHelper;
import com.ebutor.models.AddCart1Response;
import com.ebutor.models.AddressModel;
import com.ebutor.models.AppDataModel;
import com.ebutor.models.ApprovalDataModel;
import com.ebutor.models.ApprovalHistoryModel;
import com.ebutor.models.ApprovalHistoryResponseModel;
import com.ebutor.models.ApprovalOptionsModel;
import com.ebutor.models.ApprovalResponseModel;
import com.ebutor.models.BannerModel;
import com.ebutor.models.BeatModel;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CashBackDetailsModel;
import com.ebutor.models.CashBackHistoryModel;
import com.ebutor.models.CategoryModel;
import com.ebutor.models.CheckCartInventoryModel;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.ContainerModel;
import com.ebutor.models.CountryModel;
import com.ebutor.models.CrateModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.DashboardModel;
import com.ebutor.models.DashboardModelNew;
import com.ebutor.models.DiscountModel;
import com.ebutor.models.ExpenseDetailsModel;
import com.ebutor.models.ExpenseMasterLookupData;
import com.ebutor.models.ExpenseMasterlookupModel;
import com.ebutor.models.ExpenseModel;
import com.ebutor.models.FeatureData;
import com.ebutor.models.FeaturedOfferModel;
import com.ebutor.models.FilterDataModel;
import com.ebutor.models.FilterModel;
import com.ebutor.models.GetExpensesResponse;
import com.ebutor.models.HomeDataModel;
import com.ebutor.models.HomeModel;
import com.ebutor.models.InventoryModel;
import com.ebutor.models.ManufacturerModel;
import com.ebutor.models.MasterLookUpModel;
import com.ebutor.models.NewExpenseModel;
import com.ebutor.models.NewPacksModel;
import com.ebutor.models.NewProductModel;
import com.ebutor.models.NewProductSKUListModel;
import com.ebutor.models.NewVariantModel;
import com.ebutor.models.NotificationModel;
import com.ebutor.models.OrderDetailModel;
import com.ebutor.models.OrderLevelCashBackModel;
import com.ebutor.models.OrdersModel;
import com.ebutor.models.POMasterLookUpModel;
import com.ebutor.models.PincodeDataModel;
import com.ebutor.models.PriceHistoryModel;
import com.ebutor.models.ProductFilterModel;
import com.ebutor.models.ProductModel;
import com.ebutor.models.ProductSKUListModel;
import com.ebutor.models.ProductsModel;
import com.ebutor.models.ProductsResponse;
import com.ebutor.models.ReasonsModel;
import com.ebutor.models.RecommendedCategoryModel;
import com.ebutor.models.RecommendedProductModel;
import com.ebutor.models.RetailersModel;
import com.ebutor.models.ReviewModel;
import com.ebutor.models.SKUModel;
import com.ebutor.models.SKUPacksModel;
import com.ebutor.models.SODashboardModel;
import com.ebutor.models.ShipmentTrackingModel;
import com.ebutor.models.SortModel;
import com.ebutor.models.SpecificationsModel;
import com.ebutor.models.StateModel;
import com.ebutor.models.StatusModel;
import com.ebutor.models.SubCategoryModel;
import com.ebutor.models.TabData;
import com.ebutor.models.TabModel;
import com.ebutor.models.TalleyModel;
import com.ebutor.models.TeamDashboard;
import com.ebutor.models.TestProductModel;
import com.ebutor.models.TopBrandsModel;
import com.ebutor.models.UserAddressModel;
import com.ebutor.models.WishListModel;
import com.ebutor.srm.models.PODetailModel;
import com.ebutor.srm.models.POModel;
import com.ebutor.srm.models.SupplierMasterLookupModel;
import com.ebutor.srm.models.SupplierModel;
import com.ebutor.srm.models.WarehouseModel;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

public class JSONParser {

    private SharedPreferences mSharedPreferences;

    public ProductSKUListModel getProducts(JSONObject jsonObject) {
        ProductSKUListModel productSKUListModel = new ProductSKUListModel();
        try {
            int totalItemsCount;
            JSONObject dataObj = jsonObject.optJSONObject("data");
            try {
                String totalProducts = dataObj.optString("TotalProducts");
                if (totalProducts != null) {
                    totalItemsCount = totalProducts.equalsIgnoreCase("0") ? 0 : Integer.parseInt(dataObj.optString("TotalProducts"));
                } else {
                    totalItemsCount = 0;
                }
            } catch (Exception e) {
                totalItemsCount = 0;
            }
//            totalItemsCount = 100;
            productSKUListModel.setTotatItemsCount(totalItemsCount);
            if (totalItemsCount > 0) {
                JSONArray dataArray = dataObj.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    ArrayList<ProductModel> productModelArrayList = new ArrayList<>();
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject productObj = dataArray.optJSONObject(i);
                        ProductModel model = new ProductModel();
//                                    model.setPosition(i);
                        model.setProductName(productObj.optString("name"));
                        model.setProductId(productObj.optString("product_id"));
                        model.setProductRating(productObj.optString("rating"));
                        try {
                            model.setProductDescription(productObj.optString("description"));
                        } catch (Exception e) {

                        }

                        JSONArray variantsArray = productObj.optJSONArray("variants");
                        ArrayList<SKUModel> skuModelArrayList = new ArrayList<>();
                        if (variantsArray != null && variantsArray.length() > 0) {
                            for (int j = 0; j < variantsArray.length(); j++) {
                                JSONObject variantObj = variantsArray.optJSONObject(j);
                                SKUModel skuModel = new SKUModel();
                                skuModel.setPosition(i);
                                skuModel.setSkuId(variantObj.optString("variant_id"));
                                skuModel.setAvailableQuantity(variantObj.optString("quantity"));
                                skuModel.setProductVariantId(variantObj.optString("product_variant_id"));
                                skuModel.setProductName(variantObj.optString("product_name").trim());
                                skuModel.setProductDescription(variantObj.optString("description"));
                                skuModel.setSkuImage(variantObj.optString("image"));
                                skuModel.setSku(variantObj.optString("sku"));
                                skuModel.setMrp(variantObj.optString("mrp"));
                                skuModel.setDefault(variantObj.optString("is_default").equals("1"));
                                skuModel.setSelected(false);
                                skuModel.setSkuName(variantObj.optString("name"));

                                JSONArray packsArray = variantObj.optJSONArray("pack");
                                ArrayList<SKUPacksModel> skuPacksModelArrayList = new ArrayList<>();
                                if (packsArray != null && packsArray.length() > 0) {
                                    for (int k = 0; k < packsArray.length(); k++) {
                                        JSONObject packObj = packsArray.optJSONObject(k);
                                        SKUPacksModel packsModel = new SKUPacksModel();

                                        packsModel.setPackPrice(packObj.optString("dealer_price"));
                                        packsModel.setVariantPriceId(packObj.optString("variant_price_id"));
                                        packsModel.setUnitPrice(packObj.optString("unit_price"));
                                        packsModel.setPackSize(packObj.optString("pack_size"));
                                        packsModel.setMargin(packObj.optString("margin"));

                                        skuPacksModelArrayList.add(packsModel);

                                    }
                                }
                                skuModel.setSkuPacksModelArrayList(skuPacksModelArrayList);
                                if (skuModel.getSkuPacksModelArrayList() != null && skuModel.getSkuPacksModelArrayList().size() > 0)
                                    skuModelArrayList.add(skuModel);
                            }
                        }
                        model.setSkuModelArrayList(skuModelArrayList);
                        if (!productModelArrayList.contains(model))
                            productModelArrayList.add(model);
                    }
                    productSKUListModel.setArrProducts(productModelArrayList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productSKUListModel;
    }

    public ArrayList<HomeModel> getHomePageDataNew(JSONObject jsonObject) {
        ArrayList<HomeModel> arrhomeData = new ArrayList<>();
        try {

            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    HomeModel homeModel = new HomeModel();
                    JSONObject dataObj = dataArray.optJSONObject(i);
                    String flag = dataObj.optString("flag");
                    String key = dataObj.optString("key");
                    String displayTitle = dataObj.optString("display_title");
                    JSONArray arrModels = dataObj.optJSONArray("items");
                    if (arrModels != null && arrModels.length() > 0) {
                        ArrayList<HomeDataModel> modelArrayList = new ArrayList<>();
                        for (int j = 0; j < arrModels.length(); j++) {
                            HomeDataModel homeDataModel = new HomeDataModel();
                            JSONObject modelObj = arrModels.optJSONObject(j);
                            String id = modelObj.optString("id");
                            String name = modelObj.optString("name");
                            JSONArray imagesArray = modelObj.optJSONArray("image");
                            ArrayList<String> arrImages = new ArrayList<>();
                            if (imagesArray != null && imagesArray.length() > 0) {
                                for (int k = 0; k < imagesArray.length(); k++) {
                                    String image = imagesArray.optString(k);
                                    arrImages.add(image);
                                }
                            }

                            homeDataModel.setId(id);
                            homeDataModel.setName(name);
                            homeDataModel.setArrImages(arrImages);
                            homeDataModel.setFlag(flag);
                            homeDataModel.setDisplayTitle(displayTitle);
                            homeDataModel.setKey(key);

                            modelArrayList.add(homeDataModel);
                        }
                        homeModel.setModelArrayList(modelArrayList);
                    }

                    homeModel.setFlag(flag);
                    homeModel.setKey(key);
                    homeModel.setTitle(displayTitle);
                    arrhomeData.add(homeModel);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrhomeData;
    }

    public TabData getTabData(JSONObject jsonObject) {
        TabData tabData = new TabData();
        ArrayList<TabModel> arrTabs = new ArrayList<>();
        try {
            JSONArray tabsArray = jsonObject.getJSONArray("data");
            JSONObject ecashObj = jsonObject.optJSONObject("ecash_details");
            if (ecashObj != null) {
                double creditLimit = ecashObj.optDouble("creditlimit");
                double eCash = ecashObj.optDouble("ecash");
                double payementDue = ecashObj.optDouble("payment_due");

                tabData.setEcash(true);
                tabData.setCreditLimit(creditLimit);
                tabData.setEcash(eCash);
                tabData.setPaymentDue(payementDue);
            } else {
                tabData.setEcash(false); // to know whether ecash is available or not
            }
            if (tabsArray != null && tabsArray.length() > 0) {
                TabModel tabModel = new TabModel();
                tabModel.setTabValue("");
                tabModel.setTabName("Home");
                arrTabs.add(tabModel);
                for (int i = 0; i < tabsArray.length(); i++) {
                    JSONObject tabObj = tabsArray.getJSONObject(i);
                    String tabId = tabObj.getString("id");
                    String tabName = tabObj.getString("name");
                    String tabValue = tabObj.getString("value");
                    if (!tabName.equalsIgnoreCase("PRICE DROP")) {
                        TabModel model = new TabModel();
                        model.setTabId(tabId);
                        model.setTabName(tabName);
                        model.setTabValue(tabValue);
                        arrTabs.add(model);
                    }
                }
                tabData.setArrTabs(arrTabs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tabData;
    }

    public ShipmentTrackingModel getShipmentTrackingData(JSONObject jsonObject) {
        ShipmentTrackingModel shipmentTrackingModel = new ShipmentTrackingModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");

            JSONObject trackingObj = dataObj.optJSONObject("trackingData");

            shipmentTrackingModel.setDocketNo(trackingObj.optString("docket_no"));
            shipmentTrackingModel.setDeliveryDate(trackingObj.optString("delivery_type"));
            shipmentTrackingModel.setOrderId(trackingObj.optString("order_id"));
            shipmentTrackingModel.setOrderTotal(trackingObj.optString("order_total"));
            shipmentTrackingModel.setPaymentMethod(trackingObj.optString("payment_method"));
            shipmentTrackingModel.setShippingAddress1(trackingObj.optString("shipping_address_1"));
            shipmentTrackingModel.setShippingAddress2(trackingObj.optString("shipping_address_2"));
            shipmentTrackingModel.setShippingCity(trackingObj.optString("shipping_city"));
            shipmentTrackingModel.setShippingPostcode(trackingObj.optString("shipping_postcode"));
            shipmentTrackingModel.setOrderDate(trackingObj.optString("order_date"));
            shipmentTrackingModel.setSellerInvoiceNo(trackingObj.optString("seller_invoice_no"));

            JSONObject historyObj = dataObj.optJSONObject("history");

            shipmentTrackingModel.setProcessedDate(historyObj.optString("processedDate"));
            shipmentTrackingModel.setShippedDate(historyObj.optString("shipped"));
            shipmentTrackingModel.setExpectedDeliveryDate(historyObj.optString("expectedDeliveryDate"));
            shipmentTrackingModel.setDeliveryDate(historyObj.optString("deliveryDate"));
            shipmentTrackingModel.setCancelDate(historyObj.optString("cancelDate"));
            shipmentTrackingModel.setReturnDate(historyObj.optString("returnDate"));
            shipmentTrackingModel.setDocketStatus(historyObj.optString("docketStatus"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return shipmentTrackingModel;
    }

    public ProductModel getProductDetail(JSONObject jsonObject) {
        ProductModel productModel = new ProductModel();
        try {

            JSONObject dataObject = jsonObject.optJSONObject("data");
            JSONObject productDataObj = dataObject.optJSONObject("data");

            ArrayList<String> arrProductImgs = new ArrayList<>();
            ArrayList<SpecificationsModel> arrSpecs = new ArrayList<>();
            ArrayList<ReviewModel> arrReviews = new ArrayList<>();
            ArrayList<ProductsModel> arrRelatedProducts = new ArrayList<>();

            productModel.setProductId(productDataObj.optString("product_id"));
            productModel.setProductRating(productDataObj.optString("rating"));
            JSONArray variantsArr = productDataObj.optJSONArray("variants");
            ArrayList<SKUModel> skuModelArrayList = new ArrayList<>();
            for (int i = 0; i < variantsArr.length(); i++) {
                JSONObject skuObject = variantsArr.optJSONObject(i);
                ArrayList<SKUPacksModel> packsModelArrayList = new ArrayList<SKUPacksModel>();
                SKUModel skuModel = new SKUModel();
                skuModel.setSkuId(skuObject.optString("variant_id"));
                skuModel.setProductVariantId(skuObject.optString("product_variant_id"));
                skuModel.setProductName(skuObject.optString("product_name").trim());
                skuModel.setProductDescription(skuObject.optString("description").equalsIgnoreCase("null") ? "" : skuObject.optString("description"));
                skuModel.setModel(skuObject.optString("model"));
                skuModel.setDefault(skuObject.optString("is_default").equalsIgnoreCase("1") ? true : false);
                skuModel.setSkuImage(skuObject.optString("image"));
                skuModel.setAvailableQuantity(skuObject.optString("quantity"));
                skuModel.setSku(skuObject.optString("sku"));
                skuModel.setMrp(skuObject.optString("mrp"));
                skuModel.setSkuName(skuObject.optString("name"));
                JSONArray packsArr = skuObject.optJSONArray("pack");
                DecimalFormat decimalFormat = new DecimalFormat("##.##");
                for (int j = 0; j < packsArr.length(); j++) {
                    JSONObject packObject = packsArr.optJSONObject(j);
                    SKUPacksModel skuPacksModel = new SKUPacksModel();
                    skuPacksModel.setVariantPriceId(packObject.optString("variant_price_id"));
                    skuPacksModel.setPackSize(packObject.optString("pack_size"));
                    skuPacksModel.setPackPrice(TextUtils.isEmpty(packObject.optString("dealer_price")) ? "0" : decimalFormat.format(Double.parseDouble(packObject.getString("dealer_price"))));
                    skuPacksModel.setMargin(TextUtils.isEmpty(packObject.optString("margin")) ? "0" : decimalFormat.format(Double.parseDouble(packObject.getString("margin"))));
                    skuPacksModel.setUnitPrice(TextUtils.isEmpty(packObject.optString("unit_price")) ? "0" : decimalFormat.format(Double.parseDouble(packObject.getString("unit_price"))));
                    packsModelArrayList.add(skuPacksModel);
                }
                skuModel.setSkuPacksModelArrayList(packsModelArrayList);
                skuModelArrayList.add(skuModel);
                JSONArray arrImages = skuObject.optJSONArray("images");
                arrProductImgs = new ArrayList<>();
                for (int k = 0; k < arrImages.length(); k++) {
                    JSONObject objImage = arrImages.optJSONObject(k);
                    arrProductImgs.add(objImage.optString("image"));
                }
                skuModel.setArrImages(arrProductImgs);
                JSONObject objSpec = skuObject.optJSONObject("specifications");
                arrSpecs = new ArrayList<>();
                Iterator<String> keySet = objSpec.keys();
                while ((keySet.hasNext())) {
                    String key = keySet.next();
                    if (!key.equalsIgnoreCase("-1")) {
                        SpecificationsModel specificationsModel = new SpecificationsModel();
                        String specValue = objSpec.optString(key);
                        key = key.replace("_", " ");
                        specValue = specValue.replace("_", " ");
                        specificationsModel.setSpecName(key);
                        specificationsModel.setSpecValue(specValue);
                        if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(specValue))
                            arrSpecs.add(specificationsModel);
                    }
                }
                skuModel.setArrSpecs(arrSpecs);
            }

            JSONArray reviewArray = dataObject.optJSONArray("reviews");
            if (reviewArray != null && reviewArray.length() > 0) {
                for (int i = 0; i < reviewArray.length(); i++) {
                    JSONObject reviewObj = reviewArray.getJSONObject(i);
                    String review = reviewObj.optString("text");
                    String customer_id = reviewObj.optString("customer_id");
                    String author = reviewObj.optString("author");
                    String rating = reviewObj.optString("rating");

                    ReviewModel model = new ReviewModel();
                    model.setReview(review);
                    model.setAuthor(author);
                    model.setRating(rating);
                    model.setCustomerId(customer_id);
                    arrReviews.add(model);
                }
            }
            productModel.setArrReviews(arrReviews);
            productModel.setSkuModelArrayList(skuModelArrayList);
            JSONArray relatedProductsArray = dataObject.optJSONArray("related_products");
            if (relatedProductsArray != null && relatedProductsArray.length() > 0) {
                for (int i = 0; i < relatedProductsArray.length(); i++) {
                    JSONObject relatedPrdObj = relatedProductsArray.getJSONObject(i);
                    String relatedId = relatedPrdObj.optString("related_id");

                    String image = relatedPrdObj.optString("image");
                    String prdName = relatedPrdObj.optString("name");
                    ProductsModel model = new ProductsModel();
                    model.setImage(image);
                    model.setProduct_id(relatedId);
                    model.setName(prdName);
                    arrRelatedProducts.add(model);
                }

            }
            productModel.setArrRelatedPrds(arrRelatedProducts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productModel;
    }

    public String addReview(JSONObject jsonObject) {
        String rating = "";
        try {
            JSONObject dataObject = jsonObject.optJSONObject("data");
            JSONObject reviewObject = dataObject.optJSONObject("reviews");
            rating = reviewObject.optString("rating");
            if (rating == null || TextUtils.isEmpty(rating)) {
                rating = "0.0";
            }
            if (rating != null && !TextUtils.isEmpty(rating) && !rating.equalsIgnoreCase("null")) {
                DecimalFormat format = new DecimalFormat("0.0");
                double newmargin = Double.parseDouble(rating);
                rating = format.format(newmargin);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rating;
    }

    public ArrayList<CountryModel> getCountries(JSONObject jsonObject) {
        ArrayList<CountryModel> countriesList = new ArrayList<>();
        CountryModel _countryModel = new CountryModel();
        _countryModel.setCountryId("0");
        _countryModel.setCountryName("Country");
        countriesList.add(_countryModel);

        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject countryObj = dataArray.optJSONObject(i);
                    CountryModel countryModel = new CountryModel();
                    countryModel.setCountryId(countryObj.optString("country_id"));
                    countryModel.setCountryName(countryObj.optString("name"));

                    countriesList.add(countryModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countriesList;
    }

    public ArrayList<StateModel> getStates(JSONObject jsonObject) {
        ArrayList<StateModel> statesList = new ArrayList<>();
        StateModel _model = new StateModel();
        _model.setStateId("");
        _model.setStateName("Select State");
        statesList.add(_model);

        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject stateObj = dataArray.optJSONObject(i);
                    StateModel stateModel = new StateModel();
                    stateModel.setStateId(stateObj.optString("state_id"));
                    stateModel.setStateName(stateObj.optString("state_name"));
                    statesList.add(stateModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statesList;
    }

    public UserAddressModel addAddress(JSONObject jsonObject) {
        UserAddressModel userAddressModel = new UserAddressModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");

            String addressId = dataObj.optString("address_id");
            String name = dataObj.optString("FirstName").equalsIgnoreCase("null") ? "" : dataObj.optString("FirstName");
            String names[] = name.split(" ");
            String firstName = "";
            for (int j = 0; j < names.length - 1; j++) {
                firstName += names[j];
            }
            String address = dataObj.optString("Address");
            String address1 = dataObj.optString("Address1");
            String city = dataObj.optString("City");
            String pin = dataObj.optString("pin");
            String state = dataObj.optString("state");
            String country = dataObj.optString("country");
            String addressType = dataObj.optString("addressType");
            String telephone = dataObj.optString("telephone");
            String email = dataObj.optString("email");

            userAddressModel = new UserAddressModel();
            userAddressModel.setAddressId(addressId);
            userAddressModel.setFirstName(firstName);
            userAddressModel.setLastName(names[names.length - 1]);
            userAddressModel.setAddress(address);
            userAddressModel.setAddress1(address1);
            userAddressModel.setCity(city);
            userAddressModel.setPin(pin);
            userAddressModel.setState(state);
            userAddressModel.setCountry(country);
            userAddressModel.setAddressType(addressType);
            userAddressModel.setTelephone(telephone);
            userAddressModel.setEmail(email);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userAddressModel;
    }

    public String cancelRequest(JSONObject jsonObject) {
        String cancelstatus = "", cancelRequestId = "", pickupDate = "";
        try {
            JSONObject cancelObj = jsonObject.optJSONObject("data");
            cancelRequestId = cancelObj.optString("requestId");
            cancelstatus = cancelObj.optString("status");
            pickupDate = cancelObj.optString("pickupDate");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cancelstatus;
    }

    public ArrayList<FeaturedOfferModel> getFeaturedOffers(JSONObject jsonObject) {
        ArrayList<FeaturedOfferModel> arrFeaturedOffers = new ArrayList<>();
        try {
            JSONArray featuredOffersArray = jsonObject.getJSONArray("data");
            if (featuredOffersArray != null && featuredOffersArray.length() > 0) {
                for (int i = 0; i < featuredOffersArray.length(); i++) {
                    JSONObject featuredOfferObj = featuredOffersArray.getJSONObject(i);
                    if (featuredOfferObj != null) {
                        String margin = featuredOfferObj.getString("margin");
                        String categoryId = featuredOfferObj.getString("categoryID");
                        String categoryName = featuredOfferObj.getString("Categoryname");
                        String image = featuredOfferObj.getString("image");

                        FeaturedOfferModel model = new FeaturedOfferModel();
                        model.setCategoryId(categoryId);
                        model.setCategoryName(categoryName);
                        model.setCategoryImage(image);
                        model.setMargin(margin);
                        arrFeaturedOffers.add(model);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrFeaturedOffers;
    }

    public ArrayList<RecommendedCategoryModel> getRecommendedCategories(JSONObject jsonObject) {
        ArrayList<RecommendedCategoryModel> arrRecommendedCategories = new ArrayList<>();
        try {
            JSONArray recommendedCategories = jsonObject.getJSONArray("data");
            if (recommendedCategories != null && recommendedCategories.length() > 0) {
                for (int i = 0; i < recommendedCategories.length(); i++) {
                    JSONObject recommendedObj = recommendedCategories.getJSONObject(i);
                    if (recommendedObj != null) {
                        String categoryId = recommendedObj.getString("categoryID");
                        String categoryName = recommendedObj.getString("Categoryname");
                        String image = recommendedObj.getString("image");

                        RecommendedCategoryModel model = new RecommendedCategoryModel();
                        model.setCategoryName(categoryName);
                        model.setCategoryImage(image);
                        model.setCategoryId(categoryId);
                        arrRecommendedCategories.add(model);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrRecommendedCategories;
    }

    public ArrayList<RecommendedProductModel> getRecommendedProducts(JSONObject jsonObject) {
        ArrayList<RecommendedProductModel> arrRecommendedProducts = new ArrayList<>();
        try {
            JSONArray recommendedProducts = jsonObject.getJSONArray("data");
            if (recommendedProducts != null && recommendedProducts.length() > 0) {
                for (int i = 0; i < recommendedProducts.length(); i++) {
                    JSONObject recommendedObj = recommendedProducts.getJSONObject(i);
                    if (recommendedObj != null) {
                        String productId = recommendedObj.getString("product_id");
                        String categoryId = recommendedObj.getString("category_id");
                        String name = recommendedObj.getString("name");
                        String image = recommendedObj.getString("image");

                        RecommendedProductModel model = new RecommendedProductModel();
                        model.setProductId(productId);
                        model.setCategoryId(categoryId);
                        model.setRecommendedProdImage(image);
                        model.setRecommendedProdName(name);

                        arrRecommendedProducts.add(model);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrRecommendedProducts;
    }

    public ArrayList<TopBrandsModel> getTopBrands(JSONObject jsonObject) {
        ArrayList<TopBrandsModel> arrTopBrands = new ArrayList<>();
        try {
            JSONArray topBrandsArray = jsonObject.getJSONArray("data");
            if (topBrandsArray != null && topBrandsArray.length() > 0) {
                for (int i = 0; i < topBrandsArray.length(); i++) {
                    JSONObject topBrandObject = topBrandsArray.optJSONObject(i);
                    if (topBrandObject != null) {
                        String manufacturedId = topBrandObject.optString("id");
                        String name = topBrandObject.optString("name");
                        JSONArray imagesArray = topBrandObject.optJSONArray("image");
                        ArrayList<String> arrImages = new ArrayList<>();
                        if (imagesArray != null && imagesArray.length() > 0) {
                            for (int k = 0; k < imagesArray.length(); k++) {
                                String image = imagesArray.optString(k);
                                arrImages.add(image);
                            }
                        }

                        TopBrandsModel model = new TopBrandsModel();
                        model.setManufacturerId(manufacturedId);
                        model.setTopBrandImages(arrImages);
                        model.setTopBrandName(name);
                        arrTopBrands.add(model);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrTopBrands;
    }

    public ArrayList<BannerModel> getBanners(JSONObject jsonObject) {
        ArrayList<BannerModel> arrBanners = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    BannerModel bannerModel = new BannerModel();
                    JSONObject dataObj = dataArray.optJSONObject(i);
                    String bannerId = dataObj.optString("banner_id");
                    String image = dataObj.optString("image_url");
                    String title = dataObj.optString("title");
                    String navigatorObjects = dataObj.optString("navigator_objects");
                    String navigateObjectId = dataObj.optString("navigate_object_id");
                    String frequency = dataObj.optString("frequency");

                    bannerModel.setBannerId(bannerId);
                    bannerModel.setImage(image);
                    bannerModel.setTitle(title);
                    bannerModel.setNavigatorObject(navigatorObjects);
                    bannerModel.setNavigatorObjectId(navigateObjectId);
                    bannerModel.setFrequency(frequency);

                    arrBanners.add(bannerModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrBanners;
    }

    public int addCart(JSONObject jsonObject) {
        ArrayList<String> cartIds = new ArrayList<>();
        int cartcount = 0;
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            JSONArray cartArray = dataObj.optJSONArray("cart");
            try {
                cartcount = dataObj.optString("cartcount") != null ? Integer.parseInt(dataObj.optString("cartcount")) : 0;
            } catch (Exception e) {
                cartcount = 0;
            }
            if (cartArray != null && cartArray.length() > 0) {
                for (int i = 0; i < cartArray.length(); i++) {
                    cartIds.add(cartArray.optString(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cartcount;
    }

    public ArrayList<WishListModel> getWishList(JSONObject jsonObject) {
        ArrayList<WishListModel> arrWishList = new ArrayList<>();
        try {
            JSONArray listArray = jsonObject.optJSONArray("data");
            if (listArray != null && listArray.length() > 0) {
                for (int i = 0; i < listArray.length(); i++) {
                    JSONObject listObj = listArray.optJSONObject(i);
                    WishListModel model = new WishListModel();
                    model.setListId(listObj.optString("buyer_listing_id"));
                    model.setListName(listObj.optString("listing_name"));
                    model.setCreatedDate(listObj.optString("create_date"));
                    arrWishList.add(model);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrWishList;
    }

    public OrdersModel getOrders(JSONObject jsonObject) {
        OrdersModel _ordersModel = new OrdersModel();
        int count = 0;
        ArrayList<OrdersModel> ordersList = new ArrayList<OrdersModel>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            count = jsonObject.optInt("count");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    OrdersModel ordersModel = new OrdersModel();
                    JSONObject ordersObj = dataArray.optJSONObject(i);
                    ordersModel.setOrderNumber(ordersObj.optString("order_number"));
                    ordersModel.setOrderCode(ordersObj.optString("order_code"));
                    ordersModel.setOrderStatus(ordersObj.optString("order_status"));
                    ordersModel.setDate(ordersObj.optString("date"));
                    ordersModel.setTotalAmount(ordersObj.optString("total_amount"));
                    ordersList.add(ordersModel);
                }
            }
            _ordersModel.setCount(count);
            _ordersModel.setArrOrders(ordersList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _ordersModel;
    }

    public ArrayList<OrdersModel> getPOs(JSONObject jsonObject) {
        ArrayList<OrdersModel> ordersList = new ArrayList<OrdersModel>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    OrdersModel ordersModel = new OrdersModel();
                    JSONObject ordersObj = dataArray.optJSONObject(i);
                    ordersModel.setOrderNumber(ordersObj.optString("po_id"));
                    ordersModel.setOrderCode(ordersObj.optString("po_code"));
                    ordersModel.setOrderStatus(ordersObj.optString("po_status"));
                    ordersModel.setPoApprovalStatus(ordersObj.optString("po_approval_status"));
                    ordersModel.setDate(ordersObj.optString("date"));
                    ordersModel.setSupplierName(ordersObj.optString("supplier_name"));
                    ordersList.add(ordersModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ordersList;
    }

    public PODetailModel getPoDetail(JSONObject jsonObject) {
        PODetailModel poDetailModel = new PODetailModel();
        try {
            ArrayList<POModel> arrPOs = new ArrayList<>();
            JSONObject dataObj = jsonObject.optJSONObject("data");

            JSONObject supplierAddressObj = dataObj.optJSONObject("Supplier_address");
            String supplierName = supplierAddressObj.optString("supplier_name");
            String supplierId = supplierAddressObj.optString("supplier_id");
            String supplierAddress1 = supplierAddressObj.optString("address1");
            String supplierAddress2 = supplierAddressObj.optString("address2");
            String supplierCity = supplierAddressObj.optString("city");
            String supplierState = supplierAddressObj.optString("state");
            String supplierCountry = supplierAddressObj.optString("country");
            String supplierPincode = supplierAddressObj.optString("pincode");
            String supplierMobile = supplierAddressObj.optString("phone_no");
            String supplierEmail = supplierAddressObj.optString("email_id");

            JSONObject deliveryAddressObj = dataObj.optJSONObject("Delivery_address");
            String whName = deliveryAddressObj.optString("warehouse_name");
            String deliveryAddress1 = deliveryAddressObj.optString("address1");
            String deliveryAddress2 = deliveryAddressObj.optString("address2");
            String deliveryCity = deliveryAddressObj.optString("city");
            String deliveryState = deliveryAddressObj.optString("state");
            String deliveryPincode = deliveryAddressObj.optString("pincode");
            String deliveryCountry = deliveryAddressObj.optString("country");
            String deliveryMobile = deliveryAddressObj.optString("phone_no");
            String deliveryEmail = deliveryAddressObj.optString("email");
            String contactName = deliveryAddressObj.optString("contact_name");

            JSONObject poDetailObj = dataObj.optJSONObject("PO_Details");
            String poNumber = poDetailObj.optString("PO_Number");
            String poId = poDetailObj.optString("po_id");
            String poDate = poDetailObj.optString("po_date");
            String warehouseId = poDetailObj.optString("warehouse_id");
            String deliveryDate = poDetailObj.optString("delivery_date");
            String poStatus = poDetailObj.optString("po_status");
            String poApprovalStatus = poDetailObj.optString("po_approval_status");
            String poType = poDetailObj.optString("po_type");
            String totalAmount = poDetailObj.optString("total_amount");
            String createdBy = poDetailObj.optString("created_by");
            String applyDiscountOnBill = poDetailObj.optString("apply_discount_on_bill").equals("0") ? "No" : "Yes";
            String discountType = poDetailObj.optString("discount_type").equals("0") ? "Flat" : "%";
            String discount = poDetailObj.optString("discount");
            boolean isApproval = poDetailObj.optBoolean("approval_button");

            try {
                discount = String.format("%.2f", Float.parseFloat(discount));
            } catch (Exception e) {
                e.printStackTrace();
            }

            JSONArray productsArray = dataObj.optJSONArray("products");
            if (productsArray != null && productsArray.length() > 0) {
                for (int i = 0; i < productsArray.length(); i++) {
                    POModel poModel = new POModel();
                    JSONObject productObj = productsArray.optJSONObject(i);

                    String productId = productObj.optString("product_id");
                    String productName = productObj.optString("product_name");
                    String sku = productObj.optString("sku");
                    String quantity = productObj.optString("quantity");
                    String uom = productObj.optString("uom").equalsIgnoreCase("null") ? "" : productObj.optString("uom");
                    String noOfEaches = productObj.optString("no_of_eaches");
                    String freeQty = productObj.optString("free_qty");
                    String freeUom = productObj.optString("free_uom").equalsIgnoreCase("null") ? "" : productObj.optString("free_uom");
                    String freeEaches = productObj.optString("free_eaches");
                    String mrp = productObj.optString("mrp");
                    String unitPrice = productObj.optString("unit_price");
                    String price = productObj.optString("price");
                    String isTaxIncluded = productObj.optString("is_tax_included");
                    String taxName = productObj.optString("tax_name");
                    String taxPer = productObj.optString("tax_per");
                    String taxAmt = productObj.optString("tax_amt");
                    String subTotal = productObj.optString("sub_total");
                    String packsizeValue = productObj.optString("packsize_value");
                    String freepacksizeValue = productObj.optString("freepacksize_value");
                    String packId = productObj.optString("pack_id");
                    String freepackId = productObj.optString("freepack_id");
                    String parentId = productObj.optString("parent_id");
                    String applyDiscountOnBillProduct = productObj.optString("apply_discount").equals("0") ? "No" : "Yes";
                    String discountTypeProduct = productObj.optString("discount_type").equals("0") ? "Flat" : "%";
                    String discountProduct = TextUtils.isEmpty(productObj.optString("discount")) ? "0.00" : productObj.optString("discount");
                    String slp = productObj.optString("slp").equalsIgnoreCase("null") ? "0.00" : productObj.optString("slp");
                    String std = productObj.optString("std").equalsIgnoreCase("null") ? "0.00" : productObj.optString("std");
                    String thirtyd = productObj.optString("thirtyd").equalsIgnoreCase("null") ? "0.00" : productObj.optString("thirtyd");
                    String availableInventory = productObj.optString("available_inventory");

                    JSONArray packsArray = productObj.optJSONArray("packs");
                    if (packsArray != null && packsArray.length() > 0) {
                        ArrayList<NewPacksModel> arrPacks = new ArrayList<>();
                        for (int j = 0; j < packsArray.length(); j++) {
                            NewPacksModel newPacksModel = new NewPacksModel();
                            JSONObject packObj = packsArray.optJSONObject(j);
                            String id = packObj.optString("pack_id");
                            String level = packObj.optString("level");
                            String levelName = packObj.optString("level_name");
                            String packSize = packObj.optString("pack_size");

                            newPacksModel.setProductPackId(id);
                            newPacksModel.setLevel(level);
                            newPacksModel.setLevelName(levelName);
                            newPacksModel.setPackSize(packSize);
                            newPacksModel.setNoOfEaches(packSize);

                            arrPacks.add(newPacksModel);
                        }
                        poModel.setArrUOMs(arrPacks);
                    }
                    try {
                        mrp = String.format("%.2f", Float.parseFloat(mrp));
                        unitPrice = String.format("%.2f", Float.parseFloat(unitPrice));
                        price = String.format("%.2f", Float.parseFloat(price));
                        taxPer = String.format("%.2f", Float.parseFloat(taxPer));
                        taxAmt = String.format("%.2f", Float.parseFloat(taxAmt));
                        subTotal = String.format("%.2f", Float.parseFloat(subTotal));
                        discountProduct = String.format("%.2f", Float.parseFloat(discountProduct));
                        slp = String.format("%.2f", Float.parseFloat(slp));
                        std = String.format("%.2f", Float.parseFloat(std));
                        thirtyd = String.format("%.2f", Float.parseFloat(thirtyd));
                        availableInventory = String.valueOf(Math.round(Float.parseFloat(availableInventory)));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    poModel.setProductId(productId);
                    poModel.setProductName(productName);
                    poModel.setSkuCode(sku);
                    poModel.setQuantity(quantity);
                    poModel.setUom(uom);
                    poModel.setNoOfEaches(noOfEaches);
                    poModel.setFreeQty(freeQty);
                    poModel.setFreeUOM(freeUom);
                    poModel.setFreeEaches(freeEaches);
                    poModel.setMrp(mrp);
                    poModel.setUnitPrice(unitPrice);
                    poModel.setPrice(price);
                    poModel.setIsTax(isTaxIncluded);
                    poModel.setTaxName(taxName);
                    poModel.setTaxPer(taxPer);
                    poModel.setTaxAmount(taxAmt);
                    poModel.setSubTotal(subTotal);
                    poModel.setProductTotal(subTotal);
                    poModel.setPackSizeValue(packsizeValue);
                    poModel.setFreePackSizeValue(freepacksizeValue);
                    poModel.setPackId(packId);
                    poModel.setFreePackId(freepackId);
                    poModel.setParentId(parentId);
                    poModel.setApplyDiscount(applyDiscountOnBillProduct);
                    poModel.setDiscountType(discountTypeProduct);
                    poModel.setDiscount(discountProduct);
                    poModel.setSlp(slp);
                    poModel.setStd(std);
                    poModel.setThirtyd(thirtyd);
                    poModel.setAvailableInventory(availableInventory);

                    arrPOs.add(poModel);
                }
            }

            poDetailModel.setSupplierName(supplierName);
            poDetailModel.setSupplierId(supplierId);
            poDetailModel.setSupplierAddress1(supplierAddress1);
            poDetailModel.setSupplierAddress2(supplierAddress2);
            poDetailModel.setSupplierCity(supplierCity);
            poDetailModel.setSupplierState(supplierState);
            poDetailModel.setSupplierCountry(supplierCountry);
            poDetailModel.setSupplierPincode(supplierPincode);
            poDetailModel.setSupplierMobile(supplierMobile);
            poDetailModel.setSupplierEmail(supplierEmail);
            poDetailModel.setWhName(whName);
            poDetailModel.setDeliveryAddress1(deliveryAddress1);
            poDetailModel.setDeliveryAddress2(deliveryAddress2);
            poDetailModel.setDeliveryCity(deliveryCity);
            poDetailModel.setDeliveryState(deliveryState);
            poDetailModel.setDeliveryPincode(deliveryPincode);
            poDetailModel.setDeliveryCountry(deliveryCountry);
            poDetailModel.setDeliveryMobile(deliveryMobile);
            poDetailModel.setDeliveryEmail(deliveryEmail);
            poDetailModel.setContactName(contactName);
            poDetailModel.setPoNumber(poNumber);
            poDetailModel.setPoId(poId);
            poDetailModel.setPoDate(poDate);
            poDetailModel.setWarehouseId(warehouseId);
            poDetailModel.setDeliveryDate(deliveryDate);
            poDetailModel.setPoStatus(poStatus);
            poDetailModel.setPoApprovalStatus(poApprovalStatus);
            poDetailModel.setPoType(poType);
            poDetailModel.setTotalAmount(totalAmount);
            poDetailModel.setCreatedBy(createdBy);
            poDetailModel.setApproval(isApproval);
            poDetailModel.setApplyDiscount(applyDiscountOnBill);
            poDetailModel.setDiscountType(discountType);
            poDetailModel.setDiscount(discount);
            poDetailModel.setArrPos(arrPOs);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return poDetailModel;
    }

    public OrderDetailModel getOrderDetail(JSONObject jsonObject) {
        OrderDetailModel orderDetailModel = new OrderDetailModel();
        try {
            ArrayList<OrdersModel> arrOrders = new ArrayList<>();
            JSONObject dataObj = jsonObject.optJSONObject("data");
            String orderId = dataObj.optString("order_id");
            String orderCode = dataObj.optString("order_code");
            String orderDate = dataObj.optString("date_added");
            String totalStatus = dataObj.optString("status");

            String shippingFirstName = dataObj.optString("shipping_firstname");
            String shippingLastName = dataObj.optString("shipping_lastname");
            String shippingEmail = dataObj.optString("shipping_email");
            String shippingTelephone = dataObj.optString("shipping_telephone");
            String shippingAddress = dataObj.optString("shipping_address");
            String shippingCity = dataObj.optString("shipping_city");
            String shippingPin = dataObj.optString("shipping_pin");
            String shippingCountry = dataObj.optString("shipping_country");
            String subTotal = dataObj.optString("sub_total");
            double discountAmount = dataObj.optDouble("discount_amount");
            String grandTotal = dataObj.optString("total");

            ArrayList<ShipmentTrackingModel> arrTracking = new ArrayList<>();
            JSONArray trackingArray = dataObj.optJSONArray("shipping_track_details");
            if (trackingArray != null && trackingArray.length() > 0) {
                for (int i = 0; i < trackingArray.length(); i++) {
                    JSONObject trackingObj = trackingArray.optJSONObject(i);
                    ShipmentTrackingModel shipmentTrackingModel = new ShipmentTrackingModel();
                    String carrierName = trackingObj.optString("carrier_name");
                    String shipMethod = trackingObj.optString("ship_method");
                    String trackingId = trackingObj.optString("tracking_id");
                    String vehicleNumber = trackingObj.optString("vehicle_number");
                    String representativeName = trackingObj.optString("representative_name");
                    String contactNumber = trackingObj.optString("contact_number");
                    String qty = trackingObj.optString("qty");
                    String productId = trackingObj.optString("product_id");

                    shipmentTrackingModel.setCarrierName(carrierName);
                    shipmentTrackingModel.setShipMethod(shipMethod);
                    shipmentTrackingModel.setTrackingId(trackingId);
                    shipmentTrackingModel.setVehicleNumber(vehicleNumber);
                    shipmentTrackingModel.setRepName(representativeName);
                    shipmentTrackingModel.setContactNo(contactNumber);
                    shipmentTrackingModel.setQty(qty);
                    shipmentTrackingModel.setProductId(productId);
                    arrTracking.add(shipmentTrackingModel);
                }
            }

            orderDetailModel.setOrderId(orderId);
            orderDetailModel.setOrderCode(orderCode);
            orderDetailModel.setOrderDate(orderDate);
            orderDetailModel.setTotalStatus(totalStatus);
            orderDetailModel.setShippingFirstName(shippingFirstName);
            orderDetailModel.setShippingLastName(shippingLastName);
            orderDetailModel.setShippingEmail(shippingEmail);
            orderDetailModel.setShippingTelephone(shippingTelephone);
            orderDetailModel.setShippingAddress(shippingAddress);
            orderDetailModel.setShippingCity(shippingCity);
            orderDetailModel.setShippingPin(shippingPin);
            orderDetailModel.setShippingCountry(shippingCountry);
            orderDetailModel.setSubTotal(subTotal);
            orderDetailModel.setDiscountAmount(discountAmount);
            orderDetailModel.setGrandTotal(grandTotal);
            orderDetailModel.setArrTracking(arrTracking);

            JSONArray dataArray = dataObj.optJSONArray("products");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    OrdersModel ordersListModel = new OrdersModel();
                    JSONObject productObj = dataArray.optJSONObject(i);
                    String productId = productObj.optString("product_id");
                    String orderStatus = productObj.optString("order_status");
                    String variantId = productObj.optString("variant_id");
                    String image = productObj.optString("image");
                    String name = productObj.optString("name");
                    String variantName = productObj.optString("variant_name");
                    String packSize = productObj.optString("pack_size");
                    String dealerPrice = productObj.optString("dealer_price");
                    String unitPrice = productObj.optString("unit_price");
                    String margin = productObj.optString("margin");

                    int qty;
                    try {
                        qty = Integer.parseInt(productObj.optString("qty"));
                    } catch (Exception e) {
                        qty = 0;
                    }
                    ordersListModel.setProductId(productId);
                    ordersListModel.setOrderStatus(orderStatus);
                    ordersListModel.setVariantId(variantId);
                    ordersListModel.setImage(image);
                    ordersListModel.setName(name);
                    ordersListModel.setTotalPrice(dealerPrice);
                    ordersListModel.setTotalQuantity(qty + "");

                    arrOrders.add(ordersListModel);
                }
                orderDetailModel.setArrOrders(arrOrders);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderDetailModel;
    }

    public ArrayList<ReasonsModel> getCancelReasons(JSONObject jsonObject) {
        ArrayList<ReasonsModel> arrReasons = new ArrayList<>();
        try {
            JSONArray reasonsArray = jsonObject.optJSONArray("data");
            if (reasonsArray != null && reasonsArray.length() > 0) {
                for (int i = 0; i < reasonsArray.length(); i++) {
                    ReasonsModel model = new ReasonsModel();
                    JSONObject reasonsObj = reasonsArray.optJSONObject(i);
                    String reasonId = reasonsObj.optString("id");
                    String reason = reasonsObj.optString("name");
                    String value = reasonsObj.optString("value");

                    model.setReason(reason);
//                    model.setReasonId(reasonId);
                    model.setReasonValue(value);

                    arrReasons.add(model);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrReasons;
    }

    public ArrayList<ReasonsModel> getReturnReasons(JSONObject jsonObject) {
        ArrayList<ReasonsModel> arrReasons = new ArrayList<>();
        try {
            JSONArray reasonsArray = jsonObject.optJSONArray("data");
            if (reasonsArray != null && reasonsArray.length() > 0) {
                for (int i = 0; i < reasonsArray.length(); i++) {
                    ReasonsModel model = new ReasonsModel();
                    JSONObject reasonsObj = reasonsArray.optJSONObject(i);
                    String reasonId = reasonsObj.optString("id");
                    String reason = reasonsObj.optString("name");
                    String value = reasonsObj.optString("value");

                    model.setReason(reason);
//                    model.setReasonId(reasonId);
                    model.setReasonValue(value);

                    arrReasons.add(model);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrReasons;
    }

    public AddressModel getAddresses(JSONObject jsonObject) {
        AddressModel addressModel = new AddressModel();
        ArrayList<UserAddressModel> userAddressModelArrayList = new ArrayList<>();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            boolean flag = dataObj.optString("flag").equalsIgnoreCase("1") ? true : false;
            double eCashBal = dataObj.optDouble("ecash_amount");
            JSONArray dataArray = dataObj.optJSONArray("address");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject addressObj = dataArray.optJSONObject(i);
                    UserAddressModel userAddressModel = new UserAddressModel();
                    userAddressModel.setAddressId(addressObj.optString("address_id").equalsIgnoreCase("null") ? "" : addressObj.optString("address_id"));
                    String name = addressObj.optString("Firstname").equalsIgnoreCase("null") ? "" : addressObj.optString("Firstname");
                    String names[] = name.split(" ");
                    String firstName = "", lastName = "";
                    if (names.length > 1) {
                        for (int j = 0; j < names.length - 1; j++) {
                            firstName += names[j];
                        }
                        lastName = names[names.length - 1];
                    } else {
                        firstName = names[0];
                        lastName = "";
                    }
                    userAddressModel.setFirstName(firstName);
                    userAddressModel.setLastName(lastName);
                    userAddressModel.setAddress(addressObj.optString("Address").equalsIgnoreCase("null") ? "" : addressObj.optString("Address"));
                    userAddressModel.setAddress1(addressObj.optString("Address1").equalsIgnoreCase("null") ? "" : addressObj.optString("Address1"));
                    userAddressModel.setCity(addressObj.optString("City").equalsIgnoreCase("null") ? "" : addressObj.optString("City"));
                    userAddressModel.setPin(addressObj.optString("pin").equalsIgnoreCase("null") ? "" : addressObj.optString("pin"));
                    userAddressModel.setState(addressObj.optString("state").equalsIgnoreCase("null") ? "" : addressObj.optString("state"));
                    userAddressModel.setCountry(addressObj.optString("country").equalsIgnoreCase("null") ? "" : addressObj.optString("country"));
                    userAddressModel.setAddressType(addressObj.optString("addressType").equalsIgnoreCase("null") ? "" : addressObj.optString("addressType"));
                    userAddressModel.setTelephone(addressObj.optString("telephone").equalsIgnoreCase("null") ? "" : addressObj.optString("telephone"));
                    userAddressModel.setEmail(addressObj.optString("email").equalsIgnoreCase("null") ? "" : addressObj.optString("email"));

                    userAddressModelArrayList.add(userAddressModel);

                }
            }
            addressModel.setArrUserAddr(userAddressModelArrayList);
            addressModel.setFlag(flag);
            addressModel.seteCashBal(eCashBal);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return addressModel;
    }

    public OrdersModel addOrder(String data) {
        OrdersModel ordersModel = new OrdersModel();
        try {
            JSONObject dataObj = new JSONObject(data);
            String orderNumber = dataObj.optString("order_number");
            String date = dataObj.optString("date");
            String totalAmount = dataObj.optString("total_amount");
            String shippingDays = dataObj.optString("shiping_days");
            String orderStatus = dataObj.optString("order_status");
            String deliveryDate = dataObj.optString("delivery_date");

            ordersModel.setOrderNumber(orderNumber);
            ordersModel.setDate(date);
            ordersModel.setTotalAmount(totalAmount);
            ordersModel.setShippingDays(shippingDays);
            ordersModel.setOrderStatus(orderStatus);
            ordersModel.setDeliveryDate(deliveryDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ordersModel;
    }

    public ProductSKUListModel viewCart(JSONObject jsonObject) {

        double totalCartPrice = 0;
        int totalItems = 0;
        ProductSKUListModel productSKUListModel = new ProductSKUListModel();
        ArrayList<ProductModel> arrCart = new ArrayList<>();
        JSONArray dataArray = jsonObject.optJSONArray("data");
        if (dataArray != null && dataArray.length() > 0) {
            for (int i = 0; i < dataArray.length(); i++) {

                JSONObject productObj = dataArray.optJSONObject(i);
                ProductModel model = new ProductModel();

                int productTotalQty = 0;

//              model.setPosition(i);
                model.setProductName(productObj.optString("name"));
                model.setProductId(productObj.optString("product_id"));
                model.setCartId(productObj.optString("cartId"));
                model.setProductRating(productObj.optString("rating"));
                model.setProductDescription(productObj.optString("description"));
                model.setCartAddedDate(productObj.optString("date_added"));
                double productTotalPrice = 0;
                                    /*String totalPrice = productObj.optString("total_price");
                                    double productTotalPrice = TextUtils.isEmpty(totalPrice) ? 0 : Double.parseDouble(totalPrice);
                                    model.setTotalPrice(productTotalPrice);*/

                JSONArray variantsArray = productObj.optJSONArray("variants");
                ArrayList<SKUModel> skuModelArrayList = new ArrayList<>();
                if (variantsArray != null && variantsArray.length() > 0) {
                    for (int j = 0; j < variantsArray.length(); j++) {
                        JSONObject variantObj = variantsArray.optJSONObject(j);
                        SKUModel skuModel = new SKUModel();
                        skuModel.setPosition(i);
                        skuModel.setSkuId(variantObj.optString("variant_id"));
                        skuModel.setProductVariantId(variantObj.optString("product_variant_id"));
                        skuModel.setSkuName(variantObj.optString("name"));
                        skuModel.setSkuImage(variantObj.optString("Image"));
                        skuModel.setSku(variantObj.optString("sku"));
                        skuModel.setMrp(variantObj.optString("mrp"));
                        skuModel.setProductName(variantObj.optString("product_name").trim());
                        skuModel.setDefault(variantObj.optString("is_default").equals("1"));
                        skuModel.setModel(variantObj.optString("model"));
                        skuModel.setAvailableQuantity(variantObj.optString("quantity"));
                        skuModel.setSelected(false);
                        String appliedMrp = variantObj.optString("applied_mrp") == "null" ? "0" : variantObj.optString("applied_mrp");
                        String appliedMargin = variantObj.optString("applied_margin") == "null" ? "0" : variantObj.optString("applied_margin");
                        skuModel.setAppliedMargin(appliedMargin);
                        skuModel.setAppliedMRP(appliedMrp);
                        String qty = variantObj.optString("Total_quantity");
                        String totalPrice = variantObj.optString("Total_Price");
                        skuModel.setTotalPrice(totalPrice);
                        productTotalPrice = TextUtils.isEmpty(totalPrice) ? 0 : Double.parseDouble(totalPrice);
                        model.setTotalPrice(productTotalPrice);
                        skuModel.setTotalQuantity(qty);
                        try {
                            productTotalQty = productTotalQty + Integer.parseInt(qty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        JSONArray packsArray = variantObj.optJSONArray("pack");
                        ArrayList<SKUPacksModel> skuPacksModelArrayList = new ArrayList<>();
                        if (packsArray != null && packsArray.length() > 0) {
                            for (int k = 0; k < packsArray.length(); k++) {
                                JSONObject packObj = packsArray.optJSONObject(k);
                                SKUPacksModel packsModel = new SKUPacksModel();

                                packsModel.setPackSize(packObj.optString("pack_size") == "null" ? "0" : packObj.optString("pack_size"));
                                packsModel.setPackPrice(packObj.optString("dealer_price") == "null" ? "0" : packObj.optString("dealer_price"));
                                packsModel.setUnitPrice(packObj.optString("unit_price") == "null" ? "0" : packObj.optString("unit_price"));
                                packsModel.setMargin(packObj.optString("margin") == "null" ? "0" : packObj.optString("margin"));
                                packsModel.setVariantPriceId(packObj.optString("pack_price_id"));
                                // value selected by user
                                try {
                                    packsModel.setQty(Integer.parseInt(packObj.optString("qty")));
                                    skuPacksModelArrayList.add(packsModel);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    packsModel.setQty(0);
                                    skuPacksModelArrayList.add(packsModel);
                                }
                            }
                        }
                        skuModel.setSkuPacksModelArrayList(skuPacksModelArrayList);
                        skuModelArrayList.add(skuModel);
                    }
                }
                model.setTotalItems(productTotalQty);
                totalCartPrice += productTotalPrice;
                totalItems += productTotalQty;

                model.setSkuModelArrayList(skuModelArrayList);
                model.setProductName(productObj.optString("name"));
                arrCart.add(model);

                productSKUListModel.setTotalCartPrice(totalCartPrice);
                productSKUListModel.setTotatItemsCount(totalItems);

                productSKUListModel.setArrProducts(arrCart);


            }
        }
        return productSKUListModel;
    }

    public MasterLookUpModel getMasterLookUp(JSONObject jsonObject, Context context) {
        MasterLookUpModel model = new MasterLookUpModel();
        ArrayList<BusinessTypeModel> businessTyepArr = new ArrayList<>();
        ArrayList<CustomerTyepModel> arrCustomers = new ArrayList<>();
        ArrayList<CustomerTyepModel> arrVolumes = new ArrayList<>();
        ArrayList<CustomerTyepModel> arrLicense = new ArrayList<>();
        ArrayList<CustomerTyepModel> arrManf = new ArrayList<>();
        ArrayList<CustomerTyepModel> arrPrefSlots = new ArrayList<>();
        ArrayList<CustomerTyepModel> arrCustomerFeedBack = new ArrayList<>();

        try {
            JSONObject dataObject = jsonObject.optJSONObject("data");
            int cpSync = dataObject.optInt("cp_sync");
            int otpPopup = dataObject.optInt("otp_popup");
            double checkInDistance = dataObject.optDouble("checkin_distance");
            mSharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);

            mSharedPreferences.edit().putInt(ConstantValues.KEY_SYNC_INTERVAL, cpSync).apply();
            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_OTP_POPUP, otpPopup == 1 ? true : false).apply();
            mSharedPreferences.edit().putFloat(ConstantValues.KEY_CHECKIN_DISTANCE, (float) checkInDistance).apply();

            if (dataObject != null && dataObject.length() > 0) {

                DBHelper.getInstance().deleteTable(DBHelper.TABLE_CUSTOMER_TYPE);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_BUSINESS_TYPE);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_VOLUME_CLASS);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_LICENSE);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_MASTER_MANF);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_FF_COMMENTS);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_PREF_SLOTS);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_CUSTOMER_FEEDBACK);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_STAR);
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_DISCOUNT);

                JSONArray segmentsArray = dataObject.optJSONArray("segments");

                if (segmentsArray != null && segmentsArray.length() > 0) {
                    BusinessTypeModel businessTypeModel1 = new BusinessTypeModel();
                    businessTypeModel1.setSegmentId("");
                    businessTypeModel1.setSegmentName("Select Segment");
                    DBHelper.getInstance().insertSegment(businessTypeModel1);
                    businessTyepArr.add(0, businessTypeModel1);
                    for (int i = 0; i < segmentsArray.length(); i++) {

                        JSONObject segmentTypeObj = segmentsArray.optJSONObject(i);
                        if (segmentTypeObj != null) {
                            String segmentName = segmentTypeObj.optString("segment_name");
                            String segmentId = segmentTypeObj.optString("segment_id");
                            BusinessTypeModel businessTypeModel = new BusinessTypeModel();
                            businessTypeModel.setSegmentId(segmentId);
                            businessTypeModel.setSegmentName(segmentName);

                            DBHelper.getInstance().insertSegment(businessTypeModel);

                            businessTyepArr.add(businessTypeModel);
                        }
                    }

                    model.setBusinessTypeArr(businessTyepArr);
                }

                JSONArray buyerTypesArray = dataObject.optJSONArray("buyer_type");
                if (buyerTypesArray != null && buyerTypesArray.length() > 0) {
                    CustomerTyepModel customerTyepModel1 = new CustomerTyepModel();
                    customerTyepModel1.setCustomerGrpId("");
                    customerTyepModel1.setCustomerName("Select Customer Type");
                    DBHelper.getInstance().insertManf(customerTyepModel1, DBHelper.TABLE_CUSTOMER_TYPE);
                    arrCustomers.add(customerTyepModel1);
                    for (int i = 0; i < buyerTypesArray.length(); i++) {
                        JSONObject buyerTypeObj = buyerTypesArray.getJSONObject(i);
                        if (buyerTypeObj != null) {
                            String customerName = buyerTypeObj.getString("name");
                            String customerGrpId = buyerTypeObj.getString("value");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(customerName);
                            customerTyepModel.setCustomerGrpId(customerGrpId);

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_CUSTOMER_TYPE);

                            arrCustomers.add(customerTyepModel);

                        }
                    }
                    model.setArrCustomers(arrCustomers);
                }

                JSONArray volumesArray = dataObject.optJSONArray("volume_class");
                if (volumesArray != null && volumesArray.length() > 0) {

                    CustomerTyepModel _customerTyepModel = new CustomerTyepModel();
                    _customerTyepModel.setCustomerGrpId("");
                    _customerTyepModel.setCustomerName("Select Volume Class");
                    DBHelper.getInstance().insertManf(_customerTyepModel, DBHelper.TABLE_VOLUME_CLASS);
                    arrVolumes.add(_customerTyepModel);
                    for (int i = 0; i < volumesArray.length(); i++) {
                        JSONObject volumeObj = volumesArray.getJSONObject(i);
                        if (volumeObj != null) {
                            String customerName = volumeObj.getString("name");
                            String customerGrpId = volumeObj.getString("value");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(customerName);
                            customerTyepModel.setCustomerGrpId(customerGrpId);

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_VOLUME_CLASS);

                            arrVolumes.add(customerTyepModel);

                        }
                    }
                    model.setArrVolumes(arrVolumes);
                }

                JSONArray licenseArray = dataObject.optJSONArray("license");
                if (licenseArray != null && licenseArray.length() > 0) {
                    CustomerTyepModel _customerTyepModel = new CustomerTyepModel();
                    _customerTyepModel.setCustomerGrpId("");
                    _customerTyepModel.setCustomerName("Select License Type");
                    DBHelper.getInstance().insertManf(_customerTyepModel, DBHelper.TABLE_LICENSE);

                    arrLicense.add(_customerTyepModel);

                    for (int i = 0; i < licenseArray.length(); i++) {
                        JSONObject licenseObj = licenseArray.getJSONObject(i);
                        if (licenseObj != null) {
                            String customerName = licenseObj.getString("name");
                            String customerGrpId = licenseObj.getString("value");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(customerName);
                            customerTyepModel.setCustomerGrpId(customerGrpId);

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_LICENSE);

                            arrLicense.add(customerTyepModel);
                        }
                    }
                    model.setArrLicense(arrLicense);
                }

                JSONArray manfArray = dataObject.optJSONArray("master_manf");
                if (manfArray != null && manfArray.length() > 0) {

                    for (int i = 0; i < manfArray.length(); i++) {
                        JSONObject manfObj = manfArray.getJSONObject(i);
                        if (manfObj != null) {
                            String customerName = manfObj.getString("name");
                            String customerGrpId = manfObj.getString("value");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(customerName);
                            customerTyepModel.setCustomerGrpId(customerGrpId);

                            if (i == 0) {
                                CustomerTyepModel _customerTyepModel = new CustomerTyepModel();
                                _customerTyepModel.setCustomerName("All");
                                _customerTyepModel.setCustomerGrpId("");

                                arrManf.add(0, _customerTyepModel);
                                DBHelper.getInstance().insertManf(_customerTyepModel, DBHelper.TABLE_MASTER_MANF);
                            }

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_MASTER_MANF);

                            arrManf.add(customerTyepModel);

                        }
                    }


                    model.setArrManf(arrManf);
                }

                JSONArray ffCommentsArray = dataObject.optJSONArray("ff_comments");
                if (ffCommentsArray != null && ffCommentsArray.length() > 0) {
                    for (int i = 0; i < ffCommentsArray.length(); i++) {
                        JSONObject object = ffCommentsArray.getJSONObject(i);
                        if (object != null) {
                            String customerName = object.getString("name");
                            String customerGrpId = object.getString("value");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(customerName);
                            customerTyepModel.setCustomerGrpId(customerGrpId);

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_FF_COMMENTS);
                        }
                    }
                }

                JSONArray deliverySlotsArray = dataObject.optJSONArray("delivery_slots");
                if (deliverySlotsArray != null && deliverySlotsArray.length() > 0) {
                    for (int i = 0; i < deliverySlotsArray.length(); i++) {
                        JSONObject object = deliverySlotsArray.getJSONObject(i);
                        if (object != null) {
                            String name = object.getString("name");
                            String value = object.getString("value");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(name);
                            customerTyepModel.setCustomerGrpId(value);

                            if (i == 0) {
                                CustomerTyepModel _customerTyepModel = new CustomerTyepModel();
                                _customerTyepModel.setCustomerName("Select Preferred Slot");
                                _customerTyepModel.setCustomerGrpId("");

                                arrPrefSlots.add(0, _customerTyepModel);
                                DBHelper.getInstance().insertManf(_customerTyepModel, DBHelper.TABLE_PREF_SLOTS);
                            }

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_PREF_SLOTS);
                            arrPrefSlots.add(customerTyepModel);

                        }
                    }
                }

                JSONArray customerFeedbackArray = dataObject.optJSONArray("customer_feedback");
                if (customerFeedbackArray != null && customerFeedbackArray.length() > 0) {
                    for (int i = 0; i < customerFeedbackArray.length(); i++) {
                        JSONObject object = customerFeedbackArray.optJSONObject(i);
                        if (object != null) {
                            String name = object.optString("name");
                            String value = object.optString("value");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(name);
                            customerTyepModel.setCustomerGrpId(value);

                            if (i == 0) {
                                CustomerTyepModel _customerTyepModel = new CustomerTyepModel();
                                _customerTyepModel.setCustomerName("Select Customer Feedback");
                                _customerTyepModel.setCustomerGrpId("");

                                arrPrefSlots.add(0, _customerTyepModel);
                                DBHelper.getInstance().insertManf(_customerTyepModel, DBHelper.TABLE_CUSTOMER_FEEDBACK);
                            }

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_CUSTOMER_FEEDBACK);
                            arrCustomerFeedBack.add(customerTyepModel);

                        }
                    }
                }

                JSONArray starArray = dataObject.optJSONArray("stars");
                if (starArray != null && starArray.length() > 0) {
                    for (int i = 0; i < starArray.length(); i++) {
                        JSONObject starObj = starArray.optJSONObject(i);
                        if (starObj != null) {
                            String name = starObj.optString("name");
                            String value = starObj.optString("value");
                            String description = starObj.optString("description");

                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerName(name);
                            customerTyepModel.setCustomerGrpId(value);
                            customerTyepModel.setDescription(description);

                            DBHelper.getInstance().insertManf(customerTyepModel, DBHelper.TABLE_STAR);
                        }
                    }
                }

                JSONArray discountsArray = dataObject.optJSONArray("discounts");
                if (discountsArray != null && discountsArray.length() > 0) {
                    for (int i = 0; i < discountsArray.length(); i++) {
                        JSONObject discountObj = discountsArray.optJSONObject(i);
                        if (discountObj != null) {
                            String discountType = discountObj.optString("discount_type");
                            String discountOn = discountObj.optString("discount_on");
                            String discountOnValues = discountObj.optString("discount_on_values");
                            String discount = discountObj.optString("discount");

                            ArrayList<String> arrDiscountValues = new ArrayList<>();

                            if (!TextUtils.isEmpty(discountOnValues))
                                arrDiscountValues = new ArrayList<String>(Arrays.asList(discountOnValues.split("\\s*,\\s*")));

                            if (arrDiscountValues != null && arrDiscountValues.size() > 0) {
                                for (int j = 0; j < arrDiscountValues.size(); j++) {
                                    DiscountModel discountModel = new DiscountModel();
                                    discountModel.setDiscountType(discountType);
                                    discountModel.setDiscountOn(discountOn);
                                    discountModel.setDiscountValues(arrDiscountValues.get(j));
                                    discountModel.setDiscount(discount);

                                    DBHelper.getInstance().insertDiscount(discountModel);

                                }
                            }

                        }
                    }
                }

                JSONArray gstArray = dataObject.optJSONArray("gst_state_codes");
                if (gstArray != null && gstArray.length() > 0) {
                    for (int i = 0; i < gstArray.length(); i++) {
                        mSharedPreferences.edit().putString(ConstantValues.GST_CODES, gstArray.getString(i)).apply();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;
    }

    public ArrayList<CategoryModel> getCategories(JSONObject jsonObject) {
        ArrayList<CategoryModel> arrCategories = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.getJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                DBHelper.getInstance().deleteTable(DBHelper.TABLE_CATEGORY);
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject categoryObj = dataArray.getJSONObject(i);
                    String categoryId = categoryObj.optString("id");
                    String segmentId = categoryObj.optString("segment_id");
                    String parentId = categoryObj.optString("parent_cat_id");
                    String categoryImage = categoryObj.optString("image");
                    String categoryName = categoryObj.optString("name");
                    String productNo = categoryObj.optString("productNo");

                    CategoryModel model = new CategoryModel();
                    model.setCategoryId(categoryId);
                    model.setCategoryImage(categoryImage);
                    model.setSegmentId(segmentId);
                    model.setParentId(parentId);
                    model.setCategoryName(categoryName);
                    model.setProductNo(productNo);

                    DBHelper.getInstance().insertCategory(model);

                    arrCategories.add(model);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrCategories;
    }

    public AppDataModel appUpdate(JSONObject jsonObject) {
        AppDataModel model = new AppDataModel();
        try {

            String status = jsonObject.optString("status");
            String versionUpdateStatus = jsonObject.optString("versionUpdateStatus");
            String versionNumber = jsonObject.optString("version_number");

            model.setVersionNumber(versionNumber);
            model.setVersionUpdateStatus(versionUpdateStatus);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    public SubCategoryModel getSubCategories(JSONObject jsonObject) {
        SubCategoryModel model = new SubCategoryModel();
        ArrayList<CategoryModel> arrSubCategories;
        DBHelper databaseObj = DBHelper.getInstance();
        try {
            JSONObject dataObject = jsonObject.getJSONObject("data");
            String isSubcategory = dataObject.getString("is_subcategory");
            if (isSubcategory.equalsIgnoreCase("1")) {
                arrSubCategories = new ArrayList<>();

                JSONArray dataArray = dataObject.getJSONArray("data");
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject categoryObject = dataArray.getJSONObject(i);
                    CategoryModel categoryModel = new CategoryModel();
                    categoryModel.setCategoryId(categoryObject.getString("category_id"));
                    categoryModel.setCategoryName(categoryObject.getString("name"));
                    categoryModel.setParentId(categoryObject.getString("parent_id"));
                    categoryModel.setCategoryImage(databaseObj.getCategoryImage(categoryObject.getString("category_id")));
                    arrSubCategories = new ArrayList<>();
                    arrSubCategories.add(categoryModel);
                }
                model.setArrSubCategories(arrSubCategories);
                model.setIsSubCategory(isSubcategory);
            } else if (isSubcategory.equalsIgnoreCase("0")) {
                model.setIsSubCategory(isSubcategory);

            } else {
                model.setIsSubCategory(isSubcategory);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;
    }

    public ArrayList<ProductFilterModel> getFilterDataNew(String filterData, boolean displayStar) {
        ArrayList<ProductFilterModel> arrProductFilters = new ArrayList<>();
        try {
            if (filterData != null && filterData.length() > 0) {
                JSONObject filterObj = new JSONObject(filterData);
                if (filterObj.length() > 0) {
                    ProductFilterModel productFilterModel = new ProductFilterModel();

//                    ArrayList<ManufacturerModel> manufacturerModelList = new ArrayList<>();
//                    ArrayList<BrandModel> brandModelList = new ArrayList<>();
//                    ArrayList<CategoryModel> categoryModelList = new ArrayList<>();

                    ArrayList<FilterModel> arrFilters = new ArrayList<>();

                    JSONArray manufacturesArray = filterObj.optJSONArray("manufacturers");
                    productFilterModel.setAttributeId("");
                    productFilterModel.setGroupName("Manufacturers");
                    productFilterModel.setFilterType("Manufacturers");
                    for (int i = 0; i < manufacturesArray.length(); i++) {
                        JSONObject obj = manufacturesArray.optJSONObject(i);
                        FilterModel model = new FilterModel();
                        model.setManufacturerId(obj.optString("manufacturer_id"));
                        model.setFilterName(obj.optString("manufacturer_name"));

                        arrFilters.add(model);
                    }
                    productFilterModel.setArrFilters(arrFilters);
                    arrProductFilters.add(productFilterModel);

                    JSONArray brandArray = filterObj.optJSONArray("brand");
                    arrFilters = new ArrayList<>();
                    productFilterModel = new ProductFilterModel();
                    productFilterModel.setAttributeId("");
                    productFilterModel.setGroupName("Brands");
                    productFilterModel.setFilterType("Brands");
                    for (int i = 0; i < brandArray.length(); i++) {
                        JSONObject obj = brandArray.optJSONObject(i);
                        FilterModel model = new FilterModel();
                        model.setManufacturerId(obj.optString("manufacturer_id"));
                        model.setFilterName(obj.optString("brand_name"));
                        model.setBrandId(obj.optString("brand_id"));
                        JSONArray catArr = obj.optJSONArray("category_id");
                        if (catArr != null && catArr.length() > 0) {
                            ArrayList<String> arrCat = new ArrayList<>();
                            for (int j = 0; j < catArr.length(); j++) {
                                arrCat.add(catArr.optString(j));
                            }
                            model.setCategoryIds(arrCat);
                        }
                        arrFilters.add(model);
                    }
                    productFilterModel.setArrFilters(arrFilters);
                    arrProductFilters.add(productFilterModel);

                    JSONArray categoryArray = filterObj.optJSONArray("categories");
                    arrFilters = new ArrayList<>();
                    productFilterModel = new ProductFilterModel();
                    productFilterModel.setAttributeId("");
                    productFilterModel.setGroupName("Categories");
                    productFilterModel.setFilterType("Categories");
                    for (int i = 0; i < categoryArray.length(); i++) {
                        JSONObject obj = categoryArray.optJSONObject(i);
                        FilterModel model = new FilterModel();
                        model.setFilterName(obj.optString("category_name"));
                        model.setCategoryId(obj.optString("category_id"));

                        JSONArray brandArr = obj.optJSONArray("brand_id");
                        if (brandArr != null && brandArr.length() > 0) {
                            ArrayList<String> arrBrand = new ArrayList<>();
                            for (int j = 0; j < brandArr.length(); j++) {
                                arrBrand.add(brandArr.optString(j));
                            }
                            model.setBrandIds(arrBrand);
                        }
                        JSONArray manfArr = obj.optJSONArray("manf_id");
                        if (manfArr != null && manfArr.length() > 0) {
                            ArrayList<String> arrManf = new ArrayList<>();
                            for (int j = 0; j < manfArr.length(); j++) {
                                arrManf.add(manfArr.optString(j));
                            }
                            model.setManfIds(arrManf);
                        }
                        arrFilters.add(model);

                    }
                    productFilterModel.setArrFilters(arrFilters);
                    arrProductFilters.add(productFilterModel);

                    JSONArray otherFiltersArray = filterObj.optJSONArray("otherfilters");
                    if (otherFiltersArray != null) {
                        for (int i = 0; i < otherFiltersArray.length(); i++) {
                            productFilterModel = new ProductFilterModel();
                            JSONObject filterObject = otherFiltersArray.getJSONObject(i);
                            String attributeId = filterObject.optString("attribute_id");
                            String name = filterObject.optString("name");
                            String type = filterObject.optString("type");

                            productFilterModel.setAttributeId(attributeId);
                            productFilterModel.setGroupName(name);
                            productFilterModel.setFilterType(type);

                            if (type.equalsIgnoreCase("range")) {
                                JSONArray optionArray = filterObject.optJSONArray("option");
                                if (optionArray != null && optionArray.length() > 0) {

                                    for (int j = 0; j < optionArray.length(); j++) {
                                        JSONObject optionObj = optionArray.getJSONObject(j);
                                        String min = optionObj.optString("minvalue");
                                        String max = optionObj.optString("maxvalue");
                                        float minVal, maxVal;
                                        try {
                                            minVal = Float.parseFloat(min);
                                            maxVal = Float.parseFloat(max);
                                            min = String.format("%.2f", minVal);
                                            max = String.format("%.2f", maxVal);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        productFilterModel.setMaxValue(max);
                                        productFilterModel.setMinValue(min);
                                        productFilterModel.setSelMin(min);
                                        productFilterModel.setSelMax(max);
                                        if (!min.equalsIgnoreCase(max)) {
                                            arrProductFilters.add(productFilterModel);
                                        }
                                    }
                                }

                            } else {
                                JSONArray optionArray = filterObject.optJSONArray("option");
                                if (optionArray != null && optionArray.length() > 0) {
                                    ArrayList<FilterModel> filterModelArrayList = new ArrayList<>();

                                    for (int j = 0; j < optionArray.length(); j++) {
                                        FilterModel model = new FilterModel();
                                        JSONObject optionObj = optionArray.getJSONObject(j);
                                        String filterName = optionObj.optString("filtername");
                                        model.setFilterName(filterName);
                                        if (name.equalsIgnoreCase("star"))
                                            model.setStar(true);
                                        else
                                            model.setStar(false);
                                        filterModelArrayList.add(model);
                                    }
                                    productFilterModel.setArrFilters(filterModelArrayList);
                                }
                                if (name.equalsIgnoreCase("star") && displayStar)
                                    arrProductFilters.add(productFilterModel);
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrProductFilters;
    }

    public ArrayList<ProductFilterModel> getFilterData(String filterData) {
        ArrayList<ProductFilterModel> arrProductFilters = new ArrayList<>();
        try {
            if (filterData != null && filterData.length() > 0) {
                JSONArray dataArray = new JSONArray(filterData);
                if (dataArray != null) {
                    for (int i = 0; i < dataArray.length(); i++) {
                        ProductFilterModel productFilterModel = new ProductFilterModel();
                        JSONObject filterObj = dataArray.getJSONObject(i);
                        String attributeId = filterObj.optString("attribute_id");
                        String name = filterObj.optString("name");
                        String type = filterObj.optString("type");

                        productFilterModel.setAttributeId(attributeId);
                        productFilterModel.setGroupName(name);
                        productFilterModel.setFilterType(type);

                        if (type.equalsIgnoreCase("range")) {
                            JSONArray optionArray = filterObj.optJSONArray("option");
                            if (optionArray != null && optionArray.length() > 0) {

                                for (int j = 0; j < optionArray.length(); j++) {
                                    JSONObject optionObj = optionArray.getJSONObject(j);
                                    String min = optionObj.optString("minvalue");
                                    String max = optionObj.optString("maxvalue");
                                    productFilterModel.setMaxValue(max);
                                    productFilterModel.setMinValue(min);
                                    productFilterModel.setSelMin(min);
                                    productFilterModel.setSelMax(max);
                                    if (!min.equalsIgnoreCase(max)) {
                                        arrProductFilters.add(productFilterModel);
                                    }
                                }
                            }

                        } else {
                            JSONArray optionArray = filterObj.optJSONArray("option");
                            if (optionArray != null && optionArray.length() > 0) {
                                ArrayList<FilterModel> filterModelArrayList = new ArrayList<>();

                                for (int j = 0; j < optionArray.length(); j++) {
                                    FilterModel model = new FilterModel();
                                    JSONObject optionObj = optionArray.getJSONObject(j);
                                    String filterName = optionObj.optString("filtername");
                                    model.setFilterName(filterName);
                                    filterModelArrayList.add(model);
                                }
                                productFilterModel.setArrFilters(filterModelArrayList);
                            }
                            arrProductFilters.add(productFilterModel);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrProductFilters;
    }

    public CustomerTyepModel confirmOtp(JSONObject jsonObject) {
        CustomerTyepModel model = new CustomerTyepModel();

        try {
            JSONObject dataObject = jsonObject.optJSONObject("data");
            if (dataObject != null) {

                boolean isLogin = dataObject.optInt("approved") == 1;


                model.setIsLogin(isLogin);
                if (isLogin) {
                    JSONObject detailsObj = dataObject.optJSONObject("data");

                    String is_active = detailsObj.optString("is_active");
                    boolean isActive = is_active.equalsIgnoreCase("1");
                    model.setActive(isActive);

                    String customerGroupId = detailsObj.optString("customer_group_id");
                    String customerToken = detailsObj.optString("customer_token");
                    String pincode = detailsObj.optString("pincode");
                    String whId = detailsObj.optString("le_wh_id");
                    String hub = detailsObj.optString("hub");
                    String customerId = detailsObj.optString("customer_id");
                    String firstName = detailsObj.optString("firstname");
                    String image = /*ConstantValues.NEW_BASE_IMAGE_AUTH + */detailsObj.optString("image");
                    String segmentId = detailsObj.optString("segment_id");
                    int isFf = detailsObj.optInt("is_ff");
                    int isSrm = detailsObj.optInt("is_srm");
                    String legalEntityId = detailsObj.optString("legal_entity_id");
                    int isDashboard = detailsObj.optInt("is_dashboard");
                    int hasChild = detailsObj.optInt("has_child");
                    String beatId = detailsObj.optString("beat_id");
                    String latitude = detailsObj.optString("latitude");
                    String longitude = detailsObj.optString("longitude");

                    JSONObject object = detailsObj.optJSONObject("ecash_details");

                    if (object != null) {
                        double creditLimit = object.optDouble("creditlimit");
                        double eCash = object.optDouble("ecash");
                        double payementDue = object.optDouble("payment_due");

                        model.setCreditLimit(creditLimit);
                        model.setEcash(eCash);
                        model.setPaymentDue(payementDue);
                    }

                    JSONObject ffeCashObj = detailsObj.optJSONObject("ff_ecash_details");

                    if (ffeCashObj != null) {
                        double creditLimit = ffeCashObj.optDouble("creditlimit");
                        double eCash = ffeCashObj.optDouble("ecash");
                        double payementDue = ffeCashObj.optDouble("payment_due");

                        model.setFfCreditLimit(creditLimit);
                        model.setFfecash(eCash);
                        model.setFfPaymentDue(payementDue);
                    }

                    JSONArray featuresArray = detailsObj.optJSONArray("mobile_feature");
                    if (featuresArray != null && featuresArray.length() > 0) {
                        for (int i = 0; i < featuresArray.length(); i++) {
                            JSONObject feature = featuresArray.optJSONObject(i);
                            String featureCode = feature.optString("feature_code");
                            String name = feature.optString("name");
                            String isMenu = feature.optString("is_menu");
                            String parentId = feature.optString("parent_id");
                            String featureId = feature.optString("feature_id");

                            FeatureData data = new FeatureData(featureCode, name, parentId, isMenu, featureId);
                            DBHelper.getInstance().insertFeature(data);
                        }
                    }

                    //boolean isFF = true;
                    model.setCustomerGrpId(customerGroupId);
                    model.setCustomerToken(customerToken);
                    model.setCustomerId(customerId);
                    model.setFirstName(firstName);
                    model.setProfilePic(image);
                    model.setSegmentId(segmentId);
                    model.setPostcode(pincode);
                    model.setWhId(whId);
                    model.setHub(hub);
                    model.setIsFF(isFf);
                    model.setIsSRM(isSrm);
                    model.setLegalEntityId(legalEntityId);
                    model.setHasChild(hasChild);
                    model.setIsDashboard(isDashboard);
                    model.setBeatId(beatId);
                    model.setLatitude(latitude);
                    model.setLongitude(longitude);
                } else {
                    model.setActive(true);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    public CustomerTyepModel getProfile(JSONObject jsonObject) {
        CustomerTyepModel model = new CustomerTyepModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            String firstname = dataObj.optString("firstname");
            String lastname = dataObj.optString("lastname");
            String documents = /*ConstantValues.NEW_BASE_IMAGE_AUTH +*/ dataObj.optString("documents");
            String company = dataObj.optString("company");
            String address_1 = dataObj.optString("address_1");
            String address_2 = dataObj.optString("address_2");
            String locality = dataObj.optString("locality");
            String landmark = dataObj.optString("landmark");
            String city = dataObj.optString("city");
            String country = dataObj.optString("name");
            String postcode = dataObj.optString("postcode");
            String telephone = dataObj.optString("telephone");
            String email = dataObj.optString("email").contains("@nomail.com") ? "" : dataObj.optString("email");
            String addressId = dataObj.optString("address_id");
            String businessType = dataObj.optString("business_type");
            String buyerType = dataObj.optString("buyer_type");
            String manufacturers = dataObj.optString("manufacturers");
            String smartphone = dataObj.optString("smartphone");
            String internetAvailability = dataObj.optString("internet_availability");
            String noOfShutters = dataObj.optString("No_of_shutters");
            String areaId = dataObj.optString("area_id");
            String area = dataObj.optString("area");
            String state = dataObj.optString("state");
            String volumeClass = dataObj.optString("volume_class");
            String deliveryTime = dataObj.optString("delivery_time");
            String businessStartTime = dataObj.optString("business_start_time");
            String businessEndTime = dataObj.optString("business_end_time");
            String contactName1 = dataObj.optString("contact_name1");
            String contactNo1 = dataObj.optString("contact_no1");
            String userId1 = dataObj.optString("user_id1");
            String contactName2 = dataObj.optString("contact_name2");
            String contactNo2 = dataObj.optString("contact_no2");
            String userId2 = dataObj.optString("user_id2");
            String isParent = dataObj.optString("is_parent");
            String prefValue1 = dataObj.optString("delivery_time");
            String prefValue2 = dataObj.optString("pref_value2");
            String beatId = dataObj.optString("beat_id");
            String gst = dataObj.optString("gstin").equalsIgnoreCase("null") ? "" : dataObj.optString("gstin");
            String arn = dataObj.optString("arn_number").equalsIgnoreCase("null") ? "" : dataObj.optString("arn_number");
            boolean isPremium = dataObj.optInt("is_premium") == 1 ? true : false;

            model.setFirstName(firstname);
            model.setLastName(lastname);
            model.setDocuments(documents);
            model.setCompany(company);
            model.setAddress1(address_1);
            model.setAddress2(address_2);
            model.setLocality(locality);
            model.setLandmark(landmark);
            model.setCity(city);
            model.setCountry(country);
            model.setPostcode(postcode);
            model.setTelephone(telephone);
            model.setEmail(email);
            model.setAddressId(addressId);
            model.setBuyerType(buyerType);
            model.setBusinessType(businessType);
            model.setNoOfShutters(noOfShutters);
            model.setAreaId(areaId);
            model.setArea(area);
            model.setState(state);
            model.setVolumeClass(volumeClass);
            model.setManfIds(manufacturers);
            model.setBusinessStartTime(businessStartTime);
            model.setBusinessEndTime(businessEndTime);
            model.setDeliveryTime(deliveryTime);
            model.setContactName1(contactName1);
            model.setContactNo1(contactNo1);
            model.setUserId1(userId1);
            model.setContactName2(contactName2);
            model.setContactNo2(contactNo2);
            model.setUserId2(userId2);
            model.setIsSmartPhone(smartphone.equalsIgnoreCase("1"));
            model.setIsInternet(internetAvailability.equalsIgnoreCase("1"));
            model.setIsParent(isParent.equalsIgnoreCase("1"));
            model.setPrefSlot1(prefValue1);
            model.setPrefSlot2(prefValue2);
            model.setBeatId(beatId);
            model.setGst(gst);
            model.setArn(arn);
            model.setPremium(isPremium);

        } catch (Exception e) {

        }
        return model;
    }

    public CustomerTyepModel editProfile(JSONObject jsonObject) {
        CustomerTyepModel model = new CustomerTyepModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            String company = dataObj.optString("company");
            String address_1 = dataObj.optString("address_1");
            String address_2 = dataObj.optString("address_2");
            String locality = dataObj.optString("locality");
            String landmark = dataObj.optString("landmark");
            String city = dataObj.optString("city");
            String country = dataObj.optString("name");
            String postcode = dataObj.optString("postcode");
            String email = dataObj.optString("email").contains("@nomail.com") ? "" : dataObj.optString("email");
            String addressId = dataObj.optString("address_id");
            String businessType = dataObj.optString("business_type");
            String buyerType = dataObj.optString("buyer_type");
            String manufacturers = dataObj.optString("manufacturers");
            String smartphone = dataObj.optString("smartphone");
            String internetAvailability = dataObj.optString("internet_availability");
            String noOfShutters = dataObj.optString("No_of_shutters");
            String areaId = dataObj.optString("area_id");
            String area = dataObj.optString("area");
            String state = dataObj.optString("state");
            String volumeClass = dataObj.optString("volume_class");
            String businessStartTime = dataObj.optString("business_start_time");
            String businessEndTime = dataObj.optString("business_end_time");
            String deliveryTime = dataObj.optString("delivery_time");
            String contactName1 = dataObj.optString("contact_name1");
            String contactNo1 = dataObj.optString("contact_no1");
            String userId1 = dataObj.optString("user_id1");
            String contactName2 = dataObj.optString("contact_name2");
            String contactNo2 = dataObj.optString("contact_no2");
            String userId2 = dataObj.optString("user_id2");
            String prefValue1 = dataObj.optString("delivery_time");
            String prefValue2 = dataObj.optString("pref_value2");
            String beatId = dataObj.optString("beat_id");
            String gst = dataObj.optString("gstin").equalsIgnoreCase("null") ? "" : dataObj.optString("gstin");
            String arn = dataObj.optString("arn_number").equalsIgnoreCase("null") ? "" : dataObj.optString("arn_number");

            model.setCompany(company);
            model.setAddress1(address_1);
            model.setAddress2(address_2);
            model.setLocality(locality);
            model.setLandmark(landmark);
            model.setCity(city);
            model.setCountry(country);
            model.setPostcode(postcode);
            model.setEmail(email);
            model.setAddressId(addressId);
            model.setBuyerType(buyerType);
            model.setBusinessType(businessType);
            model.setNoOfShutters(noOfShutters);
            model.setAreaId(areaId);
            model.setArea(area);
            model.setState(state);
            model.setVolumeClass(volumeClass);
            model.setManfIds(manufacturers);
            model.setBusinessStartTime(businessStartTime);
            model.setBusinessEndTime(businessEndTime);
            model.setDeliveryTime(deliveryTime);
            model.setContactName1(contactName1);
            model.setContactNo1(contactNo1);
            model.setUserId1(userId1);
            model.setContactName2(contactName2);
            model.setContactNo2(contactNo2);
            model.setUserId2(userId2);
            model.setIsSmartPhone(smartphone.equalsIgnoreCase("1"));
            model.setIsInternet(internetAvailability.equalsIgnoreCase("1"));
            model.setPrefSlot1(prefValue1);
            model.setPrefSlot2(prefValue2);
            model.setBeatId(beatId);
            model.setGst(gst);
            model.setArn(arn);

        } catch (Exception e) {

        }
        return model;
    }

    public CustomerTyepModel editName(JSONObject jsonObject) {
        CustomerTyepModel model = new CustomerTyepModel();
        JSONObject dataObj = jsonObject.optJSONObject("data");
        String firstname = dataObj.optString("firstname");
        String lastname = dataObj.optString("lastname");
        String documents = /*ConstantValues.NEW_BASE_IMAGE_AUTH + */dataObj.optString("documents");
        model.setFirstName(firstname);
        model.setLastName(lastname);
        model.setDocuments(documents);
        return model;
    }

    public String editEmail(JSONObject jsonObject) {
        JSONObject dataObj = jsonObject.optJSONObject("data");
        String email = dataObj.optString("email");
        return email;
    }

    public String editMobile(JSONObject jsonObject) {
        JSONObject dataObj = jsonObject.optJSONObject("data");
        String mobile = dataObj.optString("telephone");
        return mobile;
    }

    public CustomerTyepModel editAddress(JSONObject jsonObject) {
        CustomerTyepModel model = new CustomerTyepModel();

        JSONObject dataObj = jsonObject.optJSONObject("data");
        String address1 = dataObj.optString("address_1");
        String address2 = dataObj.optString("address_2");
        String city = dataObj.optString("city");
        String postcode = dataObj.optString("postcode");
        String whId = dataObj.optString("le_wh_id");

        model.setAddress1(address1);
        model.setAddress2(address2);
        model.setCity(city);
        model.setPostcode(postcode);
        model.setWhId(whId);

        return model;
    }

    public ArrayList<RetailersModel> getAllSuppliers(JSONObject jsonObject) {
        ArrayList<RetailersModel> arrSuppliers = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {

                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject obj = dataArray.optJSONObject(i);

                    String supplierId = obj.optString("supplier_id");
                    String latitude = obj.optString("latitude");
                    String longitude = obj.optString("longitude");
                    String supplierName = obj.optString("supplier_name");
                    String telephone = obj.optString("telephone");
                    String address1 = obj.optString("address1");
                    String address2 = obj.optString("address2");
                    String city = obj.optString("city");
                    String pincode = obj.optString("pincode");
                    String businessTypeId = obj.optString("business_type_id");
                    String customerToken = obj.optString("customer_token");

                    RetailersModel suppliersModel = new RetailersModel();
                    suppliersModel.setCustomerId(supplierId);
                    suppliersModel.setFirstName(supplierName);
                    suppliersModel.setTelephone(telephone);
                    suppliersModel.setAddress1(address1);
                    suppliersModel.setAddress2(address2);
                    suppliersModel.setCity(city);
                    suppliersModel.setPincode(pincode);
                    suppliersModel.setBusinessTypeId(businessTypeId);
                    suppliersModel.setLatitude(latitude);
                    suppliersModel.setLongitude(longitude);
                    suppliersModel.setCustomerToken(customerToken);

//                    DBHelper.getInstance().insertRetailer(retailersModel);

                    arrSuppliers.add(suppliersModel);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrSuppliers;
    }

    public ArrayList<RetailersModel> getAllRetailers(JSONObject jsonObject) {
        ArrayList<RetailersModel> arrRetailers = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {

                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject obj = dataArray.optJSONObject(i);

                    String customerId = obj.optString("customer_id");
                    String latitude = obj.optString("latitude");
                    String longitude = obj.optString("longitude");
                    String firstname = obj.optString("firstname");
                    String telephone = obj.optString("telephone");
                    String company = obj.optString("company");
                    String address1 = obj.optString("address_1");
                    String address2 = obj.optString("address2");
                    String legalEntityId = obj.optString("legal_entity_id");
                    String noOfShutters = obj.optString("No_of_shutters");
                    String volumeClass = obj.optString("volume_class");
                    String businessTypeId = obj.optString("business_type_id");
                    String masterManf = obj.optString("master_manf");
                    String buyerTypeId = obj.optString("buyer_type");
                    String popup = obj.optString("popup");
                    String lastVisit = obj.optString("check_in");
                    String lastOrder = obj.optString("last_order");
                    String customerToken = obj.optString("customer_token");
                    String beatId = obj.optString("beat_id");
                    String beatName = obj.optString("beatname");
                    int noOfOrders = obj.optInt("no_of_orders");
                    int returnOrders = obj.optInt("return_orders");
                    String totalBusiness = obj.optString("total_business");
                    String avgBillVal = obj.optString("avg_bill_val");
                    String rank = obj.optString("rank").equalsIgnoreCase("null") ? "0" : obj.optString("rank");

                    RetailersModel retailersModel = new RetailersModel();
                    retailersModel.setAddress1(address1);
                    retailersModel.setAddress2(address2);
                    retailersModel.setLegalEntityId(legalEntityId);
                    retailersModel.setCompany(company);
                    retailersModel.setCustomerId(customerId);
                    retailersModel.setFirstName(firstname);
                    retailersModel.setTelephone(telephone);
                    retailersModel.setNoOfShutters((noOfShutters == null || noOfShutters.equalsIgnoreCase("null")) ? "0" : noOfShutters);
//                    if(null!=volumeClass){// getting actual value from Local DB
//                        volumeClass = DBHelper.getInstance().checkTableForRecord(DBHelper.COL_CUSTOMER_NAME,
//                                DBHelper.TABLE_VOLUME_CLASS, DBHelper.COL_CUSTOMER_GRP_ID, volumeClass);
//                    }
                    retailersModel.setVolumeClass(volumeClass);
                    retailersModel.setBusinessTypeId(businessTypeId);
                    retailersModel.setMasterManfIds(masterManf);
                    retailersModel.setBuyerTypeId(buyerTypeId);
                    retailersModel.setPopup(popup.equalsIgnoreCase("1"));
                    retailersModel.setLatitude(latitude.equalsIgnoreCase("null") ? "0.00" : latitude);
                    retailersModel.setLongitude(longitude.equalsIgnoreCase("null") ? "0.00" : longitude);
                    retailersModel.setLastVisit(lastVisit.equalsIgnoreCase("null") ? "" : lastVisit);
                    retailersModel.setLastOrder(lastOrder.equalsIgnoreCase("null") ? "0" : lastOrder);
                    retailersModel.setCustomerToken(customerToken);
                    retailersModel.setBeatId(beatId);
                    retailersModel.setBeatName(beatName);
                    retailersModel.setNoOfOrders(noOfOrders);
                    retailersModel.setReturnOrders(returnOrders);
                    retailersModel.setTotalBusiness(totalBusiness);
                    retailersModel.setAvgBillValue(avgBillVal);
                    retailersModel.setRank(rank);

//                    DBHelper.getInstance().insertRetailer(retailersModel);

                    arrRetailers.add(retailersModel);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrRetailers;
    }

    public ArrayList<NewPacksModel> getSlabRates(JSONObject jsonObject) {
        ArrayList<NewPacksModel> arrSlabRates = new ArrayList<>();
        try {
            JSONArray dataArr = jsonObject.optJSONArray("data");
            if (dataArr != null && dataArr.length() > 0) {
                for (int i = 0; i < dataArr.length(); i++) {
                    JSONObject slabRateObj = dataArr.optJSONObject(i);
                    NewPacksModel newPacksModel = new NewPacksModel();

                    String packSize = slabRateObj.optString("pack_size").equalsIgnoreCase("null") ? "0" : slabRateObj.optString("pack_size");
                    String unitPrice = slabRateObj.optString("unit_price").equalsIgnoreCase("null") ? "0.0" : slabRateObj.optString("unit_price");
                    String productId = slabRateObj.optString("product_id");
                    String isMarkup = slabRateObj.optString("is_markup");
                    String margin = slabRateObj.optString("margin").equalsIgnoreCase("null") ? "0.0" : slabRateObj.optString("margin");
                    String freebieDesc = slabRateObj.optString("freebee_desc");
                    String freebiePrdId = slabRateObj.optString("freebee_prd_id");
                    String freebieMpq = slabRateObj.optString("freebee_mpq");
                    String freebieQty = slabRateObj.optString("freebee_qty");
                    String ptr = slabRateObj.optString("ptr");
                    String level = slabRateObj.optString("level");
                    String levelName = slabRateObj.optString("level_name");
                    String stock = slabRateObj.optString("stock");
                    String productPackId = slabRateObj.optString("product_pack_id");
                    String productPriceId = slabRateObj.optString("product_price_id");
                    String freebeePackId = slabRateObj.optString("freebee_pack_id");
                    String noOfEaches = slabRateObj.optString("no_of_eaches");
                    String cfc = slabRateObj.optString("cfc");
                    boolean isSlab = slabRateObj.optString("is_slab").equals("1") ? true : false;
                    String blockedQty = slabRateObj.optString("blocked_qty");
                    int prmtDetId = slabRateObj.optInt("prmt_det_id");
                    int esu = slabRateObj.optInt("esu") == 0 ? 1 : slabRateObj.optInt("esu");
                    String star = slabRateObj.optString("star");
                    int packLevel = slabRateObj.optInt("pack_level");
                    int productSlabId = slabRateObj.optInt("product_slab_id");

                    newPacksModel.setPackSize(packSize);
                    newPacksModel.setUnitPrice(unitPrice);
                    newPacksModel.setProductId(productId);
                    newPacksModel.setIsMarkup(isMarkup);
                    newPacksModel.setMargin(margin);
                    newPacksModel.setFreebieDesc(freebieDesc);
                    newPacksModel.setFreebieProductId(freebiePrdId);
                    newPacksModel.setPtr(ptr);
                    newPacksModel.setLevel(level);
                    newPacksModel.setLevelName(levelName);
                    newPacksModel.setStock(stock);
                    newPacksModel.setProductPackId(productPackId);
                    newPacksModel.setProductPriceId(productPriceId);
                    newPacksModel.setFreebiePackId(freebeePackId);
                    newPacksModel.setNoOfEaches(noOfEaches);
                    newPacksModel.setCfc(cfc);
                    newPacksModel.setPrmtDetId(prmtDetId);
                    newPacksModel.setSlabEsu(esu);
                    newPacksModel.setStar(star);
                    newPacksModel.setPackLevel(packLevel);
                    newPacksModel.setProductSlabId(productSlabId);

                    double _freebieQty = 0;
                    int blockQty = 0;
                    try {
                        _freebieQty = freebieQty == null ? 0 : Double.parseDouble(freebieQty);
                        blockQty = blockedQty == null ? 0 : Integer.parseInt(blockedQty);
                    } catch (Exception e) {
                    }

                    newPacksModel.setBlockedQty(blockQty);
                    newPacksModel.setSlab(isSlab);

                    int _freebieMpq = 0;
                    try {
                        _freebieMpq = freebieMpq == null ? 0 : Integer.parseInt(freebieMpq);
                        newPacksModel.setFreebieMpq(_freebieMpq);
                        newPacksModel.setFreebieQty(_freebieQty);
                    } catch (Exception e) {
                        newPacksModel.setFreebieQty(0);
                    }
                    ArrayList<CashBackDetailsModel> cashBackDetailsModelArrayList = new ArrayList<>();

                    try {
                        JSONArray cashBackArr = new JSONArray();

                        String request = slabRateObj.optString("cashback_details");

                        if (request != null && !TextUtils.isEmpty(request) && !request.equalsIgnoreCase("null")) {

                            cashBackArr = new JSONArray(request);
                            if (cashBackArr != null && cashBackArr.length() > 0) {
                                for (int j = 0; j < cashBackArr.length(); j++) {
                                    JSONObject cashBackObj = cashBackArr.optJSONObject(j);
                                    CashBackDetailsModel cashBackDetailsModel = new CashBackDetailsModel();

                                    double cbk_value = cashBackObj.optDouble("cbk_value");
                                    int cashback_id = cashBackObj.optInt("cashback_id");//send
                                    int reference_id = cashBackObj.optInt("reference_id");
                                    int cashback_type = cashBackObj.optInt("cashback_type");
                                    double qty_from_range = cashBackObj.optDouble("qty_from_range");
                                    int cbk_source_type = cashBackObj.optInt("cbk_source_type");//2 prd detail ecash,
                                    double ff_qty_to_range = cashBackObj.optDouble("qty_to_range");
                                    int benificiary_type = cashBackObj.optInt("benificiary_type");
                                    String cashback_description = cashBackObj.optString("cashback_description");
                                    String cashbackStar = cashBackObj.optString("product_star");
//                                    Log.e("Cashback_description", cashback_description);

                                    cashBackDetailsModel.setCbk_value(cbk_value);
                                    cashBackDetailsModel.setCashback_id(cashback_id);
                                    cashBackDetailsModel.setReference_id(reference_id);
                                    cashBackDetailsModel.setCashback_type(cashback_type);
                                    cashBackDetailsModel.setQty_from_range(qty_from_range);
                                    cashBackDetailsModel.setCbk_source_type(cbk_source_type);
                                    cashBackDetailsModel.setFf_qty_to_range(ff_qty_to_range);
                                    cashBackDetailsModel.setBenificiary_type(benificiary_type);
                                    cashBackDetailsModel.setCashback_description(cashback_description);
                                    cashBackDetailsModel.setCashbackStar(cashbackStar);

                                    cashBackDetailsModelArrayList.add(cashBackDetailsModel);

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    newPacksModel.setArrCashBackDetails(cashBackDetailsModelArrayList);
                    arrSlabRates.add(newPacksModel);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrSlabRates;
    }

    public ArrayList<SortModel> getSortData(String sortData) {
        ArrayList<SortModel> arrSort = new ArrayList<>();
        try {
            if (sortData != null && sortData.length() > 0) {
                JSONArray dataArray = new JSONArray(sortData);
                if (dataArray != null) {
                    for (int i = 0; i < dataArray.length(); i++) {
                        SortModel sortModel = new SortModel();
                        JSONObject sortObj = dataArray.getJSONObject(i);
                        String id = sortObj.optString("id");
                        String name = sortObj.optString("name");
                        String sortId = sortObj.optString("sort_id");
                        sortModel.setId(id);
                        sortModel.setName(name);
                        sortModel.setSortId(sortId);
                        arrSort.add(sortModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrSort;
    }

    public NewProductModel getProductDetails(JSONObject jsonObject) {
        NewProductModel productModel = new NewProductModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            JSONObject productObj = dataObj.optJSONObject("data");
            String productId = productObj.optString("product_id");
            String productName = productObj.optString("product_name").trim();
            String productRating = productObj.optString("rating");
            ArrayList<SpecificationsModel> arrSpecs = new ArrayList<>();
            ArrayList<ReviewModel> arrReviews = new ArrayList<>();
            ArrayList<ProductsModel> arrRelatedProducts = new ArrayList<>();

            JSONArray variantsArray = productObj.optJSONArray("variants");
            // primary
            ArrayList<NewVariantModel> variantsArrayList = new ArrayList<>();
            if (variantsArray != null && variantsArray.length() > 0) {
                variantsArrayList = getVariants(variantsArray);
            }
            productModel.setProductId(productId);
            productModel.setProductName(productName);
            productModel.setProductRating(productRating);
            productModel.setVariantModelArrayList(variantsArrayList);
            JSONArray reviewArray = productObj.optJSONArray("reviews");
            if (reviewArray != null && reviewArray.length() > 0) {
                for (int i = 0; i < reviewArray.length(); i++) {
                    JSONObject reviewObj = reviewArray.getJSONObject(i);
                    String review = reviewObj.optString("text");
                    String customer_id = reviewObj.optString("customer_id");
                    String author = reviewObj.optString("author");
                    String rating = reviewObj.optString("rating");

                    ReviewModel model = new ReviewModel();
                    model.setReview(review);
                    model.setAuthor(author);
                    model.setRating(rating);
                    model.setCustomerId(customer_id);
                    arrReviews.add(model);
                }
            }
            productModel.setArrReviews(arrReviews);
            JSONArray relatedProductsArray = productObj.optJSONArray("related_products");
            if (relatedProductsArray != null && relatedProductsArray.length() > 0) {
                for (int i = 0; i < relatedProductsArray.length(); i++) {
                    JSONObject relatedPrdObj = relatedProductsArray.getJSONObject(i);
                    String relatedId = relatedPrdObj.optString("related_id");

                    String image = relatedPrdObj.optString("image");
                    String prdName = relatedPrdObj.optString("name");
                    ProductsModel model = new ProductsModel();
                    model.setImage(image);
                    model.setProduct_id(relatedId);
                    model.setName(prdName);
                    arrRelatedProducts.add(model);
                }

            }
            productModel.setArrRelatedPrds(arrRelatedProducts);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return productModel;
    }

    public ProductsResponse getProductIds(JSONObject jsonObject, boolean isArray) {

        ProductsResponse productsResponse = new ProductsResponse();
        DBHelper.getInstance().deleteTable(DBHelper.TABLE_TEMP_PRODUCTS);
        try {
            String productIds = "";
            int count = 0;
            JSONObject dataObj = null;
            if (isArray) {
                dataObj = jsonObject.optJSONArray("data").optJSONObject(0);
            } else {
                dataObj = jsonObject.optJSONObject("data");
            }
            if (dataObj != null && dataObj.length() > 0) {
                productIds = dataObj.optString("product_id");
                if (productIds.endsWith(",")) {
                    productIds = productIds.substring(0, productIds.length() - 1);
                }
                try {
                    count = Integer.parseInt(dataObj.optString("count"));
                } catch (Exception e) {
                    count = 0;
                }
                DBHelper.getInstance().insertProductInTemp(productIds);
            } else {
                count = 0;
            }
            productsResponse.setProductIds(productIds);
            productsResponse.setTotalItemsCount(count);
        } catch (Exception e) {
            productsResponse.setTotalItemsCount(0);
            e.printStackTrace();
        }
        return productsResponse;
    }

    public NewProductSKUListModel getMultiVariantProducts(JSONObject jsonObject) {
        NewProductSKUListModel productSKUListModel = new NewProductSKUListModel();
        try {
            int totalItemsCount;
            JSONObject dataObj = jsonObject.optJSONObject("data");
            try {
                String totalProducts = dataObj.optString("TotalProducts");
                if (totalProducts != null) {
                    totalItemsCount = totalProducts.equalsIgnoreCase("0") ? 0 : Integer.parseInt(dataObj.optString("TotalProducts"));
                } else {
                    totalItemsCount = 0;
                }
            } catch (Exception e) {
                totalItemsCount = 0;
            }

            productSKUListModel.setTotalItemsCount(totalItemsCount);

            if (totalItemsCount > 0) {


                JSONArray dataArray = dataObj.optJSONArray("data");
                if (dataArray != null && dataArray.length() > 0) {
                    ArrayList<NewProductModel> productModelArrayList = new ArrayList<>();
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject productObj = dataArray.optJSONObject(i);
                        String productId = productObj.optString("product_id");
                        String productName = productObj.optString("product_name").trim();
                        String rating = productObj.optString("rating");

                        JSONArray variantsArray = productObj.optJSONArray("variants");
                        // primary
                        ArrayList<NewVariantModel> variantsArrayList = new ArrayList<>();
                        if (variantsArray != null && variantsArray.length() > 0) {
                            variantsArrayList = getVariants(variantsArray);
                        }

                        NewProductModel productModel = new NewProductModel();
                        productModel.setProductId(productId);
                        productModel.setProductName(productName);
                        productModel.setProductRating(rating);
                        productModel.setVariantModelArrayList(variantsArrayList);

                        productModelArrayList.add(productModel);

                    }
                    productSKUListModel.setProductModelArrayList(productModelArrayList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productSKUListModel;
    }

    private ArrayList<NewVariantModel> getVariants(JSONArray jsonArray) {
        ArrayList<NewVariantModel> variantsList = new ArrayList<>();
        if (jsonArray != null && jsonArray.length() > 0) {
            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject variantObj = jsonArray.optJSONObject(j);
                String variantName = variantObj.optString("variant_name");
                String productName = variantObj.optString("product_name").trim();
                String productId = variantObj.optString("product_id");
                String description = variantObj.optString("description");
                String mrp = variantObj.optString("mrp");
                String quantity = variantObj.optString("quantity");
                String orderedQuantity = variantObj.optString("ordered_quantity");
                String rating = variantObj.optString("rating");
                String image = variantObj.optString("image");
                int hasInnerVarients = variantObj.optInt("has_inner_varients");
                JSONArray arrImages = variantObj.optJSONArray("images");
                ArrayList<SpecificationsModel> arrSpecs = new ArrayList<>();
                ArrayList<String> arrProductImgs = new ArrayList<>();
                if (arrImages != null && arrImages.length() > 0) {
                    for (int k = 0; k < arrImages.length(); k++) {
                        JSONObject objImage = arrImages.optJSONObject(k);
                        arrProductImgs.add(objImage.optString("image"));
                    }
                }

                JSONArray specArr = variantObj.optJSONArray("specifications");
                if (specArr != null && specArr.length() > 0) {
                    for (int k = 0; k < specArr.length(); k++) {
                        JSONObject specObj = specArr.optJSONObject(k);
                        SpecificationsModel specificationsModel = new SpecificationsModel();
                        specificationsModel.setAttributeId(specObj.optString("attribute_id"));
                        specificationsModel.setSpecValue(specObj.optString("value"));
                        specificationsModel.setSpecName(specObj.optString("name"));
                        arrSpecs.add(specificationsModel);
                    }
                }

                ArrayList<NewVariantModel> variantsArrayList = null;
                ArrayList<NewPacksModel> packs = null;
                if (hasInnerVarients == 1) {
                    JSONArray secondaryPacksArray = variantObj.optJSONArray("variants");
                    variantsArrayList = getVariants(secondaryPacksArray);
                } else {
                    //packs parsing
                    JSONArray packsArray = variantObj.optJSONArray("packs");
                    packs = getPacks(packsArray);
                }

                NewVariantModel testVariant = new NewVariantModel();
                testVariant.setVariantName(variantName);
                testVariant.setArrImages(arrProductImgs);
                testVariant.setArrSpecs(arrSpecs);
                testVariant.setHasInnerVariants(hasInnerVarients);
                testVariant.setVariantModelArrayList(variantsArrayList);
                testVariant.setPacksModelArrayList(packs);
                testVariant.setProductName(productName);
                testVariant.setSkuId(productId);
                testVariant.setProductDescription(description);
                testVariant.setMrp(mrp);
                testVariant.setAvailableQuantity(quantity);
                testVariant.setOrderedQuantity(orderedQuantity);
                testVariant.setImage(image);
                testVariant.setRating(rating);
                variantsList.add(testVariant);
            }
        }
        return variantsList;
    }

    private ArrayList<NewPacksModel> getPacks(JSONArray jsonArray) {
        ArrayList<NewPacksModel> packs = new ArrayList<>();
        try {
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject pack = jsonArray.optJSONObject(i);
                    String productSlabId = pack.optString("product_slab_id").equalsIgnoreCase("null") ? "" : pack.optString("product_slab_id");
                    String productId = pack.optString("product_id").equalsIgnoreCase("null") ? "" : pack.optString("product_id");
                    String packSize = pack.optString("pack_size").equalsIgnoreCase("null") ? "" : pack.optString("pack_size");
                    String margin = pack.optString("margin").equalsIgnoreCase("null") ? "" : pack.optString("margin");
                    String unitPrice = pack.optString("unit_price").equalsIgnoreCase("null") ? "" : pack.optString("unit_price");
                    String isMarkup = pack.optString("is_markup");

                    NewPacksModel testPack = new NewPacksModel();
                    testPack.setIsMarkup(isMarkup);
                    testPack.setMargin(margin);
                    testPack.setPackSize(packSize);
                    testPack.setProductId(productId);
                    testPack.setUnitPrice(unitPrice);

                    packs.add(testPack);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return packs;
    }

    public ArrayList<TestProductModel> getOfflineProducts(JSONArray results, Context context) {

        mSharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);

        int syncTimeInterval = mSharedPreferences.getInt(ConstantValues.KEY_SYNC_INTERVAL, ConstantValues.KEY_SYNC_INTERVAL_DURATION);
//        int syncTimeInterval = ConstantValues.KEY_SYNC_INTERVAL_DURATION;
        String past = Utils.getPastDateTime(syncTimeInterval);

        DBHelper.getInstance().deleteOldRecords(past);

        ArrayList<TestProductModel> testProductModelArrayList = new ArrayList<>();
        try {
            for (int i = 0; i < results.length(); i++) {
                JSONObject dataObj = results.optJSONObject(i);

                String product_id = dataObj.optString("product_id");
                String primary_image = dataObj.optString("primary_image");
                String thumbnail_image = dataObj.optString("thumbnail_image");
                String product_title = dataObj.optString("product_title");
                String category_id = dataObj.optString("category_id");
                String brand_id = dataObj.optString("brand_id");
                String manufacturer_id = dataObj.optString("manufacturer_id");
                String mrp = dataObj.optString("mrp");
                String variant_value1 = dataObj.optString("variant_value1");
                String variant_value2 = dataObj.optString("variant_value2");
                String variant_value3 = dataObj.optString("variant_value3");
                int is_parent = dataObj.optInt("is_parent");
                String key_value_index = dataObj.optString("key_value_index");
                String parent_id = dataObj.optString("parent_id");
                String meta_keywords = dataObj.optString("meta_keywords");
                String esu = dataObj.optString("esu");
                String packType = dataObj.optString("pack_type");
                String sku = dataObj.optString("sku");
                String star = (TextUtils.isEmpty(dataObj.optString("star")) || dataObj.optString("star").equals("0") || dataObj.optString("star").equalsIgnoreCase("null")) ? "#FFFFFF" : dataObj.optString("star");
                boolean isCashback = dataObj.optInt("cashback_flag") == 1 ? true : false;

                TestProductModel productModel = new TestProductModel();
                productModel.setProductId(product_id);
                productModel.setPrimaryImage(primary_image);
                productModel.setThumbnailImage(thumbnail_image);
                productModel.setProductTitle(product_title);
                productModel.setCategoryId(category_id);
                productModel.setBrandId(brand_id);
                productModel.setManufacturerId(manufacturer_id);
                productModel.setMrp(mrp);
                productModel.setVariantValue1(variant_value1);
                productModel.setVariantValue2(variant_value2);
                productModel.setVariantValue3(variant_value3);
                productModel.setIsParent(is_parent);
                productModel.setKeyValueIndex(key_value_index);
                productModel.setParentId(parent_id);
                productModel.setMetaKeywords(meta_keywords);
                productModel.setEsu(esu);
                productModel.setPackType(packType);
                productModel.setSKUCode(sku);
                productModel.setStar(star);
                productModel.setCashback(isCashback);

                DBHelper.getInstance().insertProduct(productModel, past);
                testProductModelArrayList.add(productModel);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return testProductModelArrayList;
    }

    public RetailersModel getRetailerToken(JSONObject jsonObject) {
        RetailersModel retailersModel = new RetailersModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            JSONObject detailsObj = dataObj.optJSONObject("data");

            String customerGroupId = detailsObj.optString("customer_group_id");
            String customerToken = detailsObj.optString("customer_token");
            String customerId = detailsObj.optString("customer_id");
            String firstName = detailsObj.optString("firstname");
            String lastName = detailsObj.optString("lastname");
            String image = detailsObj.optString("image");
            String segmentId = detailsObj.optString("segment_id");
            String pincode = detailsObj.optString("pincode");
            String leWhId = detailsObj.optString("le_wh_id");
            String hub = detailsObj.optString("hub");
            String legalEntityId = detailsObj.optString("legal_entity_id");
            String beatId = detailsObj.optString("beat_id");
            String latitude = detailsObj.optString("latitude");
            String longitude = detailsObj.optString("longitude");

            JSONObject object = detailsObj.optJSONObject("ecash_details");

            if (object != null) {
                double creditLimit = object.optDouble("creditlimit");
                double eCash = object.optDouble("ecash");
                double payementDue = object.optDouble("payment_due");

                retailersModel.setCreditLimit(creditLimit);
                retailersModel.setEcash(eCash);
                retailersModel.setPaymentDue(payementDue);
            }

            JSONObject ffeCashObj = detailsObj.optJSONObject("ff_ecash_details");

            if (ffeCashObj != null) {
                double creditLimit = ffeCashObj.optDouble("creditlimit");
                double eCash = ffeCashObj.optDouble("ecash");
                double payementDue = ffeCashObj.optDouble("payment_due");

                retailersModel.setFfCreditLimit(creditLimit);
                retailersModel.setFfeCash(eCash);
                retailersModel.setFfPaymentDue(payementDue);
            }

            retailersModel.setCustomerGrpId(customerGroupId);
            retailersModel.setCustomerToken(customerToken);
            retailersModel.setCustomerId(customerId);
            retailersModel.setFirstName(firstName);
            retailersModel.setLastName(lastName);
            retailersModel.setImage(image);
            retailersModel.setSegmentId(segmentId);
            retailersModel.setPincode(pincode);
            retailersModel.setLeWhId(leWhId);
            retailersModel.setHub(hub);
            retailersModel.setLegalEntityId(legalEntityId);
            retailersModel.setBeatId(beatId);
            retailersModel.setLatitude(latitude);
            retailersModel.setLongitude(longitude);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return retailersModel;
    }

    public ArrayList<String> getPinCodeAreas(JSONArray jsonArray) {
        ArrayList<String> areas = new ArrayList<>();
        try {
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject obj = jsonArray.optJSONObject(i);
                    String area = obj.optString("officename");
                    areas.add(area);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return areas;
    }

    public PincodeDataModel getPinCodeData(JSONObject jsonObject) {
        PincodeDataModel pincodeDataModel = new PincodeDataModel();
        ArrayList<String> areas = new ArrayList<>();
        String stateId, stateName;
        stateId = jsonObject.optString("state_id");
        stateName = jsonObject.optString("state_name");
        JSONArray dataArray = jsonObject.optJSONArray("data");
        try {
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject obj = dataArray.optJSONObject(i);
                    String area = obj.optString("officename");
                    areas.add(area);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pincodeDataModel.setArrAreas(areas);
        pincodeDataModel.setStateId(stateId);
        pincodeDataModel.setStateName(stateName);

        return pincodeDataModel;
    }

    public SupplierMasterLookupModel getSupplierMasterLookupData(JSONObject object) {
        SupplierMasterLookupModel supplierMasterLookupModel = null;
        try {
            supplierMasterLookupModel = new SupplierMasterLookupModel();
            JSONArray organizationType = object.optJSONArray("organization_type");
            if (organizationType != null && organizationType.length() > 0) {
                ArrayList<CustomerTyepModel> list = new ArrayList<>();
                for (int i = 0; i < organizationType.length(); i++) {
                    JSONObject obj = organizationType.optJSONObject(i);
                    String masterLookupName = obj.optString("master_lookup_name");
                    String value = obj.optString("value");

                    if (i == 0) {
                        CustomerTyepModel model = new CustomerTyepModel();
                        model.setCustomerName("Please select Organization Type");
                        model.setCustomerGrpId("");
                        list.add(model);
                    }
                    CustomerTyepModel model = new CustomerTyepModel();
                    model.setCustomerName(masterLookupName);
                    model.setCustomerGrpId(value);
                    list.add(model);
                }
                supplierMasterLookupModel.setOrganizationTypes(list);
            }

            JSONArray supplierTypes = object.optJSONArray("supplier_type");
            if (supplierTypes != null && supplierTypes.length() > 0) {
                ArrayList<CustomerTyepModel> list = new ArrayList<>();
                for (int i = 0; i < supplierTypes.length(); i++) {
                    JSONObject obj = supplierTypes.optJSONObject(i);
                    String masterLookupName = obj.optString("master_lookup_name");
                    String value = obj.optString("value");

                    if (i == 0) {
                        CustomerTyepModel model = new CustomerTyepModel();
                        model.setCustomerName("Please select Supplier Type");
                        model.setCustomerGrpId("");
                        list.add(model);
                    }
                    CustomerTyepModel model = new CustomerTyepModel();
                    model.setCustomerName(masterLookupName);
                    model.setCustomerGrpId(value);
                    list.add(model);
                }
                supplierMasterLookupModel.setSupplierTypes(list);
            }

            JSONArray supplierRanks = object.optJSONArray("supplier_rank");
            if (supplierRanks != null && supplierRanks.length() > 0) {
                ArrayList<CustomerTyepModel> list = new ArrayList<>();
                for (int i = 0; i < supplierRanks.length(); i++) {
                    JSONObject obj = supplierRanks.optJSONObject(i);
                    String masterLookupName = obj.optString("master_lookup_name");
                    String value = obj.optString("value");

                    if (i == 0) {
                        CustomerTyepModel model = new CustomerTyepModel();
                        model.setCustomerName("Please select Supplier Rank");
                        model.setCustomerGrpId("");
                        list.add(model);
                    }

                    CustomerTyepModel model = new CustomerTyepModel();
                    model.setCustomerName(masterLookupName);
                    model.setCustomerGrpId(value);
                    list.add(model);
                }
                supplierMasterLookupModel.setSupplierRanks(list);
            }

            JSONArray relationshipManagers = object.optJSONArray("relation_manager_list");
            if (relationshipManagers != null && relationshipManagers.length() > 0) {
                ArrayList<CustomerTyepModel> list = new ArrayList<>();
                for (int i = 0; i < relationshipManagers.length(); i++) {
                    JSONObject obj = relationshipManagers.optJSONObject(i);
                    String username = obj.optString("username");
                    String id = obj.optString("id");

                    if (i == 0) {
                        CustomerTyepModel model = new CustomerTyepModel();
                        model.setCustomerName("Please select Relation Manger");
                        model.setCustomerGrpId("");
                        list.add(model);
                    }

                    CustomerTyepModel model = new CustomerTyepModel();
                    model.setCustomerName(username);
                    model.setCustomerGrpId(id);
                    list.add(model);
                }
                supplierMasterLookupModel.setRelationshipManagers(list);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return supplierMasterLookupModel;
        }
        return supplierMasterLookupModel;
    }

    public TestProductModel getImagesDesc(JSONObject jsonObject) {
        TestProductModel testProductModel = new TestProductModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            JSONArray imagesArray = dataObj.optJSONArray("images");
            ArrayList<String> arrImages = new ArrayList<>();
            ;
            if (imagesArray != null && imagesArray.length() > 0) {
                for (int i = 0; i < imagesArray.length(); i++) {
                    JSONObject obj = imagesArray.optJSONObject(i);
                    String image = obj.optString("image");
                    arrImages.add(image);
                }
            }
            String description = dataObj.optString("description");

            testProductModel.setDescription(description);
            testProductModel.setArrImages(arrImages);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return testProductModel;
    }

    public TestProductModel getSpecsReview(JSONObject jsonObject) {
        TestProductModel testProductModel = new TestProductModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            JSONArray specsArray = dataObj.optJSONArray("specifications");
            ArrayList<SpecificationsModel> arrSpes = new ArrayList<>();
            ;
            if (specsArray != null && specsArray.length() > 0) {
                for (int i = 0; i < specsArray.length(); i++) {
                    SpecificationsModel specificationsModel = new SpecificationsModel();
                    JSONObject specsObj = specsArray.optJSONObject(i);
                    String attributeId = specsObj.optString("attribute_id");
                    String value = specsObj.optString("value");
                    String name = specsObj.optString("name");

                    specificationsModel.setAttributeId(attributeId);
                    specificationsModel.setSpecValue(value);
                    specificationsModel.setSpecName(name);

                    arrSpes.add(specificationsModel);
                }
            }
            ArrayList<ReviewModel> arrReviews = new ArrayList<>();
            JSONArray reviewArray = dataObj.optJSONArray("reviews");
            if (reviewArray != null && reviewArray.length() > 0) {
                for (int i = 0; i < reviewArray.length(); i++) {
                    ReviewModel reviewModel = new ReviewModel();
                    JSONObject reviewObj = reviewArray.optJSONObject(i);
                    String userId = reviewObj.optString("user_id");
                    String author = reviewObj.optString("author");
                    String comment = reviewObj.optString("comment");
                    String rating = reviewObj.optString("rating");

                    reviewModel.setCustomerId(userId);
                    reviewModel.setAuthor(author);
                    reviewModel.setReview(comment);
                    reviewModel.setRating(rating);

                    arrReviews.add(reviewModel);
                }
            }

            testProductModel.setArrSpecs(arrSpes);
            testProductModel.setArrReviews(arrReviews);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return testProductModel;
    }

    public AddCart1Response getAddCart1Response(JSONObject object) {
        AddCart1Response response = null;
        try {
            String productId = object.optString("product_id");
            String availableQuantity = object.optString("available_quantity");
            int status = object.optInt("status");

            response = new AddCart1Response();
            response.setStatus(status == 1);
            response.setAvailableQty(availableQuantity);
            response.setProductId(productId);

        } catch (Exception e) {
            e.printStackTrace();
            response = null;
        } finally {

        }
        return response;
    }

    ArrayList<DashboardModel> getRetailerDashboard(JSONArray array) {

        ArrayList<DashboardModel> dashboardModelArrayList = new ArrayList<>();
        DashboardModel model;
        try {
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String key = object.optString("key");
                    String value = object.optString("value");

                    model = new DashboardModel(key, value);
                    dashboardModelArrayList.add(model);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return dashboardModelArrayList;
    }

    ArrayList<DashboardModelNew> getRetailerDashboardNew(JSONArray array) {

        ArrayList<DashboardModelNew> dashboardModelArrayList = new ArrayList<>();
        try {
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {

                    DashboardModelNew dashboardModelNew = new DashboardModelNew();

                    JSONArray dataArray = array.optJSONArray(i);
                    if (dataArray != null && dataArray.length() > 0) {
                        ArrayList<DashboardModel> arrData = new ArrayList<>();
                        for (int j = 0; j < dataArray.length(); j++) {
                            JSONObject object = dataArray.optJSONObject(j);

                            String key = object.optString("key");
                            String value = object.optString("val");
                            String per = object.optString("per");

                            DashboardModel model = new DashboardModel(key, value);
                            model.setPer(per);
                            arrData.add(model);
                        }

                        dashboardModelNew.setCount(arrData.size());
                        dashboardModelNew.setArrData(arrData);

                    }
                    dashboardModelArrayList.add(dashboardModelNew);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return dashboardModelArrayList;
    }


    public ArrayList<TeamDashboard> getMyTeamDashboard(JSONArray dataArray) {

        ArrayList<TeamDashboard> teamDashboardArrayList = new ArrayList<>();
        if (dataArray != null && dataArray.length() > 0) {
            try {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject object = dataArray.optJSONObject(i);
                    String name = object.optString("name");
                    String userId = object.optString("user_id");
                    ArrayList<DashboardModel> dashboardModelArrayList = getRetailerDashboard(object.optJSONArray("data"));

                    TeamDashboard dashboard = new TeamDashboard();
                    dashboard.setName(name);
                    dashboard.setUserId(userId);
                    dashboard.setDashboardModelArrayList(dashboardModelArrayList);
                    teamDashboardArrayList.add(dashboard);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return teamDashboardArrayList;
    }

    public CheckCartInventoryModel checkCartInventoryResponse(JSONObject jsonObject, Context context) {
        CheckCartInventoryModel checkCartInventoryModel = new CheckCartInventoryModel();
        ArrayList<CheckInventoryModel> inventoryResult = null;
        mSharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        String customerId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
        try {
            double discount = jsonObject.optDouble("discount");
            JSONArray orderlevelarr = new JSONArray();

            ArrayList<OrderLevelCashBackModel> orderLevelCashBackModelArrayList = new ArrayList<>();
            String request = jsonObject.optString("order_level_cashback");
            if (request != null && !request.equalsIgnoreCase("null") && !TextUtils.isEmpty(request)) {

                try {
                    orderlevelarr = new JSONArray(request);
                    if (orderlevelarr != null && orderlevelarr.length() > 0) {

                        for (int j = 0; j < orderlevelarr.length(); j++) {

                            JSONObject orderLevelObj = orderlevelarr.optJSONObject(j);
                            OrderLevelCashBackModel orderLevelCashBackModel = new OrderLevelCashBackModel();
                            String cbk_id = orderLevelObj.optString("cbk_id");
                            String cbk_label = orderLevelObj.optString("cbk_label");

                            orderLevelCashBackModel.setCbk_id(cbk_id);
                            orderLevelCashBackModel.setCbk_label(cbk_label);

                            orderLevelCashBackModelArrayList.add(orderLevelCashBackModel);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            checkCartInventoryModel.setArrOrderLevelCashback(orderLevelCashBackModelArrayList);
            checkCartInventoryModel.setDiscountAmount(discount);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            if (jsonArray != null && jsonArray.length() > 0) {
                inventoryResult = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    String productId = object.optString("product_id");
                    String warehouseId = object.optString("le_wh_id");
                    String cartId = object.optString("cartId");
                    int availableQty = object.optInt("available_quantity");
                    int status = object.optInt("status");
                    int isSlab = object.optInt("is_slab");
                    int blockedQty = object.optInt("blocked_qty");
                    int prmtDetId = object.optInt("prmt_det_id");

                    CheckInventoryModel inventoryModel = new CheckInventoryModel();
                    inventoryModel.setProductId(productId);
                    inventoryModel.setAvailableQty(availableQty);
                    inventoryModel.setCartId(cartId);
                    inventoryModel.setStatus(status);
                    inventoryModel.setWarehouseId(warehouseId);
                    inventoryModel.setIsSlab(isSlab);
                    inventoryModel.setBlockedQty(blockedQty);
                    inventoryModel.setPromotionId(prmtDetId);

                    DBHelper.getInstance().updateProductInCart(customerId, inventoryModel);
                    inventoryResult.add(inventoryModel);

                }
                checkCartInventoryModel.setArrCartItems(inventoryResult);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return checkCartInventoryModel;
    }


    public PriceHistoryModel getPriceHistory(JSONObject jsonObject) {
        ArrayList<NewPacksModel> arrPriceHistory = new ArrayList<>();
        PriceHistoryModel priceHistoryModel = new PriceHistoryModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            String minPrice = dataObj.optString("min");
            String maxPrice = dataObj.optString("max");
            String avgPrice = dataObj.optString("avarage");
            priceHistoryModel.setMinPrice(minPrice);
            priceHistoryModel.setAvgPrice(avgPrice);
            priceHistoryModel.setMaxPrice(maxPrice);
            JSONArray dataArr = dataObj.optJSONArray("data");
            if (dataArr != null && dataArr.length() > 0) {
                for (int i = 0; i < dataArr.length(); i++) {
                    NewPacksModel newPacksModel = new NewPacksModel();
                    JSONObject priceHistoryObj = dataArr.optJSONObject(i);
                    String productName = priceHistoryObj.optString("product_name");
                    String price = priceHistoryObj.optString("price");
                    String supplierName = priceHistoryObj.optString("supplier_name");
                    String date = priceHistoryObj.optString("effective_date");

                    newPacksModel.setProductName(productName);
                    newPacksModel.setPrice(price);
                    newPacksModel.setSupplierName(supplierName);
                    newPacksModel.setDate(date);

                    arrPriceHistory.add(newPacksModel);
                }
                priceHistoryModel.setArrPrices(arrPriceHistory);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return priceHistoryModel;
    }

    public ArrayList<BeatModel> getBeatsByFFID(JSONObject jsonObject) {
        ArrayList<BeatModel> arrBeats = new ArrayList<>();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            JSONArray beatsArr = dataObj.optJSONArray("beats");
            if (beatsArr != null && beatsArr.length() > 0) {
                for (int i = 0; i < beatsArr.length(); i++) {
                    BeatModel beatModel = new BeatModel();
                    JSONObject beatObj = beatsArr.optJSONObject(i);
                    String address = beatObj.optString("address");
                    String day = beatObj.optString("day");
                    String beatId = beatObj.optString("beat_id");
                    String totalOutlets = beatObj.optString("total_outlets");
                    String rmId = beatObj.optString("rm_id");
                    String rmName = beatObj.optString("rm_name");
                    String dcId = beatObj.optString("dc_id");
                    String spokeId = beatObj.optString("spoke_id");
                    String spokeName = beatObj.optString("spoke_name");
                    String hubId = beatObj.optString("hub_id");

                    beatModel.setAddress(address);
                    beatModel.setBeatName(address);
                    beatModel.setDay(day);
                    beatModel.setBeatId(beatId);
                    beatModel.setTotalOutlets(totalOutlets);
                    beatModel.setRmId(rmId);
                    beatModel.setRmName(rmName);
                    beatModel.setDcId(dcId);
                    beatModel.setSpokeId(spokeId);
                    beatModel.setSpokeName(spokeName);
                    beatModel.setHubId(hubId);

                    arrBeats.add(beatModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrBeats;
    }

    public ArrayList<CustomerTyepModel> getFeedbackReasons(JSONObject jsonObject) {
        ArrayList<CustomerTyepModel> arrFeedbackReasons = new ArrayList<>();
        try {
            JSONArray FeedbackReasonsArr = jsonObject.optJSONArray("data");
            if (FeedbackReasonsArr != null && FeedbackReasonsArr.length() > 0) {
                for (int i = 0; i < FeedbackReasonsArr.length(); i++) {
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    JSONObject feedbackReasonObj = FeedbackReasonsArr.optJSONObject(i);
                    String name = feedbackReasonObj.optString("name");
                    String value = feedbackReasonObj.optString("value");

                    customerTyepModel.setCustomerName(name);
                    customerTyepModel.setCustomerGrpId(value);

                    arrFeedbackReasons.add(customerTyepModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrFeedbackReasons;
    }

    public ArrayList<WarehouseModel> getwareHouses(JSONObject jsonObject) {
        ArrayList<WarehouseModel> arrWhs = new ArrayList<>();
        try {
            JSONArray dataArr = jsonObject.optJSONArray("data");
            if (dataArr != null && dataArr.length() > 0) {
                for (int i = 0; i < dataArr.length(); i++) {
                    JSONObject whObj = dataArr.optJSONObject(i);
                    WarehouseModel warehouseModel = new WarehouseModel();
                    String whId = whObj.optString("le_wh_id");
                    String whName = whObj.optString("lp_wh_name").trim();

                    warehouseModel.setWhId(whId);
                    warehouseModel.setWhName(whName);

                    arrWhs.add(warehouseModel);

                }
                Collections.sort(arrWhs, new Comparator<WarehouseModel>() {
                    public int compare(WarehouseModel v1, WarehouseModel v2) {
                        return v1.getWhName().compareToIgnoreCase(v2.getWhName());
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrWhs;
    }

    public ArrayList<InventoryModel> getInventory(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() == 0) {
            return null;
        }
        ArrayList<InventoryModel> inventoryModels = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.optJSONObject(i);
                String key = obj.optString("key");
                key = key.replace("_", " ");
                String value = obj.optString("value");
                value = value.replace("_", " ");
                String[] strKeyArray = key.split(" ");
                StringBuilder builderKey = new StringBuilder();
                for (String s : strKeyArray) {
                    String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                    builderKey.append(cap + " ");
                }

                String[] strValueArray = value.split(" ");
                StringBuilder builderValue = new StringBuilder();
                for (String s : strValueArray) {
                    String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                    builderValue.append(cap + " ");
                }

                InventoryModel inventoryModel = new InventoryModel(builderKey.toString(), builderValue.toString());
                inventoryModels.add(inventoryModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inventoryModels;
    }

    public SupplierModel getCreateSupplierResponse(JSONObject object) {
        SupplierModel supplierModel = null;
        try {
            boolean status = object.optBoolean("status");
            if (status) {
                String supplierId = object.optString("supplier_id");
                String erpCode = object.optString("erp_code");
                String legalEntityId = object.optString("legalentity_id");
                supplierModel = new SupplierModel(supplierId, erpCode, legalEntityId);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return supplierModel;
    }

    public ArrayList<ManufacturerModel> getManufacturers(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() == 0) {
            return null;
        }
        ArrayList<ManufacturerModel> inventoryModels = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.optJSONObject(i);
                String businessLegalName = obj.optString("business_legal_name").trim();
                String legalEntityId = obj.optString("legal_entity_id");

                ManufacturerModel model = new ManufacturerModel();
                model.setManufacturerId(legalEntityId);
                model.setManufacturerName(businessLegalName);
                inventoryModels.add(model);

            }
            Collections.sort(inventoryModels, new Comparator<ManufacturerModel>() {
                public int compare(ManufacturerModel m1, ManufacturerModel m2) {
                    return m1.getManufacturerName().compareToIgnoreCase(m2.getManufacturerName());
                }
            });

            ManufacturerModel model = new ManufacturerModel();
            model.setManufacturerId("");
            model.setManufacturerName("Select Manufacturers");
            inventoryModels.add(0, model);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return inventoryModels;
    }

    public POMasterLookUpModel getPOMasterLookUp(JSONObject jsonObject) {

        POMasterLookUpModel poMasterLookUpModel = new POMasterLookUpModel();
        try {
            JSONObject dataObject = jsonObject.optJSONObject("data");
            JSONArray paymentTypeArr = dataObject.optJSONArray("payment_type");
            if (paymentTypeArr != null && paymentTypeArr.length() > 0) {
                ArrayList<CustomerTyepModel> arrPaymentType = new ArrayList<>();
                for (int i = 0; i < paymentTypeArr.length(); i++) {
                    JSONObject paymentTypeObj = paymentTypeArr.optJSONObject(i);
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    String name = paymentTypeObj.optString("name");
                    String value = paymentTypeObj.optString("value");

                    customerTyepModel.setCustomerName(name);
                    customerTyepModel.setCustomerGrpId(value);

                    if (i == 0) {
                        CustomerTyepModel customerTyepModel1 = new CustomerTyepModel();
                        customerTyepModel1.setCustomerGrpId("");
                        customerTyepModel1.setCustomerName("Select Type");
                        arrPaymentType.add(customerTyepModel1);
                    }

                    arrPaymentType.add(customerTyepModel);
                }
                poMasterLookUpModel.setArrPaymentType(arrPaymentType);
            }

            JSONArray paymentModeArr = dataObject.optJSONArray("payment_mode");
            if (paymentModeArr != null && paymentModeArr.length() > 0) {
                ArrayList<CustomerTyepModel> arrPaymentMode = new ArrayList<>();
                for (int i = 0; i < paymentModeArr.length(); i++) {
                    JSONObject paymentModeObj = paymentModeArr.optJSONObject(i);
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    String name = paymentModeObj.optString("name");
                    String value = paymentModeObj.optString("value");

                    customerTyepModel.setCustomerName(name);
                    customerTyepModel.setCustomerGrpId(value);

                    arrPaymentMode.add(customerTyepModel);
                }
                poMasterLookUpModel.setArrPaymentMode(arrPaymentMode);
            }

            JSONArray paidThroughArr = dataObject.optJSONArray("paid_through");
            if (paidThroughArr != null && paidThroughArr.length() > 0) {
                ArrayList<CustomerTyepModel> arrPaidThrough = new ArrayList<>();
                for (int i = 0; i < paidThroughArr.length(); i++) {
                    JSONObject paidThroughObj = paidThroughArr.optJSONObject(i);
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    String tlmId = paidThroughObj.optString("tlm_id");
                    String tlmName = paidThroughObj.optString("tlm_name");
                    String tlmGroup = paidThroughObj.optString("tlm_group");

                    customerTyepModel.setCustomerGrpId(tlmId);
                    customerTyepModel.setCustomerName(tlmName);
                    customerTyepModel.setCustomerGrpName(tlmGroup);

                    if (i == 0) {
                        CustomerTyepModel customerTyepModel1 = new CustomerTyepModel();
                        customerTyepModel1.setCustomerGrpId("");
                        customerTyepModel1.setCustomerName("Select Amount");
                        customerTyepModel1.setCustomerGrpName("");
                        arrPaidThrough.add(customerTyepModel1);
                    }

                    arrPaidThrough.add(customerTyepModel);
                }

                poMasterLookUpModel.setArrPaidThrough(arrPaidThrough);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return poMasterLookUpModel;
    }

    public ArrayList<FilterDataModel> getFFList(JSONObject jsonObject) {
        ArrayList<FilterDataModel> arrFFs = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null & dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    FilterDataModel filterDataModel = new FilterDataModel();
                    JSONObject ffObj = dataArray.optJSONObject(i);

                    String ffId = ffObj.optString("ff_id");
                    String name = ffObj.optString("name");
//                    String

                    filterDataModel.setId(ffId);
                    filterDataModel.setValue(name);

                    arrFFs.add(filterDataModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrFFs;
    }

    public HashMap<String, ArrayList<FilterDataModel>> getSODashboardFilters(JSONObject jsonObject) {
        HashMap<String, ArrayList<FilterDataModel>> hashMap = new HashMap<>();
        try {

            JSONObject dataObj = jsonObject.optJSONObject("data");
            Iterator<String> keySet = dataObj.keys();
            while ((keySet.hasNext())) {
                String key = keySet.next();
                if (key.equalsIgnoreCase("hubs")) {
                    JSONArray jsonArray = dataObj.optJSONArray(key);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        ArrayList<FilterDataModel> arrData = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            FilterDataModel filterDataModel = new FilterDataModel();
                            JSONObject wareHouseObj = jsonArray.optJSONObject(i);
                            if (wareHouseObj != null) {
                                String leWhId = wareHouseObj.optString("id");
                                String lpWhName = wareHouseObj.optString("name");

                                filterDataModel.setId(leWhId);
                                filterDataModel.setValue(lpWhName);
                                filterDataModel.setKey(key);

                                arrData.add(filterDataModel);
                            }
                        }

                        hashMap.put(key, arrData);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public ArrayList<CustomerTyepModel> getBeats(JSONObject jsonObject) {
        ArrayList<CustomerTyepModel> arrBeats = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    JSONObject beatObj = dataArray.optJSONObject(i);
                    if (beatObj != null) {
                        String pjpName = beatObj.optString("pjp_name");
                        String pjpPincodeAreaId = beatObj.optString("pjp_pincode_area_id");

                        customerTyepModel.setCustomerGrpId(pjpPincodeAreaId);
                        customerTyepModel.setCustomerName(pjpName);

                        arrBeats.add(customerTyepModel);
                    }
                }

                Collections.sort(arrBeats, new Comparator<CustomerTyepModel>() {
                    public int compare(CustomerTyepModel c1, CustomerTyepModel c2) {
                        return c1.getCustomerName().compareToIgnoreCase(c2.getCustomerName());
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrBeats;
    }

    public GetExpensesResponse getExpenses(JSONObject jsonObject) {
        GetExpensesResponse expensesResponse = null;
        ArrayList<ExpenseModel> arrFFs = new ArrayList<>();
        try {
            expensesResponse = new GetExpensesResponse();
            String walletTotal = jsonObject.optString("walletTotal");
            expensesResponse.setWalletTotal(walletTotal);
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null & dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {

                    JSONObject obj = dataArray.optJSONObject(i);

                    String expId = obj.optString("exp_id");
                    String expCdoe = obj.optString("ExpCdoe");
                    String requestFor = obj.optString("RequestFor");
                    String expSubject = obj.optString("ExpSubject");
                    String actualAmount = obj.optString("ActualAmount");
                    String approvedAmount = obj.optString("ApprovedAmount");
                    String expDate = obj.optString("ExpDate");
                    String currentStatus = obj.optString("CurrentStatus");
                    String currentStatusId = obj.optString("CurrentStatusID");
                    String submittedByName = obj.optString("SubmittedByName");

                    ExpenseModel expenseModel = new ExpenseModel(expId, expCdoe, requestFor, expSubject,
                            actualAmount, approvedAmount, expDate, currentStatus, currentStatusId, submittedByName);
                    arrFFs.add(expenseModel);
                }
            }
            expensesResponse.setExpenses(arrFFs);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return expensesResponse;
    }

    public ArrayList<SODashboardModel> getSODashboard(JSONArray array) {

        ArrayList<SODashboardModel> dashboardModelArrayList = new ArrayList<>();
        SODashboardModel model;
        try {
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String count = object.optString("count");
                    String status = object.optString("status");
                    String total = object.optString("total");
                    String percentage = object.optString("percentage").equalsIgnoreCase("null") ? "0" : object.optString("percentage");

                    model = new SODashboardModel();
                    model.setCount(count);
                    model.setStatus(status);
                    model.setTotal(total);
                    model.setPercentage(percentage);

                    dashboardModelArrayList.add(model);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return dashboardModelArrayList;
    }

    public String[][] getSODashboardNew(JSONArray array) {

        String[][] arr = new String[][]{};
        try {
            if (array != null && array.length() > 0) {
                String[][] _arr = new String[array.length() + 1][4];
                _arr[0][0] = "Status";
                _arr[0][1] = "Count";
                _arr[0][2] = "Total";
                _arr[0][3] = "Percentage";
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String count = object.optString("count");
                    String status = object.optString("status");
                    String total = object.optString("total");
                    String percentage = object.optString("percentage").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("percentage")));

                    _arr[i + 1][0] = status;
                    _arr[i + 1][1] = count;
                    _arr[i + 1][2] = total;
                    _arr[i + 1][3] = percentage;

                }
                arr = _arr;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return arr;
    }

    public ArrayList<SODashboardModel> getSODashboardPicker(JSONArray array) {

        ArrayList<SODashboardModel> dashboardModelArrayList = new ArrayList<>();
        SODashboardModel model;
        try {
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String pickername = object.optString("pickername");
                    String pickerId = object.optString("picker_id");
                    String readyToDispatch = object.optString("ready_to_dispatch");
                    String assigned = object.optString("assigned");
                    String percentCompleted = object.optString("percent_completed");
                    String contribution = object.optString("contribution").equalsIgnoreCase("null") ? "0" : object.optString("contribution");

                    model = new SODashboardModel();

                    model.setPickername(pickername);
                    model.setPickerId(pickerId);
                    model.setReadyToDispatch(readyToDispatch);
                    model.setAssigned(assigned);
                    model.setPercentCompleted(percentCompleted);
                    model.setContribution(contribution);

                    dashboardModelArrayList.add(model);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return dashboardModelArrayList;
    }

    public String[][] getSODashboardPickerNew(JSONArray array) {

        String[][] arr = new String[][]{};
        try {
            if (array != null && array.length() > 0) {
                String[][] _arr = new String[array.length() + 1][6];
                _arr[0][0] = "Name";
                _arr[0][1] = "Assigned";
                _arr[0][2] = "Ready To Dispatch";
                _arr[0][3] = "Pending";
                _arr[0][4] = "Per Completed";
                _arr[0][5] = "Contribution";

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String pickername = object.optString("pickername");
                    String pickerId = object.optString("picker_id");
                    String readyToDispatch = object.optString("ready_to_dispatch");
                    String assigned = object.optString("assigned");
                    String pending = object.optString("pending");
                    String lineItems = object.optString("line_items");
                    String prLineItems = object.optString("pr_line_items");
                    String percentCompleted = object.optString("percent_completed");
                    String contribution = object.optString("contribution").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("contribution")));

                    _arr[i + 1][0] = pickername;
                    _arr[i + 1][1] = assigned;
                    _arr[i + 1][2] = readyToDispatch;
                    _arr[i + 1][3] = pending;
                    _arr[i + 1][4] = percentCompleted;
                    _arr[i + 1][5] = contribution;

                }
                arr = _arr;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return arr;
    }

    public String[][] getSODashboardDeliveryNew(JSONArray array) {

        String[][] arr = new String[][]{};
        try {
            if (array != null && array.length() > 0) {
                String[][] _arr = new String[array.length() + 1][14];
                _arr[0][0] = "DeliverName";
                _arr[0][1] = "Delivered";
                _arr[0][2] = "PartialDelivered";
                _arr[0][3] = "Completed";
                _arr[0][4] = "Assigned";
                _arr[0][5] = "Hold";
                _arr[0][6] = "Returned";
                _arr[0][7] = "Pending";
                _arr[0][8] = "Out for Delivery";
                _arr[0][9] = "Success Rate";
                _arr[0][10] = "Contribution";
                _arr[0][11] = "Order Value";
                _arr[0][12] = "Invoice Value";
                _arr[0][13] = "Return Value";

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String delivername = object.optString("delivername");
                    String deliveredBy = object.optString("delivered_by");
                    String delivered = object.optString("delivered");
                    String partialDelivered = object.optString("partial_delivered");
                    String completed = object.optString("completed");
                    String assigned = object.optString("assigned");
                    String hold = object.optString("hold");
                    String returned = object.optString("returned");
                    String pending = object.optString("pending");
                    String outForDelivery = object.optString("out_for_delivery");
                    String successRate = object.optString("success_rate").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("success_rate")));
                    String contribution = object.optString("contribution").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("contribution")));
                    String orderValue = object.optString("ordervalue").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("ordervalue")));
                    String invoiceValue = object.optString("invoice_value").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("invoice_value")));
                    String returnValue = object.optString("return_value").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("return_value")));

                    _arr[i + 1][0] = delivername;
                    _arr[i + 1][1] = delivered;
                    _arr[i + 1][2] = partialDelivered;
                    _arr[i + 1][3] = completed;
                    _arr[i + 1][4] = assigned;
                    _arr[i + 1][5] = hold;
                    _arr[i + 1][6] = returned;
                    _arr[i + 1][7] = pending;
                    _arr[i + 1][8] = outForDelivery;
                    _arr[i + 1][9] = successRate;
                    _arr[i + 1][10] = contribution;
                    _arr[i + 1][11] = orderValue;
                    _arr[i + 1][12] = invoiceValue;
                    _arr[i + 1][13] = returnValue;

                }
                arr = _arr;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return arr;
    }

    public ArrayList<SODashboardModel> getSODashboardDelivery(JSONArray array) {

        ArrayList<SODashboardModel> dashboardModelArrayList = new ArrayList<>();
        SODashboardModel model;
        try {
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String delivername = object.optString("delivername");
                    String assigned = object.optString("assigned");
                    String delivered = object.optString("delivered");
                    String hold = object.optString("hold");
                    String returned = object.optString("returned");
                    String deliverySucessRate = object.optString("success_rate");
                    String contribution = object.optString("contribution").equalsIgnoreCase("null") ? "0" : object.optString("contribution");

                    model = new SODashboardModel();

                    model.setDeliverName(delivername);
                    model.setAssigned(assigned);
                    model.setDelivered(delivered);
                    model.setHold(hold);
                    model.setReturned(returned);
                    model.setDeliverySuccessRate(deliverySucessRate);
                    model.setContribution(contribution);

                    dashboardModelArrayList.add(model);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return dashboardModelArrayList;
    }

    public String[][] getSOReturnReasonsNew(JSONArray array) {

        String[][] arr = new String[][]{};
        try {
            if (array != null && array.length() > 0) {
                String[][] _arr = new String[array.length() + 1][15];
                _arr[0][0] = "Delivered By";
                _arr[0][1] = "ONPR";
                _arr[0][2] = "WOP";
                _arr[0][3] = "WAD";
                _arr[0][4] = "NC";
                _arr[0][5] = "Credit";
                _arr[0][6] = "SS";
                _arr[0][7] = "Delay";
                _arr[0][8] = "QATC";
                _arr[0][9] = "INVMIS";
                _arr[0][10] = "PFD";
                _arr[0][11] = "MRP";
                _arr[0][12] = "ESP";
                _arr[0][13] = "MultipleAtt";
                _arr[0][14] = "Others";

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.optJSONObject(i);

                    String deliveredBy = object.optString("DeliveredBy");
                    String deliveredByID = object.optString("delivered_by");
                    String onpr = object.optString("ONPR").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("ONPR")));
                    String wop = object.optString("WOP").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("WOP")));
                    String wad = object.optString("WAD").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("WAD")));
                    String nc = object.optString("NC").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("NC")));
                    String credit = object.optString("Credit").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("Credit")));
                    String ss = object.optString("SS").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("SS")));
                    String delay = object.optString("Delay").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("Delay")));
                    String qatc = object.optString("QATC").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("QATC")));
                    String invmis = object.optString("INVMIS").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("INVMIS")));
                    String pfd = object.optString("PFD").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("PFD")));
                    String mrp = object.optString("MRP").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("MRP")));
                    String esp = object.optString("ESP").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("ESP")));
                    String multipleAtt = object.optString("MultipleAtt").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("MultipleAtt")));
                    String others = object.optString("Others").equalsIgnoreCase("null") ? "0.00" : String.format("%.2f", Float.parseFloat(object.optString("Others")));

                    _arr[i + 1][0] = deliveredBy;
                    _arr[i + 1][1] = onpr;
                    _arr[i + 1][2] = wop;
                    _arr[i + 1][3] = wad;
                    _arr[i + 1][4] = nc;
                    _arr[i + 1][5] = credit;
                    _arr[i + 1][6] = ss;
                    _arr[i + 1][7] = delay;
                    _arr[i + 1][8] = qatc;
                    _arr[i + 1][9] = invmis;
                    _arr[i + 1][10] = pfd;
                    _arr[i + 1][11] = mrp;
                    _arr[i + 1][12] = esp;
                    _arr[i + 1][13] = multipleAtt;
                    _arr[i + 1][14] = others;

                }
                arr = _arr;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return arr;
    }

    public ArrayList<ExpenseDetailsModel> getExpenseDetails(JSONObject jsonObject) {
        ArrayList<ExpenseDetailsModel> arrFFs = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null & dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {

                    JSONObject obj = dataArray.optJSONObject(i);

                    String expDetId = obj.optString("exp_det_id");
                    String detActualAmount = obj.optString("DetActualAmount");
                    String detApprovedAmount = obj.optString("DetApprovedAmount");
                    String detExpType = obj.optString("DetExpType");
                    String detDescription = obj.optString("DetDescription");
                    String attachedFiles = obj.optString("AttachedFiles");

                    ExpenseDetailsModel expenseModel = new ExpenseDetailsModel(expDetId, detActualAmount, detApprovedAmount, detExpType, detDescription, attachedFiles);
                    arrFFs.add(expenseModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrFFs;
    }

    public ExpenseMasterlookupModel getExpenseMasterLookupData(JSONObject jsonObject) {
        ExpenseMasterlookupModel model = new ExpenseMasterlookupModel();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");

            JSONArray requestTypesArray = dataObj.optJSONArray("RequestType");
            JSONArray expTypesArray = dataObj.optJSONArray("ExpType");
            JSONArray requestTypeForArray = dataObj.optJSONArray("RequestTypeFor");

            ArrayList<ExpenseMasterLookupData> requestTypes = new ArrayList<>();
            if (requestTypesArray != null && requestTypesArray.length() > 0) {

                for (int i = 0; i < requestTypesArray.length(); i++) {
                    JSONObject obj = requestTypesArray.optJSONObject(i);
                    String masterLookupName = obj.optString("master_lookup_name");
                    String value = obj.optString("value");
                    ExpenseMasterLookupData data = new ExpenseMasterLookupData();
                    data.setMasterLookupName(masterLookupName);
                    data.setValue(value);
                    requestTypes.add(data);
                }
            }

            model.setRequestTypes(requestTypes);

            ArrayList<ExpenseMasterLookupData> expenseTypes = new ArrayList<>();
            if (expTypesArray != null && expTypesArray.length() > 0) {
                for (int i = 0; i < expTypesArray.length(); i++) {
                    JSONObject obj = expTypesArray.optJSONObject(i);
                    String masterLookupName = obj.optString("master_lookup_name");
                    String value = obj.optString("value");
                    ExpenseMasterLookupData data = new ExpenseMasterLookupData();
                    data.setMasterLookupName(masterLookupName);
                    data.setValue(value);
                    if (i == 0) {
                        ExpenseMasterLookupData data1 = new ExpenseMasterLookupData();
                        data1.setMasterLookupName("Please select");
                        data1.setValue("");
                        expenseTypes.add(data1);
                    }
                    expenseTypes.add(data);
                }
            }
            model.setExpenseTypes(expenseTypes);

            ArrayList<ExpenseMasterLookupData> requestForTypes = new ArrayList<>();
            if (requestTypeForArray != null && requestTypeForArray.length() > 0) {
                for (int i = 0; i < requestTypeForArray.length(); i++) {
                    JSONObject obj = requestTypeForArray.optJSONObject(i);
                    String masterLookupName = obj.optString("master_lookup_name");
                    String value = obj.optString("value");
                    ExpenseMasterLookupData data = new ExpenseMasterLookupData();
                    data.setMasterLookupName(masterLookupName);
                    data.setValue(value);
                    requestForTypes.add(data);
                }
            }
            model.setRequestForTypes(requestForTypes);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    public ApprovalResponseModel getApprovalDataResponse(JSONObject object) {
        ApprovalResponseModel model = new ApprovalResponseModel();

        String currentStatusName = object.optString("currentStatusName");
        String currentStatusId = object.optString("currentStatusId");

        JSONArray dataArray = object.optJSONArray("data");
        ArrayList<ApprovalDataModel> approvalDataModels = new ArrayList<>();
        if (dataArray != null && dataArray.length() > 0) {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.optJSONObject(i);
                String conditionId = obj.optString("conditionId");
                String condition = obj.optString("condition");
                String nextStatusId = obj.optString("nextStatusId");
                String nextStatus = obj.optString("nextStatus");
                String isFinalStep = obj.optString("isFinalStep");

                ApprovalDataModel approvalDataModel = new ApprovalDataModel();
                approvalDataModel.setCondition(condition);
                approvalDataModel.setConditionId(conditionId);
                approvalDataModel.setIsFinalStep(isFinalStep);
                approvalDataModel.setNextStatus(nextStatus);
                approvalDataModel.setNextStatusId(nextStatusId);
                approvalDataModels.add(approvalDataModel);
            }

            model.setApprovalDataModels(approvalDataModels);
            model.setCurrentStatusId(currentStatusId);
            model.setCurrentStatusName(currentStatusName);
        }

        return model;

    }

    public ApprovalHistoryResponseModel getApprovalHistoryResponse(JSONObject object) {
        ApprovalHistoryResponseModel model = new ApprovalHistoryResponseModel();

        String code = object.optString("code");

        JSONArray dataArray = object.optJSONArray("data");
        ArrayList<ApprovalHistoryModel> approvalHistoryModels = new ArrayList<>();
        if (dataArray != null && dataArray.length() > 0) {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.optJSONObject(i);
                String awfHistoryId = obj.optString("awf_history_id");
                String profilePicture = obj.optString("profile_picture");
                String firstname = obj.optString("firstname");
                String lastname = obj.optString("lastname");
                String name = obj.optString("name");
                String createdAt = obj.optString("created_at");
                String statusToId = obj.optString("status_to_id");
                String statusFromId = obj.optString("status_from_id");
                String awfComment = obj.optString("awf_comment");
                String masterLookupName = obj.optString("master_lookup_name");

                ApprovalHistoryModel approvalHistoryModel = new ApprovalHistoryModel();
                approvalHistoryModel.setAwf_history_id(awfHistoryId);
                approvalHistoryModel.setProfile_picture(profilePicture);
                approvalHistoryModel.setFirstname(firstname);
                approvalHistoryModel.setLastname(lastname);
                approvalHistoryModel.setName(name);
                approvalHistoryModel.setCreated_at(createdAt);
                approvalHistoryModel.setStatus_to_id(statusToId);
                approvalHistoryModel.setStatus_from_id(statusFromId);
                approvalHistoryModel.setAwf_comment(awfComment);
                approvalHistoryModel.setMaster_lookup_name(masterLookupName);
                approvalHistoryModels.add(approvalHistoryModel);
            }

            model.setCode(code);
            model.setApprovalHistoryModelArrayList(approvalHistoryModels);
        }

        return model;

    }

    public CustomerTyepModel updateBeat(JSONObject jsonObject) {

        CustomerTyepModel customerTyepModel = new CustomerTyepModel();

        try {

            JSONObject dataObj = jsonObject.optJSONObject("data");
            if (dataObj != null) {
                String hubId = dataObj.optString("hub_id");
                String leWhId = dataObj.optString("le_wh_id");

                customerTyepModel.setHub(hubId);
                customerTyepModel.setWhId(leWhId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customerTyepModel;
    }

    public ArrayList<TestProductModel> getAllManufacturerProducts(JSONObject jsonObject) {
        ArrayList<TestProductModel> arrProducts = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataObj = dataArray.optJSONObject(i);
                    if (dataObj != null) {
                        TestProductModel testProductModel = new TestProductModel();

                        String productId = dataObj.optString("product_id");
                        String productTitle = dataObj.optString("product_title");
                        String sku = dataObj.optString("sku");
                        String primaryImage = dataObj.optString("primary_image");
                        String flag = dataObj.optString("flag");

                        testProductModel.setProductId(productId);
                        testProductModel.setProductTitle(productTitle);
                        testProductModel.setSKUCode(sku);
                        testProductModel.setPrimaryImage(primaryImage);
                        testProductModel.setFlag(flag);
                        testProductModel.setChecked(flag.equalsIgnoreCase("1") ? true : false);

                        arrProducts.add(testProductModel);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrProducts;
    }

    public String[][] getBeatsData(JSONObject jsonObject) {
        String[][] array = new String[][]{};
        try {
            JSONArray beatsArr = jsonObject.optJSONArray("data");
            if (beatsArr != null && beatsArr.length() > 0) {
                String[][] _arr = new String[beatsArr.length() + 1][9];
                _arr[0][0] = "Beat Name";
                _arr[0][1] = "Reporting Manager";
                _arr[0][2] = "Days";
                _arr[0][3] = "No of Beats";
                _arr[0][4] = "Map";
                _arr[0][5] = "Beat Id";
                _arr[0][6] = "RM Id";
                _arr[0][7] = "Hub Id";
                _arr[0][8] = "DC Id";

                for (int i = 0; i < beatsArr.length(); i++) {
                    JSONObject object = beatsArr.optJSONObject(i);

                    String pjpName = object.optString("pjp_name");
                    String rmName = object.optString("rm_name");
                    String days = object.optString("days");
                    String totalOutlet = object.optString("total_outlet");
                    String pjpPincodeAreaId = object.optString("pjp_pincode_area_id");
                    String rmId = object.optString("rm_id");
                    String hubId = object.optString("hub_id");
                    String dcId = object.optString("dc_id");

                    _arr[i + 1][0] = pjpName;
                    _arr[i + 1][1] = rmName;
                    _arr[i + 1][2] = days;
                    _arr[i + 1][3] = totalOutlet;
                    _arr[i + 1][4] = "Map";
                    _arr[i + 1][5] = pjpPincodeAreaId;
                    _arr[i + 1][6] = rmId;
                    _arr[i + 1][7] = hubId;
                    _arr[i + 1][8] = dcId;

                }
                array = _arr;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;
    }

    public ArrayList<CustomerTyepModel> getAllData(JSONObject jsonObject) {
        ArrayList<CustomerTyepModel> arrData = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    JSONObject dataObj = dataArray.optJSONObject(i);
                    if (dataObj != null) {
                        String leWhId = dataObj.optString("id");
                        String lpWhName = dataObj.optString("name");
                        String pincode = dataObj.optString("pincode");

                        customerTyepModel.setCustomerGrpId(leWhId);
                        customerTyepModel.setCustomerName(lpWhName);
                        customerTyepModel.setPostcode(pincode.equalsIgnoreCase("null") ? "" : pincode);
                    }

                    arrData.add(customerTyepModel);

                    Collections.sort(arrData, new Comparator<CustomerTyepModel>() {
                        public int compare(CustomerTyepModel c1, CustomerTyepModel c2) {
                            return c1.getCustomerName().compareToIgnoreCase(c2.getCustomerName());
                        }
                    });
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrData;
    }

    public ArrayList<CustomerTyepModel> getRMList(JSONObject jsonObject) {
        ArrayList<CustomerTyepModel> arrFFs = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null & dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    JSONObject ffObj = dataArray.optJSONObject(i);

                    String userId = ffObj.optString("user_id");
                    String name = ffObj.optString("firstname") + " " + ffObj.optString("lastname");

                    customerTyepModel.setCustomerGrpId(userId);
                    customerTyepModel.setCustomerName(name);

                    arrFFs.add(customerTyepModel);
                }

                Collections.sort(arrFFs, new Comparator<CustomerTyepModel>() {
                    public int compare(CustomerTyepModel c1, CustomerTyepModel c2) {
                        return c1.getCustomerName().compareToIgnoreCase(c2.getCustomerName());
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrFFs;
    }

    public CustomerTyepModel addSpoke(JSONObject jsonObject) {
        CustomerTyepModel customerTyepModel = new CustomerTyepModel();
        try {

            JSONObject dataObject = jsonObject.optJSONObject("data");
            if (dataObject != null) {
                String customerToken = dataObject.optString("customer_token");
                String userId = dataObject.optString("user_id");
                String pincode = dataObject.optString("pincode");
                String name = dataObject.optString("name");
                String id = dataObject.optString("id");
                String hubId = dataObject.optString("le_wh_id");

                customerTyepModel.setCustomerToken(customerToken);
                customerTyepModel.setUserId(userId);
                customerTyepModel.setPostcode(pincode);
                customerTyepModel.setCustomerName(name);
                customerTyepModel.setCustomerGrpId(id);
                customerTyepModel.setHub(hubId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customerTyepModel;
    }

    public ArrayList<BeatModel> getManageBeats(JSONObject jsonObject) {
        ArrayList<BeatModel> arrBeats = new ArrayList<>();
        try {

            JSONArray beatsArr = jsonObject.optJSONArray("data");
            if (beatsArr != null && beatsArr.length() > 0) {
                for (int i = 0; i < beatsArr.length(); i++) {
                    JSONObject object = beatsArr.optJSONObject(i);
                    BeatModel beatModel = new BeatModel();

                    String pjpName = object.optString("pjp_name");
                    String rmName = object.optString("rm_name");
                    String days = object.optString("days");
                    String totalOutlet = object.optString("total_outlet");
                    String pjpPincodeAreaId = object.optString("pjp_pincode_area_id");
                    String rmId = object.optString("rm_id");
                    String hubId = object.optString("hub_id");
                    String dcId = object.optString("dc_id");

                    beatModel.setBeatName(pjpName);
                    beatModel.setRmName(rmName);
                    beatModel.setDay(days);
                    beatModel.setTotalOutlets(totalOutlet);
                    beatModel.setBeatId(pjpPincodeAreaId);
                    beatModel.setRmId(rmId);
                    beatModel.setHubId(hubId);
                    beatModel.setDcId(dcId);

                    arrBeats.add(beatModel);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrBeats;
    }

    public OrdersModel getPendingPOs(JSONObject jsonObject) {
        OrdersModel _ordersModel = new OrdersModel();
        int count = 0;
        ArrayList<OrdersModel> ordersList = new ArrayList<OrdersModel>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            count = jsonObject.optInt("totcount");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    OrdersModel ordersModel = new OrdersModel();
                    JSONObject ordersObj = dataArray.optJSONObject(i);
                    ordersModel.setOrderNumber(ordersObj.optString("po_id"));
                    ordersModel.setOrderCode(ordersObj.optString("po_code"));
                    ordersModel.setDate(ordersObj.optString("po_date"));
                    ordersModel.setSupplierName(ordersObj.optString("supplier"));
                    ordersModel.setSupplierCode(ordersObj.optString("supplier_code"));
                    ordersModel.setTotalAmount(ordersObj.optString("po_total"));
                    ordersModel.setOrderStatus(ordersObj.optString("po_status"));
                    ordersModel.setApprovalStatusId(ordersObj.optString("approval_status"));
                    ordersModel.setApprovalStatus(ordersObj.optString("approvalStatus"));
                    ordersModel.setApprovedBy(ordersObj.optString("approved_by"));
                    ordersModel.setApprovedAt(ordersObj.optString("approved_at"));
                    ordersModel.setRoles(ordersObj.optString("Roles"));
                    ordersList.add(ordersModel);
                }
                _ordersModel.setArrOrders(ordersList);
            }
            _ordersModel.setCount(count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _ordersModel;
    }

    public ApprovalOptionsModel getPOApprovalOptions(JSONObject jsonObject) {
        ApprovalOptionsModel approvalOptionsModel = new ApprovalOptionsModel();
        try {
            JSONObject approvalValObj = jsonObject.optJSONObject("approvalVal");
            if (approvalValObj != null) {
                String currentStatus = approvalValObj.optString("current_status");
                String approvalUniqueId = approvalValObj.optString("approval_unique_id");
                String approvalModule = approvalValObj.optString("approval_module");
                String tableName = approvalValObj.optString("table_name");
                String uniqueColumn = approvalValObj.optString("unique_column");

                approvalOptionsModel.setCurrentStatus(currentStatus);
                approvalOptionsModel.setApprovalUniqueId(approvalUniqueId);
                approvalOptionsModel.setApprovalModule(approvalModule);
                approvalOptionsModel.setTableName(tableName);
                approvalOptionsModel.setUniqueColumn(uniqueColumn);
            }

            JSONArray approvalOptionsArr = jsonObject.optJSONArray("approvalOptions");
            if (approvalOptionsArr != null && approvalOptionsArr.length() > 0) {
                ArrayList<ApprovalDataModel> arrApprovalOptions = new ArrayList<>();
                for (int i = 0; i < approvalOptionsArr.length(); i++) {
                    ApprovalDataModel approvalDataModel = new ApprovalDataModel();
                    JSONObject approvalOptionsObj = approvalOptionsArr.optJSONObject(i);
                    if (approvalOptionsObj != null) {
                        String nextStatusId = approvalOptionsObj.optString("nextStatusId");
                        String isFinalStep = approvalOptionsObj.optString("isFinalStep");
                        String condition = approvalOptionsObj.optString("condition");

                        approvalDataModel.setNextStatusId(nextStatusId);
                        approvalDataModel.setIsFinalStep(isFinalStep);
                        approvalDataModel.setCondition(condition);
                    }

                    if (i == 0) {
                        ApprovalDataModel approvalDataModel1 = new ApprovalDataModel();
                        approvalDataModel1.setNextStatusId("");
                        approvalDataModel1.setIsFinalStep("");
                        approvalDataModel1.setCondition("Please Select");
                        arrApprovalOptions.add(approvalDataModel1);

                    }

                    arrApprovalOptions.add(approvalDataModel);
                }
                approvalOptionsModel.setArrApprovalOptions(arrApprovalOptions);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return approvalOptionsModel;
    }

    public ArrayList<TalleyModel> getTallyLedgerData(JSONArray dataArray) {
        ArrayList<TalleyModel> list = new ArrayList<>();

        if (dataArray != null && dataArray.length() > 0) {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject obj = dataArray.optJSONObject(i);
                String tlm_name = obj.optString("tlm_name");
                String tlm_group = obj.optString("tlm_group");
                String tlm_id = obj.optString("tlm_id");

                if (i == 0) {
                    TalleyModel _model = new TalleyModel("Select", "", "");
                    list.add(_model);
                }
                TalleyModel model = new TalleyModel(tlm_name, tlm_group, tlm_id);
                list.add(model);
            }
        }

        return list;

    }

    public ArrayList<ExpenseModel> getAllExpenses(JSONObject jsonObject) {
        ArrayList<ExpenseModel> arrExpenses = new ArrayList<>();
        try {

            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataObj = dataArray.optJSONObject(i);
                    ExpenseModel expenseModel = new ExpenseModel();
                    if (dataObj != null) {
                        String expId = dataObj.optString("exp_id");
                        String expCode = dataObj.optString("exp_code");
                        String expReqType = dataObj.optString("exp_req_type");
                        String expReqTypeForId = dataObj.optString("exp_req_type_for_id");
                        String expSubject = dataObj.optString("exp_subject");
                        String expActualAmount = dataObj.optString("exp_actual_amount");
                        String tallyLedgerName = dataObj.optString("tally_ledger_name");
                        String expApprovedAmount = dataObj.optString("exp_approved_amount");
                        String submitDate = dataObj.optString("submit_date");
                        String expReffId = dataObj.optString("exp_reff_id");
                        String submitedById = dataObj.optString("submited_by_id");
                        String expApprStatus = dataObj.optString("exp_appr_status");
                        boolean isActive = dataObj.optInt("is_active") == 1;

                        expenseModel.setExpenseId(expId);
                        expenseModel.setExpenseCode(expCode);
                        expenseModel.setExpenseReqType(expReqType);
                        expenseModel.setExpenseReqTypeForId(expReqTypeForId);
                        expenseModel.setExpenseSubject(expSubject);
                        expenseModel.setActualAmount(expActualAmount);
                        expenseModel.setTallyLedgerName(tallyLedgerName);
                        expenseModel.setApprovedAmount(expApprovedAmount);
                        expenseModel.setExpenseDate(submitDate);
                        expenseModel.setExpenseReffId(expReffId);
                        expenseModel.setApprovalStatus(expApprStatus);
                        expenseModel.setActive(isActive);

                        arrExpenses.add(expenseModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrExpenses;
    }

    public ArrayList<StatusModel> getPOStatusList(JSONObject jsonObject) {
        ArrayList<StatusModel> arrStatus = new ArrayList<>();
        try {
            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataObj = dataArray.optJSONObject(i);
                    StatusModel statusModel = new StatusModel();
                    if (dataObj != null) {
                        String name = dataObj.optString("name");
                        String code = dataObj.optString("code");
                        String count = dataObj.optString("count");

                        statusModel.setName(name);
                        statusModel.setCode(code);
                        statusModel.setCount(count);

                        arrStatus.add(statusModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrStatus;
    }

    public ArrayList<CustomerTyepModel> getFilterOrderStatus(JSONObject jsonObject) {
        ArrayList<CustomerTyepModel> arrFilterStatus = new ArrayList<>();
        try {

            JSONArray dataArray = jsonObject.optJSONArray("data");
            if (dataArray != null && dataArray.length() > 0) {
                for (int i = 0; i < dataArray.length(); i++) {
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    JSONObject dataObj = dataArray.optJSONObject(i);
                    if (dataObj != null) {
                        String value = dataObj.optString("value");
                        String masterLookupName = dataObj.optString("master_lookup_name");

                        customerTyepModel.setCustomerGrpId(value);
                        customerTyepModel.setCustomerName(masterLookupName);

                        arrFilterStatus.add(customerTyepModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrFilterStatus;

    }

    public ArrayList<SpecificationsModel> getOrderData(JSONObject jsonObject) {
        ArrayList<SpecificationsModel> arrData = new ArrayList<>();
        try {
            JSONObject dataObj = jsonObject.optJSONObject("data");
            Iterator<String> keySet = dataObj.keys();
            while ((keySet.hasNext())) {
                String key = keySet.next();
                if (!key.equalsIgnoreCase("-1")) {
                    SpecificationsModel specificationsModel = new SpecificationsModel();
                    String specValue = dataObj.optString(key);
                    key = key.replace("_", " ");
                    specValue = specValue.replace("_", " ");
                    specificationsModel.setSpecName(key);
                    specificationsModel.setSpecValue(specValue);
                    if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(specValue))
                        arrData.add(specificationsModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrData;
    }

    public CrateModel getCrateData(JSONObject jsonObject) {
        CrateModel crateModel = new CrateModel();
        try {
            JSONObject responseBodyObj = jsonObject.optJSONObject("ResponseBody");
            if (responseBodyObj != null) {
                String gdsOrderId = responseBodyObj.optString("gds_order_id");
                String orderCode = responseBodyObj.optString("order_code");
                String orderDate = responseBodyObj.optString("order_date");
                String whName = responseBodyObj.optString("wh_name");
                String hubName = responseBodyObj.optString("hub_name");
                String orderStatus = responseBodyObj.optString("order_status");
                String soName = responseBodyObj.optString("so_name");
                String total = responseBodyObj.optString("total");
                String shopName = responseBodyObj.optString("shop_name");
                String beat = responseBodyObj.optString("beat");
                String invoiceCode = responseBodyObj.optString("invoice_code").equalsIgnoreCase("null") ? "" : responseBodyObj.optString("invoice_code");
                String grandTotal = responseBodyObj.optString("grand_total").equalsIgnoreCase("null") ? "" : responseBodyObj.optString("grand_total");
                String createdAt = responseBodyObj.optString("created_at").equalsIgnoreCase("null") ? "" : responseBodyObj.optString("created_at");
                String createdBy = responseBodyObj.optString("created_by").equalsIgnoreCase("null") ? "" : responseBodyObj.optString("created_by");
                String pickedBy = responseBodyObj.optString("picked_by").equalsIgnoreCase("null") ? "" : responseBodyObj.optString("picked_by");
                String pickerId = responseBodyObj.optString("picker_id").equalsIgnoreCase("null") ? "" : responseBodyObj.optString("picker_id");
                String pickedAt = responseBodyObj.optString("picked_at").equalsIgnoreCase("null") ? "" : responseBodyObj.optString("picked_at");
                int crate = responseBodyObj.optInt("Crate");
                int cfc = responseBodyObj.optInt("Cfc");
                int bag = responseBodyObj.optInt("Bag");

                JSONArray containersArr = responseBodyObj.optJSONArray("containers");
                if (containersArr != null && containersArr.length() > 0) {
                    ArrayList<ContainerModel> arrContainers = new ArrayList<>();
                    for (int i = 0; i < containersArr.length(); i++) {
                        ContainerModel containerModel = new ContainerModel();
                        JSONObject containerObj = containersArr.optJSONObject(i);
                        if (containerObj != null) {
                            String containerBarcode = containerObj.optString("container_barcode");
                            String containerType = containerObj.optString("container_type");
                            String status = containerObj.optString("status");
                            String weight = containerObj.optString("weight");
                            String isVerified = containerObj.optString("is_verified");

                            JSONArray productsArr = containerObj.optJSONArray("products");
                            if (productsArr != null && productsArr.length() > 0) {
                                ArrayList<TestProductModel> arrProducts = new ArrayList<>();
                                for (int j = 0; j < productsArr.length(); j++) {
                                    TestProductModel testProductModel = new TestProductModel();
                                    JSONObject productObj = productsArr.optJSONObject(j);
                                    if (productObj != null) {
                                        String productTitle = productObj.optString("product_title");
                                        String productId = productObj.optString("product_id");
                                        String mrp = productObj.optString("mrp");
                                        String pickedQty = productObj.optString("picked_qty");

                                        testProductModel.setProductTitle(productTitle);
                                        testProductModel.setProductId(productId);
                                        testProductModel.setMrp(mrp);
                                        testProductModel.setPickedQty(pickedQty);

                                        arrProducts.add(testProductModel);
                                    }
                                }

                                containerModel.setArrProducts(arrProducts);
                            }

                            containerModel.setContainerBarcode(containerBarcode);
                            containerModel.setContainerType(containerType);
                            containerModel.setStatus(status);
                            containerModel.setWeight(weight);
                            containerModel.setIsVerified(isVerified);

                            arrContainers.add(containerModel);
                        }
                    }
                    crateModel.setArrContainers(arrContainers);
                }

                crateModel.setGdsOrderId(gdsOrderId);
                crateModel.setOrderCode(orderCode);
                crateModel.setOrderDate(orderDate);
                crateModel.setWhName(whName);
                crateModel.setHubName(hubName);
                crateModel.setOrderStatus(orderStatus);
                crateModel.setSoName(soName);
                crateModel.setTotal(total);
                crateModel.setShopName(shopName);
                crateModel.setBeat(beat);
                crateModel.setInvoiceCode(invoiceCode);
                crateModel.setGrandTotal(grandTotal);
                crateModel.setCreatedAt(createdAt);
                crateModel.setCreatedBy(createdBy);
                crateModel.setPickedBy(pickedBy);
                crateModel.setPickerId(pickerId);
                crateModel.setPickedAt(pickedAt);
                crateModel.setCrate(crate);
                crateModel.setCfc(cfc);
                crateModel.setBag(bag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return crateModel;
    }

    public NotificationModel getNotifications(JSONObject jsonObject) {
        NotificationModel notificationModel = new NotificationModel();
        try {
            int count = jsonObject.optInt("count");
            notificationModel.setCount(count);
            JSONArray dataArr = jsonObject.optJSONArray("data");
            if (dataArr != null && dataArr.length() > 0) {
                ArrayList<NotificationModel> arrData = new ArrayList<>();
                for (int i = 0; i < dataArr.length(); i++) {
                    JSONObject dataObj = dataArr.optJSONObject(i);
                    NotificationModel _notificationModel = new NotificationModel();
                    if (dataObj != null) {
                        String id = dataObj.optString("id");
                        String referenceId = dataObj.optString("reference_id");
                        String message = dataObj.optString("message");
                        String createdOn = dataObj.optString("created_on");

                        _notificationModel.setId(id);
                        _notificationModel.setReferenceId(referenceId);
                        _notificationModel.setMessage(message);
                        _notificationModel.setDate(createdOn);

                        arrData.add(_notificationModel);

                    }
                }
                notificationModel.setArrNotifications(arrData);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return notificationModel;
    }

    public ArrayList<NewExpenseModel> getUnclaimedExpenses(JSONObject jsonObject) {
        ArrayList<NewExpenseModel> data = new ArrayList<>();

        JSONArray dataArray = jsonObject.optJSONArray("data");
        if (dataArray != null && dataArray.length() > 0) {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject object = dataArray.optJSONObject(i);

                String expDetId = object.optString("exp_det_id");
                String amount = object.optString("ExpDetAmount");
                String date = object.optString("ExpDetDate");
                String expenseType = object.optString("ExpDetType");
//                            model.setExpenseTypeId(cursor.getString(cursor.getColumnIndex(KEY_EXPENSE_TYPE_ID)));
                String desc = object.optString("Description");
                String attachments = object.optString("ProofImageKey");

                NewExpenseModel model = new NewExpenseModel(expDetId, amount, date, expenseType, "", desc, attachments);
                data.add(model);
            }

        }
        return data;
    }

    public MasterLookUpModel getMasterLookUpDiscounts(JSONObject jsonObject) {
        MasterLookUpModel model = new MasterLookUpModel();

        try {
            JSONObject dataObject = jsonObject.optJSONObject("data");

            if (dataObject != null && dataObject.length() > 0) {

                DBHelper.getInstance().deleteTable(DBHelper.TABLE_DISCOUNT);

                JSONArray discountsArray = dataObject.optJSONArray("discounts");
                if (discountsArray != null && discountsArray.length() > 0) {
                    for (int i = 0; i < discountsArray.length(); i++) {
                        JSONObject discountObj = discountsArray.optJSONObject(i);
                        if (discountObj != null) {
                            String discountType = discountObj.optString("discount_type");
                            String discountOn = discountObj.optString("discount_on");
                            String discountOnValues = discountObj.optString("discount_on_values");
                            String discount = discountObj.optString("discount");

                            ArrayList<String> arrDiscountValues = new ArrayList<>();

                            if (!TextUtils.isEmpty(discountOnValues))
                                arrDiscountValues = new ArrayList<String>(Arrays.asList(discountOnValues.split("\\s*,\\s*")));

                            if (arrDiscountValues != null && arrDiscountValues.size() > 0) {
                                for (int j = 0; j < arrDiscountValues.size(); j++) {
                                    DiscountModel discountModel = new DiscountModel();
                                    discountModel.setDiscountType(discountType);
                                    discountModel.setDiscountOn(discountOn);
                                    discountModel.setDiscountValues(arrDiscountValues.get(j));
                                    discountModel.setDiscount(discount);

                                    DBHelper.getInstance().insertDiscount(discountModel);

                                }
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return model;
    }


    public ArrayList<OrderLevelCashBackModel> getOrderLevelCashBack(JSONObject jsonObject) {

        ArrayList<OrderLevelCashBackModel> arrorderLevelCashBackModels = new ArrayList<>();
        JSONArray dataArray = jsonObject.optJSONArray("data");
        if (dataArray != null && dataArray.length() > 0) {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject object = dataArray.optJSONObject(i);
                int cashBackType = object.optInt("cashback_type");
                String cbkValue = ((object.optString("cbk_value").equals("null") || TextUtils.isEmpty(object.optString("cbk_value"))) ? "0" : object.optString("cbk_value"));
                double qtyFromRange = object.optDouble("qty_from_range");
                double qtytoRange = object.optDouble("qty_to_range");
                String cashBackDesc = object.optString("cashback_description");
                int cashBackId = object.optInt("cashback_id");
                String cbkSourceType = object.optString("cbk_source_type");
                int benficiaryType = object.optInt("benificiary_type");
                int productStar = object.optInt("product_star");

                OrderLevelCashBackModel orderLevelCashBackModel = new OrderLevelCashBackModel();
                orderLevelCashBackModel.setCashback_type(cashBackType);
                orderLevelCashBackModel.setCbk_value(cbkValue);
                orderLevelCashBackModel.setQty_from_range(qtyFromRange);
                orderLevelCashBackModel.setQty_to_range(qtytoRange);
                orderLevelCashBackModel.setCashback_description(cashBackDesc);
                orderLevelCashBackModel.setCashback_id(cashBackId);
                orderLevelCashBackModel.setCbk_source_type(cbkSourceType);
                orderLevelCashBackModel.setProduct_star(productStar);
                orderLevelCashBackModel.setBenificiary_type(benficiaryType);

                arrorderLevelCashBackModels.add(orderLevelCashBackModel);
            }
        }
        return arrorderLevelCashBackModels;
    }

    public ArrayList<CashBackHistoryModel> getCashBackHistory(JSONObject jsonObject) {
        ArrayList<CashBackHistoryModel> cashBackHistoryArrayList = new ArrayList<>();
        JSONArray dataArray = jsonObject.optJSONArray("data");
        if (dataArray != null && dataArray.length() > 0) {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject object = dataArray.optJSONObject(i);

                String order_code = object.optString("order_code");
                double cash_back_amount = object.optDouble("cash_back_amount");
                String cashback_type = object.optString("cashback_type");
                String transactionDate = object.optString("transaction_date");
                String orderId=object.optString("order_id");

                CashBackHistoryModel cashBackHistoryModel = new CashBackHistoryModel();
                cashBackHistoryModel.setOrdercode(order_code);
                cashBackHistoryModel.setCashbackamount(cash_back_amount);
                cashBackHistoryModel.setCashbacktype(cashback_type);
                cashBackHistoryModel.setTransactionDate(transactionDate);
                cashBackHistoryModel.setOrderId(orderId);

                cashBackHistoryArrayList.add(cashBackHistoryModel);
            }
        }

        return cashBackHistoryArrayList;
    }

}
