package com.ebutor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.ebutor.utils.ConstantValues;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by 300024 on 3/11/2016.
 */
public class RegisterSuccessActivity extends ParentActivity {
    TextView tvFactail, tvretailerName, tvShopName;
    private SharedPreferences mSharedPreferences;
    private Tracker mTracker;
    private boolean isServiceCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_success);

        tvretailerName = (TextView) findViewById(R.id.tv_retailer_name);
        tvShopName = (TextView) findViewById(R.id.tv_shop_name);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);
        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_INCOMPLETE_REG, false).apply();
        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        if (getIntent().hasExtra("RetailerName")) {
            String retailerName = getIntent().getStringExtra("RetailerName");
            String shopName = getIntent().getStringExtra("ShopName");
            isServiceCheck  = getIntent().getBooleanExtra("serviceCheck",false);
            tvretailerName.setText(retailerName);
            tvShopName.setText(shopName);
        }
        String changeString = "Fac";
        tvFactail = (TextView) findViewById(R.id.tv_factail);
       /* Spannable spanText = new SpannableString("Factail.com");
        spanText.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.primary)), 0, changeString.length(), 0);*/

//        tvFactail.setText(spanText);

//        View done = findViewById(R.id.iv_done);
//        if (done != null) {
//            done.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    startActivity(new Intent(RegisterSuccessActivity.this, LoginActivity.class));
//                    finish();
//                }
//            });
//        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(RegisterSuccessActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        }, 5000);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Registration Success Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }
}
