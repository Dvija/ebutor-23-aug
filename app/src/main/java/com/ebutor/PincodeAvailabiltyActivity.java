package com.ebutor;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CategoryModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PincodeAvailabiltyActivity extends ParentActivity implements Response.ErrorListener, VolleyHandler<Object> {

    public SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private String pincode = "", whIds = "";
    private LinearLayout llPinCode, llPinCodeNotAvailable;
    private TextView tvTryAgain;
    private Button btnVerify;
    private EditText etPin;
    private DBHelper databaseObj;
    private ArrayList<CategoryModel> arrCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_delivery_availability);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);
        databaseObj = new DBHelper(PincodeAvailabiltyActivity.this);

        llPinCode = (LinearLayout) findViewById(R.id.ll_pincode);
        llPinCodeNotAvailable = (LinearLayout) findViewById(R.id.ll_pincode_not_available);
        tvTryAgain = (TextView) findViewById(R.id.tv_try_again);
        btnVerify = (Button) findViewById(R.id.btnVerify);
        etPin = (EditText) findViewById(R.id.etPin);

        tvTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPin.setText("");
                llPinCode.setVisibility(View.VISIBLE);
                llPinCodeNotAvailable.setVisibility(View.GONE);
            }
        });

        dialog = Utils.createLoader(PincodeAvailabiltyActivity.this, ConstantValues.PROGRESS);

        if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {

            btnVerify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(etPin.getText().toString()))
                        Toast.makeText(PincodeAvailabiltyActivity.this, getResources().getString(R.string.please_enter_area_pin), Toast.LENGTH_SHORT).show();
                    else if (etPin.getText().length() != 6) {
                        Toast.makeText(PincodeAvailabiltyActivity.this, getResources().getString(R.string.please_enter_valid_area_pin), Toast.LENGTH_SHORT).show();
                    } else {
                        checkAvailability(etPin.getText().toString());
                    }
                }
            });
        }
    }

    private void checkAvailability(String pinCode) {
        if (Networking.isNetworkAvailable(PincodeAvailabiltyActivity.this)) {
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject jsonObject = new JSONObject();
                if (pinCode != null && pinCode.length() > 0) {
                    jsonObject.put("pincode", pinCode);
                }
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask tabsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.pincodeURL, map, PincodeAvailabiltyActivity.this, PincodeAvailabiltyActivity.this, PARSER_TYPE.PINCODE_AVAILABILITY);
                tabsRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(tabsRequest, AppURL.pincodeURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.no_network_connection));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if (error != null)
//            Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.PINCODE_AVAILABILITY) {
                    if (response instanceof String) {
                        whIds = (String) response;
                        if (whIds != null && !TextUtils.isEmpty(whIds) && !whIds.equals("0")) {
                            mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, whIds).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_LE_WH_IDS, whIds).apply();
                        }
                        Intent intent = new Intent(PincodeAvailabiltyActivity.this, SkipSignupActivity.class);
                        startActivity(intent);
                        finish();
//                        getCategories();

                    }
                } else if (requestType == PARSER_TYPE.GET_MAIN_CATEGORIES) {

                    if (status.equalsIgnoreCase("success")) {

                        databaseObj.deleteTable(DBHelper.TABLE_CATEGORY);

                        if (response instanceof ArrayList) {
                            arrCategories = (ArrayList<CategoryModel>) response;
                            for (int i = 0; i < arrCategories.size(); i++) {
                                databaseObj.insertCategory(arrCategories.get(i));
                            }
                        }
                        Log.e("Count 4 :", databaseObj.getCount() + "");

                    } else {
                        Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, message);
                    }
                }
            } else if (requestType == PARSER_TYPE.PINCODE_AVAILABILITY) {
                /*Intent intent = new Intent(PincodeAvailabiltyActivity.this,UnAvailablePincodeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);*/
                llPinCode.setVisibility(View.GONE);
                llPinCodeNotAvailable.setVisibility(View.VISIBLE);
            } else {
                Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, message);
            }
        } else {
            Utils.showAlertWithMessage(PincodeAvailabiltyActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    private void getCategories() {

        if (Networking.isNetworkAvailable(PincodeAvailabiltyActivity.this)) {
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                obj.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                map.put("data", obj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.categoriesURL, map, PincodeAvailabiltyActivity.this, PincodeAvailabiltyActivity.this, PARSER_TYPE.GET_MAIN_CATEGORIES);
            wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.categoriesURL);
            if (dialog != null)
                dialog.show();
        } else {
        }

    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(PincodeAvailabiltyActivity.this, message);
    }

}
