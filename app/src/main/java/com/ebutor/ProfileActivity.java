package com.ebutor;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.adapters.AreaAutoCompleteAdapter;
import com.ebutor.adapters.StatesSpinnerAdapter;
import com.ebutor.backgroundtask.MultipartEntity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.fragments.ManufacturerFragment;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.PincodeDataModel;
import com.ebutor.models.StateModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.RoundedImageView;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ProfileActivity extends BaseActivity implements com.google.android.gms.location.LocationListener, View.OnClickListener, Response.ErrorListener, ManufacturerFragment.OnClickListener, VolleyHandler<Object>, ResultCallback<LocationSettingsResult>, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQUEST_LOCATION = 0;
    public static Activity instance;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    private EditText etFirstName, etLastName, etMobile, etEmail, etAddress1, etAddress2, etCity, etPincode, etBusinessName, tvManf;
    private EditText etNoOfShutters, etContactName1, etContactName2, etContactNo1, etContactNo2, tvStartTime, tvEndTime;
    private EditText etLocality, etLandmark, etGst, etArn;
    private ImageView ivHeaderEdit, ivMobileEdit;
    private AutoCompleteTextView etArea;
    private FloatingActionButton fabEdit;
    private String selectedCustomerGrpId = "", selectedBusinessTypeId = "", selectedVolumeClass, selectedStateId, userId1, userId2, selBeat;
    private int cartCount = 0;
    private Tracker mTracker;
    private ArrayList<String> areasList;
    private boolean isHeaderEdit, isMobileEdit, isProfileEdit, isEmailEdit, isAddressEdit, isFF, isPremium;
    private String mobile = "", addressId = "", mobile1 = "", gst_values;
    private RelativeLayout rlUserProfile;
    private KeyListener keyListener;
    private InputMethodManager imm;
    private RoundedImageView ivPersonImage;
    private LinearLayout llMain;
    private TextView tvAlertMsg, tvBeat, tvUpdateLocation/*,tvLocateMe*/;
    private RelativeLayout rlAlert;
    private String requestType = "", strBusinessStartTime, strBusinessEndTime/*, strPrefSlot1, strPrefSlot2*/;
    private String picturePath = "", _path = "", manfIds = "", selectedManfIds = "", cntName1 = "", cntName2 = "", cntNo1 = "", cntNo2 = "";
    private Dialog dialog, progressDialog;
    private ArrayList<BusinessTypeModel> arrBusinessTypes;
    private ArrayList<CustomerTyepModel> arrVolumes, arrBuyerTypes, arrManf, arrBeats/*, arrPrefSlot1*/;
    private ArrayList<StateModel> arrStates;
    private Spinner spinnerBuyerType, spinnerBusinessType, spinnerVolumeClass, spinnerState, spBeat/*, spinnerPrefSlot1, spinnerPrefSlot2*/;
    private RadioButton radioSmartYes, radioSmartNo, radioInternetYes, radioInternetNo;
    private int hour, minute;
    private ArrayList<String> check_gst;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private Date startTime, startTimePrev, endTime, endTimePrev;
    private LinearLayout llContact1, llContact2;
    View.OnClickListener addContactClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (llContact1.getVisibility() != View.VISIBLE) {
                llContact1.setVisibility(View.VISIBLE);
            } else if (llContact2.getVisibility() != View.VISIBLE) {
                llContact2.setVisibility(View.VISIBLE);
            } else {
                Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.you_can_add_only_2_contacts));
            }
        }
    };
    private boolean isUpdate = false;
    private double currentLat = 0.0, currentLong = 0.0;
    private String mLatitude, mLongitude;
    private SharedPreferences mSharedPreferences;
    View.OnClickListener delete2Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            llContact2.setVisibility(View.GONE);
            etContactName2.setText("");
            etContactNo2.setText("");
            if (!TextUtils.isEmpty(cntName2) && !TextUtils.isEmpty(cntNo2)) {
                disableContact(cntNo2);
            }
        }
    };
    View.OnClickListener delete1Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            llContact1.setVisibility(View.GONE);
            etContactName1.setText("");
            etContactNo1.setText("");
            if (!TextUtils.isEmpty(cntName1) && !TextUtils.isEmpty(cntNo1)) {
                disableContact(cntNo1);
            }
        }
    };
    private boolean isFlag = false;
    private ImageView ivDelete1, ivDelete2;
    private ImageButton ibAddContact;

    public static ArrayList<String> filter(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerGrpId());
            }
        }
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rlUserProfile = (RelativeLayout) inflater.inflate(R.layout.activity_profile1, null, false);
        flbody.addView(rlUserProfile, baseLayoutParams);

        setContext(ProfileActivity.this);

        instance = this;
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        dialog = Utils.createLoader(ProfileActivity.this, ConstantValues.PROGRESS);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);

        gst_values = mSharedPreferences.getString(ConstantValues.GST_CODES, "");
        check_gst = new ArrayList<>(Arrays.asList(gst_values.split(",")));

        ibAddContact = (ImageButton) findViewById(R.id.tv_add_contact);
        llContact1 = (LinearLayout) findViewById(R.id.ll_contact_1);
        llContact2 = (LinearLayout) findViewById(R.id.ll_contact_2);
        ivDelete1 = (ImageView) findViewById(R.id.iv_delete_1);
        ivDelete2 = (ImageView) findViewById(R.id.iv_delete_2);

        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etMobile = (EditText) findViewById(R.id.et_mobile);
        etEmail = (EditText) findViewById(R.id.et_email_address);
        etAddress1 = (EditText) findViewById(R.id.et_address1);
        etAddress2 = (EditText) findViewById(R.id.et_address2);
        etLocality = (EditText) findViewById(R.id.et_locality);
        etLandmark = (EditText) findViewById(R.id.et_landmark);
        etCity = (EditText) findViewById(R.id.et_city);
        etPincode = (EditText) findViewById(R.id.et_pincode);
        etBusinessName = (EditText) findViewById(R.id.et_business_name);
        etNoOfShutters = (EditText) findViewById(R.id.et_no_of_shutters);
        etContactName1 = (EditText) findViewById(R.id.et_contact_name_1);
        etContactName2 = (EditText) findViewById(R.id.et_contact_name_2);
        etContactNo1 = (EditText) findViewById(R.id.et_contact_number_1);
        etContactNo2 = (EditText) findViewById(R.id.et_contact_number_2);
        tvManf = (EditText) findViewById(R.id.tv_manf);
        tvStartTime = (EditText) findViewById(R.id.tv_start_time);
        tvEndTime = (EditText) findViewById(R.id.tv_end_time);
        etArea = (AutoCompleteTextView) findViewById(R.id.et_area);
        ivHeaderEdit = (ImageView) findViewById(R.id.iv_header_edit);
        ivMobileEdit = (ImageView) findViewById(R.id.iv_mobile_edit);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        tvBeat = (TextView) findViewById(R.id.tv_beat);
        tvUpdateLocation = (TextView) findViewById(R.id.tv_update_location);
//        tvLocateMe = (TextView) findViewById(R.id.tv_locate_me);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        llMain = (LinearLayout) findViewById(R.id.ll_main);
        spinnerBuyerType = (Spinner) findViewById(R.id.spinner_buyer_type);
        spinnerBusinessType = (Spinner) findViewById(R.id.spinner_business_type);
        spinnerVolumeClass = (Spinner) findViewById(R.id.spinner_volume_class);
        spinnerState = (Spinner) findViewById(R.id.spinner_state);
        spBeat = (Spinner) findViewById(R.id.spinner_beat);
       /* spinnerPrefSlot1 = (Spinner) findViewById(R.id.spinner_pref_slot_1);
        spinnerPrefSlot2 = (Spinner) findViewById(R.id.spinner_pref_slot_2);*/
        radioSmartYes = (RadioButton) findViewById(R.id.radio_smart_yes);
        radioSmartNo = (RadioButton) findViewById(R.id.radio_smart_no);
        radioInternetYes = (RadioButton) findViewById(R.id.radio_internet_yes);
        radioInternetNo = (RadioButton) findViewById(R.id.radio_internet_no);
        fabEdit = (FloatingActionButton) findViewById(R.id.fab_edit);
        etGst = (EditText) findViewById(R.id.et_gst);
        etArn = (EditText) findViewById(R.id.et_arn);

        if (isFF) {
            tvBeat.setVisibility(View.VISIBLE);
            spBeat.setVisibility(View.VISIBLE);
        } else {
            tvBeat.setVisibility(View.GONE);
            spBeat.setVisibility(View.GONE);
        }

        spinnerBuyerType.setEnabled(false);
        spinnerBusinessType.setEnabled(false);
        spinnerVolumeClass.setEnabled(false);
        /*spinnerPrefSlot1.setEnabled(false);
        spinnerPrefSlot2.setEnabled(false);*/
        spinnerState.setEnabled(false);
        spBeat.setEnabled(false);

        etMobile.setSelection(0);

        requestLocationPermission();
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
//        checkLocationSettings();
        getStates();
        if (isFF) {
            getBeats();
        } else {
            getProfile();
        }

        ivPersonImage = (RoundedImageView) findViewById(R.id.iv_person_image);

        ivPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();
        ivDelete1.setOnClickListener(null);
        ivDelete2.setOnClickListener(null);
        ibAddContact.setOnClickListener(null);

        final String profileImage = mSharedPreferences.getString(ConstantValues.KEY_PROFILE_IMAGE, null);
        personName = mSharedPreferences.getString(ConstantValues.KEY_FIRST_NAME, null);
        if (personName != null && !TextUtils.isEmpty(personName))
            tvPerson.setText(personName);
        if (profileImage != null && !TextUtils.isEmpty(profileImage)) {
            ImageLoader.getInstance().displayImage(profileImage, ivperson, options);
            ImageLoader.getInstance().displayImage(profileImage, ivPersonImage, options);
        }

        spinnerBuyerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                selectedCustomerGrpId = arrBuyerTypes.get(i).getCustomerGrpId();
//                if (selectedCustomerGrpId != null && selectedCustomerGrpId.length() > 0) {
//                    editor.putString(ConstantValues.KEY_CUSTOMER_GRP_ID, selectedCustomerGrpId);
//                    editor.apply();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spBeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selBeat = arrBeats.get(position).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerBusinessType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                selectedBusinessTypeId = arrBusinessTypes.get(i).getSegmentId();
//                if (selectedBusinessTypeId != null && selectedBusinessTypeId.length() > 0) {
//                    editor.putString(ConstantValues.KEY_SEGMENT_ID, selectedBusinessTypeId);
//                    editor.apply();
//                }
//                getCategories();
//                getSegments();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerVolumeClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedVolumeClass = arrVolumes.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*spinnerPrefSlot1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strPrefSlot1 = arrPrefSlot1.get(i).getCustomerName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerPrefSlot2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strPrefSlot2 = arrPrefSlot1.get(i).getCustomerName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedStateId = arrStates.get(i).getStateId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        arrBeats = new ArrayList<>();

        arrManf = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_MASTER_MANF);

        arrBuyerTypes = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_CUSTOMER_TYPE);

        arrBusinessTypes = DBHelper.getInstance().getAllSegments();
        if (arrBusinessTypes != null && arrBusinessTypes.size() > 0) {
            ArrayAdapter<BusinessTypeModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrBusinessTypes);
            spinnerBusinessType.setAdapter(arrayAdapter);
        }

        arrVolumes = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_VOLUME_CLASS);
        if (arrVolumes != null && arrVolumes.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrVolumes);
            spinnerVolumeClass.setAdapter(arrayAdapter);
        }

        /*arrPrefSlot1 = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_PREF_SLOTS);
        if (arrPrefSlot1 != null && arrPrefSlot1.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrPrefSlot1);
            spinnerPrefSlot1.setAdapter(arrayAdapter);
            spinnerPrefSlot2.setAdapter(arrayAdapter);
        }*/

//        ivDelete1.setOnClickListener(delete1Click);
//
//        ivDelete2.setOnClickListener(delete2Click);

//        ibAddContact.setOnClickListener(addContactClick);

        etPincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isProfileEdit) {
                    if (s != null && s.length() == 6) {
                        isFlag = true;
                        getPincodeAreas(s.toString());
                        etArea.setEnabled(true);
                    } else {
                        etArea.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tvUpdateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLocationSettings();
            }
        });

        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hour = 7;
                minute = 0;
                TimePickerDialog timePickerDialog = new TimePickerDialog(ProfileActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String selStartTime = hourOfDay + ":" + minute + ":00";
                        try {
                            startTimePrev = startTime;
                            startTime = sdf.parse(selStartTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (endTime != null && (startTime.compareTo(endTime) < 0)) {
                            String startTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_STD_PATTERN, hourOfDay + ":" + minute);
                            strBusinessStartTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_PATTERN1, hourOfDay + ":" + minute);
                            if (null != startTime)
                                tvStartTime.setText(startTime);
                        } else {
                            startTime = startTimePrev;
                            Toast.makeText(ProfileActivity.this, getResources().getString(R.string.please_select_endtime_after_starttime), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        tvEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hour = 9;
                minute = 0;
                if (null != strBusinessStartTime && !TextUtils.isEmpty(strBusinessStartTime)) {

                    TimePickerDialog timePickerDialog = new TimePickerDialog(ProfileActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String selEndTime = hourOfDay + ":" + minute + ":00";
                            try {
                                endTimePrev = endTime;
                                endTime = sdf.parse(selEndTime);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (startTime.compareTo(endTime) < 0) {
                                String strEndTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_STD_PATTERN, hourOfDay + ":" + minute);
                                strBusinessEndTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_PATTERN1, hourOfDay + ":" + minute);
                                tvEndTime.setText(strEndTime);
                            } else {
                                endTime = endTimePrev;
                                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.please_select_endtime_after_starttime), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, hour, minute, false);
                    timePickerDialog.show();
                } else {
                    Toast.makeText(ProfileActivity.this, getResources().getString(R.string.please_select_start_time), Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(ProfileActivity.this)) {
                    switch (requestType) {
                        case "GetProfile":
                            getProfile();
                            break;
                        case "EditEmail":
                            editEmail();
                            break;
                        case "EditAddress":
                            editAddress();
                            break;
                        case "EditMobile":
                            editMobile();
                            break;
                        case "EditProfile":
                            editAddress();
                            break;
                        case "UpdateProfileCapture":
                            new UploadFileToServer().execute(_path);
                            break;
                        case "UpdateProfilePic":
                            new UploadFileToServer().execute(picturePath);
                            break;
                        default:
                            getProfile();
                            break;
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvManf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrManf != null && arrManf.size() > 0)
                    showManfDialog();
                else
                    Toast.makeText(ProfileActivity.this, getResources().getString(R.string.no_manf), Toast.LENGTH_SHORT).show();
            }
        });

        ivHeaderEdit.setOnClickListener(this);
        ivMobileEdit.setOnClickListener(this);
        fabEdit.setOnClickListener(this);
    }

    private void updateGeoLocation() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (isFF) {
                    jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                    jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                }
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                jsonObject.put("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));
                jsonObject.put("latitude", mLatitude);
                jsonObject.put("longitude", mLongitude);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateGeoURL,
                        map, ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.UPDATE_GEO);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.updateGeoURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void requestLocationPermission() {
        Log.i("", "CAMERA permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Log.i("",
                    "Displaying camera permission rationale to provide additional context.");
            ActivityCompat.requestPermissions(ProfileActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
            /*Snackbar.make(findViewById(R.id.tv_capture), R.string.permission_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    })
                    .show();*/
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
        // END_INCLUDE(camera_permission_request)
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(5000);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(1000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    private void disableContact(String cntNo) {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                jsonObject.put("telephone", cntNo);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.disableContactURL,
                        map, ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.DISABLE_CONTACT);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.disableContactURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        } else {
/*            progressDialog = ProgressDialog.show(ProfileActivity.this, "", "Fetching Location Please wait..", true);

            if (progressDialog != null)
                progressDialog.show();
            SingleShotLocationProvider.requestSingleUpdate(ProfileActivity.this,
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override
                        public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            Log.d("Location", "my location is " + location.toString());
                            Toast.makeText(ProfileActivity.this, location.latitude + "\n" + location.longitude, Toast.LENGTH_SHORT).show();
                        }
                    });*/
//            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            if (mCurrentLocation != null) {
//
//                double latitude = mCurrentLocation.getLatitude();
//                double longitude = mCurrentLocation.getLongitude();
//                mLatitude = String.valueOf(latitude);
//                mLongitude = String.valueOf(longitude);

            if (mLatitude != null && mLongitude != null) {
                updateGeoLocation();
            } else {
//                    getCurrentLocation();
                Toast.makeText(this, getResources().getString(R.string.unable_find_location), Toast.LENGTH_LONG).show();
            }

            /*} else {
//                getCurrentLocation();
                Toast.makeText(this, getResources().getString(R.string.unable_find_location), Toast.LENGTH_LONG).show();
            }*/
        }

    }

    private void getBeats() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getbeatsURL, map, ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.GET_BEATS);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getbeatsURL);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void getPincodeAreas(String pincode) {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
//            rlAlert.setVisibility(View.GONE);
//            requestType = "GetPinCodeAreas";
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pincode", pincode);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPinCodeDataURL,
                        map, ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.GET_PINCODE_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getPinCodeDataURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void showManfDialog() {
        FragmentManager fm = getSupportFragmentManager();
        ManufacturerFragment manufacturerFragment = ManufacturerFragment.newInstance(arrManf, manfIds);
        manufacturerFragment.setClickListener(ProfileActivity.this);
        manufacturerFragment.setCancelable(true);
        manufacturerFragment.show(fm, "manf_fragment");
    }

    private void getStates() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "GetStates";
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("flag", "2");
                jsonObject.put("country", "99");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getStateCountriesURL, map, ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.GET_STATES);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getStateCountriesURL);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "GetStates";
        }
    }

    private void getProfile() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            requestType = "GetProfile";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                /*obj.put("city", etCity.getText().toString());
                obj.put("address_1", etAddress1.getText().toString());
                obj.put("address_2", etAddress2.getText().toString());
                obj.put("postcode", etPincode.getText().toString());
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));*/
                map.put("data", obj.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask profileRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.profileURL, map,
                    ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.GET_PROFILE);
            profileRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(profileRequest, AppURL.profileURL);
            if (dialog != null)
                dialog.show();

        } else {
            requestType = "GetProfile";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void selectImage() {

        final CharSequence[] options = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(getResources().getString(R.string.upload_profile_pic));
        builder.setCancelable(true);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {
                    int permissionCheck = ContextCompat.checkSelfPermission(ProfileActivity.this,
                            Manifest.permission.CAMERA);

                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ProfileActivity.this,
                                new String[]{Manifest.permission.CAMERA}, 9);
                        return;
                    }

                    int permissionCheck1 = ContextCompat.checkSelfPermission(ProfileActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ProfileActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                        return;
                    }

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 10);
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                    int permissionCheck = ContextCompat.checkSelfPermission(ProfileActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ProfileActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 20);

                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void enableKey() {
        keyListener = new KeyListener() {
            @Override
            public int getInputType() {
                return 1;
            }

            @Override
            public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyUp(View view, Editable text, int keyCode, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyOther(View view, Editable text, KeyEvent event) {
                return false;
            }

            @Override
            public void clearMetaKeyState(View view, Editable content, int states) {

            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.i("", "User agreed to make required location settings changes.");
//                    try {
//                        new Thread().sleep(2000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

                   /* new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getCurrentLocation();
                        }
                    }, 10000);*/
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i("", "User chose not to make required location settings changes.");
                    break;
            }
        } else if (resultCode == RESULT_OK) {
            if (requestCode == 10) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Bitmap bitmap, newBitmap;
                int rotate = 0;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                _path = f.getAbsolutePath();
                try {


                    ExifInterface exif = new ExifInterface(_path);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotate);
                    bitmap = BitmapFactory.decodeFile(_path, bitmapOptions);
                    newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    ivPersonImage.setImageBitmap(newBitmap);
                    f.delete();
                    f = new File(_path);
                    f.createNewFile();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    newBitmap.compress(Bitmap.CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Networking.isNetworkAvailable(ProfileActivity.this)) {
                    rlAlert.setVisibility(View.GONE);
                    llMain.setVisibility(View.VISIBLE);
                    requestType = "UpdateProfileCapture";
                    new UploadFileToServer().execute(_path);
                } else {
                    rlAlert.setVisibility(View.VISIBLE);
                    llMain.setVisibility(View.GONE);
                    requestType = "UpdateProfileCapture";
                }

            } else if (requestCode == 20) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                picturePath = c.getString(columnIndex);
                c.close();
                int rotate = 0;
                Bitmap newBitmap;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                Matrix matrix = new Matrix();

                try {
                    ExifInterface exif = new ExifInterface(picturePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                    matrix.postRotate(rotate);
                    Bitmap bitmap = (BitmapFactory.decodeFile(picturePath));
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes1);
                    ivperson.setImageBitmap(newBitmap);
                    ivPersonImage.setImageBitmap(newBitmap);
                    File f = new File(selectedImage.getPath());
                    f.delete();
                    f = new File(picturePath);
                    f.createNewFile();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    newBitmap.compress(Bitmap.CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    ivperson.setImageBitmap(bitmap);
                    ivPersonImage.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (Networking.isNetworkAvailable(ProfileActivity.this)) {
                    rlAlert.setVisibility(View.GONE);
                    llMain.setVisibility(View.VISIBLE);
                    requestType = "UpdateProfilePick";
                    new UploadFileToServer().execute(picturePath);
                } else {
                    rlAlert.setVisibility(View.VISIBLE);
                    llMain.setVisibility(View.GONE);
                    requestType = "UpdateProfilePick";
                }

            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            mCurrentLocation = location;
            if (mCurrentLocation != null) {
                double latitude = mCurrentLocation.getLatitude();
                double longitude = mCurrentLocation.getLongitude();
                mLatitude = String.valueOf(latitude);
                mLongitude = String.valueOf(longitude);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*@Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                // BEGIN_INCLUDE(permission_result)
                // Received permission result for camera permission.
                Log.i("", "Received response for Location permission request.");

                // Check if the only required permission has been granted
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Location permission has been granted
                    Log.i("", "Locations permission has now been granted.");
                    /*Snackbar.make(findViewById(R.id.tv_capture), R.string.permision_available_locations,
                            Snackbar.LENGTH_SHORT).show();*/
                    getCurrentLocation();

                } else {
                    Log.i("", "Location permission was NOT granted.");
//                /*Snackbar.make(findViewById(R.id.button), R.string.permissions_not_granted,
//                        Snackbar.LENGTH_SHORT).show();*/

                }
                // END_INCLUDE(permission_result)
                break;
            case 9: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // camera task you need to do.

                    int permissionCheck = ContextCompat.checkSelfPermission(ProfileActivity.this,
                            Manifest.permission.CAMERA);

                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(ProfileActivity.this, getString(R.string.unable_open_camera), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int permissionCheck1 = ContextCompat.checkSelfPermission(ProfileActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(ProfileActivity.this, getString(R.string.unable_open_camera), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 10);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

            }
            break;
        }
    }

    private void showAlertDialog(String message, final PARSER_TYPE requestType) {
        new AlertDialog.Builder(ProfileActivity.this)
                .setTitle(getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (requestType == PARSER_TYPE.EDIT_MOBILE) {
                            etMobile.setText(mobile1);
                            etMobile.setEnabled(false);
                            ivMobileEdit.setImageResource(R.drawable.ic_edit);
                            isMobileEdit = false;
                        } else if (requestType == PARSER_TYPE.EDIT_EMAIL) {
                            etEmail.setEnabled(false);
//                            ivEmailEdit.setImageResource(R.drawable.ic_edit);
                            isEmailEdit = false;
                        } else if (requestType == PARSER_TYPE.EDIT_EMAIL) {
                            etEmail.setEnabled(false);
//                            ivEmailEdit.setImageResource(R.drawable.ic_edit);
                            isEmailEdit = false;
                        }
                    }
                })
                .show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_header_edit:
                if (isHeaderEdit) {
                    imm.hideSoftInputFromWindow(etFirstName.getWindowToken(), 0);
                    editName();
                } else {
                    enableKey();
                    etFirstName.setKeyListener(keyListener);
                    etFirstName.setInputType(InputType.TYPE_CLASS_TEXT);
                    etFirstName.setEnabled(true);
                    etFirstName.setFocusable(true);
                    etFirstName.requestFocus();
                    etFirstName.setClickable(true);
                    etFirstName.setFocusableInTouchMode(true);
                    etFirstName.setCursorVisible(true);
                    etFirstName.setSelection(etFirstName.length());
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
                    etLastName.setKeyListener(keyListener);
                    etLastName.setInputType(InputType.TYPE_CLASS_TEXT);
                    etLastName.setEnabled(true);
                    etLastName.setFocusable(true);
                    etLastName.setClickable(true);
                    etLastName.setFocusableInTouchMode(true);
                    etLastName.setCursorVisible(true);
                    etLastName.setSelection(etLastName.length());
                    imm.showSoftInput(etLastName, InputMethodManager.SHOW_IMPLICIT);
                    ivHeaderEdit.setImageResource(R.drawable.ic_save_green);
                    isHeaderEdit = true;
                }
                break;
            case R.id.iv_mobile_edit:

                if (isMobileEdit) {
                    if (mobile.equalsIgnoreCase(etMobile.getText().toString())) {
                        Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.no_change_telephone));
                        ivMobileEdit.setImageResource(R.drawable.ic_edit);
                        return;
                    }
                    editMobile();
                    imm.hideSoftInputFromWindow(etMobile.getWindowToken(), 0);
                } else {
                    enableKey();
                    etMobile.setKeyListener(keyListener);
                    etMobile.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etMobile.setEnabled(true);
                    etMobile.setFocusable(true);
                    etMobile.requestFocus();
                    etMobile.setClickable(true);
                    etMobile.setFocusableInTouchMode(true);
                    etMobile.setCursorVisible(true);
                    etMobile.setSelection(etMobile.length());
                    imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
                    ivMobileEdit.setImageResource(R.drawable.ic_save_green);
                    isMobileEdit = true;
                }
                break;
            case R.id.fab_edit:
                if (isProfileEdit) {

                    String mobileNo = etMobile.getText().toString().trim();
                    if (mobileNo.length() != 10) {
                        etMobile.setError(getResources().getString(R.string.please_enter_valid_mobile_number));
                        etMobile.requestFocus();
                        return;
                    }
                    if (mobileNo.substring(0, 1).equalsIgnoreCase("0")) {
                        etMobile.setError(getResources().getString(R.string.please_enter_valid_mobile_number));
                        etMobile.requestFocus();
                        return;
                    }

                    String strAddress1 = etAddress1.getText().toString().trim();
                    if (TextUtils.isEmpty(strAddress1)) {
                        etAddress1.setError(getResources().getString(R.string.please_enter_address1));
                        etAddress1.requestFocus();
                        return;
                    }

                    String streetName = etAddress2.getText().toString().trim();
                    if (TextUtils.isEmpty(streetName)) {
                        etAddress2.setError(getResources().getString(R.string.please_enter_address2));
                        etAddress2.requestFocus();
                        return;
                    }

                    String locality = etLocality.getText().toString().trim();
                    if (TextUtils.isEmpty(locality)) {
                        etLocality.setError(getResources().getString(R.string.please_enter_locality));
                        etLocality.requestFocus();
                        return;
                    }

                    String strPinCode = etPincode.getText().toString().trim();
                    if (TextUtils.isEmpty(strPinCode) || strPinCode.length() < 6) {
                        etPincode.setError(getResources().getString(R.string.please_enter_valid_area_pin));
                        etPincode.requestFocus();
                        return;
                    }
                    String strArea = etArea.getText().toString().trim();
                    if (TextUtils.isEmpty(strArea)) {
                        etArea.setError(getResources().getString(R.string.please_enter_area));
                        etArea.requestFocus();
                        return;
                    }

                    if (isFF && (null == selBeat || selBeat.length() <= 0)) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_beat));
                        return;
                    }

                    String strCity = etCity.getText().toString().trim();
                    if (TextUtils.isEmpty(strCity)) {
                        etCity.setError(getResources().getString(R.string.please_enter_city));
                        etCity.requestFocus();
                        return;
                    }

                    Predicate<CustomerTyepModel> isCheckedManfs = new Predicate<CustomerTyepModel>() {
                        @Override
                        public boolean apply(CustomerTyepModel customerTyepModel) {
                            return customerTyepModel.isChecked();
                        }
                    };

                    ArrayList<String> checkedFilters = filter(arrManf, isCheckedManfs);
                    selectedManfIds = TextUtils.join(",", checkedFilters);
                    if (selectedManfIds != null && selectedManfIds.startsWith(",")) {
                        selectedManfIds = selectedManfIds.replaceFirst(",", "");
                    }
                    String gst_check = "";

                    if (null == selectedCustomerGrpId || selectedCustomerGrpId.length() <= 0) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_customer_type));
                        return;
                    }
                    if (null == selectedBusinessTypeId || selectedBusinessTypeId.length() <= 0) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_business_type));
                        return;
                    }
                    if (TextUtils.isEmpty(etBusinessName.getText().toString())) {
                        etBusinessName.setError(getResources().getString(R.string.please_enter_business_name));
                        etBusinessName.requestFocus();
                    }
                    if (etBusinessName.getText().toString().length() < 4) {
                        Utils.showAlertWithMessage(ProfileActivity.this, getString(R.string.please_enter_shop_name_4_32));
                        return;
                    }
                    if (TextUtils.isEmpty(etNoOfShutters.getText().toString())) {
                        etNoOfShutters.setError(getResources().getString(R.string.please_enter_no_of_shutters));
                        etNoOfShutters.requestFocus();
                    }
                    if (null == selectedVolumeClass || selectedVolumeClass.length() <= 0) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_volume_class));
                        return;
                    }
                    if (TextUtils.isEmpty(selectedManfIds)) {
                        Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.please_select_manufacturers));
                    }
                    if (strBusinessStartTime == null || TextUtils.isEmpty(strBusinessStartTime)) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_start_time));
                        return;
                    }
                    if (strBusinessEndTime == null || TextUtils.isEmpty(strBusinessEndTime)) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_end_time));
                        return;
                    } /*else if (TextUtils.isEmpty(strPrefSlot1) || strPrefSlot1.equalsIgnoreCase("Select Preferred Slot")) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_pref_slot_1));
                        return;
                    }*/
                    if (null == selectedStateId || selectedStateId.length() <= 0) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_select_state));
                        return;
                    }
                    if (!TextUtils.isEmpty(etGst.getText().toString()) && etGst.getText().length() != 15) {
                        etGst.setError(getString(R.string.please_enter_proper_gst_number));
                        return;
                    }
                    if (!TextUtils.isEmpty(etGst.getText().toString())) {
                        gst_check = etGst.getText().toString().substring(0, 2);

                        if (!check_gst.contains(gst_check)) {
                            etGst.setError(getString(R.string.please_enter_proper_gst_number));
                            return;
                        }
                    }
                    if (!TextUtils.isEmpty(etArn.getText().toString()) && etArn.getText().length() != 15) {
                        etArn.setError(getString(R.string.please_enter_proper_arn_number));
                        return;
                    }
                    if (TextUtils.isEmpty(etContactName1.getText().toString()) && !TextUtils.isEmpty(etContactNo1.getText().toString())) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_enter_contact_name_1));
                        return;
                    }
                    if (TextUtils.isEmpty(etContactNo1.getText().toString()) && !TextUtils.isEmpty(etContactName1.getText().toString())) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_enter_contact_number_1));
                        return;
                    }
                    if ((etContactNo1.getVisibility() == View.VISIBLE) && !TextUtils.isEmpty(etContactNo1.getText().toString()) && etContactNo1.getText().toString().length() != 10) {
                        etContactNo1.setError(getResources().getString(R.string.please_enter_valid_contact_number_1));
                        etContactNo1.requestFocus();
                        return;
                    }
                    if (TextUtils.isEmpty(etContactName2.getText().toString()) && !TextUtils.isEmpty(etContactNo2.getText().toString())) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_enter_contact_name_2));
                        return;
                    }
                    if (TextUtils.isEmpty(etContactNo2.getText().toString()) && !TextUtils.isEmpty(etContactName2.getText().toString())) {
                        Utils.showAlertDialog(ProfileActivity.this, getResources().getString(R.string.please_enter_contact_number_2));
                        return;
                    }
                    if ((etContactNo2.getVisibility() == View.VISIBLE) && !TextUtils.isEmpty(etContactNo2.getText().toString()) && etContactNo2.getText().toString().length() != 10) {
                        etContactNo2.setError(getResources().getString(R.string.please_enter_valid_contact_number_2));
                        etContactNo2.requestFocus();
                        return;
                    }
                    editProfile();
                } else {
                    enableKey();
//                    tvLocateMe.setEnabled(true);
                    ivDelete1.setOnClickListener(delete1Click);
                    ivDelete2.setOnClickListener(delete2Click);
                    ibAddContact.setOnClickListener(addContactClick);
                    if (isFF || !isPremium) {
                        spinnerBuyerType.setEnabled(true);
                        spinnerBuyerType.setFocusable(true);
                        spinnerBuyerType.setClickable(true);
                        spinnerBuyerType.setFocusableInTouchMode(true);
                    } else {
                        spinnerBuyerType.setEnabled(false);
                    }
                    if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EDIT_SEGMENT_FEATURE_CODE) > 0) {
                        spinnerBusinessType.setEnabled(true);
                        spinnerBusinessType.setFocusable(true);
                        spinnerBusinessType.setClickable(true);
                        spinnerBusinessType.setFocusableInTouchMode(true);
                    } else {
                        spinnerBusinessType.setEnabled(false);
                    }
                    spinnerVolumeClass.setEnabled(true);
                    spinnerVolumeClass.setFocusable(true);
                    spinnerVolumeClass.setClickable(true);
                    spinnerVolumeClass.setFocusableInTouchMode(true);
                    spBeat.setEnabled(true);
                    spBeat.setFocusable(true);
                    spBeat.setClickable(true);
                    spBeat.setFocusableInTouchMode(true);
                    /*spinnerPrefSlot1.setEnabled(true);
                    spinnerPrefSlot1.setFocusable(true);
                    spinnerPrefSlot1.setClickable(true);
                    spinnerPrefSlot1.setFocusableInTouchMode(true);
                    spinnerPrefSlot2.setEnabled(true);
                    spinnerPrefSlot2.setFocusable(true);
                    spinnerPrefSlot2.setClickable(true);
                    spinnerPrefSlot2.setFocusableInTouchMode(true);*/
//                    spinnerState.setEnabled(true);
//                    spinnerState.setFocusable(true);
//                    spinnerState.setClickable(true);
//                    spinnerState.setFocusableInTouchMode(true);
                    tvManf.setEnabled(true);
                    tvManf.setFocusable(true);
                    tvStartTime.setEnabled(true);
                    tvStartTime.setFocusable(true);
                    tvEndTime.setEnabled(true);
                    tvEndTime.setFocusable(true);
                    etBusinessName.setKeyListener(keyListener);
                    etBusinessName.setInputType(InputType.TYPE_CLASS_TEXT);
                    etBusinessName.setEnabled(true);
                    etBusinessName.setFocusable(true);
                    etBusinessName.setClickable(true);
                    etBusinessName.setFocusableInTouchMode(true);
                    etBusinessName.setCursorVisible(true);
                    etBusinessName.setSelection(etBusinessName.length());
                    imm.showSoftInput(etBusinessName, InputMethodManager.SHOW_IMPLICIT);
                    etNoOfShutters.setKeyListener(keyListener);
                    etNoOfShutters.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etNoOfShutters.setEnabled(true);
                    etNoOfShutters.setFocusable(true);
                    etNoOfShutters.setClickable(true);
                    etNoOfShutters.setFocusableInTouchMode(true);
                    etNoOfShutters.setCursorVisible(true);
                    etNoOfShutters.setSelection(etNoOfShutters.length());
                    imm.showSoftInput(etNoOfShutters, InputMethodManager.SHOW_IMPLICIT);
                    etEmail.setKeyListener(keyListener);
                    etEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    etEmail.setEnabled(true);
                    etEmail.setFocusable(true);
                    etEmail.setClickable(true);
                    etEmail.setFocusableInTouchMode(true);
                    etEmail.setCursorVisible(true);
                    etEmail.setSelection(etEmail.length());
                    imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
                    etAddress1.setKeyListener(keyListener);
                    etAddress1.setInputType(InputType.TYPE_CLASS_TEXT);
                    etAddress1.setEnabled(true);
                    etAddress1.setFocusable(true);
                    etAddress1.setClickable(true);
                    etAddress1.setFocusableInTouchMode(true);
                    etAddress1.setCursorVisible(true);
                    etAddress1.setSelection(etAddress1.length());
                    imm.showSoftInput(etAddress1, InputMethodManager.SHOW_IMPLICIT);

                    etAddress2.setKeyListener(keyListener);
                    etAddress2.setInputType(InputType.TYPE_CLASS_TEXT);
                    etAddress2.setEnabled(true);
                    etAddress2.setFocusable(true);
                    etAddress2.setClickable(true);
                    etAddress2.setFocusableInTouchMode(true);
                    etAddress2.setCursorVisible(true);
                    etAddress2.setSelection(etAddress2.length());
                    imm.showSoftInput(etAddress2, InputMethodManager.SHOW_IMPLICIT);

                    etLocality.setKeyListener(keyListener);
                    etLocality.setInputType(InputType.TYPE_CLASS_TEXT);
                    etLocality.setEnabled(true);
                    etLocality.setFocusable(true);
                    etLocality.setClickable(true);
                    etLocality.setFocusableInTouchMode(true);
                    etLocality.setCursorVisible(true);
                    etLocality.setSelection(etLocality.length());
                    imm.showSoftInput(etLocality, InputMethodManager.SHOW_IMPLICIT);

                    etLandmark.setKeyListener(keyListener);
                    etLandmark.setInputType(InputType.TYPE_CLASS_TEXT);
                    etLandmark.setEnabled(true);
                    etLandmark.setFocusable(true);
                    etLandmark.setClickable(true);
                    etLandmark.setFocusableInTouchMode(true);
                    etLandmark.setCursorVisible(true);
                    etLandmark.setSelection(etLandmark.length());
                    imm.showSoftInput(etLandmark, InputMethodManager.SHOW_IMPLICIT);

                    etCity.setKeyListener(keyListener);
                    etCity.setInputType(InputType.TYPE_CLASS_TEXT);
                    etCity.setEnabled(true);
                    etCity.setFocusable(true);
                    etCity.setSelection(etCity.length());
                    etPincode.setKeyListener(keyListener);
                    etPincode.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etPincode.setEnabled(true);
                    etPincode.setFocusable(true);
                    etPincode.setSelection(etPincode.length());
                    etArea.setKeyListener(keyListener);
                    etArea.setInputType(InputType.TYPE_CLASS_TEXT);
                    etArea.setEnabled(true);
                    etArea.setFocusable(true);
                    etArea.setClickable(true);
                    etArea.setFocusableInTouchMode(true);
                    etArea.setCursorVisible(true);
                    etArea.setSelection(etArea.length());
                    /*NumberKeyListener listener = new NumberKeyListener() {

                        public int getInputType() {
                            return InputType.TYPE_MASK_VARIATION;
                        }

                        @Override
                        protected char[] getAcceptedChars() {
                            return new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
                        }
                    };
                    etGst.setKeyListener(listener);*/
//                    etGst.setInputType(InputType.TYPE_CLASS_TEXT);

                    /*InputFilter filter = new InputFilter() {
                        @Override
                        public CharSequence filter(CharSequence src, int start, int end, Spanned dest, int dstart, int dend) {
                            for (int i = start; i < end; ++i) {
                                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890]*").matcher(String.valueOf(src.charAt(i))).matches()) {
                                    return "";
                                }
                            }


                            return null;
                        }
                    };
                    etGst.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(15)});*/

                    etGst.setEnabled(true);
                    etGst.setFocusable(true);
                    /*etGst.requestFocus();
                    etGst.setClickable(true);
                    etGst.setFocusableInTouchMode(true);
                    etGst.setCursorVisible(true);
                    etGst.setSelection(etGst.length());
                    imm.showSoftInput(etGst, InputMethodManager.SHOW_IMPLICIT);*/

//                    etArn.setKeyListener(listener);
//                    etArn.setInputType(InputType.TYPE_CLASS_TEXT);
//                    etArn.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(15)});
                    etArn.setEnabled(true);
                    etArn.setFocusable(true);
                    /*etArn.requestFocus();
                    etArn.setClickable(true);
                    etArn.setFocusableInTouchMode(true);
                    etArn.setCursorVisible(true);
                    etArn.setSelection(etArn.length());
                    imm.showSoftInput(etArn, InputMethodManager.SHOW_IMPLICIT);*/
                    etContactName1.setKeyListener(keyListener);
                    etContactName1.setInputType(InputType.TYPE_CLASS_TEXT);
                    etContactName1.setEnabled(true);
                    etContactName1.setFocusable(true);
                    etContactName1.setClickable(true);
                    etContactName1.setFocusableInTouchMode(true);
                    etContactName1.setCursorVisible(true);
                    etContactName1.setSelection(etContactName1.length());
                    etContactNo1.setKeyListener(keyListener);
                    etContactNo1.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etContactNo1.setEnabled(true);
                    etContactNo1.setFocusable(true);
                    etContactNo1.setClickable(true);
                    etContactNo1.setFocusableInTouchMode(true);
                    etContactNo1.setCursorVisible(true);
                    etContactNo1.setSelection(etContactNo1.length());
                    etContactName2.setKeyListener(keyListener);
                    etContactName2.setInputType(InputType.TYPE_CLASS_TEXT);
                    etContactName2.setEnabled(true);
                    etContactName2.setFocusable(true);
                    etContactName2.setClickable(true);
                    etContactName2.setFocusableInTouchMode(true);
                    etContactName2.setCursorVisible(true);
                    etContactName2.setSelection(etContactName2.length());
                    etContactNo2.setKeyListener(keyListener);
                    etContactNo2.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etContactNo2.setEnabled(true);
                    etContactNo2.setFocusable(true);
                    etContactNo2.setClickable(true);
                    etContactNo2.setFocusableInTouchMode(true);
                    etContactNo2.setCursorVisible(true);
                    etContactNo2.setSelection(etContactNo2.length());
                    radioSmartYes.setEnabled(true);
                    radioSmartYes.setFocusable(true);
                    radioSmartYes.setClickable(true);
                    radioSmartNo.setEnabled(true);
                    radioSmartNo.setFocusable(true);
                    radioSmartNo.setClickable(true);
                    radioInternetYes.setEnabled(true);
                    radioInternetYes.setFocusable(true);
                    radioInternetYes.setClickable(true);
                    radioInternetNo.setEnabled(true);
                    radioInternetNo.setFocusable(true);
                    radioInternetNo.setClickable(true);
                    fabEdit.setImageResource(R.drawable.ic_save_green);
                    isProfileEdit = true;
                }
                break;
            case R.id.iv_email_edit:
                if (isEmailEdit) {
                    imm.hideSoftInputFromWindow(etEmail.getWindowToken(), 0);
                    editEmail();
                } else {
                    enableKey();
                    etEmail.setKeyListener(keyListener);
                    etEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    etEmail.setEnabled(true);
                    etEmail.setFocusable(true);
                    etEmail.requestFocus();
                    etEmail.setClickable(true);
                    etEmail.setFocusableInTouchMode(true);
                    etEmail.setCursorVisible(true);
                    etEmail.setSelection(etEmail.length());
                    imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
//                    ivEmailEdit.setImageResource(R.drawable.ic_save_green);
                    isEmailEdit = true;
                }
                break;
            case R.id.iv_address_edit:
                if (isAddressEdit) {
                    editAddress();
                    imm.hideSoftInputFromWindow(etAddress1.getWindowToken(), 0);
                } else {
                    enableKey();
                    etAddress1.setKeyListener(keyListener);
                    etAddress1.setInputType(InputType.TYPE_CLASS_TEXT);
                    etAddress1.setEnabled(true);
                    etAddress1.setFocusable(true);
                    etAddress1.requestFocus();
                    etAddress1.setClickable(true);
                    etAddress1.setFocusableInTouchMode(true);
                    etAddress1.setCursorVisible(true);
                    etAddress1.setSelection(etAddress1.length());
                    imm.showSoftInput(etAddress1, InputMethodManager.SHOW_IMPLICIT);
                    etAddress2.setKeyListener(keyListener);
                    etAddress2.setInputType(InputType.TYPE_CLASS_TEXT);
                    etAddress2.setEnabled(true);
                    etAddress2.setFocusable(true);
                    etAddress2.requestFocus();
                    etAddress2.setClickable(true);
                    etAddress2.setFocusableInTouchMode(true);
                    etAddress2.setCursorVisible(true);
                    etAddress2.setSelection(etAddress2.length());
                    imm.showSoftInput(etAddress2, InputMethodManager.SHOW_IMPLICIT);
                    etCity.setKeyListener(keyListener);
                    etCity.setInputType(InputType.TYPE_CLASS_TEXT);
                    etCity.setEnabled(true);
                    etCity.setFocusable(true);
                    etCity.setSelection(etCity.length());
                    etPincode.setKeyListener(keyListener);
                    etPincode.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etPincode.setEnabled(true);
                    etPincode.setFocusable(true);
                    etPincode.setSelection(etPincode.length());
//                    ivAddressEdit.setImageResource(R.drawable.ic_save_green);
                    isAddressEdit = true;
                }
                break;
        }
    }

    private void editAddress() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            requestType = "EditAddress";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("flag", "5");
                detailsObj.put("city", etCity.getText().toString());
                detailsObj.put("address_1", etAddress1.getText().toString());
                detailsObj.put("address_2", etAddress2.getText().toString());
                detailsObj.put("postcode", etPincode.getText().toString());
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                map.put("data", detailsObj.toString());

                VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateProfileURL, map,
                        ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.EDIT_ADDRESS);
                productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.updateProfileURL);
                if (dialog != null)
                    dialog.show();
            } catch (Exception e) {

            }

        } else {
            requestType = "EditAddress";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void editEmail() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            requestType = "EditEmail";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("flag", "2");
                detailsObj.put("email", etEmail.getText().toString());
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                map.put("data", detailsObj.toString());

                VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateProfileURL, map,
                        ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.EDIT_EMAIL);
                productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.updateProfileURL);
                if (dialog != null)
                    dialog.show();
            } catch (Exception e) {

            }
        } else {
            requestType = "EditEmail";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void editProfile() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            requestType = "EditProfile";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    detailsObj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    detailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("flag", "5");
                detailsObj.put("business_type", selectedBusinessTypeId);
                detailsObj.put("buyer_type", selectedCustomerGrpId);
                detailsObj.put("volume_class", selectedVolumeClass);
                detailsObj.put("company", etBusinessName.getText().toString());
                detailsObj.put("email", etEmail.getText().toString());
                detailsObj.put("No_of_shutters", etNoOfShutters.getText().toString());
                detailsObj.put("manufacturers", selectedManfIds);
                detailsObj.put("business_start_time", strBusinessStartTime);
                detailsObj.put("business_end_time", strBusinessEndTime);
                /*detailsObj.put("delivery_time", strPrefSlot1);
                detailsObj.put("pref_value1", strPrefSlot2);*/
                detailsObj.put("smartphone", radioSmartYes.isChecked() ? "1" : "0");
                detailsObj.put("internet_availability", radioInternetYes.isChecked() ? "1" : "0");
                detailsObj.put("city", etCity.getText().toString());
                detailsObj.put("address_1", etAddress1.getText().toString());
                detailsObj.put("address_2", etAddress2.getText().toString());
                detailsObj.put("locality", etLocality.getText().toString());
                detailsObj.put("landmark", etLandmark.getText().toString());
                detailsObj.put("postcode", etPincode.getText().toString());
                detailsObj.put("area", etArea.getText().toString());
                detailsObj.put("state", selectedStateId);
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                detailsObj.put("beat_id", selBeat);
                detailsObj.put("gstin", etGst.getText().toString());
                detailsObj.put("arn_number", etArn.getText().toString());

                if (etContactName1.getText().toString() != null && !TextUtils.isEmpty(etContactName1.getText().toString())) {
                    detailsObj.put("contact_name1", etContactName1.getText().toString());
                }
                if (etContactNo1.getText().toString() != null && !TextUtils.isEmpty(etContactNo1.getText().toString())) {
                    detailsObj.put("contact_no1", etContactNo1.getText().toString());
                }
                detailsObj.put("user_id1", userId1);
                if (etContactName2.getText().toString() != null && !TextUtils.isEmpty(etContactName2.getText().toString())) {
                    detailsObj.put("contact_name2", etContactName2.getText().toString());
                }
                if (etContactNo2.getText().toString() != null && !TextUtils.isEmpty(etContactNo2.getText().toString())) {
                    detailsObj.put("contact_no2", etContactNo2.getText().toString());
                }
                detailsObj.put("user_id2", userId2);

                map.put("data", detailsObj.toString());
            } catch (Exception e) {

            }
            VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateProfileURL, map,
                    ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.EDIT_PROFILE);
            productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.updateProfileURL);
            if (dialog != null)
                dialog.show();
        } else {
            requestType = "EditProfile";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void editMobile() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            requestType = "EditMobile";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    detailsObj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    detailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("telephone", etMobile.getText().toString());
                detailsObj.put("flag", "3");
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                map.put("data", detailsObj.toString());
            } catch (Exception e) {

            }
            VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateProfileURL, map,
                    ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.EDIT_MOBILE);
            productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.updateProfileURL);
            if (dialog != null)
                dialog.show();
        } else {
            requestType = "EditMobile";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void editName() {
        if (Networking.isNetworkAvailable(ProfileActivity.this)) {
            requestType = "EditName";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    detailsObj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    detailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("flag", "1");
                detailsObj.put("firstname", etFirstName.getText().toString());
                detailsObj.put("lastname", etLastName.getText().toString());
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                map.put("data", detailsObj.toString());

                VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateProfileURL, map,
                        ProfileActivity.this, ProfileActivity.this, PARSER_TYPE.EDIT_NAME);
                productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.updateProfileURL);
                if (dialog != null)
                    dialog.show();
            } catch (Exception e) {

            }
        } else {
            requestType = "EditName";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartCount();
        mTracker.setScreenName("Profile Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(ProfileActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing() && requestType != PARSER_TYPE.GET_BEATS && requestType != PARSER_TYPE.GET_STATES)
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {

                    if (requestType == PARSER_TYPE.GET_STATES) {
                        if (response instanceof ArrayList) {
                            if (arrStates != null)
                                arrStates.clear();
                            arrStates = (ArrayList<StateModel>) response;

                            StatesSpinnerAdapter adapter = new StatesSpinnerAdapter(ProfileActivity.this, arrStates);
                            spinnerState.setAdapter(adapter);

                            getProfile();
//                            getBeats();
                        }
                    } else if (requestType == PARSER_TYPE.GET_PINCODE_DATA) {
                        if (response instanceof PincodeDataModel) {
                            PincodeDataModel pincodeDataModel = (PincodeDataModel) response;
                            if (areasList != null)
                                areasList.clear();
                            if (isFlag)
                                etArea.setText("");
                            areasList = pincodeDataModel.getArrAreas();

                            AreaAutoCompleteAdapter adapter = new AreaAutoCompleteAdapter(ProfileActivity.this, R.layout.row_area_name, areasList);
                            etArea.setAdapter(adapter);

                            int pos = 0;
                            for (int i = 0; i < arrStates.size(); i++) {
                                StateModel stateModel = arrStates.get(i);
                                if (stateModel.getStateId().equalsIgnoreCase(pincodeDataModel.getStateId())) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerState.setSelection(pos);
                            spinnerState.setEnabled(false);

                        }
                    } else if (requestType == PARSER_TYPE.GET_PROFILE) {

                        if (response instanceof CustomerTyepModel) {
                            CustomerTyepModel model = (CustomerTyepModel) response;
                            String firstname = model.getFirstName();
                            String lastname = model.getLastName();
                            String documents = model.getDocuments();
                            String company = model.getCompany();
                            String address_1 = model.getAddress1();
                            String address_2 = model.getAddress2();
                            String locality = model.getLocality();
                            String landmark = model.getLandmark();
                            String city = model.getCity();
                            String state = model.getState();
                            String area = model.getArea();
                            String country = model.getCountry();
                            String postcode = model.getPostcode();
                            mobile = model.getTelephone();
                            String email = model.getEmail();
                            addressId = model.getAddressId();
                            String bStartTime = model.getBusinessStartTime().equalsIgnoreCase("null") ? "07:00:00" : model.getBusinessStartTime();
                            String bEndTime = model.getBusinessEndTime().equalsIgnoreCase("null") ? "21:00:00" : model.getBusinessEndTime();
                            String prefDeliveryTime = model.getDeliveryTime();
                            String businessType = model.getBusinessType();
                            String buyerType = model.getBuyerType();
                            String volumeClass = model.getVolumeClass();
                            String noOfShutters = model.getNoOfShutters();
                            String contactName1 = model.getContactName1();
                            String contactName2 = model.getContactName2();
                            String contactNo1 = model.getContactNo1();
                            String contactNo2 = model.getContactNo2();
                            String prefSlot1 = model.getPrefSlot1();
                            String prefSlot2 = model.getPrefSlot2();
                            String beatId = model.getBeatId();
                            String gstValue = model.getGst();
                            String arnValue = model.getArn();
                            isPremium = model.isPremium();
                            selBeat = beatId;
                            cntName1 = contactName1;
                            cntName2 = contactName2;
                            cntNo1 = contactNo1;
                            cntNo2 = contactNo2;
                            boolean isSmartPhone = model.isSmartPhone();
                            boolean isInternet = model.isInternet();
                            boolean isParent = model.isParent();

                            if (!isFF && !isPremium) {
                                for (int i = 0; i < arrBuyerTypes.size(); i++) {
                                    CustomerTyepModel customerTyepModel = arrBuyerTypes.get(i);
                                    if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase("3013")) {
                                        arrBuyerTypes.remove(customerTyepModel);
                                    }
                                }
                            }
                            if (arrBuyerTypes != null && arrBuyerTypes.size() > 0) {
                                ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrBuyerTypes);
                                spinnerBuyerType.setAdapter(arrayAdapter);
                            }

                            if (isParent) {
                                ivHeaderEdit.setEnabled(true);
                                ivMobileEdit.setEnabled(true);
                                fabEdit.setEnabled(true);
                            } else {
                                ivHeaderEdit.setEnabled(false);
                                ivMobileEdit.setEnabled(false);
                                fabEdit.setEnabled(false);
                            }
                            userId1 = model.getUserId1();
                            userId2 = model.getUserId2();
                            manfIds = model.getManfIds() == null ? "" : model.getManfIds();
                            if (manfIds != null) {
                                if (manfIds.startsWith(",")) {
                                    manfIds = manfIds.substring(1, manfIds.length());
                                }
                            }
                            String manfNames = DBHelper.getInstance().getManfNames(manfIds);
                            tvManf.setText(manfNames);
                            ArrayList<String> manfArray = new ArrayList<>();
                            if (manfIds != null && manfIds.length() > 0)
                                manfArray = new ArrayList<String>(Arrays.asList(manfIds.split("\\s*,\\s*")));
                            for (int i = 0; i < manfArray.size(); i++) {
                                for (int j = 0; j < arrManf.size(); j++) {
                                    if (arrManf.get(j).getCustomerGrpId().equalsIgnoreCase(manfArray.get(i)))
                                        arrManf.get(j).setIsChecked(true);
                                }
                            }
                            if (!bStartTime.equalsIgnoreCase("null") && !bEndTime.equalsIgnoreCase("null")) {
                                startTime = sdf.parse(bStartTime);
                                startTimePrev = startTime;
                                endTime = sdf.parse(bEndTime);
                                endTimePrev = endTime;
                                strBusinessStartTime = bStartTime;
                                strBusinessEndTime = bEndTime;
                                bStartTime = Utils.parseDate(Utils.TIME_PATTERN1, Utils.TIME_STD_PATTERN, bStartTime);
                                bEndTime = Utils.parseDate(Utils.TIME_PATTERN1, Utils.TIME_STD_PATTERN, bEndTime);
                                try {
                                    startTime = sdf.parse(bStartTime);
                                    startTimePrev = startTime;
                                    endTime = sdf.parse(bEndTime);
                                    endTimePrev = endTime;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                tvStartTime.setText(bStartTime);
                                tvEndTime.setText(bEndTime);
                            }
                            if (isSmartPhone)
                                radioSmartYes.setChecked(true);
                            if (isInternet)
                                radioInternetYes.setChecked(true);

                            etFirstName.setText(firstname);
                            etLastName.setText(lastname);
                            etMobile.setText(mobile);
                            etBusinessName.setText(company);
                            mobile1 = mobile;
                            etEmail.setText(email);
                            etAddress1.setText(address_1);

//                            if (TextUtils.isEmpty(address_2))
//                                etAddress2.setVisibility(View.GONE);
//                            else
                            etAddress2.setText(address_2);
                            etLocality.setText(locality == null ? "" : locality);
                            etLandmark.setText(landmark == null ? "" : landmark);
                            etCity.setText(city);
                            etPincode.setText(postcode);
                            isFlag = false;
                            getPincodeAreas(postcode);
                            etArea.setText(area);
                            etNoOfShutters.setText(noOfShutters);
                            if (TextUtils.isEmpty(contactName1) && TextUtils.isEmpty(contactNo1)) {
                                llContact1.setVisibility(View.GONE);
                            } else {
                                llContact1.setVisibility(View.VISIBLE);
                                etContactName1.setText(contactName1);
                                etContactNo1.setText(contactNo1);
                            }
                            if (TextUtils.isEmpty(contactName2) && TextUtils.isEmpty(contactNo2)) {
                                llContact2.setVisibility(View.GONE);
                            } else {
                                llContact2.setVisibility(View.VISIBLE);
                                etContactName2.setText(contactName2);
                                etContactNo2.setText(contactNo2);
                            }
                            etGst.setText(gstValue);
                            etArn.setText(arnValue);
                            int pos = 0;
                            for (int i = 0; i < arrBuyerTypes.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrBuyerTypes.get(i);
                                if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(buyerType)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerBuyerType.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrBusinessTypes.size(); i++) {
                                BusinessTypeModel businessTypeModel = arrBusinessTypes.get(i);
                                if (businessTypeModel.getSegmentId().equalsIgnoreCase(businessType)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerBusinessType.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrVolumes.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrVolumes.get(i);
                                if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(volumeClass)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerVolumeClass.setSelection(pos);

                            pos = 0;
                            if (arrBeats != null) {
                                for (int i = 0; i < arrBeats.size(); i++) {
                                    CustomerTyepModel customerTyepModel = arrBeats.get(i);
                                    if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(beatId)) {
                                        pos = i;
                                        break;
                                    }
                                }
                            }
                            spBeat.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrStates.size(); i++) {
                                StateModel stateModel = arrStates.get(i);
                                if (stateModel.getStateName().equalsIgnoreCase(state)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerState.setSelection(pos);

                            /*pos = 0;
                            for (int i = 0; i < arrPrefSlot1.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrPrefSlot1.get(i);
                                if (customerTyepModel.getCustomerName().equalsIgnoreCase(prefSlot1)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerPrefSlot1.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrPrefSlot1.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrPrefSlot1.get(i);
                                if (customerTyepModel.getCustomerName().equalsIgnoreCase(prefSlot2)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerPrefSlot2.setSelection(pos);*/

                            mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, documents).apply();
                            ImageLoader.getInstance().displayImage(documents, ivPersonImage, options);
                            ImageLoader.getInstance().displayImage(documents, ivperson, options);
                        }

                    } else if (requestType == PARSER_TYPE.EDIT_NAME) {
                        if (message.equalsIgnoreCase("updateProfile")) {
                            message = "Profile updated successfully.";
                        }
                        if (response instanceof CustomerTyepModel) {
                            CustomerTyepModel model = (CustomerTyepModel) response;
                            String firstname = model.getFirstName();
                            String lastName = model.getLastName();
                            String documents = model.getDocuments();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_FIRST_NAME, firstname).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, documents).apply();
                            etFirstName.setText(firstname);
                            etFirstName.setEnabled(false);
                            etLastName.setText(lastName);
                            etLastName.setEnabled(false);
                            tvPerson.setText(firstname + " " + lastName);

                        }

                        ivHeaderEdit.setImageResource(R.drawable.ic_edit_white);
                        isHeaderEdit = false;

                    } else if (requestType == PARSER_TYPE.EDIT_EMAIL) {
                        if (response instanceof String) {
                            String email = (String) response;
                            etEmail.setText(email);
                        }

                        etEmail.setEnabled(false);
//                        ivEmailEdit.setImageResource(R.drawable.ic_edit);
                        isEmailEdit = false;
                    } else if (requestType == PARSER_TYPE.EDIT_MOBILE) {
                        if (response instanceof String) {
                            mobile = (String) response;
                        }
                        Intent intent = new Intent(ProfileActivity.this, OtpActivity.class);
                        intent.putExtra("MobileNo", mobile);
                        intent.putExtra("FromProfile", "Profile");
                        startActivity(intent);
                    } else if (requestType == PARSER_TYPE.EDIT_PROFILE) {
                        if (response instanceof CustomerTyepModel) {
                            CustomerTyepModel model = (CustomerTyepModel) response;

                            String company = model.getCompany();
                            String address_1 = model.getAddress1();
                            String address_2 = model.getAddress2();
                            String locality = model.getLocality();
                            String landmark = model.getLandmark();
                            String city = model.getCity();
                            String state = model.getState();
                            String area = model.getArea();
                            String country = model.getCountry();
                            String postcode = model.getPostcode();
                            String email = model.getEmail();
                            addressId = model.getAddressId();
                            String bStartTime = model.getBusinessStartTime();
                            String bEndTime = model.getBusinessEndTime();
                            String prefDeliveryTime = model.getDeliveryTime();
                            String businessType = model.getBusinessType();
                            String buyerType = model.getBuyerType();
                            String volumeClass = model.getVolumeClass();
                            String noOfShutters = model.getNoOfShutters();
                            String contactName1 = model.getContactName1();
                            String contactName2 = model.getContactName2();
                            String contactNo1 = model.getContactNo1();
                            String contactNo2 = model.getContactNo2();
                            String prefSlot1 = model.getPrefSlot1();
                            String prefSlot2 = model.getPrefSlot2();
                            String beatId = model.getBeatId();
                            String gstValue = model.getGst();
                            String arnValue = model.getArn();
                            selBeat = beatId;
                            cntName1 = contactName1;
                            cntName2 = contactName2;
                            cntNo1 = contactNo1;
                            cntNo2 = contactNo2;
                            boolean isSmartPhone = model.isSmartPhone();
                            boolean isInternet = model.isInternet();
                            manfIds = model.getManfIds() == null ? "" : model.getManfIds();

                            mSharedPreferences.edit().putString(ConstantValues.KEY_SHOP_NAME, company).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, beatId).apply();
                            tvRetailerName.setText(company);
                            etBusinessName.setText(company);
                            etEmail.setText(email);
                            etAddress1.setText(address_1);
                            etAddress2.setText(address_2);
                            etLocality.setText(locality == null ? "" : locality);
                            etLandmark.setText(landmark == null ? "" : landmark);
                            etCity.setText(city);
                            etPincode.setText(postcode);
                            isFlag = false;
                            getPincodeAreas(postcode);
                            etArea.setText(area);
                            etNoOfShutters.setText(noOfShutters);
                            etContactName1.setText(contactName1);
                            etContactName2.setText(contactName2);
                            etContactNo1.setText(contactNo1);
                            etContactNo2.setText(contactNo2);
                            if (!bStartTime.equalsIgnoreCase("null") && !bEndTime.equalsIgnoreCase("null")) {
                                startTime = sdf.parse(bStartTime);
                                startTimePrev = startTime;
                                endTime = sdf.parse(bEndTime);
                                endTimePrev = endTime;
                                strBusinessStartTime = bStartTime;
                                strBusinessEndTime = bEndTime;
                                bStartTime = Utils.parseDate(Utils.TIME_PATTERN1, Utils.TIME_STD_PATTERN, bStartTime);
                                bEndTime = Utils.parseDate(Utils.TIME_PATTERN1, Utils.TIME_STD_PATTERN, bEndTime);
                                try {
                                    startTime = sdf.parse(bStartTime);
                                    startTimePrev = startTime;
                                    endTime = sdf.parse(bEndTime);
                                    endTimePrev = endTime;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                tvStartTime.setText(bStartTime);
                                tvEndTime.setText(bEndTime);
                            }

                            if (isSmartPhone)
                                radioSmartYes.setChecked(true);
                            if (isInternet)
                                radioInternetYes.setChecked(true);
                            if (TextUtils.isEmpty(contactName1) && TextUtils.isEmpty(contactNo1)) {
                                llContact1.setVisibility(View.GONE);
                            } else {
                                llContact1.setVisibility(View.VISIBLE);
                                etContactName1.setText(contactName1);
                                etContactNo1.setText(contactNo1);
                            }
                            if (TextUtils.isEmpty(contactName2) && TextUtils.isEmpty(contactNo2)) {
                                llContact2.setVisibility(View.GONE);
                            } else {
                                llContact2.setVisibility(View.VISIBLE);
                                etContactName2.setText(contactName2);
                                etContactNo2.setText(contactNo2);
                            }

                            etGst.setText(gstValue);
                            etArn.setText(arnValue);
                            int pos = 0;
                            for (int i = 0; i < arrBuyerTypes.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrBuyerTypes.get(i);
                                if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(buyerType)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerBuyerType.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrBusinessTypes.size(); i++) {
                                BusinessTypeModel businessTypeModel = arrBusinessTypes.get(i);
                                if (businessTypeModel.getSegmentId().equalsIgnoreCase(businessType)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerBusinessType.setSelection(pos);
//                            businessTypeSpinner.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrVolumes.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrVolumes.get(i);
                                if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(volumeClass)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerVolumeClass.setSelection(pos);

                            pos = 0;
                            if (arrBeats != null) {
                                for (int i = 0; i < arrBeats.size(); i++) {
                                    CustomerTyepModel customerTyepModel = arrBeats.get(i);
                                    if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(beatId)) {
                                        pos = i;
                                        break;
                                    }
                                }
                            }
                            spBeat.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrStates.size(); i++) {
                                StateModel stateModel = arrStates.get(i);
                                if (stateModel.getStateName().equalsIgnoreCase(state)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerState.setSelection(pos);

                            /*pos = 0;
                            for (int i = 0; i < arrPrefSlot1.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrPrefSlot1.get(i);
                                if (customerTyepModel.getCustomerName().equalsIgnoreCase(prefSlot1)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerPrefSlot1.setSelection(pos);

                            pos = 0;
                            for (int i = 0; i < arrPrefSlot1.size(); i++) {
                                CustomerTyepModel customerTyepModel = arrPrefSlot1.get(i);
                                if (customerTyepModel.getCustomerName().equalsIgnoreCase(prefSlot2)) {
                                    pos = i;
                                    break;
                                }
                            }
                            spinnerPrefSlot2.setSelection(pos);*/

                        }

//                        tvLocateMe.setEnabled(false);
                        ivDelete1.setOnClickListener(null);
                        ivDelete2.setOnClickListener(null);
                        ibAddContact.setOnClickListener(null);
                        etBusinessName.setEnabled(false);
                        etEmail.setEnabled(false);
                        etAddress1.setEnabled(false);
                        etAddress2.setEnabled(false);
                        etLocality.setEnabled(false);
                        etLandmark.setEnabled(false);
                        etCity.setEnabled(false);
                        etPincode.setEnabled(false);
                        etArea.setEnabled(false);
                        etNoOfShutters.setEnabled(false);
                        etGst.setEnabled(false);
                        etArn.setEnabled(false);
                        etContactName1.setEnabled(false);
                        etContactName2.setEnabled(false);
                        etContactNo1.setEnabled(false);
                        etContactNo2.setEnabled(false);
                        radioSmartYes.setClickable(false);
                        radioSmartNo.setClickable(false);
                        radioInternetYes.setClickable(false);
                        radioInternetNo.setClickable(false);
                        spinnerBuyerType.setEnabled(false);
                        spinnerBusinessType.setEnabled(false);
                        spinnerVolumeClass.setEnabled(false);
                        spinnerState.setEnabled(false);
                        spBeat.setEnabled(false);
                        /*spinnerPrefSlot1.setEnabled(false);
                        spinnerPrefSlot2.setEnabled(false);*/
                        tvManf.setEnabled(false);
                        tvStartTime.setEnabled(false);
                        tvEndTime.setEnabled(false);

                        fabEdit.setImageResource(R.drawable.ic_edit_white);
                        isProfileEdit = false;

                    } else if (requestType == PARSER_TYPE.EDIT_ADDRESS) {

                        if (response instanceof CustomerTyepModel) {
                            CustomerTyepModel model = (CustomerTyepModel) response;
                            String address1 = model.getAddress1();
                            String address2 = model.getAddress2();
                            String locality = model.getLocality();
                            String landmark = model.getLandmark();
                            String city = model.getCity();
                            String postcode = model.getPostcode();
                            String whId = model.getWhId();

                            etAddress1.setText(address1);
                            etAddress2.setText(address2);
                            etLocality.setText(locality);
                            etLandmark.setText(landmark);
                            etCity.setText(city);
                            etPincode.setText(postcode);

                            if (whId != null && !TextUtils.isEmpty(whId) && !whId.equals("0"))
                                mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, whId).apply();
                        }

                        etAddress1.setEnabled(false);
                        etAddress2.setEnabled(false);
                        etLandmark.setEnabled(false);
                        etLocality.setEnabled(false);
                        etCity.setEnabled(false);
                        etPincode.setEnabled(false);
//                        ivAddressEdit.setImageResource(R.drawable.ic_edit);
                        isAddressEdit = false;
                    } else if (requestType == PARSER_TYPE.DISABLE_CONTACT) {
                        if (response instanceof String) {

                        }
                    } else if (requestType == PARSER_TYPE.GET_BEATS) {
                        if (response instanceof ArrayList) {
                            arrBeats = (ArrayList<CustomerTyepModel>) response;
                            if (arrBeats != null) {
                                CustomerTyepModel customerTyepModel1 = new CustomerTyepModel();
                                customerTyepModel1.setCustomerGrpId("");
                                customerTyepModel1.setCustomerName("Select Beat");

                                arrBeats.add(0, customerTyepModel1);

                                ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrBeats);
                                spBeat.setAdapter(arrayAdapter);
                            }
                            getProfile();
                        }
                    } else if (requestType == PARSER_TYPE.UPDATE_GEO) {
                        if (response instanceof String) {
                            Utils.showAlertWithMessage(ProfileActivity.this, message);
                        }
                    }
                } else {
                    if (requestType == PARSER_TYPE.GET_PINCODE_DATA) {
                        etArea.setText("");
                        areasList.clear();
                        etArea.setAdapter(null);
                    } else {
                        if (requestType != PARSER_TYPE.DISABLE_CONTACT)
                            Utils.showAlertWithMessage(ProfileActivity.this, message);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        invalidate(cartCount);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(ProfileActivity.this, message);
    }

    @Override
    public void onClicked(String string) {
        if (string != null) {

            Predicate<CustomerTyepModel> isCheckedManfs = new Predicate<CustomerTyepModel>() {
                @Override
                public boolean apply(CustomerTyepModel customerTyepModel) {
                    return customerTyepModel.isChecked();
                }
            };
            ArrayList<String> checkedFilters = filter(arrManf, isCheckedManfs);
            manfIds = TextUtils.join(",", checkedFilters);
            if (string.contains("All")) {
                tvManf.setText("All");
            } else {
                tvManf.setText(string);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                getCurrentLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(ProfileActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("connected", "connected");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestLocationPermission();
            return;
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {

            mLatitude = location.getLatitude() + "";
            mLongitude = location.getLongitude() + "";
            /*if (isUpdate) {
                updateGeoLocation();
            }*/
            isUpdate = true;

        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, ProfileActivity.this);


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private class UploadFileToServer extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(ProfileActivity.this, "", getResources().getString(R.string.please_wait));
            if (dialog != null)
                dialog.show();
            else {
                setDialog(0, "");
                dialog.show();
            }
        }

        public void setDialog(int id, String url) {

            dialog = new ProgressDialog(ProfileActivity.this);
            dialog.setTitle("");
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... params) {

            String strResponse = null;

            MultipartEntity multipart = null;
            try {
                String url = ConstantValues.NEW_BASE_URL_AUTH + "updateProfile";

                multipart = new MultipartEntity(url, "UTF-8");
                try {

                    JSONObject detailsObj = new JSONObject();
                    if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                        detailsObj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                        detailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                    }

                    detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    detailsObj.put("flag", "1");

                    multipart.addFormField("data", detailsObj.toString());

                    multipart.addFilePart("DOC", new File(params[0]));


                } catch (Exception e) {
                }

                List<String> response = multipart.finish(); // response from server.
                if (response != null && response.size() > 0) {
                    strResponse = response.get(0);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return strResponse;
        }

        @Override
        protected void onPostExecute(String s) {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            super.onPostExecute(s);
            if (s != null) {
                Log.e("resp", s);
                try {
                    JSONObject object = new JSONObject(s);
                    String status = object.getString("status");
                    String message = object.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject dataObj = object.optJSONObject("data");
                        String firstname = dataObj.optString("firstname");
                        String lastname = dataObj.optString("lastname");
                        String documents = /*ConstantValues.NEW_BASE_IMAGE_AUTH + */dataObj.optString("documents");
                        mSharedPreferences.edit().putString(ConstantValues.KEY_FIRST_NAME, firstname).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, documents).apply();
                        etFirstName.setText(firstname);
                        etFirstName.setEnabled(false);
                        etLastName.setText(lastname);
                        etLastName.setEnabled(false);
                        ivHeaderEdit.setImageResource(R.drawable.ic_edit_white);
                        isHeaderEdit = false;
                        String profileImage = mSharedPreferences.getString(ConstantValues.KEY_PROFILE_IMAGE, "");
                        if (profileImage != null && !TextUtils.isEmpty(profileImage)) {
                            ImageLoader.getInstance().displayImage(profileImage, ivperson, options);
                        }
                        Utils.showAlertDialog(ProfileActivity.this, message);
                    } else if (status.equalsIgnoreCase("session")) {
                        Utils.logout(ProfileActivity.this, message);
                    } else {
                        Utils.showAlertDialog(ProfileActivity.this, message);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
