package com.ebutor;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.backgroundtask.JSONParser;
import com.ebutor.models.OrdersModel;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/29/2016.
 */
public class OrderConfirmationActivity extends BaseActivity {

    private TextView tvMessage, tvOrderNumber, tvOrderDate, tvTotalAmount, tvDeliveryDate;
    private LinearLayout llOrderConfirmation, llApproved;
    private TextView tvPaymentApproved, tvPaymentDate, tvPaymentMode;
    private String paymentMode;
    private Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llOrderConfirmation = (LinearLayout) inflater.inflate(R.layout.activity_order_confirmation, null, false);
        flbody.addView(llOrderConfirmation, baseLayoutParams);

        setContext(OrderConfirmationActivity.this);

        tvMessage = (TextView) findViewById(R.id.tvMessage);
        tvOrderDate = (TextView) findViewById(R.id.tvOrderDate);
        tvOrderNumber = (TextView) findViewById(R.id.tvOrderNumber);
        tvTotalAmount = (TextView) findViewById(R.id.tvTotalAmount);
        tvDeliveryDate = (TextView) findViewById(R.id.tvDeliveryDate);
        tvPaymentApproved = (TextView) findViewById(R.id.tvPaymentConf);
        tvPaymentDate = (TextView) findViewById(R.id.tvPaymentDate);
        tvPaymentMode = (TextView) findViewById(R.id.tvPaymentMode);
        llApproved = (LinearLayout) findViewById(R.id.llApproved);
        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        SpannableStringBuilder sb = new SpannableStringBuilder("YOUR PAYMENT HAS BEEN APPROVED");
        setSpannedText(sb, tvPaymentApproved, 21);

        String data = getIntent().getStringExtra("Data");
        if (getIntent().hasExtra("paymentMode")) {
            paymentMode = getIntent().getStringExtra("paymentMode");
        }

        if ("cod".equalsIgnoreCase(paymentMode)) {
            paymentMode = getResources().getString(R.string.cash_on_delivery);
            llApproved.setVisibility(View.GONE);
        }
        JSONParser jsonParser = new JSONParser();
        OrdersModel ordersModel = jsonParser.addOrder(data);

        String orderNumber = ordersModel.getOrderNumber();
        String date = ordersModel.getDate();
        String totalAmount = ordersModel.getTotalAmount();
        String shippingDays = ordersModel.getShippingDays();
        String orderStatus = ordersModel.getOrderStatus();
        String deliveryDate = ordersModel.getDeliveryDate();

        tvOrderDate.setText(parseDateTime(date, "MMMM dd,yyyy hh:mm a"));
        tvPaymentDate.setText(parseDateTime(date, "MMMM dd,yyyy hh:mm a"));
        tvOrderNumber.setText(orderNumber);
        tvPaymentMode.setText(paymentMode);
        double totalAmountDouble = 0;
        try {
            totalAmountDouble = Double.parseDouble(totalAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvTotalAmount.setText(String.format(Locale.CANADA, "%.2f", totalAmountDouble)+" Rs");
        tvMessage.setText(shippingDays);
        tvDeliveryDate.setText(parseDate(deliveryDate, "MMMM dd,yyyy"));
    }

    public String parseDate(String date, String format) {

        String newDate = "";

        SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public String parseDateTime(String date, String format) {
        String newDate = "";

        SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat(format, Locale.US);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    private void setSpannedText(SpannableStringBuilder sb, TextView textView, int start) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(7, 157, 67));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

//        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
//        sb.setSpan(typefaceSpan, start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        sb.setSpan(new RelativeSizeSpan(1.2f), start, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(sb);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(OrderConfirmationActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Order Confirmation Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }

}
