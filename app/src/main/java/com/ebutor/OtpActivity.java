package com.ebutor;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by 300024 on 3/4/2016.
 */
public class OtpActivity extends ParentActivity implements ResultHandler, VolleyHandler, Response.ErrorListener {
    static EditText etOtp;
    String appId, mobileNo;
    String otp;
    Button btConfirmOtp, btResendOtp, btGetOtp;
    private String fromProfile = "", customerGroupId, customerId, customerToken = "", firstName, image, segmentId, pincode, whId;
    //    private boolean isApproved;
    private SharedPreferences mSharedPreferences;
    private LinearLayout llMain;
    private TextView tvAlertMsg;
    private RelativeLayout rlAlert;
    private String requestType = "";
    private Tracker mTracker;
    private Dialog dialog;
    private boolean isFF = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        if (getIntent().hasExtra("FromProfile")) {
            fromProfile = getIntent().getStringExtra("FromProfile");
        }
        dialog = Utils.createLoader(OtpActivity.this, ConstantValues.PROGRESS);

        btConfirmOtp = (Button) findViewById(R.id.bt_confirm_otp);
        btResendOtp = (Button) findViewById(R.id.bt_resend_otp);
        btGetOtp = (Button) findViewById(R.id.bt_get_otp);
        etOtp = (EditText) findViewById(R.id.et_otp);
        llMain = (LinearLayout) findViewById(R.id.ll_main);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        appId = mSharedPreferences.getString("appId", null);
        customerToken = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "");
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);

        if (isFF) {
            btGetOtp.setVisibility(View.VISIBLE);
        } else {
            btGetOtp.setVisibility(View.GONE);
        }

        int permissionCheck = ContextCompat.checkSelfPermission(OtpActivity.this,
                Manifest.permission.RECEIVE_SMS);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(OtpActivity.this,
                    new String[]{Manifest.permission.RECEIVE_SMS}, 9);
        }
        MyApplication application = (MyApplication) getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(OtpActivity.this)) {
                    switch (requestType) {
                        case "Confirm":
                            confirmOtp();
                            break;
                        case "ResendOtp":
                            resendOtp();
                            break;
                        case "Otp":
                            Otp();
                            break;
                        default:
                            rlAlert.setVisibility(View.GONE);
                            llMain.setVisibility(View.VISIBLE);
                            break;
                    }
                } else {
                    Toast.makeText(OtpActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btConfirmOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                otp = etOtp.getText().toString();
                if (TextUtils.isEmpty(otp)) {
                    etOtp.setError(getResources().getString(R.string.please_enter_otp));
                    etOtp.requestFocus();
                    return;
                } else if (etOtp.length() < 6) {
                    etOtp.setError(getResources().getString(R.string.please_enter_valid_otp));
                    etOtp.requestFocus();
                    return;
                } else if (!TextUtils.isEmpty(fromProfile)) {
                    Otp();
                } else {
                    confirmOtp();
                }


            }
        });

        btResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                etOtp.setText("");
                resendOtp();
                btResendOtp.setAlpha(0.5f);
                btResendOtp.setEnabled(false);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        btResendOtp.setEnabled(true);
                        btResendOtp.setAlpha(1);

                    }
                }, 30 * 1000);
            }
        });

        btGetOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                getOtp();
            }
        });

        if (getIntent().hasExtra("MobileNo")) {
            mobileNo = getIntent().getStringExtra("MobileNo");
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                btResendOtp.setAlpha(1);
                btResendOtp.setEnabled(true);

            }
        }, 30 * 1000);

    }

    private void getOtp() {
        if (Networking.isNetworkAvailable(OtpActivity.this)) {
            requestType = "GetOtp";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                String customerToken = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "");
                if (!TextUtils.isEmpty(fromProfile) && customerToken != null && customerToken.length() > 0) {
                    jsonObject.put("customer_token", customerToken);
                }
                if (mobileNo != null && mobileNo.length() > 0) {
                    jsonObject.put("telephone", mobileNo);
                }
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getOTPURL, map, OtpActivity.this, OtpActivity.this, PARSER_TYPE.GET_OTP);
                wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.getOTPURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {

            }

        } else {
            requestType = "GetOtp";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }

    }

    public void resendOtp() {
        if (Networking.isNetworkAvailable(OtpActivity.this)) {
            requestType = "ResendOtp";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();

                if (!TextUtils.isEmpty(fromProfile)) {
                    jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, null));
                }
                if (mobileNo != null && mobileNo.length() > 0) {
                    jsonObject.put("telephone", mobileNo);
                }
                jsonObject.put("flag", "3");
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.registrationURL, map, OtpActivity.this, OtpActivity.this, PARSER_TYPE.RESEND_OTP);
                wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.registrationURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestType = "ResendOtp";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    public void Otp() {
        if (Networking.isNetworkAvailable(OtpActivity.this)) {
            requestType = "Otp";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, null));
                if (otp != null && otp.length() > 0) {
                    jsonObject.put("otp_sent", otp);
                }
                jsonObject.put("flag", "4");
                if (mobileNo != null && mobileNo.length() > 0) {
                    jsonObject.put("telephone", mobileNo);
                }
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateProfileURL, map,
                        OtpActivity.this, OtpActivity.this, PARSER_TYPE.OTP);
                productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.updateProfileURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {

            }

        } else {
            requestType = "Otp";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    public void confirmOtp() {
        if (Networking.isNetworkAvailable(OtpActivity.this)) {
            requestType = "Confirm";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();

                if (mobileNo != null && mobileNo.length() > 0) {
                    jsonObject.put("telephone", mobileNo);
                }
                if (otp != null && otp.length() > 0) {
                    jsonObject.put("otp", otp);
                }
                jsonObject.put("flag", "2");
                jsonObject.put("device_id", mSharedPreferences.getString(ConstantValues.DEVICE_ID, ""));
                jsonObject.put("ip_address", mSharedPreferences.getString(ConstantValues.IP_ADDRESS, ""));
                jsonObject.put("reg_id", mSharedPreferences.getString(ConstantValues.FCM_ID, ""));
                jsonObject.put("platform_id", ConstantValues.PLATFORM_ID);

                map.put("data", jsonObject.toString());

                VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.registrationURL, map, OtpActivity.this, OtpActivity.this, PARSER_TYPE.CONFIRM_OTP);
                wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.registrationURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {

            }
        } else {
            requestType = "Confirm";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        String response = "";
        if (results != null) {
            response = (String) results;
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.optString("message");
                    String status = jsonObject.optString("status");

                    if (status.equalsIgnoreCase("success")) {
                        if (requestType == PARSER_TYPE.OTP) {
                            JSONObject dataObject = jsonObject.optJSONObject("data");
                            if (dataObject != null) {
                            }
                        }
                    } else {
                        if (requestType == PARSER_TYPE.CONFIRM_OTP || requestType == PARSER_TYPE.OTP)
                            etOtp.setText("");
                        Utils.showAlertWithMessage(OtpActivity.this, message);
                    }
                } catch (Exception e) {

                }
            }
        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {

        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            if (requestType == PARSER_TYPE.CONFIRM_OTP) {
                this.requestType = "Confirm";
            } else if (requestType == PARSER_TYPE.RESEND_OTP) {
                this.requestType = "ResendOtp";
            } else if (requestType == PARSER_TYPE.OTP) {
                this.requestType = "Confirm";
            }
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void receivedSms(String message) {
        try {
            String otp = message.substring(message.length() - 6, message.length());
            etOtp.setText(otp);
            etOtp.setSelection(otp.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("Otp Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if (error != null)
//            Utils.showAlertWithMessage(OtpActivity.this, getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        if (response != null) {
            try {
                if (requestType == PARSER_TYPE.CONFIRM_OTP) {
                    if (status.equalsIgnoreCase("1")) {
                        CustomerTyepModel model = (CustomerTyepModel) response;
//                        if (model != null) {
                        boolean isApproved = model.isLogin();
                        boolean isActive = model.isActive();

                        if (isActive) {
                            // User is Active, then check if he is approved
                            //Store the mobile number for next use
                            etOtp.setText("");
                            mSharedPreferences.edit().putString(ConstantValues.KEY_MOBILE, mobileNo).apply();

                            if (isApproved) {
                                customerGroupId = model.getCustomerGrpId();
                                customerToken = model.getCustomerToken();
                                customerId = model.getCustomerId();
                                firstName = model.getFirstName();
                                image = model.getProfilePic();
                                segmentId = model.getSegmentId();
                                pincode = model.getPostcode();
                                whId = model.getWhId();
                                int isDashboard = model.isDashboard;
                                int isFf = model.isFF;
                                String beatId = model.getBeatId();
                                String latitude = model.getLatitude();
                                String longitude = model.getLongitude();
                                String legalEntityId = model.getLegalEntityId();
                                double eCash = model.getEcash();
                                double creditLimit = model.getCreditLimit();
                                double paymentDue = model.getPaymentDue();
                                double ffeCash = model.getFfecash();
                                double ffCreditLimit = model.getFfCreditLimit();
                                double ffPaymentDue = model.getFfPaymentDue();

//                                int isFf = 1;
//                                boolean isFF = isFf == 1;
                                int isSrm = model.getIsSRM();
//                                boolean isSRM = isSrm == 1;
                                int _hasChild = model.getHasChild();
                                boolean hasChild = _hasChild == 1;

                                if (!(DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.DASHBOARD_FEATURE_CODE) > 0) && (TextUtils.isEmpty(beatId) || beatId.equalsIgnoreCase("0"))) {
                                    showAlertWithMessage(getString(R.string.your_account_under_activation), customerToken, legalEntityId);
                                    return;
                                } else {

                                    mSharedPreferences.edit().putString(ConstantValues.KEY_FIRST_NAME, firstName).apply();
                                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_DASHBOARD, isDashboard == 1).apply();
                                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_HAS_CHILD, hasChild).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, image).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_LEGAL_ENTITY_ID, model.getLegalEntityId()).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, beatId).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_BEAT_ID, beatId).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, latitude).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, longitude).apply();
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_CREDIT_LIMIT, (float) creditLimit).apply();
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_ECASH, (float) eCash).apply();
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_PAYMENT_DUE, (float) paymentDue).apply();

                                    if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.DASHBOARD_FEATURE_CODE) > 0) {
                                        mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_CREDIT_LIMIT, (float) ffCreditLimit).apply();
                                        mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_ECASH, (float) ffeCash).apply();
                                        mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_PAYMENT_DUE, (float) ffPaymentDue).apply();
                                    }

                                    if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.SALES_AGENT_FEATURE_CODE) > 0){
                                        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SALES_AGENT,true).apply();
                                    }else{
                                        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SALES_AGENT,false).apply();
                                    }
//                                mSharedPreferences.edit().putString(ConstantValues.KEY_FF_TOKEN, customerToken).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_SRM_TOKEN, customerToken).apply();
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_TOKEN, customerToken).apply();

                                    mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_ID, customerId).apply();
                                    if (null != customerGroupId && !TextUtils.isEmpty(customerGroupId)) {
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_GRP_ID, customerGroupId).apply();
                                        mSharedPreferences.edit().putString(ConstantValues.CUSTOMER_GRP_ID, customerGroupId).apply();
                                    }
                                    if (null != segmentId && !TextUtils.isEmpty(segmentId)) {
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_SEGMENT_ID, segmentId).apply();
                                        mSharedPreferences.edit().putString(ConstantValues.SEGMENT_ID, segmentId).apply();
                                    }
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_PINCODE, pincode).apply();
                                    if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.UPDATE_BEATS_FEATURE_CODE) > 0 || (model.getHub() != null && !TextUtils.isEmpty(model.getHub()) && !(model.getHub().equals("0")))) {
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_HUB_ID, model.getHub()).apply();
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_HUB_ID, model.getHub()).apply();
                                    }
                                    if (null != whId && !TextUtils.isEmpty(whId) && !whId.equals("0")) {
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, whId).apply();
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_LE_WH_IDS, whId).apply();
                                    }

                                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_LOGGED_IN, true).apply();

                                    if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.DASHBOARD_FEATURE_CODE) > 0) {
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_NAME, firstName).apply();
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_ID, customerId).apply();
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_TOKEN, customerToken).apply();
                                        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, false).apply();
                                        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_LEGAL_ENTITY_ID, model.getLegalEntityId()).apply();

                                        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_FF, true).apply();
                                        Intent intent = new Intent(OtpActivity.this, FFDashboardActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(OtpActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                    }

                                }
                            } else {
                                Intent intent = new Intent(OtpActivity.this, BuyerDetailsActivity.class);
                                startActivity(intent);
                            }

                            finish();
                            if (FirstSignupActivity.instnace != null)
                                FirstSignupActivity.instnace.finish();
                            if (SkipSignupActivity.instnace != null)
                                SkipSignupActivity.instnace.finish();

                        } else {
                            // user is not active, Show him hell
                            Utils.showAlertDialog(OtpActivity.this, message);
                        }

                    } else {
                        etOtp.setText("");
                        Utils.showAlertWithMessage(OtpActivity.this, message);
                    }
                } else if (requestType == PARSER_TYPE.SEND_MAIL_TO_FF) {
                    if (response instanceof String) {

                    }
                } else if (requestType == PARSER_TYPE.RESEND_OTP) {
                    if (status.equalsIgnoreCase("1")) {
                        Utils.showAlertWithMessage(OtpActivity.this, message);
                    } else {
                        Utils.showAlertWithMessage(OtpActivity.this, message);
                    }

                } else if (requestType == PARSER_TYPE.GET_OTP) {
                    //// TODO parse get OTP response
                    if (status.equalsIgnoreCase("success")) {
                        if (response instanceof String) {
                            otp = (String) response;
                            etOtp.setText(otp);
                            etOtp.setSelection(etOtp.length());
                        }
                    } else {
                        Utils.showAlertWithMessage(OtpActivity.this, message);
                    }

                } else if (requestType == PARSER_TYPE.OTP) {
                    if (status.equalsIgnoreCase("success")) {
                        Intent intent = new Intent(OtpActivity.this, ProfileActivity.class);
                        intent.putExtra("Update", "Success");
                        startActivity(intent);
                        if (ProfileActivity.instance != null)
                            ProfileActivity.instance.finish();
                        finish();
                    } else {
                        etOtp.setText("");
                        Utils.showAlertWithMessage(OtpActivity.this, message);
                    }
                } else {
                    if (requestType == PARSER_TYPE.CONFIRM_OTP || requestType == PARSER_TYPE.OTP)
                        etOtp.setText("");
                    Utils.showAlertWithMessage(OtpActivity.this, message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void sendMailToFF(String customerToken, String customerId) {
        if (Networking.isNetworkAvailable(OtpActivity.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", customerToken);
                jsonObject.put("customer_id", customerId);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.sendMailToFFURL, map, OtpActivity.this, OtpActivity.this, PARSER_TYPE.SEND_MAIL_TO_FF);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.sendMailToFFURL);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    public void showAlertWithMessage(String string, final String customerToken, final String customerId) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(OtpActivity.this);
        dialog.setMessage(string);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                sendMailToFF(customerToken, customerId);
                finish();
                if (FirstSignupActivity.instnace != null)
                    FirstSignupActivity.instnace.finish();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(OtpActivity.this, message);
    }

}
