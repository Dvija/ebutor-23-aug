package com.ebutor.callbacks;


import com.ebutor.utils.PARSER_TYPE;

public interface ResultHandlerVolley {

    public void onFinishVolley(Object results, PARSER_TYPE requestType);

    public void onErrorVolley(String errorCode, PARSER_TYPE requestType);

}
