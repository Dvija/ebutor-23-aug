package com.ebutor.callbacks;

import com.android.volley.Response;
import com.ebutor.utils.PARSER_TYPE;

/**
 * Created by Srikanth Nama on 13-Jul-16.
 */
public interface VolleyHandler<T> {
    public void onResponse(T response, PARSER_TYPE requestType,String status,String message);
    public void onSessionError(String message);
}
