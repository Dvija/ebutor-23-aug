package com.ebutor.callbacks;


import com.ebutor.utils.PARSER_TYPE;

public interface ResultHandler {

    public void onFinish(Object results, PARSER_TYPE requestType);

    public void onError(String errorCode, PARSER_TYPE requestType);

}
