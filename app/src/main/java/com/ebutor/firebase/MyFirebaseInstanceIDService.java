package com.ebutor.firebase;


import android.util.Log;

import com.ebutor.utils.ConstantValues;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = null;
        try {
            refreshedToken = FirebaseInstanceId.getInstance().getToken(ConstantValues.FCM_SENDER_ID,"FCM");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Displaying token on logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}
