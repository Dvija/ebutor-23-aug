/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ebutor;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appolica.interactiveinfowindow.InfoWindowManager;
import com.appolica.interactiveinfowindow.fragment.MapInfoWindowFragment;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.clusters.clustering.ClusterManager;
import com.ebutor.fragments.FormFragment;
import com.ebutor.models.RetailersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.MapWrapperLayout;
import com.ebutor.utils.Networking;
import com.ebutor.utils.OnInfoWindowElemTouchListener;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Simple activity demonstrating ClusterManager.
 */
public class ClusteringDemoActivity extends ParentActivity implements ClusterManager.OnClusterItemInfoWindowClickListener<RetailersModel>, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, FormFragment.OnClickListener, Response.ErrorListener, VolleyHandler<Object> {
    MapWrapperLayout mapWrapperLayout;
    private ClusterManager<RetailersModel> mClusterManager;
    private GoogleMap mMap;
    private InfoWindowManager infoWindowManager;
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private MapInfoWindowFragment mapInfoWindowFragment;
    private RetailersModel clickedClusterItem;
    private OnInfoWindowElemTouchListener infoButtonListener;
    private ViewGroup infoWindow;
    private TextView infoTitle;
    private Button infoButton;

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom);
        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(ClusteringDemoActivity.this, ConstantValues.TOOLBAR_PROGRESS);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.map_relative_layout);
        mapFragment.getMapAsync(this);

//        mapInfoWindowFragment =
//                (MapInfoWindowFragment) getSupportFragmentManager().findFragmentById(R.id.infoWindowMap);
//
//        infoWindowManager = mapInfoWindowFragment.infoWindowManager();
//        infoWindowManager.setHideOnFling(false);

    }

    protected void startDemo() {
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(17.401903, 78.546278), 10));

        mClusterManager = new ClusterManager<RetailersModel>(this, getMap());
        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnMarkerClickListener(this);
        getData();

        /*try {
            readItems();
        } catch (JSONException e) {
            Toast.makeText(this, "Problem reading list of markers.", Toast.LENGTH_LONG).show();
        }*/
    }

    /*private void readItems() throws JSONException {
        InputStream inputStream = getResources().openRawResource(R.raw.radar_search);
        List<MyItem> items = new MyItemReader().read(inputStream);
        mClusterManager.addItems(items);
    }*/

    private void getData() {
        if (Networking.isNetworkAvailable(ClusteringDemoActivity.this)) {
            try {
                HashMap<String, String> map = new HashMap<>();
                JSONObject dataObj = new JSONObject();
                dataObj.put("customer_token", "85f4e85e23f6f07a0add64cd05a09771");
                dataObj.put("offset", "0");
                dataObj.put("offset_limit", "0");
                dataObj.put("ff_id", "97");
//            if (object instanceof JSONArray) {
                dataObj.put("flag", "1");
//            }
                dataObj.put("beat_id", new JSONArray().put("-1"));
                dataObj.put("hub", "5387");
                map.put("data", dataObj.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllRetailersURL, map, ClusteringDemoActivity.this, ClusteringDemoActivity.this, PARSER_TYPE.OUTLETS_MAPS);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getAllRetailersURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mapInfoWindowFragment.getMapAsync(ClusteringDemoActivity.this);
        /*if (mMap == null) {

        }*/
//        setUpMap();
    }

    private void setUpMap() {
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            mMap = googleMap;
            startDemo();
        } else {
            mapInfoWindowFragment.getMapAsync(ClusteringDemoActivity.this);
        }
    }

    protected GoogleMap getMap() {
        return mMap;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

//        InfoWindow.MarkerSpecification markerSpec = new InfoWindow.MarkerSpecification(20, 90);
//
//        FormFragment fragment = new FormFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
//        fragment.setClickListener(ClusteringDemoActivity.this);
//
//        InfoWindow infoWindow = new InfoWindow(marker, markerSpec, fragment);
//        infoWindowManager.toggle(infoWindow, true);

        return false;
    }

    @Override
    public void onClicked(Object retailersModel) {

    }

    @Override
    public void onClose() {

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()/* && requestType != PARSER_TYPE.OUTLETS_MAPS*/)
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.OUTLETS_MAPS) {
                        if (response instanceof RetailersModel.Retailers) {
                            RetailersModel.Retailers retailersModel = (RetailersModel.Retailers) response;
                            ArrayList<RetailersModel> arrOutlets = retailersModel.getArrRetailers();
                            mClusterManager.addItems(arrOutlets);
                            mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                                    new MyCustomAdapterForItems());

                            mapWrapperLayout.init(mMap, getPixelsFromDp(this, 39 + 20));

                            // We want to reuse the info window for all the markers,
                            // so let's create only one class member instance
                            this.infoWindow = (ViewGroup) getLayoutInflater().inflate(R.layout.info_window, null);
                            this.infoTitle = (TextView) infoWindow.findViewById(R.id.txtTitle);
                            this.infoButton = (Button) infoWindow.findViewById(R.id.btn);

                            // Setting custom OnTouchListener which deals with the pressed state
                            // so it shows up
                            this.infoButtonListener = new OnInfoWindowElemTouchListener(infoButton) {
                                @Override
                                protected void onClickConfirmed(View v, Marker marker) {
                                    // Here we can perform some action triggered after clicking the button
                                    Toast.makeText(ClusteringDemoActivity.this, marker.getTitle() + "'s button clicked!", Toast.LENGTH_SHORT).show();
                                }
                            };
                            this.infoButton.setOnTouchListener(infoButtonListener);


                            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                                @Override
                                public View getInfoWindow(Marker marker) {
                                    return null;
                                }

                                @Override
                                public View getInfoContents(Marker marker) {
                                    // Setting up the infoWindow with current's marker info
                                    infoTitle.setText(marker.getTitle());
                                    infoButtonListener.setMarker(marker);

                                    // We must call this to set the current marker and infoWindow references
                                    // to the MapWrapperLayout
                                    mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow);
                                    return infoWindow;
                                }
                            });


                            /*mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(Marker marker) {
                                    if (marker.getTag() instanceof RetailersModel) {
                                        RetailersModel retailersModel = (RetailersModel) marker.getTag();

                                        InfoWindow.MarkerSpecification markerSpec =
                                                new InfoWindow.MarkerSpecification(20, 90);


                                        FormFragment fragment = new FormFragment();
                                        Bundle args = new Bundle();
                                        args.putParcelable("Retailer", retailersModel);
                                        fragment.setArguments(args);
                                        fragment.setClickListener(ClusteringDemoActivity.this);

//                                        InfoWindow infoWindow = new InfoWindow(marker, markerSpec, fragment);
//                                        infoWindowManager.toggle(infoWindow, true);

                                    }
                                    return true;
                                }
                            });*/
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSessionError(String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onClusterItemInfoWindowClick(RetailersModel item) {
        clickedClusterItem = item;
    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyCustomAdapterForItems() {
            myContentsView = getLayoutInflater().inflate(
                    R.layout.info_window, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {

            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.txtTitle));
            Button btn = (Button) myContentsView.findViewById(R.id.btn);

            tvTitle.setText(clickedClusterItem.getTitle());

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ClusteringDemoActivity.this, clickedClusterItem.getTitle(), Toast.LENGTH_SHORT).show();
                }
            });
            return myContentsView;
        }
    }
}