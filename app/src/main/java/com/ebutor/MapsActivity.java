package com.ebutor;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ebutor.fragments.MapsFragment;
import com.ebutor.models.BeatModel;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MapsActivity extends ParentActivity {

    public DrawerLayout mDrawerLayout;
    public ActionBarDrawerToggle mDrawerToggle;
    public RecyclerView rvMaps;
    private BeatModel beatModel;
    private boolean isFromManageBeats = false;
    public static Activity instnace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        instnace = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.update_beats));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().hasExtra("beatModel")) {
            beatModel = (BeatModel) getIntent().getSerializableExtra("beatModel");
        }

        if (getIntent().hasExtra("isFromManageBeats")) {
            isFromManageBeats = getIntent().getBooleanExtra("isFromManageBeats", false);
        }

        if (savedInstanceState == null) {
            Fragment fragment = new MapsFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("beatModel", beatModel);
            bundle.putBoolean("isFromManageBeats", isFromManageBeats);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }

        rvMaps = (RecyclerView) findViewById(R.id.rv_maps);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle = new ActionBarDrawerToggle(
                MapsActivity.this,                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                MapsActivity.this.supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                MapsActivity.this.supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()

            }
        };

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {

                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
            if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {

                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            } else {

                mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
