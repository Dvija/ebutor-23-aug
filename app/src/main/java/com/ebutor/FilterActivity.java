package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.LinearLayout;

import com.ebutor.fragments.FilterFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FilterActivity extends BaseActivity {

    private LinearLayout llFilter;
    private String filterData = "", type = "", inputId = "", keyId = "";
    private boolean isTabs;
    private FilterFragment fr;
    private int cartCount = 0;
    private boolean displayStar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        llFilter = (LinearLayout) inflater.inflate(R.layout.activity_filter, null, false);
        flbody.addView(llFilter, baseLayoutParams);

        setContext(FilterActivity.this);

        if (getIntent().getStringExtra("FilterString") != null)
            filterData = getIntent().getStringExtra("FilterString");
        if (getIntent().getStringExtra("Type") != null)
            type = getIntent().getStringExtra("Type");
        if (getIntent().getStringExtra("Id") != null)
            inputId = getIntent().getStringExtra("Id");
        if (getIntent().hasExtra("key_id"))
            keyId = getIntent().getStringExtra("key_id");
        if (getIntent().getBooleanExtra("Tabs", false))
            isTabs = getIntent().getBooleanExtra("Tabs", false);
        if (getIntent().hasExtra("displayStar"))
            displayStar = getIntent().getBooleanExtra("displayStar", false);

        fr = new FilterFragment();
        Bundle bundle = new Bundle();
        bundle.putString("FilterString", filterData);
        bundle.putString("Type", type);
        bundle.putString("Id", inputId);
        bundle.putString("key_id", keyId);
        bundle.putBoolean("Tabs", isTabs);
        bundle.putBoolean("displayStar", displayStar);
        fr.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frag_product, fr);
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void updateCart(int cartCount) {
        this.cartCount = cartCount;
        invalidate(cartCount);
    }

}
