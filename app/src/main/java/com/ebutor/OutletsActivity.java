package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.ebutor.fragments.OutletsSearchableFragmentNew;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OutletsActivity extends BaseActivity {

    private LinearLayout llOutlets;
    private boolean isFromPayment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llOutlets = (LinearLayout) inflater.inflate(R.layout.empty_fragment_layout, null, false);
        flbody.addView(llOutlets, baseLayoutParams);

        setContext(OutletsActivity.this);

        if (getIntent().hasExtra("isFromPayment")) {
            isFromPayment = getIntent().getBooleanExtra("isFromPayment", false);
        }

        if (isFromPayment) {
            setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        if (savedInstanceState == null) {
//            Fragment fragment = new MapsFragment();
            Fragment fragment = new OutletsSearchableFragmentNew();
            Bundle bundle = new Bundle();
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
