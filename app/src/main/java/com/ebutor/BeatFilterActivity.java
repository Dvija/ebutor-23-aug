package com.ebutor;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.ebutor.fragments.BeatFilterFragment;
import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BeatFilterActivity extends ParentActivity {

    private ArrayList<CustomerTyepModel> arrFilterBeats;
    private ArrayList<String> arrSelectedBeats;
    private String selDcId, selHubIds, selBeatIds, selBeatNames, selHubNames, beatsArray;
    private SharedPreferences mSharedPreferences;
    private boolean showBeats;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_beat_filter);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);

        if (getIntent().hasExtra("FilterBeats")) {
            arrFilterBeats = (ArrayList<CustomerTyepModel>) getIntent().getSerializableExtra("FilterBeats");
        }
        if (getIntent().hasExtra("SelectedBeats")) {
            arrSelectedBeats = (ArrayList<String>) getIntent().getSerializableExtra("SelectedBeats");
        }
        if (getIntent().hasExtra("selDcId")) {
            selDcId = getIntent().getStringExtra("selDcId");
        }
        if (getIntent().hasExtra("selBeatIds")) {
            selBeatIds = getIntent().getStringExtra("selBeatIds");
        }
        if (getIntent().hasExtra("selHubIds")) {
            selHubIds = getIntent().getStringExtra("selHubIds");
        }
        if (getIntent().hasExtra("selBeatNames")) {
            selBeatNames = getIntent().getStringExtra("selBeatNames");
        }
        if (getIntent().hasExtra("selHubNames")) {
            selHubNames = getIntent().getStringExtra("selHubNames");
        }
        if (getIntent().hasExtra("beatsArray")) {
            beatsArray = getIntent().getStringExtra("beatsArray");
        }
        if (getIntent().hasExtra("showBeats"))
            showBeats = getIntent().getBooleanExtra("showBeats", false);

        if (savedInstanceState == null) {
            Fragment fragment = new BeatFilterFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("FilterBeats", arrFilterBeats);
            bundle.putSerializable("SelectedBeats", arrSelectedBeats);
            bundle.putString("selDcId", selDcId);
            bundle.putString("selBeatIds", selBeatIds);
            bundle.putString("selBeatNames", selBeatNames);
            bundle.putString("selHubNames", selHubNames);
            bundle.putString("beatsArray", beatsArray);
            bundle.putBoolean("showBeats", showBeats);
            bundle.putSerializable("selHubIds", selHubIds);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (!showBeats && MapsActivity.instnace != null)
            MapsActivity.instnace.finish();
        super.onBackPressed();
    }
}
