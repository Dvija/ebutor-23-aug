package com.ebutor;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.github.mzule.activityrouter.annotation.Router;

import java.util.List;

/**
 * Created by Srikanth Nama on 4/18/2016.
 */
@Router(value = {"http://mzule.com/main", "main", "home"}, longExtra = {"status", "name"}, boolExtra = "web", transfer = "web=>fromWeb")
public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_test);

        Uri data = getIntent().getData();
        String scheme = data.getScheme(); // "http"
        String host = data.getHost(); // "twitter.com"
        List<String> params = data.getPathSegments();
        String first = params.get(0); // "status"
        String second = params.get(1); // "1234"


        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(first+" "+second );

    }

    public void CallIntent(View v) {
        startActivity(new Intent("com.factail.action.RESERVE",
                Uri.parse("factail://opentable.com/2947?partySize=3")));
    }
}
