package com.ebutor;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.ebutor.fragments.DashBoardFragmentNew;
import com.ebutor.fragments.RoutesFragment;
import com.ebutor.fragments.UnBilledSKUsFragment;
import com.ebutor.fragments.UnbilledOutletsFragment;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;
import com.ebutor.utils.Utils;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class FFDashboardActivity extends BaseActivity {

    Fragment fragment = null;
    private ViewPager mViewPager;
    private SharedPreferences mSharedPreferences;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ArrayList<String> mTitles;
    private int cartCount = 0;
    private LinearLayout llDashBoard;
    private String beatId = "";
    private boolean isCheckOut = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llDashBoard = (LinearLayout) inflater.inflate(R.layout.activity_ff_dashboard, null, false);
        flbody.addView(llDashBoard, baseLayoutParams);

        setContext(FFDashboardActivity.this);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        findViewById(R.id.iv_icon).setVisibility(View.VISIBLE);

        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

        beatId = mSharedPreferences.getString(ConstantValues.KEY_BEAT_ID, "");

        if (getIntent().hasExtra("isCheckout")) {
            isCheckOut = getIntent().getBooleanExtra("isCheckout", false);
        }

        mTitles = new ArrayList<>();
        mTitles.add(getResources().getString(R.string.routes));
        /*if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.SO_DASHBOARD_FEATURE_CODE) > 0)
            mTitles.add(getResources().getString(R.string.so));*/
//        mTitles.add(getResources().getString(R.string.po));
        if (!TextUtils.isEmpty(beatId)) {
            mTitles.add(getResources().getString(R.string.unbilled_outlets));
            mTitles.add(getResources().getString(R.string.unbilled_skus));
        }
        mTitles.add(getResources().getString(R.string.home));

        if (isCheckOut) {
            tvRetailerName.setText("No Retailer Selected");
            tvPerson.setText(mSharedPreferences.getString(ConstantValues.KEY_FF_NAME, ""));
        }

        mViewPager = (ViewPager) findViewById(R.id.container);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        if (TextUtils.isEmpty(beatId)) {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        } else {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        tabLayout.setTabTextColors(getResources().getColor(R.color.text_color), getResources().getColor(R.color.primary));
        setTabTitles(tabLayout);

    }

    public void updateCart(int cartCount) {
        this.cartCount = cartCount;
        invalidate(cartCount);
    }

    private void setTabTitles(TabLayout tabLayout) {
        tabLayout.getTabAt(0).setText(mTitles.get(0));
        tabLayout.getTabAt(1).setText(mTitles.get(1));
        if (!TextUtils.isEmpty(beatId)) {
            tabLayout.getTabAt(2).setText(mTitles.get(2));
            tabLayout.getTabAt(3).setText(mTitles.get(3));
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        updateCart(this.cartCount);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).


            /*if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.SO_DASHBOARD_FEATURE_CODE) > 0) {
                switch (position) {

                    case 0:
                        fragment = RoutesFragment.newInstance();
                        break;
                    case 1:
                        fragment = new SODashBoardFragmentNew();
                        break;
                    case 2:
                        if (!TextUtils.isEmpty(beatId)) {
                            fragment = UnbilledOutletsFragment.newInstance();
                        } else {
                            fragment = DashBoardFragmentNew.newInstance(DashBoardTypes.ORGANIZATION);
                        }

                        break;
                    case 3:
                        fragment = UnBilledSKUsFragment.newInstance();
                        break;
                    case 4:
                        fragment = DashBoardFragmentNew.newInstance(DashBoardTypes.ORGANIZATION);
                        break;
                    default:
                        fragment = UnbilledOutletsFragment.newInstance();
                        break;
                }

            } else {*/
            switch (position) {

                case 0:
                    fragment = RoutesFragment.newInstance();
                    break;
                case 1:
                    if (!TextUtils.isEmpty(beatId)) {
                        fragment = UnbilledOutletsFragment.newInstance();
                    } else {
                        fragment = DashBoardFragmentNew.newInstance(DashBoardTypes.ORGANIZATION);
                    }
                    break;
                case 2:
                    fragment = UnBilledSKUsFragment.newInstance();
                    break;
                case 3:
                    fragment = DashBoardFragmentNew.newInstance(DashBoardTypes.ORGANIZATION);
                    break;
                default:
                    fragment = UnbilledOutletsFragment.newInstance();
                    break;
//                }
            }

            return fragment;
        }


        @Override
        public int getCount() {
            return mTitles.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }

    }
}
