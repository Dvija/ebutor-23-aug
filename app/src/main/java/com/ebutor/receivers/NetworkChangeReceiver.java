package com.ebutor.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Srikanth Nama on 05-Jan-17.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null) {
            if (info.isConnected()) {
                // you got a connection! tell your user!
                Log.i("Post", "Connected");
//                Intent intent_service = new Intent(context, UploadOfflineExpensesService.class);
//                context.startService(intent_service);
            }
        }
    }
}
