package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.widget.LinearLayout;

import com.ebutor.fragments.MyWishListFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WishListActivity extends BaseActivity {

    private LinearLayout llWishList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llWishList = (LinearLayout) inflater.inflate(R.layout.empty_fragment_layout, null, false);
        flbody.addView(llWishList, baseLayoutParams);

        setContext(WishListActivity.this);

        if (savedInstanceState == null) {
            Fragment fragment = new MyWishListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
