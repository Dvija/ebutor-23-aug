package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.widget.LinearLayout;

import com.ebutor.fragments.AddNewAddressFragment;
import com.ebutor.fragments.AddressesFragment;
import com.ebutor.models.UserAddressModel;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by Srikanth Nama on 2/29/2016.
 */
public class UserAddressActivity extends BaseActivity {

    private LinearLayout llUserAddress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llUserAddress = (LinearLayout) inflater.inflate(R.layout.empty_fragment_layout,null,false);
        flbody.addView(llUserAddress,baseLayoutParams);

        setContext(UserAddressActivity.this);

        ArrayList<UserAddressModel> addresses = null;
        if (getIntent().hasExtra("Addresses")) {
            addresses = (ArrayList<UserAddressModel>) getIntent().getSerializableExtra("Addresses");
        }

        if (savedInstanceState == null) {
            if (addresses != null && addresses.size() > 0) {
                Fragment fragment = new AddressesFragment();
                Bundle args = new Bundle();
                args.putSerializable("Addresses", addresses);
                fragment.setArguments(args);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, fragment)
                        .commit();
            } else {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new AddNewAddressFragment())
                        .commit();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
