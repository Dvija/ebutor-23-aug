package com.ebutor;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;

public class MyTableAdapter extends BaseTableAdapter {

//    private String headers[][] = new String[][]{{
//            "Header 1",
//            "Header 2",
//            "Header 3",
//            "Header 4",
//            "Header 5",
//            "Header 6"},
//            {
//                    "Lorem",
//                    "sed",
//                    "do",
//                    "eiusmod",
//                    "tempor",
//                    "incididunt"},
//            {
//                    "ipsum",
//                    "irure",
//                    "occaecat",
//                    "enim",
//                    "laborum",
//                    "reprehenderit"},
//            {
//                    "dolor",
//                    "fugiat",
//                    "nulla",
//                    "reprehenderit",
//                    "laborum",
//                    "consequat"},
//            {
//                    "sit",
//                    "consequat",
//                    "laborum",
//                    "fugiat",
//                    "eiusmod",
//                    "enim"},
//            {
//                    "amet",
//                    "nulla",
//                    "Excepteur",
//                    "voluptate",
//                    "occaecat",
//                    "et"},
//            {
//                    "consectetur",
//                    "occaecat",
//                    "fugiat",
//                    "dolore",
//                    "consequat",
//                    "eiusmod"},
//            {
//                    "adipisicing",
//                    "fugiat",
//                    "Excepteur",
//                    "occaecat",
//                    "fugiat",
//                    "laborum"},
//            {
//                    "elit",
//                    "voluptate",
//                    "reprehenderit",
//                    "Excepteur",
//                    "fugiat",
//                    "nulla"},
//            {
//                    "Lorem",
//                    "sed",
//                    "do",
//                    "eiusmod",
//                    "tempor",
//                    "incididunt"},
//            {
//                    "ipsum",
//                    "irure",
//                    "occaecat",
//                    "enim",
//                    "laborum",
//                    "reprehenderit"},
//            {
//                    "dolor",
//                    "fugiat",
//                    "nulla",
//                    "reprehenderit",
//                    "laborum",
//                    "consequat"},
//            {
//                    "sit",
//                    "consequat",
//                    "laborum",
//                    "fugiat",
//                    "eiusmod",
//                    "enim"},
//            {
//                    "amet",
//                    "nulla",
//                    "Excepteur",
//                    "voluptate",
//                    "occaecat",
//                    "et"},
//            {
//                    "consectetur",
//                    "occaecat",
//                    "fugiat",
//                    "dolore",
//                    "consequat",
//                    "eiusmod"},
//            {
//                    "adipisicing",
//                    "fugiat",
//                    "Excepteur",
//                    "occaecat",
//                    "fugiat",
//                    "laborum"},
//            {
//                    "elit",
//                    "voluptate",
//                    "reprehenderit",
//                    "Excepteur",
//                    "fugiat",
//                    "nulla"},
//            {
//                    "Lorem",
//                    "sed",
//                    "do",
//                    "eiusmod",
//                    "tempor",
//                    "incididunt"},
//            {
//                    "ipsum",
//                    "irure",
//                    "occaecat",
//                    "enim",
//                    "laborum",
//                    "reprehenderit"},
//            {
//                    "dolor",
//                    "fugiat",
//                    "nulla",
//                    "reprehenderit",
//                    "laborum",
//                    "consequat"},
//            {
//                    "sit",
//                    "consequat",
//                    "laborum",
//                    "fugiat",
//                    "eiusmod",
//                    "enim"},
//            {
//                    "amet",
//                    "nulla",
//                    "Excepteur",
//                    "voluptate",
//                    "occaecat",
//                    "et"},
//            {
//                    "consectetur",
//                    "occaecat",
//                    "fugiat",
//                    "dolore",
//                    "consequat",
//                    "eiusmod"},
//            {
//                    "adipisicing",
//                    "fugiat",
//                    "Excepteur",
//                    "occaecat",
//                    "fugiat",
//                    "laborum"},
//            {
//                    "elit",
//                    "voluptate",
//                    "reprehenderit",
//                    "Excepteur",
//                    "fugiat",
//                    "nulla"},
//    };

    private final static int WIDTH_DIP = 110;
    private final static int HEIGHT_DIP = 50;
    private final float density;
    private final int[] widths = {150, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
    private final int width;
    private final int height;
    private Context context;
    private String[][] dataArray;

    public MyTableAdapter(Context context, String[][] dataArray, int[] widths) {
        super();
        this.context = context;
        this.dataArray = dataArray;
//        this.widths = widths;
        density = context.getResources().getDisplayMetrics().density;
        Resources r = context.getResources();
        width = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, WIDTH_DIP, r.getDisplayMetrics()));
        height = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, HEIGHT_DIP, r.getDisplayMetrics()));

    }

    @Override
    public int getRowCount() {
        return dataArray.length - 1;
    }

    @Override
    public int getColumnCount() {
        return dataArray[0].length - 1;
    }

    @Override
    public View getView(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_table, parent, false);
        }

        TextView textView = ((TextView) convertView.findViewById(R.id.textview));
//        if(row == -1 || column == -1)
//            textView.setTextS
        textView.setText(dataArray[row + 1][column + 1].toString());
        return convertView;
    }

    @Override
    public int getWidth(int column) {
//        return width;
        return Math.round(widths[column + 1] * density);
    }

    @Override
    public int getHeight(int row) {
        return height;
    }

    @Override
    public int getItemViewType(int row, int column) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }
}
