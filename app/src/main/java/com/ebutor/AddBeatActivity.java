package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.ebutor.fragments.AddBeatFragment;
import com.ebutor.models.BeatModel;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddBeatActivity extends ParentActivity {

    private BeatModel beatModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_beat);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.manage_beats));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().hasExtra("beatModel")) {
            beatModel = (BeatModel) getIntent().getSerializableExtra("beatModel");
        }

        if (savedInstanceState == null) {
            Fragment fragment = new AddBeatFragment();
            Bundle bundle = new Bundle();
            if (beatModel != null)
                bundle.putSerializable("beatModel", beatModel);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
