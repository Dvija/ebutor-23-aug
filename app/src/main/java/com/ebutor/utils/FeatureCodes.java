package com.ebutor.utils;

/**
 * Created by Srikanth Nama on 14-Dec-16.
 */

public class FeatureCodes {

    // COMMON
    public static final String PLACE_ORDER_FEATURE_CODE = "";
    public static final String ORDERS_FEATURE_CODE = "MOR001";
    public static final String CANCEL_ORDER_FEATURE_CODE = "MOR007";
    public static final String RETURN_ORDER_FEATURE_CODE = "MOR008";
    public static final String ORDER_DETAILS_FEATURE_CODE = "MOR002";
    public static final String EDIT_PROFILE_FEATURE_CODE = "MRT002";
    public static final String DOWNLOAD_INVOICE_FEATURE_CODE = "MOR006";
    public static final String PRIVACY_POLICY_FEATURE_CODE = "PRIVACY_POLICY";
    public static final String RATE_US_FEATURE_CODE = "RATE_US";
    public static final String ECASH_FEATURE_CODE = "ECASH";
    public static final String CREDIT_LIMIT_FEATURE_CODE = "CREDIT_LIMIT";
    public static final String PAYMENT_DUE_FEATURE_CODE = "PAYMENT_DUE";
    public static final String SCAN_FEATURE_CODE = "MOS001";

    // CRM
    public static final String DASHBOARD_FEATURE_CODE = "MFD001";
    public static final String ADD_RETAILER_FEATURE_CODE = "MRT001";
    public static final String CHECKOUT_FEATURE_CODE = "CHECKOUT";
    public static final String CLEAR_DATA_FEATURE_CODE = "MCD001";
    public static final String UPDATE_BEATS_FEATURE_CODE = "MUB001";
    public static final String MANAGE_BEATS_FEATURE_CODE = "MMB001";
    public static final String SO_DASHBOARD_FEATURE_CODE = "MSD001";
    public static final String EDIT_SEGMENT_FEATURE_CODE = "MES001";

    //SRM
    public static final String SUPPLIERS_FEATURE_CODE = "MSP001";
    public static final String CREATE_SUPPLIER_FEATURE_CODE = "MSP002";
    public static final String SUBSCRIBE_PRODUCTS_FEATURE_CODE = "MSP003";
    public static final String CREATE_PO_FEATURE_CODE = "MPO002";
    public static final String EDIT_PO_FEATURE_CODE = "MPO007";
    public static final String INVENTORY_FEATURE_CODE = "MIN001";
    public static final String APPROVE_PO_FEATURE_CODE = "MPO001";


    // Expenses Module
    public static final String EXPENSES_FEATURE_CODE = "MEX001";
    public static final String EXPENSES_APPROVAL_FEATURE_CODE = "MEX002";
    public static final String EXPENSES_TALLEY_FEATURE_CODE = "MEX003";

    // Sales Agent
    public static final String SALES_AGENT_FEATURE_CODE = "SSAG01";
}
