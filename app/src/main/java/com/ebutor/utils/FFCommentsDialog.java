package com.ebutor.utils;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ebutor.R;
import com.ebutor.adapters.FFCommentsRecyclerAdapter;
import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;

public class FFCommentsDialog extends DialogFragment {
    // this method create view for your Dialog
    ArrayList<CustomerTyepModel> ffOptions;
    private RecyclerView mRecyclerView;
    private FFCommentsRecyclerAdapter adapter;
    private OnFFOptionSelectedListener mListener;

    public interface OnFFOptionSelectedListener{
        void OnFFOptionSelected(Object object);
    }

    public static FFCommentsDialog newInstance(ArrayList<CustomerTyepModel> ffOptions) {
        FFCommentsDialog dialog = new FFCommentsDialog();
        Bundle args = new Bundle();
        args.putSerializable("Options", ffOptions);
        dialog.setArguments(args);
        return dialog;
    }

    public void setListener(OnFFOptionSelectedListener listener){
        this.mListener =  listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ff_comments_dialog, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ffOptions = new ArrayList<>();

        if (getArguments() != null && getArguments().containsKey("Options")) {
            ffOptions = (ArrayList<CustomerTyepModel>) getArguments().getSerializable("Options");
        }

        adapter = new FFCommentsRecyclerAdapter(getActivity(), ffOptions);
        adapter.setListener(new FFCommentsRecyclerAdapter.onOptionSelectedListener() {
            @Override
            public void onOptionSelected(Object object, int position) {
                dismiss();
                if(mListener!=null)
                    mListener.OnFFOptionSelected(object);
            }
        });
        mRecyclerView.setAdapter(adapter);

        return v;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
}
