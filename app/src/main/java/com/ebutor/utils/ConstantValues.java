package com.ebutor.utils;

import java.util.HashMap;

public class ConstantValues {

    public static final String S3_BUCKET_URL = "http://s3.ap-south-1.amazonaws.com/supplier1204/";
    public static final String BASE_URL = "http://prelive.ebutor.com/";
    public static final String NEW_BASE_URL_AUTH = BASE_URL + "cpmanager/";
    public static final String EXPENSE_MODULE_URL = BASE_URL + "expensestracker/API/";

    //    public static final String AUTH_dTOKEN = "tnky64densu85ioi";
    public static final String MODULE_ID = "4005";

    public static final String GOOGLE_PROJECT_ID = "215775723780"; // Google Project number
    public static final String FCM_SENDER_ID = "1037002584167"; // FCM Project number

    public static final String KEY_GCM_TOKEN = "gcm_token";

    public static final String KEY_APP_ID = "appId";
    public static final String KEY_IS_FIRST_TIME_USER = "isFirstUser";
    public static final String KEY_IS_TOOL_TIP = "isToolTip";
    public static final String KEY_IS_LOGGED_IN = "IsLoggedIn";
    public static final String KEY_IS_INCOMPLETE_REG = "IsInCompleteReg";
    public static final String KEY_CUSTOMER_ID = "customerId";
    public static final String KEY_CUSTOMER_GRP_ID = "customerGroupId";
    public static final String CUSTOMER_GRP_ID = "customer_groupId";
    public static final String KEY_ORDER_ID = "orderId";
    public static final String KEY_CUSTOMER_TOKEN = "customerToken";
    public static final String KEY_SEGMENT_ID = "segment_id";
    public static final String SEGMENT_ID = "segmentId";
    //    public static final String outletsa = "buyer_type_id";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_PROFILE_IMAGE = "profile_image";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_WH_IDS = "whIds";
    public static final String KEY_LE_WH_IDS = "leWhIds";
    public static final String KEY_FF_HUB_ID = "ff_hub_id";
    public static final String KEY_HUB_ID = "hub_id";
    public static final String KEY_SRM_WH_ID = "srm_wh_id";
    public static final String KEY_SEL_SUPPLIER_ID = "sel_supplier_id";
    public static final String KEY_LEGAL_ENTITY_ID = "key_legal_entity_id";
    public static final String KEY_FF_LEGAL_ENTITY_ID = "key_ff_legal_entity_id";
    public static final String KEY_OTP_POPUP = "key_otp_popup";
    public static final String KEY_IS_HUB_EMPTY = "key_is_hub_empty";
    public static final String GST_CODES = "gst_codes";
    public static final String KEY_CHECKIN_DISTANCE = "checkin_distance";

    public static final String KEY_HAS_CHILD = "has_child";
    public static final String KEY_IS_DASHBOARD = "is_dashboard";
    public static final String KEY_IS_FF = "is_field_force";
    public static final String KEY_SHOP_NAME = "selected_shop_name";

    public static final String KEY_FF_NAME = "field_force_name";
    public static final String KEY_FF_ID = "field_force_id";
    //    public static final String KEY_RETAILER_TOKEN = "retailer_token";
    public static final String KEY_FF_TOKEN = "FFToken";
    public static final String KEY_RETAILER_NAME = "retailer_name";
    public static final String KEY_IS_SELECTED_RETAILER = "is_selected_retailer";
    public static final String KEY_LAST_UPDATED_DATE = "last_updated_date";

    //ecash keys
    public static final String KEY_CREDIT_LIMIT = "credit_limit";
    public static final String KEY_FF_CREDIT_LIMIT = "ff_credit_limit";
    public static final String KEY_ECASH = "ecash";
    public static final String KEY_FF_ECASH = "ff_ecash";
    public static final String KEY_PAYMENT_DUE = "payment_due";
    public static final String KEY_FF_PAYMENT_DUE = "ff_payment_due";

    public static final String KEY_SRM_TOKEN = "srm_token";
    public static final String KEY_SRM_ID = "srm_id";

    public static final String SPLASH_PROGRESS = "splash_progress";
    public static final String PROGRESS = "progress";
    public static final String KEY_SYNC_INTERVAL = "sync_interval";
    public static final int KEY_SYNC_INTERVAL_DURATION = 24 * 60;
    public static final int KEY_SYNC_CART_INTERVAL = 2 * 60;
    public static final String HELP_PROGRESS = "help_progress";
    public static final String TABS_PROGRESS = "tabs_progress";
    public static final String TOOLBAR_PROGRESS = "toolbar_progress";

    public static final String SEE_ALL = "See All";

    public static final String KEY_SUPPLIER_NAME = "supplier_name";
    public static final String KEY_IS_CHECKIN = "is_checkin";
    public static final String KEY_BEAT_ID = "beat_id";
    public static final String KEY_CUSTOMER_BEAT_ID = "customer_beat_id";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String AWS_BUCKET_NAME = "supplier1204";
    public static final String AWS_FOLDER_PATH = "android/";

    public static final String PLATFORM_ID = "5004";
    public static final String DEVICE_ID = "device_id";
    public static final String IP_ADDRESS = "ip_address";
    public static final String FCM_ID = "fcm_id";

    public static final String DC_TYPE = "118001";
    public static final String HUB_TYPE = "118002";
    public static final String SPOKE_TYPE = "118003";
    public static final String BEAT_TYPE = "118004";
    public static final String AREA_TYPE = "118005";

    // cashback source type
    public static final int PRODUCT = 2;
    public static final int ORDER = 1;

    // cashback beneficiary type

    public static final int CUSTOMER = 62;
    public static final int FF_MANAGER = 52;
    public static final int FF_ASSOCIATE = 53;
    public static final int SALES_AGENT = 89;
    public static final String KEY_IS_SALES_AGENT = "is_sales_agent";


    // cashback value type

    public static final int CASHBACK_TYPE_VALUE = 0;
    public static final int CASHBACK_TYPE_PERCENTAGE = 1;

    public static final int EACHES = 16001;

    public static HashMap<String, String> getRequestHeaders() {
        HashMap<String, String> headers = new HashMap<>();

        headers.put("Content-Type", "application/json");
        headers.put("auth", "E446F5E53AD8835EAA4FA63511E22");

        return headers;
    }

}
