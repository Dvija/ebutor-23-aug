package com.ebutor.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import com.ebutor.R;

public class EditTextWithDeleteButton extends LinearLayout {	
	public EditText editText;
	public ImageButton clearTextButton,searchButton;
	private Drawable img;

	public interface TextChangedListener extends TextWatcher{
	}
	TextChangedListener editTextListener = null;
	public void addTextChangedListener(TextChangedListener listener) {
        this.editTextListener = listener;
    }
	public EditTextWithDeleteButton(Context context) {
		super(context);
		LayoutInflater.from(context).inflate(R.layout.edittext_delere, this);
	}

	public EditTextWithDeleteButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public EditTextWithDeleteButton(Context context, AttributeSet attrs, int defStyle) {
		this(context, attrs);
		initViews(context, attrs);
	}

	private void initViews(Context context, AttributeSet attrs) {
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
				R.styleable.EditTextWithDeleteButton, 0, 0);
		String hintText;
		int deleteButtonRes,searchButtonRes;
		try {
			// get the text and colors specified using the names in attrs.xml
			hintText = "Search here..";
			deleteButtonRes = a.getResourceId(
					R.styleable.EditTextWithDeleteButton_deleteButtonRes,
					R.drawable.search_close);
			searchButtonRes = a.getResourceId(
					R.styleable.EditTextWithDeleteButton_deleteButtonRes,
					R.drawable.ic_search_filter);

		} finally {
			a.recycle();
		}
		img = getContext().getResources().getDrawable(R.drawable.ic_search_filter);
		editText = createEditText(context, hintText);
		clearTextButton = createImageButton(context, deleteButtonRes);
		searchButton = createSearchButton(context, searchButtonRes);

		this.addView(editText);
		this.addView(clearTextButton);
		this.addView(searchButton);
		editText.addTextChangedListener(txtEntered());
	

		editText.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus && editText.getText().toString().length() > 0){
					clearTextButton.setVisibility(View.VISIBLE);
					searchButton.setVisibility(GONE);
					editText.setCompoundDrawables(null,null,null,null);
				}
				else {
					clearTextButton.setVisibility(View.GONE);
					searchButton.setVisibility(VISIBLE);
					editText.setCompoundDrawables(null, null, img, null);
				}

			}
		});
		clearTextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				editText.setText("");
				searchButton.setVisibility(VISIBLE);
				editText.setCompoundDrawables(null, null, img, null);
			}
		});
	}



	public TextWatcher txtEntered() {
		return new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				 if (editTextListener != null)
			            editTextListener.onTextChanged(s, start, before, count);

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (editTextListener != null)
		            editTextListener.afterTextChanged(s);
				if (editText.getText().toString().length() > 0){
					editText.setCompoundDrawables(null, null, null, null);
					clearTextButton.setVisibility(View.VISIBLE);
					searchButton.setVisibility(View.GONE);
				}
				else {
					clearTextButton.setVisibility(View.GONE);
					searchButton.setVisibility(View.VISIBLE);
					editText.setCompoundDrawables(null, null, img, null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				if (editTextListener != null)
		            editTextListener.beforeTextChanged(s, start, count, after);

			}

		};
	}

	@SuppressLint("NewApi")
	private EditText createEditText(Context context, String hintText) {
		editText = new EditText(context);
		editText.setRawInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
		editText.setLayoutParams(new TableLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
		editText.setHorizontallyScrolling(false);
		editText.setVerticalScrollBarEnabled(true);
		editText.setGravity(Gravity.LEFT);
		editText.setCompoundDrawables(null,null,img,null);
		editText.setTextColor(getResources().getColor(R.color.text_color));
		editText.setBackground(null);
		editText.setHint(hintText);
		return editText;
	}

	private ImageButton createImageButton(Context context, int deleteButtonRes) {
		clearTextButton = new ImageButton(context);
		LayoutParams params = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		params.gravity = Gravity.CENTER_VERTICAL;
		clearTextButton.setLayoutParams(params);
		clearTextButton.setBackgroundResource(deleteButtonRes);
		clearTextButton.setVisibility(View.GONE);
		return clearTextButton;
	}

	private ImageButton createSearchButton(Context context, int searchButtonRes) {
		searchButton = new ImageButton(context);
		LayoutParams params = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		params.gravity = Gravity.CENTER_VERTICAL;
		searchButton.setLayoutParams(params);
		searchButton.setBackgroundResource(searchButtonRes);
		searchButton.setVisibility(View.VISIBLE);
		return searchButton;
	}

}
