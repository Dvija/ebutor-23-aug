package com.ebutor.utils;

/**
 * Created by wds1 on 7/1/2015.
 */
public interface IPredicate<T> { boolean apply(T type); }
