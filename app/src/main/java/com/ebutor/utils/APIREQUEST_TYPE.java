package com.ebutor.utils;

/**
 * Created by 300041 on 1/20/2015.
 */
public enum APIREQUEST_TYPE {

    HTTP_GET, HTTP_POST, HTTP_DELETE
}
