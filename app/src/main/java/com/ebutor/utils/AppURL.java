package com.ebutor.utils;

/**
 * Created by Srikanth Nama on 11-Jul-16.
 */
public class AppURL {

    public static final int initial_time_out = 200000;

    //=======================NEW API'S==============================================
    public static final String masterLookUpURL = ConstantValues.NEW_BASE_URL_AUTH + "getMasterLookup";
    public static final String categoriesURL = ConstantValues.NEW_BASE_URL_AUTH + "getCategories";
    public static final String appUpdateURL = ConstantValues.NEW_BASE_URL_AUTH + "versioncheck";
    public static final String tabsURL = ConstantValues.NEW_BASE_URL_AUTH + "getheaders";
    public static final String bannersURL = ConstantValues.NEW_BASE_URL_AUTH + "getBanner";
    public static final String featuredOffersSeeAllURL = ConstantValues.NEW_BASE_URL_AUTH + "getfeaturedproductsViewall";
    public static final String registrationURL = ConstantValues.NEW_BASE_URL_AUTH + "registration";
    public static final String getOTPURL = ConstantValues.NEW_BASE_URL_AUTH + "getotp";
    public static final String homePageBannersURL = ConstantValues.NEW_BASE_URL_AUTH + "getfeaturedproducts";
    public static final String profileURL = ConstantValues.NEW_BASE_URL_AUTH + "profile";
    public static final String updateProfileURL = ConstantValues.NEW_BASE_URL_AUTH + "updateProfile";
    public static final String updateProfile1URL = ConstantValues.NEW_BASE_URL_AUTH + "updateprofile1";
    public static final String productDetailURL = ConstantValues.NEW_BASE_URL_AUTH + "productDetails";
    public static final String searchURL = ConstantValues.NEW_BASE_URL_AUTH + "getSearchAjax";
    public static final String addCartURL = ConstantValues.NEW_BASE_URL_AUTH + "addcart";
    public static final String viewCartURL = ConstantValues.NEW_BASE_URL_AUTH + "getcartdetails";
    public static final String cartCountURL = ConstantValues.NEW_BASE_URL_AUTH + "cartcount";
    public static final String deleteCartURL = ConstantValues.NEW_BASE_URL_AUTH + "deletecart";
    public static final String getStateCountriesURL = ConstantValues.NEW_BASE_URL_AUTH + "getCountries";
    public static final String addAddressURL = ConstantValues.NEW_BASE_URL_AUTH + "saveAddress";
    public static final String offerProductsURL = ConstantValues.NEW_BASE_URL_AUTH + "getOfferProducts";
    public static final String addOrderURL = ConstantValues.NEW_BASE_URL_AUTH + "createOrder1";
    public static final String shoppinglistURL = ConstantValues.NEW_BASE_URL_AUTH + "getShoppingList";
    public static final String productListOperationsURL = ConstantValues.NEW_BASE_URL_AUTH + "productListOperations";
    public static final String ordersListURL = ConstantValues.NEW_BASE_URL_AUTH + "getMyOrders";
    public static final String getAddressURL = ConstantValues.NEW_BASE_URL_AUTH + "getShippingAddress";
    public static final String cancelOrderURL = ConstantValues.NEW_BASE_URL_AUTH + "cancelOrder";
    public static final String returnOrderURL = ConstantValues.NEW_BASE_URL_AUTH + "returnOrder";
    public static final String addReviewURL = ConstantValues.NEW_BASE_URL_AUTH + "addReviewRating";
    public static final String filterDataURL = ConstantValues.NEW_BASE_URL_AUTH + "getFilters";
    public static final String shipmentTrackURL = ConstantValues.NEW_BASE_URL_AUTH + "tracking";
    public static final String orderDetailsURL = ConstantValues.NEW_BASE_URL_AUTH + "Orderdetails";
    public static final String returnReasonsURL = ConstantValues.NEW_BASE_URL_AUTH + "returnresons";
    public static final String cancelReasonsURL = ConstantValues.NEW_BASE_URL_AUTH + "cancelresons";
    public static final String sortingURL = ConstantValues.NEW_BASE_URL_AUTH + "sorting";
    public static final String searchProductsURL = ConstantValues.NEW_BASE_URL_AUTH + "search";
    public static final String applyFilterURL = ConstantValues.NEW_BASE_URL_AUTH + "search";
    public static final String multiVariantProductsURL = ConstantValues.NEW_BASE_URL_AUTH + "getCategories";
    public static final String editCartDataURL = ConstantValues.NEW_BASE_URL_AUTH + "editCart";
    public static final String pincodeURL = ConstantValues.NEW_BASE_URL_AUTH + "pincode";
    public static final String generateInvoiceURL = ConstantValues.NEW_BASE_URL_AUTH + "generateInvoice";
    public static final String offlineProductsURL = ConstantValues.NEW_BASE_URL_AUTH + "getOfflineProducts";
    public static final String getSlabRatesURL = ConstantValues.NEW_BASE_URL_AUTH + "getProductSlabs";
    public static final String getAllRetailersURL = ConstantValues.NEW_BASE_URL_AUTH + "retailers";
    public static final String getRetailerTokenURL = ConstantValues.NEW_BASE_URL_AUTH + "retailertoken";
    //    public static final String getPinCodeAreasURL = ConstantValues.NEW_BASE_URL_AUTH+"getPincodeAreas";
    public static final String updateRetailerURL = ConstantValues.NEW_BASE_URL_AUTH + "updateRetailerData";
    public static final String addCart1URL = ConstantValues.NEW_BASE_URL_AUTH + "addcart1";
    public static final String ffCommentsURL = ConstantValues.NEW_BASE_URL_AUTH + "ffcomments";
    public static final String getImagesDescURL = ConstantValues.NEW_BASE_URL_AUTH + "getMediaDesc";
    public static final String getReviewsSpecsURL = ConstantValues.NEW_BASE_URL_AUTH + "getReviewSpec";
    public static final String checkCartInventoryURL = ConstantValues.NEW_BASE_URL_AUTH + "CheckCartInventory";
    public static final String retailerDashboardURL = ConstantValues.NEW_BASE_URL_AUTH + "dashboardreport";
    public static final String getAllSuppliersURL = ConstantValues.NEW_BASE_URL_AUTH + "getsuppplierlist";
    public static final String getSupplierProductsURL = ConstantValues.NEW_BASE_URL_AUTH + "getSupplierProductLists";
    public static final String getPurchaseSlabsURL = ConstantValues.NEW_BASE_URL_AUTH + "getPurchaseSlabs";
    public static final String getPurchasePriceHistoryURL = ConstantValues.NEW_BASE_URL_AUTH + "getPurchasepricehistory";
    public static final String getPOListURL = ConstantValues.NEW_BASE_URL_AUTH + "getPolist";
    public static final String getPODetailURL = ConstantValues.NEW_BASE_URL_AUTH + "getPodetails";
    public static final String disableContactURL = ConstantValues.NEW_BASE_URL_AUTH + "DisableContactuser";
    public static final String getWarehouseListURL = ConstantValues.NEW_BASE_URL_AUTH + "getwarehouseslist";
    public static final String getInventoryURL = ConstantValues.NEW_BASE_URL_AUTH + "getInventory";
    public static final String createPOURL = ConstantValues.NEW_BASE_URL_AUTH + "createPO";
    public static final String getBeatsByFFIDURL = ConstantValues.NEW_BASE_URL_AUTH + "getBeatsbyffID";
    public static final String unBilledSKUsURL = ConstantValues.NEW_BASE_URL_AUTH + "UnBilledskus";
    public static final String feedbackReasonsURL = ConstantValues.NEW_BASE_URL_AUTH + "getFeedbackReasons";
    public static final String saveFeedbackReasonsURL = ConstantValues.NEW_BASE_URL_AUTH + "saveFeedbackReasons";
    public static final String srmProductsURL = ConstantValues.NEW_BASE_URL_AUTH + "getSrmProducts";
    public static final String updatePOURL = ConstantValues.NEW_BASE_URL_AUTH + "updatePO";
    public static final String getPinCodeDataURL = ConstantValues.NEW_BASE_URL_AUTH + "getPincodeData";
    public static final String getSupplierMasterLookupURL = ConstantValues.NEW_BASE_URL_AUTH + "getSupplierMasterlookupdata";
    public static final String createSupplierURL = ConstantValues.NEW_BASE_URL_AUTH + "createSupplier";
    public static final String getManufacturersURL = ConstantValues.NEW_BASE_URL_AUTH + "getmanufacturerlist";
    public static final String getManufacturerProducts = ConstantValues.NEW_BASE_URL_AUTH + "getmanufacturerproducts";
    public static final String subscribeProductURL = ConstantValues.NEW_BASE_URL_AUTH + "getSubscribeProducts";
    public static final String getPOMasterlookupdataURL = ConstantValues.NEW_BASE_URL_AUTH + "getPOMasterlookupdata";
    public static final String getfieldforcelistURL = ConstantValues.NEW_BASE_URL_AUTH + "getfieldforcelist";
    public static final String getsodashboardfiltersURL = ConstantValues.NEW_BASE_URL_AUTH + "getsodashboardfilters";
    public static final String getbeatsURL = ConstantValues.NEW_BASE_URL_AUTH + "getbeats";
    public static final String updateBeatURL = ConstantValues.NEW_BASE_URL_AUTH + "updatebeat";
    public static final String sendMailToFFURL = ConstantValues.NEW_BASE_URL_AUTH + "sendemailtoff";
    public static final String addSpokeURL = ConstantValues.NEW_BASE_URL_AUTH + "storespoke";
    public static final String getSODashboard = ConstantValues.NEW_BASE_URL_AUTH + "getsodashboard";
    public static final String updateGeoURL = ConstantValues.NEW_BASE_URL_AUTH + "updategeo";
    public static final String getallmanfproductsURL = ConstantValues.NEW_BASE_URL_AUTH + "getallmanfproducts";
    public static final String addBeatURL = ConstantValues.NEW_BASE_URL_AUTH + "storebeat";
    public static final String getAllDataURL = ConstantValues.NEW_BASE_URL_AUTH + "getalldata";
    public static final String generateOrderRefURL = ConstantValues.NEW_BASE_URL_AUTH + "genarateOrderref";
    public static final String getapprovalhistoryURL = ConstantValues.NEW_BASE_URL_AUTH + "getapprovalhistory";
    public static final String getApprovalOptionsURL = ConstantValues.NEW_BASE_URL_AUTH + "getApprovalOptions";
    public static final String submitapprovestatusURL = ConstantValues.NEW_BASE_URL_AUTH + "submitapprovestatus";
    public static final String generateotpURL = ConstantValues.NEW_BASE_URL_AUTH + "generateotp";
    public static final String orderotpconfirmURL = ConstantValues.NEW_BASE_URL_AUTH + "orderotpconfirm";
    public static final String getPoStatusListURL = ConstantValues.NEW_BASE_URL_AUTH + "getPoStatusList";
    public static final String getfilterorderstatusURL = ConstantValues.NEW_BASE_URL_AUTH + "getfilterorderstatus";
    public static final String getPolistByStatusURL = ConstantValues.NEW_BASE_URL_AUTH + "getPolistByStatus";
    public static final String getOrderListViewURL = ConstantValues.NEW_BASE_URL_AUTH + "getOrderListView";
    public static final String getNotificationsURL = ConstantValues.NEW_BASE_URL_AUTH + "getallnotifications";
    public static final String getOrderCashbackData=ConstantValues.NEW_BASE_URL_AUTH + "getOrderCashbackData";
    public static final String getCashbackDataHistory=ConstantValues.NEW_BASE_URL_AUTH + "getCashbackHistory";

    //Expenses Module
    public static final String getExpensesURL = ConstantValues.EXPENSE_MODULE_URL + "getexpenses";
    public static final String getApprovalDataAsPerRoleURL = ConstantValues.EXPENSE_MODULE_URL + "getapprovaldataasperrole";
    public static final String getApprovalActivityDetailsURL = ConstantValues.EXPENSE_MODULE_URL + "getapprovalactivitydetails";
    public static final String getExpenseDetailURL = ConstantValues.EXPENSE_MODULE_URL + "getexpensesbyid";
    public static final String getExpenseMasterValuesURL = ConstantValues.EXPENSE_MODULE_URL + "getmastervalforexpses";
    public static final String saveExpenseLineItemURL = ConstantValues.EXPENSE_MODULE_URL + "saveexpenseslineitems";
    public static final String getExpensesLineItemsURL = ConstantValues.EXPENSE_MODULE_URL + "getexpenseslineitems";
    public static final String mapDetailsWithExpensesURL = ConstantValues.EXPENSE_MODULE_URL + "mapdetailswithexpenses";
    public static final String addExpenseDetailsURL = ConstantValues.EXPENSE_MODULE_URL + "addexpensesdetails";
    public static final String getApprovalDataURL = ConstantValues.EXPENSE_MODULE_URL + "getapprovaldata";
    public static final String saveApprovalDataURL = ConstantValues.EXPENSE_MODULE_URL + "saveapprovaldata";
    public static final String getApprovalHistorybyidURL = ConstantValues.EXPENSE_MODULE_URL + "getapprovalhistorybyid";
    public static final String deleteUnClaimedExpensesURL = ConstantValues.EXPENSE_MODULE_URL + "deleteunclaimedexp";
    public static final String getTalleyLedgersURL = ConstantValues.EXPENSE_MODULE_URL + "gettallyledgers";
    public static final String getAllUsersExpensesURL = ConstantValues.EXPENSE_MODULE_URL + "getallusersexpenses";

    //UPI Payments
    public final static String TOKEN_URL = "https://upiuat.axisbank.co.in/WebPaymentS2S/Merchant/MerchantToken";
    public final static String COLLECT_URL = "https://upiuat.axisbank.co.in/WebPaymentS2S/Merchant/requestCollect/";


    public final static String EBUTOR_COLLECT_URL = "http://10.175.8.65:1337/upiebutor/CollectionRequest";

    public final static String getCrateDetailsURL = "http://10.175.8.65:1338/crate/getCrateDetails";


    public final static String getPOApprovalsList = ConstantValues.NEW_BASE_URL_AUTH + "getpendingapprovalpolist";
    public final static String uploadImagesURL = ConstantValues.NEW_BASE_URL_AUTH + "uploadtos3";


}

