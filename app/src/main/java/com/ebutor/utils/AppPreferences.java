package com.ebutor.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.ebutor.R;


/**
 * Created by 300041 on 3/8/2015.
 */
public class AppPreferences {

    public Context mContext;
    SharedPreferences mAppPreferences;
    public String is_first_run ;


    public AppPreferences(Context context) {
        mContext = context;
    }

    public String isIs_first_run() {
        return is_first_run;
    }

    public void setIs_first_run(String is_first_run) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(mContext.getString(R.string.app_name), Activity.MODE_PRIVATE).edit();
        editor.putString("ISFIRSTRUN", is_first_run);
        editor.commit();
    }

    public String getIs_first_run() {
        String data = mContext.getSharedPreferences(mContext.getString(R.string.app_name), Activity.MODE_PRIVATE).getString("ISFIRSTRUN",null);
        return data;
    }


}
