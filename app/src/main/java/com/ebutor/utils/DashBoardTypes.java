package com.ebutor.utils;

/**
 * Created by Srikanth Nama on 27-Sep-16.
 */

public class DashBoardTypes {
    public static final int ORGANIZATION = 0;
    public static final int MY_DASHBOARD = 1;
    public static final int MY_TEAM_DASHBOARD = 2;
}
