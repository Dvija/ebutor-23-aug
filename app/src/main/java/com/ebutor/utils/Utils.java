package com.ebutor.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.ebutor.MyApplication;
import com.ebutor.PincodeAvailabiltyActivity;
import com.ebutor.R;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CartModel;
import com.ebutor.models.FeatureData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by 300041 on 6/4/2015.
 */
public class Utils {

    public static final String DATE_STD_PATTERN = "dd-MM-yyyy";
    public static final String DATE_TIME_PATTERN = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_STD_PATTERN1 = "MMMM dd,yyyy HH:mm:ss a";
    public static final String DATE_STD_PATTERN2 = "MMMM dd,yyyy";
    public static final String DATE_STD_PATTERN3 = "dd MM yyyy";
    public static final String DATE_STD_PATTERN4 = "dd MMMM ,yyyy";
    public static final String DATE_STD_PATTERN5 = "yyyy-MM-dd";
    public static final String DATE_TIME_STD_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_PATTERN = "HH:mm";
    public static final String TIME_PATTERN1 = "HH:mm:ss";
    public static final String TIME_STD_PATTERN = "hh:mm a";
    public static final String TIME_STD_PATTERN2 = "HH:mm a";
    public static final String TIME_STAMP_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_PATTERN = "dd-MM";
    //    public static final int[] arrOptions = new int[]{R.string.orders, /*"Lists", "Shipment Tracking",*/ R.string.terms_conditions,R.string.rate_our_app/*, "Change Password"*/};
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static int offsetLimit = 20;

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public static String urlEncode(String sUrl) {
        int i = 0;
        String urlOK = "";
        while (i < sUrl.length()) {
            if (sUrl.charAt(i) == ' ') {
                urlOK = urlOK + "%20";
            } else {
                urlOK = urlOK + sUrl.charAt(i);
            }
            i++;
        }
        return (urlOK);
    }

    public static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 18192);
        StringBuilder sb = new StringBuilder();
        System.gc();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
                line = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    public static void showAlertWithMessage(Context context, String string) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(string);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    public static Dialog createLoader(Context context, String type) {
        Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        dialog.setTitle("");
        int resId = R.layout.toolbar_progress;
        switch (type) {
            case ConstantValues.SPLASH_PROGRESS:
                resId = R.layout.splash_progress;
                break;
            case ConstantValues.PROGRESS:
                resId = R.layout.progress;
                break;
            case ConstantValues.TOOLBAR_PROGRESS:
                resId = R.layout.toolbar_progress;
                break;
            case ConstantValues.TABS_PROGRESS:
                resId = R.layout.tabs_progress;
                break;
        }
        dialog.setContentView(resId);
        dialog.setCancelable(false);
        return dialog;
    }

    public static void showAlertDialog(Context context, String message) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage(message);
        dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    public static String parseDate(String fromFormat, String toFormat, String date) {

        String newDate = "";

        SimpleDateFormat input = new SimpleDateFormat(fromFormat);
        SimpleDateFormat output = new SimpleDateFormat(toFormat);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public static void startHomeActivity(Context context, Class<?> cls) {
//        if (projectsDao != null) projectsDao.close();
        if (context == null) {
            return;
        }

        Intent intent = new Intent(context, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static Calendar getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal;                                  // return the date part
    }

    /**
     * This method also assumes endDate >= startDate
     **/
    public static long daysBetween(Date startDate, Date endDate) {
        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    /* delete local data (tokens,ids,features,outlets table) while logout */
    public static void deleteLocalData(final Activity context) {

        SharedPreferences mSharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_ID, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_TOKEN, null).apply();
        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_LOGGED_IN, false).apply();
//                        mSharedPreferences.edit().putString(ConstantValues.KEY_SEGMENT_ID, null).apply();
        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, false).apply();
        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_FF, false).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_TOKEN, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_ID, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_PINCODE, null).apply();
        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_CHECKIN, false).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_LEGAL_ENTITY_ID, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_LEGAL_ENTITY_ID, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_BEAT_ID, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_HUB_ID, null).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_FF_HUB_ID, null).apply();

        DBHelper.getInstance().deleteTable(DBHelper.TABLE_OUTLETS);
        DBHelper.getInstance().deleteTable(DBHelper.TABLE_FEATURES);
//                        mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, null).apply();

//                        DBHelper.getInstance().deleteTable(DBHelper.TABLE_HOME);
//                        DBHelper.getInstance().deleteTable(DBHelper.TABLE_HOME_CHILD);
//                        DBHelper.getInstance().deleteTable(DBHelper.TABLE_CATEGORY);
//                        DBHelper.getInstance().deleteTable(DBHelper.TABLE_BRANDS);
//                        DBHelper.getInstance().deleteTable(DBHelper.TABLE_MANUFACTURERS);

        // clear cart count
        MyApplication.getInstance().setCartCount(0);

        //clear filter
        MyApplication.getInstance().setArrProductFilters(null);

        // clear filters in update beats
        MyApplication.getInstance().setSelBeats("");
        MyApplication.getInstance().setDcId("");
        MyApplication.getInstance().setSelHubIds(mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, ""));
        MyApplication.getInstance().setSelBeatIds("");
        MyApplication.getInstance().setSelBeatNames("");
        MyApplication.getInstance().setSelSpokeIds("");

    }

    public static void logout(final Activity context, String message) {

        new AlertDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        deleteLocalData(context);

                        Intent intent = new Intent(context, PincodeAvailabiltyActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                        context.finish();

                    }
                })

                .show();

    }

    public static String getPastDateTime(int syncInterval) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -syncInterval);
        Date pastDate = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String past = dateFormat.format(pastDate);

        return past;
    }

    public static boolean compareDates(String productSyncDate, String pastDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date strDate = null;
        Date pstDate = null;
        try {
            strDate = sdf.parse(productSyncDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            pstDate = sdf.parse(pastDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (pstDate != null && strDate != null) {
            if (pstDate.getTime() > strDate.getTime()) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    public static String getQuery(String tableName, String columnName1, String columnName2, String commaSeparatedIds, boolean areChilds, boolean isSort, String sortParam, String sortOrder) {
        /*select * from supplier.products where product_id in (1,3,2,4,6,7,9,8,10,5)
        order by case product_id when 1 then 1 when 3 then 2 when 2 then 3 when 4 then 4 when 6 then 5
        when 7 then 6 when 9 then 7 when 8 then 8 when 10 then 9 when 5 then 10 end;*/

        String table = "", specProductId = "", id = "", name = "";
        if (!TextUtils.isEmpty(sortParam)) {
            switch (sortParam) {
                case "Brand":
                    table = DBHelper.TABLE_BRANDS;
                    specProductId = DBHelper.COL_PRODUCT_BRAND_ID;
                    id = DBHelper.COL_BRAND_ID;
                    name = DBHelper.COL_BRAND_NAME;
                    break;
                case "Category":
                    table = DBHelper.TABLE_CATEGORY;
                    specProductId = DBHelper.COL_PRODUCT_CAT_ID;
                    id = DBHelper.COL_CATEGORY_ID;
                    name = DBHelper.COL_CATEGORY_NAME;
                    break;
                case "Manufacturer":
                    table = DBHelper.TABLE_MANUFACTURERS;
                    specProductId = DBHelper.COL_PRODUCT_MANF_ID;
                    id = DBHelper.COL_MFG_ID;
                    name = DBHelper.COL_MFG_NAME;
                    break;
                default:
                    table = "";
                    specProductId = "";
                    id = "";
                    name = "";
                    break;
            }
        }

        String query = "select * from " + tableName + " where " + columnName1 + " in (" + commaSeparatedIds + ") and " + columnName2 + "=" + columnName1 +
                " order by case " + columnName1;

        if (isSort) {
            query = "select * from " + tableName + " left join " + table + " on " + tableName + "." + specProductId + " = " + table + "." + id + " where " + columnName1 + " in (" + commaSeparatedIds + ") and " + columnName2 + "=" + columnName1 +
                    " group by " + specProductId + "," + "product_id order by " + table + "." + name + " " + sortOrder;
        }

        if (areChilds) {
//            query = Utils.getQuery(TABLE_PRODUCTS, COL_PRODUCT_ID, COL_PRODUCT_PARENT_ID, productIds, false);
            if (isSort) {
                query = "select * from " + tableName + " left join " + table + " on " + tableName + "." + specProductId + " = " + table + "." + id + " where " + columnName1 + " in (" + commaSeparatedIds + ")" +
                        " group by " + specProductId + "," + "product_id order by " + table + "." + name + " " + sortOrder;
            } else {
                query = "select * from " + tableName + " where " + columnName1 + " in (" + commaSeparatedIds + ")" +
                        " order by case " + columnName1;
            }
        }

        StringBuilder sb = new StringBuilder(query);

        if (!isSort) {
            if (commaSeparatedIds != null) {
                if (commaSeparatedIds.contains(",")) {
                    String[] ids = commaSeparatedIds.split(",");
                    for (int i = 0; i < ids.length; i++) {
                        sb.append(" when " + ids[i] + " then " + i);
                    }
                    sb.append(" end");
                    return sb.toString();
                } else {
                    // single id, return normal query
                    String _query = "select * from " + tableName + " where " + columnName1 + " in (" + commaSeparatedIds + ") and " + columnName2 + "=" + columnName1;
                    return _query;
                }

            } else {
                // id is null return error
                return null;
            }
        } else {
            return sb.toString();
        }
    }

    public static long insertOrUpdateCart(CartModel model) {
        if (model == null) {
            // product model is null, return error
            return -1;
        }

        if (model.getTotalPrice() == 0) {
            return -3;
        }
        // Inserting Product into cart table
        long rowId = DBHelper.getInstance().insertProductInCart(model);
        if (rowId < 0) {
            return rowId;// to identify that problem inserting in Main product
        }
        // Check if product has freebie item
        if (model.getFreebieProductId() != null) {

            if (model.getFreebieQty() != null && !model.getFreebieQty().equals("0")) {
                // insert freebie item in Cart table
                rowId = DBHelper.getInstance().insertFreeProductInCart(model);
            } else {
                //delete freebie product from cart
                DBHelper.getInstance().deleteCartRow(model.getCustomerId(), model.getFreebieProductId(), false, true);
            }

        }
        if (rowId < 0) {
            rowId = -2; // to identify that problem in inserting freebie product.
        }

        return rowId;
    }

    public static int getCartCount(String customerId) {
        if (customerId == null || TextUtils.isEmpty(customerId)) {
            return -1;
        }
        return DBHelper.getInstance().getCartCount(customerId);
    }

    public static double getCartValue(String customerId) {
        if (customerId == null || TextUtils.isEmpty(customerId)) {
            return -1;
        }
        return DBHelper.getInstance().getCartValue(customerId);
    }

    public static double getCashbackValue(String customerId) {
        if (customerId == null || TextUtils.isEmpty(customerId)) {
            return -1;
        }
        return DBHelper.getInstance().getCashbackValue(customerId);
    }

    public static double getCartValue(String customerId,String starvalue) {
        if ((customerId == null || TextUtils.isEmpty(customerId)) && starvalue==null || TextUtils.isEmpty(starvalue)) {
            return -1;
        }
        return DBHelper.getInstance().getCartStarValue(customerId,starvalue);
    }

    public static int deleteCartRow(String customerId, String productId, boolean deleteAll) {
        return DBHelper.getInstance().deleteCartRow(customerId, productId, deleteAll, false);
    }

    /*SRM Functions*/
    public static long insertOrUpdatePOCart(CartModel model) {
        if (model == null) {
            // product model is null, return error
            return -1;
        }
        // Inserting Product into cart table
        long rowId = DBHelper.getInstance().insertProductInPOCart(model);
        if (rowId < 0) {
            return rowId;// to identify that problem inserting in Main product
        }

        return rowId;
    }

    public static int getPOCartCount(String customerId, String warehouseId) {
        if (customerId == null || TextUtils.isEmpty(customerId)) {
            return -1;
        }
        if (warehouseId == null || TextUtils.isEmpty(warehouseId)) {
            return -1;
        }
        return DBHelper.getInstance().getPOCartCount(customerId, warehouseId);
    }

    public static double getPOCartValue(String customerId, String warehouseId) {
        if (customerId == null || TextUtils.isEmpty(customerId)) {
            return -1;
        }
        if (warehouseId == null || TextUtils.isEmpty(warehouseId)) {
            return -1;
        }
        return DBHelper.getInstance().getPOCartValue(customerId, warehouseId);
    }

    public static int deletePOCartRow(String customerId, String productId, boolean deleteAll, String isFreebie) {
        return DBHelper.getInstance().deletePOCartRow(customerId, productId, deleteAll, isFreebie);
    }

    @NonNull
    public static String getAWSFilePath() {
        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append(ConstantValues.AWS_FOLDER_PATH);
        stringBuilder.append("expense_");
        stringBuilder.append(System.currentTimeMillis());//todo change this logic..
        // Append userId and any other params to make it more unique??
        return stringBuilder.toString();
    }

    public static ArrayList<FeatureData> getRightMenuOptions() {
        return DBHelper.getInstance().getRightMenuOptions();
    }

    public static void callCrashlyticsViewEvent(String customerId, String name, String id) {
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("")
                .putContentType(name)
                .putContentId(id)
                .putCustomAttribute("CustomerId", customerId));
    }

    public static Bitmap convertDrawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap changeImageColor(Bitmap sourceBitmap, int color) {
        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0,
                sourceBitmap.getWidth() - 1, sourceBitmap.getHeight() - 1);
        Paint p = new Paint();
        ColorFilter filter = new LightingColorFilter(color, 1);
        p.setColorFilter(filter);

        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);
        return resultBitmap;
    }

    public static String capitalizeWords(String s) {
        final StringBuilder result = new StringBuilder(s.length());
        String[] words = s.split("\\s");
        for (int i = 0, l = words.length; i < l; ++i) {
            if (i > 0) result.append(" ");
            result.append(Character.toUpperCase(words[i].charAt(0)))
                    .append(words[i].substring(1));

        }
        return result.toString();
    }
}
