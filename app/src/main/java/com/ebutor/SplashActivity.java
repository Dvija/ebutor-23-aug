package com.ebutor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.activities.ExpenseModuleMainActivity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.AppDataModel;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CategoryModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.MasterLookUpModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by 300024 on 3/16/2016.
 */
public class SplashActivity extends ParentActivity implements ResultHandler, VolleyHandler, Response.ErrorListener {

    public static Activity instnace;
    SharedPreferences mSharedPreferences;
    DBHelper databaseObj;
    ArrayList<BusinessTypeModel> businessTyepArr;
    private GoogleCloudMessaging gcm;
    private RelativeLayout rlAlert;
    private TextView tvAlertMsg;
    private ImageView ivSplash;
    private String deviceId = "", ipAddress = "", requestType = "", firebaseToken = "";
    private String token = "", versionName = "";
    private int versionNumber;
    private boolean isLoad = false, isLoggedIn, isFirstUser, isInCompleteUser, isLoadSegments = true, isFF, isNotification = false;
    private Dialog dialog;
    private ArrayList<CustomerTyepModel> customerTypesArr, volumesArr, licenseArr;
    private ArrayList<CategoryModel> arrCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        try {
//            AndroidLogger logger = AndroidLogger.createInstance(getApplicationContext(),
//                    true, true, false, null, 0, "cea2ac48-70bf-4f73-9bde-a1dbb7749a6c", true);
//            logger.log("Hello");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String currentDate = df.format(c.getTime());
        Date pastDate = null, currDate = null;
        try {
            currDate = df.parse(df.format(c.getTime()));
            pastDate = df.parse(mSharedPreferences.getString(ConstantValues.KEY_LAST_UPDATED_DATE, currentDate));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((int) ((currDate.getTime() - pastDate.getTime()) / (1000 * 60 * 60 * 24)) > 0) {
            DBHelper dbHelper = DBHelper.getInstance();
            dbHelper.deleteTable(DBHelper.TABLE_PRODUCTS);
            dbHelper.deleteTable(DBHelper.TABLE_CART);
            dbHelper.deleteTable(DBHelper.TABLE_PO_CART);
        }
        mSharedPreferences.edit().putString(ConstantValues.KEY_LAST_UPDATED_DATE, currentDate).apply();

        gcm = GoogleCloudMessaging.getInstance(this);
        databaseObj = new DBHelper(SplashActivity.this);
        instnace = this;
        dialog = Utils.createLoader(SplashActivity.this, ConstantValues.SPLASH_PROGRESS);
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);

        FirebaseCrash.log("Splash Activity created");
        FirebaseCrash.logcat(Log.ERROR, "Tag", "NPE caught");
        FirebaseCrash.report(new JSONException("weew"));

        if (getIntent().hasExtra("isNotification")) {
            isNotification = getIntent().getBooleanExtra("isNotification", false);
        }

        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        ivSplash = (ImageView) findViewById(R.id.iv_splah);

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(SplashActivity.this)) {
                    switch (requestType) {
                        case "getCategories":
                            Log.e("Count 3 : ", databaseObj.getCount() + "");
                            databaseObj.deleteTable(DBHelper.TABLE_CATEGORY);
                            getCategories();
                            break;
                        case "GetSegments":
                            databaseObj.deleteTable(DBHelper.TABLE_BUSINESS_TYPE);
                            getMasterLookUp();
                            break;
                        default:
                            loadData();
                            break;
                    }
                } else {
                    Toast.makeText(SplashActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });


        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        deviceId = Settings.Secure.getString(SplashActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("device id", "" + deviceId);
        mSharedPreferences.edit().putString(ConstantValues.DEVICE_ID, deviceId).apply();

        int ip = wifiInfo.getIpAddress();

        ipAddress = Formatter.formatIpAddress(ip);
        mSharedPreferences.edit().putString(ConstantValues.IP_ADDRESS, ipAddress).apply();

        try {

            // register our app with FCM
            registerFCM();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // check app update
        getUpdate();

//        loadData();

    }

    private void registerFCM() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... arg0) {

                try {
                    firebaseToken = FirebaseInstanceId.getInstance().getToken(ConstantValues.FCM_SENDER_ID, "FCM");
                    Log.e("Firbase id login", "Refreshed token: " + firebaseToken);
                    mSharedPreferences.edit().putString(ConstantValues.FCM_ID, firebaseToken).apply();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
            }
        }.execute(null, null, null);
    }

    /* Checking the update of app by sending the current version code and version number  */
    private void getUpdate() {

        if (Networking.isNetworkAvailable(SplashActivity.this)) {
            requestType = "appUpdate";
            rlAlert.setVisibility(View.GONE);
            ivSplash.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                versionName = pInfo.versionName;
                versionNumber = pInfo.versionCode;
                JSONObject object = new JSONObject();

                object.put("number", versionNumber + "");
                object.put("type", "android");
                object.put("device_id", deviceId);
                object.put("ip_address", ipAddress);
                object.put("reg_id", firebaseToken);
                object.put("platform_id", ConstantValues.PLATFORM_ID);
                object.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));

                map.put("data", object.toString());

                Log.e("APP_UPDATE", object.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.appUpdateURL, map, SplashActivity.this, SplashActivity.this, PARSER_TYPE.APP_UPDATE);
            wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.appUpdateURL);
            if (!((Activity) SplashActivity.this).isFinishing() && dialog != null)
                dialog.show();
        } else {
            requestType = "appUpdate";
            rlAlert.setVisibility(View.VISIBLE);
            ivSplash.setVisibility(View.GONE);
        }
    }


    private void registerGcm() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... arg0) {

                InstanceID instanceID = InstanceID.getInstance(SplashActivity.this);
                try {
                    token = instanceID.getToken(ConstantValues.GOOGLE_PROJECT_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.e("GCM Token", token);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                mSharedPreferences.edit().putString(ConstantValues.KEY_GCM_TOKEN, token).apply();
            }
        }.execute(null, null, null);
    }

    private void loadData() {


        if (mSharedPreferences != null) {
            isLoggedIn = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false);
            isFirstUser = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FIRST_TIME_USER, true);
            isInCompleteUser = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_INCOMPLETE_REG, false);

//            if (isFirstUser) {
//                startActivity(new Intent(SplashActivity.this, PincodeAvailabiltyActivity.class));
//                finish();
//            } else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isLoggedIn) {
                        getCategories();
                    } else {
                        if (TextUtils.isEmpty(mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""))) {
                            startActivity(new Intent(SplashActivity.this, PincodeAvailabiltyActivity.class));
                            finish();
                        } else {
                            startActivity(new Intent(SplashActivity.this, SkipSignupActivity.class));
                            finish();
                        }
                    }
                }
            }, 3000);

//            }
        }
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null) {

            String response = (String) results;
            try {
                if (response != null) {
                    if (requestType == PARSER_TYPE.APP_UPDATE) {
                        if (response != null && !TextUtils.isEmpty(response)) {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.optString("status");
                            JSONObject messageObj = jsonObject.optJSONObject("message");
                            if (status.equalsIgnoreCase("200")) {
                                String versionUpdateStatus = messageObj.optString("versionUpdateStatus");
                                String versionNumber = messageObj.optString("versionNumber");
                                Log.e("version status", versionUpdateStatus);
                                Log.e("version number", versionNumber);
                                if (versionUpdateStatus.equalsIgnoreCase("1")) {
                                    showAlertWithMessage(getResources().getString(R.string.new_update_available));
                                } else {
                                    loadData();
                                }
                            }
                        } else {
                            loadData();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void getMasterLookUp() {

        if (Networking.isNetworkAvailable(SplashActivity.this)) {
            requestType = "GetSegments";
            rlAlert.setVisibility(View.GONE);
            ivSplash.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                map.put("data", obj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.masterLookUpURL, map, SplashActivity.this, SplashActivity.this, PARSER_TYPE.GET_MASTER_LOOKUP);
            wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.masterLookUpURL);
            if (!((Activity) SplashActivity.this).isFinishing() && dialog != null)
                dialog.show();
        } else {
            requestType = "GetSegments";
            rlAlert.setVisibility(View.VISIBLE);
            ivSplash.setVisibility(View.GONE);
        }
    }

    private void getCategories() {

        if (Networking.isNetworkAvailable(SplashActivity.this)) {
            requestType = "getCategories";
            rlAlert.setVisibility(View.GONE);
            ivSplash.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                obj.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
//                obj.put("segment_id", "48001");
                map.put("data", obj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.categoriesURL, map, SplashActivity.this, SplashActivity.this, PARSER_TYPE.GET_MAIN_CATEGORIES);
            wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.categoriesURL);
            if (!((Activity) SplashActivity.this).isFinishing() && dialog != null && instnace != null)
                dialog.show();
        } else {
            requestType = "getCategories";
            rlAlert.setVisibility(View.VISIBLE);
            ivSplash.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        instnace = null;
    }


    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {

        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            if (requestType == PARSER_TYPE.GET_MAIN_CATEGORIES) {
                this.requestType = "getCategories";
            } else if (requestType == PARSER_TYPE.GET_MASTER_LOOKUP) {
                this.requestType = "GetSegments";
            }
            rlAlert.setVisibility(View.VISIBLE);
            ivSplash.setVisibility(View.GONE);
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void showAlertWithMessage(String string) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(string);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                isLoad = true;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                startActivityForResult(i, 333);
                finish();
            }
        });
        dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
//                loadData();
                finish();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 333) {
            loadData();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.server_error));
            }

        } catch (Exception e) {

        }
        /*if (error != null) {
            Utils.showAlertWithMessage(SplashActivity.this, getResources().getString(R.string.unexpected_error));
            isLoadSegments = false;
            loadData();
        }*/
    }

    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (response != null) {

                    if (requestType == PARSER_TYPE.GET_MASTER_LOOKUP) {
                        if (status.equalsIgnoreCase("success")) {

                            if (response instanceof MasterLookUpModel) {

                            }

                            if (isNotification && DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EXPENSES_FEATURE_CODE) > 0) {

                                TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
                                Intent homeIntent = new Intent(this, HomeActivity.class);
                                taskStackBuilder.addNextIntent(homeIntent);
                                Intent expensesIntent = new Intent(this, ExpenseModuleMainActivity.class);
                                taskStackBuilder.addNextIntent(expensesIntent);
                                taskStackBuilder.startActivities();
                            } else {
                                if (isFF) {
                                    startActivity(new Intent(SplashActivity.this, FFDashboardActivity.class));
                                } else {
                                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                }
                            }
                            finish();
                        } else {
                            Utils.showAlertWithMessage(SplashActivity.this, message);
                        }
                    } else if (requestType == PARSER_TYPE.GET_MAIN_CATEGORIES) {

                        if (status.equalsIgnoreCase("success")) {

                            databaseObj.deleteTable(DBHelper.TABLE_CATEGORY);

                            if (response instanceof ArrayList) {
                                arrCategories = (ArrayList<CategoryModel>) response;
                                for (int i = 0; i < arrCategories.size(); i++) {
                                    databaseObj.insertCategory(arrCategories.get(i));
                                }
                            }
                            Log.e("Count 4 :", databaseObj.getCount() + "");
                            getMasterLookUp();

//                            if (isLoggedIn) {
//                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
//                                finish();
//                            } else if (!isFirstUser){
//                                startActivity(new Intent(SplashActivity.this, SkipSignupActivity.class));
//                                finish();
//                            }
//                            isLoadSegments = false;
//                            loadData();
                        } else {
                            Utils.showAlertWithMessage(SplashActivity.this, message);
                        }
                    } else if (requestType == PARSER_TYPE.APP_UPDATE) {

                        if (response != null && !TextUtils.isEmpty(response.toString())) {
                            if (response instanceof AppDataModel) {
                                AppDataModel appDataModel = (AppDataModel) response;
                                String versionUpdateStatus = appDataModel.getVersionUpdateStatus();
                                String versionNumber = appDataModel.getVersionNumber();
                                if (versionUpdateStatus.equalsIgnoreCase("1")) {
//                                    Utils.deleteLocalData(SplashActivity.this);
                                    showAlertWithMessage(getResources().getString(R.string.new_update_available));
                                } else {
                                    loadData();
                                }
                            }

                        } else {
                            loadData();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(SplashActivity.this, message);
    }

}