package com.ebutor;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.adapters.AreaAutoCompleteAdapter;
import com.ebutor.adapters.StatesSpinnerAdapter;
import com.ebutor.backgroundtask.HTTPBackgroundTask;
import com.ebutor.backgroundtask.MultipartEntity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.fragments.ManufacturerFragment;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.PincodeDataModel;
import com.ebutor.models.RetailersModel;
import com.ebutor.models.StateModel;
import com.ebutor.services.Locations;
import com.ebutor.utils.APIREQUEST_TYPE;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by 300024 on 3/4/2016.
 */
public class BuyerDetailsActivity extends ParentActivity implements PermissionCallback, ErrorCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<LocationSettingsResult>, LocationListener, View.OnClickListener, ResultHandler, Response.ErrorListener, VolleyHandler<Object>, ManufacturerFragment.OnClickListener {

    public static final int RESULT_CAPTURE_IMG = 3;
    public static final int RESULT_LOAD_IMG = 2;
    /**
     * Constant used in the location settings dialog.
     */
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQUEST_LOCATION = 0;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 12;

    protected Location mCurrentLocation;
    String picType, registeredShopName, retailerName, ShopPath, DOCPath, deviceId, ipAddress, customerToken, customerId, image;
    HashMap<String, String> mapFileType = new HashMap<>();
    ArrayList<BusinessTypeModel> arrBusinessTypes;
    ArrayList<CustomerTyepModel> arrCustomerType, arrVolumes, arrLicense, arrManf, arrBeats/*,arrPrefSlot1*/;
    ArrayList<StateModel> arrStates;
    String mLatitude, mLongitude;
    double latitude = 0.0, longitude = 0.0;
    double lat, lon;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {

            try {
                mLatitude = String.valueOf(location.getLatitude());
                mLongitude = String.valueOf(location.getLongitude());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
            mLatitude = "";
            mLongitude = "";
        }
    };
    private LatLng latLng;
    private LocationManager locationManager;
    private String strBusinessName, strNoOfShutters, strManufacturers, strBusinessStartTime,
            strBusinessEndTime/*, strPrefSlot1,strPrefSlot2*/, customerGrpId;
    private String strFirstName, strLastName, strMobileNumber, strEmail, gst_values;
    private String strAddress1, strAddress2, strLocality, strLandmark, strPinCode, strArea, strCity;
    private String strContact1Name, strContact1Number, strContact2Name, strContact2Number, gst, arn;
    private String selectedStateId, selectedState, selectedVolumeClass, selectedSegment, selectedLicense, selectedCustomerType, selBeat, segmentId;
    private Spinner spinCustomerType, spinSegment, spBeat;
    private EditText etNameOfBusiness, etNoOfShutters;
    private Spinner spinVolumeClass/*,spinnerPrefSlot1,spinnerPrefSlot2*/;
    private EditText etManufacturers, etBusinessStartTime, etBusinessEndTime;
    private RadioButton rbSmartPhone, rbNoSmartPhone, rbNetwork, rbNoNetwork;
    private EditText etFirstName, etLastName, etMobileNumber, etEmail, etGST, etARN;
    private TextView tvGeoTag;
    private ImageButton ibEditGeoLocation;
    private EditText etAddress1, etAddress2, etPinCode;
    private AutoCompleteTextView actArea;
    private EditText etCity;
    private Spinner spinState;
    private TextView tvShopCapture, tvShopUpload, tvCapture, tvUpload, tvBeat;
    private ImageView ivShop, ivDocument;
    private Spinner spinLicenseType;
    private ImageButton ibAddContact;
    private LinearLayout llContact1, llContact2;
    private ImageView ivDelete1, ivDelete2;
    private EditText etContact1Name, etContact1Number, etContact2Name, etContact2Number;
    private EditText etLocality, etLandmark;
    private boolean isFF = false;
    private CheckBox cbTermsAndConditions;
    private Button btnSubmit;
    private boolean showDialog = true;
    private SharedPreferences mSharedPreferences;
    private ScrollView slMain;
    private Dialog dialog;
    private int hour, minute, prevSelLicense = 0, currSelLicense = 0;
    private boolean isServiceCheck;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private Date startTime, endTime, startTimePrev, endTimePrev;
    private ArrayList<String> areasList;
    private ArrayList<String> check_gst;
    private AlertDialog alertDialog;
    private AlertDialog.Builder build;
    private String city, pin, add1, add2, subloc;
    public BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            latitude = intent.getDoubleExtra(Locations.EXTRA_LATITUDE, 0);
            longitude = intent.getDoubleExtra(Locations.EXTRA_LONGITUDE, 0);
            city = intent.getStringExtra(Locations.STREET);
            pin = intent.getStringExtra(Locations.POSTAL_CODE);
            add1 = intent.getStringExtra(Locations.ADDRESS_LINE1);
            add2 = intent.getStringExtra(Locations.ADDRESS_LINE2);
            subloc = intent.getStringExtra(Locations.SUB_Locality);

            latLng = new LatLng(latitude, longitude);
            getGeocodes(latitude, longitude, city, pin, add1, add2, subloc);
            if (latitude != 0.0 && longitude != 0.0) {
                lat = latitude;
                lon = longitude;
            }
            if (latLng != null) {
                stopService(new Intent(getApplicationContext(), Locations.class));
            }
        }
    };

    public static ArrayList<String> filter(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerGrpId());
            }
        }
        return result;
    }

    public static ArrayList<StateModel> filterState(Collection<StateModel> target, Predicate<StateModel> predicate) {
        ArrayList<StateModel> result = new ArrayList<StateModel>();
        for (StateModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static boolean isDeviceLocationEnabled(Context mContext) {
        int locMode = 0;
        String locProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locMode = Settings.Secure.getInt(mContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locProviders = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locProviders);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_details);

        arrBusinessTypes = new ArrayList<>();
        arrCustomerType = new ArrayList<>();
        arrVolumes = new ArrayList<>();
        arrLicense = new ArrayList<>();
//        arrPrefSlot1 = new ArrayList<>();
        arrStates = new ArrayList<>();
        arrBeats = new ArrayList<>();

//        documentType = "TIN";
        dialog = Utils.createLoader(BuyerDetailsActivity.this, ConstantValues.PROGRESS);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);
        if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_INCOMPLETE_REG, true).apply();
        }
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);

        gst_values = mSharedPreferences.getString(ConstantValues.GST_CODES, "");
        check_gst = new ArrayList<>(Arrays.asList(gst_values.split(",")));

        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        deviceId = Settings.Secure.getString(BuyerDetailsActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        int ip = wifiInfo.getIpAddress();

        ipAddress = Formatter.formatIpAddress(ip);

        slMain = (ScrollView) findViewById(R.id.sl_main);

        initialiseViews();

        reqPermission();

        tvCapture = (TextView) findViewById(R.id.tv_capture);
        tvUpload = (TextView) findViewById(R.id.tv_upload);
//        llPicsLayout = (LinearLayout) findViewById(R.id.pics_layout);
//        shopPictureLayout = (LinearLayout) findViewById(R.id.shop_pics_layout);
        btnSubmit = (Button) findViewById(R.id.bt_submit);

        build = new AlertDialog.Builder(BuyerDetailsActivity.this);
        build.setTitle(getString(R.string.app_name));
        build.setMessage(getString(R.string.please_switch_on_loc));
        build.setPositiveButton(R.string.action_settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });

        build.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    // Toast.makeToast(getContext(), "There is no way back!").;
                    return true; // Consumed
                } else {
                    return false; // Not consumed
                }
            }
        });
        alertDialog = build.create();
        alertDialog.setCanceledOnTouchOutside(false);

        if (!isDeviceLocationEnabled(this)) {
            // Utils.showAlertDialog(BuyerDetailsActivity.this,"please enable the loctions");
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }


        arrManf = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_MASTER_MANF);

        if (isFF) {
            tvBeat.setVisibility(View.VISIBLE);
            spBeat.setVisibility(View.VISIBLE);
        } else {
            tvBeat.setVisibility(View.GONE);
            spBeat.setVisibility(View.GONE);
        }

        getStates();
        if (isFF) {
            getBeats();
        }
        /*requestLocationPermission();
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        checkLocationSettings();*/

        etManufacturers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrManf != null && arrManf.size() > 0)
                    showManfDialog();
                else
                    Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.no_manf), Toast.LENGTH_SHORT).show();
            }
        });


//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, 7);
//        calendar.set(Calendar.MINUTE, 0);

        etBusinessStartTime.setText(Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_STD_PATTERN, "07" + ":" + "00"));
        strBusinessStartTime = "07:00 AM";
        try {
            startTime = sdf.parse("07:00:00");
            startTimePrev = startTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        etBusinessStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hour = 7;
                minute = 0;
                TimePickerDialog timePickerDialog = new TimePickerDialog(BuyerDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String selStartTime = hourOfDay + ":" + minute + ":00";
                        try {
                            startTimePrev = startTime;
                            startTime = sdf.parse(selStartTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (endTime != null && (startTime.compareTo(endTime) < 0)) {
                            String startTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_STD_PATTERN, hourOfDay + ":" + minute);
                            strBusinessStartTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_PATTERN1, hourOfDay + ":" + minute);
                            if (null != startTime)
                                etBusinessStartTime.setText(startTime);
                        } else {
                            startTime = startTimePrev;
                            Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_endtime_after_starttime), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        etBusinessEndTime.setText(Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_STD_PATTERN, "21" + ":" + "00"));
        strBusinessEndTime = "21:00 PM";
        try {
            endTime = sdf.parse("21:00:00");
            endTimePrev = endTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        etBusinessEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hour = 9;
                minute = 0;
                if (null != strBusinessStartTime && !TextUtils.isEmpty(strBusinessStartTime)) {

                    TimePickerDialog timePickerDialog = new TimePickerDialog(BuyerDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String selEndTime = hourOfDay + ":" + minute + ":00";
                            try {
                                endTimePrev = endTime;
                                endTime = sdf.parse(selEndTime);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (startTime.compareTo(endTime) < 0) {
                                String endTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_STD_PATTERN, hourOfDay + ":" + minute);
                                strBusinessEndTime = Utils.parseDate(Utils.TIME_PATTERN, Utils.TIME_PATTERN1, hourOfDay + ":" + minute);
                                etBusinessEndTime.setText(endTime);
                            } else {
                                endTime = endTimePrev;
                                Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_endtime_after_starttime), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, hour, minute, false);
                    timePickerDialog.show();
                } else {
                    Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_start_time), Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvGeoTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(BuyerDetailsActivity.this);
                dialog.setTitle(getResources().getString(R.string.app_name));
                dialog.setMessage(getResources().getString(R.string.are_you_at_shop));
                dialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

//                        checkLocationSettings();

                        dialog.dismiss();

                    }
                });
                dialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface negativeDialog, int i) {
                        negativeDialog.dismiss();
                    }
                });
                AlertDialog alert = dialog.create();
                alert.show();

            }
        });

        ivDelete1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llContact1.setVisibility(View.GONE);
                etContact1Name.setText("");
                etContact1Number.setText("");
            }
        });
        ivDelete2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llContact2.setVisibility(View.GONE);
                etContact2Name.setText("");
                etContact2Number.setText("");
            }
        });

        ibAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (llContact1.getVisibility() != View.VISIBLE) {
                    llContact1.setVisibility(View.VISIBLE);
                } else if (llContact2.getVisibility() != View.VISIBLE) {
                    llContact2.setVisibility(View.VISIBLE);
                } else {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.you_can_add_only_2_contacts));
                }
            }
        });

        tvShopCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picType = "shop";
//                shopScrollView.setVisibility(View.VISIBLE);
                takePicture();
            }
        });
        tvShopUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picType = "shop";
//                shopScrollView.setVisibility(View.VISIBLE);
                uploadPicture();
            }
        });
        tvCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picType = "doc";
//                scrollView.setVisibility(View.VISIBLE);
                takePicture();
            }
        });
        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                scrollView.setVisibility(View.VISIBLE);
                picType = "doc";
                uploadPicture();
            }
        });

        etPinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() == 6) {
                    getPincodeAreas(s.toString());
                    actArea.setEnabled(true);
                } else {
                    actArea.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*etGST.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() == 15) {
                    int pos = 0;
                    for (int i = 0; i < arrLicense.size(); i++) {
                        if (arrLicense.get(i).getCustomerGrpId().equalsIgnoreCase("97031")) {
                            pos = i;
                            break;
                        }
                    }

                    spinLicenseType.setSelection(pos);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/

        strMobileNumber = mSharedPreferences.getString(ConstantValues.KEY_MOBILE, "");
        if (null != strMobileNumber) {
            etMobileNumber.setText(strMobileNumber);
            etMobileNumber.setEnabled(false);
        }

        spinCustomerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCustomerType = adapterView.getItemAtPosition(i).toString();
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                customerGrpId = arrCustomerType.get(i).getCustomerGrpId();
                /*if (customerGrpId != null && customerGrpId.length() > 0) {
                    editor.putInt("position", i);
                    editor.putString(ConstantValues.KEY_CUSTOMER_GRP_ID, customerGrpId);
                    editor.apply();
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spBeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selBeat = arrBeats.get(position).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinSegment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedSegment = adapterView.getItemAtPosition(i).toString();
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                segmentId = arrBusinessTypes.get(i).getSegmentId();
                if (segmentId != null && segmentId.length() > 0) {
                    editor.putInt("position", i);
                    editor.putString(ConstantValues.KEY_SEGMENT_ID, segmentId);
                    editor.apply();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinVolumeClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedVolumeClass = arrVolumes.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinLicenseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                prevSelLicense = currSelLicense;
                currSelLicense = i;
                if (etGST.getText().length() > 0 && !(arrLicense.get(i).getCustomerGrpId().equalsIgnoreCase("97031") || arrLicense.get(i).getCustomerGrpId().equalsIgnoreCase(""))) {
                    currSelLicense = prevSelLicense;

                }
                selectedLicense = arrLicense.get(currSelLicense).getCustomerGrpId();
                spinLicenseType.setSelection(currSelLicense);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*spinnerPrefSlot1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    strPrefSlot1 = "";
                } else {
                    strPrefSlot1 = arrPrefSlot1.get(i).getCustomerName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerPrefSlot2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    strPrefSlot1 = "";
                } else {
                    strPrefSlot2 = arrPrefSlot1.get(i).getCustomerName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view != null) {
                    Object obj = view.getTag();
                    if (obj != null && obj instanceof StateModel) {
                        StateModel stateModel = (StateModel) obj;
                        selectedStateId = stateModel.getStateId();
                        selectedState = stateModel.getStateName();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        spinnerDocumentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                documentType = (String) adapterView.getItemAtPosition(i);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        arrCustomerType = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_CUSTOMER_TYPE);
        if (arrCustomerType != null && arrCustomerType.size() > 0) {
            if (!isFF) {
                for (int i = 0; i < arrCustomerType.size(); i++) {
                    CustomerTyepModel customerTyepModel = arrCustomerType.get(i);
                    if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase("3013")) {
                        arrCustomerType.remove(customerTyepModel);
                    }
                }
            }
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrCustomerType);
            spinCustomerType.setAdapter(arrayAdapter);
            int pos = 0;
            for (int i = 0; i < arrCustomerType.size(); i++) {
                CustomerTyepModel customerTyepModel = arrCustomerType.get(i);
                if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""))) {
                    pos = i;
                    break;
                }
            }
            spinCustomerType.setSelection(pos);
        }

        arrBusinessTypes = DBHelper.getInstance().getAllSegments();
        if (arrBusinessTypes != null && arrBusinessTypes.size() > 0) {
            ArrayAdapter<BusinessTypeModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrBusinessTypes);
            spinSegment.setAdapter(arrayAdapter);
            int pos = 0;
            for (int i = 0; i < arrBusinessTypes.size(); i++) {
                BusinessTypeModel businessTypeModel = arrBusinessTypes.get(i);
                if (businessTypeModel.getSegmentId().equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""))) {
                    pos = i;
                    break;
                }
            }
            spinSegment.setSelection(pos);
        }

        arrVolumes = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_VOLUME_CLASS);
        if (arrVolumes != null && arrVolumes.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrVolumes);
            spinVolumeClass.setAdapter(arrayAdapter);
            try {
                spinVolumeClass.setSelection(0);
            } catch (Exception e) {

            }
        }

        arrLicense = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_LICENSE);
        if (arrLicense != null && arrLicense.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrLicense);
            spinLicenseType.setAdapter(arrayAdapter);
            try {
                spinLicenseType.setSelection(0);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*arrPrefSlot1 = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_PREF_SLOTS);
        if (arrPrefSlot1 != null && arrPrefSlot1.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrPrefSlot1);
            spinnerPrefSlot1.setAdapter(arrayAdapter);
            spinnerPrefSlot2.setAdapter(arrayAdapter);
            try {
                spinnerPrefSlot1.setSelection(0);
                spinnerPrefSlot2.setSelection(0);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isDeviceLocationEnabled(BuyerDetailsActivity.this)) {
                    if (!alertDialog.isShowing()) {
                        alertDialog.show();
                    }
                    return;
                }
                if (latitude == 0.0 && longitude == 0.0) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getString(R.string.please_wait_loc_fetched));

                    LocalBroadcastManager.getInstance(BuyerDetailsActivity.this).registerReceiver(message
                            , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST));

                    Intent in = new Intent(BuyerDetailsActivity.this, Locations.class);
                    startService(in);
                    return;

                }

                if (null == customerGrpId || customerGrpId.length() <= 0) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_customer_type));
                    return;
                }

                if (null == segmentId || segmentId.length() <= 0) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_business_type));
                    return;
                }

                strBusinessName = etNameOfBusiness.getText().toString().trim();
                if (TextUtils.isEmpty(strBusinessName)) {
                    etNameOfBusiness.setError(getResources().getString(R.string.please_enter_shop_name));
                    etNameOfBusiness.requestFocus();
                    return;
                }

                if (strBusinessName.length() < 4) {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getString(R.string.please_enter_shop_name_4_32));
                    return;
                }

                strNoOfShutters = etNoOfShutters.getText().toString().trim();
                if (TextUtils.isEmpty(strNoOfShutters)) {
                    etNoOfShutters.setError(getResources().getString(R.string.please_enter_no_of_shutters));
                    etNoOfShutters.requestFocus();
                    return;
                }

                if (null == selectedVolumeClass || selectedVolumeClass.length() <= 0) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_volume_class));
                    return;
                }

                if (TextUtils.isEmpty(strManufacturers)) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_manufacturers));
                    return;
                }

                if (strBusinessStartTime == null || TextUtils.isEmpty(strBusinessStartTime)) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_start_time));
                    return;
                }

                if (strBusinessEndTime == null || TextUtils.isEmpty(strBusinessEndTime)) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_end_time));
                    return;
                }

               /* if(TextUtils.isEmpty(strPrefSlot1) || strPrefSlot1.equalsIgnoreCase("Select Preferred Slot")) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_pref_slot_1));
                    return;
                }*/

                strFirstName = etFirstName.getText().toString().trim();
                if (TextUtils.isEmpty(strFirstName)) {
                    etFirstName.setError(getResources().getString(R.string.please_enter_first_name));
                    etFirstName.requestFocus();
                    return;
                }

                strLastName = etLastName.getText().toString().trim();
                /*if (TextUtils.isEmpty(strLastName)) {
                    etLastName.setError(getResources().getString(R.string.please_enter_last_name));
                    etLastName.requestFocus();
                    return;
                }*/

                strEmail = etEmail.getText().toString().trim();
                if (!TextUtils.isEmpty(strEmail) && !Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
                    etEmail.setError(getResources().getString(R.string.please_enter_valid_email));
                    etEmail.requestFocus();
                    return;
                }


                strAddress1 = etAddress1.getText().toString().trim();
                if (TextUtils.isEmpty(strAddress1)) {
                    etAddress1.setError(getResources().getString(R.string.please_enter_address1));
                    etAddress1.requestFocus();
                    return;
                }

                strAddress2 = etAddress2.getText().toString().trim();
                if (TextUtils.isEmpty(strAddress2)) {
                    etAddress2.setError(getResources().getString(R.string.please_enter_address2));
                    etAddress2.requestFocus();
                    return;
                }
                strLocality = etLocality.getText().toString().trim();
                if (TextUtils.isEmpty(strLocality)) {
                    etLocality.setError(getResources().getString(R.string.please_enter_locality));
                    etLocality.requestFocus();
                    return;
                }
                strLandmark = etLandmark.getText().toString().trim();
                strPinCode = etPinCode.getText().toString().trim();
                if (TextUtils.isEmpty(strPinCode) || strPinCode.length() < 6) {
                    etPinCode.setError(getResources().getString(R.string.please_enter_valid_area_pin));
                    etPinCode.requestFocus();
                    return;
                }

                strArea = actArea.getText().toString().trim();
                if (TextUtils.isEmpty(strArea)) {
                    actArea.setError(getResources().getString(R.string.please_enter_area));
                    actArea.requestFocus();
                    return;
                }

                if (isFF && (null == selBeat || selBeat.length() <= 0)) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_beat));
                    return;
                }

                strCity = etCity.getText().toString().trim();
                if (TextUtils.isEmpty(strCity)) {
                    etCity.setError(getResources().getString(R.string.please_enter_city));
                    etCity.requestFocus();
                    return;
                }

                if (null == selectedStateId || selectedStateId.length() <= 0) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_state));
                    return;
                }

                strContact1Name = etContact1Name.getText().toString().trim();
                strContact1Number = etContact1Number.getText().toString().trim();
                if (!TextUtils.isEmpty(strContact1Name) && (TextUtils.isEmpty(strContact1Number) || strContact1Number.length() < 10)) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, "Please enter valid Representative 1 details");
                    return;
                }

                strContact2Name = etContact2Name.getText().toString().trim();
                strContact2Number = etContact2Number.getText().toString().trim();
                if (!TextUtils.isEmpty(strContact2Name) && (TextUtils.isEmpty(strContact2Number) || strContact2Number.length() < 10)) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, "Please enter valid Representative 2 details");
                    return;
                }

                if (null != DOCPath && TextUtils.isEmpty(selectedLicense)) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_license_type));
                    return;
                }

                if (!TextUtils.isEmpty(etGST.getText().toString()) && etGST.getText().length() != 15) {
                    etGST.setError(getString(R.string.please_enter_proper_gst_number));
                    return;
                }

                String gst_check = "";
                if (!TextUtils.isEmpty(etGST.getText().toString())) {
                    gst_check = etGST.getText().toString().substring(0, 2);

                    if (!check_gst.contains(gst_check)) {
                        etGST.setError(getString(R.string.please_enter_proper_gst_number));
                        return;
                    }
                }

                if (!TextUtils.isEmpty(etARN.getText().toString()) && etARN.getText().length() != 15) {
                    etARN.setError(getString(R.string.please_enter_proper_arn_number));
                    return;
                }

//                if (null == selectedLicense || selectedLicense.length() <= 0) {
//                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_select_license_type));
//                    return;
//                }

                if (!cbTermsAndConditions.isChecked()) {
                    Utils.showAlertDialog(BuyerDetailsActivity.this, getResources().getString(R.string.please_accept_terms_conditions));
                    return;
                }

                 /*
                private String strContact1Name, strContact1Number, strContact2Name, strContact2Number;*/

                /*Predicate<CustomerTyepModel> isCheckedManfs = new Predicate<CustomerTyepModel>() {
                    @Override
                    public boolean apply(CustomerTyepModel customerTyepModel) {
                        return customerTyepModel.isChecked();
                    }
                };

                ArrayList<String> checkedFilters = filter(arrManf, isCheckedManfs);
                manfIds = TextUtils.join(",", checkedFilters);
                if (manfIds != null && manfIds.startsWith(",")) {
                    manfIds = manfIds.replaceFirst(",", "");
                }*/


                submitBuyerDetails();

            }
        });
    }

    private void initialiseViews() {
        spinCustomerType = (Spinner) findViewById(R.id.spinner_customer_type);
        spinSegment = (Spinner) findViewById(R.id.spinner_business_type);
        spBeat = (Spinner) findViewById(R.id.spinner_beat);
        etNameOfBusiness = (EditText) findViewById(R.id.et_business_name);
        etNoOfShutters = (EditText) findViewById(R.id.et_no_of_shutters);
        spinVolumeClass = (Spinner) findViewById(R.id.spinner_volume_class);
        /*spinnerPrefSlot1 = (Spinner) findViewById(R.id.spinner_pref_slot_1);
        spinnerPrefSlot2 = (Spinner) findViewById(R.id.spinner_pref_slot_2);*/
        etManufacturers = (EditText) findViewById(R.id.et_manufacturers);
        etBusinessStartTime = (EditText) findViewById(R.id.et_start_time);
        etBusinessEndTime = (EditText) findViewById(R.id.et_end_time);

        rbSmartPhone = (RadioButton) findViewById(R.id.radio_smart_yes);
        rbNoSmartPhone = (RadioButton) findViewById(R.id.radio_smart_no);
        rbNetwork = (RadioButton) findViewById(R.id.radio_internet_yes);
        rbNoNetwork = (RadioButton) findViewById(R.id.radio_internet_no);

        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etMobileNumber = (EditText) findViewById(R.id.et_mobile);
        etEmail = (EditText) findViewById(R.id.et_email);
        etGST = (EditText) findViewById(R.id.et_gst);
        etARN = (EditText) findViewById(R.id.et_arn);

        tvGeoTag = (TextView) findViewById(R.id.tv_locate_me);
        ibEditGeoLocation = (ImageButton) findViewById(R.id.ib_edit_location);

        etAddress1 = (EditText) findViewById(R.id.et_address1);
        etAddress2 = (EditText) findViewById(R.id.et_address2);
        etLocality = (EditText) findViewById(R.id.et_locality);
        etLandmark = (EditText) findViewById(R.id.et_landmark);
        etPinCode = (EditText) findViewById(R.id.et_pincode);
        actArea = (AutoCompleteTextView) findViewById(R.id.et_area);
        etCity = (EditText) findViewById(R.id.et_city);
        spinState = (Spinner) findViewById(R.id.spinner_state);

        tvShopCapture = (TextView) findViewById(R.id.tv_shop_capture);
        tvShopUpload = (TextView) findViewById(R.id.tv_shop_upload);
        tvBeat = (TextView) findViewById(R.id.tv_beat);
        ivShop = (ImageView) findViewById(R.id.ivShop);
        ivDocument = (ImageView) findViewById(R.id.ivDocument);
        spinLicenseType = (Spinner) findViewById(R.id.spinner_license_type);
        ibAddContact = (ImageButton) findViewById(R.id.tv_add_contact);
        llContact1 = (LinearLayout) findViewById(R.id.ll_contact_1);
        llContact2 = (LinearLayout) findViewById(R.id.ll_contact_2);
        ivDelete1 = (ImageView) findViewById(R.id.iv_delete_1);
        ivDelete2 = (ImageView) findViewById(R.id.iv_delete_2);

        etContact1Name = (EditText) findViewById(R.id.et_contact_name_1);
        etContact2Name = (EditText) findViewById(R.id.et_contact_name_2);
        etContact1Number = (EditText) findViewById(R.id.et_contact_number_1);
        etContact2Number = (EditText) findViewById(R.id.et_contact_number_2);

        cbTermsAndConditions = (CheckBox) findViewById(R.id.terms_checkbox);

    }

    private void showManfDialog() {
        FragmentManager fm = getSupportFragmentManager();
        ManufacturerFragment manufacturerFragment = ManufacturerFragment.newInstance(arrManf, this.strManufacturers);
        manufacturerFragment.setClickListener(BuyerDetailsActivity.this);
        manufacturerFragment.setCancelable(true);
        manufacturerFragment.show(fm, "manf_fragment");
    }

    private void getPincodeAreas(String pincode) {
        if (Networking.isNetworkAvailable(BuyerDetailsActivity.this)) {
//            rlAlert.setVisibility(View.GONE);
            slMain.setVisibility(View.VISIBLE);
//            requestType = "GetPinCodeAreas";
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pincode", pincode);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPinCodeDataURL,
                        map, BuyerDetailsActivity.this, BuyerDetailsActivity.this, PARSER_TYPE.GET_PINCODE_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getPinCodeDataURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void reqPermission() {
        new AskPermission.Builder(this).setPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)
                .setCallback(this)
                .setErrorCallback(this)
                .request(99);
    }

    private void getBeats() {
        if (Networking.isNetworkAvailable(BuyerDetailsActivity.this)) {
            slMain.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getbeatsURL, map, BuyerDetailsActivity.this, BuyerDetailsActivity.this, PARSER_TYPE.GET_BEATS);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getbeatsURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            slMain.setVisibility(View.GONE);
        }
    }

    private void getStates() {
        if (Networking.isNetworkAvailable(BuyerDetailsActivity.this)) {
//            rlAlert.setVisibility(View.GONE);
            slMain.setVisibility(View.VISIBLE);
//            requestType = "GetStates";
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("flag", "2");
                jsonObject.put("country", "99");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getStateCountriesURL, map, BuyerDetailsActivity.this, BuyerDetailsActivity.this, PARSER_TYPE.GET_STATES);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getStateCountriesURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
//            rlAlert.setVisibility(View.VISIBLE);
            slMain.setVisibility(View.GONE);
//            requestType = "GetStates";
        }
    }

    public void getAllSegments() {
        if (Networking.isNetworkAvailable(BuyerDetailsActivity.this)) {
//            rlAlert.setVisibility(View.GONE);
            slMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                String appId = mSharedPreferences.getString(ConstantValues.KEY_APP_ID, "");
                JSONObject dataObject = new JSONObject();
                if (appId.length() > 0) {
                    dataObject.put("appId", appId);
                }

                map.put("data", dataObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            HTTPBackgroundTask task = new HTTPBackgroundTask(BuyerDetailsActivity.this, BuyerDetailsActivity.this, PARSER_TYPE.GET_ALL_SEGMENTS, APIREQUEST_TYPE.HTTP_POST, false, ConstantValues.PROGRESS);
            task.execute(map);
        } else {
//            rlAlert.setVisibility(View.VISIBLE);
            slMain.setVisibility(View.GONE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            Log.i("", "Received response for Location permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Location permission has been granted
                Log.i("", "Locations permission has now been granted.");
                Snackbar.make(findViewById(R.id.tv_capture), R.string.permision_available_locations,
                        Snackbar.LENGTH_SHORT).show();
                getCurrentLocation();

            } else {
                Log.i("", "Location permission was NOT granted.");
//                Snackbar.make(findViewById(R.id.button), R.string.permissions_not_granted,
//                        Snackbar.LENGTH_SHORT).show();

            }
            // END_INCLUDE(permission_result)

        } else if (requestCode == 9) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // camera task you need to do.

                int permissionCheck = ContextCompat.checkSelfPermission(BuyerDetailsActivity.this,
                        Manifest.permission.CAMERA);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(BuyerDetailsActivity.this, getString(R.string.unable_open_camera), Toast.LENGTH_SHORT).show();
                    return;
                }

                int permissionCheck1 = ContextCompat.checkSelfPermission(BuyerDetailsActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

                if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(BuyerDetailsActivity.this, getString(R.string.unable_open_camera), Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(Environment.getExternalStorageDirectory(), picType + ".jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, 1);

            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
//        mGoogleApiClient.connect();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user asynchronously -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("permission request")
                        .setMessage("please....enable the location")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(BuyerDetailsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isDeviceLocationEnabled(this)) {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }

        if (checkLocationPermission()) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                LocalBroadcastManager.getInstance(this).registerReceiver(message
                        , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST));

                Intent in = new Intent(this, Locations.class);
                startService(in);


            }
        }
    }

    /*protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }
*/
    @Override
    protected void onPause() {
        super.onPause();
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(locationListener);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(message);
        stopService(new Intent(this, Locations.class));
    }

    /**
     * Requests the Locations permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestLocationPermission() {
        Log.i("", "CAMERA permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Log.i("",
                    "Displaying camera permission rationale to provide additional context.");
            Snackbar.make(findViewById(R.id.tv_capture), R.string.permission_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(BuyerDetailsActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_LOCATION);
                        }
                    })
                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
        // END_INCLUDE(camera_permission_request)
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopService(new Intent(this, Locations.class));
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        } else {
            String provider_info = "";
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            //getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            //getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSEnabled) {
                provider_info = LocationManager.GPS_PROVIDER;
            } else if (isNetworkEnabled) {
                provider_info = LocationManager.NETWORK_PROVIDER;
            }

            if (provider_info != null && !provider_info.isEmpty()) {
                locationManager.requestLocationUpdates(provider_info, 0, 0, BuyerDetailsActivity.this);
                mCurrentLocation = locationManager.getLastKnownLocation(provider_info);

            }
//            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mCurrentLocation != null) {

                double latitude = mCurrentLocation.getLatitude();
                double longitude = mCurrentLocation.getLongitude();
                mLatitude = String.valueOf(latitude);
                mLongitude = String.valueOf(longitude);

                try {
                    Address address = getAddress(latitude, longitude);
                    if (address != null) {
                        int maxAddressLine = address.getMaxAddressLineIndex();
                        String strAddress = "";
                        String addressLine2 = "";
                        strAddress = address.getAddressLine(0);
                        addressLine2 = address.getAddressLine(1);

                        if (address.getMaxAddressLineIndex() > 0) {
                            String lastLine = address.getAddressLine(maxAddressLine - 1);
                            if (lastLine.length() >= 6) {
                                lastLine = lastLine.substring(lastLine.length() - 6, lastLine.length());
                            }
                            if (lastLine.matches(".*\\d+.*")) {
                                etPinCode.setText(lastLine);
                            }
                        }

                        String area = address.getSubAdminArea();
                        String adminArea = address.getAdminArea();
                        String locality = address.getLocality();
                        String postalCode = address.getPostalCode();
                        String state = address.getAdminArea();
                        checkIfStateAvaiable(state);
                        if (postalCode != null)
                            etPinCode.setText(postalCode);

                        if (locality != null)
                            etCity.setText(locality);

                        etAddress1.setText(strAddress);
                        etAddress2.setText(addressLine2);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, mLatitude).apply();
//                mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, mLongitude).apply();
                //todo Service call to pass location coordinates
//                Toast.makeText(getApplicationContext(), mCurrentLocation.getLatitude() + "  " + mCurrentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getResources().getString(R.string.unable_find_location), Toast.LENGTH_LONG).show();
            }
//            MyApplication.getInstance().setSelectedArea(null);
//            finish();
        }

    }

    private void checkIfStateAvaiable(final String state) {
        int pos = 0;

        Predicate<StateModel> isSelectedState = new Predicate<StateModel>() {
            @Override
            public boolean apply(StateModel stateModel) {
                return stateModel.getStateName().equalsIgnoreCase("Telangana");
            }
        };

        ArrayList<StateModel> stateModels = filterState(arrStates, isSelectedState);
        if (stateModels != null && stateModels.size() > 0) {
            pos = arrStates.indexOf(stateModels.get(0));
        }
        spinState.setSelection(pos);

    }

    /**
     * Get the postal address from given LatLong
     *
     * @param latitude  user current location, Latitude and Longitude
     * @param longitude
     * @return address resolved address from the given LatLong
     */
    public Address getAddress(double latitude, double longitude) throws IOException {
//        if (location == null) {
//            return new Address(null);
//        }
        Geocoder gc = new Geocoder(BuyerDetailsActivity.this);
        Address address = null;
        List<Address> addresses = gc.getFromLocation(latitude, longitude, 1);
        if (addresses.size() > 0) {
            address = addresses.get(0);
        }
        return address;
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                getCurrentLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(BuyerDetailsActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.i("", "User agreed to make required location settings changes.");
//                    try {
//                        new Thread().sleep(2000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getCurrentLocation();
                        }
                    }, 3000);
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i("", "User chose not to make required location settings changes.");
                    break;
            }
        } else if (requestCode == RESULT_CAPTURE_IMG && resultCode == Activity.RESULT_OK) {

            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals(picType + ".jpg")) {
                    f = temp;
                    break;
                }
            }
            try {

                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                String _path = f.getAbsolutePath();
                Bitmap bitmap = BitmapFactory.decodeFile(_path, bitmapOptions);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                Bitmap newBitmap = null;
                int rotate = 0;
                try {
                    ExifInterface exif = new ExifInterface(_path);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotate);
                    newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                f.delete();
                f = new File(_path);
                f.createNewFile();

                Log.e("new path", "" + _path);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                newBitmap.compress(Bitmap.CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
                addBitmap(newBitmap, _path);

            } catch (Exception e) {
                e.printStackTrace();
            }

           /* if (data != null) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                addBitmap(bitmap, data.getExtras().get("data").toString());
            }*/
        } else if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                addBitmap(thumbnail, picturePath);
            }
        }

    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
   /* protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }*/

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    /*protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }*/

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    /*protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(20000);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(10000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }*/
    private void addBitmap(Bitmap bitmap, String _path) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;

        if (picType.equalsIgnoreCase("doc")) {
//            docBitmap = bitmap;
            DOCPath = _path;

            ivDocument.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
            ivDocument.setTag(_path);
            ivDocument.setVisibility(View.VISIBLE);
//                llPicsLayout.addView(imageViewItem, 0);
        } else {
//            tinBitmap = bitmap;
            ShopPath = _path;

            ivShop.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
            ivShop.setTag(_path);
            ivShop.setVisibility(View.VISIBLE);
//                shopPictureLayout.addView(imageViewItem, 0);
        }
//        } else {
//            Toast.makeText(BuyerDetailsActivity.this, "You can capture only 4 images", Toast.LENGTH_SHORT).show();
//        }

//        ivCross.setOnClickListener(this);

    }

    public void uploadPicture() {
        int permissionCheck = ContextCompat.checkSelfPermission(BuyerDetailsActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BuyerDetailsActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMG);
    }

    public void takePicture() {
        int permissionCheck = ContextCompat.checkSelfPermission(BuyerDetailsActivity.this,
                Manifest.permission.CAMERA);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BuyerDetailsActivity.this,
                    new String[]{Manifest.permission.CAMERA}, 9);
            return;
        }

        int permissionCheck1 = ContextCompat.checkSelfPermission(BuyerDetailsActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BuyerDetailsActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
            return;
        }

        int permissionCheck2 = ContextCompat.checkSelfPermission(BuyerDetailsActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck2 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BuyerDetailsActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 9);
            return;
        }


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), picType + ".jpg");
//        MimeTypeMap mime = MimeTypeMap.getSingleton();
//        String ext = f.getName().substring(f.getName().lastIndexOf(".") + 1);
//        String type = mime.getMimeTypeFromExtension(ext);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            Uri contentUri = FileProvider.getUriForFile(BuyerDetailsActivity.this, "com.ebutor.fileProvider", f);
//            intent.setDataAndType(contentUri, type);
//        } else {
//            intent.setDataAndType(Uri.fromFile(f), type);
//        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, RESULT_CAPTURE_IMG);
    }

    public void submitBuyerDetails() {

        if (Networking.isNetworkAvailable(BuyerDetailsActivity.this)) {
            slMain.setVisibility(View.VISIBLE);
            new UploadTask().execute(ShopPath);
        } else {
            slMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        String filePath = (String) view.getTag();

//        FrameLayout fl = (FrameLayout) llPicsLayout.findViewWithTag(filePath);
//        llPicsLayout.removeView(fl);

    }

    private void showAlertWithMessage(String string, final boolean isFF) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(BuyerDetailsActivity.this);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(string);
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                etNameOfBusiness.setText("");
                etNoOfShutters.setText("");
                etFirstName.setText("");
                etLastName.setText("");
                etEmail.setText("");
                etCity.setText("");
                etAddress1.setText("");
                etAddress2.setText("");
                etPinCode.setText("");

                spinCustomerType.setSelection(0);
                spinSegment.setSelection(0);
                spBeat.setSelection(0);
                spinVolumeClass.setSelection(0);
                spinLicenseType.setSelection(0);
                /*spinnerPrefSlot1.setSelection(0);
                spinnerPrefSlot2.setSelection(0);*/

                if (isFF) {
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    editor.putString(ConstantValues.KEY_CUSTOMER_ID, customerId);
                    editor.putString(ConstantValues.KEY_RETAILER_NAME, retailerName);
                    editor.putString(ConstantValues.KEY_CUSTOMER_TOKEN, customerToken);
                    editor.putString(ConstantValues.KEY_FIRST_NAME, retailerName);
                    editor.putString(ConstantValues.KEY_PROFILE_IMAGE, image);
                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_CHECKIN, true).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_SHOP_NAME, registeredShopName).apply();
                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, true).apply();
                    editor.apply();

                    Intent intent = new Intent(BuyerDetailsActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(BuyerDetailsActivity.this, RegisterSuccessActivity.class);
                    intent.putExtra("ShopName", registeredShopName);
                    intent.putExtra("RetailerName", retailerName);
                    intent.putExtra("MobileNo", strMobileNumber);
                    intent.putExtra("serviceCheck", isServiceCheck);
                    startActivity(intent);
                    finish();
                }


            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private void showActivationAlert(String string, final String customerToken, final String customerId) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(BuyerDetailsActivity.this);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(string);
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                sendMailToFF(customerToken, customerId);
                finish();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private void sendMailToFF(String customerToken, String customerId) {
        if (Networking.isNetworkAvailable(BuyerDetailsActivity.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", customerToken);
                jsonObject.put("customer_id", customerId);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.sendMailToFFURL, map, BuyerDetailsActivity.this, BuyerDetailsActivity.this, PARSER_TYPE.SEND_MAIL_TO_FF);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.sendMailToFFURL);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void showInActiveAlert(String string) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(BuyerDetailsActivity.this);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(string);
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void getGeocodes(double lat, double lng, String city, String pin, String add1, String add2, String subloc) {

        try {
            if (pin != null) {
                etPinCode.setText(pin);
            }
            if (city != null) {
                etCity.setText(city);
            }
            if (add1 != null) {
                etAddress1.setText(add1);
            }
            if (add2 != null) {
                etAddress2.setText(add2);
            }
            etLocality.setText(subloc);

        } catch (Exception e) {

        }

    }

    public String uploadFile() {

        String stringResponse = "";
        String charset = "UTF-8";

        String requestURL = ConstantValues.NEW_BASE_URL_AUTH + "registration";

        try {
//            String appId = mSharedPreferences.getString(ConstantValues.KEY_APP_ID, "");
            JSONObject addressDetailsObj = new JSONObject();
            addressDetailsObj.put("telephone", strMobileNumber);
            addressDetailsObj.put("business_type_id", mSharedPreferences.getString(ConstantValues.SEGMENT_ID, "29"));
            addressDetailsObj.put("customer_type", customerGrpId);

            addressDetailsObj.put("flag", "4");

            addressDetailsObj.put("customer_token", "");
            addressDetailsObj.put("master_manf", strManufacturers);

            addressDetailsObj.put("business_legal_name", strBusinessName);
            addressDetailsObj.put("address1", strAddress1);
            addressDetailsObj.put("address2", strAddress2);
            addressDetailsObj.put("locality", strLocality);
            addressDetailsObj.put("landmark", strLandmark);
            addressDetailsObj.put("area", strArea);
            addressDetailsObj.put("city", strCity);
            addressDetailsObj.put("device_id", deviceId);

            if (ipAddress != null && ipAddress.length() > 0) {
                addressDetailsObj.put("ip_address", ipAddress);
            }
            addressDetailsObj.put("pincode", strPinCode);
            addressDetailsObj.put("state_id", selectedStateId);

            if (latitude == 0.0 && longitude == 0.0) {


                Toast.makeText(this, getString(R.string.please_wait_loc_fetched), Toast.LENGTH_SHORT).show();
                LocalBroadcastManager.getInstance(this).registerReceiver(message
                        , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
                );

                Intent in = new Intent(this, Locations.class);
                startService(in);

            }

            if (lat != 0.0) {
                addressDetailsObj.put("latitude", lat);
            } else {
                addressDetailsObj.put("latitude", "");
            }

            if (lon != 0.0) {
                addressDetailsObj.put("longitude", lon);
            } else {
                addressDetailsObj.put("longitude", "");
            }

            addressDetailsObj.put("firstname", strFirstName);
            addressDetailsObj.put("lastname", strLastName);
            addressDetailsObj.put("email_id", strEmail == null ? "" : strEmail);
            addressDetailsObj.put("bstart_time", strBusinessStartTime);
            addressDetailsObj.put("bend_time", strBusinessEndTime);
            /*addressDetailsObj.put("pref_value", strPrefSlot1);
            addressDetailsObj.put("pref_value1", strPrefSlot2);*/

            addressDetailsObj.put("volume_class", selectedVolumeClass);
            if (isFF)
                addressDetailsObj.put("beat_id", selBeat);

            if (selectedLicense != null && selectedLicense.length() > 0) {
                addressDetailsObj.put("license_type", selectedLicense);
            }
            addressDetailsObj.put("noof_shutters", strNoOfShutters);
            addressDetailsObj.put("gstin", etGST.getText().toString());
            addressDetailsObj.put("arn_number", etARN.getText().toString());

            if (strContact1Name != null && strContact1Name.length() > 0) {
                addressDetailsObj.put("contact_name1", strContact1Name);
            }
            if (strContact1Number != null && strContact1Number.length() > 0) {
                addressDetailsObj.put("contact_no1", strContact1Number);
            }
            if (strContact2Name != null && strContact2Name.length() > 0) {
                addressDetailsObj.put("contact_name2", strContact2Name);
            }
            if (strContact2Number != null && strContact2Number.length() > 0) {
                addressDetailsObj.put("contact_no2", strContact2Number);
            }

            addressDetailsObj.put("smartphone", rbSmartPhone.isChecked() ? "1" : "0");
            addressDetailsObj.put("network", rbNetwork.isChecked() ? "1" : "0");

            addressDetailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, null));
            if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                addressDetailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
            }
            addressDetailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, null));

            MultipartEntity multipart = new MultipartEntity(requestURL, charset);

            multipart.addFormField("data", addressDetailsObj.toString());
            Log.e("Registration", addressDetailsObj.toString());

            if (ShopPath != null)
                multipart.addFilePart("profile_picture", new File(ShopPath));
            if (DOCPath != null)
                multipart.addFilePart("doc_url", new File(DOCPath));

            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");
            StringBuilder sb = new StringBuilder();
            for (String line : response) {
                System.out.println(line);
                sb.append(line);
            }

            return sb.toString();
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null && results instanceof String) {
            String response = (String) results;
            if (!TextUtils.isEmpty(response)) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("success")) {
                        if (requestType == PARSER_TYPE.GET_ALL_SEGMENTS) {
                            BusinessTypeModel businessTypeModel = new BusinessTypeModel();
                            businessTypeModel.setSegmentId("0");
                            businessTypeModel.setSegmentName("Please Select");
                            arrBusinessTypes.add(businessTypeModel);

                            JSONArray dataArray = jsonObject.optJSONArray("data");
                            if (dataArray != null && dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject segmentObj = dataArray.optJSONObject(i);
                                    String segmentId = segmentObj.optString("segment_id");
                                    String segmentName = segmentObj.optString("name");

                                    BusinessTypeModel model = new BusinessTypeModel();
                                    model.setSegmentId(segmentId);
                                    model.setSegmentName(segmentName);
                                    arrBusinessTypes.add(model);
                                }
                            }

                            if (arrBusinessTypes != null && arrBusinessTypes.size() > 0) {
                                ArrayAdapter<BusinessTypeModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrBusinessTypes);
                                spinSegment.setAdapter(arrayAdapter);
                                /*try {
                                    spinenrBusinessType.setSelection(mSharedPreferences.getInt("position", 0));
                                } catch (Exception e) {

                                }*/
                            }
                        }
                    }

                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {

    }

    public int uploadFile(String sourceFileUri) {


        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 4 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {


            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.source_file_not_exist) + " :", Toast.LENGTH_LONG).show();
                }
            });

            return 0;

        } else {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL("http://factail.com/api/index.php?route=customer/customers/SaveAddress");

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
               /* dos.writeBytes("Content-Disposition: form-data; name="+"uploaded_file";filename=""
                                + fileName + """ + lineEnd");*/
                String fieldName = "DOC";
                dos.writeBytes("Content-Disposition: form-data; name=\"" + fieldName
                        + "\"; filename=\"" + fileName + "\"");
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                int serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {

                            String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
                                    + " http://www.androidexample.com/media/uploads/";

//                            messageText.setText(msg);
                            Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.file_upload_complete),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {


                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
//                        messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.something_wrong),
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {


                runOnUiThread(new Runnable() {
                    public void run() {
//                        messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(BuyerDetailsActivity.this, getResources().getString(R.string.something_wrong),
                                Toast.LENGTH_SHORT).show();
                    }
                });

            }

            return 1;

        } // End else block
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if (error != null)
//            Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                String state = "Telangana";
                if (requestType == PARSER_TYPE.GET_STATES) {
                    if (response instanceof ArrayList) {
                        if (arrStates != null)
                            arrStates.clear();
                        arrStates = (ArrayList<StateModel>) response;

                        StatesSpinnerAdapter adapter = new StatesSpinnerAdapter(BuyerDetailsActivity.this, arrStates);
                        spinState.setAdapter(adapter);
                        if (!TextUtils.isEmpty(state)) {
                            checkIfStateAvaiable(state);
                        }

                    }
                } else if (requestType == PARSER_TYPE.GET_PINCODE_DATA) {
                    if (response instanceof PincodeDataModel) {
                        PincodeDataModel pincodeDataModel = (PincodeDataModel) response;
                        if (areasList != null)
                            areasList.clear();
                        actArea.setText("");
                        areasList = pincodeDataModel.getArrAreas();

                        AreaAutoCompleteAdapter adapter = new AreaAutoCompleteAdapter(BuyerDetailsActivity.this, R.layout.row_area_name, areasList);
                        actArea.setAdapter(adapter);

                        int pos = 0;
                        for (int i = 0; i < arrStates.size(); i++) {
                            StateModel stateModel = arrStates.get(i);
                            if (stateModel.getStateId().equalsIgnoreCase(pincodeDataModel.getStateId())) {
                                pos = i;
                                break;
                            }
                        }
                        spinState.setSelection(pos);
                        spinState.setEnabled(false);

                    }
                } else if (requestType == PARSER_TYPE.GET_BEATS) {
                    if (response instanceof ArrayList) {
                        arrBeats = (ArrayList<CustomerTyepModel>) response;
                        if (arrBeats != null) {
                            CustomerTyepModel customerTyepModel1 = new CustomerTyepModel();
                            customerTyepModel1.setCustomerGrpId("");
                            customerTyepModel1.setCustomerName("Select Beat");

                            arrBeats.add(0, customerTyepModel1);
                            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrBeats);
                            spBeat.setAdapter(arrayAdapter);
                        }
                    }
                }
            } else {
                if (requestType == PARSER_TYPE.GET_PINCODE_DATA) {
                    actArea.setText("");
                    areasList.clear();
                    actArea.setAdapter(null);
                } else {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, message);
                }
            }
        } else {
            Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(BuyerDetailsActivity.this, message);
    }

    @Override
    public void onClicked(String string) {

        Predicate<CustomerTyepModel> isCheckedManfs = new Predicate<CustomerTyepModel>() {
            @Override
            public boolean apply(CustomerTyepModel customerTyepModel) {
                return customerTyepModel.isChecked();
            }
        };

        ArrayList<String> checkedFilters = filter(arrManf, isCheckedManfs);
        strManufacturers = TextUtils.join(",", checkedFilters);
        if (strManufacturers != null && strManufacturers.startsWith(",")) {
            strManufacturers = strManufacturers.replaceFirst(",", "");
        }

        if (string != null) {
            if (string.contains("All")) {
                etManufacturers.setText("All");
            } else {
                etManufacturers.setText(string);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            mCurrentLocation = location;
            if (mCurrentLocation != null) {
                double latitude = mCurrentLocation.getLatitude();
                double longitude = mCurrentLocation.getLongitude();
                mLatitude = String.valueOf(latitude);
                mLongitude = String.valueOf(longitude);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onShowRationalDialog(final PermissionInterface permissionInterface, int requestCode) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("We need permissions for this app.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onDialogShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }

    @Override
    public void onShowSettings(final PermissionInterface permissionInterface, int requestCode) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("We need permissions for this app. Open setting screen?");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onSettingsShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        LocalBroadcastManager.getInstance(this).registerReceiver(message
                , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST));

        Intent in = new Intent(this, Locations.class);
        startService(in);
    }

    @Override
    public void onPermissionsDenied(int requestCode) {
        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(callGPSSettingIntent);
    }

    private class UploadTask extends AsyncTask<String, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialog != null)
                dialog.show();
            else {
                setDialog(0, "");
                dialog.show();
            }

//            dialog = ProgressDialog.show(BuyerDetailsActivity.this, "", "Please wait...");
        }

        private void setDialog(int i, String s) {
            dialog = new Dialog(BuyerDetailsActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
            dialog.setTitle("");
            dialog.setContentView(R.layout.progress);
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {
            return uploadFile();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            try {
                try {

                    Log.e("Registration", result);

                    JSONObject obj = new JSONObject(result);

                    String status = obj.getString("status");
                    String message = obj.getString("message");

                    if (status.equalsIgnoreCase("success")) {

                        JSONObject dataObject = obj.getJSONObject("data");
                        message = dataObject.getString("message");
                        isServiceCheck = dataObject.optString("serviceCheck").equalsIgnoreCase("1");

                        registeredShopName = dataObject.optString("business_legal_name");
                        retailerName = dataObject.optString("firstname") + " " + dataObject.optString("lastname");
                        customerId = dataObject.optString("customer_id");
                        customerToken = dataObject.optString("customer_token");
                        String customergrpId = dataObject.optString("customer_group_id");
                        String pincode = dataObject.optString("pincode");
                        String whId = dataObject.optString("le_wh_id");
                        String hub = dataObject.optString("hub");
                        image = /*ConstantValues.NEW_BASE_IMAGE_AUTH + */dataObject.optString("image");
                        String segmentId = dataObject.optString("segment_id");
                        String legalEntityId = dataObject.optString("legal_entity_id");
                        int isActive = dataObject.optInt("is_active");
                        String beatId = dataObject.optString("beat_id");
                        String latitude = dataObject.optString("latitude").equalsIgnoreCase("null") ? "0.00" : dataObject.optString("latitude");
                        String longitude = dataObject.optString("longitude").equalsIgnoreCase("null") ? "0.00" : dataObject.optString("longitude");

                        if (!isFF && (TextUtils.isEmpty(beatId) || beatId.equalsIgnoreCase("0"))) {
                            showActivationAlert(getString(R.string.your_account_under_activation), customerToken, legalEntityId);
                            return;
                        }

                        RetailersModel retailersModel = new RetailersModel();
                        retailersModel.setCustomerId(customerId);
                        retailersModel.setLatitude(latitude);
                        retailersModel.setLongitude(longitude);
                        retailersModel.setFirstName(retailerName);
                        retailersModel.setTelephone(strMobileNumber);
                        retailersModel.setCompany(registeredShopName);
                        retailersModel.setAddress1(strAddress1);
                        retailersModel.setAddress2(strAddress2);
                        retailersModel.setNoOfShutters(strNoOfShutters);
                        retailersModel.setVolumeClass(selectedVolumeClass);
                        retailersModel.setSegmentId(segmentId);
                        retailersModel.setMasterManfIds(strManufacturers);
                        retailersModel.setBuyerTypeId(customergrpId);
                        retailersModel.setCustomerToken(customerToken);
                        retailersModel.setLastOrder("0");
                        retailersModel.setLastVisit("");

                        DBHelper.getInstance().insertOutlet(retailersModel);

                        mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, beatId).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_BEAT_ID, beatId).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, latitude).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, longitude).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_LEGAL_ENTITY_ID, legalEntityId).apply();
                        if (hub != null && !TextUtils.isEmpty(hub) && !hub.equals("0"))
                            mSharedPreferences.edit().putString(ConstantValues.KEY_HUB_ID, hub).apply();
                        if (whId != null && !TextUtils.isEmpty(whId) && !whId.equals("0"))
                            mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, whId).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_SEGMENT_ID, segmentId).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_GRP_ID, customergrpId).apply();

                        if (isActive == 1) {

                            if (isFF) {
                                showAlertWithMessage(message, true);
                                return;
                            }

                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_ID, customerId).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_TOKEN, customerToken).apply();
                            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_LOGGED_IN, true).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_FIRST_NAME, retailerName).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, image).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_PINCODE, pincode).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_LEGAL_ENTITY_ID, legalEntityId).apply();

//                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_HOME);
//                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_HOME_CHILD);
//                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_CATEGORY);
//                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_BRANDS);
//                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_MANUFACTURERS);

                            showAlertWithMessage(message, isFF);
                        } else {
                            showInActiveAlert(message);
                        }


                    } else {
                        Utils.showAlertWithMessage(BuyerDetailsActivity.this, message);
                    }
                    //finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    AlertDialog.Builder alert = new AlertDialog.Builder(BuyerDetailsActivity.this);
                    alert.setTitle(getPackageName());
                    alert.setMessage(getResources().getString(R.string.oops_try_again));
                    alert.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            etFirstName.setText("");
                            etLastName.setText("");
                            etEmail.setText("");
                            spinSegment.setSelection(0);
                            spBeat.setSelection(0);
                            spinState.setSelection(0);
                            spinVolumeClass.setSelection(0);
                            spinLicenseType.setSelection(0);
                            /*spinnerPrefSlot1.setSelection(0);
                            spinnerPrefSlot2.setSelection(0);*/
                            etNameOfBusiness.setText("");
                            etCity.setText("");
                            etAddress1.setText("");
                            etAddress2.setText("");
                            etPinCode.setText("");
                            etNoOfShutters.setText("");
                            ivDocument.setVisibility(View.GONE);
//                            scrollView.setVisibility(View.GONE);
//                            llPicsLayout.removeAllViews();
                        }

                    });

                } catch (Exception e) {
                    Utils.showAlertWithMessage(BuyerDetailsActivity.this, getResources().getString(R.string.server_error));
                }
            } finally {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        }
    }

}
