package com.ebutor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by 300024 on 3/4/2016.
 */
public class FirstSignupActivity extends ParentActivity implements ResultHandler, VolleyHandler<Object>, Response.ErrorListener {
    public static Activity instnace;
    EditText etMobileNo/*,etFirstName, etMailId*/;
    String mobileNo/*,name, mailId*/;
    DBHelper databaseObj;
    private Button btnContinue;
    private SharedPreferences mSharedPreferences;
    private LinearLayout llMain;
    private RelativeLayout rlAlert;
    private TextView tvAlertMsg;
    private Tracker mTracker;
    private Dialog dialog;

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_signup);
        instnace = this;

        databaseObj = new DBHelper(FirstSignupActivity.this);
        dialog = Utils.createLoader(FirstSignupActivity.this, ConstantValues.PROGRESS);

//        etFirstName = (EditText) findViewById(R.id.et_full_name);
        etMobileNo = (EditText) findViewById(R.id.et_mobile_no);
//        etMailId = (EditText) findViewById(R.id.et_mail_id);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        btnContinue = (Button) findViewById(R.id.bt_continue);
        llMain = (LinearLayout) findViewById(R.id.ll_main);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);

        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(FirstSignupActivity.this)) {
                    addRegistration();
                } else {
                    Toast.makeText(FirstSignupActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                mobileNo = etMobileNo.getText().toString().trim();
                if (TextUtils.isEmpty(mobileNo)) {
                    etMobileNo.setError(getResources().getString(R.string.please_enter_mobile_number));
                    etMobileNo.requestFocus();
                    return;
                } else if (mobileNo.length() != 10) {
                    etMobileNo.setError(getResources().getString(R.string.please_enter_valid_mobile_number));
                    etMobileNo.requestFocus();
                    return;
                } else if (mobileNo.substring(0, 1).equalsIgnoreCase("0")) {
                    etMobileNo.requestFocus();
                    etMobileNo.setError(getResources().getString(R.string.please_enter_valid_mobile_number));
                    return;
                }
                addRegistration();
            }
        });

        /*TelephonyManager tMgr = (TelephonyManager)FirstSignupActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        if(mPhoneNumber!=null) {
            etMobileNo.setText(mPhoneNumber);
        }*/
    }


    public void addRegistration() {
        if (Networking.isNetworkAvailable(FirstSignupActivity.this)) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();

            try {

                JSONObject dataObject = new JSONObject();
                if (mobileNo != null && mobileNo.length() > 0) {
                    dataObject.put("telephone", mobileNo);
                }

                boolean isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
                if (isFF) {
                    dataObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }

                dataObject.put("business_type_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                dataObject.put("flag", "1");
                map.put("data", dataObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.registrationURL, map, FirstSignupActivity.this, FirstSignupActivity.this, PARSER_TYPE.ADD_REGISTRATION);
            wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.registrationURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null && requestType == PARSER_TYPE.ADD_REGISTRATION) {
            String response = (String) results;
            if (!TextUtils.isEmpty(response)) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    String status = jsonObject.getString("status");

                    if (status.equalsIgnoreCase("success")) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        message = dataObject.getString("message");
                        status = dataObject.getString("status");
                        if (status.equalsIgnoreCase("1")) {

                        } else {
                            Utils.showAlertDialog(FirstSignupActivity.this, message);
                        }

                    } else {
                        etMobileNo.setText("");
                        Utils.showAlertWithMessage(FirstSignupActivity.this, message);
                    }
                } catch (Exception e) {
                    Utils.showAlertWithMessage(FirstSignupActivity.this, e.getLocalizedMessage());
                }
            }
        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {

        if (requestType == PARSER_TYPE.ADD_REGISTRATION) {
            if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
                rlAlert.setVisibility(View.VISIBLE);
                llMain.setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName(" Signup First Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if(error!=null)
//            Utils.showAlertWithMessage(FirstSignupActivity.this,getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("1")) {
//                etMobileNo.setText("");
                Intent intent = new Intent(FirstSignupActivity.this, OtpActivity.class);
                intent.putExtra("MobileNo", mobileNo);
                startActivity(intent);
            } else {
                etMobileNo.setText("");
                Utils.showAlertWithMessage(FirstSignupActivity.this, message);
            }
        } else {
            Utils.showAlertWithMessage(FirstSignupActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(FirstSignupActivity.this, message);
    }

}

