package com.ebutor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.expandable.library.ExpandCollapseAnimation;
import com.ebutor.database.DBHelper;
import com.ebutor.fragments.AdditionalCashBackFragment;
import com.ebutor.models.AddressModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.DiscountModel;
import com.ebutor.models.MasterLookUpModel;
import com.ebutor.models.StarLevelCashBackDetailsModel;
import com.ebutor.models.UserAddressModel;
import com.ebutor.services.Locations;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FourDigitCardFormatWatcher;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.model.LatLng;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;
import com.payu.india.Interfaces.PaymentRelatedDetailsListener;
import com.payu.india.Model.MerchantWebService;
import com.payu.india.Model.PaymentDetails;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.PostParams.MerchantWebServicePostParams;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.india.Tasks.GetPaymentRelatedDetailsTask;
import com.payu.payuui.PaymentsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Srikanth Nama on 3/24/2016.
 */
public class PaymentActivity extends BaseActivity implements PermissionCallback, ErrorCallback, /*ResultHandler,*/ PaymentRelatedDetailsListener, Response.ErrorListener, VolleyHandler<Object>/*, OtpFragment.OnUpdate*/ {

    public static final Pattern VPA_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}");
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 12;
    private ArrayList<StarLevelCashBackDetailsModel> starLevelCashBackArray;
    /**
     * We remember, for each collapsable view its height.
     * So we dont need to recalculate.
     * The height is calculated just before the view is drawn.
     */
    private final SparseIntArray viewHeights = new SparseIntArray(10);
    UserAddressModel userDefaultAddress = null;
    PaymentParams mPaymentParams;
    PayuHashes mPayUHashes;
    PayuConfig payuConfig;
    //    GPSTracker gpsTracker;
    private Tracker mTracker;
    private boolean flag/*, isOrderDiscount = false*/;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy");
    private int selYear = 0, selMonth = 0, selDay = 0;
    private boolean isFF, isOtpPopUp;
    private String otp = "";
    private ArrayList<CustomerTyepModel> arrBeats;
    /**
     * Reference to the last expanded list item.
     * Since lists are recycled this might be null if
     * though there is an expanded list item
     */
    private View lastOpen = null;
    /**
     * The position of the last expanded list item.
     * If -1 there is no list item expanded.
     * Otherwise it points to the position of the last expanded list item
     */
    private int lastOpenPosition = -1;
    /**
     * Default Animation duration
     * Set animation duration with @see setAnimationDuration
     */
    private int animationDuration = 330;
    /**
     * A list of positions of all list items that are expanded.
     * Normally only one is expanded. But a mode to expand
     * multiple will be added soon.
     * <p/>
     * If an item onj position x is open, its bit is set
     */
    private BitSet openItems = new BitSet();
    private View viewUPI, viewCC, viewNet;
    private ArrayList<UserAddressModel> userAddressModelArrayList;
    private TextView tvAddress, tvChangeAddress, tvAddNewAddress;
    //    private Button btnPay;
    private EditText etCardNumber, etCardHolderName, etExpiryMonth, etExpiryYear, etCVV, etScheduleDeliveryDate;
    private EditText etVPA;

    private AlertDialog alertDialog;
    private AlertDialog.Builder build;
    //    private String salt;
    private String key;
    private double cartAmount = 0.00, discountAmount, finalAmount, grandTotal, eCashApplied = 0, eCashBal = 0, totalCashback = 0;
    private int totalItems = 0;
    private String transactionId = "", mPayuId = "", scheduleDate = "", scheduleDeliveryDate;
    private TextView tvCartAmount, tvTotalAmount, tvDiscountAmount, tvECashBal, tvTotalCashback;
    private JSONArray /*productsArray,*/ cartId;
    //    private JSONObject profileObj;
    private CheckBox cbCod, cbECash;
    private Button btnProceed;
    //    private ListView mListView;
    private Spinner spBanks, spinnerPrefSlot1, spinnerPrefSlot2;
    private PayuResponse mPayuResponse;
    private ArrayList<PaymentDetails> netBankingList;
    private LinearLayout llMain, llDiscount;
    private RelativeLayout rlPayment;
    private TextView tvBadge, tvAlertMsg;
    private RelativeLayout rlAlert;
    private String paymentMode;
    private int position;
    private double currentLat = 0.0, currentLong = 0.0;
    private double lat = 0.0, lon = 0.0;
    private String requestType = "", bankcode = "", strPrefSlot1, strPrefSlot2;
    private Dialog dialog;
    private String orderId = "", cbkIds = "";
    private ArrayList<CustomerTyepModel> arrPrefSlot1;
    private DiscountModel orderDiscountModel;
    private LatLng latLng;
    public BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            currentLat = intent.getDoubleExtra(Locations.EXTRA_LATITUDE, 0);
            currentLong = intent.getDoubleExtra(Locations.EXTRA_LONGITUDE, 0);

            latLng = new LatLng(currentLat, currentLong);
            if (currentLat != 0.0 && currentLong != 0.0) {
                lat = currentLat;
                lon = currentLong;
            }
            if (latLng != null) {
                stopService(new Intent(getApplicationContext(), Locations.class));
            }
        }
    };

    /****
     * Method for Setting the Height of the ListView dynamically.
     * *** Hack to fix the issue of not showing all the items of the ListView
     * *** when placed inside a ScrollView
     ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static boolean isDeviceLocationEnabled(Context mContext) {
        int locMode = 0;
        String locProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locMode = Settings.Secure.getInt(mContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locProviders = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locProviders);
        }
    }

    private void reqPermission() {
        new AskPermission.Builder(this).setPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                .setCallback(this)
                .setErrorCallback(this)
                .request(99);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rlPayment = (RelativeLayout) inflater.inflate(R.layout.fragment_billing_address, null, false);
        flbody.addView(rlPayment, baseLayoutParams);

        setContext(PaymentActivity.this);

//        if (getIntent().hasExtra("IsOrderDiscount"))
//            isOrderDiscount = getIntent().getBooleanExtra("IsOrderDiscount", false);

        if (getIntent().hasExtra("OrderDiscountmodel"))
            orderDiscountModel = (DiscountModel) getIntent().getSerializableExtra("OrderDiscountmodel");

        Bundle bundle = getIntent().getExtras();
        starLevelCashBackArray = (ArrayList<StarLevelCashBackDetailsModel>) bundle.getSerializable("StarColorModelArray");
        cbkIds = getIntent().getStringExtra("cbkIds");

        dialog = Utils.createLoader(PaymentActivity.this, ConstantValues.TOOLBAR_PROGRESS);

        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        isOtpPopUp = mSharedPreferences.getBoolean(ConstantValues.KEY_OTP_POPUP, false);

        reqPermission();

        build = new AlertDialog.Builder(PaymentActivity.this);
        build.setTitle("Ebutor");
        build.setMessage("please enable the locations");
        build.setPositiveButton("go to settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callGPSSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });

        build.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    // Toast.makeToast(getContext(), "There is no way back!").;
                    return true; // Consumed
                } else {
                    return false; // Not consumed
                }
            }
        });
        alertDialog = build.create();
        alertDialog.setCanceledOnTouchOutside(false);

        View toolbarLogo = findViewById(R.id.iv_toolbar_logo);

        if (toolbarLogo != null) {
            toolbarLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

        /*gpsTracker = new GPSTracker(PaymentActivity.this);
        if (!gpsTracker.getIsGPSTrackingEnabled()) {
            gpsTracker.showSettingsAlert();

        }*/

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.syncState();

//                key = "gtKFFx"; // test
        key = "5lbWi1"; // live
//        salt = "WF09hpni";

        if (getIntent().hasExtra("cashbackAmount"))
            totalCashback = getIntent().getDoubleExtra("cashbackAmount", 0.00);
        cartAmount = getIntent().getDoubleExtra("TotalAmount", 0.00);
      //  discountAmount = getIntent().getDoubleExtra("DiscountAmount", 0.00);
        discountAmount=0.0;
//        productsArray = getIntent().getStringExtra("Products");
        String cartIds = getIntent().getStringExtra("cartId");
        if (cartIds != null) {
            try {
                cartId = new JSONArray(cartIds);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        totalItems = getIntent().getIntExtra("TotalItems", 0);


        viewCC = findViewById(R.id.expandable_toggle_cc);
        viewUPI = findViewById(R.id.expandable_toggle_upi);
        viewNet = findViewById(R.id.expandable_toggle_net);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvChangeAddress = (TextView) findViewById(R.id.tv_change_address);
        tvAddNewAddress = (TextView) findViewById(R.id.tvAddAddress);
        tvCartAmount = (TextView) findViewById(R.id.tvGrandTotal);
        tvTotalAmount = (TextView) findViewById(R.id.tvTotalAmount);
        tvDiscountAmount = (TextView) findViewById(R.id.tvDiscountAmount);
        tvECashBal = (TextView) findViewById(R.id.tv_ecash_bal);
        tvTotalCashback = (TextView) findViewById(R.id.tv_cashback_amount);
        tvBadge = (TextView) findViewById(R.id.tvBadge);

        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        llMain = (LinearLayout) findViewById(R.id.ll_main);
        llDiscount = (LinearLayout) findViewById(R.id.ll_discount);
        spinnerPrefSlot1 = (Spinner) findViewById(R.id.spinner_pref_slot_1);
        spinnerPrefSlot2 = (Spinner) findViewById(R.id.spinner_pref_slot_2);
        cbCod = (CheckBox) findViewById(R.id.cb_cod);
        cbECash = (CheckBox) findViewById(R.id.cb_ecash);
//        mListView = (ListView) findViewById(R.id.list);
        spBanks = (Spinner) findViewById(R.id.sp_banks);
        btnProceed = (Button) findViewById(R.id.btnProceed);

        /*if (isFF) {
            llDiscount.setVisibility(View.GONE);
        } else {
            llDiscount.setVisibility(View.VISIBLE);
        }*/

        arrBeats = new ArrayList<>();
//        setListViewHeightBasedOnChildren(mListView);

//        mListView.setOnTouchListener(new View.OnTouchListener() {
//            // Setting on Touch Listener for handling the touch inside ScrollView
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                // Disallow the touch request for parent scroll on touch of child view
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });

//        mListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                bankcode = netBankingList.get(position).getBankCode();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(PaymentActivity.this)) {
                    switch (requestType) {
                        case "AddOrder":
                            processOrder(transactionId, mPayuId, paymentMode);
                            break;
                        case "Address":
                            getAddress();
                            break;
                        default:
                            getAddress();
                            break;
                    }
                } else {
                    Toast.makeText(PaymentActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        arrPrefSlot1 = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_PREF_SLOTS);
        if (arrPrefSlot1 != null && arrPrefSlot1.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_dropdown_item, arrPrefSlot1);
            spinnerPrefSlot1.setAdapter(arrayAdapter);
            spinnerPrefSlot2.setAdapter(arrayAdapter);
            try {
                spinnerPrefSlot1.setSelection(0);
                spinnerPrefSlot2.setSelection(0);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        spinnerPrefSlot1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strPrefSlot1 = arrPrefSlot1.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerPrefSlot2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strPrefSlot2 = arrPrefSlot1.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tvBadge.setText(String.valueOf(Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""))));

        tvTotalCashback.setText(getString(R.string.total_cashback) + " : " + String.format(Locale.CANADA, "%.2f", totalCashback));

        tvTotalCashback.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvTotalCashback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdditionalCashBackFragment additionalCashBackFragment=AdditionalCashBackFragment.newInstance(starLevelCashBackArray);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                additionalCashBackFragment.show(ft, "cashDialog");


            }
        });

        tvDiscountAmount.setText(getString(R.string.discount_value_amount) + " : " + String.format(Locale.CANADA, "%.2f", discountAmount));
        tvTotalAmount.setText(getString(R.string.total_value_amount) + " : " + String.format(Locale.CANADA, "%.2f", cartAmount));
        finalAmount = cartAmount - discountAmount;
        grandTotal = finalAmount;
        tvCartAmount.setText(String.format(Locale.CANADA, "%.2f", grandTotal));

        cbECash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (finalAmount < eCashBal) {
                        grandTotal = 0;
                        eCashApplied = finalAmount;
                    } else {
                        grandTotal = finalAmount - eCashBal;
                        eCashApplied = eCashBal;
                    }
                } else {
                    grandTotal = finalAmount;
                }
                tvCartAmount.setText(String.format(Locale.CANADA, "%.2f", grandTotal));
            }
        });


        etCardNumber = (EditText) findViewById(R.id.etCardNumber);
        etCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        etVPA = (EditText) findViewById(R.id.etVpa);
        etExpiryMonth = (EditText) findViewById(R.id.etExpiryMonth);
        etExpiryYear = (EditText) findViewById(R.id.etExpiryyear);
        etCVV = (EditText) findViewById(R.id.etCVV);
        etCardHolderName = (EditText) findViewById(R.id.etCardHolderName);
        etScheduleDeliveryDate = (EditText) findViewById(R.id.et_schedule_date);
//        btnPay = (Button) findViewById(R.id.btnPay);

        int year, month, day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH) + 1;

        scheduleDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, day + " " + (month + 1) + " " + year);
        scheduleDeliveryDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_TIME_STD_PATTERN, day + " " + (month + 1) + " " + year);
        etScheduleDeliveryDate.setText(scheduleDate);

        etExpiryMonth.setFilters(new InputFilter[]{new InputFilterMinMax(1, 12)});
        etExpiryYear.setFilters(new InputFilter[]{new InputFilterMinMax(1, 2030)});

        mPaymentParams = new PaymentParams();
        mPaymentParams.setKey(key);
        mPaymentParams.setAmount(String.format(Locale.CANADA, "%.2f", cartAmount));
        mPaymentParams.setProductInfo("Factail products");
        mPaymentParams.setFirstName("Srikanth");
        mPaymentParams.setEmail("srikanth.nama@meltag.com");
        mPaymentParams.setTxnId(System.currentTimeMillis() + "");
//                "https://payu.herokuapp.com/success", "https://payu.herokuapp.com/failure"
        mPaymentParams.setSurl("https://payu.herokuapp.com/success");
        mPaymentParams.setFurl("https://payu.herokuapp.com/failure");
        mPaymentParams.setUdf1("udf1l");
        mPaymentParams.setUdf2("udf12");
        mPaymentParams.setUdf3("udf13");
        mPaymentParams.setUdf4("udf14");
        mPaymentParams.setUdf5("udf15");

        //todo change environment
        payuConfig = new PayuConfig();
        payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);

//        generateHashFromServer(mPaymentParams);

        etScheduleDeliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int year, month, day;
                final Calendar c = Calendar.getInstance();
                if (selYear == 0 || selMonth == 0 || selDay == 0) {
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH) + 1;
                } else {
                    year = selYear;
                    month = selMonth;
                    day = selDay;
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(PaymentActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        selYear = year;
                        selMonth = monthOfYear;
                        selDay = dayOfMonth;

                        scheduleDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                        scheduleDeliveryDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_TIME_STD_PATTERN, dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                        etScheduleDeliveryDate.setText(scheduleDate);
                    }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 1));
                datePickerDialog.show();
            }
        });


        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isDeviceLocationEnabled(PaymentActivity.this)) {
                    if (!alertDialog.isShowing()) {
                        alertDialog.show();
                    }
                    return;
                }
                if (currentLat == 0.0 && currentLong == 0.0) {
                    Utils.showAlertDialog(PaymentActivity.this, getString(R.string.please_wait_loc_fetched));

                    LocalBroadcastManager.getInstance(PaymentActivity.this).registerReceiver(message
                            , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST));

                    Intent in = new Intent(PaymentActivity.this, Locations.class);
                    startService(in);
                    return;

                }

                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                if (userDefaultAddress == null) {
                    Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.please_choose_shipping_address_order));
                    return;
                }

                if (TextUtils.isEmpty(scheduleDeliveryDate)) {
                    Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.please_select_schedule_delivery_date));
                    return;
                }

                if (TextUtils.isEmpty(strPrefSlot1) || strPrefSlot1.equalsIgnoreCase("Select Preferred Slot")) {
                    Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.please_select_pref_slot_1));
                    return;
                }

                continuePayment();

            }
        });

        //dummy card data
//        etCardNumber.setText("5123456789012346");
//        etExpiryMonth.setText("05");
//        etExpiryYear.setText("2017");
//        etCVV.setText("123");

//        enableFor(viewCod, findViewById(R.id.expandable_cod), 0);

        getAddress();

        tvChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentActivity.this, UserAddressActivity.class);
                intent.putExtra("Addresses", userAddressModelArrayList);
                startActivityForResult(intent, 321);
            }
        });
        tvAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentActivity.this, UserAddressActivity.class);
                startActivityForResult(intent, 321);
            }
        });

        cbCod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    findViewById(R.id.expandable_toggle_cc).setOnClickListener(null);
                    findViewById(R.id.expandable_toggle_upi).setOnClickListener(null);
                    findViewById(R.id.expandable_toggle_net).setOnClickListener(null);
                } else {
                    enableFor(viewCC, findViewById(R.id.expandable_cc), 2);
//                    enableFor(viewUPI, findViewById(R.id.expandable_upi), 1);
                    enableFor(viewNet, findViewById(R.id.expandable_net), 3);
                }
                etCardNumber.setText("");
                etCardHolderName.setText("");
                etExpiryMonth.setText("");
                etExpiryYear.setText("");
                etCVV.setText("");
                spBanks.setSelection(0);
                findViewById(R.id.expandable_upi).setVisibility(View.GONE);
                findViewById(R.id.expandable_cc).setVisibility(View.GONE);
                findViewById(R.id.expandable_net).setVisibility(View.GONE);
            }
        });

        spBanks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankcode = netBankingList.get(position).getBankCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        enableFor(viewUPI, findViewById(R.id.expandable_upi), 1);
        enableFor(viewCC, findViewById(R.id.expandable_cc), 2);
        enableFor(viewNet, findViewById(R.id.expandable_net), 3);

        /*btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cardNumber = etCardNumber.getText().toString().trim();
                String expiryMonth = etExpiryMonth.getText().toString().trim();
                String expiryYear = etExpiryYear.getText().toString().trim();
                String cvv = etCVV.getText().toString().trim();


                if (cardNumber.contains(" ")) {
                    cardNumber = cardNumber.replace(" ", "");
                }
                mPaymentParams = new PaymentParams();
                mPaymentParams.setKey(key);
                mPaymentParams.setAmount(String.format(Locale.CANADA, "%.2f", cartAmount));
                mPaymentParams.setProductInfo("Factail products");
                mPaymentParams.setFirstName("Srikanth");
                mPaymentParams.setEmail("srikanth.nama@meltag.com");
                mPaymentParams.setTxnId(System.currentTimeMillis() + "");
//                "https://payu.herokuapp.com/success", "https://payu.herokuapp.com/failure"
                mPaymentParams.setSurl("https://payu.herokuapp.com/success");
                mPaymentParams.setFurl("https://payu.herokuapp.com/failure");
                mPaymentParams.setUdf1("udf1l");
                mPaymentParams.setUdf2("udf12");
                mPaymentParams.setUdf3("udf13");
                mPaymentParams.setUdf4("udf14");
                mPaymentParams.setUdf5("udf15");


                mPaymentParams.setCardNumber(cardNumber);
                mPaymentParams.setCardName(cardNumber);
                mPaymentParams.setNameOnCard(cardNumber);
                mPaymentParams.setExpiryMonth(expiryMonth);// MM
                mPaymentParams.setExpiryYear(expiryYear);// YYYY
                mPaymentParams.setCvv(cvv);

//                mPaymentParams.setEnableOneClickPayment(1);

                generateHashFromServer(mPaymentParams);

            }
        });*/

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                deleteCart();
//                finish();
            }
        }, 1000 * 60 * 10);

    }

    private void generateOtp() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("telephone", mSharedPreferences.getString(ConstantValues.KEY_MOBILE, ""));

        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("data", obj.toString());

        VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.generateotpURL, map, PaymentActivity.this, PaymentActivity.this, PARSER_TYPE.GENERATE_OTP);
        getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.generateotpURL);

        if (dialog != null)
            dialog.show();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user asynchronously -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("permission request")
                        .setMessage("please....enable the location")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(PaymentActivity.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private void showOtpDialog() {

        Intent intent = new Intent(PaymentActivity.this, ConfirmOtpActivity.class);
        intent.putExtra("MobileNo", mSharedPreferences.getString(ConstantValues.KEY_MOBILE, ""));
        intent.putExtra("isFromPayment", true);
        startActivityForResult(intent, 111);

//        FragmentManager fm = getSupportFragmentManager();
//        OtpFragment otpFragment = OtpFragment.newInstance(mSharedPreferences.getString(ConstantValues.KEY_MOBILE, ""), true);
//        otpFragment.setClickListener(PaymentActivity.this);
//        otpFragment.setCancelable(true);
//        otpFragment.show(fm, "otp_fragment");

    }

    private void continuePayment() {
        if (cbCod.isChecked()) {
            paymentMode = "cod";
        } else if (!TextUtils.isEmpty(etVPA.getText().toString().trim())) {
            String vpa = etVPA.getText().toString().trim();
            if (!VPA_PATTERN.matcher(vpa).matches()) {
                new AlertDialog.Builder(PaymentActivity.this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.please_enter_valid_vpa))
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
                return;
            }

            paymentMode = "upi";

        } else {
            Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.please_select_payment_method));
            return;
        }

        if (isFF && isOtpPopUp) {
            generateOtp();
        } else {
            genarateOrderref();

        }

        /*if (!TextUtils.isEmpty(etCardNumber.getText().toString())) {
            paymentMode = "Credit/Debit Card";
            String cardNumber = etCardNumber.getText().toString().trim();
            String expiryMonth = etExpiryMonth.getText().toString().trim();
            String expiryYear = etExpiryYear.getText().toString().trim();
            String cvv = etCVV.getText().toString().trim();

            if (cardNumber.contains(" ")) {
                cardNumber = cardNumber.replace(" ", "");
            }

            mPaymentParams.setCardNumber(cardNumber);
            mPaymentParams.setCardName(cardNumber);
            mPaymentParams.setNameOnCard(cardNumber);
            mPaymentParams.setExpiryMonth(expiryMonth);// MM
            mPaymentParams.setExpiryYear(expiryYear);// YYYY
            mPaymentParams.setCvv(cvv);

//                mPaymentParams.setEnableOneClickPayment(1);

            launchSdkUI(mPayUHashes, PayuConstants.CC);


        } else if (bankcode != null && !TextUtils.isEmpty(bankcode)) {

            paymentMode = "Net Banking";

            mPaymentParams.setHash(mPayUHashes.getPaymentHash());
            mPaymentParams.setBankCode(bankcode);

            launchSdkUI(mPayUHashes, PayuConstants.NB);
        } else {
            Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.please_select_payment_method));
        }*/
    }

    private void genarateOrderref() {

        final JSONObject obj = new JSONObject();

        try {
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_LE_WH_IDS, ""));

            Log.e("generateOrderRefURL", obj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("data", obj.toString());

        VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.generateOrderRefURL, map, PaymentActivity.this, PaymentActivity.this, PARSER_TYPE.GENERATE_ORDER_REF);
        getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.generateOrderRefURL);

        if (dialog != null)
            dialog.show();

    }

    private void validateUPIPayment(String customervpa) {

//        String orderId = "ABC123";//todo
        String amount = String.format(Locale.CANADA, "%.2f", cartAmount);

        JSONObject obj = new JSONObject();

        try {
            obj.put("amount", amount);
            obj.put("orderId", orderId);
            obj.put("customerVpa", customervpa);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("Request", obj.toString());

        // getMhhferchant Token
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.EBUTOR_COLLECT_URL, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject object) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();


                try {
//                    JSONObject object = new JSONObject(result);
                    String code = object.optString("code");
                    String _result = object.optString("result");

                    if (code.equals("00")) {
                        JSONObject dataObj = object.optJSONObject("data");
                        String merchTranId = dataObj.optString("merchTranId");
                        String wCollectTxnId = dataObj.optString("wCollectTxnId");

                        processOrder(wCollectTxnId, merchTranId, paymentMode);//todo
                    } else {
                        Utils.showAlertDialog(PaymentActivity.this, _result);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("Response", object.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(request, AppURL.EBUTOR_COLLECT_URL);

        if (dialog != null)
            dialog.show();
    }

    private void deleteCart() {
        if (Networking.isNetworkAvailable(PaymentActivity.this)) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "DeleteCart";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("isClearCart", "true");

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.deleteCartURL, map, PaymentActivity.this, PaymentActivity.this, PARSER_TYPE.DELETE_CART);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.deleteCartURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "DeleteCart";
        }
    }

    @Override
    public void onBackPressed() {
        // disable device back button
    }

    private void getAddress() {
        if (Networking.isNetworkAvailable(PaymentActivity.this)) {
            requestType = "Address";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask addressesRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAddressURL, map,
                        PaymentActivity.this, PaymentActivity.this, PARSER_TYPE.GET_ADDRESSES);
                addressesRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(addressesRequest, AppURL.getAddressURL);
                if (dialog != null)
                    dialog.show();

//                HTTPBackgroundTask task = new HTTPBackgroundTask(PaymentActivity.this, PaymentActivity.this, PARSER_TYPE.GET_ADDRESSES, APIREQUEST_TYPE.HTTP_POST);
//                task.execute(map);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }

        } else {
            requestType = "Address";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }

    }

    private void enableFor(final View button, final View target, final int position) {

        etCardNumber.setText("");
        etCardHolderName.setText("");
        etExpiryMonth.setText("");
        etExpiryYear.setText("");
        etCVV.setText("");
        spBanks.setSelection(0);

        if (target == lastOpen && position != lastOpenPosition) {
            // lastOpen is recycled, so its reference is false
            lastOpen = null;
        }
        if (position == lastOpenPosition) {
            // re reference to the last view
            // so when can animate it when collapsed
            lastOpen = target;
        }
        int height = viewHeights.get(position, -1);
        if (height == -1) {
            viewHeights.put(position, target.getMeasuredHeight());
            updateExpandable(target, position);
        } else {
            updateExpandable(target, position);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                etCardNumber.setText("");
                etCardHolderName.setText("");
                etExpiryMonth.setText("");
                etExpiryYear.setText("");
                etCVV.setText("");
                spBanks.setSelection(0);
                etVPA.setText("");

                Animation a = target.getAnimation();

                if (a != null && a.hasStarted() && !a.hasEnded()) {

                    a.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            view.performClick();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                } else {

                    target.setAnimation(null);

                    int type = target.getVisibility() == View.VISIBLE
                            ? ExpandCollapseAnimation.COLLAPSE
                            : ExpandCollapseAnimation.EXPAND;

                    // remember the state
                    if (type == ExpandCollapseAnimation.EXPAND) {
                        openItems.set(position, true);
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        if (etVPA != null)
//                            imm.showSoftInput(etVPA, InputMethodManager.SHOW_IMPLICIT);
                    } else {
                        openItems.set(position, false);
                    }
                    // check if we need to collapse a different view
                    if (type == ExpandCollapseAnimation.EXPAND) {
                        if (lastOpenPosition != -1 && lastOpenPosition != position) {
                            if (lastOpen != null) {
                                animateView(lastOpen, ExpandCollapseAnimation.COLLAPSE);

                            }
                            openItems.set(lastOpenPosition, false);
                        }
                        lastOpen = target;
                        lastOpenPosition = position;
                    } else if (lastOpenPosition == position) {
                        lastOpenPosition = -1;
                    }
                    animateView(target, type);
                }
            }
        });
    }

    /**
     * Performs either COLLAPSE or EXPAND animation on the target view
     *
     * @param target the view to animate
     * @param type   the animation type, either ExpandCollapseAnimation.COLLAPSE
     *               or ExpandCollapseAnimation.EXPAND
     */
    private void animateView(final View target, final int type) {
        Animation anim = new ExpandCollapseAnimation(
                target,
                type
        );
        anim.setDuration(animationDuration);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {


            }
        });
        target.startAnimation(anim);
    }

    private void updateExpandable(View target, int position) {

        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) target.getLayoutParams();
        if (openItems.get(position)) {
            target.setVisibility(View.VISIBLE);
            params.bottomMargin = 0;
        } else {
            target.setVisibility(View.GONE);
            params.bottomMargin = 0 - viewHeights.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_payment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            deleteCart();
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null && results instanceof String) {
            String result = (String) results;
            if (!TextUtils.isEmpty(result)) {
                try {
                    JSONObject object = new JSONObject(result);
                    String status = object.optString("status");
                    String message = object.optString("message");
                    if (status.equalsIgnoreCase("success")) {
                        if (requestType == PARSER_TYPE.GET_ADDRESSES) {
                            userAddressModelArrayList = new ArrayList<>();

                            JSONArray dataArray = object.optJSONArray("data");
                            if (dataArray != null && dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject addressObj = dataArray.optJSONObject(i);
                                    UserAddressModel userAddressModel = new UserAddressModel();
                                    userAddressModel.setAddressId(addressObj.optString("address_id").equalsIgnoreCase("null") ? "" : addressObj.optString("address_id"));
                                    userAddressModel.setFirstName(addressObj.optString("FirstName").equalsIgnoreCase("null") ? "" : addressObj.optString("FirstName"));
                                    userAddressModel.setLastName(addressObj.optString("LastName").equalsIgnoreCase("null") ? "" : addressObj.optString("LastName"));
                                    userAddressModel.setAddress(addressObj.optString("Address").equalsIgnoreCase("null") ? "" : addressObj.optString("Address"));
                                    userAddressModel.setAddress1(addressObj.optString("Address1").equalsIgnoreCase("null") ? "" : addressObj.optString("Address1"));
                                    userAddressModel.setCity(addressObj.optString("City").equalsIgnoreCase("null") ? "" : addressObj.optString("City"));
                                    userAddressModel.setPin(addressObj.optString("pin").equalsIgnoreCase("null") ? "" : addressObj.optString("pin"));
                                    userAddressModel.setState(addressObj.optString("state").equalsIgnoreCase("null") ? "" : addressObj.optString("state"));
                                    userAddressModel.setCountry(addressObj.optString("country").equalsIgnoreCase("null") ? "" : addressObj.optString("country"));
                                    userAddressModel.setAddressType(addressObj.optString("addressType").equalsIgnoreCase("null") ? "" : addressObj.optString("addressType"));
                                    userAddressModel.setTelephone(addressObj.optString("telephone").equalsIgnoreCase("null") ? "" : addressObj.optString("telephone"));
                                    userAddressModel.setEmail(addressObj.optString("email").equalsIgnoreCase("null") ? "" : addressObj.optString("email"));

                                    if (i == 0)
                                        userDefaultAddress = userAddressModel;


                                    userAddressModelArrayList.add(userAddressModel);
                                }

                                if (userDefaultAddress != null) {
                                    if (TextUtils.isEmpty(userDefaultAddress.getAddress1())) {
                                        tvAddress.setText(String.format(getResources().getString(R.string.userAddress1), userDefaultAddress.getFirstName(),
                                                userDefaultAddress.getLastName(), userDefaultAddress.getAddress(), userDefaultAddress.getCity(),
                                                userDefaultAddress.getState(), userDefaultAddress.getPin(), userDefaultAddress.getTelephone()));
                                    } else {
                                        tvAddress.setText(String.format(getResources().getString(R.string.userAddress), userDefaultAddress.getFirstName(),
                                                userDefaultAddress.getLastName(), userDefaultAddress.getAddress(), userDefaultAddress.getAddress1(), userDefaultAddress.getCity(),
                                                userDefaultAddress.getState(), userDefaultAddress.getPin(), userDefaultAddress.getTelephone()));
                                    }
                                }

                                if (TextUtils.isEmpty(tvAddress.getText().toString())) {
                                    tvChangeAddress.setVisibility(View.GONE);
                                } else if (flag) {
                                    tvChangeAddress.setVisibility(View.VISIBLE);
                                }
                            }
                        } *//*else if (requestType == PARSER_TYPE.GET_PROFILE) {
                            this.profileObj = object.optJSONObject("data");
                        }*//* else if (requestType == PARSER_TYPE.ADD_ORDER) {

                            JSONObject dataObj = object.optJSONObject("data");
                            Intent intent = new Intent(PaymentActivity.this, OrderConfirmationActivity.class);
                            intent.putExtra("Data", dataObj.toString());
                            intent.putExtra("paymentMode", paymentMode);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        Utils.showAlertDialog(PaymentActivity.this, message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showAlertDialog(PaymentActivity.this, e.getMessage());
                }

            } else {
                Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.something_wrong));
            }
        } else {
            Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {
        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            if (requestType == PARSER_TYPE.ADD_ORDER) {
                this.requestType = "AddOrder";
            } else if (requestType == PARSER_TYPE.GET_ADDRESSES && requestType == PARSER_TYPE.GET_PROFILE) {
                this.requestType = "Address";
            }
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }

    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == 321) {

            if (data != null && data.hasExtra("SelectedAddress")) {
                userDefaultAddress = (UserAddressModel) data.getSerializableExtra("SelectedAddress");
                if (data.hasExtra("position")) {
                    if (data.getStringExtra("position") != null) {
                        position = Integer.parseInt(data.getStringExtra("position"));
                        userAddressModelArrayList.set(position, userDefaultAddress);
                    }
                }
                boolean foundAddress = false;

                for (UserAddressModel model : userAddressModelArrayList) {
                    if (model.getAddress().equalsIgnoreCase(userDefaultAddress.getAddress())) {
                        foundAddress = true;
                    }
                }
                if (!foundAddress) {
                    userAddressModelArrayList.add(userDefaultAddress);
                }

                if (TextUtils.isEmpty(userDefaultAddress.getAddress1())) {
                    tvAddress.setText(String.format(getResources().getString(R.string.userAddress1), userDefaultAddress.getFirstName(),
                            userDefaultAddress.getLastName(), userDefaultAddress.getAddress(), userDefaultAddress.getCity(),
                            userDefaultAddress.getState(), userDefaultAddress.getPin(), userDefaultAddress.getTelephone()));
                } else {
                    tvAddress.setText(String.format(getResources().getString(R.string.userAddress), userDefaultAddress.getFirstName(),
                            userDefaultAddress.getLastName(), userDefaultAddress.getAddress(), userDefaultAddress.getAddress1(), userDefaultAddress.getCity(),
                            userDefaultAddress.getState(), userDefaultAddress.getPin(), userDefaultAddress.getTelephone()));
                }
            }

        } else if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {
                // Got the response from Gateway, need to analyse the response now
                // todo parse response..
                HashMap<String, String> map = getPaymentResponse(data.getStringExtra("result"));
                if (map != null) {

                    String status = null;
                    Iterator<String> payuHashIterator = map.keySet().iterator();
                    while (payuHashIterator.hasNext()) {
                        String key = payuHashIterator.next();
                        switch (key) {
                            case "status":
                                status = map.get(key);
                                Log.e("Status", map.get(key));
                                break;
                            case "mihpayid":
                                mPayuId = map.get(key);
                                break;
                            case "txnid":
                                transactionId = map.get(key);
                                break;

                            default:
                                Log.e(key, map.get(key));
                                break;
                        }
                    }

                    if (status != null && status.equalsIgnoreCase("success")) {
//                        Utils.showAlertDialog(PaymentActivity.this, "Payment Success "+mihpayid);
                        /*startActivity(new Intent(PaymentActivity.this, OrderConfirmationActivity.class));
                        finish();*/
                        paymentMode = "payu";
                        processOrder(transactionId, mPayuId, paymentMode);
                    } else {
                        Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.payment_failure) + status);
                    }
                } else {
                    Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.something_wrong));
                }

            } else {
                // todo order cancel - User pressed back button without making payment
                Toast.makeText(PaymentActivity.this, getResources().getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == 111) {
            genarateOrderref();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void processOrder(String txnId, String payuId, String paymentMethod) {

        if (Networking.isNetworkAvailable(PaymentActivity.this)) {
            requestType = "AddOrder";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);

            try {
                JSONObject object = new JSONObject();
//                FF_ID,FF_TOKEN
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    object.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    object.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }
                object.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                object.put("customer_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
//                if (profileObj != null) {
//                    String firstName = profileObj.optString("firstname");
//                    String documents = profileObj.optString("documents");
//                    String company = profileObj.optString("company");
//                    String location = profileObj.optString("location");
//                    String address1 = profileObj.optString("address_1");
//                    String address2 = profileObj.optString("address_2");
//                    String city = profileObj.optString("city");
//                    String country = profileObj.optString("name");
//                    String postcode = profileObj.optString("postcode");
//                    String telephone = profileObj.optString("telephone");
//                    String email = profileObj.optString("email");
//
//
////                    object.put("firstname", firstName);
////                    object.put("lastname", firstName);
////                    object.put("email", email);
////                    object.put("telephone", telephone);
////                    object.put("address", address1 + "\n" + address2);
////                    object.put("city", city);
////                    object.put("pin", postcode);
////                    object.put("country", country);
//                } else {
//                    Utils.showAlertDialog(PaymentActivity.this, "Unable to get Profile Info. Please update your profile");
//                    return;
//                }

                if (userDefaultAddress != null) {
                    object.put("address_id", userDefaultAddress.getAddressId());
//                    object.put("shipping_firstname", userDefaultAddress.getFirstName());
//                    object.put("shipping_lastname", userDefaultAddress.getLastName());
//                    object.put("shipping_email", "");
//                    object.put("shipping_telephone", "");
//                    object.put("shipping_address", userDefaultAddress.getAddress() + "\n" + userDefaultAddress.getAddress1());
//                    object.put("shipping_city", userDefaultAddress.getCity());
//                    object.put("shipping_pin", userDefaultAddress.getPin());
//                    object.put("shipping_country", "India");
//                    object.put("shipping_state", userDefaultAddress.getState());
                } else {
                    Utils.showAlertDialog(PaymentActivity.this, getResources().getString(R.string.please_choose_shipping_address_order));
                    return;
                }

                if (cbECash.isChecked()) {
                    object.put("applied_ecash", eCashApplied);
                }
                object.put("order_level_cashback", cbkIds);

                object.put("otp", otp);
                object.put("orderId", orderId);
                object.put("cartId", cartId);
                /*if (*//*isOrderDiscount && *//*orderDiscountModel != null) {
                    object.put("discount", orderDiscountModel.getDiscount());
                    object.put("discount_type", orderDiscountModel.getDiscountType());
                    object.put("discount_on", orderDiscountModel.getDiscountOn());
                    object.put("discount_on_values", orderDiscountModel.getDiscountValues());
                }*/
                object.put("total", cartAmount);
              //  object.put("discountAmount", discountAmount);
                object.put("final_amount", grandTotal);
                object.put("coupon_code", "");
                object.put("coupon_value", "");
                object.put("offerId", "");
                object.put("platform_id", ConstantValues.PLATFORM_ID);
                object.put("paymentmode", paymentMethod);
                object.put("wCollectTxnId", txnId);//wCollectTxnId in case of UPI
                object.put("merchTranId", payuId);//merchTranId in case of UPI
                object.put("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));
                object.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                object.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                object.put("hub", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                object.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                object.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                object.put("scheduled_delivery_date", scheduleDeliveryDate);
                object.put("pref_value", strPrefSlot1);
                object.put("pref_value1", strPrefSlot2);

                /*if (gpsTracker == null) {
                    gpsTracker = new GPSTracker(PaymentActivity.this);
                }

                currentLat = gpsTracker.getLatitude();
                currentLong = gpsTracker.getLongitude();

                object.put("latitude", currentLat + "");
                object.put("longitude", currentLong + "");*/
                if (currentLat == 0.0 && currentLong == 0.0) {


                    Toast.makeText(this, getString(R.string.please_wait_loc_fetched), Toast.LENGTH_SHORT).show();
                    LocalBroadcastManager.getInstance(this).registerReceiver(message
                            , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
                    );

                    Intent in = new Intent(this, Locations.class);
                    startService(in);

                }

                if (lat != 0.0) {
                    object.put("latitude", lat);
                } else {
                    object.put("latitude", "");
                }

                if (lon != 0.0) {
                    object.put("longitude", lon);
                } else {
                    object.put("longitude", "");
                }
//                object.put("payment_method", paymentMethod);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", object.toString());

                VolleyBackgroundTask addOrderRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addOrderURL, map,
                        PaymentActivity.this, PaymentActivity.this, PARSER_TYPE.ADD_ORDER);
                addOrderRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(addOrderRequest, AppURL.addOrderURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
                Utils.showAlertDialog(PaymentActivity.this, e.getMessage());
                return;
            }
        } else {
            requestType = "AddOrder";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private HashMap<String, String> getPaymentResponse(String str) {

        /*String str = "mihpayid=700000000010847584&amp;mode=CC&amp;" +
                "status=success&amp;unmappedstatus=captured&amp;key=gtKFFx&amp;" +
                "txnid=1458130970405&amp;amount=10.00&amp;cardCategory=domestic&amp;" +
                "discount=0.00&amp;net_amount_debit=10&amp;addedon=2016-03-16+17%3A52%3A53&amp;" +
                "productinfo=Factail+products&amp;firstname=Srikanth&amp;lastname=&amp;address1=&amp;" +
                "address2=&amp;city=&amp;state=&amp;country=&amp;" +
                "zipcode=&amp;email=srikanth.nama%40meltag.com&amp;" +
                "phone=&amp;udf1=udf1l&amp;udf2=udf12&amp;udf3=udf13&amp;udf4=udf14&amp;" +
                "udf5=udf15&amp;udf6=&amp;udf7=&amp;udf8=&amp;udf9=&amp;udf10=&amp;" +
                "hash=b17c1dc2b44fe3215f730483b98ed8fc057d5f5d2aaad2622dfe84de387e9815a27878817d1c54d35c64b2c071d84f15eaca50b33650d5d181aee7861e20c27e&amp;" +
                "field1=607648153312&amp;field2=999999&amp;field3=9445710521760761&amp;field4=-1&amp;" +
                "field5=&amp;field6=&amp;field7=&amp;field8=&amp;field9=SUCCESS&amp;" +
                "payment_source=payu&amp;PG_TYPE=HDFCPG&amp;bank_ref_num=9445710521760761&amp;bankcode=CC&amp;" +
                "error=E000&amp;error_Message=No+Error&amp;name_on_card=5123456789012346&amp;cardnum=512345XXXXXX2346&amp;" +
                "cardhash=This+field+is+no+longer+supported+in+postback+params.&amp;issuing_bank=HDFC&amp;
                card_type=MAST&amp;device_type=1";

*/

        String[] separated = str.split("&amp;");

        HashMap<String, String> map = new HashMap<>();

        if (separated.length > 0) {
            for (String string : separated) {
                if (string.contains("=")) {
                    String[] keyValue = string.split("=");
                    if (keyValue.length == 1) {
                        map.put(keyValue[0], "");
                    } else {
                        map.put(keyValue[0], keyValue[1]);
                    }
                }

            }
        }
        return map;
    }

    /******************************
     * Server hash generation
     ********************************/
    // lets generate hashes from server
    public void generateHashFromServer(PaymentParams mPaymentParams) {
//        nextButton.setEnabled(false); // lets not allow the user to click the button again and again.
        // lets create the post params
        try {

            JSONObject dataObj = new JSONObject();

            dataObj.put(PayuConstants.KEY, mPaymentParams.getKey());
            dataObj.put(PayuConstants.AMOUNT, mPaymentParams.getAmount());
            dataObj.put(PayuConstants.TXNID, mPaymentParams.getTxnId());
            dataObj.put(PayuConstants.EMAIL, null == mPaymentParams.getEmail() ? "" : mPaymentParams.getEmail());
            dataObj.put(PayuConstants.PRODUCT_INFO, mPaymentParams.getProductInfo());
            dataObj.put(PayuConstants.FIRST_NAME, null == mPaymentParams.getFirstName() ? "" : mPaymentParams.getFirstName());
            dataObj.put(PayuConstants.UDF1, mPaymentParams.getUdf1() == null ? "" : mPaymentParams.getUdf1());
            dataObj.put(PayuConstants.UDF2, mPaymentParams.getUdf2() == null ? "" : mPaymentParams.getUdf2());
            dataObj.put(PayuConstants.UDF3, mPaymentParams.getUdf3() == null ? "" : mPaymentParams.getUdf3());
            dataObj.put(PayuConstants.UDF4, mPaymentParams.getUdf4() == null ? "" : mPaymentParams.getUdf4());
            dataObj.put(PayuConstants.UDF5, mPaymentParams.getUdf5() == null ? "" : mPaymentParams.getUdf5());
//            dataObj.put(PayuConstants.SALT, "eCwWELxi");
            dataObj.put(PayuConstants.SALT, "LIVE");


            HashMap<String, String> map = new HashMap<>();
            map.put("data", dataObj.toString());
            // make api call
            GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
            getHashesFromServerTask.execute(map);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void launchSdkUI(PayuHashes payuHashes, String payMode) {

        PostData postData = new PaymentPostParams(mPaymentParams, payMode).getPaymentPostParams();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            // launch webview
            payuConfig = new PayuConfig();
            payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);
            payuConfig.setData(postData.getResult());
            Intent intent = new Intent(this, PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        } else {
            // something went wrong
            Toast.makeText(this, postData.getResult(), Toast.LENGTH_LONG).show();
        }


        /*// let me add the other params which i might use from other activity

        Intent intent = new Intent(ChoosePaymentMethodActivity.this, PaymentsActivity.class);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
//        intent.putExtra(PayuConstants.PAYMENT_DEFAULT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);


        *//**
         *  just for testing, dont do this in production.
         *  i need to generate hash for {@link com.payu.india.Tasks.GetTransactionInfoTask} since it requires transaction id, i don't generate hash from my server
         *  merchant should generate the hash from his server.
         *
         *//*
        intent.putExtra(PayuConstants.SALT, salt);

        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);*/

    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

//            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append(entry.getKey());
            result.append("=");
//            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            result.append(entry.getValue());
        }

        return result.toString();
    }

    public String performPostCall(String requestURL,
                                  String postDataParams) throws IOException {

        URL url;
        String response = "";

        url = new URL(requestURL);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(35000);
        conn.setConnectTimeout(35000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);


        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(postDataParams);

        writer.flush();
        writer.close();
        os.close();
        int responseCode = conn.getResponseCode();

        if (responseCode == HttpsURLConnection.HTTP_OK) {
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }
        } else {
            response = "";

        }

        Log.e("response=", "" + response);

        return response;
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(message);
        stopService(new Intent(this, Locations.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopService(new Intent(this, Locations.class));
    }

    @Override
    public void onPaymentRelatedDetailsResponse(PayuResponse payuResponse) {
        mPayuResponse = payuResponse;
        if (payuResponse.isResponseAvailable() && payuResponse.getResponseStatus().getCode() == PayuErrors.NO_ERROR) {
            if (payuResponse.isNetBanksAvailable()) { // okay we have net banks now.
                netBankingList = new ArrayList<PaymentDetails>();
                PaymentDetails paymentDetails = new PaymentDetails();
                paymentDetails.setBankName("Please Select");
                paymentDetails.setBankCode("");
                paymentDetails.setBankId("");
                netBankingList = mPayuResponse.getNetBanks();
                netBankingList.add(0, paymentDetails);
                PayUNetBankingAdapter payUNetBankingAdapter = new PayUNetBankingAdapter(PaymentActivity.this, netBankingList);
                spBanks.setAdapter(payUNetBankingAdapter);
//                mListView.setAdapter(payUNetBankingAdapter);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if (error != null) {
//            Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GENERATE_ORDER_REF) {
                    if (response instanceof String) {
                        orderId = (String) response;
                        if (cbCod.isChecked()) {
                            processOrder("", "", paymentMode);
                        } else {
                            String customervpa = etVPA.getText().toString().trim();
                            validateUPIPayment(customervpa);
                        }
                    }
                } else if (requestType == PARSER_TYPE.GENERATE_OTP) {
                    showOtpDialog();
                } else if (requestType == PARSER_TYPE.GET_ADDRESSES) {
                    if (response instanceof AddressModel) {
                        AddressModel addressModel = (AddressModel) response;
                        flag = addressModel.isFlag();
                        eCashBal = addressModel.geteCashBal();

                        mSharedPreferences.edit().putFloat(ConstantValues.KEY_ECASH, (float) eCashBal).apply();

                        if (eCashBal > 0) {
                            cbECash.setEnabled(false);
                            String s = getString(R.string.your_ecash_bal) + " " + String.format(getResources().getString(R.string.rupee)) + String.format(Locale.CANADA, "%.2f", eCashBal);
                            String s1 = getString(R.string.you_can_apply_ecash_delivery_level);
                            SpannableString ss1 = new SpannableString(s);
                            SpannableString ss2 = new SpannableString(s1);
                            ss1.setSpan(new RelativeSizeSpan(2f), 0, s.length(), 0);
                            ss2.setSpan(new RelativeSizeSpan(1f), 0, s1.length(), 0);
                            Utils.showAlertWithMessage(PaymentActivity.this, ss1 + "\n" + ss2);
                        } else {
                            cbECash.setEnabled(false);
                        }

                        tvECashBal.setText(getString(R.string.ecash_bal) + " : " + String.format(Locale.CANADA, "%.2f", eCashBal));

                        if (flag) {
                            tvAddNewAddress.setVisibility(View.VISIBLE);
                            tvChangeAddress.setVisibility(View.VISIBLE);
                        }
                        userAddressModelArrayList = addressModel.getArrUserAddr();

                        if (userAddressModelArrayList != null && userAddressModelArrayList.size() > 0) {

                            userDefaultAddress = userAddressModelArrayList.get(0);

                            if (userDefaultAddress != null) {
                                if (TextUtils.isEmpty(userDefaultAddress.getAddress1())) {
                                    tvAddress.setText(String.format(getResources().getString(R.string.userAddress1), userDefaultAddress.getFirstName(),
                                            userDefaultAddress.getLastName(), userDefaultAddress.getAddress(), userDefaultAddress.getCity(),
                                            userDefaultAddress.getState(), userDefaultAddress.getPin(), userDefaultAddress.getTelephone()));
                                } else {
                                    tvAddress.setText(String.format(getResources().getString(R.string.userAddress), userDefaultAddress.getFirstName(),
                                            userDefaultAddress.getLastName(), userDefaultAddress.getAddress(), userDefaultAddress.getAddress1(), userDefaultAddress.getCity(),
                                            userDefaultAddress.getState(), userDefaultAddress.getPin(), userDefaultAddress.getTelephone()));
                                }
                            }

                            if (TextUtils.isEmpty(tvAddress.getText().toString())) {
                                tvChangeAddress.setVisibility(View.GONE);
                            } else if (flag) {
                                tvChangeAddress.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }/*else if (requestType == PARSER_TYPE.GET_PROFILE) {
                    this.profileObj = jsonObject.optJSONObject("data");
                }*/ else if (requestType == PARSER_TYPE.ADD_ORDER) {
                    if (response instanceof Double) {
                        eCashBal = (double) response;
                        // to clear all the records in Cart table for this customer ID
                        DBHelper.getInstance().deleteCartRow(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""), "", true, false);
                        DBHelper.getInstance().deleteProductSlabRow(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""), "", true);

                        if (!TextUtils.isEmpty(mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""))) {
                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_OUTLETS);
                            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_CHECKIN, false).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_TOKEN, mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, "")).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_ID, mSharedPreferences.getString(ConstantValues.KEY_FF_ID, "")).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_GRP_ID, mSharedPreferences.getString(ConstantValues.CUSTOMER_GRP_ID, "")).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_SEGMENT_ID, mSharedPreferences.getString(ConstantValues.SEGMENT_ID, "")).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_SHOP_NAME, "").apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_FIRST_NAME, mSharedPreferences.getString(ConstantValues.KEY_FF_NAME, "")).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, mSharedPreferences.getString(ConstantValues.KEY_LE_WH_IDS, "")).apply();
                            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, false).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, "").apply();
                            tvRetailerName.setText(getString(R.string.no_retailer_selected));
                            tvPerson.setText(mSharedPreferences.getString(ConstantValues.KEY_FF_NAME, ""));
                            MyApplication.getInstance().setArrProductFilters(null);
                        } /*else {
                            mSharedPreferences.edit().putFloat(ConstantValues.KEY_ECASH, (float) eCashBal).apply();
                        }*/
                        AlertDialog.Builder dialog = new AlertDialog.Builder(PaymentActivity.this);
                        dialog.setMessage(message);
                        dialog.setTitle(getString(R.string.app_name));
                        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
//                                MyApplication.getInstance().setCartCount(0);
                                if (isFF) {
                                    Intent intent = new Intent(PaymentActivity.this, OutletsActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("isFromPayment", true);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(PaymentActivity.this, HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            }
                        });
                        AlertDialog alert = dialog.create();
                        alert.setCanceledOnTouchOutside(false);
                        alert.setCancelable(false);
                        alert.show();
//                        Intent intent = new Intent(PaymentActivity.this, OrderConfirmationActivity.class);
//                        intent.putExtra("Data", dataObj);
//                        intent.putExtra("paymentMode", paymentMode);
//                        startActivity(intent);
//                        finish();
                    }
                } else if (requestType == PARSER_TYPE.DELETE_CART) {
                    finish();
                } else if (requestType == PARSER_TYPE.GET_MASTER_LOOKUP_DISCOUNTS) {
                    if (response instanceof MasterLookUpModel) {
                        finish();

                    }
                }
            } else if (status.equals("0") && requestType == PARSER_TYPE.ADD_ORDER) {
                showRefreshedCartAlert(message);
            } else if (status.equals("-3") && requestType == PARSER_TYPE.ADD_ORDER) {
                showRefreshedDiscountAlert(message);
            } else {
                Utils.showAlertWithMessage(PaymentActivity.this, message);
            }
        } else {
            Utils.showAlertWithMessage(PaymentActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    public void showRefreshedDiscountAlert(String string) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(PaymentActivity.this);
        dialog.setMessage(string);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getDiscounts();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private void getDiscounts() {
        if (Networking.isNetworkAvailable(PaymentActivity.this)) {
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                obj.put("key", "discounts");
                map.put("data", obj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask discountRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.masterLookUpURL, map, PaymentActivity.this, PaymentActivity.this, PARSER_TYPE.GET_MASTER_LOOKUP_DISCOUNTS);
            discountRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(discountRequest, AppURL.masterLookUpURL);
            if (dialog != null)
                dialog.show();
        }
    }

    public void showRefreshedCartAlert(String string) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(PaymentActivity.this);
        dialog.setMessage(string);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isDeviceLocationEnabled(this)) {
            // Utils.showAlertDialog(BuyerDetailsActivity.this,"please enable the loctions");
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }

        if (checkLocationPermission()) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                LocalBroadcastManager.getInstance(this).registerReceiver(message
                        , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST));

                Intent in = new Intent(this, Locations.class);
                startService(in);


            }
        }


        mTracker.setScreenName("Payment Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(PaymentActivity.this, message);
    }

    @Override
    public void onShowRationalDialog(final PermissionInterface permissionInterface, int requestCode) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("We need permissions for this app.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onDialogShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }

    @Override
    public void onShowSettings(final PermissionInterface permissionInterface, int requestCode) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("We need permissions for this app. Open setting screen?");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onSettingsShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        LocalBroadcastManager.getInstance(this).registerReceiver(message
                , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST));

        Intent in = new Intent(this, Locations.class);
        startService(in);
    }

    @Override
    public void onPermissionsDenied(int requestCode) {
        Intent callGPSSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(callGPSSettingIntent);
    }

//    @Override
//    public void updateOtp(String otp) {
//        this.otp = otp;
//        continuePayment();
//    }

    class PayUNetBankingAdapter extends BaseAdapter {
        Context mContext;
        ArrayList<PaymentDetails> mNetBankingList;

        public PayUNetBankingAdapter(Context context, ArrayList<PaymentDetails> netBankingList) {
            mContext = context;
            mContext = context;
            mNetBankingList = netBankingList;
        }

        @Override
        public int getCount() {
            return mNetBankingList.size();
        }

        @Override
        public Object getItem(int i) {
            if (null != mNetBankingList) return mNetBankingList.get(i);
            else return 0;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            NetbankingViewHolder netbankingViewHolder = null;
            if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(com.payu.payuui.R.layout.netbanking_list_item, null);
                netbankingViewHolder = new NetbankingViewHolder(convertView);
                convertView.setTag(netbankingViewHolder);
            } else {
                netbankingViewHolder = (NetbankingViewHolder) convertView.getTag();
            }

            PaymentDetails paymentDetails = mNetBankingList.get(position);
            convertView.setBackgroundColor(getResources().getColor(R.color.transparent));

            // set text here
            netbankingViewHolder.netbankingTextView.setText(paymentDetails.getBankName());
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    for(int i=0;i<((ViewGroup)parent).getChildCount();i++){
//                        View view = ((ViewGroup) parent).getChildAt(i);
//                        view.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    }
//                    v.setBackgroundColor(getResources().getColor(R.color.gray_dark));
//                    bankcode = netBankingList.get(position).getBankCode();
//                }
//            });
            return convertView;
        }


        class NetbankingViewHolder {
            TextView netbankingTextView;

            NetbankingViewHolder(View view) {
                netbankingTextView = (TextView) view.findViewById(com.payu.payuui.R.id.text_view_netbanking);
            }
        }
    }

    class GetHashesFromServerTask extends AsyncTask<HashMap<String, String>, String, PayuHashes> {

        @Override
        protected PayuHashes doInBackground(HashMap<String, String>... postParams) {
            PayuHashes payuHashes = new PayuHashes();
            try {
//                URL url = new URL(PayuConstants.MOBILE_TEST_FETCH_DATA_URL);
//                        URL url = new URL("http://10.100.81.49:80/merchant/postservice?form=2");;

//                URL url = new URL("https://payu.herokuapp.com/get_hash");
//                URL url = new URL("http://qc.factail.com/api/index.php?route=payu/payu/getHash");

                // get the payuConfig first
//                String postParam = "data="+postParams[0];


                String responseBuf = performPostCall("http://factail.com/api/index.php?route=payu/payu/getHash", getPostDataString(postParams[0]));
                JSONObject response = new JSONObject(responseBuf);

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        case "hash":
                            payuHashes.setPaymentHash(response.getString(key));
                            break;
                        case "merchantCodesHash": //
                            payuHashes.setMerchantIbiboCodesHash(response.getString(key));
                            break;
                        case "mobileSdk":
                            payuHashes.setVasForMobileSdkHash(response.getString(key));
                            break;
                        case "detailsForMobileSdk":
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(response.getString(key));
                            break;
                        case "delete_user_card_hash":
                            payuHashes.setDeleteCardHash(response.getString(key));
                            break;
                        case "get_user_cards_hash":
                            payuHashes.setStoredCardsHash(response.getString(key));
                            break;
                        case "edit_user_card_hash":
                            payuHashes.setEditCardHash(response.getString(key));
                            break;
                        case "save_user_card_hash":
                            payuHashes.setSaveCardHash(response.getString(key));
                            break;
                        case "check_offer_status_hash":
                            payuHashes.setCheckOfferStatusHash(response.getString(key));
                            break;
                        case "check_isDomestic_hash":
                            payuHashes.setCheckIsDomesticHash(response.getString(key));
                            break;
                        default:
                            break;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return payuHashes;
        }

        @Override
        protected void onPostExecute(PayuHashes payuHashes) {
            super.onPostExecute(payuHashes);
            mPaymentParams.setHash(payuHashes.getPaymentHash());
            mPayUHashes = payuHashes;

//            Log.e("Result", payuHashes.getPaymentHash());

            MerchantWebService merchantWebService = new MerchantWebService();
            merchantWebService.setKey(mPaymentParams.getKey());
            merchantWebService.setCommand(PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK);
            merchantWebService.setVar1(mPaymentParams.getUserCredentials() == null ? "default" : mPaymentParams.getUserCredentials());
            merchantWebService.setHash(mPayUHashes.getPaymentRelatedDetailsForMobileSdkHash());

            PostData postData = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                // ok we got the post params, let make an api call to payu to fetch the payment related details
                payuConfig.setData(postData.getResult());

                GetPaymentRelatedDetailsTask paymentRelatedDetailsForMobileSdkTask = new GetPaymentRelatedDetailsTask(PaymentActivity.this);
                paymentRelatedDetailsForMobileSdkTask.execute(payuConfig);
            } else {
                Toast.makeText(PaymentActivity.this, postData.getResult(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public class InputFilterMinMax implements InputFilter {
        private int min;
        private int max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            //noinspection EmptyCatchBlock
            try {
                int input = Integer.parseInt(dest.subSequence(0, dstart).toString() + source + dest.subSequence(dend, dest.length()));
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) {
            }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }

}
