package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.widget.LinearLayout;

import com.ebutor.fragments.ShipmentTrackingFragment;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ShipmentTrackingActivity extends BaseActivity {

    private LinearLayout llShipmentTracking;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llShipmentTracking = (LinearLayout) inflater.inflate(R.layout.empty_fragment_layout, null, false);
        flbody.addView(llShipmentTracking, baseLayoutParams);

        setContext(ShipmentTrackingActivity.this);
        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        if (savedInstanceState == null) {
            Fragment fragment = new ShipmentTrackingFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Shipment Tracking Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

}
