package com.ebutor.activities;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.adapters.POApprovalDetailsPagerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.fragments.POApprovalFragment;
import com.ebutor.models.ApprovalOptionsModel;
import com.ebutor.models.OrdersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srikanth Nama on 03-Mar-17.
 */

public class POApprovalDetailsActivity extends ParentActivity implements Response.ErrorListener, VolleyHandler<Object>, POApprovalFragment.OnClickListener {

    public MenuItem approvalItem;
    private String poId = "", status = "";
    private OrdersModel poModel;
    private Dialog dialog;
    private SharedPreferences mSharedPreferences;
    private ApprovalOptionsModel approvalOptionsModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_po_approve_details);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.po_approval));
        }

        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

        dialog = Utils.createLoader(this, ConstantValues.PROGRESS);

        if (getIntent().hasExtra("poId")) {
            poId = getIntent().getStringExtra("poId");
        }

        if (getIntent().hasExtra("status")) {
            status = getIntent().getStringExtra("status");
        }

        if (getIntent().hasExtra("POModel"))
            poModel = (OrdersModel) getIntent().getSerializableExtra("POModel");

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ArrayList<String> tabs = new ArrayList<>();

        tabs.add("Details");
        tabs.add("Approval History");

        POApprovalDetailsPagerAdapter viewPagerAdapter = new POApprovalDetailsPagerAdapter(getSupportFragmentManager(), tabs, poModel);
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_approve, menu);
        MenuItem item = menu.findItem(R.id.approve);
        item.setVisible(false);
        this.approvalItem = item;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.approve) {
            getApprovalOptionsList();
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getApprovalOptionsList() {
        if (Networking.isNetworkAvailable(POApprovalDetailsActivity.this)) {
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                detailsObj.put("user_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                detailsObj.put("uniqueId", poModel.getOrderNumber());
                detailsObj.put("approval_status", poModel.getApprovalStatusId());
                detailsObj.put("approval_module", "Purchase Order");

                map.put("data", detailsObj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getApprovalOptionsURL, map, POApprovalDetailsActivity.this, POApprovalDetailsActivity.this, PARSER_TYPE.PO_APPROVAL_OPTIONS);
            ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getApprovalOptionsURL);
            if (dialog != null)
                dialog.show();

        } else {
        }
    }

    private void showApproveDialog() {
        POApprovalFragment fragment = POApprovalFragment.newInstance(approvalOptionsModel, poModel);
        fragment.setListener(POApprovalDetailsActivity.this);
        fragment.show(getFragmentManager(), "PO Approve");
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.PO_APPROVAL_OPTIONS) {
                    if (response instanceof ApprovalOptionsModel) {
                        approvalOptionsModel = (ApprovalOptionsModel) response;
                        if (approvalOptionsModel != null)
                            showApproveDialog();

                    }
                }
            } else {
                Utils.showAlertWithMessage(POApprovalDetailsActivity.this, message);
            }
        } else {
            Utils.showAlertWithMessage(POApprovalDetailsActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(POApprovalDetailsActivity.this, message);
    }

    @Override
    public void onClicked() {

    }
}
