package com.ebutor.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeTransform;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.database.DBHelper;
import com.ebutor.fragments.PendingApprovalsFragment;
import com.ebutor.fragments.ReimbursementsFragment;
import com.ebutor.fragments.UnClaimedExpensesFragment;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Srikanth Nama on 29-Dec-16.
 */

public class ExpenseModuleMainActivityNew extends ParentActivity {
    FloatingActionMenu floatingActionMenu;
    private FloatingActionButton fab1;
    private FloatingActionButton fab2;
    private FloatingActionButton fab3;
    private TextView tvWalletAmount;
    private boolean hasChilds;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mContext = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setAllowReturnTransitionOverlap(true);
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setSharedElementExitTransition(new ChangeTransform());
        }

        setContentView(R.layout.activity_expense_module);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvWalletAmount = (TextView) toolbar.findViewById(R.id.wallet_balance);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);

        hasChilds = mSharedPreferences.getBoolean(ConstantValues.KEY_HAS_CHILD, false);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(getString(R.string.app_name));
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDefaultDisplayHomeAsUpEnabled(true);
        }

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EXPENSES_APPROVAL_FEATURE_CODE) > 0) {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        } else {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        }

        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.menu_yellow);
        fab1 = (FloatingActionButton) findViewById(R.id.fab12);
        fab2 = (FloatingActionButton) findViewById(R.id.fab22);
//        fab3 = (FloatingActionButton) findViewById(R.id.fab3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (floatingActionMenu.isOpened()) {
                    floatingActionMenu.close(false);
                }

                if ((DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EXPENSES_APPROVAL_FEATURE_CODE) > 0) &&
                        (position == 0 || position == 1)) {
                    floatingActionMenu.setVisibility(View.GONE);
                } else {
                    floatingActionMenu.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExpenseModuleMainActivityNew.this, SubmitExpenseActivity.class);
                intent.putExtra("Type", 1);
                startActivity(intent);
                if (floatingActionMenu.isOpened()) {
                    floatingActionMenu.close(false);
                }
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "Coming soon..!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ExpenseModuleMainActivityNew.this, SubmitExpenseActivity.class);
                intent.putExtra("Type", 2);
                startActivity(intent);
                if (floatingActionMenu.isOpened()) {
                    floatingActionMenu.close(true);
                }
            }
        });

    }

    public void updateWalletAmount(String amount) {
        if (amount == null)
            return;
        if (tvWalletAmount == null)
            return;

        double _amount = 0;
        try {
            _amount = Double.parseDouble(amount);
            if (_amount < 0) {
                _amount = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        tvWalletAmount.setText("Wallet Amount\n" + String.format("%.2f", _amount));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setupViewPager(ViewPager viewPager) {
        ArrayList<String> tabs = new ArrayList<>();
        boolean isApprovalFeature = false;
        if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EXPENSES_APPROVAL_FEATURE_CODE) > 0) {
            isApprovalFeature = true;
            tabs.add(getResources().getString(R.string.pending));
            tabs.add(getResources().getString(R.string.approved));
        } else {
            isApprovalFeature = false;
        }
//
        tabs.add(getResources().getString(R.string.claimed));
        tabs.add(getResources().getString(R.string.unclaimed));
//        if (hasChilds)
//            tabs.add(getResources().getString(R.string.all));

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(ExpenseModuleMainActivityNew.this, getSupportFragmentManager(), tabs, isApprovalFeature);
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<String> mTabs;
        boolean isFeatureAllowed;

        public ViewPagerAdapter(Context context, FragmentManager fm, ArrayList<String> tabs, boolean isFeatureAllowed) {
            super(fm);
            this.mTabs = tabs;
            this.isFeatureAllowed = isFeatureAllowed;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (isFeatureAllowed) {
                floatingActionMenu.setVisibility(View.GONE);
                switch (position) {
            /*case 0:
                fragment = new AdvancesFragment();
                break;*/
                    case 0:
                        fragment = PendingApprovalsFragment.newInstance(position);
                        break;
                    case 1:
                        fragment = PendingApprovalsFragment.newInstance(position);
                        break;
                    case 2:
                        fragment = new ReimbursementsFragment();
                        break;
                    case 3:
                        fragment = new UnClaimedExpensesFragment();
                        break;
                    /*case 4:
                        fragment = new GetAllExpensesFragment();
                        break;*/
                    default:
                        fragment = new ReimbursementsFragment();
                        break;
                }

                return fragment;
            } /*else if (hasChilds) {
                floatingActionMenu.setVisibility(View.VISIBLE);
                switch (position) {
            *//*case 0:
                fragment = new AdvancesFragment();
                break;*//*
                    case 0:
                        fragment = new ReimbursementsFragment();
                        break;
                    case 1:
                        fragment = new UnClaimedExpensesFragment();
                        break;
                    case 2:
                        fragment = new GetAllExpensesFragment();
                        break;
                    default:
                        fragment = new ReimbursementsFragment();
                        break;
                }

                return fragment;
            }*/ else {
                floatingActionMenu.setVisibility(View.VISIBLE);
                switch (position) {
            /*case 0:
                fragment = new AdvancesFragment();
                break;*/
                    case 0:
                        fragment = new ReimbursementsFragment();
                        break;
                    case 1:
                        fragment = new UnClaimedExpensesFragment();
                        break;
                    default:
                        fragment = new ReimbursementsFragment();
                        break;
                }

                return fragment;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabs.get(position);
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

    }

}
