package com.ebutor.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.fragments.SubmitExpenseFragment;
import com.ebutor.fragments.SubmitLineItemsFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Srikanth Nama on 27-Dec-16.
 */

public class SubmitExpenseActivity extends ParentActivity {

    public MenuItem delItem, uplItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_retailer);

//        GcmNetworkManager gcmNetworkManager = GcmNetworkManager.getInstance(this);
//        Task task = new PeriodicTask.Builder()
//                .setService(UploadOfflineExpensesService.class)
//                .setPeriod(60 * 60)//setting default background service for 60 minutes
//                .setFlex(5 * 60) // executes within 55 - 60 minutes
//                .setTag("TAG_TASK_UPLOAD_EXPENSES")
//                .setPersisted(true)
//                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
//                .setRequiresCharging(false)
//                .build();
//
//        gcmNetworkManager.schedule(task);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Add Expense");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        int _submitType = getIntent().getExtras().getInt("Type", 1);
        Fragment fragment = new SubmitExpenseFragment();

        // _submitType == 2 for new Main Entry, 3 for new main entry with line items
        if (_submitType == 2 || _submitType == 3) {
            fragment = new SubmitLineItemsFragment();
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action_mode, menu);
        MenuItem delItem = menu.findItem(R.id.delete);
        delItem.setVisible(false);
        this.delItem = delItem;
        MenuItem uplItem = menu.findItem(R.id.upload);
        uplItem.setVisible(false);
        this.uplItem = uplItem;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
