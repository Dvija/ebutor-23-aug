package com.ebutor.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.adapters.HighMarginAdapter;
import com.ebutor.models.BannerModel;
import com.ebutor.utils.ConstantValues;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 28-Feb-17.
 */

public class AttachmentsActivity extends ParentActivity {

    CirclePageIndicator indicator;
    private ViewPager autoScrollPager;
    private boolean isLocalPath = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_images_pager);


        autoScrollPager = (ViewPager) findViewById(R.id.circle_pager);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        if (getIntent().hasExtra("isLocalPath"))
            isLocalPath = getIntent().getBooleanExtra("isLocalPath", false);


        String attachments = getIntent().getStringExtra("Attachments");
        ArrayList<BannerModel> arrBanner = new ArrayList<>();
        if (attachments != null) {
            String[] multiple = attachments.split(",");

            for (String str : multiple) {
                BannerModel bannerModel = new BannerModel();
                if (str.startsWith("http://") || str.startsWith("https://") || str.contains("/storage")) {
                    bannerModel.setImage(str);
                } else {
                    bannerModel.setImage(ConstantValues.S3_BUCKET_URL + str);
                }
                arrBanner.add(bannerModel);
            }
        }

        if (arrBanner.size() > 0) {
            HighMarginAdapter highMarginAdapter = new HighMarginAdapter(AttachmentsActivity.this, arrBanner,isLocalPath);
            autoScrollPager.setAdapter(highMarginAdapter);
            indicator.setViewPager(autoScrollPager);
        }
    }
}
