package com.ebutor.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.PARSER_TYPE;

import java.util.HashMap;

/**
 * Created by Srikanth Nama on 02-Mar-17.
 */

public class SampleActivity extends ParentActivity implements ResultHandler, Response.ErrorListener, VolleyHandler<Object> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try {

            HashMap<String, String> map = new HashMap<>();
            map.put("data", "VIN='2A4RR5DG4BR658103'&CID=7005707&PWD=1O98HBeL&SID=7005708");

            VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, "https://www.autocheck.com/DealerWebLink.jsp", map, SampleActivity.this, SampleActivity.this, PARSER_TYPE.DEFAULT);
            getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(getStateRequest, "https://www.autocheck.com/DealerWebLink.jsp");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null)
            Log.e("result", results.toString());
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {
        Log.e("error", errorCode);
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (response != null)
            Log.e("result", response.toString());
    }

    @Override
    public void onSessionError(String message) {

    }
}
