package com.ebutor;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.fragments.HighMarginFragment;
import com.ebutor.fragments.HomeFragment;
import com.ebutor.models.ProductModel;
import com.ebutor.models.SKUModel;
import com.ebutor.models.SKUPacksModel;
import com.ebutor.models.TabData;
import com.ebutor.models.TabModel;
import com.ebutor.tabspager.SlidingTabLayout;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by 300024 on 3/9/2016.
 */
public class HomeActivity extends BaseActivity implements ResultHandler, Response.ErrorListener, VolleyHandler<Object> {

    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    public SharedPreferences mSharedPreferences;
    private List<SamplePagerItem> mTabs = new ArrayList<SamplePagerItem>();
    private ViewPager mViewPager;
    private ArrayList<TabModel> arrTabs;
    //    private ArrayList<Integer> subCatCount = new ArrayList<Integer>();
    private int cartCount = 0;
    private int segmentPosition;
    private GridLayoutManager manager;
    private long mBackPressed;
    private RelativeLayout rlAlert;
    private RelativeLayout llHome;
    private TextView tvAlertMsg;
    private LinearLayout llMain;
    private Dialog dialog;
    private String pincode = "";
    private boolean isFF = false, isCheckIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arrTabs = new ArrayList<>();

        llHome = (RelativeLayout) inflater.inflate(R.layout.activity_home, null, false);
        flbody.addView(llHome, baseLayoutParams);

        setContext(HomeActivity.this);

        dialog = Utils.createLoader(HomeActivity.this, ConstantValues.TOOLBAR_PROGRESS);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        findViewById(R.id.iv_icon).setVisibility(View.VISIBLE);

        View toolbarLogo = findViewById(R.id.iv_toolbar_logo);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        llMain = (LinearLayout) findViewById(R.id.ll_main);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        isCheckIn = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_CHECKIN, false);

        if (toolbarLogo != null) {
            toolbarLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(HomeActivity.this)) {
                    getSortedTabsViewCart();
                } else {
                    Toast.makeText(HomeActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        getSortedTabsViewCart();

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(1);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        //set up the adapter for the expandablelistview to display the categories.

//        mCategoryList.setAdapter(new NavigationExpandableAdapter(HomeActivity.this,  subcategory_name));

        // enable ActionBar application icon to be used to open or close the drawer layout

        //defining the behavior when any group is clicked in expandable listview
    }

    private void getSortedTabsViewCart() {
        if (Networking.isNetworkAvailable(HomeActivity.this)) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                String customerToken = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "");

                jsonObject.put("customer_token", customerToken);
                jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                jsonObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                jsonObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                map.put("data", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask tabsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.tabsURL, map, HomeActivity.this, HomeActivity.this, PARSER_TYPE.SORTED_TABS);
            tabsRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(tabsRequest, AppURL.tabsURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    public void updateCart(int cartCount) {
        this.cartCount = cartCount;
        invalidate(cartCount);
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null && results instanceof String) {
            String response = (String) results;
            if (!TextUtils.isEmpty(response)) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equalsIgnoreCase("success")) {

                        if (requestType == PARSER_TYPE.VIEW_CART) {

                            JSONArray dataArray = jsonObject.optJSONArray("data");
                            if (dataArray != null && dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {

                                    JSONObject productObj = dataArray.optJSONObject(i);
                                    ProductModel model = new ProductModel();

                                    int productTotalQty = 0;
                                    double productTotalPrice = 0;

//                                    model.setPosition(i);
                                    model.setProductName(productObj.optString("name"));
                                    String productId = productObj.optString("product_id");
                                    model.setProductId(productId);
                                    String cartId = productObj.optString("cartId");
                                    model.setCartId(cartId);
                                    model.setCartAddedDate(productObj.optString("date_added"));
                                    model.setProductRating(productObj.optString("rating"));
                                    model.setProductDescription(productObj.optString("description"));

                                    JSONArray variantsArray = productObj.optJSONArray("variants");
                                    ArrayList<SKUModel> skuModelArrayList = new ArrayList<>();
                                    if (variantsArray != null && variantsArray.length() > 0) {
                                        for (int j = 0; j < variantsArray.length(); j++) {
                                            JSONObject variantObj = variantsArray.optJSONObject(j);
                                            SKUModel skuModel = new SKUModel();
                                            skuModel.setPosition(i);
                                            skuModel.setSkuName(variantObj.optString("name"));
                                            skuModel.setSkuId(variantObj.optString("variant_id"));
                                            skuModel.setProductVariantId(variantObj.optString("product_variant_id"));
                                            skuModel.setSkuImage(variantObj.optString("image"));
                                            skuModel.setSku(variantObj.optString("sku"));
                                            skuModel.setDefault(variantObj.optString("is_default").equals("1"));
                                            skuModel.setModel(variantObj.optString("model"));
                                            skuModel.setSelected(false);
                                            String appliedMrp = variantObj.optString("applied_mrp");
                                            String appliedMargin = variantObj.optString("applied_margin");
                                            skuModel.setAppliedMargin(appliedMargin);
                                            skuModel.setAppliedMRP(appliedMrp);
                                            String qty = variantObj.optString("Total_quantity");
                                            skuModel.setTotalQuantity(qty);
                                            try {
                                                productTotalQty = productTotalQty + Integer.parseInt(qty);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            String totalPrice = variantObj.optString("Total_Price");
                                            skuModel.setTotalPrice(totalPrice);
                                            try {
                                                productTotalPrice = productTotalPrice + Double.parseDouble(totalPrice);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            JSONArray packsArray = variantObj.optJSONArray("pack");
                                            ArrayList<SKUPacksModel> skuPacksModelArrayList = new ArrayList<>();
                                            if (packsArray != null && packsArray.length() > 0) {
                                                for (int k = 0; k < packsArray.length(); k++) {
                                                    JSONObject packObj = packsArray.optJSONObject(k);
                                                    SKUPacksModel packsModel = new SKUPacksModel();

                                                    packsModel.setPackSize(packObj.optString("pack_size"));
                                                    packsModel.setPackPrice(packObj.optString("dealer_price"));
                                                    packsModel.setPackPrice(packObj.optString("dealer_price"));
                                                    packsModel.setUnitPrice(packObj.optString("unit_price"));
                                                    packsModel.setMargin(packObj.optString("margin"));
                                                    packsModel.setVariantPriceId(packObj.optString("pack_price_id"));
                                                    // value selected by user
                                                    try {
                                                        packsModel.setQty(Integer.parseInt(packObj.optString("qty")));
                                                        skuPacksModelArrayList.add(packsModel);
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                            skuModel.setSkuPacksModelArrayList(skuPacksModelArrayList);
                                            // insert cart variants in DB
//                                            dbHelper.insertCartVariant(skuModel, cartId, productId, mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                            skuModelArrayList.add(skuModel);
                                        }
                                    }
                                    model.setTotalItems(productTotalQty);
                                    model.setTotalPrice(productTotalPrice);

                                    model.setSkuModelArrayList(skuModelArrayList);
                                    //insert cart product in DB
//                                    dbHelper.insertProductIntoCart(model, mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                }

                            }
//                            exportDatabase("factail_database");
                            invalidateOptionsMenu();

                        } else if (requestType == PARSER_TYPE.SORTED_TABS) {
                            JSONArray tabsArray = jsonObject.getJSONArray("data");
                            if (tabsArray != null && tabsArray.length() > 0) {
                                for (int i = 0; i < tabsArray.length(); i++) {
                                    JSONObject tabObj = tabsArray.getJSONObject(i);
                                    String tabId = tabObj.getString("id");
                                    String tabName = tabObj.getString("name");
                                    String tabValue = tabObj.getString("value");
                                    if (!tabName.equalsIgnoreCase("PRICE DROP")) {
                                        TabModel model = new TabModel();
                                        model.setTabId(tabId);
                                        model.setTabName(tabName);
                                        model.setTabValue(tabValue);
                                        arrTabs.add(model);
                                    }
                                }
                                if (arrTabs != null && arrTabs.size() > 0) {
                                    for (int i = 0; i < arrTabs.size(); i++) {
                                        mTabs.add(new SamplePagerItem(
                                                arrTabs.get(i).getTabName().toUpperCase(), // Title
                                                this.getResources().getColor(R.color.primary), // Indicator color
                                                Color.GRAY // Divider color
                                        ));
                                    }
                                }
                                mViewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
                                SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
                                mSlidingTabLayout.setCustomTabView(R.layout.tab_layout, R.id.tab_heading);
                                mSlidingTabLayout.setDistributeEvenly(true);
                                mSlidingTabLayout.setViewPager(mViewPager);
                                mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

                                    @Override
                                    public int getIndicatorColor(int position) {
                                        return mTabs.get(position).getIndicatorColor();
                                    }

                                });
//                                mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                                    @Override
//                                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                                    }
//
//                                    @Override
//                                    public void onPageSelected(int position) {
//                                        switch (position) {
//                                            case 1:
//                                                Fragment page1 = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
//                                                if (mViewPager.getCurrentItem() == 1 && page1 != null) {
//                                                    ((HighMarginFragment) page1).getData();
//                                                }
//                                                break;
//                                            case 2:
//                                                Fragment page2 = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
//                                                if (mViewPager.getCurrentItem() == 2 && page2 != null) {
//                                                    ((FastMovingNewFragment) page2).getData();
//                                                }
//                                                break;
//                                            case 3:
//                                                Fragment page3 = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
//                                                if (mViewPager.getCurrentItem() == 3 && page3 != null) {
//                                                    ((TopRatedFragment) page3).getData();
//                                                }
//                                                break;
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onPageScrollStateChanged(int state) {
//
//                                    }
//                                });
                            }
                        }

                    } else {
                        Utils.showAlertDialog(HomeActivity.this, message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {
        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Toast.makeText(HomeActivity.this, getResources().getString(R.string.press_once_again_exit), Toast.LENGTH_SHORT).show();
            }

            mBackPressed = System.currentTimeMillis();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if (error != null)
//            Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.SORTED_TABS) {
                    if (results instanceof TabData) {
                        TabData tabData = (TabData) results;
                        if (tabData != null) {
                            arrTabs = tabData.getArrTabs();
                            MyApplication.getInstance().setArrTabs(arrTabs);
                            if (arrTabs != null && arrTabs.size() > 0) {
                                if (arrTabs != null && arrTabs.size() > 0) {
                                    for (int i = 0; i < arrTabs.size(); i++) {
                                        mTabs.add(new SamplePagerItem(
                                                arrTabs.get(i).getTabName().toUpperCase(), // Title
                                                this.getResources().getColor(R.color.primary), // Indicator color
                                                Color.GRAY // Divider color
                                        ));
                                    }
                                }
                                mViewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
                                SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
                                mSlidingTabLayout.setCustomTabView(R.layout.tab_layout, R.id.tab_heading);
                                mSlidingTabLayout.setDistributeEvenly(true);
                                mSlidingTabLayout.setViewPager(mViewPager);
                                mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

                                    @Override
                                    public int getIndicatorColor(int position) {
                                        return mTabs.get(position).getIndicatorColor();
                                    }

                                });
                            }

                            if (tabData.isEcash()) {
                                double eCash = tabData.getEcash();
                                double creditLimit = tabData.getCreditLimit();
                                double paymentDue = tabData.getPaymentDue();

                                if (isFF && !isCheckIn) {
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_CREDIT_LIMIT, (float) creditLimit).apply();
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_ECASH, (float) eCash).apply();
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_PAYMENT_DUE, (float) paymentDue).apply();

                                } else {
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_CREDIT_LIMIT, (float) creditLimit).apply();
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_ECASH, (float) eCash).apply();
                                    mSharedPreferences.edit().putFloat(ConstantValues.KEY_PAYMENT_DUE, (float) paymentDue).apply();

                                }

                                tvEcash.setText(getString(R.string.ecash) + " : " + String.format(Locale.CANADA, "%.2f", eCash));
                                tvPaymentDue.setText(getString(R.string.due) + " : " + String.format(Locale.CANADA, "%.2f", paymentDue));
                            }
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(HomeActivity.this, message);
            }
        } else {
            Utils.showAlertWithMessage(HomeActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(HomeActivity.this, message);
    }

    static class SamplePagerItem {
        private final CharSequence mTitle;
        private final int mIndicatorColor;
        private final int mDividerColor;

        SamplePagerItem(CharSequence title, int indicatorColor, int dividerColor) {
            mTitle = title;
            mIndicatorColor = indicatorColor;
            mDividerColor = dividerColor;
        }

        CharSequence getTitle() {
            return mTitle;
        }

        int getIndicatorColor() {
            return mIndicatorColor;
        }

        int getDividerColor() {
            return mDividerColor;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            String tabType = "";
            for (int i = 0; i < arrTabs.size(); i++) {
                String tabName = arrTabs.get(i).getTabName();
                if (!TextUtils.isEmpty(tabName)) {
                    if (position == i) {
                        tabType = tabName;
                    }
                }
            }

            switch (position) {
                case 0:
                    fragment = new HomeFragment();
                    break;
                case 1:
                    fragment = HighMarginFragment.newInstance(tabType);
                    break;
//                case 2:
//                    fragment = new FastMovingFragment();
//                    Bundle bundle1 = new Bundle();
//                    bundle1.putString("Type", tabType);
//                    fragment.setArguments(bundle1);
//                    break;
//                case 3:
//                    fragment = new FastMovingFragment();
//                    Bundle bundle2 = new Bundle();
//                    bundle2.putString("Type", tabType);
//                    fragment.setArguments(bundle2);
//                    break;
                default:
                    fragment = HighMarginFragment.newInstance(tabType);
                    break;
            }
            //args.putSerializable("InspectResponse", inspectModel);
//            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabs.get(position).getTitle();
        }
    }

}