package com.ebutor;


import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.ebutor.database.DBHelper;
import com.ebutor.utils.ConstantValues;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ParentActivity extends AppCompatActivity {

    private SharedPreferences mSharedPreferences;

    @Override
    protected void onResume() {
        super.onResume();

        // refresh products data for first time in a day
        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String currentDate = df.format(c.getTime());
        Date pastDate = null, currDate = null;
        try {
            currDate = df.parse(df.format(c.getTime()));
            pastDate = df.parse(mSharedPreferences.getString(ConstantValues.KEY_LAST_UPDATED_DATE, currentDate));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((int) ((currDate.getTime() - pastDate.getTime()) / (1000 * 60 * 60 * 24)) > 0) {
            DBHelper dbHelper = DBHelper.getInstance();
            dbHelper.deleteTable(DBHelper.TABLE_PRODUCTS);
            dbHelper.deleteTable(DBHelper.TABLE_CART);
            MyApplication.getInstance().setDayChanged(true);

        }
        mSharedPreferences.edit().putString(ConstantValues.KEY_LAST_UPDATED_DATE, currentDate).apply();

    }
}
