package com.ebutor;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;


/**
 * Created by 300024 on 6/1/2015.
 */
public class ToolTipActivity extends Activity implements View.OnClickListener {

    private RelativeLayout rlMain;
    private View view;
    private int i = 0;
    private ImageView ivNext, ivSkip;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tool_tip);
        rlMain = (RelativeLayout) findViewById(R.id.rl_main);

        view = getLayoutInflater().inflate(R.layout.navigation_drawer_tooltip, null);
        rlMain.removeAllViews();
        rlMain.addView(view);
        i = 1;
        ivNext = (ImageView) view.findViewById(R.id.iv_next);
        tv = (TextView) view.findViewById(R.id.tv_text);
        String str = getString(R.string.categories);
        final SpannableStringBuilder sb = new SpannableStringBuilder(str);
        setHighlightText(sb, tv, str.length()-10, str.length());
        ivSkip = (ImageView) view.findViewById(R.id.iv_skip);
        ivSkip.setOnClickListener(this);

        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv.getText().toString().equalsIgnoreCase(getResources().getString(R.string.categories))) {
                    rlMain.removeAllViews();
                    view = getLayoutInflater().inflate(R.layout.search_tooltip, null);
                    rlMain.addView(view);
                    ivNext = (ImageView) view.findViewById(R.id.iv_next);
                    tv = (TextView) view.findViewById(R.id.tv_text);
                    String str = getString(R.string.search_text);
                    final SpannableStringBuilder sb = new SpannableStringBuilder(str);
                    setHighlightText(sb, tv, 12, 19);
                    ivSkip = (ImageView) view.findViewById(R.id.iv_skip);
                    ivNext.setOnClickListener(this);
                    ivSkip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });
                } else if (tv.getText().toString().equalsIgnoreCase(getResources().getString(R.string.search_text))) {
                    rlMain.removeAllViews();
                    view = getLayoutInflater().inflate(R.layout.notification_tooltip, null);
                    rlMain.addView(view);
                    ivNext = (ImageView) view.findViewById(R.id.iv_next);
                    ivSkip = (ImageView) view.findViewById(R.id.iv_skip);
                    tv = (TextView) view.findViewById(R.id.tv_text);
                    String str = getString(R.string.notifications_text);
                    final SpannableStringBuilder sb = new SpannableStringBuilder(str);
                    setHighlightText(sb, tv, str.length()-13, str.length());
                    ivNext.setOnClickListener(this);
                    ivSkip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });

                } else if (tv.getText().toString().equalsIgnoreCase(getResources().getString(R.string.notifications_text))) {
                    rlMain.removeAllViews();
                    view = getLayoutInflater().inflate(R.layout.account_tooltip, null);
                    rlMain.addView(view);
                    tv = (TextView) view.findViewById(R.id.tv_text);
                    String str = getString(R.string.my_account_text);
                    final SpannableStringBuilder sb = new SpannableStringBuilder(str);
                    setHighlightText(sb, tv, 12, 32);
                    ivNext = (ImageView) view.findViewById(R.id.iv_next);
                    ivNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });

                    /*rlMain.removeAllViews();
                    view = getLayoutInflater().inflate(R.layout.cart_tooltip, null);
                    rlMain.addView(view);
                    ivNext = (ImageView) view.findViewById(R.id.iv_next);
                    tv = (TextView) view.findViewById(R.id.tv_text);
                    ivSkip = (ImageView) view.findViewById(R.id.iv_skip);
                    ivNext.setOnClickListener(this);
                    ivSkip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });*/
                } else if (tv.getText().toString().equalsIgnoreCase(getResources().getString(R.string.cart))) {


                }
            }
        });

    }
    private void setHighlightText(SpannableStringBuilder sb, TextView textView, int start, int end) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(211, 84, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start,end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_skip:
                finish();
                break;
        }
    }
}
