package com.ebutor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebutor.adapters.HelpScreensAdapter;
import com.ebutor.backgroundtask.HTTPBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CategoryModel;
import com.ebutor.utils.APIREQUEST_TYPE;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by 300024 on 3/8/2016.
 */
public class HelpScreensActivity extends ParentActivity implements ResultHandler {

    public static Activity instnace;
    DBHelper databaseObj;
    SharedPreferences mSharedPreferences;
    HelpScreensAdapter helpScreensAdapter;
    Button btSkip;
//    int[] images = new int[]{R.drawable.splash1,R.drawable.splash2,R.drawable.splash3,R.drawable.splash4,R.drawable.splash5,R.drawable.splash6};
    int[] images = new int[]{};
//    int[] helpLayouts = new int[]{R.layout.fragment_help_one, R.layout.fragment_help_two, R.layout.fragment_help_three, R.layout.fragment_help_four, R.layout.fragment_help_five, R.layout.fragment_help_six};
    private ViewPager pager;
    private CirclePageIndicator circlePageIndicator;
    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;
    private String appId = "";
    private RelativeLayout rlAlert;
    private TextView tvAlertMsg;
    private Tracker mTracker;
    private LinearLayout llMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_screens);
        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        pager = (ViewPager) findViewById(R.id.pager);
        btSkip = (Button) findViewById(R.id.bt_skip);
        circlePageIndicator = (CirclePageIndicator) findViewById(R.id.circlePageIndicator);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        llMain = (LinearLayout) findViewById(R.id.ll_main);

        appId = mSharedPreferences.getString(ConstantValues.KEY_APP_ID, "");
        databaseObj = new DBHelper(HelpScreensActivity.this);

        instnace = this;

        helpScreensAdapter = new HelpScreensAdapter(HelpScreensActivity.this, images, btSkip);

        pager.setAdapter(helpScreensAdapter);

        pager.setCurrentItem(0);
        circlePageIndicator.setViewPager(pager);
        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == images.length - 1) {
                    btSkip.setText(getResources().getString(R.string.next));
                } else {
                    btSkip.setText(getResources().getString(R.string.skip));
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_FIRST_TIME_USER, false).apply();
                if (TextUtils.isEmpty(mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""))) {
                    startActivity(new Intent(HelpScreensActivity.this,PincodeAvailabiltyActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(HelpScreensActivity.this, SkipSignupActivity.class));
                    finish();
                }
            }
        });

//        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(Networking.isNetworkAvailable(HelpScreensActivity.this)){
//                    getCategories();
//                }else{
//                    Toast.makeText(HelpScreensActivity.this, ConstantValues.ALERT_NO_NETWORK, Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

//        getCategories();

    }

//    private void getCategories() {
//        if (databaseObj.getAllCategories().size() == 0) {
//            if (Networking.isNetworkAvailable(HelpScreensActivity.this)) {
//                rlAlert.setVisibility(View.GONE);
//                llMain.setVisibility(View.VISIBLE);
//                HashMap<String, String> map = new HashMap<>();
//                try {
//                    JSONObject object = new JSONObject();
//                    if (appId != null && appId.length() > 0) {
//                        object.put("appId", appId);
//                    }
//                    map.put("data", object.toString());
//                } catch (Exception e) {
//
//                }
//
//                HTTPBackgroundTask task = new HTTPBackgroundTask(HelpScreensActivity.this, HelpScreensActivity.this, PARSER_TYPE.GET_CATEGORIES, APIREQUEST_TYPE.HTTP_POST, false, ConstantValues.HELP_PROGRESS);
//                task.execute(map);
//            }
//            else{
//                rlAlert.setVisibility(View.VISIBLE);
//                llMain.setVisibility(View.GONE);
//            }
//        }
//    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(HelpScreensActivity.this, getResources().getString(R.string.press_once_again_exit), Toast.LENGTH_SHORT).show();
        }

        mBackPressed = System.currentTimeMillis();
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (requestType == PARSER_TYPE.GET_CATEGORIES) {
            String response = (String) results;
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        if (dataArray != null && dataArray.length() > 0) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject categoryObj = dataArray.getJSONObject(i);
                                String categoryId = categoryObj.optString("category_id");
                                String segmentId = categoryObj.optString("segment_id");
                                String parentId = categoryObj.optString("parent_id");
                                String categoryImage = categoryObj.optString("image");
                                String categoryName = categoryObj.optString("name");
                                String productNo = categoryObj.optString("productNo");

                                CategoryModel model = new CategoryModel();
                                model.setCategoryId(categoryId);
                                model.setCategoryImage(categoryImage);
                                model.setSegmentId(segmentId);
                                model.setParentId(parentId);
                                model.setCategoryName(categoryName);
                                model.setProductNo(productNo);

                                databaseObj.insertCategory(model);
                            }
                        }
                    }else{
                        Utils.showAlertDialog(HelpScreensActivity.this, message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {

    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName(" Help screens Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }
}
