package com.ebutor.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ebutor.MainActivityNew;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.BrandProductsAdapter;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class BrandProductFragment extends Fragment implements BrandProductsAdapter.OnItemClickListener {

    private SharedPreferences mSharedPreferences;
    private String manufacturerId, manufacturerName, flag, key;
    private Tracker mTracker;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        manufacturerId = getArguments().getString("ManufacturerId");
        manufacturerName = getArguments().getString("ManufacturerName");
        flag = getArguments().getString("Flag");
        key = getArguments().getString("key_id");
        View convertView = inflater.inflate(R.layout.fragment_brand_product, null);
        return convertView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView gvBrandProducts = (RecyclerView) view.findViewById(R.id.gvBrandProducts);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);

        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        Intent intent = new Intent(getActivity(), MainActivityNew.class);
        intent.putExtra("Id", manufacturerId);
        intent.putExtra("Name", manufacturerName);
        intent.putExtra("Type", "Products");
        intent.putExtra("key_id", key);
        getActivity().finish();
        startActivity(intent);
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(getActivity(), position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("Brands Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }
}
