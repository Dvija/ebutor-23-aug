package com.ebutor.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ApprovalDataModel;
import com.ebutor.models.ApprovalOptionsModel;
import com.ebutor.models.OrdersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;

public class POApprovalFragment extends DialogFragment implements Response.ErrorListener, VolleyHandler<Object> {

    private SharedPreferences mSharedPreferences;
    private ApprovalOptionsModel approvalOptionsModel;
    private Spinner spStatus;
    private EditText etComments;
    private String status = "", comments;
    private Button btnSubmit;
    private Dialog dialog;
    private OrdersModel poModel;
    private OnClickListener onClickListener;

    public static POApprovalFragment newInstance(ApprovalOptionsModel approvalOptionsModel, OrdersModel poModel) {
        POApprovalFragment fragment = new POApprovalFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("ApprovalOptionsModel", approvalOptionsModel);
        bundle.putSerializable("POModel", poModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("PO Approve");
        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);
        if (getArguments().containsKey("ApprovalOptionsModel"))
            approvalOptionsModel = (ApprovalOptionsModel) getArguments().getSerializable("ApprovalOptionsModel");
        if (getArguments().containsKey("POModel"))
            poModel = (OrdersModel) getArguments().getSerializable("POModel");
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        return inflater.inflate(R.layout.popup_po_approval, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spStatus = (Spinner) view.findViewById(R.id.spinner_status);
        etComments = (EditText) view.findViewById(R.id.et_comments);
        btnSubmit = (Button) view.findViewById(R.id.btn_submit);

        if (approvalOptionsModel != null && approvalOptionsModel.getArrApprovalOptions() != null && approvalOptionsModel.getArrApprovalOptions().size() > 0) {
            ArrayAdapter<ApprovalDataModel> arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, approvalOptionsModel.getArrApprovalOptions());
            spStatus.setAdapter(arrayAdapter);
        }

        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    status = "";
                else
                    status = approvalOptionsModel.getArrApprovalOptions().get(i).getNextStatusId() + "," + approvalOptionsModel.getArrApprovalOptions().get(i).getIsFinalStep();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comments = etComments.getText().toString().trim();
                if (TextUtils.isEmpty(status)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_status));
                    return;
                }
//                if (TextUtils.isEmpty(comments)) {
//                    etComments.setError(getString(R.string.please_enter_comments));
//                    return;
//                }
                submitApproveStatus();
            }
        });
    }

    private void submitApproveStatus() {
        if (Networking.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                detailsObj.put("user_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                detailsObj.put("approval_unique_id", poModel.getOrderNumber());
                detailsObj.put("approval_status", status);
                detailsObj.put("approval_comment", comments);
                detailsObj.put("approval_module", "Purchase Order");
                detailsObj.put("table_name", approvalOptionsModel.getTableName());
                detailsObj.put("unique_column", approvalOptionsModel.getUniqueColumn());
                detailsObj.put("current_status", approvalOptionsModel.getCurrentStatus());

                map.put("data", detailsObj.toString());
            } catch (Exception e) {

            }

            VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.submitapprovestatusURL, map, POApprovalFragment.this, POApprovalFragment.this, PARSER_TYPE.SUBMIT_APPROVAL);
            ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.submitapprovestatusURL);
            if (dialog != null)
                dialog.show();

        } else {
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {

                if (requestType == PARSER_TYPE.SUBMIT_APPROVAL) {
                    if (response instanceof String) {
                        showAlertWithMessage("Approved Successfully");
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    public void showAlertWithMessage(String string) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(string);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dismiss();
                getActivity().finish();
//                if (onClickListener != null) {
//                    onClickListener.onClicked();
//                }
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    public interface OnClickListener {
        void onClicked();
    }

}
