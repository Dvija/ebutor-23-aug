package com.ebutor.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.ebutor.R;
import com.ebutor.adapters.AddressesListRecyclerAdapter;
import com.ebutor.models.UserAddressModel;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 2/29/2016.
 */
public class AddressesFragment extends Fragment implements AddressesListRecyclerAdapter.OnItemClickListener {

    private RecyclerView mRecyclerView;
    private ArrayList<UserAddressModel> addressModelArrayList;
    private TextView tvAddNewAddress;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_addresses, container, false);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.scrollable);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvAddNewAddress = (TextView)view.findViewById(R.id.tv_add_address);

        addressModelArrayList = (ArrayList<UserAddressModel>) getArguments().getSerializable("Addresses");
        if (addressModelArrayList != null) {
            AddressesListRecyclerAdapter adapter = new AddressesListRecyclerAdapter(getActivity(), addressModelArrayList);
            adapter.setOnItemClickListener(AddressesFragment.this);
            mRecyclerView.setAdapter(adapter);
        }

        tvAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new AddNewAddressFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.container, fragment, "Add");
                transaction.addToBackStack("ChangePassword");
                transaction.commit();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(Object object) {
        if (object != null && object instanceof UserAddressModel) {
            UserAddressModel model = (UserAddressModel) object;

            Intent intent = getActivity().getIntent();
            intent.putExtra("SelectedAddress", model);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();

        }
    }

    @Override
    public void onEditClick(Object object, int position) {
        if (object != null && object instanceof UserAddressModel) {
            UserAddressModel model = (UserAddressModel) object;

            Fragment fragment = new AddNewAddressFragment();
            Bundle args = new Bundle();
            args.putSerializable("SelectedAddress",model);
            args.putString("position",position+"");
            fragment.setArguments(args);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.container, fragment, "Edit");
            transaction.addToBackStack("EditAddress");
            transaction.commit();
        }
    }
}
