package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.backgroundtask.JSONParser;
import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;

public class StatusFragment extends DialogFragment {

    private OnSelListener confirmListener;
    private ListView lvSort;
    private ItemsAdapter mAdapter;
    private ArrayList<CustomerTyepModel> arrStatus;
    private String selId = "";
    private Button btnClear, btnApply;
    private LinearLayout llFooter;

    public static StatusFragment newInstance(ArrayList<CustomerTyepModel> arrStatus) {
        StatusFragment frag = new StatusFragment();
        Bundle args = new Bundle();
        args.putSerializable("Status", arrStatus);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments().containsKey("Status")) {
            arrStatus = (ArrayList<CustomerTyepModel>) getArguments().getSerializable("Status");
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        getDialog().getWindow().setAttributes(lp);

        return inflater.inflate(R.layout.fragment_beat, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    public void setConfirmListener(OnSelListener listener) {
        this.confirmListener = listener;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        JSONParser parser = new JSONParser();

        lvSort = (ListView) view.findViewById(R.id.lv_sort);
        llFooter = (LinearLayout) view.findViewById(R.id.ll_footer);
        btnApply = (Button) view.findViewById(R.id.btn_apply);
        btnClear = (Button) view.findViewById(R.id.btn_clear);
        if (arrStatus != null && arrStatus.size() > 0) {
            mAdapter = new ItemsAdapter(arrStatus);
            lvSort.setAdapter(mAdapter);
        }

        lvSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                Object obj = view.getTag();
                if (obj != null) {
                    CustomerTyepModel model = (CustomerTyepModel) obj;
                    selId = model != null ? model.getCustomerGrpId() : "";
                }

            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (confirmListener != null)
                    confirmListener.onSelected(selId);
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (confirmListener != null)
                    confirmListener.onSelected("");
            }
        });
    }

    public interface OnSelListener {
        void onSelected(String string);
    }

    private class ItemsAdapter extends BaseAdapter {
        ArrayList<CustomerTyepModel> items;
        String selected = "";

        public ItemsAdapter(ArrayList<CustomerTyepModel> item) {
            this.items = item;
        }

        // @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_sort_dialog, null);
            }
            TextView tvSort = (TextView) v.findViewById(R.id.tv_sort_name);
            tvSort.setText(items.get(position).getCustomerName());

            v.setTag(items.get(position));
            return v;
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }
}
