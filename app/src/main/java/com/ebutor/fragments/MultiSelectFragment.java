package com.ebutor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.android.internal.util.Predicate;
import com.ebutor.R;
import com.ebutor.adapters.MultiSelectAdapter;
import com.ebutor.models.CustomerTyepModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collection;

public class MultiSelectFragment extends DialogFragment implements MultiSelectAdapter.OnSelectListener {

    private ArrayList<CustomerTyepModel> arrBeats;
    private ArrayList<String> arrSelected;
    private OnClickListener onClickListener;
    private ListView lvBeats;
    private String selBeats;
    private Button btnDone;
    private MultiSelectAdapter multiSelectAdapter;
    private String type = "";

    public static MultiSelectFragment newInstance(String type, ArrayList<CustomerTyepModel> arrBeats, String selBeats) {
        MultiSelectFragment frag = new MultiSelectFragment();
        Bundle args = new Bundle();
        args.putSerializable("manf", arrBeats);
        args.putString("manfIds", selBeats);
        args.putString("flagType", type);
        frag.setArguments(args);
        return frag;
    }

    public static ArrayList<String> filterNames(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerName());
            }
        }
        return result;
    }

    public static ArrayList<String> filterIds(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerGrpId());
            }
        }
        return result;
    }

    public void setClickListener(OnClickListener listener) {
        this.onClickListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        arrBeats = (ArrayList<CustomerTyepModel>) getArguments().getSerializable("manf");
        selBeats = (String) getArguments().getString("manfIds");
        type = (String) getArguments().getString("flagType");
        return inflater.inflate(R.layout.fragment_manf, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lvBeats = (ListView) view.findViewById(R.id.lv_sort);
        btnDone = (Button) view.findViewById(R.id.btn_done);

        multiSelectAdapter = new MultiSelectAdapter(getActivity(), arrBeats, selBeats, type);
        multiSelectAdapter.setSelectListener(MultiSelectFragment.this);

        lvBeats.setAdapter(multiSelectAdapter);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = "";
                JSONArray idsArray;
                Predicate<CustomerTyepModel> isCheckedData = new Predicate<CustomerTyepModel>() {
                    @Override
                    public boolean apply(CustomerTyepModel customerTyepModel) {
                        return customerTyepModel.isChecked();
                    }
                };

                ArrayList<String> checkedFilters = filterNames(arrBeats, isCheckedData);
//                if (selBeats != null && selBeats.length() > 0) {
//                    arrSelected = new ArrayList<String>(Arrays.asList(selBeats.split("\\s*,\\s*")));
//                    checkedFilters.addAll(arrSelected);
//                }
                result = TextUtils.join(",", checkedFilters);
                ArrayList<String> checkedFilterIds = filterIds(arrBeats, isCheckedData);
                if (checkedFilterIds != null && checkedFilterIds.size() == 0) {
                    idsArray = new JSONArray();
                    idsArray.put("-1");
                } else {
                    idsArray = new JSONArray(checkedFilterIds);
                }
//                ids = TextUtils.join(",", checkedFilterIds);
                dismiss();
                if (type.equalsIgnoreCase("Hub"))
                    onClickListener.onClicked(result, new JSONArray(), TextUtils.join(",", checkedFilterIds));
                else
                    onClickListener.onClicked(result, idsArray, TextUtils.join(",", checkedFilterIds));
            }
        });
    }

    @Override
    public void onSelected(boolean isSelected, int pos) {
        if (arrBeats.size() > pos)
            arrBeats.get(pos).setIsChecked(isSelected);
        /*int size = lvBeats.getAdapter().getCount();
        if (isUnSelectAll) {
            for (int i = 0; i < size - 1; i++) {
                View view = lvBeats.getChildAt(i);
                CheckedTextView ctvManf = (CheckedTextView) view.findViewById(R.id.ctv_manf);
                arrBeats.get(i).setIsChecked(false);
                ctvManf.setChecked(false);

            }
        }*/
    }

    public interface OnClickListener {
        void onClicked(String string, JSONArray jsonArray, String ids);
    }
}
