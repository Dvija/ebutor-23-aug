package com.ebutor.fragments;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.FullImagePreviewActivity;
import com.ebutor.MyApplication;
import com.ebutor.ProductDetailsActivity;
import com.ebutor.R;
import com.ebutor.adapters.ProductsAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.expandable.library.ExpandCollapseAnimation;
import com.ebutor.database.DBHelper;
import com.ebutor.models.AddCart1Response;
import com.ebutor.models.CartModel;
import com.ebutor.models.CashBackDetailsModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.NewPacksModel;
import com.ebutor.models.NewProductModel;
import com.ebutor.models.NewVariantModel;
import com.ebutor.models.ProductSlabData;
import com.ebutor.models.ProductsModel;
import com.ebutor.models.ReviewModel;
import com.ebutor.models.SpecificationsModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.models.WishListModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

//import com.ebutor.models.SKUPacksModel;

public class ProductDetailFragment extends Fragment implements View.OnClickListener /*,ResultHandler,PackAdapter.onPackItemClickListener*/, ProductsAdapter.OnImageClickListner, Response.ErrorListener, VolleyHandler<Object> {

    /**
     * We remember, for each collapsable view its height.
     * So we dont need to recalculate.
     * The height is calculated just before the view is drawn.
     */
    private final SparseIntArray viewHeights = new SparseIntArray(10);
    public LayoutInflater inflater;
    boolean isAddReviewVisible = false;
    ArrayList<ReviewModel> arrReviews;
    ArrayList<ProductsModel> arrRelatedProducts;
    String appId, name;
    JSONArray variantsJSONArray = new JSONArray();
    ArrayList<NewPacksModel> arrSlabRates;
    WishListFragment.OkListener confirmListener = new WishListFragment.OkListener() {
        @Override
        public void onOkClicked(String wishListId) {
            ProductDetailFragment.this.wishListId = wishListId;
            addProductToWishList(wishListId);
        }
    };
    private ArrayList<ProductSlabData> arrFreebieSlabData;
    private CartModel cartModel;
    private TextView tvFreebieDesc;
    private View offerDescView, offerEcashView;
    private Button btnOfferImage;
    private TextView tvProduct, tvMRP, tvESP, tvAddReview, tvInv, tvCfc,
            tvDescription, tvPackOf, tvPackPrice, tvMargin, tvQTY;
    private Button btReviewSubmit;
    private ArrayList<String> arrVariant2;
    private ViewPager pager;
    private EditText etReview;
    private TestProductModel selectedProductModel;
    //    private ListView lvPacks;
    private int cartCount = 0;
    private ProductsAdapter productsAdapter;
    private ArrayList<String> arrProductImgs;
    private String primaryImage = "";
    //    private PackAdapter packAdapter;
    private View view;
    private CirclePageIndicator circlePageIndicator;
    private String productId, slabProductId, parentId;
    private SharedPreferences mSharedPreferences;
    private LinearLayout llSKU;
    //    private NewProductModel productModel;
    //    private ArrayList<SKUPacksModel> arrpacks;
    private boolean expandable, isFF = false,isSalesAgent = false;
    private LinearLayout llCashBack, slabsLayout, llReviewRating, llMainRelatedPrds, llPreviousReview, llAddReview, variantsLayout1, variantsLayout2, innerVariantsLayout1, innerVariantsLayout2;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    private NewPacksModel selectedPackModel;
    private ProgressBar progress;
    private TextView tvNoPacks, tvEcashDesc;
    private boolean isSpecs = true;
    private int totalQty = 0, selPackBlockedQty = 0, selPackPromotionId = 0, selProductSlabId = 0;
    private int freebieMpq;
    private boolean isChild = false, selPackIsSlab = false;
    private String variant_1_name, variant_2_name, variant_3_name, marginToApply, unitPriceToApply;
    /**
     * Reference to the last expanded list item.
     * Since lists are recycled this might be null if
     * though there is an expanded list item
     */
    private View lastOpen = null;
    /**
     * The position of the last expanded list item.
     * If -1 there is no list item expanded.
     * Otherwise it points to the position of the last expanded list item
     */
    private int lastOpenPosition = -1;
    /**
     * Default Animation duration
     * Set animation duration with @see setAnimationDuration
     */
    private int animationDuration = 330;
    /**
     * A list of positions of all list items that are expanded.
     * Normally only one is expanded. But a mode to expand
     * multiple will be added soon.
     * <p/>
     * If an item onj position x is open, its bit is set
     */
    private BitSet openItems = new BitSet();
    private double totalPrice;
    private View viewDesc, viewSpecs, viewRatings;
    private RatingBar rbRating;
    private String position, rating, requestType = "", wishListId = "";
    private TextView tvInventory, tvAlertMsg;
    private ImageView ivWishList;
    private ArrayList<WishListModel> wishListArray;
    private RelativeLayout rlAlert;
    private LinearLayout llMain, llSpecifications;
    //    private View footerView;
    private TextView tvBadge, tvTotalAmount, tvSpecName, tvSpecValue, tvNoSpec;
    private Button btnProceed;
    private Tracker mTracker;
    private ImageView ivNoImage;
    private String freebieProductId, freebieQty;
    private TestProductModel testProductModel;
    private ArrayList<SpecificationsModel> arrSpecs;
    private Dialog dialog;
    private ProgressDialog progressDialog;
    private boolean isFreebie = false;
    private String packType = "";
    private Button rupee;
    View.OnClickListener onVariantClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            Toast.makeText(getActivity(), "Variant Clicked", Toast.LENGTH_SHORT).show();
            marginToApply = "0";
            unitPriceToApply = "0";
            if (v != null && v.getId() == R.id.id_variant_one) {
                int count = variantsLayout1.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout1.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                Object object = v.getTag();
                if (object != null && object instanceof String) {
                    variant_2_name = (String) object;

                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                    ll.removeAllViews();

                    innerVariantsLayout2 = (LinearLayout) view.findViewById(R.id.inner_variants_2);
                    innerVariantsLayout2.removeAllViews();
                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, "");
                    if (testProductModel != null) {
                        setDetails(testProductModel);
                        isSpecs = true;
                        view.findViewById(R.id.expandable_product_specifications).setVisibility(View.GONE);
                        view.findViewById(R.id.expandable_product_ratings).setVisibility(View.GONE);
                        packType = testProductModel.getPackType();

                    }
                    String variant2list = DBHelper.getInstance().getVariant3List(parentId, variant_1_name, variant_2_name);
                    if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                        if (variant2list != null && variant2list.length() > 0)
                            arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));
                        if (arrVariant2 != null && arrVariant2.size() > 0) {
                            // has inner variants, Create one more variants row
                            View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                            if (innerVariantsView != null) {
                                variantsLayout2 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout2 != null) {
                                    variantsLayout2.removeAllViews();
                                    int selectedPos = 0;
                                    ArrayList<String> arrTemp = new ArrayList<>();
                                    String name = DBHelper.getInstance().getVariantName(productId, variant_1_name, variant_2_name, "", DBHelper.COL_PRODUCT_VARIANT3);
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "", color = "#FFFFFF";
                                        if (arrVariant2.get(i).split(":").length > 0) {
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        }
                                        if (arrVariant2.get(i).split(":").length > 1) {
                                            color = arrVariant2.get(i).split(":")[1];
                                        }
                                        if (childVariant.equalsIgnoreCase(name))
                                            selectedPos = i;
                                        if (!arrTemp.contains(childVariant)) {
                                            arrTemp.add(childVariant);
                                            LinearLayout childVariantLabel = createChildVariant(childVariant, color, R.id.id_variant_two);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout2.addView(childVariantLabel);
                                        }

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout2.getChildCount() > 0) {
                                        variantsLayout2.getChildAt(selectedPos).performClick();
                                        variantsLayout2.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout2.addView(innerVariantsView);
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //add packs
                        if (testProductModel != null) {
                            slabProductId = testProductModel.getProductId();
                            selectedProductModel = testProductModel;
                            LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            TextView tvEsp = (TextView) view.findViewById(R.id.tvESP);
                            TextView tvInv = (TextView) view.findViewById(R.id.tvInv);
                            TextView tvCfc = (TextView) view.findViewById(R.id.tvcfc);
                            ImageView ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);
                            if (testProductModel.isCashback()) {
                                ivCashback.setVisibility(View.VISIBLE);
                            } else {
                                ivCashback.setVisibility(View.GONE);
                            }
                            if (tvEsp != null)
                                tvEsp.setText("");
                            if (tvInv != null)
                                tvInv.setVisibility(View.GONE);
                            if (tvCfc != null)
                                tvCfc.setVisibility(View.GONE);
                            getSlabRates(slabProductId);
                        }
                    }
                }
            } else if (v.getId() == R.id.id_variant_two) {
                //todo
                Object object = v.getTag();
                int count = variantsLayout2.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout2.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                if (v.getTag() != null)
                    variant_3_name = (String) v.getTag();
                final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                ll.removeAllViews();
                TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, variant_3_name);
                if (testProductModel != null) {

                    setDetails(testProductModel);
                    isSpecs = true;
                    view.findViewById(R.id.expandable_product_specifications).setVisibility(View.GONE);
                    view.findViewById(R.id.expandable_product_ratings).setVisibility(View.GONE);

                    slabProductId = testProductModel.getProductId();
                    selectedProductModel = testProductModel;
                    packType = testProductModel.getPackType();
                    LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                    slabRatesLayout.removeAllViews();
                    TextView tvEsp = (TextView) view.findViewById(R.id.tvESP);
                    TextView tvInv = (TextView) view.findViewById(R.id.tvInv);
                    TextView tvCfc = (TextView) view.findViewById(R.id.tvcfc);
                    ImageView ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);
                    if (tvEsp != null)
                        tvEsp.setText("");
                    if (tvInv != null)
                        tvInv.setVisibility(View.GONE);
                    if (tvCfc != null)
                        tvCfc.setVisibility(View.GONE);

                    if (testProductModel.isCashback()) {
                        ivCashback.setVisibility(View.VISIBLE);
                    } else {
                        ivCashback.setVisibility(View.GONE);
                    }
                    getSlabRates(slabProductId);
                }
            } else if (v.getId() == R.id.id_slab_qty) {
                if (v.getTag() != null) {

                    NewPacksModel newPacksModel = (NewPacksModel) v.getTag();
                    selectedPackModel = newPacksModel;
                    int slabsCount = slabsLayout.getChildCount();
                    for (int i = 0; i < slabsCount; i++) {
                        View btnView = slabsLayout.getChildAt(i);
                        btnView.setSelected(false);
                        selectedPackModel.setSelected(false);
                    }
                    v.setSelected(true);
                    selectedPackModel.setSelected(true);

                    if (tvESP != null) {

                        double ptr = 0;
                        try {
                            ptr = Double.parseDouble(newPacksModel.getPtr());
                        } catch (Exception e) {

                        }
                        if (ptr > 0) {
                            final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getPtr() == null ? getActivity().getResources().getString(R.string.ptr) + " : " : getActivity().getResources().getString(R.string.ptr) + " : " + String.format(Locale.CANADA, "%.2f", ptr));
                            setMRP(sb, tvESP);

                        } else {
                            tvESP.setText("");
                        }
                    }
                    if (isFF) {
                        tvInv.setVisibility(View.VISIBLE);
                        tvCfc.setVisibility(View.VISIBLE);
                    } else {
                        tvInv.setVisibility(View.GONE);
                        tvCfc.setVisibility(View.GONE);
                    }

                    if (tvInv != null) {

                        int inv = 0;
                        try {
                            inv = Integer.parseInt(newPacksModel.getStock());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getStock() == null ? getResources().getString(R.string.inv) + " : " : getResources().getString(R.string.inv) + " : " + inv);
                        setMRP(sb, tvInv);
                    }

                    if (tvCfc != null) {

                        int cfc = 0;
                        try {
                            cfc = Integer.parseInt(newPacksModel.getCfc());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getCfc() == null ? getResources().getString(R.string.cfc) + " : " : getResources().getString(R.string.cfc) + " : " + cfc);
                        setMRP(sb, tvCfc);
                    }
                    if (newPacksModel != null) {
                        final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                        ll.removeAllViews();
                        createPacksForVariant(newPacksModel, ll);
                    }
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        productId = getArguments().getString("ProductId");
        parentId = getArguments().getString("ParentId");
        isChild = getArguments().getBoolean("isChild");
        position = getArguments().getString("position");
        View convertView = inflater.inflate(R.layout.fragment_product_detail, container, false);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return convertView;
    }

    @Override
    public void onViewCreated(View convertView, Bundle savedInstanceState) {
        super.onViewCreated(convertView, savedInstanceState);
        setHasOptionsMenu(true);
        view = convertView;

        inflater = getActivity().getLayoutInflater();

        appId = mSharedPreferences.getString(ConstantValues.KEY_APP_ID, "");
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        isSalesAgent = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_SALES_AGENT, false);

        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        llSKU = (LinearLayout) convertView.findViewById(R.id.llSKU);
        llReviewRating = (LinearLayout) convertView.findViewById(R.id.ll_review_rating);
        llMainRelatedPrds = (LinearLayout) convertView.findViewById(R.id.ll_main_relaed_prds);
        llAddReview = (LinearLayout) convertView.findViewById(R.id.ll_add_review);
        llPreviousReview = (LinearLayout) convertView.findViewById(R.id.ll_previous_reviews);
        ivWishList = (ImageView) convertView.findViewById(R.id.iv_wish_list);
        tvProduct = (TextView) convertView.findViewById(R.id.tvProduct);
        tvMRP = (TextView) convertView.findViewById(R.id.tvMRP);
        tvESP = (TextView) convertView.findViewById(R.id.tvESP);
        tvInv = (TextView) convertView.findViewById(R.id.tvInv);
        tvCfc = (TextView) convertView.findViewById(R.id.tvcfc);
        tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
        tvInventory = (TextView) convertView.findViewById(R.id.tvInventory);
        tvAddReview = (TextView) convertView.findViewById(R.id.tv_add_review);
        tvAlertMsg = (TextView) convertView.findViewById(R.id.tv_alert_msg);
        ivNoImage = (ImageView) convertView.findViewById(R.id.iv_no_image);
        tvNoSpec = (TextView) convertView.findViewById(R.id.tv_no_spec);
        llMain = (LinearLayout) convertView.findViewById(R.id.ll_main);
        llSpecifications = (LinearLayout) convertView.findViewById(R.id.expandable_product_specifications);
        rlAlert = (RelativeLayout) convertView.findViewById(R.id.rl_alert);
        etReview = (EditText) convertView.findViewById(R.id.et_review);
        btReviewSubmit = (Button) convertView.findViewById(R.id.bt_review_submit);
        pager = (ViewPager) convertView.findViewById(R.id.pager);
        circlePageIndicator = (CirclePageIndicator) convertView.findViewById(R.id.circlePageIndicator);
        viewDesc = convertView.findViewById(R.id.expandable_toggle_product_desc);
        viewSpecs = convertView.findViewById(R.id.expandable_toggle_product_specs);
        viewRatings = convertView.findViewById(R.id.expandable_toggle_product_ratings);
        rbRating = (RatingBar) convertView.findViewById(R.id.product_rating);

        tvAddReview.setPaintFlags(tvAddReview.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

//        footerView = convertView.findViewById(R.id.cart_footer);
//        if (footerView != null)
//            tvBadge = (TextView) footerView.findViewById(R.id.tvBadge);
//
//        if (footerView != null) {
//            tvTotalAmount = (TextView) footerView.findViewById(R.id.tvTotalAmount);
//            btnProceed = (Button) footerView.findViewById(R.id.btnProceed);
//
//            btnProceed.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
//                        startActivity(new Intent(getActivity(), POCartActivity.class));
//                    } else {
//                        Toast.makeText(getActivity(), "Please login to view cart", Toast.LENGTH_LONG).show();
//                    }
//                }
//            });
//        }

        arrProductImgs = new ArrayList<String>();
//        arrpacks = new ArrayList<>();
        arrReviews = new ArrayList<>();
        arrRelatedProducts = new ArrayList<>();
        wishListArray = new ArrayList<WishListModel>();
        ivWishList.setVisibility(View.GONE);

//        lvPacks = (ListView) convertView.findViewById(R.id.lvPacks);

        testProductModel = DBHelper.getInstance().getProductById(parentId, isChild);
        if (testProductModel != null) {
            setProductDetails();
        } else {
            getProduct();
        }

        enableFor(viewDesc, convertView.findViewById(R.id.expandable_product_desc), 1);
        enableFor(viewSpecs, convertView.findViewById(R.id.expandable_product_specifications), 2);
        enableFor(viewRatings, convertView.findViewById(R.id.expandable_product_ratings), 3);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        tvAddReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isAddReviewVisible) {
                    llAddReview.setVisibility(View.VISIBLE);
                    // tvReviewRating.setCompoundDrawables(null,null,getResources().getDrawable(R.drawable.ic_down_arrow),null);
                    isAddReviewVisible = true;
                } else {
                    llAddReview.setVisibility(View.GONE);
                    //tvReviewRating.setCompoundDrawables(null, null,getResources().getDrawable(R.drawable.ic_right_arrow), null);
                    isAddReviewVisible = false;
                }
            }
        });

        ivWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWishList();

            }
        });

        tvInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Networking.isNetworkAvailable(getActivity())) {

                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("product_id", slabProductId);
                        obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                        obj.put("quantity", "0");
                        obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                        obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                        obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

                        HashMap<String, String> map = new HashMap<>();
                        map.put("data", obj.toString());

                        VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCart1URL, map, ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.CHECK_INVENTORY);
                        getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.addCart1URL);
                        progressDialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showToast(getString(R.string.no_network_connection));
                }
            }
        });

        btReviewSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                String review = etReview.getText().toString();
                if (TextUtils.isEmpty(review)) {
                    Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.please_enter_review));
                } else {
                    addReview();
                }
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestType) {
                        case "AddReview":
                            addReview();
                            break;
                        case "WishList":
                            getWishList();
                            break;
                        case "AddWishList":
                            addProductToWishList(wishListId);
                            break;
                        case "ProductDetail":
//                            getProductDetails();
                            break;
                        case "AddCart":
//                            updateToCart(productModel);
                            break;
                        default:
//                            getProductDetails();
                            break;
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void getProduct() {
        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.OFFLINE_PRODUCTS;
            String appUrl = AppURL.offlineProductsURL;

            obj.put("offset", "0");
            obj.put("offset_limit", "20");
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("product_ids", productId);

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, appUrl, map,
                    ProductDetailFragment.this, ProductDetailFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, appUrl);
            if (dialog != null)
                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addReview() {
        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {

            if (Networking.isNetworkAvailable(getActivity())) {
                rlAlert.setVisibility(View.GONE);
                llMain.setVisibility(View.VISIBLE);
                requestType = "AddReview";
                try {
                    HashMap<String, String> map = new HashMap<>();

                    JSONObject dataObject = new JSONObject();
                    dataObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                    JSONObject reviewObject = new JSONObject();
                    reviewObject.put("review_type", "product");
                    reviewObject.put("entity_id", productId);
                    reviewObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    reviewObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    reviewObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                    String review = etReview.getText().toString();
                    if (!TextUtils.isEmpty(review) && review.length() > 0) {
                        reviewObject.put("comment", review);
                    }

                    float rating = rbRating.getRating();
                    reviewObject.put("rating", rating);
                    if (rating < 0.5) {
                        Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.please_rate_product));
                        return;
                    }
                    reviewObject.put("status", "0");
                    dataObject.put("reviews", reviewObject);

                    map.put("data", dataObject.toString());
                    VolleyBackgroundTask addReviewReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.addReviewURL, map, ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.ADD_REVIEW);
                    addReviewReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    MyApplication.getInstance().addToRequestQueue(addReviewReq, AppURL.addReviewURL);
                    if (dialog != null)
                        dialog.show();
                } catch (Exception e) {

                }

            } else {
                rlAlert.setVisibility(View.VISIBLE);
                llMain.setVisibility(View.GONE);
                requestType = "AddReview";
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.please_login_add_review));
        }
    }

    private void getWishList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "GetWishList";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "1");
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.shoppinglistURL, map, ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.WISHLIST);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.shoppinglistURL);
                if (dialog != null)
                    dialog.show();

//                HTTPBackgroundTask task = new HTTPBackgroundTask(MainActivity.this, MainActivity.this, PARSER_TYPE.WISHLIST, APIREQUEST_TYPE.HTTP_POST);
//                task.execute(map);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestType = "GetWishList";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void enableFor(final View button, final View target, final int position) {
        if (target == lastOpen && position != lastOpenPosition) {
            // lastOpen is recycled, so its reference is false
            lastOpen = null;
        }
        if (position == lastOpenPosition) {
            // re reference to the last view
            // so when can animate it when collapsed
            lastOpen = target;
        }
        int height = viewHeights.get(position, -1);
        if (height == -1) {
            viewHeights.put(position, target.getMeasuredHeight());
            updateExpandable(target, position);
        } else {
            updateExpandable(target, position);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (position == 2 || position == 3) {
                    if (isSpecs)
                        getReviewsSpecs();
                }
                Animation a = target.getAnimation();

                if (a != null && a.hasStarted() && !a.hasEnded()) {

                    a.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            view.performClick();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                } else {

                    target.setAnimation(null);

                    int type = target.getVisibility() == View.VISIBLE
                            ? ExpandCollapseAnimation.COLLAPSE
                            : ExpandCollapseAnimation.EXPAND;

                    // remember the state
                    if (type == ExpandCollapseAnimation.EXPAND) {
                        openItems.set(position, true);
                    } else {
                        openItems.set(position, false);
                    }
                    // check if we need to collapse a different view
                    if (type == ExpandCollapseAnimation.EXPAND) {
                        if (lastOpenPosition != -1 && lastOpenPosition != position) {
                            if (lastOpen != null) {
                                animateView(lastOpen, ExpandCollapseAnimation.COLLAPSE);

                            }
                            openItems.set(lastOpenPosition, false);
                        }
                        lastOpen = target;
                        lastOpenPosition = position;
                    } else if (lastOpenPosition == position) {
                        lastOpenPosition = -1;
                    }
                    animateView(target, type);
                }
            }
        });
    }

    private void getReviewsSpecs() {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "ReviewsSpecs";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);

            try {
                JSONObject obj = new JSONObject();
                obj.put("product_id", slabProductId == null ? productId : slabProductId);
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getReviewsSpecsURL, map,
                        ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.REVIEW_SPEC);
                productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getReviewsSpecsURL);

                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestType = "ReviewsSpecs";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    /**
     * Performs either COLLAPSE or EXPAND animation on the target view
     *
     * @param target the view to animate
     * @param type   the animation type, either ExpandCollapseAnimation.COLLAPSE
     *               or ExpandCollapseAnimation.EXPAND
     */
    private void animateView(final View target, final int type) {
        Animation anim = new ExpandCollapseAnimation(
                target,
                type
        );
        anim.setDuration(animationDuration);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {


            }
        });
        target.startAnimation(anim);
    }

    private void updateExpandable(View target, int position) {

        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) target.getLayoutParams();
        if (openItems.get(position)) {
            target.setVisibility(View.VISIBLE);
            params.bottomMargin = 0;
        } else {
            target.setVisibility(View.GONE);
            params.bottomMargin = 0 - viewHeights.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       /* if (item.getItemId() == R.id.cart) {

            if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                startActivity(new Intent(getActivity(), POCartActivity.class));
            } else {
                Toast.makeText(getActivity(), "Please login to view cart", Toast.LENGTH_LONG).show();
            }

        } else if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }*/
        return super.onOptionsItemSelected(item);
    }

    private void getProductDetails() {

        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "ProductDetail";

            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("product_id", productId);
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                jsonObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                jsonObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                map.put("data", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productDetailURL, map,
                    ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.GET_PRODUCT_DETAIL);
            productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.productDetailURL);
            if (dialog != null)
                dialog.show();


        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "ProductDetail";
        }
    }

//    private void showPopup() {
//        View view = LayoutInflater.from(getActivity()).inflate(R.layout.popup_delivery_availability, null);
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setCanceledOnTouchOutside(true);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(view);
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        dialog.show();
//        Button btnVerify = (Button) view.findViewById(R.id.btnVerify);
//        final EditText etPin = (EditText) view.findViewById(R.id.etPin);
//        btnVerify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (TextUtils.isEmpty(etPin.getText().toString()))
//                    Toast.makeText(getActivity(), "Please enter Area Pin.", Toast.LENGTH_SHORT).show();
//                else {
//                    dialog.dismiss();
//                    checkAvailability(etPin.getText().toString());
//                }
//            }
//        });
//    }

//    private void checkAvailability(String pinCode) {
//        if (Networking.isNetworkAvailable(getActivity())) {
//            HashMap<String, String> map = new HashMap<>();
//            try {
//                String appId = mSharedPreferences.getString(ConstantValues.KEY_APP_ID, "");
//                JSONObject jsonObject = new JSONObject();
//                if (!TextUtils.isEmpty(appId) && appId.length() > 0) {
//                    jsonObject.put("appId", appId);
//                }
//                if (pinCode != null && pinCode.length() > 0) {
//                    jsonObject.put("pinCode", pinCode);
//                }
//                jsonObject.put("token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
//                map.put("data", jsonObject.toString());
//
//                HTTPBackgroundTask task = new HTTPBackgroundTask(this, getActivity(), PARSER_TYPE.PINCODE_AVAILABILITY, APIREQUEST_TYPE.HTTP_POST);
//                task.execute(map);
//
//            } catch (Exception e) {
//
//            }
//        } else{
//            Utils.showAlertWithMessage(getActivity(), ConstantValues.ALERT_NO_NETWORK);
//        }
//    }

    private void getProducts(String productIds, boolean isFreebie) {
        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.OFFLINE_PRODUCTS;
            String appUrl = AppURL.offlineProductsURL;

            obj.put("offset", "0");
            obj.put("offset_limit", "20");
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("product_ids", productIds);

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, appUrl, map,
                    ProductDetailFragment.this, ProductDetailFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, appUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListViewHeightBasedOnChildren(ListView lvPacks) {
        ListAdapter listAdapter = lvPacks.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(lvPacks.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, lvPacks);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = lvPacks.getLayoutParams();
        params.height = totalHeight + (lvPacks.getDividerHeight() * (listAdapter.getCount() - 1));
        lvPacks.setLayoutParams(params);
    }

//    @Override
//    public void onFinish(Object results, PARSER_TYPE requestType) {
//
//        if (results != null) {
//
//            String response = (String) results;
//            if (!TextUtils.isEmpty(response)) {
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    String message = jsonObject.getString("message");
//                    String status = jsonObject.getString("status");
//
//                    if (status.equalsIgnoreCase("success")) {
//
//                        if (requestType == PARSER_TYPE.PINCODE_AVAILABILITY) {
//                            message = jsonObject.getString("data");
//                            Utils.showAlertWithMessage(getActivity(), message);
//                        } else if (requestType == PARSER_TYPE.ADD_REVIEW) {
//                            JSONObject dataObj = jsonObject.optJSONObject("data");
//                            message = dataObj.optString("message");
//                            rating = dataObj.optString("rating");
//                            if (rating == null || TextUtils.isEmpty(rating)) {
//                                rating = "0.0";
//                            }
//                            if (rating != null && !TextUtils.isEmpty(rating) && !rating.equalsIgnoreCase("null")) {
//                                DecimalFormat format = new DecimalFormat("0.0");
//                                double newmargin = Double.parseDouble(rating);
//                                rating = format.format(newmargin);
//                            }
//                            productModel.setProductRating(rating);
//                            tvRating.setText(rating);
//                            etReview.setText("");
//                            rbRating.setRating(0);
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                        } else if (requestType == PARSER_TYPE.WISHLIST) {
//                            JSONArray listArray = jsonObject.optJSONArray("data");
//                            if (listArray != null && listArray.length() > 0) {
//                                for (int i = 0; i < listArray.length(); i++) {
//                                    JSONObject listObj = listArray.optJSONObject(i);
//                                    WishListModel model = new WishListModel();
//                                    model.setListId(listObj.optString("buyer_listing_id"));
//                                    model.setListName(listObj.optString("listing_name"));
//                                    model.setCreatedDate(listObj.optString("create_date"));
//                                    wishListArray.add(model);
//                                }
//                                showWishListDialog();
//                            } else {
//                                Toast.makeText(getActivity(), "Please create a list", Toast.LENGTH_SHORT).show();
//                            }
//                        } else if (requestType == PARSER_TYPE.ADD_TO_CART) {
//                            JSONObject dataObj = jsonObject.optJSONObject("data");
//
//                            JSONArray cartIds = dataObj.optJSONArray("cart");
//                            if (cartIds != null && cartIds.length() > 0) {
//                                String cartId = cartIds.getString(0);
//                                if (productModel != null) {
//                                    productModel.setCartId(cartId);
//                                }
//
//                                MyApplication.getInstance().addToCart(productModel, mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
//                                updateCartCount();
////                                productModel.setTotalPrice(0);
////                                productModel.setTotalItems(0);
////                                for(int i = 0;i<productModel.getSkuModelArrayList().size();i++){
////                                }
////                                getProductDetails();
//                                Toast.makeText(getActivity(), "Added to cart", Toast.LENGTH_LONG).show();
//                                exportDatabase("factail_database");
//                            }
//
//                        } else if (requestType == PARSER_TYPE.GET_PRODUCT_DETAIL) {
//                            JSONObject dataObject = jsonObject.getJSONObject("data");
//                            JSONObject productDataObj = dataObject.getJSONObject("data");
//
//                            productModel = new NewProductModel();
//                            productModel.setProductId(productDataObj.getString("product_id"));
//                            productModel.setProductName(productDataObj.getString("name"));
//                            productModel.setProductRating(productDataObj.getString("rating"));
//                            productModel.setProductDescription(productDataObj.getString("description"));
//                            JSONArray variantsArr = productDataObj.getJSONArray("variants");
//                            ArrayList<SKUModel> skuModelArrayList = new ArrayList<>();
//                            for (int i = 0; i < variantsArr.length(); i++) {
//                                JSONObject skuObject = variantsArr.getJSONObject(i);
//                                ArrayList<SKUPacksModel> packsModelArrayList = new ArrayList<SKUPacksModel>();
//                                SKUModel skuModel = new SKUModel();
//                                skuModel.setSkuId(skuObject.getString("variant_id"));
//                                skuModel.setProductVariantId(skuObject.optString("product_variant_id"));
//                                skuModel.setProductName(skuObject.optString("product_name"));
//                                skuModel.setProductDescription(skuObject.optString("description"));
//                                skuModel.setModel(skuObject.getString("model"));
//                                skuModel.setDefault(skuObject.getString("is_default").equalsIgnoreCase("1") ? true : false);
//                                skuModel.setSkuImage(skuObject.getString("image"));
//                                skuModel.setAvailableQuantity(skuObject.optString("quantity"));
//                                skuModel.setSku(skuObject.getString("sku"));
//                                skuModel.setMrp(skuObject.getString("mrp"));
//                                skuModel.setSkuName(skuObject.getString("name"));
//                                JSONArray packsArr = skuObject.getJSONArray("pack");
//                                DecimalFormat decimalFormat = new DecimalFormat("##.##");
//                                for (int j = 0; j < packsArr.length(); j++) {
//                                    JSONObject packObject = packsArr.getJSONObject(j);
//                                    SKUPacksModel skuPacksModel = new SKUPacksModel();
//                                    skuPacksModel.setVariantPriceId(packObject.optString("variant_price_id"));
//                                    skuPacksModel.setPackSize(packObject.getString("pack_size"));
//                                    skuPacksModel.setPackPrice(TextUtils.isEmpty(packObject.getString("dealer_price")) ? "0" : decimalFormat.format(Double.parseDouble(packObject.getString("dealer_price"))));
//                                    skuPacksModel.setMargin(TextUtils.isEmpty(packObject.getString("margin")) ? "0" : decimalFormat.format(Double.parseDouble(packObject.getString("margin"))));
//                                    skuPacksModel.setUnitPrice(TextUtils.isEmpty(packObject.getString("unit_price")) ? "0" : decimalFormat.format(Double.parseDouble(packObject.getString("unit_price"))));
//                                    packsModelArrayList.add(skuPacksModel);
//                                }
//                                skuModel.setSkuPacksModelArrayList(packsModelArrayList);
//                                skuModelArrayList.add(skuModel);
//                                JSONArray arrImages = skuObject.optJSONArray("images");
//                                arrProductImgs = new ArrayList<>();
//                                for (int k = 0; k < arrImages.length(); k++) {
//                                    JSONObject objImage = arrImages.optJSONObject(k);
//                                    arrProductImgs.add(objImage.optString("image"));
//                                }
//                                skuModel.setArrImages(arrProductImgs);
//                                JSONObject objSpec = skuObject.optJSONObject("specifications");
//                                arrSpecs = new ArrayList<>();
//                                Iterator<String> keySet = objSpec.keys();
//                                while ((keySet.hasNext())) {
//                                    String key = keySet.next();
//                                    if (!key.equalsIgnoreCase("-1")) {
//                                        SpecificationsModel specificationsModel = new SpecificationsModel();
//                                        String specValue = objSpec.optString(key);
//                                        key = key.replace("_", " ");
//                                        specValue = specValue.replace("_", " ");
//                                        specificationsModel.setSpecName(key);
//                                        specificationsModel.setSpecValue(specValue);
//                                        if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(specValue))
//                                            arrSpecs.add(specificationsModel);
//                                    }
//                                }
//                                skuModel.setArrSpecs(arrSpecs);
//                            }
//
//                            JSONArray reviewArray = dataObject.optJSONArray("reviews");
//                            if (reviewArray != null && reviewArray.length() > 0) {
//                                for (int i = 0; i < reviewArray.length(); i++) {
//                                    JSONObject reviewObj = reviewArray.getJSONObject(i);
//                                    String review = reviewObj.optString("text");
//                                    String customer_id = reviewObj.optString("customer_id");
//                                    String author = reviewObj.optString("author");
//                                    String rating = reviewObj.optString("rating");
//
//                                    ReviewModel model = new ReviewModel();
//                                    model.setReview(review);
//                                    model.setAuthor(author);
//                                    model.setRating(rating);
//                                    model.setCustomerId(customer_id);
//                                    arrReviews.add(model);
//                                }
//                            }
//                            productModel.setArrReviews(arrReviews);
//                            productModel.setSkuModelArrayList(skuModelArrayList);
//                            JSONArray relatedProductsArray = dataObject.optJSONArray("related_products");
//                            if (relatedProductsArray != null && relatedProductsArray.length() > 0) {
//                                for (int i = 0; i < relatedProductsArray.length(); i++) {
//                                    JSONObject relatedPrdObj = relatedProductsArray.getJSONObject(i);
//                                    String relatedId = relatedPrdObj.optString("related_id");
//
//                                    String image = relatedPrdObj.optString("image");
//                                    //todo change key name from service
//                                    String prdName = relatedPrdObj.optString("name");
//                                    ProductsModel model = new ProductsModel();
//                                    model.setImage(image);
//                                    model.setProduct_id(relatedId);
//                                    model.setName(prdName);
//                                    arrRelatedProducts.add(model);
//                                }
//
//                            }
//                            productModel.setArrRelatedPrds(arrRelatedProducts);
//                            if (productModel != null) {
//                                setProductDetails();
//                            }
//
//                        }
//                    } else {
//                        Utils.showAlertWithMessage(getActivity(), message);
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llRecommendedCategories:
                Object objModel = v.getTag();
                if (objModel != null && objModel instanceof ProductsModel) {
                    Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
                    intent.putExtra("ProductId", ((ProductsModel) objModel).getProduct_id());
                    startActivity(intent);
                }
                break;
            case R.id.ivMinus:
                String qty = tvQTY.getText().toString();
                int intQty = Integer.parseInt(qty);
                if (intQty > 0)
                    intQty--;
                tvQTY.setText(String.valueOf(intQty));
                break;
//            case R.id.tvSku:
//                arrpacks = (ArrayList<SKUPacksModel>) v.getTag();
//                if (arrpacks.size() > 0) {
//                    lvPacks.setVisibility(View.VISIBLE);
//                    tvNoPack.setVisibility(View.GONE);
//                    packAdapter = new PackAdapter(getActivity(), arrpacks);
//                    packAdapter.initializeListener(ProductDetailFragment.this);
//                    lvPacks.setAdapter(packAdapter);
////                    packAdapter.refresh(arrpacks);
//                } else {
//                    lvPacks.setVisibility(View.GONE);
//                    tvNoPack.setVisibility(View.VISIBLE);
//                }
//                break;
        }
    }

    public void showWishListDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        WishListFragment editNameDialog = WishListFragment.newInstance(wishListArray);
        editNameDialog.setCancelable(true);
        editNameDialog.setConfirmListener(confirmListener);
        editNameDialog.show(fm, "areas_fragment_dialog");
    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        ((ProductDetailsActivity) getActivity()).updateCart(cartCount);
        if (tvBadge != null) {
            tvBadge.setText(String.valueOf(this.cartCount));
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setProductDetails() {

        setDetails(testProductModel);

        llSKU.removeAllViews();
        if (testProductModel != null && testProductModel.getVariantModelArrayList() != null) {
            ArrayList<String> arrTemp = new ArrayList<>();
            for (int j = 0; j < testProductModel.getVariantModelArrayList().size(); j++) {
//            final NewVariantModel skuModel = productModel.getVariantModelArrayList().get(j);
//            tvProduct.setText(testProductModel.getVariantModelArrayList().get(0).trim());

                DisplayMetrics displaymetrics = new DisplayMetrics();
                WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
                windowManager.getDefaultDisplay().getMetrics(displaymetrics);
                int height = displaymetrics.heightPixels;

                final LinearLayout skuView = new LinearLayout(getActivity());
                skuView.setOrientation(LinearLayout.HORIZONTAL);
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                params1.setMargins(0, 0, 20, 0);
                skuView.setLayoutParams(params1);

                ArrayList<String> variantsList = testProductModel.getVariantModelArrayList();
                name = "";
                String color = "#FFFFFF";
                if (variantsList.get(j).split(":").length > 0) {
                    name = variantsList.get(j).split(":")[0];
                }
                if (variantsList.get(j).split(":").length > 1) {
                    color = variantsList.get(j).split(":")[1];
                }

                if (!arrTemp.contains(name)) {
                    arrTemp.add(name);
                    ImageView imageView = new ImageView(getActivity());
                    Drawable sourceDrawable = getActivity().getResources().getDrawable(R.drawable.ic_star);

                    Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
                    Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(color));
                    imageView.setImageBitmap(mFinalBitmap);
                    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                    params2.gravity = Gravity.CENTER_VERTICAL;
                    params2.setMargins(0, 0, 0, 0);
                    imageView.setLayoutParams(params2);

                    final TextView textView = new TextView(getActivity());
                    textView.setTextSize(12);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
                    params.gravity = Gravity.CENTER_VERTICAL;
                    params.setMargins(5, 0, 0, 0);
                    textView.setGravity(Gravity.CENTER);
                    textView.setLayoutParams(params);
                    skuView.setId(R.id.id_one);
                    skuView.setTag(name);
                    textView.setText(name == null ? "" : name);
                    String variantName = DBHelper.getInstance().getVariantName(productId, "", "", "", DBHelper.COL_PRODUCT_VARIANT1);
                    if (isChild && j == 0) {
                        skuView.setSelected(true);
                        OnProductSKUClicked(name, testProductModel.isChild());
                    } else {
                        if (name.equalsIgnoreCase(variantName)) {
                            skuView.setSelected(true);
                            OnProductSKUClicked(name, testProductModel.isChild());
                        } else {
                            skuView.setSelected(false);
                        }
                    }
                    skuView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int count = llSKU.getChildCount();
                            if (count > 0) {
                                for (int i = 0; i < count; i++) {
                                    View btnView = llSKU.getChildAt(i);
                                    btnView.setSelected(false);
                                }
                            }
                            v.setSelected(true);
                            if (offerDescView != null)
                                offerDescView.setVisibility(View.GONE);
                            boolean isChild = testProductModel.isChild();
                            TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, name, "", "");
                            if (testProductModel != null) {
                                setDetails(testProductModel);
                                isSpecs = true;
                                view.findViewById(R.id.expandable_product_specifications).setVisibility(View.GONE);
                                view.findViewById(R.id.expandable_product_ratings).setVisibility(View.GONE);

                            }
                            OnProductSKUClicked(skuView.getTag(), isChild);

                        }
                    });

                    skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
                    skuView.setPadding(10, 5, 10, 5);
                    skuView.addView(imageView);
                    skuView.addView(textView);

                    llSKU.addView(skuView);

          /*  if (productModel.getSkuModelArrayList() != null && productModel.getSkuModelArrayList().size() > 0) {
                final SKUModel SKUModel = productModel.getSkuModelArrayList().get(0);
                if (SKUModel.getSkuPacksModelArrayList() != null && SKUModel.getSkuPacksModelArrayList().size() > 0) {
                    SKUPacksModel skuPacksModel = SKUModel.getSkuPacksModelArrayList().get(0);
                    double mrp = 0;
                    try {
                        mrp = Double.parseDouble(SKUModel.getMrp());
                    } catch (Exception e) {
                    }
                    final SpannableStringBuilder sb = new SpannableStringBuilder(skuModel.getMrp() == null ? "" : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
                    setMRP(sb);
                }
            }*/
                }


            }
//        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
//                                        /*SKUModel skuModel = productModel.getSkuModelArrayList().get(0);
//                                        OnProductSKUClicked(skuModel, expandedPosition);*/
//            NewVariantModel selectedSKUModel = null;
//            for (NewVariantModel skuModel : productModel.getVariantModelArrayList()) {
//                if (skuModel.isSelected()) {
//                    selectedSKUModel = skuModel;
//                    break;
//                }
//            }
//
//            if (selectedSKUModel == null) {
//                for (NewVariantModel skuModel : productModel.getVariantModelArrayList()) {
//                    if (skuModel.isDefault()) {
//                        selectedSKUModel = skuModel;
//                        break;
//                    }
//                }
//            }
//
//            if (selectedSKUModel != null) {
//                OnProductSKUClicked(selectedSKUModel);
//            }
//        }
//        packAdapter = new PackAdapter(getActivity(), arrpacks);
//        packAdapter.initializeListener(ProductDetailFragment.this);
//        if (arrpacks.size() > 0) {
//            lvPacks.setAdapter(packAdapter);
//            lvPacks.setVisibility(View.VISIBLE);
//            tvNoPack.setVisibility(View.GONE);
//        } else {
//            lvPacks.setVisibility(View.GONE);
//            tvNoPack.setVisibility(View.VISIBLE);
//        }
//
//        setListViewHeightBasedOnChildren(lvPacks);
//        lvPacks.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });

//        for (int i = 0; i < productModel.getArrReviews().size(); i++) {
//            ReviewModel reviewModel = productModel.getArrReviews().get(i);
//            LinearLayout llReview = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_review, null);
//            TextView tvAuthor = (TextView) llReview.findViewById(R.id.tv_author_name);
//            TextView tvReview = (TextView) llReview.findViewById(R.id.tv_review);
//            RatingBar ratingBar = (RatingBar) llReview.findViewById(R.id.rating);
//            tvAuthor.setText(reviewModel.getAuthor());
//            tvReview.setText(reviewModel.getReview());
//            ratingBar.setRating(Float.parseFloat(reviewModel.getRating()));
//
//            llPreviousReview.addView(llReview);
//        }
//        if (productModel.getArrRelatedPrds().size() == 0) {
//            view.findViewById(R.id.tvRelatedProduct).setVisibility(View.GONE);
//        } else {
//            view.findViewById(R.id.tvRelatedProduct).setVisibility(View.VISIBLE);
//            for (int i = 0; i < productModel.getArrRelatedPrds().size(); i++) {
//
//                LinearLayout llRelatedPrd = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_top_brand, null);
//                TextView tvName = (TextView) llRelatedPrd.findViewById(R.id.tv_top_brand);
//                ImageView imageView = (ImageView) llRelatedPrd.findViewById(R.id.iv_top_brand);
//                tvName.setText(productModel.getArrRelatedPrds().get(i).getName());
//                ImageLoader.getInstance().displayImage(productModel.getArrRelatedPrds().get(i).getImage(), imageView, options, animateFirstListener);
//                llMainRelatedPrds.addView(llRelatedPrd);
//                llRelatedPrd.setTag(productModel.getArrRelatedPrds().get(i));
//                llRelatedPrd.setId(R.id.llRecommendedCategories);
//                llRelatedPrd.setOnClickListener(this);
//            }
//        }
        }
    }

    private void setDetails(TestProductModel testProductModel) {
        this.primaryImage = testProductModel.getPrimaryImage();
        tvProduct.setText(testProductModel.getProductTitle());
        double mrp = 0;
        try {
            mrp = Double.parseDouble(testProductModel.getMrp());
        } catch (Exception e) {
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
        setMRP(sb, tvMRP);
        arrProductImgs = new ArrayList<String>();
        arrProductImgs.add(testProductModel.getPrimaryImage() == null ? "" : testProductModel.getPrimaryImage());
        if (arrProductImgs.size() == 0) {
            ivNoImage.setVisibility(View.VISIBLE);
            pager.setVisibility(View.GONE);
        } else {
            ivNoImage.setVisibility(View.GONE);
            pager.setVisibility(View.VISIBLE);
            productsAdapter = new ProductsAdapter(getActivity(), arrProductImgs);
            productsAdapter.setOnImageClickListener(ProductDetailFragment.this);
            pager.setAdapter(productsAdapter);
            pager.setCurrentItem(0);
            circlePageIndicator.setViewPager(pager);
        }

    }

    public void OnProductSKUClicked(Object object, boolean isChild) {

        if (object == null)
            return;

        if (object instanceof String) {
            variant_1_name = (String) object;
            marginToApply = "0";
            unitPriceToApply = "0";

            getImagesDesc();
            final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
            ll.removeAllViews();

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            innerVariantsLayout1 = (LinearLayout) view.findViewById(R.id.inner_variants);
            if (innerVariantsLayout1 != null) {
                innerVariantsLayout1.removeAllViews();

                if (isChild) {
                    slabProductId = parentId;
                    TestProductModel testProductModel = DBHelper.getInstance().getProductById(parentId, true);
                    if (testProductModel != null) {
                        productId = testProductModel.getProductId();
                        slabProductId = productId;
                        selectedProductModel = testProductModel;
                        LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                        slabRatesLayout.removeAllViews();
                        TextView tvEsp = (TextView) view.findViewById(R.id.tvESP);
                        TextView tvInv = (TextView) view.findViewById(R.id.tvInv);
                        TextView tvCfc = (TextView) view.findViewById(R.id.tvcfc);
                        ImageView ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);
                        if (testProductModel.isCashback()) {
                            ivCashback.setVisibility(View.VISIBLE);
                        } else {
                            ivCashback.setVisibility(View.GONE);
                        }
                        if (tvEsp != null)
                            tvEsp.setText("");
                        if (tvInv != null)
                            tvInv.setVisibility(View.GONE);
                        if (tvCfc != null)
                            tvCfc.setVisibility(View.GONE);
                        getSlabRates(productId);
                    }
                } else {
                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, "", "");
                    if (testProductModel != null) {
                        setDetails(testProductModel);
                        isSpecs = true;
                        view.findViewById(R.id.expandable_product_specifications).setVisibility(View.GONE);
                        view.findViewById(R.id.expandable_product_ratings).setVisibility(View.GONE);

                    }

                    String variant2list = DBHelper.getInstance().getVariant2List(parentId, variant_1_name);

                    if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                        if (variant2list != null && variant2list.length() > 0)
                            arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));

                        if (arrVariant2 != null && arrVariant2.size() > 0) {
                            // has inner variants, Create one more variants row
                            View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                            if (innerVariantsView != null) {
                                variantsLayout1 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout1 != null) {
                                    variantsLayout1.removeAllViews();

                                    int selectedPos = 0;
                                    ArrayList<String> arrTemp = new ArrayList<>();
                                    String name = DBHelper.getInstance().getVariantName(productId, variant_1_name, "", "", DBHelper.COL_PRODUCT_VARIANT2);
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "", color = "#FFFFFF";
                                        if (arrVariant2.get(i).split(":").length > 0) {
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        }
                                        if (arrVariant2.get(i).split(":").length > 1) {
                                            color = arrVariant2.get(i).split(":")[1];
                                        }
                                        if (childVariant.equalsIgnoreCase(name))
                                            selectedPos = i;
                                        if (!arrTemp.contains(childVariant)) {
                                            arrTemp.add(childVariant);
                                            LinearLayout childVariantLabel = createChildVariant(childVariant, color, R.id.id_variant_one);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout1.addView(childVariantLabel);
                                        }

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout1.getChildCount() > 0) {
                                        variantsLayout1.getChildAt(selectedPos).performClick();
                                        variantsLayout1.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout1.addView(innerVariantsView);

                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //create packs
                        testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, "", "");
                        if (testProductModel != null) {
                            slabProductId = testProductModel.getProductId();
                            selectedProductModel = testProductModel;
                            LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            TextView tvEsp = (TextView) view.findViewById(R.id.tvESP);
                            TextView tvInv = (TextView) view.findViewById(R.id.tvInv);
                            TextView tvCfc = (TextView) view.findViewById(R.id.tvcfc);
                            ImageView ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);
                            if (testProductModel.isCashback()) {
                                ivCashback.setVisibility(View.VISIBLE);
                            } else {
                                ivCashback.setVisibility(View.GONE);
                            }
                            if (tvEsp != null)
                                tvEsp.setText("");
                            if (tvInv != null)
                                tvInv.setVisibility(View.GONE);
                            if (tvCfc != null)
                                tvCfc.setVisibility(View.GONE);
                            getSlabRates(slabProductId);
                        }
                    }
                }

            }
        }
    }

    private void getImagesDesc() {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "ImagesDec";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);

            try {
                JSONObject obj = new JSONObject();
                obj.put("product_id", productId);
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getImagesDescURL, map,
                        ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.IMAGES_DESC);
                productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getImagesDescURL);

                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestType = "ImagesDec";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void getSlabRates(String productId) {

        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.SLAB_RATES;
            obj.put("product_id", productId);
            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            //to cancel already running request(if running)
            MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.getSlabRatesURL);

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSlabRatesURL, map,
                    ProductDetailFragment.this, ProductDetailFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getSlabRatesURL);

            final FrameLayout fl = (FrameLayout) view.findViewById(R.id.fl_expand_content);
            progress = (ProgressBar) fl.findViewById(R.id.loading);
            tvNoPacks = (TextView) fl.findViewById(R.id.tv_no_packs);
            tvNoPacks.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
//            if (dialog != null)
//                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addProductToWishList(String wishListId) {
//        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
//            if (Networking.isNetworkAvailable(getActivity())) {
//                requestType = "AddWishList";
//                rlAlert.setVisibility(View.GONE);
//                llMain.setVisibility(View.VISIBLE);
//                try {
//                    JSONObject obj = new JSONObject();
//                    obj.put("flag", "1");
//                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
//                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
//                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
//                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
//                    obj.put("buyer_listing_id", wishListId);
//                    obj.put("product_id", productModel.getProductId());
//
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("data", obj.toString());
//
//                    VolleyBackgroundTask addWishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productListOperationsURL, map,
//                            ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.ADDTO_WISHLIST);
//                    addWishListRequest.setRetryPolicy(new DefaultRetryPolicy(
//                            AppURL.initial_time_out,
//                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//                    MyApplication.getInstance().addToRequestQueue(addWishListRequest, AppURL.productListOperationsURL);
//                    if (dialog != null)
//                        dialog.show();
//
////                    HTTPBackgroundTask task = new HTTPBackgroundTask(MainActivity.this, MainActivity.this, PARSER_TYPE.ADDTO_WISHLIST, APIREQUEST_TYPE.HTTP_POST);
////                    task.execute(map);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else {
//                requestType = "AddWishList";
//                rlAlert.setVisibility(View.VISIBLE);
//                llMain.setVisibility(View.GONE);
//            }
//
//        } else {
//            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_addto_wishlist), Toast.LENGTH_SHORT).show();
//        }
    }

    public void OnCartItemAdded(Object model) {
//        if (model == null)
//            return;
//
//        if (model instanceof NewProductModel) {
//            NewProductModel productModel = (NewProductModel) model;
//            this.productModel = productModel;
//            updateToCart(productModel);
////            updateFooter(expandedItemView.findViewById(R.id.footer), model);
//        }
//
//        updateCartCount(MyApplication.getInstance());

        if (model instanceof TestProductModel) {
            TestProductModel productModel = (TestProductModel) model;

            ArrayList<NewPacksModel> packs = arrSlabRates;
            totalQty = 0;
            if (packs != null && packs.size() > 0) {
                for (NewPacksModel packModel : packs) {
                    int qty = packModel.getQty();
                    totalQty += qty;
                }
            }

            if (totalQty <= 0) {
                // show alert
                Utils.showAlertDialog(getActivity(), getString(R.string.please_add_packs));
                return;
            }

            if (totalPrice <= 0) {
                Utils.showAlertDialog(getActivity(), getString(R.string.oops_try_again));
                return;
            }

            /*if (selPackIsSlab && selPackBlockedQty > 0 && totalQty > selPackBlockedQty) {
                Utils.showAlertDialog(getActivity(), getString(R.string.qty_grt_blck));
                return;
            }*/

            checkInventory(slabProductId, totalQty);
//            this.selectedProductModel = productModel;
//            checkInventory(productId, totalQty);

//            updateToCart(productModel);
//            updateFooter(expandedItemView.findViewById(R.id.footer), model);
        }

    }

    private void checkInventory(String productId, int quantity) {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "checkInventory";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("product_id", productId);
                obj.put("quantity", quantity);
                obj.put("is_slab", selPackIsSlab == true ? 1 : 0);
                obj.put("blocked_qty", selPackBlockedQty);
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCart1URL, map, ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.ADD_CART_1);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.addCart1URL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestType = "GetWishList";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    public JSONArray getInnerVariantsArray(ArrayList<NewVariantModel> variants) {
        try {
            for (NewVariantModel skuModel : variants) {
                if (skuModel.getHasInnerVariants() == 1) {
                    ArrayList<NewVariantModel> arrInner = skuModel.getVariantModelArrayList();
                    getInnerVariantsArray(arrInner);
                } else {
                    if (variants != null && variants.size() > 0) {
                        for (NewVariantModel newVariantModel : variants) {
                            JSONObject variantObj = new JSONObject();
                            variantObj.put("variant_id", newVariantModel.getSkuId());

                            ArrayList<NewPacksModel> packs = newVariantModel.getPacksModelArrayList();
                            JSONArray packsArray = new JSONArray();
                            int totalQty = 0;
                            if (packs != null && packs.size() > 0) {
                                for (NewPacksModel packModel : packs) {
                                    int qty = packModel.getQty();
//                                    if (qty > 0) {
                                    JSONObject packObj = new JSONObject();
                                    packObj.put("pack_qty", qty);
//                                    packObj.put("packprice_id", packModel.getProductSlabId());
                                    packsArray.put(packObj);
//                                    }
                                    totalQty += qty;
                                }
                            }
                            variantObj.put("total_qty", totalQty);
                            variantObj.put("total_price", newVariantModel.getTotalPrice());
                            variantObj.put("applied_margin", newVariantModel.getAppliedMargin());
                            variantObj.put("unit_price", newVariantModel.getAppliedMRP());
                            variantObj.put("packs", packsArray);
                            if (totalQty > 0) {
                                variantsJSONArray.put(variantObj);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return variantsJSONArray;
    }

    private void updateToCart(NewProductModel productModel) {
        variantsJSONArray = new JSONArray();
        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "AddCart";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONArray productsArray = new JSONArray();

                JSONObject productObj = new JSONObject();
                productObj.put("product_id", productModel.getProductId());
                ArrayList<NewVariantModel> variants = productModel.getVariantModelArrayList();
                if (variants != null && variants.size() > 0) {
                    variantsJSONArray = getInnerVariantsArray(variants);
                }
                productObj.put("variants", variantsJSONArray);
                if (variantsJSONArray != null && variantsJSONArray.length() > 0) {
                    productsArray.put(productObj);
                }


                if (productsArray.length() > 0) {
                    JSONObject dataObj = new JSONObject();
                    dataObj.put("products", productsArray);
                    dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    dataObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    dataObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    dataObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    Log.e("Cart Obj", dataObj.toString());

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", dataObj.toString());

                    VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCartURL, map, ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.ADD_TO_CART);
                    viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.addCartURL);
                    if (dialog != null)
                        dialog.show();

//                    HTTPBackgroundTask cartTask = new HTTPBackgroundTask(MainActivity.this, MainActivity.this, PARSER_TYPE.ADD_TO_CART, APIREQUEST_TYPE.HTTP_POST);
//                    cartTask.execute(paramsMap);
                } else {
                    Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.please_add_packs));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            requestType = "AddCart";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void updateCartCount(MyApplication application) {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        ((ProductDetailsActivity) getActivity()).updateCart(cartCount);
        if (tvBadge != null) {
            tvBadge.setText(String.valueOf(this.cartCount));
        }
    }

    private void updateFooter(View footerView, NewPacksModel newPacksModel) {
        if (footerView == null) {
            return;
        }
        if (newPacksModel == null)
            return;

        int totalQty = 0;
        marginToApply = "0";
        unitPriceToApply = "0";
        for (NewPacksModel skuPacksModel : arrSlabRates) {
            totalQty = totalQty + skuPacksModel.getQty();
        }

        for (NewPacksModel skuPacksmodel : arrSlabRates) {
            int packQty = Integer.parseInt(skuPacksmodel.getPackSize());
            int esu = skuPacksmodel.getSlabEsu();
            if (totalQty >= packQty * esu) {
                marginToApply = skuPacksmodel.getMargin();
                unitPriceToApply = skuPacksmodel.getUnitPrice();
                selPackBlockedQty = skuPacksmodel.getBlockedQty();
                selPackPromotionId = skuPacksmodel.getPrmtDetId();
                selPackIsSlab = skuPacksmodel.isSlab();
                selProductSlabId = skuPacksmodel.getProductSlabId();
                selectedPackModel = skuPacksmodel;

                freebieProductId = skuPacksmodel.getFreebieProductId();
                freebieMpq = skuPacksmodel.getFreebieMpq();
                //                freebieQty = String.valueOf((totalQty / packQty) * skuPacksmodel.getFreebieQty());
                double _qty = 0;
                if (freebieMpq > 0) {
                    _qty = (int) (totalQty / freebieMpq) * skuPacksmodel.getFreebieQty();
                }
                freebieQty = String.valueOf((int) _qty);
            }
        }

        double unitPrice = 0;
        if (!TextUtils.isEmpty(unitPriceToApply)) {
            unitPrice = Double.parseDouble(unitPriceToApply);
        }

        double margin = 0;
        if (!TextUtils.isEmpty(marginToApply)) {
            margin = Double.parseDouble(marginToApply);
        }

//            skuModel.setAppliedMargin(marginToApply);
//            skuModel.setAppliedMRP(unitPriceToApply);

        TextView tvTotalQty = (TextView) footerView.findViewById(R.id.tvTotalQty);
        TextView tvMargin = (TextView) footerView.findViewById(R.id.tvTotalMargin);
        TextView tvTotalPrice = (TextView) footerView.findViewById(R.id.tvTotalPrice);
        if (tvTotalQty != null) {
//                tvTotalQty.setText("TOTAL QTY\n" + totalQty);
            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.qty) + "\n" + totalQty);
            setSpannedText(sb, tvTotalQty, 3);
        }

        if (tvMargin != null) {
//                tvMargin.setText("MARGIN\n" + String.format(Locale.CANADA, "%.2f", margin));
            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.markup_margin) + "\n" + String.format(Locale.CANADA, "%.2f", margin) + "%");
            setSpannedText(sb, tvMargin, 8);
        }
        totalPrice = unitPrice * totalQty;
//            skuModel.setTotalPrice(String.format(Locale.CANADA, "%.2f", totalPrice));
        if (tvTotalPrice != null) {
//                tvTotalPrice.setText("TOTAL PRICE\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            setSpannedText(sb, tvTotalPrice, 11);

        } else {
            return;
        }

        offerEcashView = view.findViewById(R.id.ll_cashback);
        tvFreebieDesc = (TextView) view.findViewById(R.id.tvFreebieDesc);
        offerDescView = view.findViewById(R.id.ll_freebie_desc);
        btnOfferImage = (Button) view.findViewById(R.id.iv_offer_desc);
        if (selectedPackModel != null && selectedPackModel.getFreebieDesc() != null &&
                !TextUtils.isEmpty(selectedPackModel.getFreebieDesc()) && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("null") && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("0")) {
            tvFreebieDesc.setText(selectedPackModel.getFreebieDesc());
            offerDescView.setVisibility(View.VISIBLE);
            packType = testProductModel.getPackType();
            setOfferBanner(selectedProductModel.getPackType(), btnOfferImage);
        } else
            offerDescView.setVisibility(View.GONE);

        if (selectedPackModel != null && selectedPackModel.getArrCashBackDetails() != null && selectedPackModel.getArrCashBackDetails().size() > 0) {
            int count = selectedPackModel.getArrCashBackDetails().size();
            llCashBack = (LinearLayout) view.findViewById(R.id.ll_cash_back);
            llCashBack.removeAllViews();

            for (int i = 0; i < count; i++) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View v = inflater.inflate(R.layout.cashback_dashborad, null);
                offerEcashView = v.findViewById(R.id.ll_cashback);
                tvEcashDesc = (TextView) v.findViewById(R.id.tv_ecash);

                rupee = (Button) v.findViewById(R.id.btn_rupee);

                int cbktyp = selectedPackModel.getArrCashBackDetails().get(i).getCbk_source_type();
                int bentype = selectedPackModel.getArrCashBackDetails().get(i).getBenificiary_type();

                if (!isFF){
                    if (bentype == ConstantValues.CUSTOMER) {
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(R.drawable.cashback_bg_retailer);
                        tvEcashDesc.setTextColor(getResources().getColor(R.color.ecash_desc));
                        rupee.setBackgroundResource(R.drawable.ic_rupee);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                    }
                }else if (isFF && isSalesAgent){
                    if (bentype == ConstantValues.SALES_AGENT || bentype == ConstantValues.CUSTOMER){
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                        tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                        rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                    }
                }else if (isFF){
                    if (bentype == ConstantValues.FF_ASSOCIATE || bentype == ConstantValues.FF_MANAGER || bentype == ConstantValues.CUSTOMER){
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                        tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                        rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                    }
                }else {
                    tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                    tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                    tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                    rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                    offerEcashView.setVisibility(View.VISIBLE);
                    rupee.setVisibility(View.VISIBLE);
                    llCashBack.addView(v);
                }



                /*if (!isFF) {
                    if (bentype == ConstantValues.CUSTOMER) {
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(R.drawable.cashback_bg_retailer);
                        tvEcashDesc.setTextColor(getResources().getColor(R.color.ecash_desc));
                        rupee.setBackgroundResource(R.drawable.ic_rupee);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);

                    }
                } else {
                   // if (bentype == ConstantValues.CUSTOMER || bentype == ConstantValues.FF_MANAGER || bentype == ConstantValues.FF_ASSOCIATE) {
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                        tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                        rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                   // }
                }*/


            }

        } else {
            View v = inflater.inflate(R.layout.cashback_dashborad, null);
            v.setVisibility(View.GONE);
            offerEcashView = v.findViewById(R.id.ll_cashback);
            offerEcashView.setVisibility(View.GONE);
        }
    }

    private void setSpannedText(SpannableStringBuilder sb, TextView textView, int start) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getActivity().getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new RelativeSizeSpan(1.2f), start, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(sb);
    }

//    @Override
//    public void onError(String errorCode, PARSER_TYPE requestType) {
//        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
//            if (requestType == PARSER_TYPE.ADD_REVIEW) {
//                this.requestType = "AddReview";
//            } else if (requestType == PARSER_TYPE.WISHLIST) {
//                this.requestType = "WishList";
//            } else if (requestType == PARSER_TYPE.ADDTO_WISHLIST) {
//                this.requestType = "AddWishList";
//            } else if (requestType == PARSER_TYPE.GET_PRODUCT_DETAIL) {
//                this.requestType = "ProductDetail";
//            } else if (requestType == PARSER_TYPE.ADD_TO_CART) {
//                this.requestType = "AddCart";
//            }
//            rlAlert.setVisibility(View.VISIBLE);
//            llMain.setVisibility(View.GONE);
//        }
//    }

//    @Override
//    public void onPackItemAdd(SKUPacksModel model, Object object, int position, TextView tv) {
//        int qty = model.getQty();
//        int packSize = Integer.parseInt(model.getPackSize());
//        qty += packSize;
//        tv.setText(qty + "");
//        model.setQty(qty);
//    }
//
//    @Override
//    public void onPackItemDelete(SKUPacksModel model, Object object, int position, TextView tv) {
//        int qty = model.getQty();
//        int packSize = Integer.parseInt(model.getPackSize());
//        if (qty > 0)
//            qty -= packSize;
//        tv.setText(qty + "");
//        model.setQty(qty);
//    }

    private void setMRP(SpannableStringBuilder sb, TextView textView) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    private void createPacksForVariant(final NewPacksModel skuPacksModel, final LinearLayout linearLayout) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (skuPacksModel != null) {
            View packView = inflater.inflate(R.layout.row_packs_header, null);

            TextView tvPackOf = (TextView) packView.findViewById(R.id.tvPackOf);
            TextView tvPackPrice = (TextView) packView.findViewById(R.id.tvPackPrice);
            TextView tvUnitPrice = (TextView) packView.findViewById(R.id.tvUnitPrice);
            TextView tvMargin = (TextView) packView.findViewById(R.id.tvMargin);

            tvPackOf.setText(String.valueOf(skuPacksModel.getPackSize() == null ? "--" : skuPacksModel.getPackSize()));

            double packPrice = 0;
            try {
                packPrice = (Double.parseDouble(skuPacksModel.getPackSize()) * Double.parseDouble(skuPacksModel.getUnitPrice()));
            } catch (Exception e) {
            }
            tvPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

            double unitPrice = 0;
            try {
                unitPrice = Double.parseDouble(skuPacksModel.getUnitPrice());
            } catch (Exception e) {
            }
            tvUnitPrice.setText(skuPacksModel.getUnitPrice() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", unitPrice));

            double margin = 0;
            try {
                margin = Double.parseDouble(skuPacksModel.getMargin());
            } catch (Exception e) {
            }
            tvMargin.setText(skuPacksModel.getMargin() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", margin));

            final TextView tvQty = (TextView) packView.findViewById(R.id.tvQuantity);
            tvQty.setText(skuPacksModel.getEsuQty() + "");
            ImageButton ibMinus = (ImageButton) packView.findViewById(R.id.ibMinus);
            ImageButton ibPlus = (ImageButton) packView.findViewById(R.id.ibPlus);

            ibPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_add_packs), Toast.LENGTH_LONG).show();
                        return;
                    }

//                    String availableQty = model.getAvailableQuantity();
                    String availableQty = "100"; //temp
                    if (availableQty != null && !TextUtils.isEmpty(availableQty)) {
                        int variantQuantity = 0;
                        try {
                            variantQuantity = Integer.parseInt(availableQty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (variantQuantity > 0) {
                            int qty = skuPacksModel.getQty();
                            int esuQty = skuPacksModel.getEsuQty();

                            int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                    0 : Integer.parseInt(skuPacksModel.getPackSize());
                            int slabEsu = skuPacksModel.getSlabEsu() == 0 ? 1 : skuPacksModel.getSlabEsu();
                            int updatedQty = qty + (packSize * slabEsu);
                            esuQty = esuQty + 1;
                            tvQty.setText("" + esuQty);
                            skuPacksModel.setQty(updatedQty);
                            skuPacksModel.setEsuQty(esuQty);

                            updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                        } else {
                            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.quantity_not_available));
                        }
                    }
                }
            });

            ibMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_add_packs), Toast.LENGTH_LONG).show();
                        return;
                    }

                    int qty = skuPacksModel.getQty();
                    int esuQty = skuPacksModel.getEsuQty();

                    if (esuQty <= 0) {
                        tvQty.setText("0");
                    } else {
                        int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                0 : Integer.parseInt(skuPacksModel.getPackSize());
                        int slabEsu = skuPacksModel.getSlabEsu() == 0 ? 1 : skuPacksModel.getSlabEsu();
                        int updatedQty = qty - (packSize * slabEsu);
                        esuQty = esuQty - 1;
                        if (esuQty < 0) {
                            tvQty.setText("0");
                        } else {
                            tvQty.setText("" + esuQty);
                        }
                        skuPacksModel.setQty(updatedQty);
                        skuPacksModel.setEsuQty(esuQty);
                    }
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                }
            });

            linearLayout.addView(packView);

        }

        View footerView = inflater.inflate(R.layout.row_packs_footer, null);
        linearLayout.addView(footerView);

        footerView.findViewById(R.id.tvAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_shop), Toast.LENGTH_LONG).show();
                    return;
                } else if (mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "").equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""))) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_select_retailer), Toast.LENGTH_SHORT).show();
                    return;
                } else if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.PLACE_ORDER_FEATURE_CODE) > 0) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.you_are_not_allowed), Toast.LENGTH_LONG).show();
                    return;
                }

                OnCartItemAdded(testProductModel);

            }
        });
        updateFooter(footerView, skuPacksModel);
    }

    private LinearLayout createChildVariant(String text, String color, int id) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        LinearLayout skuView = new LinearLayout(getActivity());
        skuView.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params1.setMargins(0, 0, 20, 0);
        skuView.setLayoutParams(params1);

        ImageView imageView = new ImageView(getActivity());
        Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_star);

        Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
        Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(color));
        imageView.setImageBitmap(mFinalBitmap);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params2.gravity = Gravity.CENTER_VERTICAL;
        params2.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(params2);

        TextView textView = new TextView(getActivity());
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.gravity = Gravity.CENTER_VERTICAL;
        params.setMargins(5, 0, 0, 0);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        skuView.setId(id);
        skuView.setTag(text);
        textView.setText(text == null ? "" : text);

        skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
        skuView.setPadding(10, 5, 10, 5);
        skuView.addView(imageView);
        skuView.addView(textView);

        return skuView;
    }

//    public void onBackPressed() {
//        Intent intent = getActivity().getIntent();
//        intent.putExtra("position", position);
//        if (rating != null)
//            intent.putExtra("rating", rating);
//        getActivity().setResult(Activity.RESULT_OK, intent);
//        getActivity().finish();
//    }

    public void exportDatabase(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + getActivity().getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "Factail.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    public void setOfferBanner(String packType, View view) {

        if (view == null) {
            return;
        }
        Button ivOffer = (Button) view;

        switch (packType) {

            case "Consumer Pack":
            case "CONSUMER PACK":
            case "CP Inside":
            case "CP Outside":
            case "Consumer Pack Inside":
            case "Consumer Pack Outside":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("CP Offer");
                break;
            case "Freebie":
            case "FREEBIE":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Freebie");
                break;
            default:
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Offer");
                break;
        }
    }

    private void getFreebiePacks(String productId) {
        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.FREEBIE_SLABS;
            obj.put("product_id", productId);
            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            //to cancel already running request(if running)
            MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.getSlabRatesURL);

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSlabRatesURL, map,
                    ProductDetailFragment.this, ProductDetailFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getSlabRatesURL);

            if (dialog != null)
                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onImageClicked(ArrayList<String> arrImages, int position) {
        String productName = tvProduct.getText().toString();
        Intent intent = new Intent(getActivity(), FullImagePreviewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("Images", arrImages);
        bundle.putInt("position", position);
        bundle.putString("productName", productName);
        intent.putExtra("ArrayImages", bundle);
        startActivity(intent);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null)
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, final String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (requestType == PARSER_TYPE.GET_PRODUCT_DETAIL) {
//                if (results instanceof NewProductModel) {
//                    if (status.equalsIgnoreCase("success")) {
//                        productModel = (NewProductModel) results;
//                        if (productModel != null) {
//                            setProductDetails();
//                        }
//                    } else {
//                        Utils.showAlertWithMessage(getActivity(), message);
//                    }
//                }
            } else if (requestType == PARSER_TYPE.OFFLINE_PRODUCTS) {
                if (status.equalsIgnoreCase("success")) {
                    if (results instanceof ArrayList) {
                        testProductModel = DBHelper.getInstance().getProductById(parentId, isChild);
                        if (testProductModel != null)
                            setProductDetails();
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } else if (requestType == PARSER_TYPE.ADD_TO_CART) {
                if (status.equalsIgnoreCase("success")) {
                    if (results instanceof Integer) {
//                        ArrayList<String> cartIds = (ArrayList<String>) results;
//                        if (cartIds != null && cartIds.size() > 0) {
//                            String cartId = cartIds.get(0);
//                            if (productModel != null) {
//                                productModel.setCartId(cartId);
//                            }
//
////                            MyApplication.getInstance().addToCart(productModel, mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
//                            getCartCount();
//                            Toast.makeText(getActivity(), "Added to cart", Toast.LENGTH_LONG).show();
//                        }
                        cartCount = (int) results;
                        MyApplication.getInstance().setCartCount(cartCount);
                        updateCartCount();
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.product_added_cart), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }

            } else if (requestType == PARSER_TYPE.ADD_CART_1) {
                if (results instanceof AddCart1Response) {
                    AddCart1Response addCart1Response = (AddCart1Response) results;
                    if (addCart1Response.isStatus()) {
                        cartModel = new CartModel();

                        cartModel.setProductId(slabProductId);
                        cartModel.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                        cartModel.setQuantity(totalQty);
                        cartModel.setUnitPrice(unitPriceToApply);
                        cartModel.setMargin(marginToApply);
                        cartModel.setTotalPrice(totalPrice);
                        cartModel.setWarehouseId("");//todo
                        cartModel.setDiscount("");//todo
                        cartModel.setPackType(packType);
                        cartModel.setIsChild(isChild == true ? 1 : 0);
                        cartModel.setIsSlab(selPackIsSlab == true ? 1 : 0);
                        cartModel.setBlockedQty(selPackBlockedQty);
                        cartModel.setStar(selectedProductModel.getStar());
                        cartModel.setPrmtDetId(selPackPromotionId);
                        cartModel.setProductSlabId(selProductSlabId);
                        cartModel.setPackStar(selectedPackModel.getStar());

                        ArrayList<ProductSlabData> arrProductSlabData = new ArrayList<>();
                        int qty = 0, totalQty = this.totalQty;
                        double cashBackAmount = 0;
                        String retailerCbIds = "", ffManagerCbIds = "", ffAssCbIds = "";
                        for (int i = arrSlabRates.size() - 1; i >= 0; i--) {
                            NewPacksModel newPacksModel = arrSlabRates.get(i);
                            int packQty = 0, slabEsu = newPacksModel.getSlabEsu();
                            try {
                                packQty = Integer.parseInt(newPacksModel.getPackSize());
                                packQty = packQty * slabEsu;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (totalQty >= packQty) {
                                ProductSlabData productSlabData = new ProductSlabData();
                                qty = totalQty / packQty;
                                if (selectedPackModel.getPackLevel() == newPacksModel.getPackLevel()) {

                                    ArrayList<String> arrCashBackIds = new ArrayList<>();

                                    if (newPacksModel.getArrCashBackDetails() != null && newPacksModel.getArrCashBackDetails().size() > 0) {
                                        for (int j = 0; j < newPacksModel.getArrCashBackDetails().size(); j++) {
                                            CashBackDetailsModel cashBackDetailsModel = newPacksModel.getArrCashBackDetails().get(j);
                                            double torange = cashBackDetailsModel.getFf_qty_to_range();
                                            if (totalQty >= torange) {
                                                if (cashBackDetailsModel.getCbk_source_type() == ConstantValues.PRODUCT) {

                                                    if (!isFF) {
                                                        if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER && cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                            retailerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                        }

                                                    } else {
                                                        if (cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                            if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER) {

                                                                retailerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                            } else if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.FF_MANAGER) {
                                                                ffManagerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                            } else if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.FF_ASSOCIATE) {
                                                                ffAssCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                            }
                                                        }

                                                    }

                                                    if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER && cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                        double price = 0;
                                                        try {
                                                            price = Double.parseDouble(newPacksModel.getUnitPrice());
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }

                                                        if (cashBackDetailsModel.getCashback_type() == ConstantValues.CASHBACK_TYPE_VALUE) {
                                                            cashBackAmount = cashBackDetailsModel.getCbk_value();
                                                        } else {
                                                            cashBackAmount = (totalQty * price * cashBackDetailsModel.getCbk_value()) / 100;
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!TextUtils.isEmpty(retailerCbIds))
                                        arrCashBackIds.add(retailerCbIds);
                                    if (!TextUtils.isEmpty(ffAssCbIds))
                                        arrCashBackIds.add(ffAssCbIds);
                                    if (!TextUtils.isEmpty(ffManagerCbIds))
                                        arrCashBackIds.add(ffManagerCbIds);

                                    String comma_cashBackIds = TextUtils.join(",", arrCashBackIds);
                                    productSlabData.setCashBackIds(comma_cashBackIds);

                                }
                                productSlabData.setLevel(newPacksModel.getPackLevel());
                                productSlabData.setEsu(newPacksModel.getSlabEsu());
                                productSlabData.setQty(qty);
                                productSlabData.setPackQty(qty * packQty);
                                productSlabData.setPackSize(packQty / newPacksModel.getSlabEsu());
                                productSlabData.setStar(newPacksModel.getStar());
                                productSlabData.setProductId(productId);
                                productSlabData.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                arrProductSlabData.add(productSlabData);
                                totalQty = totalQty % packQty;  // need to cal using another variable
                                i++;
                                continue;
                            }
                        }

                        cartModel.setCashbackAmount(cashBackAmount);
                        cartModel.setArrProductSlabData(arrProductSlabData); // set slabs data for product
                        int _productEsu = selectedPackModel.getSlabEsu();
                        int esu = 1, packQty = 0;
                        if (_productEsu != 0) {
                            try {
                                packQty = Integer.parseInt(selectedPackModel.getPackSize());
                                esu = this.totalQty / (_productEsu * packQty);
                            } catch (Exception e) {
                                e.printStackTrace();
                                esu = 1;
                            }
                        }
                        cartModel.setEsu(String.valueOf(esu));

                        /*String _productEsu = selectedProductModel.getEsu();
                        int esu = 1;
                        if (_productEsu != null && !_productEsu.equals("0") && !TextUtils.isEmpty(_productEsu)) {
                            try {
                                int tempEsu = Integer.parseInt(_productEsu);
                                esu = totalQty / tempEsu;
                            } catch (Exception e) {
                                e.printStackTrace();
                                esu = 1;
                            }
                        }

                        cartModel.setEsu(String.valueOf(esu));*/

                        cartModel.setSlabEsu(selectedProductModel.getEsu());
                        cartModel.setParentId(slabProductId);

                        //check for freebie
                        if (freebieProductId != null && freebieQty != null && !freebieQty.equals("0") &&
                                this.totalQty >= freebieMpq) {
                            cartModel.setFreebieQty(freebieQty);
                        }

                        cartModel.setFreebieProductId(freebieProductId);

                        TestProductModel freebieModel = DBHelper.getInstance().getFreebieData(freebieProductId);
                        if (freebieModel != null) {
                            cartModel.setFreebieStar(freebieModel.getStar().isEmpty() ? selectedProductModel.getStar() : freebieModel.getStar());
                            cartModel.setFreebieEsu(freebieModel.getEsu().isEmpty() ? selectedProductModel.getEsu() : freebieModel.getEsu());
                        }
                        cartModel.setFreebieMpq(selectedPackModel.getFreebieMpq());
                        cartModel.setFreebieFq(selectedPackModel.getFreebieQty());

                        /*if (freebieProductId != null && !freebieProductId.equals("0") && !freebieQty.equals("0") && this.totalQty >= freebieMpq) {
                            getFreebiePacks(freebieProductId);
                        } else {*/

                        long rowId = Utils.insertOrUpdateCart(cartModel);
                        if (rowId == -1) {
                            showToast("Failed to insert in Cart");
                        } else if (rowId == -2) {
                            showToast("Failed to insert Freebie in Cart.");
                        } else if (rowId == -3) {
                            showToast("Failed to insert. Please try again");
                        }

                        updateCartCount();
//                        }

                        if (freebieProductId != null && !freebieProductId.equals("0") && DBHelper.getInstance().getProductById(freebieProductId, false) == null) {
                            isFreebie = true;
                            getProducts(freebieProductId, isFreebie);
                        }

                    } else {
                        Utils.showAlertWithMessage(getActivity(), String.format(getString(R.string.unavailable_inventory_product), addCart1Response.getAvailableQty()));
                    }
                } else {

                }
            } else if (requestType == PARSER_TYPE.FREEBIE_SLABS) {
                if (results instanceof ArrayList) {

                    ArrayList<NewPacksModel> arrFreebieSlabs = (ArrayList<NewPacksModel>) results;
                    arrFreebieSlabData = new ArrayList<>();
                    if (arrFreebieSlabs != null && arrFreebieSlabs.size() > 0) {
                        for (int i = 0; i < arrFreebieSlabs.size(); i++) {
                            NewPacksModel newPacksModel = arrFreebieSlabs.get(i);
                            int freePackQty = 0, freebie = 0, freeQty = 0, freeSlabEsu = newPacksModel.getSlabEsu();
                            try {
                                freebie = Integer.parseInt(freebieQty);
                                freePackQty = Integer.parseInt(newPacksModel.getPackSize());
                                freePackQty = freePackQty * freeSlabEsu;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (freebie >= freePackQty) {
                                ProductSlabData productSlabData = new ProductSlabData();
                                freeQty = freebie / freePackQty;
                                productSlabData.setLevel(newPacksModel.getPackLevel());
                                productSlabData.setEsu(newPacksModel.getSlabEsu());
                                productSlabData.setQty(freeQty);
                                productSlabData.setStar(newPacksModel.getStar());
                                productSlabData.setProductId(freebieProductId);
                                productSlabData.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                arrFreebieSlabData.add(productSlabData);
                                freebie = freebie % freePackQty;
                                i++;
                                continue;
                            }
                        }
                        cartModel.setArrFreebieSlabData(arrFreebieSlabData);
                    }

                    long rowId = Utils.insertOrUpdateCart(cartModel);
                    if (rowId == -1) {
                        showToast("Failed to insert in Cart");
                    } else if (rowId == -2) {
                        showToast("Failed to insert Freebie in Cart.");
                    } else if (rowId == -3) {
                        showToast("Failed to insert. Please try again");
                    }

                    updateCartCount();
                }

            } else if (requestType == PARSER_TYPE.CHECK_INVENTORY) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                if (results instanceof AddCart1Response) {
                    AddCart1Response addCart1Response = (AddCart1Response) results;
                    String _message = String.format(getString(R.string.available_inventory_products), addCart1Response.getAvailableQty());

                    if (!addCart1Response.isStatus())
                        _message = getString(R.string.check_inventory_unavailable);

                    new AlertDialog.Builder(getActivity())
                            .setTitle(getString(R.string.app_name))
                            .setMessage(_message)
                            .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //dismiss
                                }
                            })
                            .show();
                } else {
                    showToast(getString(R.string.oops));
                }
            } else if (requestType == PARSER_TYPE.IMAGES_DESC) {
                if (results instanceof TestProductModel) {
                    TestProductModel testProductModel = (TestProductModel) results;
                    if (testProductModel != null) {
                        tvDescription.setText(testProductModel.getDescription());
                        arrProductImgs = testProductModel.getArrImages();
                        arrProductImgs.add(0, this.primaryImage);
                        if (arrProductImgs.size() == 0) {
                            ivNoImage.setVisibility(View.VISIBLE);
                            pager.setVisibility(View.GONE);
                        } else {
                            ivNoImage.setVisibility(View.GONE);
                            pager.setVisibility(View.VISIBLE);
                            productsAdapter = new ProductsAdapter(getActivity(), arrProductImgs);
                            productsAdapter.setOnImageClickListener(ProductDetailFragment.this);
                            pager.setAdapter(productsAdapter);
                            pager.setCurrentItem(0);
                            circlePageIndicator.setViewPager(pager);
                        }
                    }
                }
            } else if (requestType == PARSER_TYPE.REVIEW_SPEC) {
                if (results instanceof TestProductModel) {
                    TestProductModel testProductModel = (TestProductModel) results;
                    if (testProductModel != null) {
                        isSpecs = false;
                        arrSpecs = testProductModel.getArrSpecs();
                        if (arrSpecs == null && arrSpecs.size() == 0) {
                            tvNoSpec.setVisibility(View.VISIBLE);
                        } else {
                            llSpecifications.removeAllViews();
                            tvNoSpec.setVisibility(View.GONE);
                            for (int j = 0; j < arrSpecs.size(); j++) {
                                View view = getActivity().getLayoutInflater().inflate(R.layout.row_specifications, null);
                                tvSpecName = (TextView) view.findViewById(R.id.tv_spec_name);
                                tvSpecValue = (TextView) view.findViewById(R.id.tv_spec_value);
                                tvSpecName.setText(arrSpecs.get(j).getSpecName());
                                tvSpecValue.setText(arrSpecs.get(j).getSpecValue());
                                llSpecifications.addView(view);
                            }
                        }
                        arrReviews = testProductModel.getArrReviews();
                        if (arrReviews != null && arrReviews.size() > 0) {
                            for (int i = 0; i < arrReviews.size(); i++) {
                                ReviewModel reviewModel = arrReviews.get(i);
                                LinearLayout llReview = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_review, null);
                                TextView tvAuthor = (TextView) llReview.findViewById(R.id.tv_author_name);
                                TextView tvReview = (TextView) llReview.findViewById(R.id.tv_review);
                                RatingBar ratingBar = (RatingBar) llReview.findViewById(R.id.rating);
                                tvAuthor.setText(reviewModel.getAuthor());
                                tvReview.setText(reviewModel.getReview());
                                ratingBar.setRating(Float.parseFloat(reviewModel.getRating()));

                                llPreviousReview.addView(llReview);
                            }
                        }
                    }
                }
            } else if (requestType == PARSER_TYPE.SLAB_RATES) {

                if (results instanceof ArrayList) {
                    if (progress != null)
                        progress.setVisibility(View.GONE);

                    arrSlabRates = (ArrayList<NewPacksModel>) results;

                    LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                    slabRatesLayout.removeAllViews();
                    if (arrSlabRates != null && arrSlabRates.size() > 0) {
                        tvNoPacks.setVisibility(View.GONE);
                        View slabRatesView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                        slabsLayout = (LinearLayout) slabRatesView.findViewById(R.id.variants_layout);
                        if (slabsLayout != null) {
                            slabsLayout.removeAllViews();

                            int selectedPos = 0;
                            for (int i = 0; i < arrSlabRates.size(); i++) {
                                selectedPackModel = arrSlabRates.get(i);
                                String slab = arrSlabRates.get(i).getPackSize();
                                String star = arrSlabRates.get(i).getStar();
                                String slabColor = selectedProductModel.getStar();
                                int slabEsu = selectedPackModel.getSlabEsu();
                                CustomerTyepModel customerTyepModel = DBHelper.getInstance().getStarColorName(star);
                                if (customerTyepModel != null)
                                    slabColor = customerTyepModel.getDescription();
                                LinearLayout childVariantLabel = createSlabRates(slab, slabColor, slabEsu, R.id.id_slab_qty);
                                childVariantLabel.setTag(arrSlabRates.get(i));
                                childVariantLabel.setOnClickListener(onVariantClickListener);
                                slabsLayout.addView(childVariantLabel);

                                if (selectedPackModel.isSelected())
                                    selectedPos = i;

                            }

                            if (slabsLayout.getChildCount() > 0) {
                                slabsLayout.getChildAt(selectedPos).performClick();
                                slabsLayout.getChildAt(selectedPos).setSelected(true);
                            }

                            slabRatesLayout.addView(slabRatesView);

                        }
                    } else {
                        if (tvNoPacks != null)
                            tvNoPacks.setVisibility(View.VISIBLE);
                    }
                }

            } else if (requestType == PARSER_TYPE.CART_COUNT) {
                if (results instanceof String) {
                    try {
                        cartCount = Integer.parseInt((String) results);
                    } catch (Exception e) {
                        cartCount = 0;
                        e.printStackTrace();
                    }
                }
                MyApplication.getInstance().setCartCount(cartCount);
                updateCartCount();
            } else if (requestType == PARSER_TYPE.WISHLIST) {

                if (results instanceof ArrayList) {
                    wishListArray = (ArrayList<WishListModel>) results;
                    if (wishListArray != null && wishListArray.size() > 0) {
                        showWishListDialog();
                    } else {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_create_list), Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (requestType == PARSER_TYPE.ADDTO_WISHLIST) {
                Utils.showAlertDialog(getActivity(), message);
            } else if (requestType == PARSER_TYPE.ADD_REVIEW) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setMessage(message);
                dialog.setTitle(getActivity().getString(R.string.app_name));
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (status.equalsIgnoreCase("success")) {
                            etReview.setText("");
                            rbRating.setRating(0);
                        }

                        dialog.dismiss();
                    }
                });
                AlertDialog alert = dialog.create();
                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }

        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private LinearLayout createSlabRates(final String text, String slabColor, int slabEsu, int id) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        LinearLayout skuView = new LinearLayout(getActivity());
        skuView.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params1.setMargins(0, 0, 20, 0);
        skuView.setLayoutParams(params1);

        ImageView imageView = new ImageView(getActivity());
        Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_star);

        Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
        Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(slabColor));
        imageView.setImageBitmap(mFinalBitmap);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params2.gravity = Gravity.CENTER_VERTICAL;
        params2.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(params2);

        TextView textView = new TextView(getActivity());
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.gravity = Gravity.CENTER_VERTICAL;

        textView.setTypeface(Typeface.DEFAULT_BOLD);
        params.setMargins(10, 0, 5, 0);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        skuView.setId(id);
        skuView.setTag(text);
        if (slabEsu > 1)
            textView.setText(text == null ? "" : text + " x " + slabEsu);
        else
            textView.setText(text == null ? "" : text);

        skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
        skuView.setPadding(10, 5, 10, 5);
        skuView.addView(imageView);
        skuView.addView(textView);

        return skuView;
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartCount();
        boolean isDayChanged = MyApplication.getInstance().isDayChanged();
        if (isDayChanged) {
            MyApplication.getInstance().setDayChanged(false);
            testProductModel = DBHelper.getInstance().getProductById(parentId, isChild);
            if (testProductModel != null) {
                setProductDetails();
            } else {
                getProduct();
            }
        }
        if (tvProduct.getText().toString() != null)
            mTracker.setScreenName("Details of" + tvProduct.getText().toString());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("")
                .setAction("")
                .build());

    }

    private void getCartCount() {
        if (mSharedPreferences.getBoolean("IsLoggedIn", false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                requestType = "AddWishList";
                rlAlert.setVisibility(View.GONE);
                llMain.setVisibility(View.VISIBLE);

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    HashMap<String, String> map1 = new HashMap<>();
                    map1.put("data", obj.toString());

                    VolleyBackgroundTask cartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.cartCountURL, map1, ProductDetailFragment.this, ProductDetailFragment.this, PARSER_TYPE.CART_COUNT);
                    cartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(cartRequest, AppURL.cartCountURL);
                    if (dialog != null)
                        dialog.show();


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                requestType = "AddWishList";
                rlAlert.setVisibility(View.VISIBLE);
                llMain.setVisibility(View.GONE);
            }

        }
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
