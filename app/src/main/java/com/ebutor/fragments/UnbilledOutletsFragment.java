package com.ebutor.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.FeedBackActivity;
import com.ebutor.HomeActivity;
import com.ebutor.MyApplication;
import com.ebutor.OrdersListActivity;
import com.ebutor.R;
import com.ebutor.adapters.LocateAutoCompleteAdapter;
import com.ebutor.adapters.UnBilledOutletsAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.clusters.SphericalUtil;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.RetailersModel;
import com.ebutor.services.Locations;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.EditTextWithDeleteButton;
import com.ebutor.utils.GPSTracker;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class UnbilledOutletsFragment extends Fragment implements PermissionCallback, ErrorCallback, Response.ErrorListener, VolleyHandler<Object>, UnBilledOutletsAdapter.OnCheckInListener, UpdateRetailerFragment.OnDialogClosed, OnMapReadyCallback {


    private static final int REQUEST_PERMISSIONS = 20;
    private static View rootView;
    public android.app.AlertDialog.Builder build = null;
    public android.app.AlertDialog alertDialog = null;
    GPSTracker gpsTracker;
    double lan, lon;
    GoogleApiClient googleApiClient;
    LocationRequest request;
    Locations loc;
    double latitude = 0.0;
    double longitude = 0.0;
    LatLng latLng = null;
    public BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            latitude = intent.getDoubleExtra(Locations.EXTRA_LATITUDE, 0);
            longitude = intent.getDoubleExtra(Locations.EXTRA_LONGITUDE, 0);
            latLng = new LatLng(latitude, longitude);
            // getGeocodes(latitude,longitude);
            if (latitude != 0.0 && longitude != 0.0) {
                lan = latitude;
                lon = longitude;
            }
            if (latLng != null) {
                getActivity().stopService(new Intent(context, Locations.class));
            }
        }
    };
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ActionSlideExpandableListView mActionSlideExpandableListView;
    private UnBilledOutletsAdapter unBilledOutletsAdapter;
    private ArrayList<RetailersModel> arrUnbilled, arrMapOutlets, arrListOutlets;
    private EditText etSearch;
    private boolean isFF;
    private RetailersModel selRetailerModel = new RetailersModel();
    BeatFragment.OnSelListener okListener = new BeatFragment.OnSelListener() {

        @Override
        public void onSelected(String beatId) {
            UnbilledOutletsFragment.this.selBeatId = beatId;
            if (TextUtils.isEmpty(beatId)) {
                Toast.makeText(getActivity(), getString(R.string.beat_empty), Toast.LENGTH_SHORT).show();
            } else if (beatId.equalsIgnoreCase("0")) {
                Toast.makeText(getActivity(), getString(R.string.please_select_valid_beat), Toast.LENGTH_SHORT).show();
            } else {
                mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, beatId).apply();
                updateBeat();
            }
        }
    };
    private TextView tvNoItems;
    private LinearLayout llUnbilledOutlets;
    private double currentLat = 0.0, currentLong = 0.0;
    private ImageView ivMap;
    private LinearLayout llOutlets;
    private RelativeLayout rlMap;
    private boolean isMap = false;
    private GoogleMap map;
    private ImageButton btnCross;
    private int currentOffset = 0;
    private boolean isNextCall = true;
    private String beatId, selBeatId = "";
    private ArrayList<CustomerTyepModel> arrBeats;
    private ArrayList<RetailersModel> retailersModelArrayList;
    private int offsetLimit = 50;
    //    private ArrayList<LatLng> markerPoints;
    private AutoCompleteTextView actSearch;
    private LinearLayout llFooter;

    public static UnbilledOutletsFragment newInstance() {
        UnbilledOutletsFragment fragment = new UnbilledOutletsFragment();
        return fragment;
    }

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static boolean isDeviceLocationEnabled(Context mContext) {
        int locMode = 0;
        String locProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locMode = Settings.Secure.getInt(mContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locProviders = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locProviders);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//         = inflater.inflate(R.layout.fragment_unbilled_outlets, container, false);

        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_unbilled_outlets, container, false);
        } catch (InflateException e) {
        }

        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TABS_PROGRESS);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llFooter = (LinearLayout) view.findViewById(R.id.llLoadMore);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        mActionSlideExpandableListView = (ActionSlideExpandableListView) view.findViewById(R.id.unbilled_outlets_list);
        etSearch = (EditText) view.findViewById(R.id.input_search_query);
        tvNoItems = (TextView) view.findViewById(R.id.tv_no_items);
        llUnbilledOutlets = (LinearLayout) view.findViewById(R.id.ll_unbilled_outlets);
        llOutlets = (LinearLayout) view.findViewById(R.id.ll_outlets);
        rlMap = (RelativeLayout) view.findViewById(R.id.rl_map);
        actSearch = (AutoCompleteTextView) view.findViewById(R.id.actSearch);
        ivMap = (ImageView) view.findViewById(R.id.iv_map);
        btnCross = (ImageButton) view.findViewById(R.id.button_cross);
        ivMap.setVisibility(View.VISIBLE);

        build = new android.app.AlertDialog.Builder(getActivity());
        build.setTitle(getString(R.string.app_name));
        build.setMessage(getString(R.string.please_enable_loc));
        build.setPositiveButton(getString(R.string.action_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callGPSSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });

        build.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    return true; // Consumed
                } else {
                    return false; // Not consumed
                }
            }
        });
        alertDialog = build.create();
        alertDialog.setCanceledOnTouchOutside(false);


        reqPermission();

        startServices();

        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        arrUnbilled = new ArrayList<>();
        arrListOutlets = new ArrayList<>();
        arrMapOutlets = new ArrayList<>();
        arrBeats = new ArrayList<>();

        actSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view != null && view.getTag() != null) {
                    RetailersModel retailersModel = (RetailersModel) view.getTag();
                    if (retailersModel != null && map != null) {
                        double lat = 0.0, lon = 0.0;
                        try {
                            lat = Double.parseDouble(retailersModel.getLatitude());
                            lon = Double.parseDouble(retailersModel.getLongitude());
                        } catch (Exception e) {
                            lat = 0.0;
                            lon = 0.0;
                        }

                        CameraUpdate center =
                                CameraUpdateFactory.newLatLng(new LatLng(lat, lon));
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);
                        MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lon)).title(retailersModel.getCompany() + "\n" + retailersModel.getTelephone());

                        map.addMarker(marker).showInfoWindow();
                        map.moveCamera(center);
                        map.animateCamera(zoom);
                    }
                }
            }
        });

        retailersModelArrayList = new ArrayList<>();

        beatId = mSharedPreferences.getString(ConstantValues.KEY_BEAT_ID, "");

        unBilledOutletsAdapter = new UnBilledOutletsAdapter(getActivity(), retailersModelArrayList);
        unBilledOutletsAdapter.setCheckinListener(UnbilledOutletsFragment.this);

        // gpsTracker = new GPSTracker(getActivity());
        // if (!gpsTracker.getIsGPSTrackingEnabled()) {
        //   gpsTracker.showSettingsAlert();

//        }

        /*if(isDeviceLocationEnabled(getActivity())){
            if(alertDialog.isShowing()){
                alertDialog.dismiss();
            }
        }*/

        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actSearch.setText("");
            }
        });

        actSearch.addTextChangedListener(new EditTextWithDeleteButton.TextChangedListener() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    btnCross.setVisibility(View.VISIBLE);
                } else {
                    btnCross.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mActionSlideExpandableListView.setAdapter(unBilledOutletsAdapter);
        mActionSlideExpandableListView.setOnScrollListener(onScrollListener());
        mActionSlideExpandableListView.setItemActionListener(new ActionSlideExpandableListView.OnActionClickListener() {
            @Override
            public void onClick(View itemView, View clickedView, int position) {
                if (clickedView.getId() == R.id.btn_navigation) {
                    Toast.makeText(getActivity(), "navigation", Toast.LENGTH_SHORT).show();
                } else if (clickedView.getId() == R.id.btn_check_in) {
                    Toast.makeText(getActivity(), "check out", Toast.LENGTH_SHORT).show();
                } else if (clickedView.getId() == R.id.btn_total_orders) {
                    Toast.makeText(getActivity(), "total orders", Toast.LENGTH_SHORT).show();
                } else if (clickedView.getId() == R.id.btn_feed_back) {
                    Toast.makeText(getActivity(), "feed back", Toast.LENGTH_SHORT).show();
                } else {

                }
            }
        });

        ivMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMap) {
                    isMap = false;
                    ivMap.setImageResource(R.drawable.ic_map_blue);
                    llOutlets.setVisibility(View.VISIBLE);
                    rlMap.setVisibility(View.GONE);
                    etSearch.setText("");
                } else {
                    isMap = true;
                    ivMap.setImageResource(R.drawable.ic_list_blue);
                    llOutlets.setVisibility(View.GONE);
                    rlMap.setVisibility(View.VISIBLE);
                    actSearch.setText("");
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {

            private final long DELAY = 500;
            private Timer timer = new Timer();

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
//                if (unBilledOutletsAdapter != null) {
//                    String text = etSearch.getText().toString().toLowerCase(Locale.getDefault());
//                    unBilledOutletsAdapter.getFilter().filter(text);
//                }

                currentOffset = 0;
                retailersModelArrayList = new ArrayList<RetailersModel>();
                unBilledOutletsAdapter.removeAllItems();
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() == null)
                                    return;

                                if (Networking.isNetworkAvailable(getActivity())) {
                                    try {
                                        JSONObject obj = new JSONObject();
                                        obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                                        obj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                                        obj.put("beat_id", beatId);
                                        obj.put("search", etSearch.getText().toString());
                                        obj.put("offset", String.valueOf(currentOffset));
                                        obj.put("offset_limit", String.valueOf(offsetLimit));
                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("data", obj.toString());

                                        //to cancel already running request(if running)
                                        MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.getAllRetailersURL);

                                        VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllRetailersURL, map, UnbilledOutletsFragment.this, UnbilledOutletsFragment.this, PARSER_TYPE.GET_ALL_RETAILERS);
                                        viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                        MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getAllRetailersURL);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    getActivity().runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }

                            }
                        }, DELAY);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUnbilledOutlets(createJSON(), true);
            }
        });

        getUnbilledOutlets(createJSON(), false);

    }

    private void reqPermission() {
        new AskPermission.Builder(this).setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)
                .setCallback(this)
                .setErrorCallback(this)
                .request(REQUEST_PERMISSIONS);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(message);
        getActivity().stopService(new Intent(getActivity(), Locations.class));
    }

    @Override
    public void onStop() {
        super.onStop();

        getActivity().stopService(new Intent(getActivity(), Locations.class));
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        startServices();

    }

    @Override
    public void onPermissionsDenied(int requestCode) {
    }

    @Override
    public void onShowRationalDialog(final PermissionInterface permissionInterface, int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("We need permissions for this app.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onDialogShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }

    @Override
    public void onShowSettings(final PermissionInterface permissionInterface, int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("We need permissions for this app. Open setting screen?");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onSettingsShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }


    private void setAdapter(ArrayList<RetailersModel> arrUnbilled) {
        Collections.sort(arrUnbilled, new Comparator<RetailersModel>() {
            public int compare(RetailersModel r1, RetailersModel r2) {
                return r1.getCompany().compareToIgnoreCase(r2.getCompany());
            }
        });

        unBilledOutletsAdapter = new UnBilledOutletsAdapter(getActivity(), arrUnbilled);
        unBilledOutletsAdapter.setCheckinListener(this);
        mActionSlideExpandableListView.setAdapter(unBilledOutletsAdapter);
    }

    private AbsListView.OnScrollListener onScrollListener() {
        return new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 4;
                int count = mActionSlideExpandableListView.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {

                    if (mActionSlideExpandableListView.getLastVisiblePosition() >= count - threshold && isNextCall) {

                        if (Networking.isNetworkAvailable(getActivity())) {
                            try {
                                llFooter.setVisibility(View.VISIBLE);
                                JSONObject obj = new JSONObject();
                                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                                obj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                                obj.put("beat_id", beatId);
                                obj.put("search", etSearch.getText().toString());
                                obj.put("offset", String.valueOf(currentOffset));
                                obj.put("offset_limit", String.valueOf(offsetLimit));
                                HashMap<String, String> map = new HashMap<>();
                                map.put("data", obj.toString());

                                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllRetailersURL, map, UnbilledOutletsFragment.this, UnbilledOutletsFragment.this, PARSER_TYPE.GET_ALL_RETAILERS);
                                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getAllRetailersURL);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
            }
        };
    }

    public void startServices() {

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
        );

        Intent in = new Intent(getActivity(), Locations.class);
        getActivity().startService(in);
    }

    private void loadMap() {
        // Getting reference to SupportMapFragment of the activity_main
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (map == null) {
            fm.getMapAsync(UnbilledOutletsFragment.this);
        } else {
            setMap();
        }
    }

    private void setMap() {
        MarkerOptions options = new MarkerOptions();

        for (int i = 0; i < arrMapOutlets.size(); i++) {

            double lat = 0.0, lon = 0.0;
            try {
                lat = Double.parseDouble(arrMapOutlets.get(i).getLatitude());
                lon = Double.parseDouble(arrMapOutlets.get(i).getLongitude());
            } catch (Exception e) {
                lat = 0.0;
                lon = 0.0;
            }

            if (lat != 0.0 && lon != 0.0) {
                currentLat = lat;
                currentLong = lon;
            }

            options.position(new LatLng(lat, lon)).title(arrMapOutlets.get(i).getCompany() + "\n" + arrMapOutlets.get(i).getTelephone());
            map.addMarker(options);
        }
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(new LatLng(currentLat, currentLong));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);

        map.moveCamera(center);
        map.animateCamera(zoom);
    }

    private void getUnbilledOutlets(JSONObject dataObject, boolean isSwipeDown) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllRetailersURL, map, UnbilledOutletsFragment.this, UnbilledOutletsFragment.this, PARSER_TYPE.GET_ALL_RETAILERS);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getAllRetailersURL);
                if (dialog != null && !isSwipeDown)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject createJSON() {
        JSONObject dataObj = null;
        try {
            dataObj = new JSONObject();
            dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
            dataObj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
            dataObj.put("is_billed", "1");
            dataObj.put("beat_id", mSharedPreferences.getString(ConstantValues.KEY_BEAT_ID, ""));
            dataObj.put("search", etSearch.getText().toString());
            dataObj.put("offset", String.valueOf(currentOffset));
            dataObj.put("offset_limit", String.valueOf(offsetLimit));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataObj;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isDeviceLocationEnabled(getActivity())) {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }
        startServices();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing() && requestType != PARSER_TYPE.GET_ALL_RETAILERS)
            dialog.dismiss();
        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.GET_ALL_RETAILERS) {
                        if (mSwipeRefreshLayout != null)
                            mSwipeRefreshLayout.setRefreshing(false);
                        if (response instanceof ArrayList) {
//                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_RETAILERS);

                            currentOffset += offsetLimit;
//                            DBHelper.getInstance().deleteTable(DBHelper.TABLE_RETAILERS);
                            ArrayList<RetailersModel> arrOutlets = (ArrayList<RetailersModel>) response;

                            arrListOutlets = (ArrayList<RetailersModel>) arrOutlets.clone();
                            arrMapOutlets = (ArrayList<RetailersModel>) arrOutlets.clone();

                            LocateAutoCompleteAdapter adapter = new LocateAutoCompleteAdapter(getActivity(), R.layout.row_area_name_number, arrMapOutlets);
                            actSearch.setAdapter(adapter);

                            if (arrListOutlets != null && arrListOutlets.size() > 0) {
                                if (arrListOutlets.size() == offsetLimit)
                                    isNextCall = true;
                                else
                                    isNextCall = false;

                                for (int i = 0; i < arrListOutlets.size(); i++) {
                                    unBilledOutletsAdapter.addItem(arrListOutlets.get(i));
                                    if (!retailersModelArrayList.contains(arrListOutlets.get(i)))
                                        retailersModelArrayList.add(arrListOutlets.get(i));
                                }
                            } else {
                                isNextCall = false;
                            }
                            if (retailersModelArrayList != null && retailersModelArrayList.size() > 0) {
                                llUnbilledOutlets.setVisibility(View.VISIBLE);
                                tvNoItems.setVisibility(View.GONE);
                            } else {
                                llUnbilledOutlets.setVisibility(View.GONE);
                                tvNoItems.setVisibility(View.VISIBLE);
                            }
                            unBilledOutletsAdapter.notifyDataSetChanged();

                            loadMap();
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();

                        }
                    } else if (requestType == PARSER_TYPE.GET_RETAILER_TOKEN) {
                        if (response instanceof RetailersModel) {

                            RetailersModel retailersModel = (RetailersModel) response;

                            String customerGrpId = retailersModel.getCustomerGrpId();
                            String customerToken = retailersModel.getCustomerToken();
                            String customerId = retailersModel.getCustomerId();
                            String firstName = retailersModel.getFirstName();
                            String lastName = retailersModel.getLastName();
                            String image = /*ConstantValues.NEW_BASE_IMAGE_AUTH +*/ retailersModel.getImage();
                            String legalEntityId = retailersModel.getLegalEntityId();
                            String beatId = retailersModel.getBeatId();
                            String latitude = retailersModel.getLatitude();
                            String longitude = retailersModel.getLongitude();
                            String hub = retailersModel.getHub();
                            String whId = retailersModel.getLeWhId();
                            String segmentId = retailersModel.getSegmentId();
                            double eCash = retailersModel.getEcash();
                            double creditLimit = retailersModel.getCreditLimit();
                            double paymentDue = retailersModel.getPaymentDue();
                            double ffeCash = retailersModel.getFfeCash();
                            double ffCreditLimit = retailersModel.getFfCreditLimit();
                            double ffPaymentDue = retailersModel.getFfPaymentDue();

                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_GRP_ID, customerGrpId).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_ID, customerId).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_RETAILER_NAME, firstName + " " + lastName).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_FIRST_NAME, firstName + " " + lastName).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, image).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_TOKEN, customerToken).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_SEGMENT_ID, segmentId).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_LEGAL_ENTITY_ID, legalEntityId).apply();
                            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, true).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, latitude).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, longitude).apply();
                            mSharedPreferences.edit().putFloat(ConstantValues.KEY_CREDIT_LIMIT, (float) creditLimit).apply();
                            mSharedPreferences.edit().putFloat(ConstantValues.KEY_ECASH, (float) eCash).apply();
                            mSharedPreferences.edit().putFloat(ConstantValues.KEY_PAYMENT_DUE, (float) paymentDue).apply();
                            mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_CREDIT_LIMIT, (float) ffCreditLimit).apply();
                            mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_ECASH, (float) ffeCash).apply();
                            mSharedPreferences.edit().putFloat(ConstantValues.KEY_FF_PAYMENT_DUE, (float) ffPaymentDue).apply();

                            /*if (isFF && (TextUtils.isEmpty(beatId) || beatId.equalsIgnoreCase("0"))) {
                                showBeatUpdateDialog();
                                return;
                            }*/

                            DBHelper.getInstance().insertOutlet(selRetailerModel);

                            if (whId != null && !TextUtils.isEmpty(whId) && !whId.equals("0"))
                                mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, whId).apply();
                            if (hub != null && !TextUtils.isEmpty(hub) && !hub.equals("0"))
                                mSharedPreferences.edit().putString(ConstantValues.KEY_HUB_ID, hub).apply();
                            mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, beatId).apply();
                            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_CHECKIN, true).apply();

                            Intent intent = new Intent(getActivity(), HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        } else if (requestType == PARSER_TYPE.UPDATE_BEAT) {
                            if (response instanceof CustomerTyepModel) {
                                CustomerTyepModel customerTyepModel = (CustomerTyepModel) response;
                                String hub = customerTyepModel.getHub();
                                String whId = customerTyepModel.getWhId();

                                getRetailerToken(selRetailerModel);

                                /*DBHelper.getInstance().insertOutlet(selRetailerModel);

                                if (whId != null && !TextUtils.isEmpty(whId) && !whId.equals("0"))
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, whId).apply();
                                if (hub != null && !TextUtils.isEmpty(hub) && !hub.equals("0"))
                                    mSharedPreferences.edit().putString(ConstantValues.KEY_HUB_ID, hub).apply();
                                mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, selBeatId).apply();
                                mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_CHECKIN, true).apply();

                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().finish();*/
                            }
                        } else if (requestType == PARSER_TYPE.GET_BEATS) {
                            if (response instanceof ArrayList) {
                                arrBeats = (ArrayList<CustomerTyepModel>) response;
                                if (arrBeats != null) {
                                    FragmentManager fm = getActivity().getSupportFragmentManager();
                                    BeatFragment beatFragment = BeatFragment.newInstance(arrBeats);
                                    beatFragment.setCancelable(true);
                                    beatFragment.setConfirmListener(okListener);
                                    beatFragment.show(fm, "beat_fragment");
                                }
                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void updateBeat() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("customer_id", selRetailerModel.getLegalEntityId());
                jsonObject.put("beat_id", selBeatId);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateBeatURL, map, UnbilledOutletsFragment.this, UnbilledOutletsFragment.this, PARSER_TYPE.UPDATE_BEAT);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.updateBeatURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void getBeats() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getbeatsURL, map, UnbilledOutletsFragment.this, UnbilledOutletsFragment.this, PARSER_TYPE.GET_BEATS);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getbeatsURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void showBeatUpdateDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage(getString(R.string.please_update_beat));
        dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getBeats();
            }
        });
        dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onCheckIn(int pos, Object object) {

        if (!FeedBackActivity.isDeviceLocationEnabled(getActivity())) {

            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }

            Toast.makeText(getActivity(), getString(R.string.please_wait_loc_fetched), Toast.LENGTH_SHORT).show();
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message
                    , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
            );

            Intent in = new Intent(getActivity(), Locations.class);
            getActivity().startService(in);
            return;
        }
        if (DBHelper.getInstance().getCheckinOutletsCount() > 0) {
            Toast.makeText(getActivity(), getString(R.string.please_checkout), Toast.LENGTH_SHORT).show();
        } else {
            if (object != null && object instanceof RetailersModel) {
                selRetailerModel = (RetailersModel) object;

                mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, selRetailerModel.getLatitude()).apply();
                mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, selRetailerModel.getLongitude()).apply();
                if (selRetailerModel.isPopup()) {// check if we need to show popup for the required fields
                    UpdateRetailerFragment fragment = UpdateRetailerFragment.newInstance(selRetailerModel);
                    fragment.setClickListener(UnbilledOutletsFragment.this);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    fragment.show(ft, "");
                } else {
                    if (isFF && (TextUtils.isEmpty(beatId) || beatId.equalsIgnoreCase("0"))) {
                        showBeatUpdateDialog();
                        return;
                    }
                    getRetailerToken(selRetailerModel);
                }

            } else {
                Utils.showAlertDialog(getActivity(), getString(R.string.oops));
            }

        }
    }

    @Override
    public void onNavigate(int pos, Object object) {
        if (object != null && object instanceof RetailersModel) {
            if (object != null && object instanceof RetailersModel) {


                if (((RetailersModel) object).getLatitude().equalsIgnoreCase("0.0") || ((RetailersModel) object).getLongitude().equalsIgnoreCase("0.0")) {
                    Toast.makeText(getActivity(), "Unable to find the location of retailer", Toast.LENGTH_SHORT).show();
                } else {
                    String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?&daddr=%s,%s (%s)", ((RetailersModel) object).getLatitude(), ((RetailersModel) object).getLongitude(), ((RetailersModel) object).getCompany());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        try {
                            Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                            startActivity(unrestrictedIntent);
                        } catch (ActivityNotFoundException innerEx) {
                            Toast.makeText(getActivity(), "Please install a maps application", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onOrdersClick(int pos, Object object) {
        if (object != null && object instanceof RetailersModel) {
            Intent intent = new Intent(getActivity(), OrdersListActivity.class);
            intent.putExtra("token", ((RetailersModel) object).getCustomerToken());
            intent.putExtra("cust_id", ((RetailersModel) object).getCustomerId());
            intent.putExtra("le_entity_id", ((RetailersModel) object).getLegalEntityId());
            startActivity(intent);
        }
    }

    @Override
    public void onFeedback(int pos, Object object) {
        if (object != null && object instanceof RetailersModel) {
            Intent intent = new Intent(getActivity(), FeedBackActivity.class);
            intent.putExtra("legal_entity_id", ((RetailersModel) object).getLegalEntityId());
            startActivity(intent);
        }
    }

    private void getRetailerToken(RetailersModel retailersModel) {

        mSharedPreferences.edit().putString(ConstantValues.KEY_SHOP_NAME, retailersModel.getCompany()).apply();
        mSharedPreferences.edit().putString(ConstantValues.KEY_MOBILE, retailersModel.getTelephone()).apply();
        try {
            JSONObject obj = new JSONObject();
            obj.put("telephone", retailersModel.getTelephone());
            obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));


            currentLat = latitude;
            currentLong = longitude;
            if (latitude == 0.0 && longitude == 0.0) {
                if (!isDeviceLocationEnabled(getActivity())) {

                    if (!alertDialog.isShowing()) {
                        alertDialog.show();
                    }
                }

                Toast.makeText(getActivity(), getString(R.string.please_wait_loc_fetched), Toast.LENGTH_SHORT).show();
                LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message
                        , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
                );

                Intent in = new Intent(getActivity(), Locations.class);
                getActivity().startService(in);

                return;
            }

            double retLat = 0.0, retLong = 0.0;
            try {
                retLat = Integer.parseInt(retailersModel.getLatitude());
                retLong = Integer.parseInt(retailersModel.getLongitude());
            } catch (Exception e) {
                e.printStackTrace();
            }

            Double distance = SphericalUtil.computeDistanceBetween(new LatLng(lan, lon), new LatLng(retLat, retLong));
            float checkInDistance = mSharedPreferences.getFloat(ConstantValues.KEY_CHECKIN_DISTANCE, 0);

            if (distance > checkInDistance) {
                Toast.makeText(getActivity(), String.format(getString(R.string.you_are_away), String.format("%.2f", distance)), Toast.LENGTH_SHORT).show();
            }

            obj.put("latitude", lan);
            obj.put("longitude", lon);

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.getRetailerTokenURL, map, UnbilledOutletsFragment.this, UnbilledOutletsFragment.this, PARSER_TYPE.GET_RETAILER_TOKEN);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.getRetailerTokenURL);

            if (dialog != null)
                dialog.show();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void dialogClose() {
        getRetailerToken(selRetailerModel);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            this.map = googleMap;
            setMap();
        }
    }
}