package com.ebutor.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.POApprovalHistoryRecyclerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ApprovalHistoryModel;
import com.ebutor.models.ApprovalHistoryResponseModel;
import com.ebutor.models.OrdersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class POApprovalHistoryFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object> {

    private SharedPreferences mSharedPreferences;
    private ArrayList<ApprovalHistoryModel> historyModelArrayList;
    private POApprovalHistoryRecyclerAdapter adapter;
    private RecyclerView rvOrderList;
    private TextView tvNoOrders, tvAlertMsg;
    private RelativeLayout rlAlert;
    private LinearLayout llMain;
    private Dialog dialog;
    //    private ExpenseModel expenseModel;
    private OrdersModel poModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_approval_list, container, false);


        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        rvOrderList = (RecyclerView) v.findViewById(R.id.rvOrdersList);
        tvNoOrders = (TextView) v.findViewById(R.id.tvNoOrders);
        tvAlertMsg = (TextView) v.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) v.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) v.findViewById(R.id.ll_main);

        if (getArguments().containsKey("POModel"))
            poModel = (OrdersModel) getArguments().getSerializable("POModel");

        getOrdersList();

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    getOrdersList();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }


    public void getOrdersList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("user_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                if (poModel != null)
                    obj.put("id", poModel.getOrderNumber());
                obj.put("approval_module", "Purchase Order");

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                String url = AppURL.getapprovalhistoryURL;
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, url, map, POApprovalHistoryFragment.this, POApprovalHistoryFragment.this, PARSER_TYPE.GET_PO_APPROVAL_HISTORY);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_PO_APPROVAL_HISTORY) {
                    if (results instanceof ApprovalHistoryResponseModel) {
                        ApprovalHistoryResponseModel model = (ApprovalHistoryResponseModel) results;
                        historyModelArrayList = model.getApprovalHistoryModelArrayList();
                        if (historyModelArrayList != null && historyModelArrayList.size() > 0) {
                            rvOrderList.setVisibility(View.VISIBLE);
                            tvNoOrders.setVisibility(View.GONE);
                            adapter = new POApprovalHistoryRecyclerAdapter(getActivity(), historyModelArrayList);
                            rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
                            rvOrderList.setAdapter(adapter);

                        } else {
                            rvOrderList.setVisibility(View.GONE);
                            tvNoOrders.setVisibility(View.VISIBLE);
                            tvNoOrders.setText("No Data Available");
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        Utils.logout(getActivity(), message);
    }

}

