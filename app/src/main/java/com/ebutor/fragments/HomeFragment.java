package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.Spanned;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.BrandProductsActivity;
import com.ebutor.HomeActivity;
import com.ebutor.MainActivityNew;
import com.ebutor.MyApplication;
import com.ebutor.ProductDetailsActivity;
import com.ebutor.R;
import com.ebutor.TopBrandsActivity;
import com.ebutor.adapters.HighMarginAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.BannerModel;
import com.ebutor.models.HomeDataModel;
import com.ebutor.models.HomeModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.AutoScrollViewPager;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by 300024 on 3/13/2016.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, VolleyHandler<Object>, Response.ErrorListener, HighMarginAdapter.OnItemClickListener {
    public LayoutInflater inflater;
    LinearLayout mainLL;
    DBHelper dataBaseObj;
    DisplayImageOptions options;
    CirclePageIndicator indicator;
    private SwipeRefreshLayout swipeRefreshLayout;
    private HighMarginAdapter highMarginAdapter;
    private ArrayList<BannerModel> arrBanner;
    private ArrayList<HomeModel> arrHome;
    //    ViewPager viewPager;
    private AutoScrollViewPager autoScrollPager;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private SharedPreferences mSharedPreferences;
    private ViewGroup mContainer;
    private int cartCount;
    private ScrollView slMain;
    private TextView tvAlertMsg;
    private RelativeLayout rlAlert;
    private Dialog dialog;
    private String pincode = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContainer = container;
        setHasOptionsMenu(true);
        View view = null;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_home, container, false);
            dataBaseObj = new DBHelper(getActivity());
            dialog = Utils.createLoader(getActivity(), ConstantValues.TABS_PROGRESS);
            this.inflater = getActivity().getLayoutInflater();
            mainLL = (LinearLayout) view.findViewById(R.id.ll_main);
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
//            viewPager = (ViewPager) view.findViewById(R.id.circle_pager);
            autoScrollPager = (AutoScrollViewPager) view.findViewById(R.id.circle_pager);
            indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            slMain = (ScrollView) view.findViewById(R.id.sl_main);
            tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
            rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);

            mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
            arrBanner = new ArrayList<>();

            // customerId = dataBaseObj.getCustomerDetails().getCustomerId();
            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.ic_loading)
                    .showImageForEmptyUri(R.drawable.ic_not_found)
                    .showImageOnFail(R.drawable.ic_not_found)
                    .cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
//              .displayer(new RoundedBitmapDisplayer(20))
//              DO NOT USE RoundedBitmapDisplayer. Use SimpleBitmapDisplayer!
                    .displayer(new SimpleBitmapDisplayer())
                    .build();

        } catch (InflateException e) {
            e.printStackTrace();
        }

        arrBanner = dataBaseObj.getAllBanners();
        if (arrBanner != null && arrBanner.size() > 0) {
            highMarginAdapter = new HighMarginAdapter(getActivity(), arrBanner, false);
            highMarginAdapter.setClickListener(HomeFragment.this);
            autoScrollPager.setAdapter(highMarginAdapter);
            indicator.setViewPager(autoScrollPager);
            autoScrollPager.startAutoScrollPager(autoScrollPager);
            MyApplication.getInstance().setBannersArray(arrBanner);
        } else {
            getBanners();
        }

        int syncTimeInterval = mSharedPreferences.getInt(ConstantValues.KEY_SYNC_INTERVAL, ConstantValues.KEY_SYNC_INTERVAL_DURATION);
        String past = Utils.getPastDateTime(syncTimeInterval);

        arrHome = dataBaseObj.getHomeData(past);
        if (arrHome != null && arrHome.size() > 0) {
            setHomePageData(false);
        } else {
            getHomePageOffers(false);
        }

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    getHomePageOffers(false);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getBanners();
                getHomePageOffers(true);
            }
        });

        return view;
    }

    private void getBanners() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            slMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                String customerToken = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "");

                jsonObject.put("customer_token", customerToken);
                jsonObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                jsonObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                map.put("data", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask bannersRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.bannersURL, map, HomeFragment.this, HomeFragment.this, PARSER_TYPE.GET_BANNER);
            bannersRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(bannersRequest, AppURL.bannersURL);
//            if (dialog != null && !mSharedPreferences.getBoolean("IsLoggedIn", false))
//                dialog.show();
        }
    }

    public void getHomePageOffers(boolean isSwipeDown) {

        if (Networking.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject jsonObject = new JSONObject();

                String customerToken = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "");
                jsonObject.put("customer_token", customerToken);
                String segmentId = mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, "");
                jsonObject.put("segment_id", segmentId);
                jsonObject.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                jsonObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                jsonObject.put("offset", "0");
                jsonObject.put("offset_limit", "5");

                map.put("data", jsonObject.toString());

                VolleyBackgroundTask homePageRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.homePageBannersURL, map, HomeFragment.this, HomeFragment.this, PARSER_TYPE.GET_HOME_PAGE_OFFERS);
                homePageRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(homePageRequest, AppURL.homePageBannersURL);
                if (dialog != null && !isSwipeDown)
                    dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartCount();
    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        ((HomeActivity) getActivity()).updateCart(cartCount);
    }

    public void onError(String errorCode, PARSER_TYPE requestType) {
        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            rlAlert.setVisibility(View.VISIBLE);
            slMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_see_all:
                Object object = view.getTag();
                if (object != null && object instanceof HomeModel) {
                    Intent intent1 = new Intent(getActivity(), TopBrandsActivity.class);
                    intent1.putExtra("Flag", ((HomeModel) object).getFlag());
                    intent1.putExtra("key_id", ((HomeModel) object).getKey());
                    startActivity(intent1);
                }
                break;
            case R.id.llManufacturers:
                Object objModel = view.getTag();
                if (objModel != null && objModel instanceof HomeDataModel) {
                    Intent intent = new Intent(getActivity(), BrandProductsActivity.class);
                    intent.putExtra("ManufacturerId", ((HomeDataModel) objModel).getId());
                    intent.putExtra("ManufacturerName", ((HomeDataModel) objModel).getName());
                    intent.putExtra("Flag", ((HomeDataModel) objModel).getFlag());
                    intent.putExtra("key_id", ((HomeDataModel) objModel).getKey());
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null)
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing() && requestType != PARSER_TYPE.GET_BANNER) {
            dialog.dismiss();
        }
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_HOME_PAGE_OFFERS) {
                    swipeRefreshLayout.setRefreshing(false);
                    mainLL.removeAllViews();
                    if (results instanceof ArrayList) {
                        dataBaseObj.deleteTable(DBHelper.TABLE_HOME);
                        dataBaseObj.deleteTable(DBHelper.TABLE_HOME_CHILD);

                        arrHome = (ArrayList<HomeModel>) results;
                        setHomePageData(true);
                    }

                } else if (requestType == PARSER_TYPE.GET_BANNER) {

                    if (results instanceof ArrayList) {

                        dataBaseObj.deleteTable(DBHelper.TABLE_BANNERS);

                        arrBanner = (ArrayList<BannerModel>) results;
                        if (arrBanner != null && arrBanner.size() > 0) {
                            for (int i = 0; i < arrBanner.size(); i++) {
                                dataBaseObj.insertBanner(arrBanner.get(i));
                            }
                        }
                        highMarginAdapter = new HighMarginAdapter(getActivity(), arrBanner, false);
                        highMarginAdapter.setClickListener(HomeFragment.this);
                        autoScrollPager.setAdapter(highMarginAdapter);
                        indicator.setViewPager(autoScrollPager);
                        autoScrollPager.startAutoScrollPager(autoScrollPager);
                        MyApplication.getInstance().setBannersArray(arrBanner);
                    }
                }
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), message);
        }
    }

    private void setHomePageData(boolean flag) {
        if (arrHome != null && arrHome.size() > 0) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            for (int k = 0; k < arrHome.size(); k++) {
                HomeModel homeModel = arrHome.get(k);
                if (flag)
                    dataBaseObj.insertHome(homeModel);
                ArrayList<HomeDataModel> arrHomeData = homeModel.getModelArrayList();

                if (arrHomeData != null && arrHomeData.size() > 0) {
                    LinearLayout topBrandsHorizontalView = (LinearLayout) inflater.inflate(R.layout.row_home_offer, null);
                    LinearLayout llManufacturer = (LinearLayout) topBrandsHorizontalView.findViewById(R.id.ll_products);
                    TextView tvOfferName = (TextView) topBrandsHorizontalView.findViewById(R.id.tv_offer_name);
                    TextView tvTopBrandSeeAll = (TextView) topBrandsHorizontalView.findViewById(R.id.tv_see_all);
                    tvOfferName.setText(homeModel.getTitle());
                    for (int i = 0; i < arrHomeData.size(); i++) {
                        HomeDataModel homeDataModel = arrHomeData.get(i);
                        if (homeDataModel != null) {
                            ArrayList<String> arrImages = homeDataModel.getArrImages();
                            String name = homeDataModel.getName();

                            LinearLayout singleTopBrand = (LinearLayout) inflater.inflate(R.layout.manufacturer_item_layout, null);
                            TextView tvFeaturedOfferCatName = (TextView) singleTopBrand.findViewById(R.id.tv_featured_offer_cat_name);
                            ImageView ivTopBrand = (ImageView) singleTopBrand.findViewById(R.id.iv_featured_offer);
                            if (arrImages != null && arrImages.size() > 0) {
                                String image = arrImages.get(0);
                                if (image != null) {
                                    ImageLoader.getInstance().displayImage(image, ivTopBrand, options, animateFirstListener);
                                }
                            }
                            if (name != null) {
                                Spanned string = Html.fromHtml(name);
                                tvFeaturedOfferCatName.setText(string);
                            }

                            llManufacturer.addView(singleTopBrand);
                            singleTopBrand.setTag(homeDataModel);
                            singleTopBrand.setId(R.id.llManufacturers);
                            singleTopBrand.setOnClickListener(this);
                        }
                    }
                    tvTopBrandSeeAll.setTag(homeModel);
                    tvTopBrandSeeAll.setId(R.id.tv_see_all);
                    tvTopBrandSeeAll.setOnClickListener(this);
                    mainLL.addView(topBrandsHorizontalView);
                }
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        String keyId = "";
        BannerModel bannerModel = arrBanner.get(position);
        if (bannerModel.getNavigatorObject().equalsIgnoreCase("Product")) {
            Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
            intent.putExtra("ProductId", bannerModel.getNavigatorObjectId());
            intent.putExtra("ParentId", bannerModel.getNavigatorObjectId());
            startActivity(intent);
        } else {
            switch (bannerModel.getNavigatorObject()) {
                case "Brands":
                    keyId = "brand_id";
                    break;
                case "Category":
                    keyId = "category_id";
                    break;
                case "Manufacturer":
                    keyId = "manufacturer_id";
                    break;

            }
            Intent intent = new Intent(getActivity(), MainActivityNew.class);
            intent.putExtra("Id", bannerModel.getNavigatorObjectId());
            intent.putExtra("Type", "Products");
            intent.putExtra("key_id", keyId);
            startActivity(intent);
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
