package com.ebutor.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.ExpenseDetailActivity;
import com.ebutor.adapters.ExpensesRecyclerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ExpenseModel;
import com.ebutor.models.GetExpensesResponse;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DividerItemDecoration;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import java.util.ArrayList;

public class PendingApprovalsFragment extends Fragment implements ExpensesRecyclerAdapter.OnItemClickListener, Response.ErrorListener, VolleyHandler<Object> {

    private SharedPreferences mSharedPreferences;
    private ArrayList<ExpenseModel> expenseModelArrayList;
    private ExpensesRecyclerAdapter adapter;
    private RecyclerView rvOrderList;
    private TextView tvNoOrders;
    //    private RelativeLayout rlAlert;
    private LinearLayout llMain;
    private Dialog dialog;
    //    private FloatingActionButton btnAddExpense;
//    private String customerToken = "", custId = "", leEntityId = "";
    private int type = -1;

    public static PendingApprovalsFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("type", page);
        PendingApprovalsFragment fragment = new PendingApprovalsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey("type")) {
            type = getArguments().getInt("type", 0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_expense_tracker, container, false);


        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        rvOrderList = (RecyclerView) v.findViewById(R.id.rvOrdersList);
        tvNoOrders = (TextView) v.findViewById(R.id.tvNoOrders);
        llMain = (LinearLayout) v.findViewById(R.id.ll_main);
//        btnAddExpense = (FloatingActionButton) v.findViewById(R.id.fab);
        rvOrderList.addItemDecoration(
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        return v;
    }

    public void getOrdersList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
                String url = AppURL.getApprovalDataAsPerRoleURL + "?userid=" + userId + "&exptype=ALL";

                if (type == 0) {
                    VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, url, null, PendingApprovalsFragment.this, PendingApprovalsFragment.this, PARSER_TYPE.GET_EXPENSES);
                    ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);
                } else if (type == 1) {
                    url = AppURL.getApprovalActivityDetailsURL + "?userID=" + userId;

                    VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, url, null, PendingApprovalsFragment.this, PendingApprovalsFragment.this, PARSER_TYPE.GET_EXPENSES);
                    ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);

                } else {
                    return;
                }

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), ExpenseDetailActivity.class);
        intent.putExtra("ExpenseCode", expenseModelArrayList.get(position).getExpenseCode());
        intent.putExtra("isForApproval", type == 0);
        intent.putExtra("ExpenseModel", expenseModelArrayList.get(position));
        getActivity().startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getOrdersList();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if(error!=null){
//            Utils.showAlertWithMessage(getActivity(),getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_EXPENSES) {
                    if (results instanceof GetExpensesResponse) {
                        GetExpensesResponse response = (GetExpensesResponse) results;
                        expenseModelArrayList = response.getExpenses();
                        if (expenseModelArrayList != null && expenseModelArrayList.size() > 0) {

                            rvOrderList.setVisibility(View.VISIBLE);
                            tvNoOrders.setVisibility(View.GONE);

                            adapter = new ExpensesRecyclerAdapter(getActivity(), expenseModelArrayList);
                            adapter.setViewType(2);
                            adapter.setClickListener(PendingApprovalsFragment.this);
                            rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
                            rvOrderList.setAdapter(adapter);

                        } else {
                            rvOrderList.setVisibility(View.GONE);
                            tvNoOrders.setVisibility(View.VISIBLE);
                            tvNoOrders.setText(getString(R.string.no_results));
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}
