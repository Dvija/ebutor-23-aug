package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.NotificationAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.NotificationModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DividerItemDecoration;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationsFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object> {

    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private RecyclerView rvNotifications;
    private ArrayList<NotificationModel> arrNotifications = new ArrayList<>();
    private NotificationAdapter notificationAdapter;
    private TextView tvNoResults, tvAlertMsg;
    private LinearLayout llFooter, llMain, llHeader;
    private RelativeLayout rlAlert;
    private int currentOffset = 0;
    private int totalItemsCount = 0;
    private int offsetLimit = 1;
    private boolean isLoadMore = false, isRequestInProgress = false, isFF = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notifications, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvNotifications = (RecyclerView) view.findViewById(R.id.rv_notifications);
        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        llFooter = (LinearLayout) view.findViewById(R.id.ll_footer);
        llHeader = (LinearLayout) view.findViewById(R.id.ll_header);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        tvNoResults = (TextView) view.findViewById(R.id.tv_no_results);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);

        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);

        notificationAdapter = new NotificationAdapter(getActivity(), arrNotifications);
        rvNotifications.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvNotifications.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        rvNotifications.setAdapter(notificationAdapter);

        rvNotifications.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int count = recyclerView.getLayoutManager().getItemCount();
                int pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                isLoadMore = true;

                if ((visibleItemCount + pastVisiblesItems) >= count && currentOffset * Utils.offsetLimit < totalItemsCount) {
                    if (Networking.isNetworkAvailable(getActivity())) {
                        llFooter.setVisibility(View.VISIBLE);
                        getNotications();
                    } else {
                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    getNotications();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        getNotications();
    }

    private void getNotications() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                if (isFF) {
                    jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                } else {
                    jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                }
                jsonObject.put("offset", currentOffset);
                jsonObject.put("perpage", Utils.offsetLimit);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getNotificationsURL, map, NotificationsFragment.this, NotificationsFragment.this, PARSER_TYPE.GET_NOTIFICATIONS);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                if (!isRequestInProgress)
                    MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getNotificationsURL);
                isRequestInProgress = true;

                if (dialog != null && !isLoadMore)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        currentOffset = 0;
        isLoadMore = false;
        arrNotifications = new ArrayList<>();
        notificationAdapter.removeAllItems();
        getNotications();
    }

    private void setDataAdapter(ArrayList<NotificationModel> tempList) {
        if (tempList != null && tempList.size() > 0) {

            for (int i = 0; i < tempList.size(); i++) {
                notificationAdapter.addItem(tempList.get(i));
                if (!arrNotifications.contains(tempList.get(i)))
                    arrNotifications.add(tempList.get(i));
            }

            llHeader.setVisibility(View.VISIBLE);
            rvNotifications.setVisibility(View.VISIBLE);
            tvNoResults.setVisibility(View.GONE);

            notificationAdapter.notifyDataSetChanged();
        } else {
            if (arrNotifications.size() < 1) {
                llHeader.setVisibility(View.GONE);
                rvNotifications.setVisibility(View.GONE);
                tvNoResults.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.GET_NOTIFICATIONS) {
                        if (response instanceof NotificationModel) {
                            currentOffset += offsetLimit;
                            isRequestInProgress = false;
                            NotificationModel notificationModel = (NotificationModel) response;
                            if (notificationModel != null) {
                                ArrayList<NotificationModel> tempList = notificationModel.getArrNotifications();
                                totalItemsCount = notificationModel.getCount();

                                setDataAdapter(tempList);

                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }
}
