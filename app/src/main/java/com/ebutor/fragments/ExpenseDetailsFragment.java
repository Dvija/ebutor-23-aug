package com.ebutor.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.AttachmentsActivity;
import com.ebutor.adapters.ExpenseDetailsRecyclerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.interfaces.ApprovalClickListener;
import com.ebutor.models.ApprovalDataModel;
import com.ebutor.models.ApprovalResponseModel;
import com.ebutor.models.ExpenseDetailsModel;
import com.ebutor.models.ExpenseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Srikanth Nama on 28-Dec-16.
 */

public class ExpenseDetailsFragment extends Fragment implements VolleyHandler<Object>,
        Response.ErrorListener, ExpenseDetailsRecyclerAdapter.OnItemClickListener,
        ApprovalClickListener {

    ProgressDialog dialog;
    private ExpenseModel expenseModel;
    private ArrayList<ExpenseDetailsModel> expenseDetailsModelArrayList;
    private ExpenseDetailsRecyclerAdapter expenseDetailsRecyclerAdapter;
    private RecyclerView recyclerView;
    private TextView tvExpenseCode, tvExpenseDate;
    private TextView tvRequestFor, tvExpenseSubject;
    private TextView tvActual, tvApproved;
    private TextView tvApprove, tvStatus;
    private SharedPreferences mSharedPreferences;
    private View viewHeader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_expense_details, container, false);
        return v;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.scrollable);
        tvExpenseCode = (TextView) view.findViewById(R.id.tv_expense_code);
        tvExpenseDate = (TextView) view.findViewById(R.id.tv_date);
        tvRequestFor = (TextView) view.findViewById(R.id.tv_request_for);
        tvExpenseSubject = (TextView) view.findViewById(R.id.tv_expense_subject);
        tvActual = (TextView) view.findViewById(R.id.tv_actuals);
        tvApproved = (TextView) view.findViewById(R.id.tv_approved);
        tvApprove = (TextView) view.findViewById(R.id.tv_approve);
        tvStatus = (TextView) view.findViewById(R.id.tv_status);
        viewHeader = view.findViewById(R.id.view_header);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null && extras.containsKey("ExpenseModel")) {
            expenseModel = extras.getParcelable("ExpenseModel");
            if (expenseModel != null) {
                setExpenseHeadValues();
                getExpenseDetails(expenseModel.getExpenseId());
            }
        }

        if (getActivity().getIntent().hasExtra("isForApproval")) {
            boolean isForApproval = getActivity().getIntent().getBooleanExtra("isForApproval", false);
            if (isForApproval)
                tvApprove.setVisibility(View.VISIBLE);
            else
                tvApprove.setVisibility(View.GONE);
        } else {
            tvApprove.setVisibility(View.GONE);
        }

        /*if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EXPENSES_APPROVAL_FEATURE_CODE) > 0) {
            getApprovalData();
        } else {
            tvApprove.setVisibility(View.GONE);
        }*/

        expenseDetailsModelArrayList = new ArrayList<>();
        expenseDetailsRecyclerAdapter = new ExpenseDetailsRecyclerAdapter(getActivity(), expenseDetailsModelArrayList);
        expenseDetailsRecyclerAdapter.setClickListener(ExpenseDetailsFragment.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(expenseDetailsRecyclerAdapter);

        tvApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApprovalDialogFragment fragment = ApprovalDialogFragment.newInstance(
                        expenseModel.getActualAmount(), expenseModel.getApprovedAmount(),
                        expenseModel.getCurrentStatusId(), expenseModel.getRequestFor(), expenseModel.getExpenseId());
                fragment.setListener(ExpenseDetailsFragment.this);
                fragment.show(getFragmentManager(), "Approve Expenses");
            }
        });
    }

    public void getExpenseDetails(String expenseId) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, AppURL.getExpenseDetailURL + "?exp_id=" + expenseId, null, ExpenseDetailsFragment.this, ExpenseDetailsFragment.this, PARSER_TYPE.GET_EXPENSE_DETAILS);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getExpenseDetailURL);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void setExpenseHeadValues() {
        tvExpenseCode.setText(expenseModel.getExpenseCode());
        String date = parseDate(expenseModel.getExpenseDate(), "dd/MM/yyyy");//2016-12-26 16:14:23
        tvExpenseDate.setText(date);

        SpannableStringBuilder _actualAmount = new SpannableStringBuilder("Actuals:" + expenseModel.getActualAmount());
        setTextStyles(_actualAmount, tvActual, 8, ContextCompat.getColor(getActivity(), R.color.primary));

        String approvedAmount = expenseModel.getApprovedAmount();
        SpannableStringBuilder _approvedAmount;
        if (null == approvedAmount || approvedAmount.equalsIgnoreCase("null")) {
            _approvedAmount = new SpannableStringBuilder("Approved: 0");
        } else {
            _approvedAmount = new SpannableStringBuilder("Approved: " + expenseModel.getApprovedAmount());
        }

        String status = expenseModel.getCurrentStatus();
        if (status != null && !status.equalsIgnoreCase("null")) {
            tvStatus.setText("Status : " + status);
        } else {
            tvStatus.setText("Status : ");
        }

        if (expenseModel.getCurrentStatusId() != null && !expenseModel.getCurrentStatusId().equals("1")) {
            setTextStyles(_approvedAmount, tvApproved, 9, ContextCompat.getColor(getActivity(), R.color.text_color_red));
        } else {
            tvStatus.setText("Status : Approved");
            setTextStyles(_approvedAmount, tvApproved, 9, ContextCompat.getColor(getActivity(), R.color.primary));
        }

        tvRequestFor.setText(expenseModel.getRequestFor());
        tvExpenseSubject.setText(expenseModel.getExpenseSubject());

    }

    private void setTextStyles(SpannableStringBuilder sb, TextView textView, int start, int color) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_EXPENSE_DETAILS) {
                    if (response instanceof ArrayList) {
                        expenseDetailsModelArrayList = (ArrayList<ExpenseDetailsModel>) response;
                        if (expenseDetailsModelArrayList.size() > 0) {
                            expenseDetailsRecyclerAdapter = new ExpenseDetailsRecyclerAdapter(getActivity(), expenseDetailsModelArrayList);
                            expenseDetailsRecyclerAdapter.setClickListener(ExpenseDetailsFragment.this);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setAdapter(expenseDetailsRecyclerAdapter);
                        } else {
                            viewHeader.setVisibility(View.GONE);
                        }
                    }
                } else if (requestType == PARSER_TYPE.GET_APPROVALS_DATA) {
                    if (response instanceof ApprovalResponseModel) {
                        ApprovalResponseModel model = (ApprovalResponseModel) response;
                        ArrayList<ApprovalDataModel> approvalDataModels = model.getApprovalDataModels();
                        if (approvalDataModels != null && approvalDataModels.size() > 0) {
                            tvApprove.setVisibility(View.VISIBLE);
                        } else {
                            tvApprove.setVisibility(View.GONE);
                        }

                    }

                }

            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }


    @Override
    public void onSessionError(String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        Utils.logout(getActivity(), message);
    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        try {
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if(error!=null){
//            Utils.showAlertWithMessage(getActivity(),getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onItemClick(int position) {
        if (expenseDetailsModelArrayList == null) {
            return;
        }

        try {
            String attachments = expenseDetailsModelArrayList.get(position).getAttachedFiles();
            if (null != attachments && !TextUtils.isEmpty(attachments)) {
                // go to display images

                Intent intent = new Intent(getActivity(), AttachmentsActivity.class);
                intent.putExtra("Attachments", attachments);
                startActivity(intent);
            } else {
                //do nothing
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onApprove(String currentStatus, String amount, String status, String comments, String isFinalStep, String tallyLedgerName) {
        //todo
        if (amount == null || TextUtils.isEmpty(amount)) {
            showToast("Amount is empty");
            return;
        }

        if (status == null || TextUtils.isEmpty(status)) {
            showToast("Status is empty");
            return;
        }

        if (comments == null || TextUtils.isEmpty(comments)) {
            showToast("Comments is empty");
            return;
        }

        JSONObject approveRequestObj = createJSONObject(currentStatus, amount, status, comments, isFinalStep, tallyLedgerName);
        Log.e("saveapprovaldata", approveRequestObj.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.saveApprovalDataURL, approveRequestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                String status = response.optString("status");
                String message = response.optString("message");
                if (status.equalsIgnoreCase("success")) {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } else {
                    Utils.showAlertDialog(getActivity(), message);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                showToast(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                // something to do here ??
                return ConstantValues.getRequestHeaders();
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(request, AppURL.saveApprovalDataURL);
        if (dialog != null)
            dialog.show();

    }

    @Override
    public void onCancel() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    private JSONObject createJSONObject(String currentStatus, String amount, String status,
                                        String comments, String isFinalStep, String tallyLedgerName) {
        JSONObject object = null;
        try {
            object = new JSONObject();

            object.put("FlowType", expenseModel.getRequestFor());
            object.put("ExpensesMainID", expenseModel.getExpenseId());
            object.put("CurrentStatusID", currentStatus);
            object.put("NextStatusID", status);
            object.put("Comment", comments);
            object.put("ApprovalAmount", amount);
            object.put("isFinalStep", isFinalStep);
            object.put("UserID", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
            object.put("TallyLedgerName", tallyLedgerName.equalsIgnoreCase("Select") ? "" : tallyLedgerName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

}
