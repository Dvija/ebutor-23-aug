package com.ebutor.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.TallyLedgerSpinnerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.interfaces.ApprovalClickListener;
import com.ebutor.models.ApprovalDataModel;
import com.ebutor.models.ApprovalResponseModel;
import com.ebutor.models.TalleyModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import java.util.ArrayList;

public class ApprovalDialogFragment extends DialogFragment implements VolleyHandler<Object>, Response.ErrorListener, View.OnClickListener {

    ApprovalResponseModel model;
    private ApprovalClickListener mListener;
    private Spinner spinLedgerName;
    private EditText etApprovalAmount, etComments, etRequestedFor;
    private Button btnCancel, btnApprove;
    private String nextStatusId, isFinalStep, flowType, expenseId;
    private String currentStatus;
    private SharedPreferences mSharedPreferences;
    private String requestedAmount = "", tallyName = "";
    private ProgressDialog dialog;
    private LinearLayout radioGroupLayout;

    public static ApprovalDialogFragment newInstance(String requestedAmount, String approvedAmount,
                                                     String currentStaus, String flowType, String expenseId) {
        ApprovalDialogFragment frag = new ApprovalDialogFragment();
        Bundle args = new Bundle();
        args.putString("requestedAmount", requestedAmount);
        args.putString("approvedAmount", approvedAmount);
        args.putString("currentStatus", currentStaus);
        args.putString("flowType", flowType);
        args.putString("expenseId", expenseId);
        frag.setArguments(args);
        return frag;
    }

    public void setListener(ApprovalClickListener listener) {
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setTitle("Approve");
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        return inflater.inflate(R.layout.popup_approval, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinLedgerName = (Spinner) view.findViewById(R.id.spinner_ledger);
        etApprovalAmount = (EditText) view.findViewById(R.id.et_approved);
        etRequestedFor = (EditText) view.findViewById(R.id.et_req_for);
        etComments = (EditText) view.findViewById(R.id.et_comments);

        radioGroupLayout = (LinearLayout) view.findViewById(R.id.ll_radio);

        btnApprove = (Button) view.findViewById(R.id.btn_submit);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);

        if (getArguments() != null && getArguments().containsKey("requestedAmount")) {
            requestedAmount = getArguments().getString("requestedAmount");
            etRequestedFor.setText(requestedAmount);
            etApprovalAmount.setText(requestedAmount);
        }
        if (getArguments() != null) {
            currentStatus = getArguments().getString("currentStatus");
            flowType = getArguments().getString("flowType");
//            etRequestedFor.setText(requestedAmount);
            expenseId = getArguments().getString("expenseId");
        }
        if (getArguments() != null && getArguments().containsKey("approvedAmount")) {
            String approvedAmount = getArguments().getString("approvedAmount");
            if (null != approvedAmount && !TextUtils.isEmpty(approvedAmount) && !"null".equalsIgnoreCase(approvedAmount)) {
                etApprovalAmount.setText(approvedAmount);
            }
        }


        getApprovalData();

        if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EXPENSES_TALLEY_FEATURE_CODE) <= 0) {
            view.findViewById(R.id.talley).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.talley).setVisibility(View.VISIBLE);
            getTallyLedgers();
        }

        spinLedgerName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null && view.getTag() instanceof TalleyModel) {
                    TalleyModel model = (TalleyModel) view.getTag();
                    tallyName = model.getTlm_name();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mListener != null)
                    mListener.onCancel();
            }
        });


        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String approvedAmount = etApprovalAmount.getText().toString().trim();
                if (TextUtils.isEmpty(approvedAmount)) {
                    etApprovalAmount.setError("Please enter amount");
                    etApprovalAmount.requestFocus();
                    return;
                }

                try {
                    double approvalAmount = Double.parseDouble(approvedAmount);
                    double _requestedAmount = Double.parseDouble(requestedAmount);

                    if (approvalAmount > _requestedAmount) {
                        etApprovalAmount.setError("Approved amount should be less than or equal to Requested amount.");
                        etApprovalAmount.requestFocus();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String comments = etComments.getText().toString().trim();
                if (TextUtils.isEmpty(approvedAmount)) {
                    etComments.setError("Please enter comments");
                    etComments.requestFocus();
                    return;
                }
                if (mListener != null && model != null)
                    mListener.onApprove(model.getCurrentStatusId(), approvedAmount, nextStatusId, comments, isFinalStep, tallyName);
            }
        });

    }

    public void getApprovalData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, AppURL.getApprovalDataURL
                        + "?yourTableID=" + expenseId + "&FlowType=" + flowType + "&CurrentStatus=" + currentStatus + "&UserID=" + userId, null, ApprovalDialogFragment.this, ApprovalDialogFragment.this, PARSER_TYPE.GET_APPROVALS_DATA);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getApprovalDataURL);
//                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    public void getTallyLedgers() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, AppURL.getTalleyLedgersURL,
                        null, ApprovalDialogFragment.this, ApprovalDialogFragment.this, PARSER_TYPE.GET_TALLY_LEDGERS);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getTalleyLedgersURL);
//                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        if (response != null) {
            if (requestType == PARSER_TYPE.GET_APPROVALS_DATA) {
                if (response instanceof ApprovalResponseModel) {
                    model = (ApprovalResponseModel) response;
//                    String currentStatusId = model.getCurrentStatusId();
                    ArrayList<ApprovalDataModel> approvalDataModels = model.getApprovalDataModels();
                    if (approvalDataModels != null && approvalDataModels.size() > 0) {
                        /*ApprovalStatusSpinnerAdapter adapter = new ApprovalStatusSpinnerAdapter(getActivity(), approvalDataModels);
                        spinStatus.setAdapter(adapter);*/

                        RadioGroup radioGroup = new RadioGroup(getActivity());
                        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        );
                        radioGroupLayout.addView(radioGroup, p);

                        for (int i = 0; i < approvalDataModels.size(); i++) {
                            ApprovalDataModel model = approvalDataModels.get(i);
                            RadioButton radioButtonView = new RadioButton(getActivity());
                            radioButtonView.setText(model.getCondition());
                            radioButtonView.setOnClickListener(ApprovalDialogFragment.this);
                            radioButtonView.setTag(model);
                            radioGroup.addView(radioButtonView, p);
                        }
                    }
                }
            } else if (requestType == PARSER_TYPE.GET_TALLY_LEDGERS) {
                if (response instanceof ArrayList) {
                    ArrayList<TalleyModel> tallyData = (ArrayList<TalleyModel>) response;
                    TallyLedgerSpinnerAdapter adapter = new TallyLedgerSpinnerAdapter(getActivity(), tallyData);
                    spinLedgerName.setAdapter(adapter);
                }
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.oops_try_again));
        }
    }

    @Override
    public void onSessionError(String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        Toast.makeText(getActivity(), getString(R.string.oops), Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onClick(View view) {
        if (view != null && view instanceof RadioButton) {
            Object obj = view.getTag();
            if (obj != null && obj instanceof ApprovalDataModel) {
                ApprovalDataModel model = (ApprovalDataModel) obj;
                nextStatusId = model.getNextStatusId();
                isFinalStep = model.getIsFinalStep();
            }
        }
    }
}
