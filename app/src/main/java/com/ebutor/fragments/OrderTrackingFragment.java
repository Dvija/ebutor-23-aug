package com.ebutor.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ebutor.R;
import com.ebutor.adapters.ShipmentAdapter;
import com.ebutor.models.ShipmentTrackingModel;

import java.util.ArrayList;

public class OrderTrackingFragment extends Fragment {

    private LinearLayout llMain;
    private RecyclerView rvTracking;
    private View tvNoItems;
    private ArrayList<ShipmentTrackingModel> arrShipment;
    private ShipmentAdapter shipmentAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_tracking, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        rvTracking = (RecyclerView) view.findViewById(R.id.rv_tracing);
        tvNoItems = (View) view.findViewById(R.id.tvNoItems);

        arrShipment = new ArrayList<>();

    }
}
