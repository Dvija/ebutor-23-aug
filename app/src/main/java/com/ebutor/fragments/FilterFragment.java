package com.ebutor.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.internal.util.Predicate;
import com.ebutor.FilterActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.FilterAdapter;
import com.ebutor.adapters.FilterDataAdapter;
import com.ebutor.backgroundtask.JSONParser;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.models.FilterModel;
import com.ebutor.models.ProductFilterModel;
import com.ebutor.rangebar.RangeBar;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class FilterFragment extends Fragment implements FilterAdapter.OnItemClickListener, FilterDataAdapter.OnCheckBoxClickListener, RangeBar.OnRangeBarChangeListener, ResultHandler {

    private RecyclerView rvFilterCategories, rvFilterData;
    private RangeBar rangeBar;
    private Button btnApply, btnClear;
    private LinearLayout llFilterData, llRadio;
    private FilterAdapter filterAdapter;
    private FilterDataAdapter filterDataAdapter;
    private RadioButton radioYes, radioNo;
    private FrameLayout flRangeBar;
    private FrameLayout flPrice, flMargin, flTopRated;
    //    private RangeBar marginRangeSeekbar, topRatedBar, priceBar;
    private String filterData, title, minMargin = "", maxMargin = "", minPrice = "", maxPrice = "", minRating = "", maxRating = "", minValue, maxValue;
    private float marginMin, marginMax, priceMin, priceMax, ratingMin, ratingMax;
    private String margin = "", rating = "", price = "", appId, customerToken, segmentId, type, inputId, keyId;
    private boolean marginChange = false, priceChange = false, ratingChange = false, displayStar = false;
    private View marginView, ratingView, priceView;
    private TextView tvTitle, tvMinValue, tvMaxValue, tvRadio;
    private ImageView ivRefreshBar;
    private ArrayList<ProductFilterModel> arrProductFilters;
    private ArrayList<FilterModel> arrFilters;
    private SharedPreferences mSharedPreferences;
    private int selectedPosition;
    private String filterNames = "", attributeIds = "", attributes = "", parserType = "Filter", starIds = "";
    private JSONObject inputJson;
    private Tracker mTracker;
    private int cartCount = 0;
//    private boolean isTabs;

    public static ArrayList<String> filter(Collection<FilterModel> target, Predicate<FilterModel> predicate, String filterType) {
        ArrayList<String> result = new ArrayList<String>();
        for (FilterModel element : target) {
            if (predicate.apply(element)) {
                switch (filterType) {
                    case "Manufacturers":
                        result.add(element.getManufacturerId());
                        break;
                    case "Brands":
                        result.add(element.getBrandId());
                        break;
                    case "Categories":
                        result.add(element.getCategoryId());
                        break;
                    case "checkbox":
                        result.add(element.getFilterId());
                        break;
                }
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        filterData = getArguments().getString("FilterString");
        Bundle args = getArguments();
        if (args != null) {
            if (args.getString("Type") != null)
                type = args.getString("Type");
            if (args.getString("Id") != null)
                inputId = args.getString("Id");
            if (args.getString("key_id") != null) {
                keyId = args.getString("key_id");
            }
            if (args.containsKey("displayStar"))
                displayStar = args.getBoolean("displayStar", false);

//            if (args.getBoolean("Tabs", false))
//                isTabs = args.getBoolean("Tabs", false);
        }
        View convertView = inflater.inflate(R.layout.fragment_filter, null);
        return convertView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);

        appId = mSharedPreferences.getString(ConstantValues.KEY_APP_ID, null);
        customerToken = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, null);
        segmentId = mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, null);

        rvFilterCategories = (RecyclerView) view.findViewById(R.id.rvFilterCategories);
        rvFilterData = (RecyclerView) view.findViewById(R.id.rvFilterData);
        llFilterData = (LinearLayout) view.findViewById(R.id.ll_filter_data);
        llRadio = (LinearLayout) view.findViewById(R.id.ll_radio);
        flRangeBar = (FrameLayout) view.findViewById(R.id.fl_bar);
        rangeBar = (RangeBar) view.findViewById(R.id.range_bar);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvRadio = (TextView) view.findViewById(R.id.tv_radio);
        tvMinValue = (TextView) view.findViewById(R.id.tv_min_value);
        tvMaxValue = (TextView) view.findViewById(R.id.tv_max_value);
        ivRefreshBar = (ImageView) view.findViewById(R.id.iv_refresh_bar);
        btnApply = (Button) view.findViewById(R.id.btn_apply);
        btnClear = (Button) view.findViewById(R.id.btn_clear);
        radioYes = (RadioButton) view.findViewById(R.id.radio_yes);
        radioNo = (RadioButton) view.findViewById(R.id.radio_no);

        arrProductFilters = new ArrayList<>();
        arrFilters = new ArrayList<>();

//        marginRangeSeekbar = (RangeBar) view.findViewById(R.id.margin_bar);
//        topRatedBar = (RangeBar) view.findViewById(R.id.top_rated_bar);
//        priceBar = (RangeBar) view.findViewById(R.id.price_bar);

        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        /*try {
            if (filterData != null && filterData.length() > 0) {
                JSONArray dataArray = new JSONArray(filterData);
                if (dataArray != null) {
                    for (int i = 0; i < dataArray.length(); i++) {
                        ProductFilterModel productFilterModel = new ProductFilterModel();
                        JSONObject filterObj = dataArray.getJSONObject(i);
                        String attributeId = filterObj.optString("attribute_id");
                        String name = filterObj.optString("name");
                        String type = filterObj.optString("type");

                        productFilterModel.setAttributeId(attributeId);
                        productFilterModel.setGroupName(name);
                        productFilterModel.setFilterType(type);

                        if (TextUtils.isEmpty(attributeId)) {
                            JSONArray optionArray = filterObj.optJSONArray("option");
                            if (optionArray != null && optionArray.length() > 0) {

                                for (int j = 0; j < optionArray.length(); j++) {
                                    JSONObject optionObj = optionArray.getJSONObject(j);
                                   String min = optionObj.optString("minvalue");
                                   String max = optionObj.optString("maxvalue");
                                    productFilterModel.setMaxValue(max);
                                    productFilterModel.setMinValue(min);
                                    if(!min.equalsIgnoreCase(max)) {
                                        arrProductFilters.add(productFilterModel);
                                    }
                                }
                            }

                        } else {
                            JSONArray optionArray = filterObj.optJSONArray("option");
                            if (optionArray != null && optionArray.length() > 0) {
                                ArrayList<FilterModel> filterModelArrayList = new ArrayList<>();

                                for (int j = 0; j < optionArray.length(); j++) {
                                    FilterModel model = new FilterModel();
                                    JSONObject optionObj = optionArray.getJSONObject(j);
                                    String filterName = optionObj.optString("filtername");
                                    model.setFilterName(filterName);
                                    filterModelArrayList.add(model);
                                }
                                productFilterModel.setArrFilters(filterModelArrayList);
                            }
                            arrProductFilters.add(productFilterModel);
                        }

                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        JSONParser parser = new JSONParser();
//        if (isTabs) {
        arrProductFilters = parser.getFilterDataNew(filterData, displayStar);
//        } else {
//            arrProductFilters = parser.getFilterData(filterData);
//        }

        ProductFilterModel productFilterModel = new ProductFilterModel();
        productFilterModel.setAttributeId("0");
        productFilterModel.setGroupName("Fast Moving");
        productFilterModel.setFilterType("radio");
        arrProductFilters.add(productFilterModel);

        if (MyApplication.getInstance().getArrProductFilters() != null) {
            arrProductFilters = MyApplication.getInstance().getArrProductFilters();
        }

        filterAdapter = new FilterAdapter(getActivity(), arrProductFilters);
        filterAdapter.setClickListener(FilterFragment.this);
        rvFilterCategories.setLayoutManager(new GridLayoutManager(getActivity(), 4, GridLayoutManager.VERTICAL, false));
        rvFilterCategories.addItemDecoration(new InsetDecoration());
        rvFilterCategories.setAdapter(filterAdapter);

        setData(0);

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setArrProductFilters(null);
                Intent intent = getActivity().getIntent();
                intent.putExtra("requestType", type);
                intent.putExtra("inputJson", "");
                intent.putExtra("type", type);
                intent.putExtra("clear", true);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setArrProductFilters(arrProductFilters);
                filterNames = "";
                attributeIds = "";
                attributes = "";
                starIds = "";
                HashMap<String, String> map = new HashMap<>();
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("keyword", "");
                    if (segmentId != null && segmentId.length() > 0) {
                        jsonObject.put("segment_id", segmentId);
                    } else {
                        jsonObject.put("segment_id", "");
                    }

                    if (customerToken != null && customerToken.length() > 0) {
                        jsonObject.put("customer_token", customerToken);
                    }

                    if (type != null && !TextUtils.isEmpty(type)) {
                        switch (type) {
                            case "FeaturedOffers":
                                jsonObject.put("category_id", inputId);
                                jsonObject.put("brand_id", "");
                                break;
                            case "SubCategories":
                                jsonObject.put("category_id", inputId);
                                jsonObject.put("brand_id", "");
                                break;
                            case "BrandProducts":
                                jsonObject.put("category_id", "");
                                jsonObject.put("brand_id", inputId);
                                break;
                            case "Products":
                                jsonObject.put(keyId, inputId);
                                break;
                        }
                    }

//                    if (isTabs) {
                    jsonObject.put("flag", "");

                    JSONObject filterTabsobj = new JSONObject();
                    for (int i = 0; i < arrProductFilters.size(); i++) {
                        attributes = "";
                        ArrayList<FilterModel> arrFilters = arrProductFilters.get(i).getArrFilters();

                        switch (arrProductFilters.get(i).getFilterType()) {
                            case "Manufacturers":
                                if (arrFilters != null) {
                                    Predicate<FilterModel> isCheckedFilters = new Predicate<FilterModel>() {
                                        @Override
                                        public boolean apply(FilterModel filterModel) {
                                            return filterModel.isChecked();
                                        }
                                    };

                                    ArrayList<String> checkedFilters = filter(arrFilters, isCheckedFilters, arrProductFilters.get(i).getFilterType());
                                    attributes = TextUtils.join(",", checkedFilters);

//                                    for (int j = 0; j < arrFilters.size(); j++) {
//                                        if (arrFilters.get(j).isChecked()) {
//                                            attributes += arrFilters.get(j).getManufacturerId().concat(",");
//                                        }
//                                    }
//                                    if (attributes != null && attributes.length() > 0) {
//                                        attributes = attributes.substring(0, attributes.length() - 1);
//                                    }
                                    JSONObject manfIds = new JSONObject();
                                    if (!TextUtils.isEmpty(attributes))
                                        manfIds.put("maf_id", attributes);
                                    filterTabsobj.put("manf", manfIds);
                                }
                                break;
                            case "Brands":
                                if (arrFilters != null) {

                                    Predicate<FilterModel> isCheckedFilters = new Predicate<FilterModel>() {
                                        @Override
                                        public boolean apply(FilterModel filterModel) {
                                            return filterModel.isChecked();
                                        }
                                    };

                                    ArrayList<String> checkedFilters = filter(arrFilters, isCheckedFilters, arrProductFilters.get(i).getFilterType());
                                    attributes = TextUtils.join(",", checkedFilters);

//                                    for (int j = 0; j < arrFilters.size(); j++) {
//                                        if (arrFilters.get(j).isChecked()) {
//                                            attributes += arrFilters.get(j).getBrandId().concat(",");
//                                        }
//                                    }
//                                    if (attributes != null && attributes.length() > 0) {
//                                        attributes = attributes.substring(0, attributes.length() - 1);
//                                    }

                                    JSONObject brandIds = new JSONObject();
                                    if (!TextUtils.isEmpty(attributes))
                                        brandIds.put("brand_id", attributes);
                                    filterTabsobj.put("brand", brandIds);
                                }
                                break;
                            case "Categories":
                                if (arrFilters != null) {

                                    Predicate<FilterModel> isCheckedFilters = new Predicate<FilterModel>() {
                                        @Override
                                        public boolean apply(FilterModel filterModel) {
                                            return filterModel.isChecked();
                                        }
                                    };

                                    ArrayList<String> checkedFilters = filter(arrFilters, isCheckedFilters, arrProductFilters.get(i).getFilterType());
                                    attributes = TextUtils.join(",", checkedFilters);

//                                    for (int j = 0; j < arrFilters.size(); j++) {
//                                        if (arrFilters.get(j).isChecked()) {
//                                            attributes += arrFilters.get(j).getCategoryId().concat(",");
//                                        }
//                                    }
//                                    if (attributes != null && attributes.length() > 0) {
//                                        attributes = attributes.substring(0, attributes.length() - 1);
//                                    }

                                    JSONObject catIds = new JSONObject();
                                    if (!TextUtils.isEmpty(attributes))
                                        catIds.put("category_id", attributes);
                                    filterTabsobj.put("categories", catIds);
                                }
                                break;
                            case "checkbox":
                                ArrayList<FilterModel> arrBrandFilters = arrProductFilters.get(i).getArrFilters();
                                for (int j = 0; j < arrBrandFilters.size(); j++) {
                                    FilterModel filterModel = arrBrandFilters.get(j);
                                    if (filterModel.isChecked()) {
                                        if (filterModel.isStar()) {
                                            starIds += filterModel.getFilterName().concat(",");
                                        } else {
                                            filterNames += filterModel.getFilterName().concat(",");
                                            attributeIds += arrProductFilters.get(i).getAttributeId().concat(",");
                                        }
                                    }
                                }
                                break;
                            case "range":
                                String grpName = arrProductFilters.get(i).getGroupName();
                                JSONObject rangeObj = new JSONObject();
                                if (!(arrProductFilters.get(i).getSelMin() == arrProductFilters.get(i).getMinValue() && arrProductFilters.get(i).getSelMax() == arrProductFilters.get(i).getMaxValue())) {
                                    rangeObj.put("minvalue", arrProductFilters.get(i).getSelMin());
                                    rangeObj.put("maxvalue", arrProductFilters.get(i).getSelMax());
                                }
                                filterTabsobj.put(grpName, rangeObj);
                                break;
                        }
                    }
                    JSONObject productFilterObj = new JSONObject();
                    JSONObject productColorObj = new JSONObject();
                    if (filterNames != null && filterNames.length() > 0) {
                        filterNames = filterNames.substring(0, filterNames.length() - 1);
                    }
                    if (attributeIds != null && attributeIds.length() > 0) {
                        attributeIds = attributeIds.substring(0, attributeIds.length() - 1);
                    }
                    if (starIds != null && starIds.length() > 0) {
                        starIds = starIds.substring(0, starIds.length() - 1);
                    }
                    if (!TextUtils.isEmpty(filterNames)) {
                        productFilterObj.put("values", filterNames);
                    }
                    if (!TextUtils.isEmpty(attributeIds)) {
                        productFilterObj.put("attribute_id", attributeIds);
                    }

                    if (!TextUtils.isEmpty(starIds)) {
                        productColorObj.put("color_value", starIds);
                    }
                    filterTabsobj.put("product_filters", productFilterObj);
                    filterTabsobj.put("product_color", productColorObj);
                    jsonObject.put("filters", filterTabsobj);


//                    } else {
//                        if (type != null && !TextUtils.isEmpty(type)) {
//                            switch (type) {
//                                case "FeaturedOffers":
//                                    jsonObject.put("category_id", inputId);
//                                    jsonObject.put("brand_id", "");
//                                    break;
//                                case "SubCategories":
//                                    jsonObject.put("category_id", inputId);
//                                    jsonObject.put("brand_id", "");
//                                    break;
//                                case "BrandProducts":
//                                    jsonObject.put("category_id", "");
//                                    jsonObject.put("brand_id", inputId);
//                                    break;
//                            }
//                        } else {
//                            jsonObject.put("category_id", "");
//                            jsonObject.put("brand_id", "");
//                        }
//
//                        JSONObject filterObj = new JSONObject();
//                        for (int i = 0; i < arrProductFilters.size(); i++) {
//                            String fastMoving = "";
//                            String filterType = arrProductFilters.get(i).getFilterType();
//                            switch (filterType) {
//                                case "checkbox":
//                                    ArrayList<FilterModel> arrBrandFilters = arrProductFilters.get(i).getArrFilters();
//                                    for (int j = 0; j < arrBrandFilters.size(); j++) {
//                                        if (arrBrandFilters.get(j).isChecked()) {
//                                            filterNames += arrBrandFilters.get(j).getFilterName().concat(",");
//                                            attributeIds += arrProductFilters.get(i).getAttributeId().concat(",");
//                                        }
//                                    }
//                                    break;
//                                case "range":
//                                    String grpName = arrProductFilters.get(i).getGroupName();
//                               /* if (!grpName.equalsIgnoreCase("Fast Moving")) {
//                                    JSONObject grpObj = new JSONObject();
//                                    if (!(arrProductFilters.get(i).getSelMin() == arrProductFilters.get(i).getMinValue() && arrProductFilters.get(i).getSelMax() == arrProductFilters.get(i).getMaxValue())) {
//                                        grpObj.put("minvalue", arrProductFilters.get(i).getSelMin());
//                                        grpObj.put("maxvalue", arrProductFilters.get(i).getSelMax());
//                                    }
//                                    filterObj.put(grpName, grpObj);
//                                }*/
//                                    if (grpName.equalsIgnoreCase("margin")) {
//                                        JSONObject marginObj = new JSONObject();
//                                        if (!(arrProductFilters.get(i).getSelMin() == arrProductFilters.get(i).getMinValue() && arrProductFilters.get(i).getSelMax() == arrProductFilters.get(i).getMaxValue())) {
//                                            marginObj.put("minvalue", arrProductFilters.get(i).getMinValue());
//                                            marginObj.put("maxvalue", arrProductFilters.get(i).getMaxValue());
//                                        }
//                                        filterObj.put(grpName, marginObj);
//                                    } else if (grpName.equalsIgnoreCase("price")) {
//                                        JSONObject priceObj = new JSONObject();
//                                        if (!(arrProductFilters.get(i).getSelMin() == arrProductFilters.get(i).getMinValue() && arrProductFilters.get(i).getSelMax() == arrProductFilters.get(i).getMaxValue())) {
//                                            priceObj.put("minvalue", arrProductFilters.get(i).getMinValue());
//                                            priceObj.put("maxvalue", arrProductFilters.get(i).getMaxValue());
//                                        }
//                                        filterObj.put(grpName, priceObj);
//                                    }
//                                    break;
//                                case "radio":
//                                    if (radioYes.isChecked()) {
//                                        fastMoving = "true";
//                                    } else if (radioNo.isChecked()) {
//                                        fastMoving = "false";
//                                    }
//                                    break;
//                            }
//
//                            arrProductFilters.get(i).setFastMoving(fastMoving);
//                            JSONObject fastMovingObj = new JSONObject();
//                            fastMovingObj.put("value", fastMoving);
//                            filterObj.put("fastmoving", fastMovingObj);
//
//                        }
//                        if (filterNames != null && filterNames.length() > 0) {
//                            filterNames = filterNames.substring(0, filterNames.length() - 1);
//                        }
//                        if (attributeIds != null && attributeIds.length() > 0) {
//                            attributeIds = attributeIds.substring(0, attributeIds.length() - 1);
//                        }
//
//                        JSONObject productFilterObj = new JSONObject();
//                        if (!TextUtils.isEmpty(filterNames)) {
//                            productFilterObj.put("values", filterNames);
//                        }
//                        if (!TextUtils.isEmpty(attributeIds)) {
//                            productFilterObj.put("attribute_id", attributeIds);
//                        }
//                        filterObj.put("product_filters", productFilterObj);
//
//                        jsonObject.put("filters", filterObj);
//
//                    }
                    inputJson = jsonObject;
                    Intent intent = getActivity().getIntent();
                    intent.putExtra("requestType", parserType);
                    intent.putExtra("inputJson", inputJson.toString());
                    intent.putExtra("type", type);
                    intent.putExtra("clear", false);
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onItemClick(final int position) {
        selectedPosition = position;
        setData(position);
    }

    private void setData(final int position) {
        String filterType = "";
        if (arrProductFilters != null && arrProductFilters.size() > 0 && arrProductFilters.get(position) != null) {
            filterType = arrProductFilters.get(position).getFilterType();
        }
        for (int i = 0; i < llFilterData.getChildCount(); i++) {
            View v = llFilterData.getChildAt(i);
            v.setVisibility(View.GONE);
        }
        switch (filterType) {

            case "Manufacturers":
            case "Brands":
            case "Categories":
            case "checkbox":
                arrFilters = arrProductFilters.get(position).getArrFilters();
                if (arrFilters != null) {
                    rvFilterData.setVisibility(View.VISIBLE);
                    filterDataAdapter = new FilterDataAdapter(getActivity(), arrFilters);
                    filterDataAdapter.setClickListener(FilterFragment.this);
                    rvFilterData.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
                    rvFilterData.setAdapter(filterDataAdapter);
                }
                break;
            case "range":
                String selMin = "", selMax = "";
                flRangeBar.setVisibility(View.VISIBLE);
                minValue = arrProductFilters.get(position).getMinValue();
                maxValue = arrProductFilters.get(position).getMaxValue();
                selMin = arrProductFilters.get(position).getSelMin();
                selMax = arrProductFilters.get(position).getSelMax();
                title = arrProductFilters.get(position).getGroupName();
                rangeBar.mTickStart = 0;
                rangeBar.mTickEnd = 100;
                if (title.equalsIgnoreCase(getActivity().getResources().getString(R.string.rating))) {
                    if (minValue != null && !TextUtils.isEmpty(minValue))
                        rangeBar.setTickStart(Float.parseFloat(minValue));
                    if (maxValue != null && !TextUtils.isEmpty(maxValue))
                        rangeBar.setTickEnd(Float.parseFloat(maxValue));
                    rangeBar.setTickInterval(1f);
                } else {
                    if (maxValue != null && !TextUtils.isEmpty(maxValue))
                        rangeBar.setTickEnd(Float.parseFloat(maxValue));
                    if (minValue != null && !TextUtils.isEmpty(minValue))
                        rangeBar.setTickStart(Float.parseFloat(minValue));
                    rangeBar.setTickInterval(0.01f);
                }
                if (selMin != null && !TextUtils.isEmpty(selMin) || selMax != null && !TextUtils.isEmpty(selMax)) {
                    rangeBar.setRangePinsByValue(Float.parseFloat(selMin), Float.parseFloat(selMax));
                }
                tvTitle.setText(title + " : " + selMin + " " + getActivity().getResources().getString(R.string.to) + " " + selMax);
                rangeBar.setOnRangeBarChangeListener(this);

                ivRefreshBar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rangeBar.mTickStart = 0;
                        rangeBar.mTickEnd = 100;
                        if (title.equalsIgnoreCase(getActivity().getResources().getString(R.string.top_rated))) {
                            rangeBar.setTickStart(Float.parseFloat(minValue));
                            rangeBar.setTickEnd(Float.parseFloat(maxValue));
                            rangeBar.setTickInterval(1f);
                        } else {
                            rangeBar.setTickEnd(Float.parseFloat(maxValue));
                            rangeBar.setTickStart(Float.parseFloat(minValue));
                            rangeBar.setTickInterval(0.01f);
                        }
                        rangeBar.setRangePinsByValue(Float.parseFloat(minValue), Float.parseFloat(maxValue));
                        tvTitle.setText(title + " : " + minValue + " " + getActivity().getResources().getString(R.string.to) + " " + maxValue);
                        arrProductFilters.get(position).setSelMin(minValue);
                        arrProductFilters.get(position).setSelMax(maxValue);
                    }
                });

                break;
            case "radio":
                llRadio.setVisibility(View.VISIBLE);
                String fastMoving = arrProductFilters.get(position).getFastMoving();
                tvRadio.setText(arrProductFilters.get(position).getGroupName());
                if (fastMoving.equalsIgnoreCase(getActivity().getResources().getString(R.string.true_text))) {
                    radioYes.setChecked(true);
                } else if (fastMoving.equalsIgnoreCase(getActivity().getResources().getString(R.string.true_text))) {
                    radioNo.setChecked(true);
                }
                break;


        }
    }

    @Override
    public void OnCheckBoxClick(ArrayList<FilterModel> arrFilters) {
        if (arrFilters != null)
            arrProductFilters.get(selectedPosition).setArrFilters(arrFilters);
    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
        String result = leftPinValue + " to " + rightPinValue;
        tvTitle.setText(title + " : " + result);
        arrProductFilters.get(selectedPosition).setSelMin(leftPinValue);
        arrProductFilters.get(selectedPosition).setSelMax(rightPinValue);
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null) {
            String response = (String) results;

        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {

    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartCount();
        mTracker.setScreenName("Filters Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        ((FilterActivity) getActivity()).updateCart(cartCount);
    }

    public class InsetDecoration extends RecyclerView.ItemDecoration {

        private int mInsets;

        public InsetDecoration() {
            mInsets = getActivity().getResources().getDimensionPixelSize(R.dimen.recycler_insets);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            //We can supply forced insets for each item view here in the Rect
            outRect.set(mInsets, mInsets, mInsets, mInsets);
        }
    }
}
