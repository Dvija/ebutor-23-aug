package com.ebutor.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.PaymentActivity;
import com.ebutor.R;
import com.ebutor.adapters.CountriesSpinnerAdapter;
import com.ebutor.adapters.StatesSpinnerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CountryModel;
import com.ebutor.models.StateModel;
import com.ebutor.models.UserAddressModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srikanth Nama on 2/29/2016.
 */
public class AddNewAddressFragment extends Fragment implements ResultHandler, Response.ErrorListener, VolleyHandler<Object> {

    private TextView tvSave, tvCancel, tvHeader, tvAlertMsg;
    private RelativeLayout rlAlert;
    private ScrollView slMain;
    private EditText etFirstName, etLastName, etMobile, etEmail, etAddress, etAddress1, etCity, etPin;
    private Spinner /*spinCountry,*/ spinState;
    private ArrayList<CountryModel> countriesList;
    private ArrayList<StateModel> statesList;
    private String selectedStateId;
    private String selectedState, position, requestType = "";
    private SharedPreferences mSharedPreferences;
    private UserAddressModel userAddressModel;
    private boolean isEdit;
    private PARSER_TYPE parserType;
    private Dialog dialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null && getArguments().containsKey("SelectedAddress")) {
            userAddressModel = (UserAddressModel) getArguments().getSerializable("SelectedAddress");
            isEdit = true;
        }
        if (getArguments() != null && getArguments().containsKey("position")) {
            position = getArguments().getString("position");
            isEdit = true;
        }

        View rootView = inflater.inflate(R.layout.fragent_add_new_address, container, false);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        tvSave = (TextView) view.findViewById(R.id.save);
        tvCancel = (TextView) view.findViewById(R.id.cancel);
        tvHeader = (TextView) view.findViewById(R.id.tv_saved_addresses_count);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        slMain = (ScrollView) view.findViewById(R.id.sl_main);

        etFirstName = (EditText) view.findViewById(R.id.et_first_name);
        etLastName = (EditText) view.findViewById(R.id.et_last_name);
        etMobile = (EditText) view.findViewById(R.id.et_mobile_no);
        etEmail = (EditText) view.findViewById(R.id.et_email);
        etAddress = (EditText) view.findViewById(R.id.et_address1);
        etAddress1 = (EditText) view.findViewById(R.id.et_address2);
        etCity = (EditText) view.findViewById(R.id.et_city);
        etPin = (EditText) view.findViewById(R.id.et_pin_code);

//        spinCountry = (Spinner) view.findViewById(R.id.spinner_country);
        spinState = (Spinner) view.findViewById(R.id.spinner_state);

        countriesList = new ArrayList<>();
        statesList = new ArrayList<>();

//        getCountries();

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestType) {
                        case "GetCountries":
                            getCountries();
                            break;
                        case "GetStates":
                            getStates();
                            break;
                        case "Address":
                            saveAddress();
                            break;
                        default:
                            getCountries();
                            getStates();
                            break;
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                saveAddress();

            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

//        spinCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (view != null) {
//                    Object obj = view.getTag();
//                    if (obj != null && obj instanceof CountryModel) {
//                        CountryModel countryModel = (CountryModel) obj;
//                        String countryId = countryModel.getCountryId();
//                        selectedCountryId = countryId;
//                        selectedCountry = countryModel.getCountryName();
//
//                        if (countryId.equals("0")) {
//                            statesList = new ArrayList<>();
//                            StateModel state = new StateModel();
//                            state.setStateId("0");
//                            state.setStateName("State");
//                            statesList.add(state);
//
//                            StatesSpinnerAdapter adapter = new StatesSpinnerAdapter(getActivity(), statesList);
//                            spinState.setAdapter(adapter);
//
//                            return;
//                        }
//
//                        if (Networking.isNetworkAvailable(getActivity())) {
//                            JSONObject jsonObject = new JSONObject();
//                            try {
//                                jsonObject.put("appId", mSharedPreferences.getString(ConstantValues.KEY_APP_ID, ""));
//                                jsonObject.put("country", countryId);
//                                HashMap<String, String> map = new HashMap<>();
//                                map.put("data", jsonObject.toString());
//
//                                HTTPBackgroundTask task = new HTTPBackgroundTask(AddNewAddressFragment.this, getActivity(), PARSER_TYPE.GET_STATES, APIREQUEST_TYPE.HTTP_POST);
//                                task.execute(map);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        } else {
////                            showSnackMessage(ConstantValues.NETWORK_ERROR);
//                        }
//
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        getStates();

        spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view != null) {
                    Object obj = view.getTag();
                    if (obj != null && obj instanceof StateModel) {
                        StateModel stateModel = (StateModel) obj;
                        selectedStateId = stateModel.getStateId();
                        selectedState = stateModel.getStateName();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void saveAddress() {

        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String mobile = etMobile.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String address1 = etAddress1.getText().toString().trim();
        String city = etCity.getText().toString().trim();
        String pin = etPin.getText().toString().trim();

        if (TextUtils.isEmpty(firstName)) {
            etFirstName.requestFocus();
            etFirstName.setError(getActivity().getResources().getString(R.string.please_enter_first_name));
            return;
        } else if (TextUtils.isEmpty(lastName)) {
            etLastName.requestFocus();
            etLastName.setError(getActivity().getResources().getString(R.string.please_enter_last_name));
            return;
        } else if (TextUtils.isEmpty(mobile)) {
            etMobile.requestFocus();
            etMobile.setError(getActivity().getResources().getString(R.string.please_enter_mobile_number));
            return;
        } else if (mobile.length() < 10) {
            etMobile.requestFocus();
            etMobile.setError(getActivity().getResources().getString(R.string.please_enter_valid_mobile_number));
            return;
        } else if (!(mobile.substring(0, 1).equalsIgnoreCase("7") || mobile.substring(0, 1).equalsIgnoreCase("8") || mobile.substring(0, 1).equalsIgnoreCase("9"))) {
            etMobile.requestFocus();
            etMobile.setError(getActivity().getResources().getString(R.string.please_enter_valid_mobile_number));
            return;
        } else if (!TextUtils.isEmpty(email) && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.requestFocus();
            etEmail.setError(getActivity().getResources().getString(R.string.please_enter_valid_email));
            return;
        } else if (TextUtils.isEmpty(address)) {
            etAddress.requestFocus();
            etAddress.setError(getActivity().getResources().getString(R.string.please_enter_address1));
            return;
        } else if (TextUtils.isEmpty(city)) {
            etCity.requestFocus();
            etCity.setError(getActivity().getResources().getString(R.string.please_enter_city));
            return;
        } else if (TextUtils.isEmpty(pin)) {
            etPin.requestFocus();
            etPin.setError(getActivity().getResources().getString(R.string.please_enter_pin));
            return;
        } else if (pin.length() < 6) {
            etPin.requestFocus();
            etPin.setError(getActivity().getResources().getString(R.string.please_enter_valid_pin));
            return;
        } else if (selectedState.equalsIgnoreCase(getActivity().getResources().getString(R.string.select_state))) {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.please_select_state));
        } else {
            if (Networking.isNetworkAvailable(getActivity())) {
                rlAlert.setVisibility(View.GONE);
                slMain.setVisibility(View.VISIBLE);
                requestType = "Address";
                try {
                    JSONObject obj = new JSONObject();
                    if (isEdit) {
                        obj.put("flag", "2");
                        parserType = PARSER_TYPE.EDIT_SHIPPING_ADDRESS;
                    } else {
                        obj.put("flag", "1");
                        parserType = PARSER_TYPE.ADD_ADDRESS;
                    }
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                    JSONArray detailsArray = new JSONArray();
                    JSONObject addressObj = new JSONObject();
                    if (isEdit) {
                        addressObj.put("address_id", userAddressModel.getAddressId());
                        userAddressModel = new UserAddressModel();
                        userAddressModel.setAddressId(userAddressModel.getAddressId());
                        userAddressModel.setFirstName(firstName);
                        userAddressModel.setLastName(lastName);
                        userAddressModel.setAddress(address);
                        userAddressModel.setAddress1(address1);
                        userAddressModel.setCity(city);
                        userAddressModel.setPin(pin);
                        userAddressModel.setState(selectedStateId);
                        userAddressModel.setCountry("99");
                        userAddressModel.setAddressType("shipping");
                        userAddressModel.setTelephone(mobile);
                        userAddressModel.setEmail(email);
                    }

                    addressObj.put("FirstName", firstName);
                    addressObj.put("LastName", lastName);
                    addressObj.put("Address", address);
                    addressObj.put("Address1", address1);
                    addressObj.put("City", city);
                    addressObj.put("pin", pin);
                    addressObj.put("state", selectedStateId);
                    addressObj.put("telephone", mobile);
                    addressObj.put("country", "99");
                    addressObj.put("addressType", "shipping");
                    addressObj.put("email", email);
                    detailsArray.put(addressObj);

                    obj.put("Details", detailsArray);

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask addAddressRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addAddressURL, map, AddNewAddressFragment.this, AddNewAddressFragment.this, parserType);
                    addAddressRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(addAddressRequest, AppURL.addAddressURL);
                    if (dialog != null)
                        dialog.show();

//                    HTTPBackgroundTask task = new HTTPBackgroundTask(AddNewAddressFragment.this, getActivity(), parserType, APIREQUEST_TYPE.HTTP_POST);
//                    task.execute(map);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                rlAlert.setVisibility(View.VISIBLE);
                slMain.setVisibility(View.GONE);
                requestType = "Address";
            }
        }
    }

    private void getStates() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            slMain.setVisibility(View.VISIBLE);
            requestType = "GetStates";
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("flag", "2");
                jsonObject.put("country", "99");
                jsonObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                jsonObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getStateCountriesURL, map, AddNewAddressFragment.this, AddNewAddressFragment.this, PARSER_TYPE.GET_STATES);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getStateCountriesURL);
                if (dialog != null)
                    dialog.show();

//                HTTPBackgroundTask task = new HTTPBackgroundTask(AddNewAddressFragment.this, getActivity(), PARSER_TYPE.GET_STATES, APIREQUEST_TYPE.HTTP_POST);
//                task.execute(map);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            slMain.setVisibility(View.GONE);
            requestType = "GetStates";
        }
    }

    private void getCountries() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            slMain.setVisibility(View.VISIBLE);
            requestType = "GetCountries";
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "1");
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getStateCountriesURL, map, AddNewAddressFragment.this, AddNewAddressFragment.this, PARSER_TYPE.GET_COUNTRIES);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getStateCountriesURL);
                if (dialog != null)
                    dialog.show();

//                HTTPBackgroundTask task = new HTTPBackgroundTask(AddNewAddressFragment.this, getActivity(), PARSER_TYPE.GET_COUNTRIES, APIREQUEST_TYPE.HTTP_POST);
//                task.execute(map);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            slMain.setVisibility(View.GONE);
            requestType = "GetCountries";
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null && results instanceof String) {
            String result = (String) results;
            if (!TextUtils.isEmpty(result)) {
                try {
                    JSONObject object = new JSONObject(result);
                    String status = object.optString("status");
                    String message = object.optString("message");
                    if (status.equalsIgnoreCase("success")) {
                        if (requestType == PARSER_TYPE.GET_COUNTRIES) {
                            if (countriesList != null)
                                countriesList.clear();

                            CountryModel _countryModel = new CountryModel();
                            _countryModel.setCountryId("0");
                            _countryModel.setCountryName("Country");
                            countriesList.add(_countryModel);

                            JSONArray dataArray = object.optJSONArray("data");
                            if (dataArray != null && dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject countryObj = dataArray.optJSONObject(i);
                                    CountryModel countryModel = new CountryModel();
                                    countryModel.setCountryId(countryObj.optString("country_id"));
                                    countryModel.setCountryName(countryObj.optString("name"));

                                    countriesList.add(countryModel);


                                }
                                CountriesSpinnerAdapter adapter = new CountriesSpinnerAdapter(getActivity(), countriesList);
//                                spinCountry.setAdapter(adapter);
                            }
                        } else if (requestType == PARSER_TYPE.GET_STATES) {
                            if (statesList != null)
                                statesList.clear();

                            StateModel _model = new StateModel();
                            _model.setStateId("0");
                            _model.setStateName("Select State");
                            statesList.add(_model);

                            JSONArray dataArray = object.optJSONArray("data");
                            if (dataArray != null && dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject stateObj = dataArray.optJSONObject(i);
                                    StateModel stateModel = new StateModel();
                                    stateModel.setStateId(stateObj.optString("state_id"));
                                    stateModel.setStateName(stateObj.optString("statename"));
                                    statesList.add(stateModel);
                                }
                                StatesSpinnerAdapter adapter = new StatesSpinnerAdapter(getActivity(), statesList);
                                spinState.setAdapter(adapter);

                                int pos = 0;
                                if (userAddressModel != null) {
                                    for (int i = 0; i < statesList.size(); i++) {
                                        if (userAddressModel.getState().equalsIgnoreCase(statesList.get(i).getStateName())) {
                                            pos = i;
                                        }
                                    }

                                    if (isEdit) {
                                        tvHeader.setText(getActivity().getResources().getString(R.string.edit_shipping_address));

                                        etFirstName.setText(userAddressModel.getFirstName());
                                        etLastName.setText(userAddressModel.getLastName());
                                        etAddress.setText(userAddressModel.getAddress());
                                        etAddress1.setText(userAddressModel.getAddress1());
                                        etCity.setText(userAddressModel.getCity());
                                        etPin.setText(userAddressModel.getPin());
                                        etMobile.setText(userAddressModel.getTelephone());
                                        etEmail.setText(userAddressModel.getEmail());
                                        spinState.setSelection(pos);
                                    }

                                }

                            }
                        } else if (requestType == PARSER_TYPE.ADD_ADDRESS || requestType == PARSER_TYPE.EDIT_SHIPPING_ADDRESS) {
                            JSONObject dataObj = object.optJSONObject("data");

                            String addressId = dataObj.optString("address_id");
                            String firstName = dataObj.optString("FirstName");
                            String lastName = dataObj.optString("LastName");
                            String address = dataObj.optString("Address");
                            String address1 = dataObj.optString("Address1");
                            String city = dataObj.optString("City");
                            String pin = dataObj.optString("pin");
                            String state = dataObj.optString("state");
                            String country = dataObj.optString("country");
                            String addressType = dataObj.optString("addressType");
                            String telephone = dataObj.optString("telephone");
                            String email = dataObj.optString("email");

                            userAddressModel = new UserAddressModel();
                            userAddressModel.setAddressId(addressId);
                            userAddressModel.setFirstName(firstName);
                            userAddressModel.setLastName(lastName);
                            userAddressModel.setAddress(address);
                            userAddressModel.setAddress1(address1);
                            userAddressModel.setCity(city);
                            userAddressModel.setPin(pin);
                            userAddressModel.setState(state);
                            userAddressModel.setCountry(country);
                            userAddressModel.setAddressType(addressType);
                            userAddressModel.setTelephone(telephone);
                            userAddressModel.setEmail(email);

                            Intent intent = new Intent(getActivity(), PaymentActivity.class);
                            intent.putExtra("SelectedAddress", userAddressModel);
                            if (isEdit)
                                intent.putExtra("position", position);
                            getActivity().setResult(Activity.RESULT_OK, intent);
                            getActivity().finish();
                        }
                    } else {
                        Utils.showAlertDialog(getActivity(), message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
            }
        } else {
            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {
        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            if (requestType == PARSER_TYPE.GET_COUNTRIES) {
                this.requestType = "GetCountries";
            } else if (requestType == PARSER_TYPE.GET_STATES) {
                this.requestType = "GetStates";
            } else if (requestType == PARSER_TYPE.EDIT_SHIPPING_ADDRESS || requestType == PARSER_TYPE.ADD_ADDRESS) {
                this.requestType = "Address";
            }
            rlAlert.setVisibility(View.VISIBLE);
            slMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if(error!=null)
//            Utils.showAlertWithMessage(getActivity(),getActivity().getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_COUNTRIES) {
                    if (results instanceof ArrayList) {
                        if (countriesList != null)
                            countriesList.clear();
                        countriesList = ((ArrayList<CountryModel>) results);
                        CountriesSpinnerAdapter adapter = new CountriesSpinnerAdapter(getActivity(), countriesList);
//                                spinCountry.setAdapter(adapter);
                    }
                } else if (requestType == PARSER_TYPE.GET_STATES) {
                    if (results instanceof ArrayList) {
                        if (statesList != null)
                            statesList.clear();
                        statesList = (ArrayList<StateModel>) results;

                        StatesSpinnerAdapter adapter = new StatesSpinnerAdapter(getActivity(), statesList);
                        spinState.setAdapter(adapter);

                        int pos = 0;
                        if (userAddressModel != null) {
                            for (int i = 0; i < statesList.size(); i++) {
                                if (userAddressModel.getState().equalsIgnoreCase(statesList.get(i).getStateName())) {
                                    pos = i;
                                }
                            }

                            if (isEdit) {
                                tvHeader.setText(getActivity().getResources().getString(R.string.edit_shipping_address));

                                etFirstName.setText(userAddressModel.getFirstName());
                                etLastName.setText(userAddressModel.getLastName());
                                etAddress.setText(userAddressModel.getAddress());
                                etAddress1.setText(userAddressModel.getAddress1());
                                etCity.setText(userAddressModel.getCity());
                                etPin.setText(userAddressModel.getPin());
                                etMobile.setText(userAddressModel.getTelephone());
                                etEmail.setText(userAddressModel.getEmail());
                                spinState.setSelection(pos);
                            }
                        }
                    }

                } else if (requestType == PARSER_TYPE.ADD_ADDRESS || requestType == PARSER_TYPE.EDIT_SHIPPING_ADDRESS) {
                    if (results instanceof UserAddressModel) {
                        userAddressModel = (UserAddressModel) results;
                        Intent intent = new Intent(getActivity(), PaymentActivity.class);
                        intent.putExtra("SelectedAddress", userAddressModel);
                        if (isEdit)
                            intent.putExtra("position", position);
                        getActivity().setResult(Activity.RESULT_OK, intent);
                        getActivity().finish();
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}
