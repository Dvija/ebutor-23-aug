package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ShipmentTrackingModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import org.json.JSONObject;
import java.util.HashMap;

public class ShipmentTrackingFragment extends Fragment implements VolleyHandler<Object>, Response.ErrorListener {

    private SharedPreferences mSharedPreferences;
    private EditText etShipmentNo;
    private Button btnSearch;
    private LinearLayout llShipping;
    private ScrollView slMain;
    private TextView tvAlertMsg, tvInvoiceNumber, tvOrderDate, tvTotalAmount, tvDeliveryDate, tvShippedDate, tvAddress, tvDeliveryType, tvApproved, tvProcessing, tvShipping, tvDelivery, tvPaymentConf, tvPaymentMode;
    private ImageView image1, image2, image3, image4;
    private String address = "";
    private RelativeLayout rlAlert;
    private Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shipment_tracking, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(),ConstantValues.TOOLBAR_PROGRESS);

        etShipmentNo = (EditText) view.findViewById(R.id.etShipmentNo);
        btnSearch = (Button) view.findViewById(R.id.btnSearch);
        llShipping = (LinearLayout) view.findViewById(R.id.llShipping);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        slMain = (ScrollView) view.findViewById(R.id.sl_main);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
        tvInvoiceNumber = (TextView) view.findViewById(R.id.tvInvoiceNumber);
        tvOrderDate = (TextView) view.findViewById(R.id.tvOrderDate);
        tvTotalAmount = (TextView) view.findViewById(R.id.tvTotalAmount);
        tvDeliveryDate = (TextView) view.findViewById(R.id.tvDeliveryDate);
        tvShippedDate = (TextView) view.findViewById(R.id.tvShippedDate);
        tvDeliveryType = (TextView) view.findViewById(R.id.tvDeliveryType);
        tvApproved = (TextView) view.findViewById(R.id.tvApproved);
        tvProcessing = (TextView) view.findViewById(R.id.tvProcessing);
        tvShipping = (TextView) view.findViewById(R.id.tvShipping);
        tvDelivery = (TextView) view.findViewById(R.id.tvDelivery);
        tvPaymentConf = (TextView) view.findViewById(R.id.tvPaymentConf);
        tvPaymentMode = (TextView) view.findViewById(R.id.tvPaymentMode);
        tvAddress = (TextView) view.findViewById(R.id.tv_address);
        image1 = (ImageView) view.findViewById(R.id.image1);
        image2 = (ImageView) view.findViewById(R.id.image2);
        image3 = (ImageView) view.findViewById(R.id.image3);
        image4 = (ImageView) view.findViewById(R.id.image4);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                if (TextUtils.isEmpty(etShipmentNo.getText().toString())) {
                    etShipmentNo.setError(getActivity().getResources().getString(R.string.please_enter_shipment_no));
                } else {
                    trackOrder();
                }
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    trackOrder();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void trackOrder() {
        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                rlAlert.setVisibility(View.GONE);
                slMain.setVisibility(View.VISIBLE);
                HashMap<String, String> map = new HashMap<>();
                try {
                    JSONObject detailsObj = new JSONObject();
                    detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    detailsObj.put("tracking_id", etShipmentNo.getText().toString());
                    detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    detailsObj.put("le_wh_id",mSharedPreferences.getString(ConstantValues.KEY_WH_IDS,""));
                    detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    map.put("data", detailsObj.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                VolleyBackgroundTask trackingRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.shipmentTrackURL, map, ShipmentTrackingFragment.this, ShipmentTrackingFragment.this, PARSER_TYPE.SHIPMENT_TRACKING);
                trackingRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(trackingRequest, AppURL.shipmentTrackURL);
                if(dialog!=null){
                    dialog.show();
                }
            } else {
                rlAlert.setVisibility(View.VISIBLE);
                slMain.setVisibility(View.GONE);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.please_login_track_shipping));
        }
    }

    /*@Override
    public void onFinish(String results, PARSER_TYPE requestType) {
        if (results != null && results instanceof String) {
            String result = (String) results;
            if (!TextUtils.isEmpty(result)) {
                try {
                    JSONObject object = new JSONObject(result);
                    String status = object.optString("status");
                    String message = object.optString("message");
                    if (status.equalsIgnoreCase("success")) {
                        if (requestType == PARSER_TYPE.SHIPMENT_TRACKING) {
                            JSONObject dataObj = object.optJSONObject("data");

                            JSONObject trackingObj = dataObj.optJSONObject("trackingData");

                            String docketNo = trackingObj.optString("docket_no");
                            String deliveryType = trackingObj.optString("delivery_type");
                            String orderId = trackingObj.optString("order_id");
                            String orderTotal = trackingObj.optString("order_total");
                            String paymentMethod = trackingObj.optString("payment_method");
                            String shippingAddress1 = trackingObj.optString("shipping_address_1");
                            String shippingAddress2 = trackingObj.optString("shipping_address_2");
                            String shippingCity = trackingObj.optString("shipping_city");
                            String shippingPostcode = trackingObj.optString("shipping_postcode");
                            String orderDate = trackingObj.optString("order_date");
                            String sellerInvoiceNo = trackingObj.optString("seller_invoice_no");

                            JSONObject historyObj = dataObj.optJSONObject("history");

                            String processedDate = historyObj.optString("processedDate");
                            String shippedDate = historyObj.optString("shipped");
                            String expectedDeliveryDate = historyObj.optString("expectedDeliveryDate");
                            String deliveryDate = historyObj.optString("deliveryDate");
                            String cancelDate = historyObj.optString("cancelDate");
                            String returnDate = historyObj.optString("returnDate");

                            String docketStatus = dataObj.optString("docketStatus");

                            address = shippingAddress1 + "\n" + (TextUtils.isEmpty(shippingAddress2) ? "" : shippingAddress2 + "\n") + shippingCity + "\n" + shippingPostcode;

                            llShipping.setVisibility(View.VISIBLE);
                            tvInvoiceNumber.setText(sellerInvoiceNo);
                            tvOrderDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN1, orderDate));
                            tvDeliveryDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN2, expectedDeliveryDate));
                            tvShippedDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN1, shippedDate));
                            tvPaymentConf.setText("your order has been " + docketStatus);
                            tvAddress.setText(address);
                            tvPaymentMode.setText(paymentMethod);
                            tvTotalAmount.setText(orderTotal);
                            tvDeliveryType.setText(deliveryType);
                            switch (docketStatus) {
                                case "Shipped":
                                    image1.setImageResource(R.drawable.ic_green_circle);
                                    image2.setImageResource(R.drawable.ic_green_circle);
                                    image3.setImageResource(R.drawable.ic_green_circle);
                                    image4.setImageResource(R.drawable.ic_gray_circle);
                                    break;
                            }
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
    }


    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {
        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            rlAlert.setVisibility(View.VISIBLE);
            slMain.setVisibility(View.GONE);
        }
    }*/

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if(error!=null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e){
        }
//        if(error!=null){
//            Utils.showAlertWithMessage(getActivity(),getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status,String message) {
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (results instanceof ShipmentTrackingModel) {
                ShipmentTrackingModel shipmentTrackingModel = (ShipmentTrackingModel) results;
                String shippingAddress1 = shipmentTrackingModel.getShippingAddress1();
                String shippingAddress2 = shipmentTrackingModel.getShippingAddress2();
                String shippingCity = shipmentTrackingModel.getShippingCity();
                String shippingPostcode = shipmentTrackingModel.getShippingPostcode();
                String sellerInvoiceNo = shipmentTrackingModel.getSellerInvoiceNo();
                String orderDate = shipmentTrackingModel.getOrderDate();
                String expectedDeliveryDate = shipmentTrackingModel.getExpectedDeliveryDate();
                String shippedDate = shipmentTrackingModel.getShippedDate();
                String docketStatus = shipmentTrackingModel.getDocketStatus();
                String paymentMethod = shipmentTrackingModel.getPaymentMethod();
                String orderTotal = shipmentTrackingModel.getOrderTotal();
                String deliveryType = shipmentTrackingModel.getDeliveryType();

                address = shippingAddress1 + "\n" + (TextUtils.isEmpty(shippingAddress2) ? "" : shippingAddress2 + "\n") + shippingCity + "\n" + shippingPostcode;

                llShipping.setVisibility(View.VISIBLE);
                tvInvoiceNumber.setText(sellerInvoiceNo);
                tvOrderDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN1, orderDate));
                tvDeliveryDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN2, expectedDeliveryDate));
                tvShippedDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN1, shippedDate));
                tvPaymentConf.setText(getActivity().getResources().getString(R.string.your_order_has_been)+" " + docketStatus);
                tvAddress.setText(address);
                tvPaymentMode.setText(paymentMethod);
                tvTotalAmount.setText(orderTotal);
                tvDeliveryType.setText(deliveryType);
                switch (docketStatus) {
                    case "Shipped":
                        image1.setImageResource(R.drawable.ic_green_circle);
                        image2.setImageResource(R.drawable.ic_green_circle);
                        image3.setImageResource(R.drawable.ic_green_circle);
                        image4.setImageResource(R.drawable.ic_gray_circle);
                        break;
                }
            }
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(),message);
    }

}
