package com.ebutor.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.AttachmentsActivity;
import com.ebutor.activities.SubmitExpenseActivity;
import com.ebutor.adapters.AddExpenseTypeSpinnerAdapter;
import com.ebutor.adapters.AddExpensesAdapter;
import com.ebutor.adapters.ExpenseTypeSpinnerAdapter;
import com.ebutor.backgroundtask.MultipartEntity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.models.ExpenseMasterLookupData;
import com.ebutor.models.ExpenseMasterlookupModel;
import com.ebutor.models.ImagePathData;
import com.ebutor.models.NewExpenseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.RuntimePermissionUtils;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Srikanth Nama on 29-Dec-16.
 */

public class AddExpensePotraitFragment extends Fragment implements RecyclerView.OnItemTouchListener, View.OnClickListener, VolleyHandler<Object>, Response.ErrorListener, AddExpensesAdapter.OnItemClickListener, ActionMode.Callback {

    public static final int CAMERA_PERMISSION_REQUEST_CODE = 100;
    public static final int WRITE_EXTERNAL_PERMISSION_REQUEST_CODE = 101;
    public static final int GALLERY_PERMISSION_REQUEST_CODE = 102;
    private static final int REQUEST_PERMISSION_SETTING = 125;
    ProgressDialog dialog;
    String mCurrentPhotoPath;
    GestureDetectorCompat gestureDetector;
    //    ActionMode actionMode;
    View.OnClickListener onDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            /*if (view.getTag() != null) {
                ImagePathData path = (ImagePathData) view.getTag();
                if (imagePathDataArrayList != null && imagePathDataArrayList.contains(path)) {
                    imagePathDataArrayList.remove(path);
                }

                if (imagePathDataArrayList == null || imagePathDataArrayList.size() == 0) {
//                    imagesScrollView.setVisibility(View.GONE);
                }
                imageContainer.removeAllViews();
                //
                for (ImagePathData s : imagePathDataArrayList) {
                    imageContainer.addView(setPic(s));
                }
            }*/
        }
    };
    private String selIds = "";
    private MenuItem delItem, uplItem;
    private ArrayList<NewExpenseModel> arrData = new ArrayList<>(), selData = new ArrayList<>();
    private ActionSlideExpandableListView mActionSlideExpandableListView;
    private EditText etAmount, etDate, etDesc, etReference;
    private Spinner spinExpType;
    private TextView /*btnSubmit,*/tvNoResults, tvBadge;
    private ImageView ivCamera, ivGallery;
    private String expenseTypeId, expenseType;
    private String expenseDate;
    private int _submitType = 0, imageCount = 0;
    //    private HorizontalScrollView imagesScrollView;
//    private LinearLayout imageContainer;
    private ArrayList<ImagePathData> imagePathDataArrayList;
    //    private RecyclerView rvExpenses;
    private ImageView ivAddExpense;
    private String attachmentIds;
    private NewExpenseModel newExpenseModel;
    private SharedPreferences mSharedPreferences;
    private int position = 0;
    private AddExpensesAdapter adapter;

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static ArrayList<String> filterIds(Collection<NewExpenseModel> target, Predicate<NewExpenseModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (NewExpenseModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getExpenseId());
            }
        }
        return result;
    }

    public static ArrayList<NewExpenseModel> filterData(Collection<NewExpenseModel> target, Predicate<NewExpenseModel> predicate) {
        ArrayList<NewExpenseModel> result = new ArrayList<NewExpenseModel>();
        for (NewExpenseModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_expense_potrait, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("Type")) {
                //type 1 == for Expense
                //type 2 == for Advance
                //type 3 == for Attach Unclaimed expenses with Main Record
                _submitType = extras.getInt("Type", 1);// default value 1
            }
        }
        imagePathDataArrayList = new ArrayList<>();

        etAmount = (EditText) view.findViewById(R.id.et_amount);
        etDate = (EditText) view.findViewById(R.id.et_date);
        etDesc = (EditText) view.findViewById(R.id.et_desc);
        etReference = (EditText) view.findViewById(R.id.et_reference);
        ivCamera = (ImageView) view.findViewById(R.id.camera);
        ivGallery = (ImageView) view.findViewById(R.id.gallery);
        ivAddExpense = (ImageView) view.findViewById(R.id.iv_add_expense);
        tvBadge = (TextView) view.findViewById(R.id.tvBadge);
        tvNoResults = (TextView) view.findViewById(R.id.tv_no_results);
        mActionSlideExpandableListView = (ActionSlideExpandableListView) view.findViewById(R.id.list);

//        rvExpenses = (RecyclerView) view.findViewById(R.id.rv_expenses);
//        rvExpenses.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

//        imagesScrollView = (HorizontalScrollView) view.findViewById(R.id.horizontal_scroll_view);
//        imageContainer = (LinearLayout) view.findViewById(R.id.ll_images);

        adapter = new AddExpensesAdapter(getActivity(), arrData);
        adapter.setClickListener(AddExpensePotraitFragment.this);
        mActionSlideExpandableListView.setAdapter(adapter);

        spinExpType = (Spinner) view.findViewById(R.id.spin_expense_type);
//        btnSubmit = (TextView) view.findViewById(R.id.btn_submit);

        Calendar calendar = Calendar.getInstance();
        expenseDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5,
                calendar.get(Calendar.DAY_OF_MONTH) + " " + (calendar.get(Calendar.MONTH) + 1) + " " + calendar.get(Calendar.YEAR));
        etDate.setText(expenseDate);

        ivAddExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String amount = etAmount.getText().toString().trim();
                String desc = etDesc.getText().toString().trim();
                String refId = etReference.getText().toString().trim();

                if (TextUtils.isEmpty(amount)) {
                    etAmount.requestFocus();
                    etAmount.setError("Please enter Amount");
                    return;
                }
                boolean isValidAmount = true;
                try {
                    double _amount = 0;
                    _amount = Double.parseDouble(amount);
                    if (_amount <= 0.0) {
                        isValidAmount = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isValidAmount = false;
                }

                if (!isValidAmount) {
                    etAmount.requestFocus();
                    etAmount.setError("Please enter valid Amount");
                    return;
                }

                if (TextUtils.isEmpty(expenseTypeId)) {
                    Utils.showAlertWithMessage(getActivity(), "Please select Expense Type");
                    return;
                }

                if (TextUtils.isEmpty(desc)) {
                    etDesc.requestFocus();
                    etDesc.setError("Please enter Description");
                    return;
                }
//                btnSubmit.setEnabled(false);

                if (dialog != null && !dialog.isShowing())
                    dialog.show();

                newExpenseModel = new NewExpenseModel("0", amount, expenseDate, expenseType, expenseTypeId, desc, "");

                if (imagePathDataArrayList != null && imagePathDataArrayList.size() > 0) {
                    //we have images to be uploaded, please upload
                    if (Networking.isNetworkAvailable(getActivity())) {
                       /* for (ImagePathData imagePath : imagePathDataArrayList) {
                            beginUpload(imagePath);
                        }*/
                        new UploadImageTask().execute(imagePathDataArrayList.get(position).getImagePath());
                    } else {
                        attachmentIds = TextUtils.join(",", imagePathDataArrayList);
                        // Internet is not available, Please store the info in Local DB
                        newExpenseModel.setAttachments(attachmentIds);
                        //todo store in DB
                        submitRequest(newExpenseModel);
                    }

                } else {
                    //todo submit request to server/Local DB
                    submitRequest(newExpenseModel);
                }

            }
        });


        spinExpType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null && view.getTag() instanceof ExpenseMasterLookupData) {
                    ExpenseMasterLookupData data = (ExpenseMasterLookupData) view.getTag();
                    expenseType = data.getMasterLookupName();
                    expenseTypeId = data.getValue();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();

            }
        });

        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagePathDataArrayList == null) {
                    return;
                }

                try {
                    String attachments = "";
                    for (int i = 0; i < imagePathDataArrayList.size(); i++) {
                        attachments += imagePathDataArrayList.get(i).getImagePath() + ",";
                    }
                    if (attachments.endsWith(",")) {
                        attachments = attachments.substring(0, attachments.length() - 1);
                    }
                    if (null != attachments && !TextUtils.isEmpty(attachments)) {
                        // go to display images

                        Intent intent = new Intent(getActivity(), AttachmentsActivity.class);
                        intent.putExtra("isLocalPath", true);
                        intent.putExtra("Attachments", attachments);
                        startActivity(intent);
                    } else {
                        //do nothing
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year, month, day;
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        expenseDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, dayOfMonth + " " + (monthOfYear + 1) + " " + year);

                        etDate.setText(expenseDate);

                    }
                }, year, month, day);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 21);
                calendar.set(Calendar.MINUTE, 59);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });
        getMasterLookupData();

    }

    private void selectImage() {

        final CharSequence[] options = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.upload_profile_pic));
        builder.setCancelable(true);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {
                    int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.CAMERA);

                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                            RuntimePermissionUtils.showRationale(getActivity(),
                                    AddExpensePotraitFragment.this,
                                    Manifest.permission.CAMERA,
                                    CAMERA_PERMISSION_REQUEST_CODE,
                                    "You need to allow access to Camera");
                        } else {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    CAMERA_PERMISSION_REQUEST_CODE);

                        }
                        return;
                    }

                    int _permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if (_permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            RuntimePermissionUtils.showRationale(getActivity(),
                                    AddExpensePotraitFragment.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    WRITE_EXTERNAL_PERMISSION_REQUEST_CODE,
                                    "You need to allow access to Storage");
                        } else {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    WRITE_EXTERNAL_PERMISSION_REQUEST_CODE);

                        }
                        return;
                    }

                    dispatchTakePictureIntent();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.READ_EXTERNAL_STORAGE);

                        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                RuntimePermissionUtils.showRationale(getActivity(),
                                        AddExpensePotraitFragment.this,
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        GALLERY_PERMISSION_REQUEST_CODE,
                                        "You need to allow access to Gallery");
                            } else {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                        GALLERY_PERMISSION_REQUEST_CODE);

                            }
                            return;
                        }
                        startGallery();

                    }


                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public String uploadFile(String filePath) {

        String stringResponse = "";
        String charset = "UTF-8";

        try {

            MultipartEntity multipart = new MultipartEntity(AppURL.uploadImagesURL, charset);

            multipart.addFormField("_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            multipart.addFormField("entity", "expenses");
            multipart.addFilePart("image", new File(filePath));


            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");
            StringBuilder sb = new StringBuilder();
            for (String line : response) {
                System.out.println(line);
                sb.append(line);
            }

            return sb.toString();
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void submitRequest(NewExpenseModel model) {
        if (Networking.isNetworkAvailable(getActivity())) {
            saveExpenseItem(model);

        } else {
            long rowId = DBHelper.getInstance().insertExpenseInDB(model);
            if (rowId < 0) {
//                btnSubmit.setEnabled(true);
                //oops failed to insert
                Toast.makeText(getActivity(), "oops..! failed to Save record. Please try again.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Successfully Saved record.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }

        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    private void saveExpenseItem(NewExpenseModel model) {
        if (Networking.isNetworkAvailable(getActivity())) {
            if (dialog != null && !dialog.isShowing()) {
                dialog.show();
            }
            if (imagePathDataArrayList != null && imagePathDataArrayList.size() > 0)
                attachmentIds = TextUtils.join(",", imagePathDataArrayList);
            String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
            try {
                JSONObject obj = new JSONObject();
                obj.put("ExpID", "0");
                obj.put("ExpDetActualAmount", model.getAmount());
                obj.put("ExpDetType", model.getExpenseTypeId());
                obj.put("ExpDetDate", model.getDate());
                obj.put("Description", model.getDesc());
                obj.put("ExpDetProofKey", attachmentIds);
                obj.put("UserID", userId);
                obj.put("ExpDetRecordType", "0"); // 0-for untracked 1-for tracked
//                HashMap<String, String> map1 = new HashMap<>();
//                map1.put("data", obj.toString());

                Log.e("Save Expense Line Item", obj.toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.saveExpenseLineItemURL, obj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        btnSubmit.setEnabled(true);
                        Log.e("Save Expense Line Item", response.toString());
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();

                        String status = response.optString("status");
                        String message = response.optString("message");
                        if (status.equalsIgnoreCase("success")) {
                            etAmount.setText("");
                            etDesc.setText("");
                            etReference.setText("");
                            spinExpType.setSelection(0);
                            imageCount = 0;
                            tvBadge.setText(String.valueOf(imageCount));
                            imagePathDataArrayList = new ArrayList<>();
                            Calendar calendar = Calendar.getInstance();
                            expenseDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5,
                                    calendar.get(Calendar.DAY_OF_MONTH) + " " + (calendar.get(Calendar.MONTH) + 1) + " " + calendar.get(Calendar.YEAR));
                            etDate.setText(expenseDate);
                            getData();

//                            getActivity().finish();
                        } else {
                            Utils.showAlertDialog(getActivity(), message);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        btnSubmit.setEnabled(true);
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        // something to do here ??
                        return ConstantValues.getRequestHeaders();
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(request, AppURL.saveExpenseLineItemURL);

                if (dialog != null && !dialog.isShowing())
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
//                btnSubmit.setEnabled(true);
            }

        }
    }

    private void getData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
                String url = AppURL.getExpensesLineItemsURL + "?UserID=" + userId + "&RecordType=0";
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, url, null, AddExpensePotraitFragment.this, AddExpensePotraitFragment.this, PARSER_TYPE.UNCLAIMED_EXPENSES);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void getMasterLookupData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, AppURL.getExpenseMasterValuesURL, null, AddExpensePotraitFragment.this, AddExpensePotraitFragment.this, PARSER_TYPE.GET_EXPENSE_TYPES);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getExpenseMasterValuesURL);

                /*dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, 1);
    }

    private void startGallery() {
        /*Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 2);*/

        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 19) {
            // For Android versions of KitKat or later, we use a
            // different intent to ensure
            // we can get the file path from the returned intent URI
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }

        intent.setType("image/*");
        startActivityForResult(intent, 2);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    int _permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if (_permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            RuntimePermissionUtils.showRationale(getActivity(),
                                    AddExpensePotraitFragment.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    WRITE_EXTERNAL_PERMISSION_REQUEST_CODE,
                                    "You need to allow access to Storage");
                        } else {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    WRITE_EXTERNAL_PERMISSION_REQUEST_CODE);

                        }
                        return;
                    }

//                    startCamera();
                    dispatchTakePictureIntent();
                } else {
                    boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.CAMERA);
                    if (!showRationale) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                AddExpensePotraitFragment.this,
                                Manifest.permission.READ_CONTACTS,
                                REQUEST_PERMISSION_SETTING,
                                "You need to allow permission manually from app setting",
                                true);

                    } else if (Manifest.permission.CAMERA.equals(permissions[0])) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                AddExpensePotraitFragment.this,
                                Manifest.permission.READ_CONTACTS,
                                CAMERA_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Camera");

                    }

                }
                break;
            case WRITE_EXTERNAL_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startCamera();
                    dispatchTakePictureIntent();
                } else {
                    if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permissions[0])) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                AddExpensePotraitFragment.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                WRITE_EXTERNAL_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Camera");

                    }

                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startGallery();
                } else {
                    if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(permissions[0])) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                AddExpensePotraitFragment.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                GALLERY_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Gallery");

                    }

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            int hasPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
            if (hasPermission == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            }

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            imageCount++;

            tvBadge.setText(String.valueOf(imageCount));

            ImagePathData imagePathData = new ImagePathData();
            imagePathData.setImagePath(mCurrentPhotoPath);
            imagePathData.setImageKey(Utils.getAWSFilePath() + ".png");

//            imagesScrollView.setVisibility(View.VISIBLE);
//            imageContainer.addView(setPic(imagePathData));

            imagePathDataArrayList.add(imagePathData);// Add Captured image path to list

        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            imageCount++;
            tvBadge.setText(String.valueOf(imageCount));

            try {
                String path = getPath(uri);

                ImagePathData imagePathData = new ImagePathData();
                imagePathData.setImagePath(path);
                imagePathData.setImageKey(Utils.getAWSFilePath() + ".png");
                imagePathDataArrayList.add(imagePathData);// Add picked image path to the list


//                imagesScrollView.setVisibility(View.VISIBLE);
//                imageContainer.addView(setPic(imagePathData));

//                beginUpload(path);
            } catch (URISyntaxException e) {
                Toast.makeText(getActivity(),
                        "Unable to get the file from the given URI.  See error log for details",
                        Toast.LENGTH_LONG).show();
                Log.e("onActivityResult", "Unable to upload file from the given uri", e);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private View setPic(ImagePathData imagePath) {
        // Get the dimensions of the View
        int targetW = (int) (100 * Resources.getSystem().getDisplayMetrics().density);
        int targetH = (int) (75 * Resources.getSystem().getDisplayMetrics().density);

        View child = getActivity().getLayoutInflater().inflate(R.layout.image_container_layout, null);
        ImageView mImageView = (ImageView) child.findViewById(R.id.image_preview);
        ImageButton mClose = (ImageButton) child.findViewById(R.id.button_close);
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (bmOptions.outWidth / scale / 2 >= REQUIRED_SIZE
                && bmOptions.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        bmOptions.inSampleSize = scale;
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath.getImagePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath.getImagePath(), bmOptions);
        mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
        mClose.setTag(imagePath);
        mClose.setOnClickListener(onDeleteClickListener);

        return child;

    }

    /*
   * Gets the file path of the given Uri.
   */
    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getActivity(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /*
     * Begins to upload the file specified by the file path.
     */
    private void beginUpload(ImagePathData filePath) {
        if (filePath == null) {
            Toast.makeText(getActivity(), "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath.getImagePath());
//        TransferObserver observer = transferUtility.upload(ConstantValues.AWS_BUCKET_NAME, filePath.getImageKey(), file);
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
//        observer.setTransferListener(new UploadListener());
//        observerArrayList.add(observer);
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
        }
        Uri photoURI = Uri.fromFile(photoFile); // create
        if (photoFile != null) {
            try {
                mCurrentPhotoPath = getPath(photoURI);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            i.putExtra(MediaStore.EXTRA_OUTPUT, photoURI); // set the image file
            startActivityForResult(i, 1);
        } else {
            Toast.makeText(getActivity(), "Unable to create URI", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_EXPENSE_TYPES) {
                    if (response instanceof ExpenseMasterlookupModel) {

                        ExpenseMasterlookupModel model = (ExpenseMasterlookupModel) response;
                        ArrayList<ExpenseMasterLookupData> expenses = model.getExpenseTypes();
                        ArrayList<ExpenseMasterLookupData> requestTypes = model.getRequestTypes();

                        if (_submitType == 1) {//type 1 == for Expense
                            if (expenses != null && expenses.size() > 0) {
                                AddExpenseTypeSpinnerAdapter adapter = new AddExpenseTypeSpinnerAdapter(getActivity(), expenses);
                                spinExpType.setAdapter(adapter);
                            }
                        } else if (_submitType == 2) {// type 2 == for Advance
                            if (requestTypes != null && requestTypes.size() > 0) {
                                ExpenseTypeSpinnerAdapter adapter = new ExpenseTypeSpinnerAdapter(getActivity(), requestTypes);
                                spinExpType.setAdapter(adapter);
                            }
                        }

                    }
                } else if (requestType == PARSER_TYPE.UNCLAIMED_EXPENSES) {
                    if (response instanceof ArrayList) {
                        arrData = (ArrayList<NewExpenseModel>) response;
                        if (arrData != null && arrData.size() > 0) {
                            mActionSlideExpandableListView.setVisibility(View.VISIBLE);
                            tvNoResults.setVisibility(View.GONE);

                            adapter = new AddExpensesAdapter(getActivity(), arrData);
                            adapter.setClickListener(AddExpensePotraitFragment.this);
                            mActionSlideExpandableListView.setAdapter(adapter);
                        } else {
                            mActionSlideExpandableListView.setVisibility(View.GONE);
                            tvNoResults.setVisibility(View.VISIBLE);
                            tvNoResults.setText(getString(R.string.no_results));
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }

        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {

    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.menu_action_mode, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    private void deleteUnClaimedExpenses(String commaSeparatedIds) {
        if (Networking.isNetworkAvailable(getActivity())) {
            String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
            try {
                JSONObject obj = new JSONObject();
                obj.put("unclaimedID", commaSeparatedIds);
                obj.put("UserID", userId);

                Log.e("request Obj", obj.toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.deleteUnClaimedExpensesURL, obj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();

                        String status = response.optString("status");
                        String message = response.optString("message");
                        if (status.equalsIgnoreCase("success")) {
                            Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            getData();
                        } else {
                            Utils.showAlertDialog(getActivity(), message);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        // something to do here ??
                        return ConstantValues.getRequestHeaders();
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(request, AppURL.deleteUnClaimedExpensesURL);

                if (dialog != null && !dialog.isShowing())
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        /*switch (item.getItemId()) {
            case R.id.upload:
                ArrayList<NewExpenseModel> selectedItems = adapter.getSelectedItems();
                Intent intent = new Intent(getActivity(), SubmitExpenseActivity.class);
                intent.putExtra("Type", 3);//for Attaching line items with a new Main Item
                intent.putParcelableArrayListExtra("ExpensesList", selectedItems);
                startActivity(intent);
                actionMode.finish();
                return true;
            case R.id.delete:
                String selectedIds = adapter.getSelectedIds();
                deleteUnClaimedExpenses(selectedIds);
//                Toast.makeText(getActivity(), selectedIds, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }*/
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
//        this.actionMode = null;
        /*adapter.clearSelections();*/
    }

    @Override
    public void onClick(View view) {
        /*int idx = rvExpenses.getChildLayoutPosition(view);
        if (actionMode != null) {
            myToggleSelection(idx);
        }*/
    }

    @Override
    public void onCheckChanged() {
        Predicate<NewExpenseModel> isCheckedFilters = new Predicate<NewExpenseModel>() {
            @Override
            public boolean apply(NewExpenseModel newExpenseModel) {
                return newExpenseModel.isChecked();
            }
        };

        selIds = "";
        if (arrData != null && arrData.size() > 0) {
            ArrayList<String> checkedFilters = filterIds(arrData, isCheckedFilters);
            selData = filterData(arrData, isCheckedFilters);
            selIds = TextUtils.join(",", checkedFilters);

        }

        if (getActivity() instanceof SubmitExpenseActivity) {
            delItem = ((SubmitExpenseActivity) getActivity()).delItem;
            uplItem = ((SubmitExpenseActivity) getActivity()).uplItem;
        }

        if (!TextUtils.isEmpty(selIds)) {
            if (delItem != null) {
                delItem.setVisible(true);
            }
            if (uplItem != null) {
                uplItem.setVisible(true);
            }
        } else {
            if (delItem != null) {
                delItem.setVisible(false);
            }
            if (uplItem != null) {
                uplItem.setVisible(false);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.delete) {
            Toast.makeText(getActivity(), "del", Toast.LENGTH_SHORT).show();
            deleteUnClaimedExpenses(selIds);
        } else if (item.getItemId() == R.id.upload) {
            Toast.makeText(getActivity(), "upl", Toast.LENGTH_SHORT).show();
            if (selData != null && selData.size() > 0) {
                Intent intent = new Intent(getActivity(), SubmitExpenseActivity.class);
                intent.putExtra("Type", 3);//for Attaching line items with a new Main Item
                intent.putParcelableArrayListExtra("ExpensesList", selData);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private class UploadImageTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (position == 0)
                progressDialog = ProgressDialog.show(getActivity(), "", "Please wait..!", true);
        }

        @Override
        protected String doInBackground(String... strings) {
            return uploadFile(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s != null) {
                try {
                    JSONObject resultObj = new JSONObject(s);
                    String status = resultObj.optString("status");
                    String message = resultObj.optString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject dataObj = resultObj.optJSONObject("data");
                        String url = dataObj.optString("url");
                        imagePathDataArrayList.get(position).setImageKey(url);
                        position++;
                        if (position < imagePathDataArrayList.size()) {
                            new UploadImageTask().execute(imagePathDataArrayList.get(position).getImagePath());
                        } else {
                            //upload images are done.. Now upload the expense
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            submitRequest(newExpenseModel);
                        }
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /*private class RecyclerViewDemoOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = rvExpenses.findChildViewUnder(e.getX(), e.getY());
            onClick(view);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            View view = rvExpenses.findChildViewUnder(e.getX(), e.getY());
            if (actionMode != null) {
                return;
            }
            // Start the CAB using the ActionMode.Callback defined above
            actionMode = getActivity().startActionMode(AddExpenseFragment.this);
            int idx = rvExpenses.getChildPosition(view);
            myToggleSelection(idx);
            super.onLongPress(e);
        }
    }*/

}

