package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.ReasonsSpinnerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ReasonsModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CancelRequestFragment extends DialogFragment implements AdapterView.OnItemSelectedListener, Response.ErrorListener, VolleyHandler<Object> {
    EditText etComments;
    Spinner reasons;
    Button btnCancel;
    ArrayList<ReasonsModel> reasonArray;
    OnDialogClosed onDialogClosed;
    private SharedPreferences mSharedPreferences;
    private String productId = "", variantId = "", orderId = "", comments = "";
    private String selectedReasonId, selectedReason;
    private ReasonsSpinnerAdapter adapter;
    private boolean orderStatustype;
    private TextView tvTitle;
    private Dialog dialog;


    public static CancelRequestFragment newInstance(ArrayList<ReasonsModel> reasonsArray, String productId, String variantId, String orderId, boolean isCancelOrderStatus) {

        CancelRequestFragment f = new CancelRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putString("productId", productId);
        bundle.putString("variantId", variantId);
        bundle.putString("orderId", orderId);
        bundle.putBoolean("isCancelOrderStatus", isCancelOrderStatus);
        bundle.putSerializable("reasons", reasonsArray);
        f.setArguments(bundle);

        return f;
    }

    public void setClickListener(OnDialogClosed listener) {
        onDialogClosed = listener;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if(error!=null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e){
        }
//        if(error!=null){
//            Utils.showAlertWithMessage(getActivity(),getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (response != null) {
            if(status.equalsIgnoreCase("success")) {
                Utils.showAlertDialog(getActivity(), message);
                dismiss();
                onDialogClosed.dialogClose(orderId);
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_return_request, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        theme = android.R.style.Theme_Holo_Dialog_NoActionBar;
        setStyle(style, theme);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.getSerializable("reasons") != null) {
            reasonArray = (ArrayList<ReasonsModel>) args.getSerializable("reasons");
        }
        if (args != null && args.getString("productId") != null) {
            productId = args.getString("productId");
        }
        if (args != null && args.getString("variantId") != null) {
            variantId = args.getString("variantId");
        }
        if (args != null && args.getString("orderId") != null) {
            orderId = args.getString("orderId");
        }

        if (args != null && args.getBoolean("isCancelOrderStatus")) {
            orderStatustype = args.getBoolean("isCancelOrderStatus");
        }
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        etComments = (EditText) view.findViewById(R.id.et_remarks);
        reasons = (Spinner) view.findViewById(R.id.reasonsSpinner);
        btnCancel = (Button) view.findViewById(R.id.btn_return_request);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);
        tvTitle.setText(getActivity().getResources().getString(R.string.cancel_request));

        selectedReason = reasonArray.get(0).getReason();
        selectedReasonId = reasonArray.get(0).getReasonValue();
//        ArrayAdapter<ReasonsModel> adapter = new ArrayAdapter<ReasonsModel>(getActivity(), android.R.layout.simple_spinner_dropdown_item, reasonArray);
//        reasons.setAdapter(adapter);

        adapter = new ReasonsSpinnerAdapter(getActivity(), reasonArray);
        reasons.setAdapter(adapter);
        reasons.setOnItemSelectedListener(this);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null)
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                comments = etComments.getText().toString().trim();
                if (!TextUtils.isEmpty(comments)) {
                    cancelOrder();
                } else {
                    etComments.setError(getActivity().getResources().getString(R.string.please_enter_remarks_cancel));
                }
            }
        });

    }

    private void cancelOrder() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                if (orderStatustype) {
                    JSONObject obj = new JSONObject();
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE,""));
                    obj.put("le_wh_id",mSharedPreferences.getString(ConstantValues.KEY_WH_IDS,""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID,""));
                    obj.put("orderID", orderId);
                    obj.put("reason_id", selectedReasonId);
                    obj.put("comments", comments);
                    if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                        obj.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                        obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                    }
                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask cancelOrderReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.cancelOrderURL, map, CancelRequestFragment.this, CancelRequestFragment.this, PARSER_TYPE.CANCEL_ENTIRE_ORDER);
                    cancelOrderReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    MyApplication.getInstance().addToRequestQueue(cancelOrderReq, AppURL.cancelOrderURL);
                    if (dialog != null)
                        dialog.show();
                } else {
                    JSONObject obj = new JSONObject();
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE,""));
                    obj.put("le_wh_id",mSharedPreferences.getString(ConstantValues.KEY_WH_IDS,""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID,""));
                    obj.put("orderID", orderId);
                    obj.put("reason_id", selectedReasonId);
                    obj.put("comments", comments);
                    obj.put("product_id", productId);
                    if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                        obj.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                        obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                    }
                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask cancelOrderReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.cancelOrderURL, map, CancelRequestFragment.this, CancelRequestFragment.this, PARSER_TYPE.CANCEL_ORDER);
                    cancelOrderReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    MyApplication.getInstance().addToRequestQueue(cancelOrderReq, AppURL.cancelOrderURL);
                    if (dialog != null)
                        dialog.show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            dismiss();
            onDialogClosed.dialogClose("No Network");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (view != null) {
            Object obj = view.getTag();
            if (obj != null && obj instanceof ReasonsModel) {
                ReasonsModel stateModel = (ReasonsModel) obj;
                selectedReasonId = stateModel.getReasonValue();
                selectedReason = stateModel.getReason();
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface OnDialogClosed {
        public void dialogClose(String orderId);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(),message);
    }

}
