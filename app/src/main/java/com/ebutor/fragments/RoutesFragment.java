package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.AddBeatActivity;
import com.ebutor.MapsActivity;
import com.ebutor.MyApplication;
import com.ebutor.OutletsActivity;
import com.ebutor.R;
import com.ebutor.adapters.RoutesAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.models.BeatModel;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.ebutor.utils.fastscroll.AlphabetItem;
import com.ebutor.utils.fastscroll.RecyclerViewFastScroller;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class RoutesFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, RoutesAdapter.OnBeatClickListener {


    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ActionSlideExpandableListView mActionSlideExpandableListView;
    private RecyclerViewFastScroller mRecyclerViewFastScroller;
    private RoutesAdapter routesAdapter;
    private List<AlphabetItem> mAlphabetItems;
    private ArrayList<BeatModel> arrBeats;
    private EditText etSearch;
    private TextView tvNoItems;
    private LinearLayout llUnbilledOutlets;
    private ImageView ivAddBeat;
//    private FloatingActionButton fab;
//    private LinearLayout llRoutes, llMap;
//    private boolean isMap = false;
//    private GoogleMap map;
//    private ArrayList<LatLng> markerPoints;

    public static RoutesFragment newInstance() {
        RoutesFragment fragment = new RoutesFragment();
        return fragment;
    }

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_routes, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TABS_PROGRESS);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        mActionSlideExpandableListView = (ActionSlideExpandableListView) view.findViewById(R.id.unbilled_outlets_list);
        mRecyclerViewFastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.fast_scroller);
        etSearch = (EditText) view.findViewById(R.id.input_search_query);
        tvNoItems = (TextView) view.findViewById(R.id.tv_no_items);
        llUnbilledOutlets = (LinearLayout) view.findViewById(R.id.ll_unbilled_outlets);
        ivAddBeat = (ImageView) view.findViewById(R.id.iv_add_beat);
//        llRoutes = (LinearLayout) view.findViewById(R.id.ll_routes);
//        llMap = (LinearLayout) view.findViewById(R.id.ll_map);
//        fab = (FloatingActionButton) view.findViewById(R.id.fab);
//        fab.setVisibility(View.VISIBLE);

        if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.MANAGE_BEATS_FEATURE_CODE) > 0) {
            ivAddBeat.setVisibility(View.VISIBLE);
        } else {
            ivAddBeat.setVisibility(View.GONE);
        }

        arrBeats = new ArrayList<>();

        routesAdapter = new RoutesAdapter(getActivity(), arrBeats);
        routesAdapter.setBeatClickListener(RoutesFragment.this);

//        mAlphabetItems = new ArrayList<>();
//        List<String> strAlphabets = new ArrayList<>();
//
//        for (int i = 0; i < retailersModelArrayList.size(); i++) {
//            String name = retailersModelArrayList.get(i).getCompany();
//            if (name == null || name.trim().isEmpty())
//                continue;
//
//            String word = name.substring(0, 1).toUpperCase();
//            if (!strAlphabets.contains(word)) {
//                strAlphabets.add(word);
//                mAlphabetItems.add(new AlphabetItem(i, word, false));
//            }
//        }

        mActionSlideExpandableListView.setAdapter(routesAdapter);

//        mRecyclerViewFastScroller.setRecyclerView(buyersList);
//        mRecyclerViewFastScroller.setUpAlphabet(mAlphabetItems);

        mActionSlideExpandableListView.setItemActionListener(new ActionSlideExpandableListView.OnActionClickListener() {
            @Override
            public void onClick(View itemView, View clickedView, int position) {
                if (clickedView.getId() == R.id.btn_navigation) {
                    Toast.makeText(getActivity(), "navigation", Toast.LENGTH_SHORT).show();
                } else if (clickedView.getId() == R.id.btn_check_in) {
                    Toast.makeText(getActivity(), "check out", Toast.LENGTH_SHORT).show();
                } else if (clickedView.getId() == R.id.btn_total_orders) {
                    Toast.makeText(getActivity(), "total orders", Toast.LENGTH_SHORT).show();
                } else if (clickedView.getId() == R.id.btn_feed_back) {
                    Toast.makeText(getActivity(), "feed back", Toast.LENGTH_SHORT).show();
                } else {

                }
            }
        });

//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (isMap) {
//                    llRoutes.setVisibility(View.VISIBLE);
//                    llMap.setVisibility(View.GONE);
//                } else {
//                    llRoutes.setVisibility(View.GONE);
//                    llMap.setVisibility(View.VISIBLE);
//                    loadMap();
//                }
//            }
//        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBeats(createJSON(), true);
            }
        });

        ivAddBeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddBeatActivity.class);
                startActivity(intent);
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (routesAdapter != null) {
                    String text = etSearch.getText().toString().toLowerCase(Locale.getDefault());
                    routesAdapter.getFilter().filter(text);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

    }

//    private void loadMap() {
//        // Getting reference to SupportMapFragment of the activity_main
//        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
//
//        // Getting Map for the SupportMapFragment
//        map = fm.getMap();
//
//        if (map != null) {
//
//            // Enable MyLocation Button in the Map
//            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                return;
//            }
//
//            map.setMyLocationEnabled(true);
//            markerPoints = new ArrayList<>();
//            MarkerOptions options = new MarkerOptions();
//            for(int i=0;i<arrBeats.size();i++){
////                options.position(new LatLng(arrBeats.get(i).get));
////                map.addMarker(options);
//            }
//        }
//    }

    private void getBeats(JSONObject dataObject, boolean isSwipeDown) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getBeatsByFFIDURL, map, RoutesFragment.this, RoutesFragment.this, PARSER_TYPE.GET_BEATS_BY_FF_ID);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getBeatsByFFIDURL);
                if (dialog != null && !isSwipeDown)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject createJSON() {
        String customerId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
        JSONObject dataObj = null;
        if (!TextUtils.isEmpty(customerId)) {
            try {
                dataObj = new JSONObject();
                dataObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
//                dataObj.put("sales_token", "fd40fc709b1ccc928c1757c09fee0175");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
        return dataObj;
    }

    @Override
    public void onResume() {
        super.onResume();
        getBeats(createJSON(), false);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (mSwipeRefreshLayout != null)
                        mSwipeRefreshLayout.setRefreshing(false);
                    if (response instanceof ArrayList) {
                        arrBeats = (ArrayList<BeatModel>) response;

                        if (arrBeats != null && arrBeats.size() > 0) {

                            llUnbilledOutlets.setVisibility(View.VISIBLE);
                            tvNoItems.setVisibility(View.GONE);
                            routesAdapter = new RoutesAdapter(getActivity(), arrBeats);
                            routesAdapter.setBeatClickListener(RoutesFragment.this);

//        mAlphabetItems = new ArrayList<>();
//        List<String> strAlphabets = new ArrayList<>();
//
//        for (int i = 0; i < retailersModelArrayList.size(); i++) {
//            String name = retailersModelArrayList.get(i).getCompany();
//            if (name == null || name.trim().isEmpty())
//                continue;
//
//            String word = name.substring(0, 1).toUpperCase();
//            if (!strAlphabets.contains(word)) {
//                strAlphabets.add(word);
//                mAlphabetItems.add(new AlphabetItem(i, word, false));
//            }
//        }
                            mActionSlideExpandableListView.setAdapter(routesAdapter);
                        } else {
                            llUnbilledOutlets.setVisibility(View.GONE);
                            tvNoItems.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onBeatClick(Object object) {
        if (object != null && object instanceof BeatModel) {
            BeatModel beatModel = (BeatModel) object;
            mSharedPreferences.edit().putString(ConstantValues.KEY_BEAT_ID, beatModel.getBeatId()).apply();
            Intent intent = new Intent(getActivity(), OutletsActivity.class);
            startActivity(intent);
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.oops));
        }

    }

    @Override
    public void onBeatEdit(Object object) {
        if (object != null && object instanceof BeatModel) {
            BeatModel beatModel = (BeatModel) object;
            Intent intent = new Intent(getActivity(), AddBeatActivity.class);
            intent.putExtra("beatModel", beatModel);
            startActivity(intent);

        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.oops));
        }
    }

    @Override
    public void onMapCLick(Object object) {
        if (object != null && object instanceof BeatModel) {
            BeatModel beatModel = (BeatModel) object;
            Intent intent = new Intent(getActivity(), MapsActivity.class);
            intent.putExtra("beatModel", beatModel);
            intent.putExtra("isFromManageBeats", true);
            startActivity(intent);
        }
    }
}