package com.ebutor.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.OutletsModel;

public class FormFragment extends Fragment {

    private OutletsModel outletsModel;
    private OnClickListener listener;

    public FormFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.info_window_form_fragment, container, false);

        return view;
    }

    public void setClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvCompany = (TextView) view.findViewById(R.id.tv_company);
        TextView tvMobile = (TextView) view.findViewById(R.id.tv_mobile);
        TextView tvBeat = (TextView) view.findViewById(R.id.tv_beat);
        TextView tvAddress = (TextView) view.findViewById(R.id.tv_address);

        Bundle args = getArguments();
        if (args != null && args.containsKey("Outlet")) {
            outletsModel = (OutletsModel) args.getParcelable("Outlet");
        }

        if (outletsModel != null) {
            tvCompany.setText(outletsModel.getCompany());
            tvMobile.setText(outletsModel.getTelephone());
            tvBeat.setText(getString(R.string.beat) + " : " + outletsModel.getBeatName());
            tvAddress.setText(outletsModel.getAddress1() + "\n" + outletsModel.getAddress2());
        }
        view.findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClicked(outletsModel);
            }
        });
        view.findViewById(R.id.iv_delete_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClose();
            }
        });
    }


    public interface OnClickListener {
        void onClicked(Object retailersModel);

        void onClose();
    }
}
