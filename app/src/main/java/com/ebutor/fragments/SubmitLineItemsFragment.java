package com.ebutor.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.AdvancesSpinnerAdapter;
import com.ebutor.adapters.ExpenseTypeSpinnerAdapter;
import com.ebutor.adapters.UnClaimedExpensesRecyclerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ExpenseMasterLookupData;
import com.ebutor.models.ExpenseMasterlookupModel;
import com.ebutor.models.ExpenseModel;
import com.ebutor.models.GetExpensesResponse;
import com.ebutor.models.NewExpenseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DividerItemDecoration;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;


/**
 * Created by Srikanth Nama on 29-Dec-16.
 */

public class SubmitLineItemsFragment extends Fragment implements VolleyHandler<Object>, Response.ErrorListener {

    ProgressDialog dialog;
    double totalAmount = 0;
    private EditText etAmount, etReferenceIds;
    private Spinner spinRequestType, spinRequestFor;
    private TextView btnSubmit;
    private ArrayList<NewExpenseModel> expensesList;
    private String requestTypeId, requestType;
    private String requestForId, requestFor;
    private RecyclerView recyclerView;
    private int _submitType = 2;
    private SharedPreferences mSharedPreferences;
    private View viewAdvance;
    private Spinner spinAdvance;
    private ArrayList<ExpenseModel> advancesList;
    private String expenseMainId = "0";
    CardView fragementsubmitheader;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_submit_expense_dialog, container, false);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        fragementsubmitheader= (CardView) view.findViewById(R.id.fragement_submit_header);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        advancesList = new ArrayList<>();
        etAmount = (EditText) view.findViewById(R.id.et_amount);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("Type")) {
                _submitType = extras.getInt("Type", 2);
                fragementsubmitheader.setVisibility(View.GONE);
            }
            if (_submitType == 3 && extras.containsKey("ExpensesList")) {
                etAmount.setEnabled(false);
                expensesList = extras.getParcelableArrayList("ExpensesList");
                UnClaimedExpensesRecyclerAdapter adapter = new UnClaimedExpensesRecyclerAdapter(getActivity(), expensesList);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
              //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
                recyclerView.setAdapter(adapter);

                for (NewExpenseModel model : expensesList) {
                    double total;
                    try {
                        total = Double.parseDouble(model.getAmount());
                    } catch (Exception e) {
                        total = 0;
                    }
                    totalAmount += total;
                }

            } else {
                etAmount.setEnabled(true);
                expensesList = null;
            }
        }

        if (totalAmount > 0) {
            etAmount.setText(String.valueOf(totalAmount));
        }

        etReferenceIds = (EditText) view.findViewById(R.id.et_reference);

        spinRequestType = (Spinner) view.findViewById(R.id.spin_request_type);
        spinRequestFor = (Spinner) view.findViewById(R.id.spin_request_for);
        spinAdvance = (Spinner) view.findViewById(R.id.spin_advance);
        viewAdvance = view.findViewById(R.id.row_advance);

        btnSubmit = (TextView) view.findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String amount = etAmount.getText().toString().trim();
                String referenceIds = etReferenceIds.getText().toString().trim();

                if (TextUtils.isEmpty(amount)) {
                    etAmount.requestFocus();
                    etAmount.setError("Please enter Amount");
                    return;
                }
                boolean isValidAmount = true;
                try {
                    double _amount = 0;
                    _amount = Double.parseDouble(amount);
                    if (_amount <= 0.0) {
                        isValidAmount = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isValidAmount = false;
                }

                if (!isValidAmount) {
                    etAmount.requestFocus();
                    etAmount.setError("Please enter valid Amount");
                    return;
                }

                if (requestTypeId.equals("0")) {
                    Utils.showAlertDialog(getActivity(), "Please select Request Type");
                    return;
                }

                if (requestForId.equals("0")) {
                    Utils.showAlertDialog(getActivity(), "Please select Request for");
                    return;
                }

               /* if(_submitType == 3 && !expenseMainId.equals("0")){
                    Utils.showAlertDialog(getActivity(), "Please select Request for");
                    return;
                }*/


                if (Networking.isNetworkAvailable(getActivity())) {
                    //do network call here
                    btnSubmit.setEnabled(false);
                    saveExpenseItem(amount, referenceIds);
                } else {
                    Utils.showAlertDialog(getActivity(), getString(R.string.no_network_connection));
                }

            }
        });


        spinRequestType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null && view.getTag() instanceof ExpenseMasterLookupData) {
                    ExpenseMasterLookupData data = (ExpenseMasterLookupData) view.getTag();
                    requestType = data.getMasterLookupName();
                    requestTypeId = data.getValue();

                    if (requestTypeId.equals("122001") && _submitType == 3) {
                        //todo get Advances and display in drop down
                        viewAdvance.setVisibility(View.VISIBLE);
                        getOrdersList();
                    } else {
                        viewAdvance.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinRequestFor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null && view.getTag() instanceof ExpenseMasterLookupData) {
                    ExpenseMasterLookupData data = (ExpenseMasterLookupData) view.getTag();
                    requestFor = data.getMasterLookupName();
                    requestForId = data.getValue();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinAdvance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null && view.getTag() instanceof ExpenseModel) {
                    ExpenseModel data = (ExpenseModel) view.getTag();
                    expenseMainId = data.getExpenseId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getMasterLookupData();

    }

    private void saveExpenseItem(String amount, String refIds) {
        try {
            String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");

            JSONObject obj = new JSONObject();

            obj.put("RequestFoID", requestTypeId);
            obj.put("RequestForTypeID", requestForId);
            obj.put("Subject", requestFor + " " + requestType + " of " + amount);
            obj.put("Amount", amount);
            obj.put("SubmitDate", getToday());
            obj.put("ReffIDs", refIds);
            obj.put("SubmitedByID", userId);

            if (_submitType == 3) {
                if (!expenseMainId.equals("0")) {
                    obj.put("MainTableID", expenseMainId);
                } else {
                    obj.put("MainTableID", "0");
                }
                // check weather any unclaimed expenses are selected
                if (expensesList != null && expensesList.size() > 0) {
                    obj.put("MapIDs", TextUtils.join(",", expensesList));
                } else {
                    Utils.showAlertDialog(getActivity(), "No Line items selected to map");
                    return;
                }

                Log.e("mapdetailswithexpenses",obj.toString());
                mapDetailsWithExpenses(obj);
            } else if (_submitType == 2) {
                addExpenseDetails(obj);
            }


        } catch (Exception e) {
            e.printStackTrace();
            btnSubmit.setEnabled(true);
        }

    }

    public void getOrdersList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
                String url = AppURL.getExpensesURL + "?user=" + userId + "&expensestype=ADV&withbalanceamt=1";
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, url, null, SubmitLineItemsFragment.this, SubmitLineItemsFragment.this, PARSER_TYPE.GET_EXPENSES);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void mapDetailsWithExpenses(JSONObject obj) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.mapDetailsWithExpensesURL, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("mapdetailswithexpenses",response.toString());
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                String status = response.optString("status");
                String message = response.optString("message");
                if (status.equalsIgnoreCase("success")) {
                    Toast.makeText(getActivity(), "Updated successfully.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } else {
                    Utils.showAlertDialog(getActivity(), message);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConstantValues.getRequestHeaders();
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                AppURL.initial_time_out,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(request, AppURL.mapDetailsWithExpensesURL);

        if (dialog != null)
            dialog.show();
    }

    private void addExpenseDetails(JSONObject obj) {

        if (dialog != null)
            dialog.show();
        Log.e("Add Expense Details", obj.toString());
        MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.addExpenseDetailsURL);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.addExpenseDetailsURL, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                btnSubmit.setEnabled(true);
                Log.e("Add Expense Details", response.toString());
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                String status = response.optString("status");
                String message = response.optString("message");
                if (status.equalsIgnoreCase("success")) {
                    Toast.makeText(getActivity(), "Request created successfully.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } else {
                    Utils.showAlertDialog(getActivity(), message);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                btnSubmit.setEnabled(true);
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ConstantValues.getRequestHeaders();
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                AppURL.initial_time_out,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(request, AppURL.addExpenseDetailsURL);

    }

    private String getToday() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void getMasterLookupData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, AppURL.getExpenseMasterValuesURL, null, SubmitLineItemsFragment.this, SubmitLineItemsFragment.this, PARSER_TYPE.GET_EXPENSE_TYPES);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getExpenseMasterValuesURL);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_EXPENSE_TYPES) {
                    if (response instanceof ExpenseMasterlookupModel) {

                        ExpenseMasterlookupModel model = (ExpenseMasterlookupModel) response;
                        ArrayList<ExpenseMasterLookupData> requestTypes = model.getRequestTypes();
                        ArrayList<ExpenseMasterLookupData> requestForTypes = model.getRequestForTypes();

                        ExpenseMasterLookupData data = new ExpenseMasterLookupData();
                        data.setMasterLookupName("Select");
                        data.setValue("0");

                        if (requestTypes == null) {
                            requestTypes = new ArrayList<>();
                        }
                        if (requestForTypes == null) {
                            requestForTypes = new ArrayList<>();
                        }

                        requestTypes.add(0, data);
                        requestForTypes.add(0, data);

                        if (requestTypes.size() > 0) {
                            ExpenseTypeSpinnerAdapter adapter = new ExpenseTypeSpinnerAdapter(getActivity(), requestTypes);
                            spinRequestType.setAdapter(adapter);
                        }
                        if (requestForTypes.size() > 0) {
                            ExpenseTypeSpinnerAdapter adapter = new ExpenseTypeSpinnerAdapter(getActivity(), requestForTypes);
                            spinRequestFor.setAdapter(adapter);
                        }

                    }
                } else if (requestType == PARSER_TYPE.GET_EXPENSES) {
                    GetExpensesResponse result = (GetExpensesResponse) response;
                    advancesList = result.getExpenses();
//                    advancesList = (ArrayList<ExpenseModel>) response;
                    if (advancesList == null)
                        advancesList = new ArrayList<>();

                    ExpenseModel model = new ExpenseModel("0", "0", "", "Select", "", "", "", "", "", "");
                    advancesList.add(0, model);

                    if (advancesList.size() > 0) {
                        AdvancesSpinnerAdapter adapter = new AdvancesSpinnerAdapter(getActivity(), advancesList);
                        spinAdvance.setAdapter(adapter);
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }

        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

}

