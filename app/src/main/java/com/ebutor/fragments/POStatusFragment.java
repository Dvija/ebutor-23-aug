package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.POApprovalListActivity;
import com.ebutor.R;
import com.ebutor.adapters.POStatusAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.StatusModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class POStatusFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, POStatusAdapter.OnItemClickListener {

    private SharedPreferences mSharedPreferences;
    private RecyclerView rvPOStatus;
    private TextView tvNoStatus;
    private LinearLayout llMain;
    private RelativeLayout rlAlert;
    private Dialog dialog;
    private ArrayList<StatusModel> arrStatus;
    private POStatusAdapter poStatusAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_po_status, container, false);

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        rvPOStatus = (RecyclerView) view.findViewById(R.id.rv_po_status);
        tvNoStatus = (TextView) view.findViewById(R.id.tv_no_status);

    }

    private void getData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("user_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask poStatuslistReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPoStatusListURL, map, POStatusFragment.this, POStatusFragment.this, PARSER_TYPE.PO_STATUS_LIST);
                poStatuslistReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(poStatuslistReq, AppURL.getPoStatusListURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();

    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.PO_STATUS_LIST) {
                        if (response instanceof ArrayList) {
                            arrStatus = (ArrayList<StatusModel>) response;
                            if (arrStatus != null && arrStatus.size() > 0) {
                                rvPOStatus.setVisibility(View.VISIBLE);
                                tvNoStatus.setVisibility(View.GONE);

                                poStatusAdapter = new POStatusAdapter(getActivity(), arrStatus);
                                poStatusAdapter.setClickListener(POStatusFragment.this);
                                rvPOStatus.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                                rvPOStatus.setAdapter(poStatusAdapter);
                            } else {
                                rvPOStatus.setVisibility(View.GONE);
                                tvNoStatus.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onItemClick(Object object) {
        if (object instanceof StatusModel) {
            StatusModel statusModel = (StatusModel) object;
            Intent intent = new Intent(getActivity(), POApprovalListActivity.class);
            intent.putExtra("model", statusModel);
            startActivity(intent);
        }
    }
}
