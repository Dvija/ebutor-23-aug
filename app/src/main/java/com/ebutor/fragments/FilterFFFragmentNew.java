package com.ebutor.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.ChipAdapter;
import com.ebutor.adapters.FFListAdapter;
import com.ebutor.adapters.FilterFFAdapter;
import com.ebutor.adapters.FilterFFDataAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.FilterDataModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class FilterFFFragmentNew extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, FilterFFAdapter.onClickListener, FilterFFDataAdapter.onClickListener, ChipAdapter.onClickListener, FFListAdapter.onClickListener {

    private SharedPreferences mSharedPreferences;
    private RadioButton rbOrg, rbMyDashboard, rbMyTeam;
    private RecyclerView rvFFList;
    private Dialog dialog;
    private ItemsAdapter mAdapter;
    private FFListAdapter ffListAdapter;
    private ArrayList<FilterDataModel> arrFFs;
    private boolean hasChilds = false, isMain = false, isFromSO = false;
    private int type = DashBoardTypes.ORGANIZATION;
    private String pos = "", strStartDate = "", strEndDate = "", userId;
    private Date startDate, endDate, prevStartDate, prevEndDate;
    private EditText etStartDate, etEndDate;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy");
    private int selStartYear = 0, selStartMonth = 0, selStartDay = 0, selEndYear = 0, selEndMonth = 0, selEndDay = 0;
    private ArrayList<String> arrFilterNames = new ArrayList<String>();
    private ArrayList<FilterDataModel> arrFilterData, arrSelectedData;
    private RecyclerView rvFilterNames, rvFilterData, rvSelected;
    private Button btnApply, btnClear;
    //    private SODashboardFilterModel soDashboardModel;
    private LinearLayout llPeriod, llDashboard;
    private ChipAdapter chipAdapter;
    private HashMap<String, ArrayList<FilterDataModel>> hashMap;
    private ArrayList<String> arrSelected = new ArrayList<>();
    private HashMap<String, String> hashMapApplied = new HashMap<>();
    private String currentKey = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filter_ff_new, container, false);
        if (getArguments().containsKey("type"))
            type = getArguments().getInt("type");
        if (getArguments().containsKey("pos"))
            pos = getArguments().getString("pos");
        if (getArguments().containsKey("is_main"))
            isMain = getArguments().getBoolean("is_main");
        if (getArguments().containsKey("isFromSO"))
            isFromSO = getArguments().getBoolean("isFromSO", false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dialog = new ProgressDialog(getActivity());
        hasChilds = mSharedPreferences.getBoolean(ConstantValues.KEY_HAS_CHILD, false);

        llPeriod = (LinearLayout) view.findViewById(R.id.ll_period);
        llDashboard = (LinearLayout) view.findViewById(R.id.ll_dashboard);
        rbOrg = (RadioButton) view.findViewById(R.id.rb_org);
        rbMyDashboard = (RadioButton) view.findViewById(R.id.rb_my_dashboard);
        rbMyTeam = (RadioButton) view.findViewById(R.id.rb_my_team);
        rvFFList = (RecyclerView) view.findViewById(R.id.rv_ff_list);
        etStartDate = (EditText) view.findViewById(R.id.et_start_date);
        etEndDate = (EditText) view.findViewById(R.id.et_end_date);
        rvFilterNames = (RecyclerView) view.findViewById(R.id.rv_filter_names);
        rvFilterData = (RecyclerView) view.findViewById(R.id.rv_filter_data);
        rvSelected = (RecyclerView) view.findViewById(R.id.rv_selected);
        btnApply = (Button) view.findViewById(R.id.btn_apply);
        btnClear = (Button) view.findViewById(R.id.btn_clear);
        if (hasChilds) {
            rbMyTeam.setVisibility(View.VISIBLE);
        } else {
            rbMyTeam.setVisibility(View.GONE);
        }

        hashMapApplied = MyApplication.getInstance().getSelectedFilters();
        arrFilterData = new ArrayList<>();
        arrSelectedData = MyApplication.getInstance().getArrSelectedData();

        hashMap = new HashMap<>();

        rvFilterNames.setLayoutManager(new LinearLayoutManager(getActivity()));

        arrFilterNames = new ArrayList<>();

        chipAdapter = new ChipAdapter(getActivity(), arrSelectedData);
        chipAdapter.setListener(FilterFFFragmentNew.this);
        rvSelected.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvSelected.setAdapter(chipAdapter);

        getSODashboardFilters();

        int year, month, day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        strStartDate = day + " " + (month + 1) + " " + year;
        strEndDate = day + " " + (month + 1) + " " + year;

        etStartDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, day + " " + (month + 1) + " " + year));
        etEndDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, day + " " + (month + 1) + " " + year));
        try {
            startDate = sdf.parse(day + " " + (month + 1) + " " + year);
            endDate = sdf.parse(day + " " + (month + 1) + " " + year);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (getArguments().containsKey("start_date")) {
            strStartDate = getArguments().getString("start_date");
            etStartDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, strStartDate));
            try {
                startDate = sdf.parse(strStartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (getArguments().containsKey("end_date")) {
            strEndDate = getArguments().getString("end_date");
            etEndDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, strEndDate));
            try {
                endDate = sdf.parse(strEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        switch (type) {
            case DashBoardTypes.ORGANIZATION:
                rbOrg.setChecked(true);
                break;
            case DashBoardTypes.MY_DASHBOARD:
                rbMyDashboard.setChecked(true);
                break;
            case 3:
                rbMyTeam.setChecked(true);
                getFieldForceList();
                break;
            default:
                rbOrg.setChecked(true);
                break;
        }

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedItems = getSelectedItems();
                MyApplication.getInstance().setSelectedFilters(hashMapApplied);
                MyApplication.getInstance().setArrSelectedData(arrSelectedData);
                Intent intent = getActivity().getIntent();
                intent.putExtra("type", type);
                intent.putExtra("isClear", false);
                intent.putExtra("start_date", strStartDate);
                intent.putExtra("end_date", strEndDate);
                intent.putExtra("applied_filters", selectedItems);
                if (pos != null && !TextUtils.isEmpty(pos))
                    intent.putExtra("pos", pos);
                if (userId != null && !TextUtils.isEmpty(userId))
                    intent.putExtra("user_id", userId);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setSelectedFilters(new HashMap<String, String>());
                MyApplication.getInstance().setArrSelectedData(new ArrayList<FilterDataModel>());
                Intent intent = getActivity().getIntent();
                intent.putExtra("type", type);
                intent.putExtra("isClear", true);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

        rbMyTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFieldForceList();
            }
        });

        rbOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvFFList.setVisibility(View.GONE);
                type = DashBoardTypes.ORGANIZATION;
                pos = "";
//                Intent intent = getActivity().getIntent();
//                intent.putExtra("type", DashBoardTypes.ORGANIZATION);
//                intent.putExtra("start_date", strStartDate);
//                intent.putExtra("end_date", strEndDate);
//                getActivity().setResult(Activity.RESULT_OK, intent);
//                getActivity().finish();
            }
        });

        rbMyDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvFFList.setVisibility(View.GONE);
                type = DashBoardTypes.MY_DASHBOARD;
                pos = "";
//                Intent intent = getActivity().getIntent();
//                intent.putExtra("type", DashBoardTypes.MY_DASHBOARD);
//                intent.putExtra("start_date", strStartDate);
//                intent.putExtra("end_date", strEndDate);
//                getActivity().setResult(Activity.RESULT_OK, intent);
//                getActivity().finish();
            }
        });

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int year, month, day;
                final Calendar c = Calendar.getInstance();
                if (selStartYear == 0 || selStartMonth == 0 || selStartDay == 0) {
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    year = selStartYear;
                    month = selStartMonth;
                    day = selStartDay;
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        try {
                            prevStartDate = startDate;
                            startDate = sdf.parse(dayOfMonth + " " + (monthOfYear + 1) + " " + year);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (endDate != null && ((startDate.compareTo(endDate) < 0) || startDate.compareTo(endDate) == 0)) {
                            selStartYear = year;
                            selStartMonth = monthOfYear;
                            selStartDay = dayOfMonth;

                            strStartDate = dayOfMonth + " " + (monthOfYear + 1) + " " + year;
                            etStartDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, dayOfMonth + " " + (monthOfYear + 1) + " " + year));
                        } else {
                            startDate = prevStartDate;
                            Toast.makeText(getActivity(), getResources().getString(R.string.please_select_enddate_after_startdate), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, year, month, day);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 21);
                calendar.set(Calendar.MINUTE, 59);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int year, month, day;
                final Calendar c = Calendar.getInstance();
                if (selEndYear == 0 || selEndMonth == 0 || selEndDay == 0) {
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    year = selEndYear;
                    month = selEndMonth;
                    day = selEndDay;
                }
                if (null != strStartDate && !TextUtils.isEmpty(strEndDate)) {

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                            try {
                                prevEndDate = endDate;
                                endDate = sdf.parse(dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if (startDate.compareTo(endDate) < 0 || startDate.compareTo(endDate) == 0) {
                                selEndYear = year;
                                selEndMonth = monthOfYear;
                                selEndDay = dayOfMonth;

                                strEndDate = dayOfMonth + " " + (monthOfYear + 1) + " " + year;
                                etEndDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, dayOfMonth + " " + (monthOfYear + 1) + " " + year));
                            } else {
                                endDate = prevEndDate;
                                Toast.makeText(getActivity(), getResources().getString(R.string.please_select_enddate_after_startdate), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, year, month, day);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY, 21);
                    calendar.set(Calendar.MINUTE, 59);
                    datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                    datePickerDialog.show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.please_select_start_date), Toast.LENGTH_SHORT).show();
                }
            }
        });


//        rvFFList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                pos = String.valueOf(position);
//                userId = arrFFs.get(position).getId();
//                arrFFs.get(position).setChecked(true);
////                Intent intent = getActivity().getIntent();
////                intent.putExtra("type", DashBoardTypes.MY_TEAM_DASHBOARD);
////                intent.putExtra("start_date", strStartDate);
////                intent.putExtra("end_date", strEndDate);
////                intent.putExtra("pos", String.valueOf(position));
////                if (arrFFs != null && arrFFs.size() > 0)
////                    intent.putExtra("user_id", arrFFs.get(position).getCustomerGrpId());
////                getActivity().setResult(Activity.RESULT_OK, intent);
////                getActivity().finish();
//            }
//        });

    }

    private String getSelectedItems() {
        JSONObject jsonObject = new JSONObject();
        try {
            Set<String> keySet = hashMap.keySet();
            ArrayList<String> keysArray = new ArrayList<>(keySet);

            if (keysArray != null && keysArray.size() > 0) {
                for (int i = 0; i < keysArray.size(); i++) {
                    ArrayList<FilterDataModel> arrayList = hashMap.get(keysArray.get(i));
                    Predicate<FilterDataModel> isCheckedFilters = new Predicate<FilterDataModel>() {
                        @Override
                        public boolean apply(FilterDataModel filterDataModel) {
                            return filterDataModel.isChecked();
                        }
                    };

                    ArrayList<String> checkedFilters = filter(arrayList, isCheckedFilters);
                    String ids = TextUtils.join(",", checkedFilters);
                    if (!TextUtils.isEmpty(ids)) {
                        hashMapApplied.put(keysArray.get(i), ids);
                        jsonObject.put(keysArray.get(i), ids);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    private ArrayList<String> filter(ArrayList<FilterDataModel> target, Predicate<FilterDataModel> predicate) {

        ArrayList<String> result = new ArrayList<String>();
        for (FilterDataModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getId());
            }
        }
        return result;
    }

    private void getSODashboardFilters() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                HashMap<String, String> map = new HashMap<>();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                jsonObject.put("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_FF_LEGAL_ENTITY_ID, ""));
                map.put("data", jsonObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getsodashboardfiltersURL, map, FilterFFFragmentNew.this, FilterFFFragmentNew.this, PARSER_TYPE.GET_SO_DASHBOARD_FILTERS);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getsodashboardfiltersURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getFieldForceList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                JSONObject dataObject = new JSONObject();
                HashMap<String, String> map = new HashMap<>();
                dataObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                dataObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                dataObject.put("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_FF_LEGAL_ENTITY_ID, ""));
//                if (strStartDate != null && !TextUtils.isEmpty(strStartDate))
//                    dataObject.put("start_date", Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, strStartDate));
//                if (strEndDate != null && !TextUtils.isEmpty(strEndDate))
//                    dataObject.put("end_date", Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, strEndDate));
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getfieldforcelistURL, map, FilterFFFragmentNew.this, FilterFFFragmentNew.this, PARSER_TYPE.FF_LIST);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getfieldforcelistURL);
                if (dialog != null)
                    dialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.FF_LIST) {
                        if (response instanceof ArrayList) {
                            arrFFs = (ArrayList<FilterDataModel>) response;
                            if (arrFFs != null && arrFFs.size() > 0) {
                                rvFFList.setVisibility(View.VISIBLE);
                                rvFFList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                ffListAdapter = new FFListAdapter(getActivity(), arrFFs);
                                ffListAdapter.setListener(FilterFFFragmentNew.this);
                                rvFFList.setAdapter(ffListAdapter);

//                                FilterFFDataAdapter filterFFDataAdapter = new FilterFFDataAdapter(getActivity(), arrFilterData, arrSelected, "");
//                                filterFFDataAdapter.setListener(FilterFFFragmentNew.this);
//                                rvFilterData.setLayoutManager(new LinearLayoutManager(getActivity()));
//                                rvFilterData.setAdapter(filterFFDataAdapter);
                            }
                        }
                    } else if (requestType == PARSER_TYPE.GET_SO_DASHBOARD_FILTERS) {
                        if (response instanceof HashMap) {

                            hashMap = (HashMap<String, ArrayList<FilterDataModel>>) response;
//                            soDashboardModel = (SODashboardFilterModel) response;
                            if (hashMap != null) {
                                Set<String> keySet = hashMap.keySet();
                                arrFilterNames = new ArrayList<>(keySet);
                                arrFilterNames.add(0, getString(R.string.period));
                                if (isMain)
                                    arrFilterNames.add(getString(R.string.team));
                                FilterFFAdapter filterFFAdapter = new FilterFFAdapter(getActivity(), arrFilterNames);
                                filterFFAdapter.setListener(FilterFFFragmentNew.this);
                                rvFilterNames.setAdapter(filterFFAdapter);
                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onItemClick(String key) {

        this.currentKey = key;
        if (hashMap != null) {
            ArrayList<String> arrSelected = new ArrayList<>();
            String selectedFilters = "";
            switch (key) {
                case "Period":
                    llPeriod.setVisibility(View.VISIBLE);
                    rvFilterData.setVisibility(View.GONE);
                    llDashboard.setVisibility(View.GONE);
                    break;
                case "Team":
                    llPeriod.setVisibility(View.GONE);
                    rvFilterData.setVisibility(View.GONE);
                    llDashboard.setVisibility(View.VISIBLE);
                    break;
                default:
                    llPeriod.setVisibility(View.GONE);
                    rvFilterData.setVisibility(View.VISIBLE);
                    llDashboard.setVisibility(View.GONE);
                    arrFilterData = hashMap.get(key);
                    if (hashMapApplied != null)
                        selectedFilters = hashMapApplied.get(key);
                    if (selectedFilters != null && selectedFilters.length() > 0)
                        arrSelected = new ArrayList<String>(Arrays.asList(selectedFilters.split("\\s*,\\s*")));
                    break;
            }

            if (arrFilterData != null) {
                FilterFFDataAdapter filterFFDataAdapter = new FilterFFDataAdapter(getActivity(), arrFilterData, arrSelected, "");
                filterFFDataAdapter.setListener(FilterFFFragmentNew.this);
                rvFilterData.setLayoutManager(new LinearLayoutManager(getActivity()));
                rvFilterData.setAdapter(filterFFDataAdapter);
            }

        }

    }

    @Override
    public void onItemDelete(int position, Object object) {

        if (object instanceof FilterDataModel && arrSelectedData != null && arrSelectedData.size() > 0) {
            FilterDataModel filterDataModel = (FilterDataModel) object;
            arrSelectedData.remove(filterDataModel);
            chipAdapter.notifyDataSetChanged();
            if (hashMapApplied != null) {
                String selectedFilters = "";
                ArrayList<FilterDataModel> arrayList = hashMap.get(filterDataModel.getKey());
                if (arrayList != null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).getId().equalsIgnoreCase(filterDataModel.getId())) {
                            filterDataModel.setChecked(false);
                            arrayList.get(i).setChecked(false);
                        }
                    }
                    if (currentKey != null && currentKey.equalsIgnoreCase(filterDataModel.getKey())) {
                        if (hashMapApplied != null)
                            selectedFilters = hashMapApplied.get(filterDataModel.getKey());
                        if (selectedFilters != null && selectedFilters.length() > 0) {
                            arrSelected = new ArrayList<String>(Arrays.asList(selectedFilters.split("\\s*,\\s*")));
                            if (arrSelected.contains(filterDataModel.getId())) {
                                arrSelected.remove(filterDataModel.getId());
                                hashMapApplied.put(filterDataModel.getKey(), TextUtils.join(",", arrSelected));
                            }
                        }
                        FilterFFDataAdapter filterFFDataAdapter = new FilterFFDataAdapter(getActivity(), arrayList, arrSelected, "");
                        filterFFDataAdapter.setListener(FilterFFFragmentNew.this);
                        rvFilterData.setLayoutManager(new LinearLayoutManager(getActivity()));
                        rvFilterData.setAdapter(filterFFDataAdapter);
                    }
                }
            }
        }
    }

    @Override
    public void onChecked(int position, Object object, String key, boolean isChecked) {
        if (object instanceof FilterDataModel) {
//            String selectedFilters = "";
//            if (hashMapApplied != null)
//                selectedFilters = hashMapApplied.get(key);
//            if (selectedFilters != null && selectedFilters.length() > 0)
//                arrSelected = new ArrayList<String>(Arrays.asList(selectedFilters.split("\\s*,\\s*")));
            FilterDataModel filterDataModel = (FilterDataModel) object;

//            if(isChecked){
//                if(arrSelected.contains(customerTyepModel.customerGrpId)){
//                    //do nothing as view already exixts
//                }else{
//                    arrSelected.add(customerTyepModel.customerGrpId);
//                }
//            }else{//unchecked
//                if(arrSelected.contains(customerTyepModel.customerGrpId)){
//                    //do nothing as view already exixts
//                    arrSelected.remove(customerTyepModel.customerGrpId);
//                }else{
//                    // do nothing
//                }
//            }

            Predicate<FilterDataModel> isCheckedFilters = new Predicate<FilterDataModel>() {
                @Override
                public boolean apply(FilterDataModel filterDataModel) {
                    return true;
                }
            };

            ArrayList<String> checkedFilters = filter(arrSelectedData, isCheckedFilters);
            if (!isChecked && checkedFilters.contains(filterDataModel.getId())) {
                filterDataModel.setChecked(false);
                arrSelectedData.remove(filterDataModel);
                chipAdapter.remove(filterDataModel);
            } else if (!arrSelectedData.contains(filterDataModel)) {
                filterDataModel.setChecked(true);
                filterDataModel.setKey(key);
                arrSelectedData.add(filterDataModel);
            }
            chipAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFFChecked(int position, Object object, String key, boolean isChecked) {

    }

    private class ItemsAdapter extends BaseAdapter {
        ArrayList<FilterDataModel> items;
        String selected = "";
        String childPos = "";

        public ItemsAdapter(ArrayList<FilterDataModel> item, String childPos) {
            this.items = item;
            this.childPos = childPos;
        }

        // @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_filter_dialog_new, null);
            }
            TextView tvSort = (TextView) v.findViewById(R.id.tv_sort_name);
            CheckBox cbTick = (CheckBox) v.findViewById(R.id.cb_tick);
            tvSort.setText(items.get(position).getValue());
//            cbTick.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (((CheckBox) v).isChecked()) {
//                        isChecked = true;
//                        items.get(position).setChecked(true);
//                    } else {
//                        isChecked = false;
//                        items.get(position).setChecked(false);
//                    }
//                }
//            });
            v.setTag(items.get(position));
            return v;
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }
}
