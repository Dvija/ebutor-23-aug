package com.ebutor.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.FFDashboardActivity;
import com.ebutor.HomeActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.RetailerDashboard;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class FFDashBoardFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object> {


    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private TextView tvTBV, tvUOB, tvABV, tvTLC, tvULC, tvALC;
    private int cartCount;

    private int type = DashBoardTypes.ORGANIZATION;

    public static FFDashBoardFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("type", page);
        FFDashBoardFragment fragment = new FFDashBoardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvTBV = (TextView) view.findViewById(R.id.button1);
        tvUOB = (TextView) view.findViewById(R.id.button2);
        tvABV = (TextView) view.findViewById(R.id.button3);
        tvTLC = (TextView) view.findViewById(R.id.button4);
        tvULC = (TextView) view.findViewById(R.id.button5);
        tvALC = (TextView) view.findViewById(R.id.button6);

        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);
        if (getArguments() != null && getArguments().containsKey("type")) {
            type = getArguments().getInt("type");
        }
        getDashboard(createJSON());

    }

    private void getDashboard(JSONObject dataObject) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.retailerDashboardURL, map, FFDashBoardFragment.this, FFDashBoardFragment.this, PARSER_TYPE.RETAILER_DASHBOARD);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.retailerDashboardURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject createJSON() {
        String customerId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
        JSONObject dataObj = null;
        if (!TextUtils.isEmpty(customerId)) {
            try {
                dataObj = new JSONObject();
                dataObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
        return dataObj;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartCount();
    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID,""));
        ((FFDashboardActivity) getActivity()).updateCart(cartCount);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if(error!=null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e){
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    switch (type) {
                        case DashBoardTypes.ORGANIZATION:
                            getDashboard(response);
                            break;
                        case DashBoardTypes.MY_DASHBOARD:
                            getDashboard(response);
                            break;
                        case DashBoardTypes.MY_TEAM_DASHBOARD:

                            break;
                    }

                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void getDashboard(Object response){

        if(response instanceof RetailerDashboard){
            RetailerDashboard retailerDashboard = (RetailerDashboard)response;

            tvTBV.setText(String.format(Locale.CANADA,getString(R.string.tbv), retailerDashboard.getTotalBillValue()));
            tvUOB.setText(String.format(Locale.CANADA,getString(R.string.uob), retailerDashboard.getUniqueOutletsBilled()));
            tvABV.setText(String.format(Locale.CANADA,getString(R.string.abv), retailerDashboard.getAverageBillValue()));
            tvTLC.setText(String.format(Locale.CANADA,getString(R.string.tlc), retailerDashboard.getTotalLineCut()));
            tvULC.setText(String.format(Locale.CANADA,getString(R.string.ulc), retailerDashboard.getUniqueLineCut()));
            tvALC.setText(String.format(Locale.CANADA,getString(R.string.alc), retailerDashboard.getAverageLineCut()));

        }else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}