package com.ebutor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.android.internal.util.Predicate;
import com.ebutor.R;
import com.ebutor.adapters.ManufacturerAdapter;
import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;
import java.util.Collection;

public class ManufacturerFragment extends DialogFragment implements ManufacturerAdapter.OnSelectListener {

    private ArrayList<CustomerTyepModel> arrManf;
    private OnClickListener onClickListener;
    private ListView lvManf;
    private String manfIds;
    private Button btnDone;
    private ManufacturerAdapter manufacturerAdapter;

    public static ManufacturerFragment newInstance(ArrayList<CustomerTyepModel> arrManf, String manfIds) {
        ManufacturerFragment frag = new ManufacturerFragment();
        Bundle args = new Bundle();
        args.putSerializable("manf", arrManf);
        args.putString("manfIds", manfIds);
        frag.setArguments(args);
        return frag;
    }

    public static ArrayList<String> filterNames(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerName());
            }
        }
        return result;
    }

    public void setClickListener(OnClickListener listener) {
        this.onClickListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getActivity().getResources().getString(R.string.manufacturers));
        arrManf = (ArrayList<CustomerTyepModel>) getArguments().getSerializable("manf");
        manfIds = (String) getArguments().getString("manfIds");
        return inflater.inflate(R.layout.fragment_manf, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lvManf = (ListView) view.findViewById(R.id.lv_sort);
        btnDone = (Button) view.findViewById(R.id.btn_done);

        manufacturerAdapter = new ManufacturerAdapter(getActivity(), arrManf, manfIds);
        manufacturerAdapter.setSelectListener(ManufacturerFragment.this);

        lvManf.setAdapter(manufacturerAdapter);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = "";
                Predicate<CustomerTyepModel> isCheckedData = new Predicate<CustomerTyepModel>() {
                    @Override
                    public boolean apply(CustomerTyepModel customerTyepModel) {
                        return customerTyepModel.isChecked();
                    }
                };

                ArrayList<String> checkedFilters = filterNames(arrManf, isCheckedData);
                result = TextUtils.join(",", checkedFilters);
                dismiss();
                onClickListener.onClicked(result);
            }
        });
    }

    @Override
    public void onSelected(boolean flag) {
        int size = lvManf.getAdapter().getCount();
        View view1 = lvManf.getChildAt(size - 1);
        CheckedTextView ctvManf1 = (CheckedTextView) view1.findViewById(R.id.ctv_manf);
        ctvManf1.setChecked(false);
        for (int i = 0; i < size - 1; i++) {
            View view = lvManf.getChildAt(i);
            CheckedTextView ctvManf = (CheckedTextView) view.findViewById(R.id.ctv_manf);
            if (flag) {
                arrManf.get(i).setIsChecked(true);
                ctvManf.setChecked(true);
            } else {
                arrManf.get(i).setIsChecked(false);
                ctvManf.setChecked(false);
            }
        }
    }

    @Override
    public void onUnSelected(boolean flag, boolean isUnSelectAll) {
        int size = lvManf.getAdapter().getCount();
        View view1 = lvManf.getChildAt(0);
        View view2 = lvManf.getChildAt(size - 1);
        CheckedTextView ctvManf1 = (CheckedTextView) view1.findViewById(R.id.ctv_manf);
        CheckedTextView ctvManf2 = (CheckedTextView) view2.findViewById(R.id.ctv_manf);
        ctvManf1.setChecked(false);
        if (flag) {
            ctvManf2.setChecked(true);
        } else {
            ctvManf2.setChecked(false);
        }
        if (isUnSelectAll) {
            for (int i = 0; i < size - 1; i++) {
                View view = lvManf.getChildAt(i);
                CheckedTextView ctvManf = (CheckedTextView) view.findViewById(R.id.ctv_manf);
                arrManf.get(i).setIsChecked(false);
                ctvManf.setChecked(false);

            }
        }
    }

    public interface OnClickListener {
        void onClicked(String string);
    }
}
