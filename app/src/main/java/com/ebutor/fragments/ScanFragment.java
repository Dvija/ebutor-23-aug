package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.CrateProductsAdapter;
import com.ebutor.backgroundtask.JSONParser;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ContainerModel;
import com.ebutor.models.CrateModel;
import com.ebutor.models.SpecificationsModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class ScanFragment extends Fragment implements Response.ErrorListener, RadioGroup.OnCheckedChangeListener {

    private Button btnScan;
    private EditText etScan;
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private LinearLayout llOrderData, llContainer, llCrateData, llContainerData, llNodata;
    private ArrayList<SpecificationsModel> arrData;
    private TextView tvSpecName, tvSpecValue, tvOrderNo, tvDate, tvDC, tvHub, tvOrderValue, tvStatus, tvShopName, tvBeat, tvSOName,
            tvNodata, tvPickedBy, tvPickedDate, tvInvoiceNo, tvInvoiceDate, tvInvoiceValue, tvInvoiceBy, tvCfc, tvBag, tvCrate, tvContainer;
    private RadioButton rbOrder, rbCrate;
    // private AppCompatRadioButton rbOrder,rbCrate;
    private CrateModel crateModel;
    private ArrayList<ContainerModel> arrContainers;
    private CrateProductsAdapter crateProductsAdapter;
    public ScrollView SvData;
    View view1,view2;
    private RadioGroup group;
    String content = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scan, container, false);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnScan = (Button) view.findViewById(R.id.btn_scan);
        etScan = (EditText) view.findViewById(R.id.et_scan);
        tvOrderNo = (TextView) view.findViewById(R.id.tv_order_no);
        tvDate = (TextView) view.findViewById(R.id.tv_date);
        tvDC = (TextView) view.findViewById(R.id.tv_dc);
        tvHub = (TextView) view.findViewById(R.id.tv_hub);
        tvOrderValue = (TextView) view.findViewById(R.id.tv_order_value);
        tvStatus = (TextView) view.findViewById(R.id.tv_status);
        tvShopName = (TextView) view.findViewById(R.id.tv_shop_name);
        tvBeat = (TextView) view.findViewById(R.id.tv_beat);
        tvSOName = (TextView) view.findViewById(R.id.tv_so_name);
        tvCrate = (TextView) view.findViewById(R.id.tv_crate1);
        tvBag = (TextView) view.findViewById(R.id.tv_bag1);
        tvCfc = (TextView) view.findViewById(R.id.tv_cfc1);

        tvPickedBy = (TextView) view.findViewById(R.id.tv_picked_by);
        tvPickedDate = (TextView) view.findViewById(R.id.tv_picked_date);
        tvInvoiceNo = (TextView) view.findViewById(R.id.tv_invoice_no);
        tvInvoiceDate = (TextView) view.findViewById(R.id.tv_invoice_date);
        tvInvoiceValue = (TextView) view.findViewById(R.id.tv_invoice_value);
        tvInvoiceBy = (TextView) view.findViewById(R.id.tv_invoice_by);
        rbOrder = (RadioButton) view.findViewById(R.id.rb_order);
        rbCrate = (RadioButton) view.findViewById(R.id.rb_crate);
        group = (RadioGroup) view.findViewById(R.id.rg_group);
        llOrderData = (LinearLayout) view.findViewById(R.id.ll_order_data);
        llContainer = (LinearLayout) view.findViewById(R.id.ll_container);
        llCrateData = (LinearLayout) view.findViewById(R.id.ll_crate_data);
        llContainerData = (LinearLayout) view.findViewById(R.id.ll_container_data);
        SvData = (ScrollView) view.findViewById(R.id.sv_data);
        tvContainer = (TextView) view.findViewById(R.id.tv_container);
        tvNodata = (TextView) view.findViewById(R.id.tv_nodata);
        llNodata = (LinearLayout) view.findViewById(R.id.ll_nodata);
        view1= view.findViewById(R.id.view1);
        view2=view.findViewById(R.id.view2);

        //  etScan.addTextChangedListener(this);

        group.setOnCheckedChangeListener(this);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanbutton();

            }
        });

        etScan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (rbOrder.isChecked()) {
//                    getOrderData();
//                } else {

                if (rbOrder.isChecked()) {
                    if (etScan.getText().toString()!= null && etScan.getText().length() > 0) {
                        String data = s.toString();
                        if (data != null && data.length() == 1) {
                            if (!data.equalsIgnoreCase("t")) {
                                etScan.setText("");
                                etScan.setError("please...enter proper data");

                            }
                        }
                        if (data != null && data.length() == 2) {
                            if (!data.equalsIgnoreCase("ts")) {
                                etScan.setText("t");
                                etScan.setSelection(etScan.getText().length());
                                etScan.setError("please...enter proper data");

                            }
                        }
                        if (data != null && data.length() == 3) {
                            if (!data.equalsIgnoreCase("tss")) {
                                etScan.setText("ts");
                                etScan.setSelection(etScan.getText().length());
                                etScan.setError("please...enter proper data");

                            }
                        }
                    }
                }

                if (rbCrate.isChecked()) {

                    if (etScan.getText().toString() != null && etScan.getText().length() > 0) {
                        String data1 = s.toString();
                        Log.e("message", s.toString());
                        if (data1 != null && data1.length() == 1) {
                            if (!data1.equalsIgnoreCase("c")) {
                                etScan.setText("");
                                etScan.setError("please...enter proper data");
                                Log.e("message", "error");

                            }
                        }
                        if (data1 != null && data1.length() == 2) {
                            if (!data1.equalsIgnoreCase("cr")) {
                                etScan.setText("c");
                                etScan.setSelection(etScan.getText().length());
                                etScan.setError("please...enter proper data");
                                Log.e("message", "error");
                            }
                        }
                        if (data1 != null && data1.length() == 3) {
                            if (!data1.equalsIgnoreCase("crt")) {
                                etScan.setText("cr");
                                etScan.setSelection(etScan.getText().length());
                                etScan.setError("please...enter proper data");
                                Log.e("message", "error");

                            }
                        }
                    }
                }

                if (s.length() < 15) {
                    SvData.setVisibility(View.GONE);
                }
                if (s.length() == 15) {
                    llContainerData.removeAllViews();
                    Log.e("message", "cratedata");
                    getCrateData();
                }
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
    }

    public void scanbutton() {

        etScan.setText("");
        Log.e("message", "scan");
       /* Intent intent = new Intent(getActivity(),CaptureActivity.class);
        intent.setAction("com.google.zxing.client.android.SCAN");
        intent.putExtra("SAVE_HISTORY", false);
        startActivityForResult(intent, 0);*/
        // etScan.removeTextChangedListener(getActivity());
        IntentIntegrator intentIntegrator = new IntentIntegrator(getActivity());
        intentIntegrator.forSupportFragment(this).initiateScan(IntentIntegrator.ALL_CODE_TYPES);
    }
  /*  public void click(View v){
        boolean checked=((RadioButton) v).isChecked();
        SvData.setVisibility(View.GONE);
        switch(v.getId()){
            case R.id.rb_order:
                if(checked){

                }
                break;
            case R.id.rb_crate:
                if(checked){
                    if(etScan.getText().toString().length()>=15){
                        getCrateData();
                    }
                }
        }
    }*/

    private void getCrateData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject dataObj = new JSONObject();
            JSONObject jsonObject = new JSONObject();
            if (dialog != null)
                dialog.show();
            /*try {
//                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("type", "c");
                jsonObject.put("code", "CRTZ1R1D1-00022");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getOrderListViewURL, map, ScanFragment.this, ScanFragment.this, PARSER_TYPE.GET_ORDER_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getOrderListViewURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            try {
                if (rbOrder.isChecked()) {
                    jsonObject.put("type", "o");
                    jsonObject.put("code", etScan.getText().toString());
                } else {
                    jsonObject.put("type", "c");
                    jsonObject.put("code", etScan.getText().toString());
                }
                dataObj.put("data", jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.getCrateDetailsURL, dataObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    String status = response.optString("Status");
                    String message = response.optString("Message");
                    if (status.equalsIgnoreCase("200")) {
                        Log.e("message", "success");
                        JSONParser jsonParser = new JSONParser();
                        crateModel = jsonParser.getCrateData(response);


                        if (crateModel != null) {
                            llNodata.setVisibility(View.GONE);
                            SvData.setVisibility(View.VISIBLE);
                            // tvContainer.setVisibility(View.VISIBLE);
                            // String a="N/A";
                            //  tvOrderNo.setText(crateModel.getOrderCode());
                            validate_textviews(tvOrderNo, crateModel.getOrderCode(), 0);

                            // tvDate.setText(Utils.parseDate(Utils.TIME_STAMP_PATTERN, Utils.DATE_TIME_PATTERN, crateModel.getOrderDate()));
                            // tvDate.setText(crateModel.getOrderDate());
                            String date1 = Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_TIME_PATTERN, crateModel.getOrderDate());
                            validate_textviews(tvDate, date1, 0);
                            //tvDC.setText(crateModel.getWhName());
                            validate_textviews(tvDC, crateModel.getWhName(), 0);
                            // tvHub.setText(crateModel.getHubName());
                            validate_textviews(tvHub, crateModel.getHubName(), 0);

                            //  tvOrderValue.setText(crateModel.getTotal());
                            validate_textviews(tvOrderValue, crateModel.getTotal(), 1);
                            // tvStatus.setText(crateModel.getOrderStatus());
                            validate_textviews(tvStatus, crateModel.getOrderStatus(), 0);
                            // tvShopName.setText(crateModel.getShopName());
                            validate_textviews(tvShopName, crateModel.getShopName(), 0);
                            //   tvBeat.setText(crateModel.getBeat());
                            validate_textviews(tvBeat, crateModel.getBeat(), 0);
                            //tvSOName.setText(crateModel.getSoName());
                            validate_textviews(tvSOName, crateModel.getSoName(), 0);

                            // tvPickedBy.setText(crateModel.getPickedBy());
                            validate_textviews(tvPickedBy, crateModel.getPickedBy(), 0);
                            String pickedDate = Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_TIME_PATTERN, crateModel.getPickedAt());
                            //  tvPickedDate.setText(Utils.parseDate(Utils.TIME_STAMP_PATTERN, Utils.DATE_TIME_PATTERN, crateModel.getPickedAt()));
                            //   tvPickedDate.setText(crateModel.getPickedAt());
                            validate_textviews(tvPickedDate, pickedDate, 0);
                            // tvInvoiceNo.setText(crateModel.getInvoiceCode());
                            validate_textviews(tvInvoiceNo, crateModel.getInvoiceCode(), 0);
                            String date2 = Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_TIME_PATTERN, crateModel.getCreatedAt());
                            //  tvInvoiceDate.setText(Utils.parseDate(Utils.TIME_STAMP_PATTERN, Utils.DATE_TIME_PATTERN, crateModel.getCreatedAt()));
                            //tvInvoiceDate.setText(crateModel.getCreatedAt());
                            validate_textviews(tvInvoiceDate, date2, 0);
                            // tvInvoiceValue.setText(crateModel.getGrandTotal());
                            validate_textviews(tvInvoiceValue, crateModel.getGrandTotal(), 1);
                            // tvInvoiceBy.setText(crateModel.getCreatedBy());
                            validate_textviews(tvInvoiceBy, crateModel.getCreatedBy(), 0);

                            //  tvCfc.setText(""+crateModel.getCfc());
                            validate_textviews(tvCfc, String.valueOf(crateModel.getCfc()), 0);
                            //tvBag.setText(""+crateModel.getBag());
                            validate_textviews(tvBag, String.valueOf(crateModel.getBag()), 0);
                            //tvCrate.setText(""+crateModel.getCrate());
                            validate_textviews(tvCrate, String.valueOf(crateModel.getCrate()), 0);

                            arrContainers = crateModel.getArrContainers();

                            if (arrContainers != null && arrContainers.size() > 0) {
                                llContainer.setVisibility(View.VISIBLE);
                                for (int i = 0; i < arrContainers.size(); i++) {
                                    ContainerModel containerModel = arrContainers.get(i);
                                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View crateView = inflater.inflate(R.layout.row_container, null);

                                    TextView tvCrateCode = (TextView) crateView.findViewById(R.id.tv_crate_code);
                                    TextView tvCrateStatus = (TextView) crateView.findViewById(R.id.tv_crate_status);
                                    TextView tvCrateWeight = (TextView) crateView.findViewById(R.id.tv_crate_weight);
                                    RecyclerView rvProducts = (RecyclerView) crateView.findViewById(R.id.rv_products);
                                    rvProducts.setNestedScrollingEnabled(false);



                                    if (containerModel != null) {
                                        // tvCrateCode.setText(containerModel.getContainerBarcode());
                                        validate_textviews(tvCrateCode, containerModel.getContainerBarcode(), 0);
                                        // tvCrateStatus.setText(containerModel.getStatus());
                                        validate_textviews(tvCrateStatus, containerModel.getStatus(), 0);
                                        //tvCrateWeight.setText(containerModel.getWeight());
                                        validate_textviews(tvCrateWeight, containerModel.getWeight(), 0);


                                        ArrayList<TestProductModel> arrProducts = containerModel.getArrProducts();
                                        if (arrProducts != null && arrProducts.size() > 0) {
                                            rvProducts.setVisibility(View.VISIBLE);
                                            crateProductsAdapter = new CrateProductsAdapter(getActivity(), arrProducts);
                                            rvProducts.setAdapter(crateProductsAdapter);
                                            rvProducts.setLayoutManager(new LinearLayoutManager(getActivity()));
                                        } else {
                                            rvProducts.setVisibility(View.GONE);
                                        }

                                    }
                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                                    //layoutParams.setMargins(0, 10, 0, 0);
                                    llContainerData.addView(crateView, layoutParams);
                                }
                            } else {
                                llContainer.setVisibility(View.GONE);
                            }
                        }


                    } else {
                        // Utils.showAlertDialog(getActivity(), message);
                        String responsedata = response.optString("ResponseBody");
                        SvData.setVisibility(View.GONE);
                        llNodata.setVisibility(View.VISIBLE);
                        tvNodata.setText("" + responsedata);


                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(request, AppURL.getCrateDetailsURL);
        } else {
        }
    }

    public void validate_textviews(TextView tv, String data, int flag) {
        if (data == null || data == "" || data.length() <= 0) {
            String a = "N/A";
            tv.setText(a);
        } else {
            if (flag == 1) {
                // tv.setText(new DecimalFormat("##.##").format(data));
                tv.setText("" + String.format("%.2f", Double.parseDouble(data)));
            } else {
                tv.setText("" + data);
            }
        }

    }
   /* private void getOrderData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
//                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("sales_token", "c3c87d3af655462199b36624cbebae6e");
                jsonObject.put("order_id", "16440");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getOrderListViewURL, map, ScanFragment.this, ScanFragment.this, PARSER_TYPE.GET_ORDER_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getOrderListViewURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        Log.e("message", "" + intentResult.getContents());
        content = intentResult.getContents();
        try {
            if (content.length() < 15 || content == null) {
                Toast.makeText(getActivity(), "please..scan the code properly", Toast.LENGTH_SHORT).show();
            }
            if (content.length() == 15) {
                etScan.setText("" + content);
                etScan.setSelection(etScan.getText().length());
            }
        } catch (Exception e) {
        }
       /* if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                etScan.setText(""+contents);
                Log.e("message", "contents: " + contents);
            } else if (resultCode == RESULT_CANCELED) {
// Handle cancel
                Log.e("meassge", "RESULT_CANCELED");
            }
        }*/
    }


/*    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.GET_ORDER_DATA) {
                        if (response instanceof ArrayList) {
                            arrData = (ArrayList<SpecificationsModel>) response;
                            for (int j = 0; j < arrData.size(); j++) {
                                View view = getActivity().getLayoutInflater().inflate(R.layout.row_specifications, null);
                                tvSpecName = (TextView) view.findViewById(R.id.tv_spec_name);
                                tvSpecValue = (TextView) view.findViewById(R.id.tv_spec_value);
                                tvSpecName.setText(Utils.capitalizeWords(arrData.get(j).getSpecName()));
                                tvSpecValue.setText(arrData.get(j).getSpecValue());
                                llOrderData.addView(view);
                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }*/

   /* @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }*/

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

        View radioButton = group.findViewById(checkedId);
        int index = group.indexOfChild(radioButton);
        SvData.setVisibility(View.GONE);
        etScan.setText("");

        switch (index) {

            case 0:
                view1.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.black));
                view2.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.background_grey));
                break;
            case 1:

                view2.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.black));
                view1.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.background_grey));

                if (etScan.getText().toString() == "" || etScan.getText().toString() == null) {
                    SvData.setVisibility(View.GONE);
                }
                if (etScan.getText().toString().length() >= 15) {
                    getCrateData();
                }
                break;
        }

    }
}
