package com.ebutor.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.OrderDetailActivity;
import com.ebutor.R;
import com.ebutor.adapters.CashBackHistoryAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CashBackHistoryModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class CashBackHistoryFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>{


    CashBackHistoryAdapter cashBackHistoryAdapter;
    RecyclerView recyclerView;
    TextView tvnoresult;
    boolean isFF, isCheckIn;
    private String requestType = "";
    private SharedPreferences mSharedPreferences;
    private ArrayList<CashBackHistoryModel> cashBackHistoryModels;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cash_back_history, container, false);
        setHasOptionsMenu(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_cashback_history);
        tvnoresult = (TextView) view.findViewById(R.id.no_result);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return view;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        isCheckIn = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_CHECKIN, false);

    }

    @Override
    public void onResume() {
        super.onResume();
        getCashBackHistory();
    }

    private void getCashBackHistory() {

        if (Networking.isNetworkAvailable(getActivity())) {

            tvnoresult.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            try {

                JSONObject jsonObject = new JSONObject();
                if (isFF && !isCheckIn) {
                    jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));

                } else {

                    jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                    jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                }

                HashMap<String, String> map1 = new HashMap<>();
                map1.put("data", jsonObject.toString());

                VolleyBackgroundTask cartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getCashbackDataHistory, map1, CashBackHistoryFragment.this, CashBackHistoryFragment.this, PARSER_TYPE.CASHBACK_HISTORY);
                cartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(cartRequest, AppURL.getCashbackDataHistory);

                if (dialog != null)
                    dialog.show();


            } catch (Exception e) {

                e.printStackTrace();
            }

        } else {

            requestType = "getOrderCashbackData";

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {


        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        if (response != null) {

            try {

                if (status.equalsIgnoreCase("success")) {

                    if (requestType == PARSER_TYPE.CASHBACK_HISTORY) {

                        Log.e("cashBackHistory", response.toString());
                        if (response instanceof ArrayList) {

                            cashBackHistoryModels = (ArrayList<CashBackHistoryModel>) response;

                            if (cashBackHistoryModels != null && cashBackHistoryModels.size() > 0) {
                                recyclerView.setVisibility(View.VISIBLE);
                                tvnoresult.setVisibility(View.GONE);
                                cashBackHistoryAdapter = new CashBackHistoryAdapter(getActivity(), cashBackHistoryModels);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                recyclerView.setAdapter(cashBackHistoryAdapter);


                            } else {
                                tvnoresult.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }
                        }

                    }
                }
            } catch (Exception e) {

            }

        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }


}
