package com.ebutor.fragments;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.SimpleTagImageView;
import com.ebutor.database.DBHelper;
import com.ebutor.models.AddCart1Response;
import com.ebutor.models.CartModel;
import com.ebutor.models.CashBackDetailsModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.NewPacksModel;
import com.ebutor.models.NewProductModel;
import com.ebutor.models.NewVariantModel;
import com.ebutor.models.ProductSlabData;
import com.ebutor.models.TestProductModel;
import com.ebutor.models.WishListModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class ItemCartFragment extends DialogFragment implements Response.ErrorListener, VolleyHandler<Object> {

    public LayoutInflater inflater;
    OnDialogClosed onDialogClosed;
    JSONArray variantsJSONArray = new JSONArray();
    private ProgressBar progress;
    private TextView tvFreebieDesc;
    private TextView tvNoPacks;
    private CartModel cartModel;
    private NewPacksModel selectedPackModel;
    private TestProductModel productModel, selectedWishListProductModel;
    private SimpleTagImageView productImage;
    private LinearLayout llCashBack, skuItemsLayout, slabsLayout, variantsLayout1, variantsLayout2, innerVariantsLayout1, innerVariantsLayout2;
    private TextView tvProductName,/* tvProductSKU,*/
            tvProductMRP, tvESP, tvInv, tvCfc;
    private String variant_1_name, variant_2_name, variant_3_name, marginToApply, unitPriceToApply;
    private View view;
    private String selectedProductId;
    private ArrayList<ProductSlabData> arrFreebieSlabData;
    private int cartCount = 0;
    private TestProductModel selectedProductModel;
    private SharedPreferences mSharedPreferences;
    private DisplayImageOptions options;
    private TextView tvRating, tvEcashDesc;
    private int totalQty = 0, selPackBlockedQty = 0, selPackPromotionId = 0, selProductSlabId = 0;
    private int freebieMpq;
    private double totalPrice = 0.0;
    private Tracker mTracker;
    private View offerDescView, offerEcashView;
    private Button btnOfferImage, ivOffer;
    private ImageView ivWishList,ivCashback;
    private String freebieProductId, freebieQty, name;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private ArrayList<WishListModel> wishListArray;
    private String rating = "", productId, parentId, slabProductId;
    private int quantity;
    private Context context;
    private boolean isAddToCart = false, isFF = false, selPackIsSlab = false,isSalesAgent = false;
    private ArrayList<String> arrVariant2;
    private Dialog dialog;
    WishListFragment.OkListener confirmListener = new WishListFragment.OkListener() {
        @Override
        public void onOkClicked(String wishListId) {
            addProductToWishList(wishListId);
        }
    };
    private ArrayList<NewPacksModel> arrSlabRates;
    private Button rupee;
    View.OnClickListener onVariantClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            Toast.makeText(getActivity(), "Variant Clicked", Toast.LENGTH_SHORT).show();
            marginToApply = "0";
            unitPriceToApply = "0";
            if (v != null && v.getId() == R.id.id_variant_one) {
                int count = variantsLayout1.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout1.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                Object object = v.getTag();
                if (object != null && object instanceof String) {
                    variant_2_name = (String) object;

                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                    ll.removeAllViews();

                    innerVariantsLayout2 = (LinearLayout) view.findViewById(R.id.inner_variants_2);
                    innerVariantsLayout2.removeAllViews();
                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, "");
                    if (testProductModel != null) {
                        if (testProductModel.getProductTitle() != null && !TextUtils.isEmpty(testProductModel.getProductTitle()))
                            tvProductName.setText(testProductModel.getProductTitle().trim());
                        ImageLoader.getInstance().displayImage(testProductModel.getThumbnailImage(), productImage, options, animateFirstListener);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(testProductModel.getMrp());
                        } catch (Exception e) {
                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb, tvProductMRP);
                    }
                    String variant2list = DBHelper.getInstance().getVariant3List(parentId, variant_1_name, variant_2_name);
                    if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                        if (variant2list != null && variant2list.length() > 0)
                            arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));
                        if (arrVariant2 != null && arrVariant2.size() > 0) {
                            // has inner variants, Create one more variants row
                            View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                        /*int childCount = innerVariantsLayout.getChildCount();
                        if(childCount >= 2){
                            innerVariantsLayout.removeViewAt(1);
                        }*/
                            if (innerVariantsView != null) {
                                variantsLayout2 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout2 != null) {
                                    variantsLayout2.removeAllViews();
                                    int selectedPos = 0;
                                    ArrayList<String> arrTemp = new ArrayList<>();
                                    String name = DBHelper.getInstance().getVariantName(productId, variant_1_name, variant_2_name, "", DBHelper.COL_PRODUCT_VARIANT3);
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "", color = "#FFFFFF";
                                        if (arrVariant2.get(i).split(":").length > 0) {
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        }
                                        if (arrVariant2.get(i).split(":").length > 1) {
                                            color = arrVariant2.get(i).split(":")[1];
                                        }
                                        if (childVariant.equalsIgnoreCase(name) && !arrTemp.contains(childVariant)) {
//                                            selectedPos = i;
                                            arrTemp.add(childVariant);
                                            LinearLayout childVariantLabel = createChildVariant(childVariant, color, R.id.id_variant_two);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout2.addView(childVariantLabel);
                                        }


//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }
                                    if (variantsLayout2.getChildCount() > 0) {
                                        variantsLayout2.getChildAt(selectedPos).performClick();
                                        variantsLayout2.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout2.addView(innerVariantsView);

                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //add packs
                        if (testProductModel != null) {
                            slabProductId = testProductModel.getProductId();
                            selectedProductModel = testProductModel;
                            LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            TextView tvEsp = (TextView) view.findViewById(R.id.tvESP);
                            TextView tvInv = (TextView) view.findViewById(R.id.tvInv);
                            TextView tvCfc = (TextView) view.findViewById(R.id.tvcfc);
                            ImageView ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);
                            if (testProductModel.isCashback()) {
                                ivCashback.setVisibility(View.VISIBLE);
                            } else {
                                ivCashback.setVisibility(View.GONE);
                            }
                            if (tvEsp != null)
                                tvEsp.setText("");
                            if (tvInv != null)
                                tvInv.setVisibility(View.GONE);
                            if (tvCfc != null)
                                tvCfc.setVisibility(View.GONE);
                            getSlabRates(slabProductId);
                        }
                    }
                }
            } else if (v.getId() == R.id.id_variant_two) {
                //todo
                Object object = v.getTag();
                int count = variantsLayout2.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout2.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                if (v.getTag() != null)
                    variant_3_name = (String) v.getTag();
                final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                ll.removeAllViews();
                TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, variant_3_name);
                if (testProductModel != null) {

                    if (testProductModel.getProductTitle() != null && !TextUtils.isEmpty(testProductModel.getProductTitle()))
                        tvProductName.setText(testProductModel.getProductTitle().trim());
                    ImageLoader.getInstance().displayImage(testProductModel.getThumbnailImage(), productImage, options, animateFirstListener);
                    double mrp = 0;
                    try {
                        mrp = Double.parseDouble(testProductModel.getMrp());
                    } catch (Exception e) {
                    }
                    final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                    setMRP(sb, tvProductMRP);

                    slabProductId = testProductModel.getProductId();
                    selectedProductModel = testProductModel;
                    LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                    slabRatesLayout.removeAllViews();
                    TextView tvEsp = (TextView) view.findViewById(R.id.tvESP);
                    TextView tvInv = (TextView) view.findViewById(R.id.tvInv);
                    TextView tvCfc = (TextView) view.findViewById(R.id.tvcfc);
                    ImageView ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);
                    if (testProductModel.isCashback()) {
                        ivCashback.setVisibility(View.VISIBLE);
                    } else {
                        ivCashback.setVisibility(View.GONE);
                    }
                    if (tvEsp != null)
                        tvEsp.setText("");
                    if (tvInv != null)
                        tvInv.setVisibility(View.GONE);
                    if (tvCfc != null)
                        tvCfc.setVisibility(View.GONE);
                    getSlabRates(slabProductId);

                }
            } else if (v.getId() == R.id.id_slab_qty) {
                if (v.getTag() != null) {
                    NewPacksModel newPacksModel = (NewPacksModel) v.getTag();
                    selectedPackModel = newPacksModel;
                    int slabsCount = slabsLayout.getChildCount();
                    for (int i = 0; i < slabsCount; i++) {
                        View btnView = slabsLayout.getChildAt(i);
                        btnView.setSelected(false);
                        selectedPackModel.setSelected(false);
                    }
                    v.setSelected(true);
                    selectedPackModel.setSelected(true);

                    if (tvESP != null) {

                        double ptr = 0;
                        try {
                            ptr = Double.parseDouble(newPacksModel.getPtr());
                        } catch (Exception e) {

                        }
                        if (ptr > 0) {
                            final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getPtr() == null ? getActivity().getResources().getString(R.string.ptr) + " : " : getActivity().getResources().getString(R.string.ptr) + " : " + String.format(Locale.CANADA, "%.2f", ptr));
                            setMRP(sb, tvESP);

                        } else {
                            tvESP.setText("");
                        }
                    }
                    if (isFF) {
                        tvInv.setVisibility(View.VISIBLE);
                        tvCfc.setVisibility(View.VISIBLE);
                    } else {
                        tvInv.setVisibility(View.GONE);
                        tvCfc.setVisibility(View.GONE);
                    }

                    if (tvInv != null) {

                        int inv = 0;
                        try {
                            inv = Integer.parseInt(newPacksModel.getStock());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getStock() == null ? getResources().getString(R.string.inv) + " : " : getResources().getString(R.string.inv) + " : " + inv);
                        setMRP(sb, tvInv);
                    }
                    if (tvCfc != null) {

                        int cfc = 0;
                        try {
                            cfc = Integer.parseInt(newPacksModel.getCfc());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getCfc() == null ? getResources().getString(R.string.cfc) + " : " : getResources().getString(R.string.cfc) + " : " + cfc);
                        setMRP(sb, tvCfc);
                    }
                    if (newPacksModel != null) {
                        final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                        ll.removeAllViews();
                        createPacksForVariant(newPacksModel, ll);
                    }
                }
            }
        }
    };

    public static ItemCartFragment newInstance(TestProductModel testProductModel, String productId, String parentId, int quantity, boolean isAddToCart) {

        ItemCartFragment f = new ItemCartFragment();
        Bundle args = new Bundle();
        args.putSerializable("model", testProductModel);
        args.putString("productId", productId);
        args.putString("parentId", parentId);
        args.putInt("qty", quantity);
        args.putBoolean("isAdd", isAddToCart);
        f.setArguments(args);
        return f;
    }

    public void setClickListener(OnDialogClosed listener) {
        onDialogClosed = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        theme = android.R.style.Theme_Holo_Dialog_NoActionBar;
        setStyle(style, theme);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_cart, container);
        return view;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.getSerializable("model") != null) {
            productModel = (TestProductModel) args.getSerializable("model");
        }
        if (args != null) {
            isAddToCart = args.getBoolean("isAdd");
            productId = args.getString("productId");
            parentId = args.getString("parentId");
            quantity = args.getInt("qty");

        }
        this.view = view;
        inflater = getActivity().getLayoutInflater();
        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();
        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        wishListArray = new ArrayList<WishListModel>();

        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        isSalesAgent = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_SALES_AGENT, false);

        productImage = (SimpleTagImageView) view.findViewById(R.id.productImage);
        ivOffer = (Button) view.findViewById(R.id.iv_offer);
        setOfferImageBackground(productModel.getPackType());
        tvProductName = (TextView) view.findViewById(R.id.tvProductName);
//        tvProductSKU = (TextView) view.findViewById(R.id.tvProductSKU);
        tvProductMRP = (TextView) view.findViewById(R.id.tvProductMRP);
        tvESP = (TextView) view.findViewById(R.id.tvESP);
        tvInv = (TextView) view.findViewById(R.id.tvInv);
        tvCfc = (TextView) view.findViewById(R.id.tvcfc);
        skuItemsLayout = (LinearLayout) view.findViewById(R.id.skuItemsLayout);



        tvRating = (TextView) view.findViewById(R.id.tvRating);
        ivWishList = (ImageView) view.findViewById(R.id.ivWishlist);
        ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);;

        if (productModel.isCashback()) {
            ivCashback.setVisibility(View.VISIBLE);
        } else {
            ivCashback.setVisibility(View.GONE);
        }

        ivWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedWishListProductModel = productModel;
                // Wish list
                selectedProductId = productModel.getProductId();
                if (wishListArray.size() == 0) {
                    if (Networking.isNetworkAvailable(getActivity())) {
                        try {
                            JSONObject obj = new JSONObject();
                            obj.put("flag", "1");
                            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                            obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                            HashMap<String, String> map = new HashMap<>();
                            map.put("data", obj.toString());

                            VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.shoppinglistURL, map, ItemCartFragment.this, ItemCartFragment.this, PARSER_TYPE.WISHLIST);
                            getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.shoppinglistURL);
                            if (dialog != null)
                                dialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.no_network_connection));
                    }
                } else {
                    showWishListDialog();
                }

                return;


            }
        });

//        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0 && productModel.getVariantModelArrayList().get(0).getProductName() != null && !TextUtils.isEmpty(productModel.getVariantModelArrayList().get(0).getProductName()))
//            tvProductName.setText(productModel.getProductTitle());
//        else
        tvProductName.setText(productModel.getProductTitle().trim());
        ImageLoader.getInstance().displayImage(productModel.getThumbnailImage(), productImage, options, animateFirstListener);
        double mrp = 0;
        try {
            mrp = Double.parseDouble(productModel.getMrp());
        } catch (Exception e) {
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(productModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
        setMRP(sb, tvProductMRP);
        ImageLoader.getInstance().displayImage(productModel.getThumbnailImage(), productImage, options, animateFirstListener);
//        rating = productModel.getProductRating();
//        if (rating == null || TextUtils.isEmpty(rating)) {
//            rating = "0.0";
//        }
//        if (rating != null && !TextUtils.isEmpty(rating) && !rating.equalsIgnoreCase("null")) {
//            DecimalFormat format = new DecimalFormat("0.0");
//            double newmargin = Double.parseDouble(rating);
//            rating = format.format(newmargin);
//        }
//
//        tvRating.setText(rating.equalsIgnoreCase("null") ? "0.0" : rating);
        skuItemsLayout.removeAllViews();
        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
            ArrayList<String> arrTemp = new ArrayList<>();
            for (int j = 0; j < productModel.getVariantModelArrayList().size(); j++) {
                String variantName = DBHelper.getInstance().getVariantName(productId, "", "", "", DBHelper.COL_PRODUCT_VARIANT1);

                DisplayMetrics displaymetrics = new DisplayMetrics();
                WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
                windowManager.getDefaultDisplay().getMetrics(displaymetrics);
                int height = displaymetrics.heightPixels;

                final LinearLayout skuView = new LinearLayout(getActivity());
                skuView.setOrientation(LinearLayout.HORIZONTAL);
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                params1.setMargins(0, 0, 20, 0);
                skuView.setLayoutParams(params1);

                ArrayList<String> variantsList = productModel.getVariantModelArrayList();
                name = "";
                String color = "#FFFFFF";
                if (variantsList.get(j).split(":").length > 0) {
                    name = variantsList.get(j).split(":")[0];
                }
                if (variantsList.get(j).split(":").length > 1) {
                    color = variantsList.get(j).split(":")[1];
                }

                if (!arrTemp.contains(name)) {
                    arrTemp.add(name);

                    ImageView imageView = new ImageView(getActivity());
                    Drawable sourceDrawable = getActivity().getResources().getDrawable(R.drawable.ic_star);

                    Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
                    Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(color));
                    imageView.setImageBitmap(mFinalBitmap);
                    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                    params2.gravity = Gravity.CENTER_VERTICAL;
                    params2.setMargins(0, 0, 0, 0);
                    imageView.setLayoutParams(params2);

                    final TextView textView = new TextView(getActivity());
                    textView.setTextSize(12);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
                    params.gravity = Gravity.CENTER_VERTICAL;
                    params.setMargins(5, 0, 0, 0);
                    textView.setGravity(Gravity.CENTER);
                    textView.setLayoutParams(params);
                    skuView.setId(R.id.id_one);
                    skuView.setTag(name);
                    textView.setText(name == null ? "" : name);
                    if (productModel.isChild() && j == 0) {
                        skuView.setSelected(true);
                        OnProductSKUClicked(name, productModel.isChild());
                    } else {
                        if (name.equalsIgnoreCase(variantName)) {
                            skuView.setSelected(true);
                            OnProductSKUClicked(name, productModel.isChild());
                        } else {
                            skuView.setSelected(false);
                        }
                    }
                    skuView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int count = skuItemsLayout.getChildCount();
                            if (count > 0) {
                                for (int i = 0; i < count; i++) {
                                    View btnView = skuItemsLayout.getChildAt(i);
                                    btnView.setSelected(false);
                                }
                            }
                            v.setSelected(true);
                            TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, name, "", "");
                            if (testProductModel != null) {
                                double mrp = 0;
                                try {
                                    mrp = Double.parseDouble(testProductModel.getMrp());
                                } catch (Exception e) {
                                }
                                final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                                setMRP(sb, tvProductMRP);
                                ImageLoader.getInstance().displayImage(testProductModel.getThumbnailImage(), productImage, options, animateFirstListener);
                                tvProductName.setText(testProductModel.getProductTitle().trim());
                            }

                            OnProductSKUClicked(skuView.getTag(), productModel.isChild());
                        }
                    });
                    if (productModel.isChild() || name.equalsIgnoreCase(variantName)) {

                        skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
                        skuView.setPadding(10, 5, 10, 5);
                        skuView.addView(imageView);
                        skuView.addView(textView);

                        skuItemsLayout.addView(skuView);
                    }


                /*if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
                    final NewVariantModel SKUModel = productModel.getVariantModelArrayList().get(0);
                    if (SKUModel.getPacksModelArrayList() != null && SKUModel.getPacksModelArrayList().size() > 0) {
                        NewPacksModel skuPacksModel = SKUModel.getPacksModelArrayList().get(0);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(skuModel.getMrp());
                        } catch (Exception e) {
                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(skuModel.getMrp() == null ? "" : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb);
                    }
                }*/
                }
            }

        }

//        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
//                                        /*SKUModel skuModel = productModel.getSkuModelArrayList().get(0);
//                                        OnProductSKUClicked(skuModel, expandedPosition);*/
//            NewVariantModel selectedSKUModel = null;
//            for (NewVariantModel skuModel : productModel.getVariantModelArrayList()) {
//                if (skuModel.isSelected()) {
//                    selectedSKUModel = skuModel;
//                    break;
//                }
//            }
//
//            if (selectedSKUModel == null) {
//                for (NewVariantModel skuModel : productModel.getVariantModelArrayList()) {
//                    if (skuModel.isDefault()) {
//                        selectedSKUModel = skuModel;
//                        break;
//                    }
//                }
//            }
//
//            if (selectedSKUModel != null) {
//                OnProductSKUClicked(selectedSKUModel);
//            }
//        }
    }

    public void setOfferImageBackground(String packType) {
        switch (packType) {
            case "Regular":
            case "REGULAR":
                ivOffer.setVisibility(View.GONE);
                break;
            case "Consumer Pack":
            case "CONSUMER PACK":
            case "CP Inside":
            case "CP Outside":
            case "Consumer Pack Inside":
            case "Consumer Pack Outside":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_green);
                ivOffer.setText("CP Offer");
                break;
            case "Freebie":
            case "FREEBIE":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_red);
                ivOffer.setText("Freebie");
                break;
            default:
                ivOffer.setVisibility(View.GONE);
                break;
        }
    }

    private void setMRP(SpannableStringBuilder sb, TextView textView) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
//        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 3, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public void OnProductSKUClicked(Object object, boolean isChild) {

        if (object == null)
            return;

        if (object instanceof String) {
//            final NewVariantModel model = (NewVariantModel) object;

//            int qty = 0, temp = 0, totalQuantity = 0;
//            try {
//                totalQuantity = Integer.parseInt(model.getOrderedQuantity());
//            } catch (Exception e) {
//                totalQuantity = 0;
//            }
//
//            if (model.getPacksModelArrayList() != null && model.getPacksModelArrayList().size() > 0) {
//                for (int i = model.getPacksModelArrayList().size() - 1; i >= 0; i--) {
//                    NewPacksModel skuPacksModel = model.getPacksModelArrayList().get(i);
//                    int packQty = Integer.parseInt(skuPacksModel.getPackSize());
//                    if (totalQuantity >= packQty) {
//                        qty = totalQuantity / packQty;
//                        skuPacksModel.setQty(qty * packQty);
//                        skuPacksModel.setEsuQty(qty);
//                        totalQuantity = totalQuantity % packQty;
//                        i++;
//                        continue;
//                    }
//                }
//            }

            variant_1_name = (String) object;
            marginToApply = "0";
            unitPriceToApply = "0";
            if (offerDescView != null)
                offerDescView.setVisibility(View.GONE);

            final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
            ll.removeAllViews();

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            innerVariantsLayout1 = (LinearLayout) view.findViewById(R.id.inner_variants);
            if (innerVariantsLayout1 != null) {
                innerVariantsLayout1.removeAllViews();

                if (isChild) {
                    slabProductId = parentId;
                    TestProductModel testProductModel = DBHelper.getInstance().getProductById(parentId, true);
                    if (testProductModel != null) {
                        productId = testProductModel.getProductId();
                        slabProductId = productId;
                        selectedProductModel = testProductModel;
                        getSlabRates(productId);
                    }
                } else {

                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, "");
                    if (testProductModel != null) {
                        if (testProductModel.getProductTitle() != null && !TextUtils.isEmpty(testProductModel.getProductTitle()))
                            tvProductName.setText(testProductModel.getProductTitle().trim());
                        ImageLoader.getInstance().displayImage(testProductModel.getThumbnailImage(), productImage, options, animateFirstListener);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(testProductModel.getMrp());
                        } catch (Exception e) {
                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb, tvProductMRP);
                    }

                    String variant2list = DBHelper.getInstance().getVariant2List(parentId, variant_1_name);
                    if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                        if (variant2list != null && variant2list.length() > 0)
                            arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));

                        if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                            // has inner variants, Create one more variants row
                            View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                            if (innerVariantsView != null) {
                                variantsLayout1 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout1 != null) {
                                    variantsLayout1.removeAllViews();

                                    int selectedPos = 0;
                                    ArrayList<String> arrTemp = new ArrayList<>();
                                    String name = DBHelper.getInstance().getVariantName(productId, variant_1_name, "", "", DBHelper.COL_PRODUCT_VARIANT2);
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "", color = "#FFFFFF";
                                        if (arrVariant2.get(i).split(":").length > 0) {
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        }
                                        if (arrVariant2.get(i).split(":").length > 1) {
                                            color = arrVariant2.get(i).split(":")[1];
                                        }
                                        if (childVariant.equalsIgnoreCase(name) && !arrTemp.contains(childVariant)) {
//                                            selectedPos = i;
                                            arrTemp.add(childVariant);
                                            LinearLayout childVariantLabel = createChildVariant(childVariant, color, R.id.id_variant_one);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout1.addView(childVariantLabel);
                                        }

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout1.getChildCount() > 0) {
                                        variantsLayout1.getChildAt(selectedPos).performClick();
                                        variantsLayout1.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout1.addView(innerVariantsView);


                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //create packs
                        testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, "", "");
                        if (testProductModel != null) {
                            slabProductId = testProductModel.getProductId();
                            selectedProductModel = testProductModel;
                            getSlabRates(slabProductId);
                        }
                    }
                }
            }
        }
    }

    private void createPacksForVariant(final NewPacksModel skuPacksModel, final LinearLayout linearLayout) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        int qty = 0, totalQuantity = 0;
//        totalQuantity = quantity;

//        if (arrSlabRates != null && arrSlabRates.size() > 0) {
//            for (int i = arrSlabRates.size() - 1; i >= 0; i--) {
//                NewPacksModel newPacksModel = arrSlabRates.get(i);
//                int packQty = 0;
//                try {
//                    packQty = Integer.parseInt(newPacksModel.getPackSize());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (totalQuantity >= packQty) {
//                    qty = totalQuantity / packQty;
//                    newPacksModel.setQty(qty * packQty);
//                    newPacksModel.setEsuQty(qty);
//                    totalQuantity = totalQuantity % packQty;
//                    i++;
//                    continue;
//                }
//            }
//        }

        if (skuPacksModel != null) {
            View packView = inflater.inflate(R.layout.row_packs_header, null);

            TextView tvPackOf = (TextView) packView.findViewById(R.id.tvPackOf);
            TextView tvPackPrice = (TextView) packView.findViewById(R.id.tvPackPrice);
            TextView tvUnitPrice = (TextView) packView.findViewById(R.id.tvUnitPrice);
            TextView tvMargin = (TextView) packView.findViewById(R.id.tvMargin);

            tvPackOf.setText(String.valueOf(skuPacksModel.getPackSize() == null ? "--" : skuPacksModel.getPackSize()));

            double packPrice = 0;
            try {
                packPrice = (Double.parseDouble(skuPacksModel.getPackSize()) * Double.parseDouble(skuPacksModel.getUnitPrice()));
            } catch (Exception e) {
            }
            tvPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

            double unitPrice = 0;
            try {
                unitPrice = Double.parseDouble(skuPacksModel.getUnitPrice());
            } catch (Exception e) {
            }
            tvUnitPrice.setText(skuPacksModel.getUnitPrice() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", unitPrice));

            double margin = 0;
            try {
                margin = Double.parseDouble(skuPacksModel.getMargin());
            } catch (Exception e) {
            }
            tvMargin.setText(skuPacksModel.getMargin() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", margin));

            final TextView tvQty = (TextView) packView.findViewById(R.id.tvQuantity);
            tvQty.setText(skuPacksModel.getEsuQty() + "");
            ImageButton ibMinus = (ImageButton) packView.findViewById(R.id.ibMinus);
            ImageButton ibPlus = (ImageButton) packView.findViewById(R.id.ibPlus);

            ibPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    String availableQty = model.getAvailableQuantity();
                    String availableQty = "100";//temp

                    if (availableQty != null && !TextUtils.isEmpty(availableQty)) {
                        int variantQuantity = 0;
                        try {
                            variantQuantity = Integer.parseInt(availableQty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (variantQuantity > 0) {
                            int qty = skuPacksModel.getQty();
                            int esuQty = skuPacksModel.getEsuQty();

                            int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                    0 : Integer.parseInt(skuPacksModel.getPackSize());
                            int slabEsu = skuPacksModel.getSlabEsu() == 0 ? 1 : skuPacksModel.getSlabEsu();
                            int updatedQty = qty + (packSize * slabEsu);
                            esuQty = esuQty + 1;
                            tvQty.setText("" + esuQty);
                            skuPacksModel.setQty(updatedQty);
                            skuPacksModel.setEsuQty(esuQty);
                            updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                        } else {
                            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.quantity_not_available));
                        }
                    }
                }
            });

            ibMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = skuPacksModel.getQty();
                    int esuQty = skuPacksModel.getEsuQty();

                    if (esuQty <= 0) {
                        tvQty.setText("0");
                        skuPacksModel.setQty(0);
                        skuPacksModel.setEsuQty(esuQty);
                    } else {
                        int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                0 : Integer.parseInt(skuPacksModel.getPackSize());
                        int slabEsu = skuPacksModel.getSlabEsu() == 0 ? 1 : skuPacksModel.getSlabEsu();
                        int updatedQty = qty - (packSize * slabEsu);
                        esuQty = esuQty - 1;
                        if (esuQty < 0) {
                            tvQty.setText("0");
                        } else {
                            tvQty.setText("" + esuQty);
                        }
                        skuPacksModel.setQty(updatedQty);
                        skuPacksModel.setEsuQty(esuQty);
                    }
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                }
            });

            linearLayout.addView(packView);
        }

        View footerView = inflater.inflate(R.layout.row_packs_footer, null);
        linearLayout.addView(footerView);

        footerView.findViewById(R.id.tvAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OnCartItemAdded(productModel);
            }
        });
        updateFooter(footerView, skuPacksModel);

        TextView tvAddToCart = (TextView) footerView.findViewById(R.id.tvAddToCart);
        if (isAddToCart)
            tvAddToCart.setText(getActivity().getResources().getString(R.string.add_to_cart));
        else
            tvAddToCart.setText(getActivity().getResources().getString(R.string.update_cart));
        tvAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int totalQuantity = 0;
//                NewVariantModel skuModel = productModel.getVariantModelArrayList().get(0);
//                for (int i = 0; i < skuModel.getPacksModelArrayList().size(); i++) {
//                    NewPacksModel skuPacksModel = skuModel.getPacksModelArrayList().get(i);
//                    totalQuantity += skuPacksModel.getQty();
//                }
//                if (totalQuantity > 0)
                OnCartItemAdded(productModel);
//                else
//                    Toast.makeText(getActivity(), "Total Quantity is zero", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private LinearLayout createChildVariant(String name, String color, int id) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        LinearLayout skuView = new LinearLayout(getActivity());
        skuView.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params1.setMargins(0, 0, 20, 0);
        skuView.setLayoutParams(params1);

        ImageView imageView = new ImageView(getActivity());
        Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_star);

        Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
        Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(color));
        imageView.setImageBitmap(mFinalBitmap);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params2.gravity = Gravity.CENTER_VERTICAL;
        params2.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(params2);

        TextView textView = new TextView(getActivity());
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.gravity = Gravity.CENTER_VERTICAL;
        params.setMargins(5, 0, 0, 0);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        skuView.setId(id);
        skuView.setTag(name);
        textView.setText(name == null ? "" : name);

        skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
        skuView.setPadding(10, 5, 10, 5);
        skuView.addView(imageView);
        skuView.addView(textView);

        return skuView;
    }

    private void updateFooter(View footerView, NewPacksModel newPacksModel) {
        if (footerView == null) {
            return;
        }
        if (newPacksModel == null)
            return;

        int totalQty = 0;
        marginToApply = "0";
        unitPriceToApply = "0";

        for (NewPacksModel skuPacksModel : arrSlabRates) {
            totalQty = totalQty + skuPacksModel.getQty();
        }

        for (NewPacksModel skuPacksmodel : arrSlabRates) {
            int packQty = Integer.parseInt(skuPacksmodel.getPackSize());
            int esu = skuPacksmodel.getSlabEsu();
            if (totalQty >= packQty * esu) {
                marginToApply = skuPacksmodel.getMargin();
                unitPriceToApply = skuPacksmodel.getUnitPrice();
                selPackBlockedQty = skuPacksmodel.getBlockedQty();
                selPackPromotionId = skuPacksmodel.getPrmtDetId();
                selPackIsSlab = skuPacksmodel.isSlab();
                selProductSlabId = skuPacksmodel.getProductSlabId();
                selectedPackModel = skuPacksmodel;

                freebieProductId = skuPacksmodel.getFreebieProductId();
                freebieMpq = skuPacksmodel.getFreebieMpq();
                //                freebieQty = String.valueOf((totalQty / packQty) * skuPacksmodel.getFreebieQty());
                double _qty = 0;
                if (freebieMpq > 0) {
                    _qty = (int) (totalQty / freebieMpq) * skuPacksmodel.getFreebieQty();
                }
                freebieQty = String.valueOf((int) _qty);
            }
        }

        double unitPrice = 0;
        if (!TextUtils.isEmpty(unitPriceToApply)) {
            unitPrice = Double.parseDouble(unitPriceToApply);
        }

        double margin = 0;
        if (!TextUtils.isEmpty(marginToApply)) {
            margin = Double.parseDouble(marginToApply);
        }

//            skuModel.setAppliedMargin(marginToApply);
//            skuModel.setAppliedMRP(unitPriceToApply);

        TextView tvTotalQty = (TextView) footerView.findViewById(R.id.tvTotalQty);
        TextView tvMargin = (TextView) footerView.findViewById(R.id.tvTotalMargin);
        TextView tvTotalPrice = (TextView) footerView.findViewById(R.id.tvTotalPrice);
        quantity = totalQty;
        if (tvTotalQty != null) {
//                tvTotalQty.setText("TOTAL QTY\n" + totalQty);
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.qty) + "\n" + totalQty);
            setSpannedText(sb, tvTotalQty, 3);
        }

        if (tvMargin != null) {
//                tvMargin.setText("MARGIN\n" + String.format(Locale.CANADA, "%.2f", margin));
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.markup_margin) + "\n" + String.format(Locale.CANADA, "%.2f", margin) + "%");
            setSpannedText(sb, tvMargin, 8);
        }
        totalPrice = unitPrice * totalQty;
//        skuModel.setTotalPrice(String.format(Locale.CANADA, "%.2f", totalPrice));
        if (tvTotalPrice != null) {
//                tvTotalPrice.setText("TOTAL PRICE\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            setSpannedText(sb, tvTotalPrice, 11);

        }

        offerEcashView = view.findViewById(R.id.ll_cashback);
        tvFreebieDesc = (TextView) view.findViewById(R.id.tvFreebieDesc);
        offerDescView = view.findViewById(R.id.ll_freebie_desc);
        btnOfferImage = (Button) view.findViewById(R.id.iv_offer_desc);
        if (selectedPackModel != null && selectedPackModel.getFreebieDesc() != null &&
                !TextUtils.isEmpty(selectedPackModel.getFreebieDesc()) && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("null") && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("0")) {
            tvFreebieDesc.setText(selectedPackModel.getFreebieDesc());
            offerDescView.setVisibility(View.VISIBLE);
            setOfferBanner(selectedProductModel.getPackType(), btnOfferImage);
        } else
            offerDescView.setVisibility(View.GONE);

        if (selectedPackModel != null && selectedPackModel.getArrCashBackDetails() != null && selectedPackModel.getArrCashBackDetails().size() > 0) {
            int count = selectedPackModel.getArrCashBackDetails().size();
            llCashBack = (LinearLayout) view.findViewById(R.id.ll_cash_back);
            llCashBack.removeAllViews();

            for (int i = 0; i < count; i++) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View v = inflater.inflate(R.layout.cashback_dashborad, null);
                offerEcashView = v.findViewById(R.id.ll_cashback);
                tvEcashDesc = (TextView) v.findViewById(R.id.tv_ecash);

                rupee = (Button) v.findViewById(R.id.btn_rupee);

                int cbktyp = selectedPackModel.getArrCashBackDetails().get(i).getCbk_source_type();
                int bentype = selectedPackModel.getArrCashBackDetails().get(i).getBenificiary_type();

                if (!isFF){
                    if (bentype == ConstantValues.CUSTOMER) {
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(R.drawable.cashback_bg_retailer);
                        tvEcashDesc.setTextColor(getResources().getColor(R.color.ecash_desc));
                        rupee.setBackgroundResource(R.drawable.ic_rupee);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                    }
                }else if (isFF && isSalesAgent){
                    if (bentype == ConstantValues.SALES_AGENT || bentype == ConstantValues.CUSTOMER){
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                        tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                        rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                    }
                }else if (isFF){
                    if (bentype == ConstantValues.FF_ASSOCIATE || bentype == ConstantValues.FF_MANAGER || bentype == ConstantValues.CUSTOMER){
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                        tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                        rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                    }
                }else {
                    tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                    tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                    tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                    rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                    offerEcashView.setVisibility(View.VISIBLE);
                    rupee.setVisibility(View.VISIBLE);
                    llCashBack.addView(v);
                }


               /* if (!isFF) {
                    if (bentype == ConstantValues.CUSTOMER) {
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(R.drawable.cashback_bg_retailer);
                        tvEcashDesc.setTextColor(getResources().getColor(R.color.ecash_desc));
                        rupee.setBackgroundResource(R.drawable.ic_rupee);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);

                    }
                } else {
                 //   if (bentype == ConstantValues.CUSTOMER || bentype == ConstantValues.FF_MANAGER || bentype == ConstantValues.FF_ASSOCIATE) {
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                        tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                        rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                  //  }
                }*/


            }

        } else {
            View v = inflater.inflate(R.layout.cashback_dashborad, null);
            v.setVisibility(View.GONE);
            offerEcashView = v.findViewById(R.id.ll_cashback);
            offerEcashView.setVisibility(View.GONE);
        }

    }

    public void setOfferBanner(String packType, View view) {

        if (view == null) {
            return;
        }
        Button ivOffer = (Button) view;

        switch (packType) {

            case "Consumer Pack":
            case "CONSUMER PACK":
            case "CP Inside":
            case "CP Outside":
            case "Consumer Pack Inside":
            case "Consumer Pack Outside":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("CP Offer");
                break;
            case "Freebie":
            case "FREEBIE":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Freebie");
                break;
            default:
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Offer");
                break;
        }
    }

    private void setSpannedText(SpannableStringBuilder sb, TextView textView, int start) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getActivity().getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new RelativeSizeSpan(1.2f), start, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(sb);
    }

    private void getSlabRates(String productId) {

        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.SLAB_RATES;
            obj.put("product_id", productId);
            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            //to cancel already running request(if running)
            MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.getSlabRatesURL);

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSlabRatesURL, map,
                    ItemCartFragment.this, ItemCartFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getSlabRatesURL);

            final FrameLayout fl = (FrameLayout) view.findViewById(R.id.fl_expand_content);
            progress = (ProgressBar) fl.findViewById(R.id.loading);
            tvNoPacks = (TextView) fl.findViewById(R.id.tv_no_packs);
            tvNoPacks.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
//            if (dialog != null)
//                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkInventory(String productId, int quantity) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("product_id", productId);
                obj.put("quantity", quantity);
                obj.put("is_slab", selPackIsSlab == true ? 1 : 0);
                obj.put("blocked_qty", selPackBlockedQty);
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCart1URL, map, ItemCartFragment.this, ItemCartFragment.this, PARSER_TYPE.ADD_CART_1);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.addCart1URL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    public void OnCartItemAdded(Object model) {
        if (model == null)
            return;

        if (model instanceof TestProductModel) {
            ArrayList<NewPacksModel> packs = arrSlabRates;
            totalQty = 0;
            if (packs != null && packs.size() > 0) {
                for (NewPacksModel packModel : packs) {
                    int qty = packModel.getQty();
                    totalQty += qty;
                }
            }

            if (totalQty <= 0) {
                // show alert
                Utils.showAlertDialog(getActivity(), getString(R.string.please_add_packs));
                return;
            }

            if (totalPrice <= 0) {
                Utils.showAlertDialog(getActivity(), getString(R.string.oops_try_again));
                return;
            }

            /*if (selPackIsSlab && selPackBlockedQty > 0 && totalQty > selPackBlockedQty) {
                Utils.showAlertDialog(getActivity(), getString(R.string.qty_grt_blck));
                return;
            }*/

            checkInventory(slabProductId, totalQty);
        }

        updateCartCount(MyApplication.getInstance());
    }

    /*private void updateToCart() {
        if (MyApplication.getInstance().cartSize() > 0) {
//            HashMap<String, ProductModel> map = MyApplication.getInstance().getCurrentCartItemsMap();
//            Set<String> keys = map.keySet();
            try {
                JSONArray productsArray = new JSONArray();
                for (String key : keys) {
                    ProductModel productModel = map.get(key);
                    JSONObject productObj = new JSONObject();
                    productObj.put("product_id", productModel.getProductId());
                    ArrayList<SKUModel> variants = productModel.getSkuModelArrayList();
                    JSONArray variantsJSONArray = new JSONArray();
                    if (variants != null && variants.size() > 0) {
                        for (SKUModel skuModel : variants) {

                            ArrayList<SKUPacksModel> packs = skuModel.getSkuPacksModelArrayList();
                            JSONArray packsArray = new JSONArray();
                            int totalQty = 0;
                            if (packs != null && packs.size() > 0) {
                                for (SKUPacksModel packModel : packs) {
                                    int qty = packModel.getQty();
                                    if (qty > 0) {
                                        JSONObject packObj = new JSONObject();
                                        packObj.put("pack_qty", qty);
                                        packObj.put("packprice_id", packModel.getVariantPriceId());
                                        packsArray.put(packObj);
                                    }
                                    totalQty += qty;
                                }
                                JSONObject variantObj = new JSONObject();
                                variantObj.put("variant_id", skuModel.getSkuId());
                                variantObj.put("total_qty", totalQty);
                                variantObj.put("total_price", skuModel.getTotalPrice());
                                variantObj.put("packs", packsArray);
                                if (totalQty > 0)
                                    variantsJSONArray.put(variantObj);
                            }
                        }
                    }
                    productObj.put("variants", variantsJSONArray);
                    productsArray.put(productObj);
                }

                JSONObject dataObj = new JSONObject();
                dataObj.put("products", productsArray);
                dataObj.put("appId", "1525956d");//todo
                dataObj.put("token", "1164a6b649925cd394228c008c827166");//todo

                Log.e("Cart Obj", dataObj.toString());

                HashMap<String, String> paramsMap = new HashMap<>();
                paramsMap.put("data", dataObj.toString());
                HTTPBackgroundTask cartTask = new HTTPBackgroundTask(this, getActivity(), PARSER_TYPE.EDIT_CART, APIREQUEST_TYPE.HTTP_POST);
                cartTask.execute(paramsMap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/

    public JSONArray getInnerVariantsArray(ArrayList<NewVariantModel> variants) {
        try {
            for (NewVariantModel skuModel : variants) {
                if (skuModel.getHasInnerVariants() == 1) {
                    ArrayList<NewVariantModel> arrInner = skuModel.getVariantModelArrayList();
                    getInnerVariantsArray(arrInner);
                } else {
                    if (variants != null && variants.size() > 0) {
                        for (NewVariantModel newVariantModel : variants) {
                            JSONObject variantObj = new JSONObject();
                            variantObj.put("variant_id", newVariantModel.getSkuId());

                            ArrayList<NewPacksModel> packs = newVariantModel.getPacksModelArrayList();
                            JSONArray packsArray = new JSONArray();
                            int totalQty = 0;
                            if (packs != null && packs.size() > 0) {
                                for (NewPacksModel packModel : packs) {
                                    int qty = packModel.getQty();
//                                    if (qty > 0) {
                                    JSONObject packObj = new JSONObject();
                                    packObj.put("pack_qty", qty);
//                                    packObj.put("packprice_id", packModel.getProductSlabId());
                                    packsArray.put(packObj);
//                                    }
                                    totalQty += qty;
                                }
                            }
                            variantObj.put("total_qty", totalQty);
                            variantObj.put("total_price", newVariantModel.getTotalPrice());
                            variantObj.put("applied_margin", newVariantModel.getAppliedMargin());
                            variantObj.put("unit_price", newVariantModel.getAppliedMRP());
                            variantObj.put("packs", packsArray);
                            if (totalQty > 0) {
                                variantsJSONArray.put(variantObj);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return variantsJSONArray;
    }

    private void updateToCart(NewProductModel productModel) {
        variantsJSONArray = new JSONArray();
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                JSONArray productsArray = new JSONArray();

                JSONObject productObj = new JSONObject();
                productObj.put("product_id", productModel.getProductId());
                ArrayList<NewVariantModel> variants = productModel.getVariantModelArrayList();
                if (variants != null && variants.size() > 0) {
                    variantsJSONArray = getInnerVariantsArray(variants);
                }
                productObj.put("variants", variantsJSONArray);
                if (variantsJSONArray != null && variantsJSONArray.length() > 0) {
                    productsArray.put(productObj);
                }

                if (productsArray.length() > 0) {
                    JSONObject dataObj = new JSONObject();
                    dataObj.put("products", productsArray);
                    dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    dataObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    dataObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    dataObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    Log.e("Cart Obj", dataObj.toString());

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", dataObj.toString());

                    VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCartURL, map, ItemCartFragment.this, ItemCartFragment.this, PARSER_TYPE.EDIT_CART);
                    viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.addCartURL);
                    if (dialog != null)
                        dialog.show();

                } else {
                    Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.please_add_packs));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
        }
    }

    private void updateCartCount(MyApplication application) {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
    }

    public void addProductToWishList(String wishListId) {
        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("flag", "1");
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("buyer_listing_id", wishListId);
                    obj.put("product_id", this.selectedProductId);

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask addWishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productListOperationsURL, map,
                            ItemCartFragment.this, ItemCartFragment.this, PARSER_TYPE.ADDTO_WISHLIST);
                    addWishListRequest.setRetryPolicy(new DefaultRetryPolicy(
                            AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(addWishListRequest, AppURL.productListOperationsURL);
                    if (dialog != null)
                        dialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.no_network_connection));
            }

        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_addto_wishlist), Toast.LENGTH_SHORT).show();
        }
    }

    public void showWishListDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        WishListFragment editNameDialog = WishListFragment.newInstance(wishListArray);
        editNameDialog.setCancelable(true);
        editNameDialog.setConfirmListener(confirmListener);
        editNameDialog.show(fm, "areas_fragment_dialog");
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName(" Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    private void getFreebiePacks(String productId) {
        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.FREEBIE_SLABS;
            obj.put("product_id", productId);
            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            //to cancel already running request(if running)
            MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.getSlabRatesURL);

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSlabRatesURL, map,
                    ItemCartFragment.this, ItemCartFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getSlabRatesURL);

            if (dialog != null)
                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.EDIT_CART) {

                    if (results instanceof Integer) {
                        if (isAddToCart) {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.product_added_cart), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.cart_updated), Toast.LENGTH_LONG).show();
                        }
                        dismiss();
                        onDialogClosed.dialogClose();
//                        getCartCount();
//                        MyApplication.getInstance().addToCart(selectedProductModel, mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""))
                    }

                } else if (requestType == PARSER_TYPE.WISHLIST) {
                    if (results instanceof ArrayList) {
                        wishListArray = (ArrayList<WishListModel>) results;
                        if (wishListArray != null && wishListArray.size() > 0) {
                            showWishListDialog();
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_create_list), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (requestType == PARSER_TYPE.ADDTO_WISHLIST) {
                    Utils.showAlertDialog(getActivity(), message);
                } else if (requestType == PARSER_TYPE.SLAB_RATES) {

                    if (results instanceof ArrayList) {
                        if (progress != null)
                            progress.setVisibility(View.GONE);

                        arrSlabRates = (ArrayList<NewPacksModel>) results;

                        LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                        slabRatesLayout.removeAllViews();
                        if (arrSlabRates != null && arrSlabRates.size() > 0) {

                            int qty = 0, totalQuantity = 0;
                            totalQuantity = quantity;

                            if (arrSlabRates != null && arrSlabRates.size() > 0) {
                                for (int i = arrSlabRates.size() - 1; i >= 0; i--) {
                                    NewPacksModel newPacksModel = arrSlabRates.get(i);
                                    int packQty = 0, slabEsu = newPacksModel.getSlabEsu();
                                    try {
                                        packQty = Integer.parseInt(newPacksModel.getPackSize());
                                        packQty = packQty * slabEsu;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    if (totalQuantity >= packQty) {
                                        qty = totalQuantity / packQty;
                                        newPacksModel.setQty(qty * packQty);
                                        newPacksModel.setEsuQty(qty);
                                        totalQuantity = totalQuantity % packQty;
                                        i++;
                                        continue;
                                    }
                                }
                            }

                            tvNoPacks.setVisibility(View.GONE);
                            View slabRatesView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                            slabsLayout = (LinearLayout) slabRatesView.findViewById(R.id.variants_layout);
                            if (slabsLayout != null) {
                                slabsLayout.removeAllViews();

                                int selectedPos = 0;
                                for (int i = 0; i < arrSlabRates.size(); i++) {
                                    selectedPackModel = arrSlabRates.get(i);
                                    String slab = arrSlabRates.get(i).getPackSize();
                                    String star = arrSlabRates.get(i).getStar();
                                    String slabColor = selectedProductModel.getStar();
                                    int slabEsu = selectedPackModel.getSlabEsu();
                                    CustomerTyepModel customerTyepModel = DBHelper.getInstance().getStarColorName(star);
                                    if (customerTyepModel != null)
                                        slabColor = customerTyepModel.getDescription();
                                    LinearLayout childVariantLabel = createSlabRates(slab, slabColor, slabEsu, R.id.id_slab_qty);
                                    childVariantLabel.setTag(arrSlabRates.get(i));
                                    childVariantLabel.setOnClickListener(onVariantClickListener);
                                    slabsLayout.addView(childVariantLabel);

                                    if (selectedPackModel.isSelected())
                                        selectedPos = i;

                                }

                                if (slabsLayout.getChildCount() > 0) {
                                    slabsLayout.getChildAt(selectedPos).performClick();
                                    slabsLayout.getChildAt(selectedPos).setSelected(true);
                                }

                                slabRatesLayout.addView(slabRatesView);

                            }
                        } else {
                            if (tvNoPacks != null)
                                tvNoPacks.setVisibility(View.VISIBLE);
                        }
                    }

                } else if (requestType == PARSER_TYPE.FREEBIE_SLABS) {
                    if (results instanceof ArrayList) {

                        ArrayList<NewPacksModel> arrFreebieSlabs = (ArrayList<NewPacksModel>) results;
                        arrFreebieSlabData = new ArrayList<>();
                        if (arrFreebieSlabs != null && arrFreebieSlabs.size() > 0) {
                            for (int i = 0; i < arrFreebieSlabs.size(); i++) {
                                NewPacksModel newPacksModel = arrFreebieSlabs.get(i);
                                int freePackQty = 0, freebie = 0, freeQty = 0, freeSlabEsu = newPacksModel.getSlabEsu();
                                try {
                                    freebie = Integer.parseInt(freebieQty);
                                    freePackQty = Integer.parseInt(newPacksModel.getPackSize());
                                    freePackQty = freePackQty * freeSlabEsu;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (freebie >= freePackQty) {
                                    ProductSlabData productSlabData = new ProductSlabData();
                                    freeQty = freebie / freePackQty;
                                    productSlabData.setLevel(newPacksModel.getPackLevel());
                                    productSlabData.setEsu(newPacksModel.getSlabEsu());
                                    productSlabData.setQty(freeQty);
                                    productSlabData.setStar(newPacksModel.getStar());
                                    productSlabData.setProductId(freebieProductId);
                                    productSlabData.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                    arrFreebieSlabData.add(productSlabData);
                                    freebie = freebie % freePackQty;
                                    i++;
                                    continue;
                                }
                            }
                            cartModel.setArrFreebieSlabData(arrFreebieSlabData);
                        }

                        long rowId = Utils.insertOrUpdateCart(cartModel);
                        if (rowId == -1) {
                            showToast("Failed to insert in Cart");
                        } else if (rowId == -2) {
                            showToast("Failed to insert Freebie in Cart.");
                        } else if (rowId == -3) {
                            showToast("Failed to insert. Please try again");
                        }

                        dismiss();
                        onDialogClosed.dialogClose();
                        updateCartCount();

                    }

                } else if (requestType == PARSER_TYPE.ADD_CART_1) {
                    if (results instanceof AddCart1Response) {
                        AddCart1Response addCart1Response = (AddCart1Response) results;
                        if (addCart1Response.isStatus()) {
                            cartModel = new CartModel();
                            //check for freebie
                            if (freebieProductId != null && freebieQty != null && !freebieQty.equals("0") &&
                                    this.totalQty >= freebieMpq) {
                                cartModel.setFreebieQty(freebieQty);
                            }
                            cartModel.setProductId(slabProductId);
                            cartModel.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                            cartModel.setQuantity(totalQty);
                            cartModel.setUnitPrice(unitPriceToApply);
                            cartModel.setMargin(marginToApply);
                            cartModel.setTotalPrice(totalPrice);
                            cartModel.setWarehouseId("");//todo
                            cartModel.setDiscount("");//todo
                            cartModel.setPackType(productModel.getPackType());
                            cartModel.setIsChild(productModel.isChild() == true ? 1 : 0);
                            cartModel.setIsSlab(selPackIsSlab == true ? 1 : 0);
                            cartModel.setBlockedQty(selPackBlockedQty);
                            cartModel.setStar(selectedProductModel.getStar());
                            cartModel.setPrmtDetId(selPackPromotionId);
                            cartModel.setProductSlabId(selProductSlabId);
                            cartModel.setPackStar(selectedPackModel.getStar());

                            ArrayList<ProductSlabData> arrProductSlabData = new ArrayList<>();
                            int qty = 0, totalQty = this.totalQty;
                            double cashBackAmount = 0;
                            String retailerCbIds = "", ffManagerCbIds = "", ffAssCbIds = "";
                            for (int i = arrSlabRates.size() - 1; i >= 0; i--) {
                                NewPacksModel newPacksModel = arrSlabRates.get(i);
                                int packQty = 0, slabEsu = newPacksModel.getSlabEsu();
                                try {
                                    packQty = Integer.parseInt(newPacksModel.getPackSize());
                                    packQty = packQty * slabEsu;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (totalQty >= packQty) {
                                    ProductSlabData productSlabData = new ProductSlabData();
                                    qty = totalQty / packQty;
                                    if (selectedPackModel.getPackLevel() == newPacksModel.getPackLevel()) {

                                        ArrayList<String> arrCashBackIds = new ArrayList<>();

                                        if (newPacksModel.getArrCashBackDetails() != null && newPacksModel.getArrCashBackDetails().size() > 0) {
                                            for (int j = 0; j < newPacksModel.getArrCashBackDetails().size(); j++) {
                                                CashBackDetailsModel cashBackDetailsModel = newPacksModel.getArrCashBackDetails().get(j);
                                                double torange = cashBackDetailsModel.getFf_qty_to_range();
                                                if (totalQty >= torange) {
                                                    if (cashBackDetailsModel.getCbk_source_type() == ConstantValues.PRODUCT) {

                                                        if (!isFF) {
                                                            if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER && cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                                retailerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                            }

                                                        } else {
                                                            if (cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                                if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER) {

                                                                    retailerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                                } else if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.FF_MANAGER) {
                                                                    ffManagerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                                } else if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.FF_ASSOCIATE) {
                                                                    ffAssCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                                }
                                                            }

                                                        }

                                                        if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER && cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                            double price = 0;
                                                            try {
                                                                price = Double.parseDouble(newPacksModel.getUnitPrice());
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }

                                                            if (cashBackDetailsModel.getCashback_type() == ConstantValues.CASHBACK_TYPE_VALUE) {
                                                                cashBackAmount = cashBackDetailsModel.getCbk_value();
                                                            } else {
                                                                cashBackAmount = (totalQty * price * cashBackDetailsModel.getCbk_value()) / 100;
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (!TextUtils.isEmpty(retailerCbIds))
                                            arrCashBackIds.add(retailerCbIds);
                                        if (!TextUtils.isEmpty(ffAssCbIds))
                                            arrCashBackIds.add(ffAssCbIds);
                                        if (!TextUtils.isEmpty(ffManagerCbIds))
                                            arrCashBackIds.add(ffManagerCbIds);

                                        String comma_cashBackIds = TextUtils.join(",", arrCashBackIds);
                                        productSlabData.setCashBackIds(comma_cashBackIds);

                                    }
                                    productSlabData.setLevel(newPacksModel.getPackLevel());
                                    productSlabData.setEsu(newPacksModel.getSlabEsu());
                                    productSlabData.setQty(qty);
                                    productSlabData.setPackQty(qty * packQty);
                                    productSlabData.setPackSize(packQty / newPacksModel.getSlabEsu());
                                    productSlabData.setStar(newPacksModel.getStar());
                                    productSlabData.setProductId(productId);
                                    productSlabData.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                    arrProductSlabData.add(productSlabData);
                                    totalQty = totalQty % packQty;  // need to cal using another variable
                                    i++;
                                    continue;
                                }
                            }

                            cartModel.setCashbackAmount(cashBackAmount);
                            cartModel.setArrProductSlabData(arrProductSlabData); // set slabs data for product
                            int _productEsu = selectedPackModel.getSlabEsu();
                            int esu = 1, packQty = 0;
                            if (_productEsu != 0) {
                                try {
                                    packQty = Integer.parseInt(selectedPackModel.getPackSize());
                                    esu = this.totalQty / (_productEsu * packQty);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    esu = 1;
                                }
                            }
                            cartModel.setEsu(String.valueOf(esu));

                            /*String _productEsu = selectedProductModel.getEsu();
                            int esu = 1;
                            if (_productEsu != null && !_productEsu.equals("0") && !TextUtils.isEmpty(_productEsu)) {
                                try {
                                    int tempEsu = Integer.parseInt(_productEsu);
                                    esu = totalQty / tempEsu;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    esu = 1;
                                }
                            }
                            cartModel.setEsu(String.valueOf(esu));*/

                            cartModel.setSlabEsu(selectedProductModel.getEsu());
                            cartModel.setParentId(slabProductId);
                            cartModel.setFreebieProductId(freebieProductId);

                            TestProductModel freebieModel = DBHelper.getInstance().getFreebieData(freebieProductId);
                            if (freebieModel != null) {
                                cartModel.setFreebieStar(freebieModel.getStar().isEmpty() ? selectedProductModel.getStar() : freebieModel.getStar());
                                cartModel.setFreebieEsu(freebieModel.getEsu().isEmpty() ? selectedProductModel.getEsu() : freebieModel.getEsu());
                            }
                            cartModel.setFreebieMpq(selectedPackModel.getFreebieMpq());
                            cartModel.setFreebieFq(selectedPackModel.getFreebieQty());

                            /*if (freebieProductId != null && !freebieProductId.equals("0") && !freebieQty.equals("0") && this.totalQty >= freebieMpq) {
                                getFreebiePacks(freebieProductId);
                            } else {*/

                            long rowId = Utils.insertOrUpdateCart(cartModel);
                            if (rowId == -1) {
                                showToast("Failed to insert in Cart");
                            } else if (rowId == -2) {
                                showToast("Failed to insert Freebie in Cart.");
                            } else if (rowId == -3) {
                                showToast("Failed to insert. Please try again");
                            }

                            dismiss();
                            onDialogClosed.dialogClose();
                            updateCartCount();

//                            }
                        } else {
                            Utils.showAlertWithMessage(getActivity(), String.format(getString(R.string.unavailable_inventory_product), addCart1Response.getAvailableQty()));
                        }
                    } else {

                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
    }

    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    private LinearLayout createSlabRates(final String text, String slabColor, int slabEsu, int id) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        LinearLayout skuView = new LinearLayout(getActivity());
        skuView.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params1.setMargins(0, 0, 20, 0);
        skuView.setLayoutParams(params1);

        ImageView imageView = new ImageView(getActivity());
        Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_star);

        Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
        Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(slabColor));
        imageView.setImageBitmap(mFinalBitmap);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params2.gravity = Gravity.CENTER_VERTICAL;
        params2.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(params2);

        TextView textView = new TextView(getActivity());
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.gravity = Gravity.CENTER_VERTICAL;
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        params.setMargins(10, 0, 5, 0);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        skuView.setId(id);
        skuView.setTag(text);
        if (slabEsu > 1)
            textView.setText(text == null ? "" : text + " x " + slabEsu);
        else
            textView.setText(text == null ? "" : text);

        skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
        skuView.setPadding(10, 5, 10, 5);
        skuView.addView(imageView);
        skuView.addView(textView);

        return skuView;
    }

    public interface OnDialogClosed {
        public void dialogClose();
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}