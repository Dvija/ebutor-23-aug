package com.ebutor.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.FFListAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.FilterDataModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class FilterFFFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, FFListAdapter.onClickListener {

    private SharedPreferences mSharedPreferences;
    private RadioButton rbOrg, rbMyDashboard, rbMyTeam;
    private RecyclerView rvFFList;
    private Dialog dialog;
    private ArrayList<FilterDataModel> arrFFs;
    private ItemsAdapter mAdapter;
    private FFListAdapter ffListAdapter;
    private boolean hasChilds = false, isFromSO = false;
    private int type = DashBoardTypes.ORGANIZATION;
    private String pos = "", strStartDate = "", strEndDate = "";
    private Date startDate, endDate, prevStartDate, prevEndDate;
    private EditText etStartDate, etEndDate;
    private RadioGroup radioGroup;
    private Button btnSubmit;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy");
    private int selStartYear = 0, selStartMonth = 0, selStartDay = 0, selEndYear = 0, selEndMonth = 0, selEndDay = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filter_ff, container, false);
        if (getArguments().containsKey("type"))
            type = getArguments().getInt("type");
        if (getArguments().containsKey("pos"))
            pos = getArguments().getString("pos");
        if (getArguments().containsKey("isFromSO"))
            isFromSO = getArguments().getBoolean("isFromSO", false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dialog = new ProgressDialog(getActivity());
        hasChilds = mSharedPreferences.getBoolean(ConstantValues.KEY_HAS_CHILD, false);

        rbOrg = (RadioButton) view.findViewById(R.id.rb_org);
        rbMyDashboard = (RadioButton) view.findViewById(R.id.rb_my_dashboard);
        rbMyTeam = (RadioButton) view.findViewById(R.id.rb_my_team);
        rvFFList = (RecyclerView) view.findViewById(R.id.rv_ff_list);
        etStartDate = (EditText) view.findViewById(R.id.et_start_date);
        etEndDate = (EditText) view.findViewById(R.id.et_end_date);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        btnSubmit = (Button) view.findViewById(R.id.btn_submit);
        if (hasChilds) {
            rbMyTeam.setVisibility(View.VISIBLE);
        } else {
            rbMyTeam.setVisibility(View.GONE);
        }

        if (isFromSO) {
            btnSubmit.setVisibility(View.VISIBLE);
            radioGroup.setVisibility(View.GONE);
        } else {
            btnSubmit.setVisibility(View.GONE);
            radioGroup.setVisibility(View.VISIBLE);
        }

        int year, month, day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        strStartDate = day + " " + (month + 1) + " " + year;
        strEndDate = day + " " + (month + 1) + " " + year;

        etStartDate.setVisibility(View.VISIBLE);
        etEndDate.setVisibility(View.VISIBLE);
        etStartDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, day + " " + (month + 1) + " " + year));
        etEndDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, day + " " + (month + 1) + " " + year));
        try {
            startDate = sdf.parse(day + " " + (month + 1) + " " + year);
            endDate = sdf.parse(day + " " + (month + 1) + " " + year);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (getArguments().containsKey("start_date")) {
            strStartDate = getArguments().getString("start_date");
            etStartDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, strStartDate));
            try {
                startDate = sdf.parse(strStartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (getArguments().containsKey("end_date")) {
            strEndDate = getArguments().getString("end_date");
            etEndDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, strEndDate));
            try {
                endDate = sdf.parse(strEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        switch (type) {
            case DashBoardTypes.ORGANIZATION:
                rbOrg.setChecked(true);
                break;
            case DashBoardTypes.MY_DASHBOARD:
                rbMyDashboard.setChecked(true);
                break;
            case 3:
                rbMyTeam.setChecked(true);
                getFieldForceList();
                break;
            default:
                rbOrg.setChecked(true);
                break;
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getActivity().getIntent();
                intent.putExtra("type", DashBoardTypes.ORGANIZATION);
                intent.putExtra("start_date", strStartDate);
                intent.putExtra("end_date", strEndDate);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

        rbMyTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFieldForceList();
            }
        });

        rbOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvFFList.setVisibility(View.GONE);
                Intent intent = getActivity().getIntent();
                intent.putExtra("type", DashBoardTypes.ORGANIZATION);
                intent.putExtra("start_date", strStartDate);
                intent.putExtra("end_date", strEndDate);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

        rbMyDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvFFList.setVisibility(View.GONE);
                Intent intent = getActivity().getIntent();
                intent.putExtra("type", DashBoardTypes.MY_DASHBOARD);
                intent.putExtra("start_date", strStartDate);
                intent.putExtra("end_date", strEndDate);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int year, month, day;
                final Calendar c = Calendar.getInstance();
                if (selStartYear == 0 || selStartMonth == 0 || selStartDay == 0) {
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    year = selStartYear;
                    month = selStartMonth;
                    day = selStartDay;
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        try {
                            prevStartDate = startDate;
                            startDate = sdf.parse(dayOfMonth + " " + (monthOfYear + 1) + " " + year);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (endDate != null && ((startDate.compareTo(endDate) < 0) || startDate.compareTo(endDate) == 0)) {
                            selStartYear = year;
                            selStartMonth = monthOfYear;
                            selStartDay = dayOfMonth;

                            strStartDate = dayOfMonth + " " + (monthOfYear + 1) + " " + year;
                            etStartDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, dayOfMonth + " " + (monthOfYear + 1) + " " + year));
                        } else {
                            startDate = prevStartDate;
                            Toast.makeText(getActivity(), getResources().getString(R.string.please_select_enddate_after_startdate), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, year, month, day);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 21);
                calendar.set(Calendar.MINUTE, 59);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int year, month, day;
                final Calendar c = Calendar.getInstance();
                if (selEndYear == 0 || selEndMonth == 0 || selEndDay == 0) {
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    year = selEndYear;
                    month = selEndMonth;
                    day = selEndDay;
                }
                if (null != strStartDate && !TextUtils.isEmpty(strEndDate)) {

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                            try {
                                prevEndDate = endDate;
                                endDate = sdf.parse(dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if (startDate.compareTo(endDate) < 0 || startDate.compareTo(endDate) == 0) {
                                selEndYear = year;
                                selEndMonth = monthOfYear;
                                selEndDay = dayOfMonth;

                                strEndDate = dayOfMonth + " " + (monthOfYear + 1) + " " + year;
                                etEndDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, dayOfMonth + " " + (monthOfYear + 1) + " " + year));
                            } else {
                                endDate = prevEndDate;
                                Toast.makeText(getActivity(), getResources().getString(R.string.please_select_enddate_after_startdate), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, year, month, day);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY, 21);
                    calendar.set(Calendar.MINUTE, 59);
                    datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                    datePickerDialog.show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.please_select_start_date), Toast.LENGTH_SHORT).show();
                }
            }
        });


//        rvFFList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                try {
//                    for (int i = 0; i < parent.getChildCount(); i++) {
//                        View childView = lvFFList.getChildAt(i);
//                        ImageView ivTick = (ImageView) childView.findViewById(R.id.iv_tick);
//                        ivTick.setVisibility(View.INVISIBLE);
//                    }
//                    View childView = lvFFList.getChildAt(position);
//                    ImageView ivTick = (ImageView) childView.findViewById(R.id.iv_tick);
//                    ivTick.setVisibility(View.VISIBLE);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                Intent intent = getActivity().getIntent();
//                intent.putExtra("type", DashBoardTypes.MY_TEAM_DASHBOARD);
//                intent.putExtra("start_date", strStartDate);
//                intent.putExtra("end_date", strEndDate);
//                intent.putExtra("pos", String.valueOf(position));
//                if (arrFFs != null && arrFFs.size() > 0)
//                    intent.putExtra("user_id", arrFFs.get(position).getId());
//                getActivity().setResult(Activity.RESULT_OK, intent);
//                getActivity().finish();
//            }
//        });

    }

    private void getFieldForceList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                JSONObject dataObject = new JSONObject();
                HashMap<String, String> map = new HashMap<>();
                dataObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                dataObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                dataObject.put("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_FF_LEGAL_ENTITY_ID, ""));
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getfieldforcelistURL, map, FilterFFFragment.this, FilterFFFragment.this, PARSER_TYPE.FF_LIST);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getfieldforcelistURL);
                if (dialog != null)
                    dialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.FF_LIST) {
                        if (response instanceof ArrayList) {
                            arrFFs = (ArrayList<FilterDataModel>) response;
                            if (arrFFs != null && arrFFs.size() > 0) {
                                rvFFList.setVisibility(View.VISIBLE);
                                rvFFList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                ffListAdapter = new FFListAdapter(getActivity(), arrFFs);
                                ffListAdapter.setListener(FilterFFFragment.this);
                                rvFFList.setAdapter(ffListAdapter);
                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onFFChecked(int position, Object object, String key, boolean isChecked) {
        try {
            Intent intent = getActivity().getIntent();
            intent.putExtra("type", DashBoardTypes.MY_TEAM_DASHBOARD);
            intent.putExtra("start_date", strStartDate);
            intent.putExtra("end_date", strEndDate);
            intent.putExtra("pos", String.valueOf(position));
            if (arrFFs != null && arrFFs.size() > 0)
                intent.putExtra("user_id", arrFFs.get(position).getId());
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ItemsAdapter extends BaseAdapter {
        ArrayList<FilterDataModel> items;
        String selected = "";
        String childPos = "";

        public ItemsAdapter(ArrayList<FilterDataModel> item, String childPos) {
            this.items = item;
            this.childPos = childPos;
        }

        // @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_filter_dialog, null);
            }
            TextView tvSort = (TextView) v.findViewById(R.id.tv_sort_name);
            ImageView imageView = (ImageView) v.findViewById(R.id.iv_tick);
            tvSort.setText(items.get(position).getValue());
            if (pos != null && !TextUtils.isEmpty(pos)) {
                try {
                    int checkPos = Integer.parseInt(pos);
                    if (checkPos == position)
                        imageView.setVisibility(View.VISIBLE);
                    else
                        imageView.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            v.setTag(position);
            return v;
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }
}
