package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NeededInsRequest;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.OrderDetailAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.models.OrderDetailModel;
import com.ebutor.models.OrdersModel;
import com.ebutor.models.ReasonsModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class OrderDetailFragment extends Fragment implements CancelRequestFragment.OnDialogClosed, ReturnRequestFragment.OnDialogClosed, VolleyHandler<Object>, Response.ErrorListener {

    private Button btnAction;
    private boolean isCancelOrderStatus = false, isrequestOrderStatus = false;
    private TextView tvOrderNumber, tvOrderDate;
    private ActionSlideExpandableListView actionSlideExpandableListView;
    private OrderDetailAdapter orderDetailAdapter;
    private ArrayList<OrdersModel> arrOrders;
    private SharedPreferences mSharedPreferences;
    private ArrayList<ReasonsModel> reasonArray;
    private String variantId = "", productId = "", orderId = "", status, requestType = "";
    private TextView tvNumberOfItems, tvTotalAmount, tvAddress, tvAlertMsg, tvDiscountAmount, tvGrandTotal;
    private RelativeLayout rlAlert;
    private Tracker mTracker;
    private Dialog dialog;
    private LinearLayout llMain, llDiscount, llGrandTotal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_details, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        tvOrderNumber = (TextView) view.findViewById(R.id.tvOrderNumber);
        tvNumberOfItems = (TextView) view.findViewById(R.id.tvNumberOfItems);
        tvTotalAmount = (TextView) view.findViewById(R.id.tvTotalAmount);
        tvDiscountAmount = (TextView) view.findViewById(R.id.tv_discount_amount);
        tvGrandTotal = (TextView) view.findViewById(R.id.tv_grand_total);
        tvAddress = (TextView) view.findViewById(R.id.tv_address);
        tvOrderDate = (TextView) view.findViewById(R.id.tv_order_date);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        llDiscount = (LinearLayout) view.findViewById(R.id.ll_discount);
        llGrandTotal = (LinearLayout) view.findViewById(R.id.ll_grand_total);
        actionSlideExpandableListView = (ActionSlideExpandableListView) view.findViewById(R.id.list);

        arrOrders = new ArrayList<>();
        btnAction = (Button) view.findViewById(R.id.btn_order_cancel);

        MyApplication application = (MyApplication) getActivity().getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        final Bundle args = getArguments();
        if (args != null && args.getSerializable("orderId") != null) {
            orderId = args.getString("orderId");
        }
        if (args != null && args.getSerializable("status") != null) {
            status = args.getString("status");
        }

        if (status != null){
            switch (status) {
                case "OPEN ORDER":
                case "PICKLIST GENERATED":
                    if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.CANCEL_ORDER_FEATURE_CODE) >= 0) {
                        btnAction.setText(getActivity().getResources().getString(R.string.cancel));
                        btnAction.setVisibility(View.VISIBLE);
                    }
                    break;
                case "SHIPPED":
                case "DELIVERED":
                case "INVOICED":
                    if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.DOWNLOAD_INVOICE_FEATURE_CODE) >= 0) {
                        btnAction.setText(getActivity().getResources().getString(R.string.download_invoice));
                        btnAction.setVisibility(View.VISIBLE);
                    }
                    break;
                case "CANCELLED BY EBUTOR":
                case "CANCELLED BY CUSTOMER":
                case "RETURN INITIATED":
                case "RETURNED":
                case "REFUND INITIATED":
                case "REFUNDED":
                case "CLOSED":
                case "READY TO DISPATCH":
                case "partially complete":
                case "partially shipped":
                case "partially cancel":
                case "partially processing":
                    btnAction.setText("");
                    btnAction.setVisibility(View.GONE);
                    break;
                default:
                    btnAction.setText("");
                    btnAction.setVisibility(View.GONE);
                    break;

            }
        }

        btnAction.setTextColor(getResources().getColor(R.color.white));

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnAction.getText().equals(getActivity().getResources().getString(R.string.retrn))) {
                    isrequestOrderStatus = true;
                    getReturnReasonsList();
                } else if (btnAction.getText().equals(getActivity().getResources().getString(R.string.cancel))) {
                    isCancelOrderStatus = true;
                    getCancelReasonsList();
                } else if (btnAction.getText().equals(getActivity().getResources().getString(R.string.download_invoice))) {
                    generateInvoice();
                }
            }
        });

        orderDetailAdapter = new OrderDetailAdapter(getActivity(), arrOrders);
        actionSlideExpandableListView.setAdapter(orderDetailAdapter);
        Utils.setListViewHeightBasedOnItems(actionSlideExpandableListView);

        actionSlideExpandableListView.setItemActionListener(new ActionSlideExpandableListView.OnActionClickListener() {
            @Override
            public void onClick(View itemView, View clickedView, int position) {
                variantId = arrOrders.get(position).getVariantId();
                productId = arrOrders.get(position).getProductId();

                switch (arrOrders.get(position).getOrderStatus()) {
                    case "OPEN ORDER":
                    case "CONFIRMED":
                    case "PICKED":
                    case "PACKED":
                    case "READY TO DISPATCH":
                    case "PROCESSING":
                        isCancelOrderStatus = false;
                        getCancelReasonsList();
                        break;
                    default:
                        isCancelOrderStatus = false;
                        getCancelReasonsList();
                        break;
                }

//                if (clickedView.getId() == R.id.btn_cancel) {
//                    isCancelOrderStatus = false;
//                    getCancelReasonsList();
//                } else if (clickedView.getId() == R.id.btn_return) {
//                    isrequestOrderStatus = false;
//                    getReturnReasonsList();
//                } else {
//                    isCancelOrderStatus = false;
//                    getCancelReasonsList();
//                }

            }
        }, R.id.btn_cancel);

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestType) {
                        case "OrderDetail":
                            getOrderDetail(orderId);
                            break;
                        case "ReturnReasons":
                            getReturnReasonsList();
                            break;
                        case "ReasonsList":
                            getCancelReasonsList();
                            break;
                        default:
                            getOrderDetail(orderId);
                            break;

                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        getOrderDetail(orderId);

    }

    private void generateInvoice() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "Generate Invoice";
            String url = AppURL.generateInvoiceURL + "/" + orderId;
            NeededInsRequest highMarginRequest = new NeededInsRequest(Request.Method.GET, url,
                    new Response.Listener<byte[]>() {
                        @Override
                        public void onResponse(byte[] response) {
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                            File file = new File(Environment.getExternalStorageDirectory() + "/Invoice.pdf");
                            try {
                                FileOutputStream fileOuputStream =
                                        new FileOutputStream(file);
                                fileOuputStream.write(response);
                                fileOuputStream.close();
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.file_saved_device), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_saving_file), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                                if (error != null) {
                                    if (error instanceof NetworkError) {
                                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                                    } else if (error instanceof ServerError) {
                                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                                    } else if (error instanceof AuthFailureError) {
                                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                                    } else if (error instanceof NoConnectionError) {
                                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                                    } else if (error instanceof TimeoutError) {
                                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                                    } else {
                                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                                    }
                                } else {
                                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                                }
                            } catch (Exception e) {
                            }
//                            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//                            error.printStackTrace();
                        }
                    }
            );
            MyApplication.getInstance().addToRequestQueue(highMarginRequest, url);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "Generate Invoice";
        }

    }

    private void getOrderDetail(String orderId) {

        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "OrderDetail";
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                detailsObj.put("orderID", orderId);
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    detailsObj.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    detailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }

                map.put("data", detailsObj.toString());
            } catch (Exception e) {

            }

            VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.orderDetailsURL, map, OrderDetailFragment.this, OrderDetailFragment.this, PARSER_TYPE.ORDER_DETAIL);
            ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.orderDetailsURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "OrderDetail";
        }
    }

    private void getReturnReasonsList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "ReturnReasons";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask returnReasonsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.returnReasonsURL, map, OrderDetailFragment.this, OrderDetailFragment.this, PARSER_TYPE.RETURN_REASONS);
                returnReasonsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyApplication.getInstance().addToRequestQueue(returnReasonsReq, AppURL.returnReasonsURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "ReturnReasons";
        }
    }

    private void getCancelReasonsList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "ReasonsList";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask cancelReasonsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.cancelReasonsURL, map, OrderDetailFragment.this, OrderDetailFragment.this, PARSER_TYPE.CANCEL_REASONS);
                cancelReasonsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyApplication.getInstance().addToRequestQueue(cancelReasonsReq, AppURL.cancelReasonsURL);
                if (dialog != null)
                    dialog.show();


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "ReasonsList";
        }
    }

    @Override
    public void dialogClose(String orderId) {
        if (orderId.equalsIgnoreCase(getActivity().getResources().getString(R.string.no_network))) {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        } else {
            getOrderDetail(orderId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Order Details Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {

                if (requestType == PARSER_TYPE.ORDER_DETAIL) {
                    if (response instanceof OrderDetailModel) {
                        OrderDetailModel orderDetailModel = (OrderDetailModel) response;
                        arrOrders = new ArrayList<>();
                        String orderId = orderDetailModel.getOrderId();
                        String orderDate = orderDetailModel.getOrderDate();
                        String totalStatus = orderDetailModel.getTotalStatus();

                        String shippingFirstName = orderDetailModel.getShippingFirstName();
                        String shippingLastName = orderDetailModel.getShippingLastName();
                        String shippingEmail = orderDetailModel.getShippingEmail();
                        String shippingTelephone = orderDetailModel.getShippingTelephone();
                        String shippingAddress = orderDetailModel.getShippingAddress();
                        String shippingCity = orderDetailModel.getShippingCity();
                        String shippingPin = orderDetailModel.getShippingPin();
                        String shippingCountry = orderDetailModel.getShippingCountry();

                        tvAddress.setText(String.format(getResources().getString(R.string.userAddress1), shippingFirstName,
                                shippingLastName, shippingAddress, shippingCity, shippingCountry, shippingPin, shippingTelephone));

                        String total = orderDetailModel.getSubTotal();
                        double discountAmount = orderDetailModel.getDiscountAmount();
                        String grand = orderDetailModel.getGrandTotal();
                        double totalAmount = 0.0, grandTotal = 0.0;
                        try {
                            totalAmount = Double.parseDouble(total);
                            grandTotal = Double.parseDouble(grand);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        tvTotalAmount.setText(String.format(getResources().getString(R.string.rupee)) + " " + String.format(Locale.CANADA, "%.2f", totalAmount));

                        /*if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                            llDiscount.setVisibility(View.GONE);
                            llGrandTotal.setVisibility(View.GONE);
                        } else {*/
                            llDiscount.setVisibility(View.VISIBLE);
                            llGrandTotal.setVisibility(View.VISIBLE);
//                        }

                        tvDiscountAmount.setText(String.format(getResources().getString(R.string.rupee)) + " " + String.format(Locale.CANADA, "%.2f", discountAmount));
                        tvGrandTotal.setText(String.format(getResources().getString(R.string.rupee)) + " " + String.format(Locale.CANADA, "%.2f", grandTotal));

                        String orderCode = orderDetailModel.getOrderCode();

                        tvOrderNumber.setText(orderCode == null ? orderId : orderCode);

                        tvOrderDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_TIME_PATTERN, orderDate));
                        arrOrders = orderDetailModel.getArrOrders();
//                        totalStatus = "SHIPPED";// temp
                        switch (totalStatus) {
                            case "OPEN ORDER":
                            case "PICKLIST GENERATED":
                                if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.CANCEL_ORDER_FEATURE_CODE) >= 0) {
                                    btnAction.setText(getActivity().getResources().getString(R.string.cancel));
                                    btnAction.setVisibility(View.VISIBLE);
                                }
                                break;
                            case "SHIPPED":
                            case "INVOICED":
                            case "DELIVERED":
                                if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.DOWNLOAD_INVOICE_FEATURE_CODE) >= 0) {
                                    btnAction.setText(getActivity().getResources().getString(R.string.download_invoice));
                                    btnAction.setVisibility(View.VISIBLE);
                                }
                                break;
                            case "CANCELLED BY EBUTOR":
                            case "CANCELLED BY CUSTOMER":
                            case "RETURN INITIATED":
                            case "RETURNED":
                            case "REFUND INITIATED":
                            case "REFUNDED":
                            case "CLOSED":
                            case "READY TO DISPATCH":
                            case "partially complete":
                            case "partially shipped":
                            case "partially cancel":
                            case "partially processing":
                                btnAction.setText("");
                                btnAction.setVisibility(View.GONE);
                                break;
                            default:
                                btnAction.setText("");
                                btnAction.setVisibility(View.GONE);
                                break;

                        }

                        if (arrOrders != null && arrOrders.size() > 0) {
                            orderDetailAdapter = new OrderDetailAdapter(getActivity(), arrOrders);
                            actionSlideExpandableListView.setAdapter(orderDetailAdapter);
                            Utils.setListViewHeightBasedOnItems(actionSlideExpandableListView);
                            tvNumberOfItems.setText(String.valueOf(arrOrders.size()));
                        } else {
                            tvNumberOfItems.setText("0");
                        }

                    }
//                            orderDetailAdapter.notifyDataSetChanged();
                } else if (requestType == PARSER_TYPE.RETURN_REASONS) {
                    if (response instanceof ArrayList) {
                        reasonArray = (ArrayList<ReasonsModel>) response;

                        ReturnRequestFragment fragment = ReturnRequestFragment.newInstance(reasonArray, productId, variantId, orderId, isrequestOrderStatus);
                        fragment.setClickListener(OrderDetailFragment.this);
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        fragment.show(ft, "Return Request");
                    }
                } else if (requestType == PARSER_TYPE.CANCEL_REASONS) {
                    if (response instanceof ArrayList) {
                        reasonArray = (ArrayList<ReasonsModel>) response;

                        CancelRequestFragment fragment = CancelRequestFragment.newInstance(reasonArray, productId, variantId, orderId, isCancelOrderStatus);
                        fragment.setClickListener(OrderDetailFragment.this);
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        fragment.show(ft, "Cancel Request");

                    }
                } else if (requestType == PARSER_TYPE.GENERATE_INVOICE) {

                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null)
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}
