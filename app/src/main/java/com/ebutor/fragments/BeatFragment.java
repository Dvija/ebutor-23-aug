package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.backgroundtask.JSONParser;
import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;

public class BeatFragment extends DialogFragment {

    private OnSelListener confirmListener;
    private ListView lvSort;
    private ItemsAdapter mAdapter;
    private ArrayList<CustomerTyepModel> arrBeats;

    public static BeatFragment newInstance(ArrayList<CustomerTyepModel> arrBeats) {
        BeatFragment frag = new BeatFragment();
        Bundle args = new Bundle();
        args.putSerializable("Beats", arrBeats);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getActivity().getResources().getString(R.string.select_beat));
        if (getArguments().containsKey("Beats")) {
            arrBeats = (ArrayList<CustomerTyepModel>) getArguments().getSerializable("Beats");
        }
        return inflater.inflate(R.layout.fragment_sort, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    public void setConfirmListener(OnSelListener listener) {
        this.confirmListener = listener;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        JSONParser parser = new JSONParser();

        lvSort = (ListView) view.findViewById(R.id.lv_sort);
        if (arrBeats != null && arrBeats.size() > 0) {
            mAdapter = new ItemsAdapter(arrBeats);
            lvSort.setAdapter(mAdapter);
        }

        lvSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                dismiss();
                Object obj = view.getTag();
                if (obj != null) {
                    CustomerTyepModel model = (CustomerTyepModel) obj;
                    if (confirmListener != null)
                        confirmListener.onSelected(model.getCustomerGrpId());
                }
            }
        });
    }

    public interface OnSelListener {
        void onSelected(String string);
    }

    private class ItemsAdapter extends BaseAdapter {
        ArrayList<CustomerTyepModel> items;
        String selected = "";

        public ItemsAdapter(ArrayList<CustomerTyepModel> item) {
            this.items = item;
        }

        // @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_sort_dialog, null);
            }
            TextView tvSort = (TextView) v.findViewById(R.id.tv_sort_name);
            tvSort.setText(items.get(position).getCustomerName());


            v.setTag(items.get(position));
            return v;
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }
}
