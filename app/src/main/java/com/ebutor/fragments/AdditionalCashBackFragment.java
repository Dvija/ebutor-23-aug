package com.ebutor.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.adapters.CashbackAdapter;
import com.ebutor.models.StarLevelCashBackDetailsModel;
import com.ebutor.models.TestProductModel;

import java.util.ArrayList;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdditionalCashBackFragment extends DialogFragment{


    double totalAmount=0,totalCashBackAmount=0;
    RecyclerView recyclerView;
    ArrayList<StarLevelCashBackDetailsModel> starLevelCashBackDetailsModels;
    TextView tvcalTotalamount,tvcal_cashbackamount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        theme = android.R.style.Theme_Holo_Dialog_NoActionBar;
        setStyle(style, theme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.cashback_popup, container, false);
        return v;
    }

    public  static  AdditionalCashBackFragment newInstance(ArrayList<StarLevelCashBackDetailsModel> starLevelCashBackDetailsModels)
    {

        AdditionalCashBackFragment f = new AdditionalCashBackFragment();
        Bundle args = new Bundle();
        args.putSerializable("arr",starLevelCashBackDetailsModels);
        f.setArguments(args);
        return f;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvcalTotalamount= (TextView) view.findViewById(R.id.cal_totalamount);
        tvcal_cashbackamount= (TextView) view.findViewById(R.id.cal_cashbackamount);

        Bundle args = getArguments();
        if (args != null && args.getSerializable("arr") != null) {
            starLevelCashBackDetailsModels= (ArrayList<StarLevelCashBackDetailsModel>) args.getSerializable("arr");
        }

        for(int i=0;i<starLevelCashBackDetailsModels.size();i++)
        {
           totalCashBackAmount +=starLevelCashBackDetailsModels.get(i).getStarCashbackAmount();
            totalAmount+= starLevelCashBackDetailsModels.get(i).getStarTotalAppliedAmount();

        }

        CashbackAdapter cashbackAdapter=new CashbackAdapter(getActivity(),starLevelCashBackDetailsModels);
        recyclerView= (RecyclerView) view.findViewById(R.id.rv_cashback);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(cashbackAdapter);
        cashbackAdapter.notifyDataSetChanged();


        tvcalTotalamount.setText(String.format(Locale.CANADA, "%.2f",totalAmount));
        tvcal_cashbackamount.setText(String.format(Locale.CANADA, "%.2f",totalCashBackAmount));


    }
}
