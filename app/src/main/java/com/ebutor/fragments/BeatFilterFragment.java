package com.ebutor.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

public class BeatFilterFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, MultiSelectFragment.OnClickListener {

    private OnSelListener confirmListener;
    private TextView tvCancel, tvClear;
    private Button btnApply;
    private EditText etBeat, etSpoke, etHub;
    private String /*selBeatIds = "",*/ dcId, hubIds, hubId, beatId, type = ConstantValues.DC_TYPE, selBeats = "", selBeatIds = "", selHubIds = "", selBeatNames = "", selHubNames = "", selSpokeIds = "", selSpokeNames = "";
    //    private ArrayList<String> arrSelectedBeats = new ArrayList<>();
    private ArrayList<CustomerTyepModel> arrSelectedBeatsData = new ArrayList<>();
    //    private EditText etSearch;
    private Spinner spDC;
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private ArrayList<CustomerTyepModel> arrDcs, arrHubs = new ArrayList<>(), arrBeats = new ArrayList<>(), arrBeatsTemp = new ArrayList<>(), arrSpokes = new ArrayList<>(), array;
    private String flagType = "";
    private boolean isFirst = true, showBeats = false;
    private String selBeatsTemp = "", selBeatIdsTemp = "", selHubIdsTemp = "", selBeatNamesTemp = "", selHubNamesTemp = "", selSpokeIdsTemp, selSpokeNamesTemp;
    private TextView tvBeat;

//    public static BeatFilterFragment newInstance(ArrayList<CustomerTyepModel> arrBeats, ArrayList<String> arrSelectedBeats) {
//        BeatFilterFragment frag = new BeatFilterFragment();
//        Bundle args = new Bundle();
//        args.putSerializable("Beats", arrBeats);
//        args.putSerializable("selBeatIds", arrSelectedBeats);
//        frag.setArguments(args);
//        return frag;
//    }

    public static ArrayList<String> filterNames(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerName());
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments().containsKey("FilterBeats")) {
            arrBeats = (ArrayList<CustomerTyepModel>) getArguments().getSerializable("Beats");
            if (arrBeats != null)
                arrBeatsTemp = (ArrayList<CustomerTyepModel>) arrBeats.clone();
        }
        if (getArguments().containsKey("showBeats"))
            showBeats = getArguments().getBoolean("showBeats", false);
//        if (getArguments().containsKey("SelectedBeats")) {
//            arrSelectedBeats = (ArrayList<String>) getArguments().getSerializable("selBeatIds");
//        }

        dcId = MyApplication.getInstance().getDcId();
        selHubIds = MyApplication.getInstance().getSelHubIds();
        selHubNames = MyApplication.getInstance().getSelHubNames();
        selBeatIds = MyApplication.getInstance().getSelBeatIds();
        selBeatNames = MyApplication.getInstance().getSelBeatNames();
        selBeats = MyApplication.getInstance().getSelBeats();
        selSpokeIds = MyApplication.getInstance().getSelSpokeIds();
        selSpokeNames = MyApplication.getInstance().getSelSpokeNames();
        selHubIdsTemp = selHubIds;
        selHubNamesTemp = selHubNames;
        selBeatIdsTemp = selBeatIds;
        selBeatNamesTemp = selBeatNames;
        selBeatsTemp = selBeats;
        selSpokeIdsTemp = selSpokeIds;
        selSpokeNamesTemp = selSpokeNames;

        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);
        return inflater.inflate(R.layout.fragment_beat_filter_new, container, false);
    }

    public void setConfirmListener(OnSelListener listener) {
        this.confirmListener = listener;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        if (selBeatIds != null && selBeatIds.length() > 0)
//            arrSelectedBeats = new ArrayList<String>(Arrays.asList(selBeatIds.split("\\s*,\\s*")));

        spDC = (Spinner) view.findViewById(R.id.sp_dc);
        btnApply = (Button) view.findViewById(R.id.btn_apply);
//        etSearch = (EditText) view.findViewById(R.id.et_search);
        tvCancel = (TextView) view.findViewById(R.id.tv_cancel);
        tvClear = (TextView) view.findViewById(R.id.tv_clear);
        etBeat = (EditText) view.findViewById(R.id.et_Beats);
        etSpoke = (EditText) view.findViewById(R.id.et_spoke);
        etHub = (EditText) view.findViewById(R.id.et_Hubs);
        tvBeat = (TextView) view.findViewById(R.id.tv_beat);

        if (!showBeats) {
            tvCancel.setVisibility(View.GONE);
            tvClear.setVisibility(View.GONE);
            etBeat.setVisibility(View.GONE);
            tvBeat.setVisibility(View.GONE);
        } else {
            tvCancel.setVisibility(View.VISIBLE);
            tvClear.setVisibility(View.VISIBLE);
            etBeat.setVisibility(View.VISIBLE);
            tvBeat.setVisibility(View.VISIBLE);
        }

        getAllData(ConstantValues.DC_TYPE, "");

        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getActivity().getIntent();

                MyApplication.getInstance().setSelBeats("");
                MyApplication.getInstance().setDcId("");
                String hubs = mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, "");
                String ids = "";
                ArrayList<String> array = new ArrayList<String>();
                if (!TextUtils.isEmpty(hubs))
                    array = new ArrayList<String>(Arrays.asList(hubs.split("\\s*,\\s*")));
                if (array != null && array.size() > 0)
                    ids = array.get(0);
                MyApplication.getInstance().setSelHubIds(ids);
                MyApplication.getInstance().setSelBeatIds("");
                MyApplication.getInstance().setSelBeatNames("");
                MyApplication.getInstance().setSelSpokeIds("");
//                MyApplication.getInstance().setSelHubNames("");
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        spDC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dcId = arrDcs.get(i).getCustomerGrpId();
                etHub.setText("");
                etBeat.setText("");
                etSpoke.setText("");
                selHubIdsTemp = "";
                selHubNamesTemp = "";
                etSpoke.setText("");
                selSpokeIdsTemp = "";
                selSpokeNamesTemp = "";
                arrSpokes = new ArrayList<CustomerTyepModel>();
                arrBeatsTemp = new ArrayList<CustomerTyepModel>();
                arrHubs = new ArrayList<CustomerTyepModel>();
                selBeatsTemp = "";
                selBeatIdsTemp = "";
                selBeatNamesTemp = "";
                if (i != 0) {
                    type = ConstantValues.HUB_TYPE;
                    getAllData(ConstantValues.HUB_TYPE, dcId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        etBeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (null != arrBeatsTemp && arrBeatsTemp.size() > 0) {
                    flagType = "Beat";
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    MultiSelectFragment multiSelectFragment = MultiSelectFragment.newInstance(flagType, arrBeatsTemp, selBeatIdsTemp);
                    multiSelectFragment.setClickListener(BeatFilterFragment.this);
                    multiSelectFragment.setCancelable(true);
                    multiSelectFragment.show(fm, "multi_select_fragment");
                }
            }
        });

        etSpoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != arrSpokes && arrSpokes.size() > 0) {
                    flagType = "Spoke";
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    MultiSelectFragment multiSelectFragment = MultiSelectFragment.newInstance(flagType, arrSpokes, selSpokeIdsTemp);
                    multiSelectFragment.setClickListener(BeatFilterFragment.this);
                    multiSelectFragment.setCancelable(true);
                    multiSelectFragment.show(fm, "multi_select_fragment");
                }
            }
        });

        etHub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arrHubs != null && arrHubs.size() > 0) {
                    flagType = "Hub";
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    MultiSelectFragment multiSelectFragment = MultiSelectFragment.newInstance(flagType, arrHubs, selHubIdsTemp);
                    multiSelectFragment.setClickListener(BeatFilterFragment.this);
                    multiSelectFragment.setCancelable(true);
                    multiSelectFragment.show(fm, "multi_select_fragment");
                }
            }
        });

//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                if (mAdapter != null) {
//                    String text = etSearch.getText().toString().toLowerCase(Locale.getDefault());
//                    mAdapter.getFilter().filter(text);
//                }
//            }
//        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                getActivity().finish();
//                if (confirmListener != null)
//                    confirmListener.onSelected(arrSelectedBeats, arrSelectedBeatsData);

                if (TextUtils.isEmpty(dcId)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_dc));
                    return;
                }
                if (TextUtils.isEmpty(selHubIdsTemp)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_hub));
                    return;
                }
                if (TextUtils.isEmpty(selSpokeIdsTemp)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_spoke));
                    return;
                }

                /*if (TextUtils.isEmpty(selBeats)) {
                    Utils.showAlertWithMessage(getActivity(), "Please select Beat");
                    return;
                }*/

                Intent intent = getActivity().getIntent();

                MyApplication.getInstance().setSelBeats(selBeatsTemp);
                MyApplication.getInstance().setDcId(dcId);
                MyApplication.getInstance().setSelHubIds(selHubIdsTemp);
                MyApplication.getInstance().setSelBeatIds(selBeatIdsTemp);
                MyApplication.getInstance().setSelBeatNames(selBeatNamesTemp);
                MyApplication.getInstance().setSelHubNames(selHubNamesTemp);
                MyApplication.getInstance().setSelSpokeIds(selSpokeIdsTemp);
                MyApplication.getInstance().setSelSpokeNames(selSpokeNamesTemp);

                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });

    }

    private void getAllData(String type, String id) {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("dc_type", type);
                jsonObject.put("request_id", id);
                if (type.equalsIgnoreCase(ConstantValues.BEAT_TYPE))
                    jsonObject.put("returnBeats", 1);
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_HUB_EMPTY, false))
                    jsonObject.put("return_all", "1");
                else
                    jsonObject.put("return_all", "0");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllDataURL, map, BeatFilterFragment.this, BeatFilterFragment.this, PARSER_TYPE.GET_ALL_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getAllDataURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.GET_ALL_DATA) {
                        if (response instanceof ArrayList) {
                            switch (type) {
                                case ConstantValues.DC_TYPE:
                                    arrDcs = (ArrayList<CustomerTyepModel>) response;

                                    CustomerTyepModel _CustomerTyepModel = new CustomerTyepModel();
                                    _CustomerTyepModel.setCustomerGrpId("");
                                    _CustomerTyepModel.setCustomerName("Please select");
                                    arrDcs.add(0, _CustomerTyepModel);

                                    ArrayAdapter<CustomerTyepModel> dcAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, arrDcs);
                                    spDC.setAdapter(dcAdapter);

                                    dcId = MyApplication.getInstance().getDcId();

                                    int pos = 0;
                                    for (int i = 0; i < arrDcs.size(); i++) {
                                        CustomerTyepModel customerTyepModel = arrDcs.get(i);
                                        if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(dcId)) {
                                            pos = i;
                                            break;
                                        }
                                    }
                                    spDC.setSelection(pos);


                                    break;
                                case ConstantValues.HUB_TYPE:
                                    arrHubs = (ArrayList<CustomerTyepModel>) response;

                                    hubIds = mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, "");
                                    String _dcId = MyApplication.getInstance().getDcId();
                                    if (_dcId.equals(dcId)) {
                                        selHubIdsTemp = MyApplication.getInstance().getSelHubIds();
                                        selHubNamesTemp = MyApplication.getInstance().getSelHubNames();
                                        selBeatNamesTemp = MyApplication.getInstance().getSelBeatNames();
                                        selSpokeNamesTemp = MyApplication.getInstance().getSelSpokeNames();
                                    }

//
//                                    if (selHubIds != null && selHubIds.length() > 0)
//                                        arrayList = new ArrayList<String>(Arrays.asList(selHubIds.split("\\s*,\\s*")));
//
//                                    Predicate<CustomerTyepModel> isCheckedData = new Predicate<CustomerTyepModel>() {
//                                        @Override
//                                        public boolean apply(CustomerTyepModel customerTyepModel) {
//                                            return customerTyepModel.getCustomerGrpId().equalsIgnoreCase(hubIds);
//                                        }
//                                    };
//
//                                    ArrayList<String> checkedFilters = filterNames(arrHubs, isCheckedData);

//                                    ArrayList<String> arrayListIds = new ArrayList<>(), arrayListNames = new ArrayList<>(), arrTempIds = new ArrayList<>(), arrTempNames = new ArrayList<>();
//                                    if (selHubIds != null && selHubIds.length() > 0)
//                                        arrayListIds = new ArrayList<String>(Arrays.asList(selHubIds.split("\\s*,\\s*")));
//                                    if (selHubNames != null && selHubNames.length() > 0)
//                                        arrayListNames = new ArrayList<String>(Arrays.asList(selHubNames.split("\\s*,\\s*")));
//
//                                    for (int i = 0; i < arrHubs.size(); i++) {
//
//                                        if (arrayListIds.contains(arrHubs.get(i).getCustomerGrpId())) {
//                                            arrTempIds.add(arrHubs.get(i).getBeatId());
//                                        }
//
//                                        if (arrayListNames.contains(arrHubs.get(i).getCustomerName())) {
//                                            arrTempNames.add(arrHubs.get(i).getCustomerName());
//                                        }
//                                    }
//
//                                    selHubIds = TextUtils.join(",", arrTempIds);
//                                    selHubNames = TextUtils.join(",", arrTempNames);

//                                    if (!TextUtils.isEmpty(selHubIds)) {
                                    type = ConstantValues.SPOKE_TYPE;
                                    getAllData(type, selHubIdsTemp);
                                    etHub.setText(selHubNamesTemp);
                                    etBeat.setText(selBeatNamesTemp);
                                    etSpoke.setText(selSpokeNamesTemp);
//                                    }

                                    break;

                                case ConstantValues.SPOKE_TYPE:
                                    arrSpokes = (ArrayList<CustomerTyepModel>) response;
                                    String _ids = MyApplication.getInstance().getSelSpokeIds();
                                    ArrayList<String> _arrayList = new ArrayList<>(Arrays.asList(_ids.split("\\s*,\\s*")));
                                    ArrayList<String> _arrayListIds = new ArrayList<>();
                                    ArrayList<String> _arrayListNames = new ArrayList<>();
                                    for (int i = 0; i < arrSpokes.size(); i++) {
                                        if (_arrayList.contains(arrSpokes.get(i).getCustomerGrpId())) {
                                            _arrayListIds.add(arrSpokes.get(i).getCustomerGrpId());
                                            _arrayListNames.add(arrSpokes.get(i).getCustomerName());
                                        }
                                    }
                                    selSpokeNamesTemp = TextUtils.join(",", _arrayListNames);
                                    selSpokeIdsTemp = TextUtils.join(",", _arrayListIds);

                                    type = ConstantValues.BEAT_TYPE;
                                    getAllData(type, selSpokeIdsTemp);
                                    etSpoke.setText(selSpokeNamesTemp);
                                    break;

                                case ConstantValues.BEAT_TYPE:
                                    arrBeats = (ArrayList<CustomerTyepModel>) response;
                                    arrBeatsTemp = (ArrayList<CustomerTyepModel>) arrBeats.clone();

//                                    String _hubIds = MyApplication.getInstance().getSelHubIds();
                                    String ids = MyApplication.getInstance().getSelBeatIds();
                                    ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(ids.split("\\s*,\\s*")));
                                    ArrayList<String> arrayListIds = new ArrayList<>();
                                    ArrayList<String> arrayListNames = new ArrayList<>();

                                    for (int i = 0; i < arrBeatsTemp.size(); i++) {
                                        if (arrayList.contains(arrBeatsTemp.get(i).getCustomerGrpId())) {
                                            arrayListIds.add(arrBeatsTemp.get(i).getCustomerGrpId());
                                            arrayListNames.add(arrBeatsTemp.get(i).getCustomerName());
                                        }
                                    }
                                    selBeatNamesTemp = TextUtils.join(",", arrayListNames);
                                    selBeatIdsTemp = TextUtils.join(",", arrayListIds);
                                    if (!TextUtils.isEmpty(selBeatNamesTemp) && !TextUtils.isEmpty(selBeatIdsTemp))
                                        selBeatsTemp = MyApplication.getInstance().getSelBeats();

                                    etBeat.setText(selBeatNamesTemp);
                                    break;
                                default:
                                    array = (ArrayList<CustomerTyepModel>) response;
                                    ArrayAdapter<CustomerTyepModel> adapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, arrDcs);
                                    spDC.setAdapter(adapter);
                                    break;
                            }
                        }
                    }
                } else if (requestType != PARSER_TYPE.GET_ALL_DATA) {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onClicked(String string, JSONArray idsArray, String ids) {
        if (flagType.equalsIgnoreCase("Beat")) {
            etBeat.setText(string);
            selBeatsTemp = idsArray.toString();
            selBeatIdsTemp = ids;
            selBeatNamesTemp = string;
        } else if (flagType.equalsIgnoreCase("Hub")) {
            if (ids instanceof String) {
                etBeat.setText("");
                etSpoke.setText("");
                selSpokeIdsTemp = "";
                selSpokeNamesTemp = "";
                arrSpokes = new ArrayList<CustomerTyepModel>();
                arrBeatsTemp = new ArrayList<CustomerTyepModel>();
                selBeatsTemp = "";
                selBeatIdsTemp = "";
                selBeatNamesTemp = "";

                etHub.setText(string);
                selHubIdsTemp = ids;
                selHubNamesTemp = string;
                type = ConstantValues.SPOKE_TYPE;
                if (!TextUtils.isEmpty(selHubIdsTemp))
                    getAllData(type, selHubIdsTemp);
            }
        } else if (flagType.equalsIgnoreCase("Spoke")) {
            if (ids instanceof String) {
                etBeat.setText("");
                arrBeatsTemp = new ArrayList<CustomerTyepModel>();
                selBeatsTemp = "";
                selBeatIdsTemp = "";
                selBeatNamesTemp = "";

                etSpoke.setText(string);
                selSpokeIdsTemp = ids;
                selSpokeNamesTemp = string;
                type = ConstantValues.BEAT_TYPE;
                if (!TextUtils.isEmpty(selSpokeIdsTemp))
                    getAllData(type, selSpokeIdsTemp);

            }
        }
    }

    public interface OnSelListener {
        void onSelected(ArrayList<String> string, ArrayList<CustomerTyepModel> data);
    }
}
