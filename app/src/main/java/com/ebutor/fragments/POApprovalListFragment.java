package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.POApprovalDetailsActivity;
import com.ebutor.adapters.POApprovalsListAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.OrdersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class POApprovalListFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, POApprovalsListAdapter.OnItemClickListener {

    private SharedPreferences mSharedPreferences;
    private ArrayList<OrdersModel> poList = new ArrayList<>();
    private POApprovalsListAdapter adapter;
    private RecyclerView rvPOList;
    private TextView tvNoPOs, tvAlertMsg;
    private RelativeLayout rlAlert;
    private LinearLayout llMain, llFooter, llHeader;
    private Tracker mTracker;
    private Dialog dialog;
    private String code = "";
    private int currentOffset = 0;
    private int totalItemsCount = 0;
    private int offsetLimit = 1;
    private boolean isLoadMore = false, isRequestInProgress = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey("code"))
            code = getArguments().getString("code");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_approval_list, container, false);

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        rvPOList = (RecyclerView) v.findViewById(R.id.rvOrdersList);
        tvNoPOs = (TextView) v.findViewById(R.id.tvNoOrders);
        tvAlertMsg = (TextView) v.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) v.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) v.findViewById(R.id.ll_main);
        llFooter = (LinearLayout) v.findViewById(R.id.ll_footer);
        llHeader = (LinearLayout) v.findViewById(R.id.ll_header);

        adapter = new POApprovalsListAdapter(getActivity(), poList);
        adapter.setClickListener(POApprovalListFragment.this);
        rvPOList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPOList.setAdapter(adapter);

        rvPOList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int count = recyclerView.getLayoutManager().getItemCount();
                int pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                isLoadMore = true;

                if ((visibleItemCount + pastVisiblesItems) >= count && currentOffset * Utils.offsetLimit < totalItemsCount) {
                    if (Networking.isNetworkAvailable(getActivity())) {
                        llFooter.setVisibility(View.VISIBLE);
                        getPOApprovalList();
                    } else {
                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    getPOApprovalList();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });
        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        return v;
    }

    private void getPOApprovalList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("user_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                obj.put("status", code);
                obj.put("offset", currentOffset);
                obj.put("perpage", Utils.offsetLimit);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPolistByStatusURL, map, POApprovalListFragment.this, POApprovalListFragment.this, PARSER_TYPE.PO_PENDING_APPROVALS);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                if (!isRequestInProgress)
                    MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getPolistByStatusURL);
                isRequestInProgress = true;

                if (dialog != null && !isLoadMore)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), POApprovalDetailsActivity.class);
        intent.putExtra("poId", poList.get(position).getOrderNumber());
        intent.putExtra("POModel", poList.get(position));
        intent.putExtra("status", poList.get(position).getOrderStatus());
        getActivity().startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume();
        currentOffset = 0;
        isLoadMore = false;
        poList = new ArrayList<>();
        adapter.removeAllItems();
        getPOApprovalList();

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataAdapter(ArrayList<OrdersModel> tempList) {
        if (tempList != null && tempList.size() > 0) {
            for (int i = 0; i < tempList.size(); i++) {
                adapter.addItem(tempList.get(i));
                if (!poList.contains(tempList.get(i)))
                    poList.add(tempList.get(i));
            }

            adapter.notifyDataSetChanged();
        } else {
            if (poList.size() < 1) {
                llHeader.setVisibility(View.GONE);
                tvNoPOs.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.PO_PENDING_APPROVALS) {
                    if (results instanceof OrdersModel) {
                        currentOffset += offsetLimit;
                        isRequestInProgress = false;
                        OrdersModel ordersModel = (OrdersModel) results;
                        ArrayList<OrdersModel> tempList = ordersModel.getArrOrders();
                        totalItemsCount = ordersModel.getCount();
                        if (tempList != null && tempList.size() > 0) {
                            setDataAdapter(tempList);
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}
