package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.RetailersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class UpdateRetailerFragment extends DialogFragment implements Response.ErrorListener, VolleyHandler<Object>, ManufacturerFragment.OnClickListener {

    private Spinner spinnerVolumeClass, spinnerBusinessType, spinnerBuyerType;
    private EditText etNoOfShutters, tvManf;
    private Button btnSubmit;
    private ArrayList<CustomerTyepModel> arrVolumes, arrManf,arrBuyerType;
    private ArrayList<BusinessTypeModel> arrBusinessTypes;
    private DBHelper dataBaseObj;
    private String volumeClass, businessType, buyerType;
    private OnDialogClosed onDialogClosed;
    private Dialog dialog;
    private SharedPreferences mSharedPreferences;
    private String manfIds = "";
    private RadioButton rbSmartYes, rbSmartNo, rbInternetYes, rbInternetNo;
    private RetailersModel retailersModel;

    public static UpdateRetailerFragment newInstance(RetailersModel retailersModel) {

        UpdateRetailerFragment f = new UpdateRetailerFragment();
        Bundle args = new Bundle();
        args.putParcelable("RetailersModel", retailersModel);
        f.setArguments(args);
        return f;
    }

    public static ArrayList<String> filter(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerGrpId());
            }
        }
        return result;
    }

    public static ArrayList<String> filterNames(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerName());
            }
        }
        return result;
    }

    public void setClickListener(OnDialogClosed listener) {
        onDialogClosed = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
//        theme = android.R.style.Theme_Holo_Dialog_NoActionBar;
//        setStyle(style, theme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update_retailer, container);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.getParcelable("RetailersModel") != null) {
            retailersModel = (RetailersModel) args.getParcelable("RetailersModel");
        }

        spinnerVolumeClass = (Spinner) view.findViewById(R.id.spinner_volume_class);
        spinnerBusinessType = (Spinner) view.findViewById(R.id.spinner_business_type);
        spinnerBuyerType = (Spinner) view.findViewById(R.id.spinner_buyer_type);
        etNoOfShutters = (EditText) view.findViewById(R.id.et_no_of_shutters);
        tvManf = (EditText) view.findViewById(R.id.tv_manf);
        btnSubmit = (Button) view.findViewById(R.id.bt_submit);
        rbSmartYes = (RadioButton) view.findViewById(R.id.radio_smart_yes);
        rbSmartNo = (RadioButton) view.findViewById(R.id.radio_smart_no);
        rbInternetYes = (RadioButton) view.findViewById(R.id.radio_internet_yes);
        rbInternetNo = (RadioButton) view.findViewById(R.id.radio_internet_no);

        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);

        dataBaseObj = new DBHelper(getActivity());

        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);

        arrManf = dataBaseObj.getMasterLookUpData(DBHelper.TABLE_MASTER_MANF);

        arrBuyerType = dataBaseObj.getMasterLookUpData(DBHelper.TABLE_CUSTOMER_TYPE);
        if (arrBuyerType != null && arrBuyerType.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrBuyerType);
            spinnerBuyerType.setAdapter(arrayAdapter);
            int pos = 0;
            for (int i = 0; i < arrBuyerType.size(); i++) {
                CustomerTyepModel customerTyepModel = arrBuyerType.get(i);
                if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(retailersModel.getBuyerTypeId())) {
                    pos = i;
                    break;
                }
            }
            spinnerBuyerType.setSelection(pos);
        }

        arrVolumes = dataBaseObj.getMasterLookUpData(DBHelper.TABLE_VOLUME_CLASS);
        if (arrVolumes != null && arrVolumes.size() > 0) {
            ArrayAdapter<CustomerTyepModel> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrVolumes);
            spinnerVolumeClass.setAdapter(arrayAdapter);
            int pos = 0;
            for (int i = 0; i < arrVolumes.size(); i++) {
                CustomerTyepModel customerTyepModel = arrVolumes.get(i);
                if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(retailersModel.getVolumeClass())) {
                    pos = i;
                    break;
                }
            }
            spinnerVolumeClass.setSelection(pos);
        }

        arrBusinessTypes = dataBaseObj.getAllSegments();
        if (arrBusinessTypes != null && arrBusinessTypes.size() > 0) {
            ArrayAdapter<BusinessTypeModel> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrBusinessTypes);
            spinnerBusinessType.setAdapter(arrayAdapter);
            int pos = 0;
            for (int i = 0; i < arrBusinessTypes.size(); i++) {
                BusinessTypeModel businessTypeModel = arrBusinessTypes.get(i);
                if (businessTypeModel.getSegmentId().equalsIgnoreCase(retailersModel.getBusinessTypeId())) {
                    pos = i;
                    break;
                }
            }
            spinnerBusinessType.setSelection(pos);
        }

//        if (arrManf != null && arrManf.size() > 0) {
//            ManufacturerAdapter adapter = new ManufacturerAdapter(getActivity(), arrManf, retailersModel);
//            spinnerManufacturers.setAdapter(adapter);
//        }

        tvManf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrManf != null && arrManf.size() > 0)
                    showManfDialog();
                else
                    Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.no_manf),Toast.LENGTH_SHORT).show();
            }
        });

        if (retailersModel != null) {
            etNoOfShutters.setText(retailersModel.getNoOfShutters());
            etNoOfShutters.setSelection(etNoOfShutters.getText().length());
        }

//        spinnerManufacturers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                for(int i=0;i<arrManf.size();i++){
//                    String result = "";
//                    Predicate<CustomerTyepModel> isCheckedData = new Predicate<CustomerTyepModel>() {
//                        @Override
//                        public boolean apply(CustomerTyepModel customerTyepModel) {
//                            return customerTyepModel.isChecked();
//                        }
//                    };
//
//                    ArrayList<String> checkedFilters = filterNames(arrManf, isCheckedData);
//                    result = TextUtils.join(",", checkedFilters);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        spinnerBuyerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                buyerType = arrBuyerType.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerVolumeClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                volumeClass = arrVolumes.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerBusinessType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                businessType = arrBusinessTypes.get(i).getSegmentId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Predicate<CustomerTyepModel> isCheckedManfs = new Predicate<CustomerTyepModel>() {
                    @Override
                    public boolean apply(CustomerTyepModel customerTyepModel) {
                        return customerTyepModel.isChecked();
                    }
                };

                ArrayList<String> checkedFilters = filter(arrManf, isCheckedManfs);
                manfIds = TextUtils.join(",", checkedFilters);
                if (manfIds != null && manfIds.startsWith(",")) {
                    manfIds = manfIds.replaceFirst(",", "");
                }

                if (TextUtils.isEmpty(buyerType) || buyerType.equalsIgnoreCase(getResources().getString(R.string.select_buyer_type))) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.please_select_buyer_type));
                } else if (TextUtils.isEmpty(volumeClass) || volumeClass.equalsIgnoreCase(getResources().getString(R.string.select_volume_class))) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.please_select_volume_class));
                } else if (TextUtils.isEmpty(businessType) || businessType.equalsIgnoreCase(getResources().getString(R.string.select_segment))) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.please_select_segment));
                } else if (TextUtils.isEmpty(etNoOfShutters.getText().toString())) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.please_enter_no_of_shutters));
                } else if (TextUtils.isEmpty(manfIds)) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.please_select_manufacturers));
                } else {
                    updateRetailer();
                }
            }
        });
    }

    private void showManfDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ManufacturerFragment manufacturerFragment = ManufacturerFragment.newInstance(arrManf,retailersModel.getMasterManfIds());
        manufacturerFragment.setClickListener(UpdateRetailerFragment.this);
        manufacturerFragment.setCancelable(true);
        manufacturerFragment.show(fm, "manf_fragment");
    }

    private void updateRetailer() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                JSONObject obj = new JSONObject();

                HashMap<String, String> map = new HashMap<>();
                obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("user_id", retailersModel.getCustomerId());
                obj.put("buyer_type", buyerType);
                obj.put("volume_class", volumeClass);
                obj.put("noof_shutters", etNoOfShutters.getText().toString());
                obj.put("master_manf", manfIds);
                obj.put("smartphone", rbSmartYes.isChecked() ? "1" : "0");
                obj.put("network", rbInternetYes.isChecked() ? "1" : "0");
                map.put("data", obj.toString());

                VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateRetailerURL, map, UpdateRetailerFragment.this, UpdateRetailerFragment.this, PARSER_TYPE.UPDATE_RETAILER);
                topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.getAllRetailersURL);

                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if(error!=null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e){
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.UPDATE_RETAILER) {
                    if (response instanceof String) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setMessage(message);
                        dialog.setTitle(getString(R.string.app_name));
                        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                UpdateRetailerFragment.this.dismiss();
                                onDialogClosed.dialogClose();
                            }
                        });
                        AlertDialog alert = dialog.create();
                        alert.setCanceledOnTouchOutside(false);
                        alert.show();

                    }
                }
            } else {
                Utils.showAlertDialog(getActivity(), message);
            }
        } else {
            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onClicked(String string) {
        if (string != null) {
            if (string.contains("All")) {
                tvManf.setText("All");
            } else {
                tvManf.setText(string);
            }
        }

    }

    public interface OnDialogClosed {
        public void dialogClose();
    }
}
