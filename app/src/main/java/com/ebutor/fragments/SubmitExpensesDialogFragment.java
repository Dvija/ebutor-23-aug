package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.NewPacksModel;
import com.ebutor.models.PriceHistoryModel;

import java.util.ArrayList;
import java.util.Locale;

public class SubmitExpensesDialogFragment extends DialogFragment {

    private ArrayList<NewPacksModel> priceList;
    private PriceHistoryModel priceHistoryModel;
    private LinearLayout llPriceList;
    private TextView tvMinPrice, tvAvgPrice, tvMaxPrice;

    public SubmitExpensesDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static SubmitExpensesDialogFragment newInstance(PriceHistoryModel priceHistoryModel) {
        SubmitExpensesDialogFragment frag = new SubmitExpensesDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable("PriceHistoryModel", priceHistoryModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.popup_price_history, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view

        priceHistoryModel = (PriceHistoryModel) getArguments().getSerializable("PriceHistoryModel");

        priceList = priceHistoryModel.getArrPrices();

        llPriceList = (LinearLayout) view.findViewById(R.id.ll_price_list);
        tvMinPrice = (TextView) view.findViewById(R.id.tv_min_price);
        tvAvgPrice = (TextView) view.findViewById(R.id.tv_avg_price);
        tvMaxPrice = (TextView) view.findViewById(R.id.tv_max_price);

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String minPrice = priceHistoryModel.getMinPrice();
        if(null!= minPrice && !TextUtils.isEmpty(minPrice)){
            minPrice = String.format(Locale.CANADA, "%.2f", Float.valueOf(minPrice));
        }else{
            minPrice = "0.00";
        }

        String avgPrice = priceHistoryModel.getAvgPrice();
        if(null!= avgPrice && !TextUtils.isEmpty(avgPrice)){
            avgPrice = String.format(Locale.CANADA, "%.2f", Float.valueOf(avgPrice));
        }else{
            avgPrice = "0.00";
        }

        String maxPrice = priceHistoryModel.getMaxPrice();
        if(null!= maxPrice && !TextUtils.isEmpty(maxPrice)){
            maxPrice = String.format(Locale.CANADA, "%.2f", Float.valueOf(maxPrice));
        }else{
            maxPrice = "0.00";
        }

        tvMinPrice.setText(getActivity().getResources().getString(R.string.min_price) + "\n" + minPrice);
        tvAvgPrice.setText(getActivity().getResources().getString(R.string.avg_price) + "\n" + avgPrice);
        tvMaxPrice.setText(getActivity().getResources().getString(R.string.max_price) + "\n" + maxPrice);

        if (priceList != null && priceList.size() > 0) {
            for (int i = 0; i < priceList.size(); i++) {
                NewPacksModel newPacksModel = priceList.get(i);
                if (newPacksModel != null) {
                    View packView = inflater.inflate(R.layout.row_price_history, null);

                    final TextView tvSupplierName = (TextView) packView.findViewById(R.id.tvSupplierName);
                    final TextView tvDate = (TextView) packView.findViewById(R.id.tvDate);
                    final TextView tvPrice = (TextView) packView.findViewById(R.id.tvPrice);

                    double price = 0.0;
                    try {
                        price = Double.parseDouble(newPacksModel.getPrice());
                    } catch (Exception e) {
                        price = 0.0;
                    }

                    tvSupplierName.setText(newPacksModel.getSupplierName());
                    tvDate.setText(newPacksModel.getDate());
                    tvPrice.setText(String.format(Locale.CANADA, "%.2f", price));

                    llPriceList.addView(packView);
                }
            }
        }
    }

    public interface OkListener {
        void onOkClicked(String city);
    }

}
