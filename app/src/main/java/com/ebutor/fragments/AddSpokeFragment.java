package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AddSpokeFragment extends DialogFragment implements Response.ErrorListener, VolleyHandler<Object> {

    private CustomerTyepModel spokeModel;
    private EditText etSpoke;
    private Button btnOk;
    private String spoke, hubId;
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private OnSpokeClose onSpokeClose;

    public static AddSpokeFragment newInstance(CustomerTyepModel spokeModel, String hubId) {
        AddSpokeFragment frag = new AddSpokeFragment();
        Bundle args = new Bundle();
        args.putSerializable("SpokeModel", spokeModel);
        args.putString("hubId", hubId);
        frag.setArguments(args);
        return frag;
    }

    public void setClickListener(OnSpokeClose listener) {
        onSpokeClose = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setTitle(getActivity().getResources().getString(R.string.add_spoke));
        if (getArguments().containsKey("SpokeModel")) {
            spokeModel = (CustomerTyepModel) getArguments().getSerializable("SpokeModel");
        }
        if (getArguments().containsKey("hubId")) {
            hubId = getArguments().getString("hubId");
        }
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TABS_PROGRESS);
        return inflater.inflate(R.layout.fragment_add_spoke, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSpoke = (EditText) view.findViewById(R.id.et_spoke);
        btnOk = (Button) view.findViewById(R.id.btn_ok);

        if (spokeModel != null) {
            if (spokeModel.getCustomerName() != null && !spokeModel.getCustomerName().equalsIgnoreCase("Please select")) {
                etSpoke.setText(spokeModel.getCustomerName());
                etSpoke.setSelection(etSpoke.getText().length());
            }
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                spoke = etSpoke.getText().toString();

                if (TextUtils.isEmpty(spoke)) {
                    Toast.makeText(getActivity(), getString(R.string.please_enter_spoke), Toast.LENGTH_SHORT).show();
                    return;
                }

                addSpoke();
            }
        });
    }

    private void addSpoke() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                jsonObject.put("name", spoke);
                jsonObject.put("le_wh_id", hubId);
                jsonObject.put("id", spokeModel.getCustomerGrpId());
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addSpokeURL, map, AddSpokeFragment.this, AddSpokeFragment.this, PARSER_TYPE.ADD_SPOKE);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.addSpokeURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.ADD_SPOKE) {
                        if (response instanceof CustomerTyepModel) {

                            spokeModel = (CustomerTyepModel) response;
                            dismiss();
                            onSpokeClose.spokeClose(spokeModel);
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    public interface OnSpokeClose {
        public void spokeClose(Object spokeModel);
    }
}
