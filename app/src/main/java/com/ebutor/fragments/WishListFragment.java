package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.WishListModel;

import java.util.ArrayList;

/**
 * Created by santoshbaggam on 13/04/16.
 */
public class WishListFragment extends DialogFragment {
    private OkListener confirmListener;
    private ListView mRecyclerView;

    private ItemsAdapter mAdapter;
    private ArrayList<WishListModel> areasList;

    public WishListFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static WishListFragment newInstance(ArrayList<WishListModel> list) {
        WishListFragment frag = new WishListFragment();
        Bundle args = new Bundle();
        args.putSerializable("WishList", list);
        frag.setArguments(args);
        return frag;
    }

    public void setConfirmListener(OkListener listener) {
        this.confirmListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getActivity().getResources().getString(R.string.wishlist));
        return inflater.inflate(R.layout.wish_list_dialog_fragment, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view


        mRecyclerView = (ListView) view.findViewById(R.id.wishListListView);

        areasList = (ArrayList<WishListModel>) getArguments().getSerializable("WishList");

        mAdapter = new ItemsAdapter(getActivity(), areasList);
        mRecyclerView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Toast.makeText(getActivity(), (String) view.getTag(), Fse.LENGTH_LONG).show();
                view.setSelected(true);
                dismiss();
                Object obj = view.getTag();
                if (obj != null) {
                    WishListModel model = (WishListModel) obj;
//                    areasList.get(position).setSelected(true);
//                    MyApplication.getInstance().setCitiesList(areasList);
                    //toggle((CheckedTextView) view);
                    if (confirmListener != null) confirmListener.onOkClicked(model.getListId());
                }

            }
        });
        /*mAdapter.SetOnItemClickListener(new AreasDialogAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Object obj) {
                Toast.makeText(getActivity(), ((AreaModel)obj).getAreaName(), Toast.LENGTH_LONG).show();
            }
        });*/

        /*btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmListener != null) {
                    confirmListener.onOkClicked();
                }
                dismiss();
            }
        });*/


    }

    public void toggle(CheckedTextView v) {
        if (v.isChecked()) {
            v.setChecked(false);
        } else {
            v.setChecked(true);
        }
    }

    public interface OkListener {
        void onOkClicked(String city);
    }

    private class ItemsAdapter extends BaseAdapter {
        ArrayList<WishListModel> items;
        String selected = "";
        private Context context;

        public ItemsAdapter(Context context, ArrayList<WishListModel> item) {
            this.context = context;
            this.items = item;
        }

        // @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_wish_list_dialog, null);
            }
            TextView wishListNameTextView = (TextView)v.findViewById(R.id.tvWishListName);
            wishListNameTextView.setText(items.get(position).getListName());


            v.setTag(items.get(position));
            return v;
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }
}
