package com.ebutor.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.FilterFFActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.SODashboardRecyclerAdapter;
import com.ebutor.adapters.SODeliveryDashboardAdapter;
import com.ebutor.adapters.SOPickerDashboardAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.SODashboardModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class SODashBoardFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object> {


    private static final int SUMMARY = 0;
    private static final int PICKER = 1;
    private static final int DELIVERY = 2;
    Typeface tf;
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private String startDate, endDate;
    private TextView tvFilter, tvTitle;
    //    private SwipeRefreshLayout mSwipeRefreshLayout;
    //    private TextView tvTBV, tvUOB, tvABV, tvTLC, tvULC, tvALC;
    private boolean hasChilds;
    private LinearLayout llSummary, llPicker, llDelivery;
    private RecyclerView mRecyclerView, rvPicker, rvDelivery;
    private RecyclerView.LayoutManager mLayoutManager;
    private PARSER_TYPE parserType = PARSER_TYPE.RETAILER_DASHBOARD;
    private int type = DashBoardTypes.ORGANIZATION;
    private int flag = SUMMARY;
    private FloatingActionButton fab;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);
    DashboardFilterFragment.OnSelListener okListener = new DashboardFilterFragment.OnSelListener() {

        @Override
        public void onSelected(int pos) {
            switch (pos) {
                case 0:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.ORGANIZATION;
                    break;
                case 1:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.MY_DASHBOARD;
                    break;
                case 2:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.MY_DASHBOARD;
                    showDatePicker();
                    break;
                case 3:
                    parserType = PARSER_TYPE.MY_TEAM_DASHBOARD;
                    type = DashBoardTypes.MY_TEAM_DASHBOARD;
                    break;
                default:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.ORGANIZATION;
                    break;
            }
            getSODashboard(createJSON(null, null, ""), false);
        }
    };
    private PieChart mChart;

    public static SODashBoardFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("type", page);
        SODashBoardFragment fragment = new SODashBoardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_so_dashboard, container, false);
        setHasOptionsMenu(true);
        tf = Typeface.createFromAsset(getActivity().getAssets(), "font/OpenSans-Regular.ttf");
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mChart = (PieChart) view.findViewById(R.id.pieChart1);
        mChart.setDrawHoleEnabled(false);

        hasChilds = mSharedPreferences.getBoolean(ConstantValues.KEY_HAS_CHILD, false);
        llSummary = (LinearLayout) view.findViewById(R.id.ll_summary);
        llPicker = (LinearLayout) view.findViewById(R.id.ll_picker);
        llDelivery = (LinearLayout) view.findViewById(R.id.ll_delivery);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        rvPicker = (RecyclerView) view.findViewById(R.id.rv_picker);
        rvDelivery = (RecyclerView) view.findViewById(R.id.rv_delivery);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        tvFilter = (TextView) view.findViewById(R.id.tv_filter);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);
        if (getArguments() != null && getArguments().containsKey("type")) {
            type = getArguments().getInt("type");
            parserType = PARSER_TYPE.RETAILER_DASHBOARD;
        }

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new MarginDecoration(getActivity()));
        mRecyclerView.setHasFixedSize(true);

        rvPicker.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        rvPicker.addItemDecoration(new MarginDecoration(getActivity()));
        rvPicker.setHasFixedSize(true);

        rvDelivery.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        rvDelivery.addItemDecoration(new MarginDecoration(getActivity()));
        rvDelivery.setHasFixedSize(true);

        getSODashboard(createJSON(null, null, ""), false);

// radius of the center hole in percent of maximum radius
        mChart.setHoleRadius(0f);
        mChart.setTransparentCircleRadius(0f);
        Description description = new Description();
        description.setText("");
        mChart.setDescription(description);


        tvFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), FilterFFActivity.class);
                if (startDate != null)
                    intent.putExtra("start_date", Utils.parseDate(Utils.DATE_STD_PATTERN5, Utils.DATE_STD_PATTERN3, startDate));
                if (endDate != null)
                    intent.putExtra("end_date", Utils.parseDate(Utils.DATE_STD_PATTERN5, Utils.DATE_STD_PATTERN3, endDate));
                startActivityForResult(intent, 1);

//                FragmentManager fm = getActivity().getSupportFragmentManager();
//                DashboardFilterFragment dashboardFilterFragment = DashboardFilterFragment.newInstance(arrFilters);
//                dashboardFilterFragment.setCancelable(true);
//                dashboardFilterFragment.setConfirmListener(okListener);
//                dashboardFilterFragment.show(fm, "filter dashboard");
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fab.isShown()) {
                    fab.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    fab.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        rvPicker.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fab.isShown()) {
                    fab.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    fab.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        rvDelivery.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fab.isShown()) {
                    fab.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    fab.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mChart.setVisibility(View.GONE);
                switch (flag) {
                    case SUMMARY:
                        flag = PICKER;
                        fab.setImageResource(R.drawable.ic_picker);
                        tvTitle.setText(getString(R.string.picker));
                        llSummary.setVisibility(View.GONE);
                        getSODashboard(createJSON(startDate, endDate, ""), false);

//                        rvDelivery.setVisibility(View.GONE);
//                        getSODashboard(createJSON(null, null, ""), false);
//                        flag = PICKER;
                        break;
                    case PICKER:
                        flag = DELIVERY;
                        fab.setImageResource(R.drawable.ic_delivery);
                        tvTitle.setText(getString(R.string.delivery));
                        llPicker.setVisibility(View.GONE);
                        getSODashboard(createJSON(startDate, endDate, ""), false);

//                        mRecyclerView.setVisibility(View.GONE);
//                        getSODashboard(createJSON(null, null, ""), false);
//                        flag = DELIVERY;
                        break;
                    case DELIVERY:
                        flag = SUMMARY;
                        fab.setImageResource(R.drawable.ic_summary);
                        tvTitle.setText(getString(R.string.summary));
                        llDelivery.setVisibility(View.GONE);
                        getSODashboard(createJSON(startDate, endDate, ""), false);

//                        rvPicker.setVisibility(View.GONE);
//                        getSODashboard(createJSON(null, null, ""), false);
//                        flag = SUMMARY;
                        break;
                    default:
                        getSODashboard(createJSON(startDate, endDate, ""), false);
//                        flag = SUMMARY;
                        break;
                }
            }
        });

    }

    private SpannableString generateCenterText() {
        SpannableString s = new SpannableString("Revenues\nQuarters 2016");
        s.setSpan(new RelativeSizeSpan(2f), 0, 8, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 8, s.length(), 0);
        return s;
    }

    /**
     * generates less data (1 DataSet, 4 values)
     *
     * @return
     */
    protected PieData generatePieData(ArrayList<SODashboardModel> list, int flag) {

        int count = list.size();

        ArrayList<PieEntry> entries1 = new ArrayList<PieEntry>();

        for (int i = 0; i < count; i++) {
            String label = "";
            float value = 0;
            switch (flag) {
                case SUMMARY:
                    value = (float) (Float.parseFloat(list.get(i).getPercentage()));
                    label = list.get(i).getStatus();
                    break;
                case PICKER:
                    value = (float) (Float.parseFloat(list.get(i).getContribution()));
                    label = list.get(i).getPickername();
                    break;
                case DELIVERY:
                    value = (float) (Float.parseFloat(list.get(i).getContribution()));
                    label = list.get(i).getDeliverName();
                    break;
            }
            entries1.add(new PieEntry(value, label));
        }

        PieDataSet ds1 = new PieDataSet(entries1, "");
        ds1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.BLACK);
        ds1.setValueTextSize(12f);
        ds1.setValueLinePart1OffsetPercentage(80.f);
        ds1.setValueLinePart1Length(0.4f);
        ds1.setValueLinePart2Length(0.4f);
        ds1.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
//        ds1.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData d = new PieData(ds1);
        d.setValueTypeface(tf);
        d.setValueFormatter(new PercentFormatter());
        d.setValueTextSize(11f);
        d.setValueTextColor(Color.BLACK);
        d.setValueTypeface(tf);

        mChart.setData(d);

        return d;
    }

    private void getSODashboard(JSONObject dataObject, boolean isSwipeDown) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                PARSER_TYPE parserType = PARSER_TYPE.SO_DASHBOARD;
                switch (flag) {
                    case SUMMARY:
                        parserType = PARSER_TYPE.SO_DASHBOARD;
                        break;
                    case PICKER:
                        parserType = PARSER_TYPE.SO_DASHBOARD_PICKER;
                        break;
                    case DELIVERY:
                        parserType = PARSER_TYPE.SO_DASHBOARD_DELIVERY;
                        break;
                }
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSODashboard, map, SODashBoardFragment.this, SODashBoardFragment.this, parserType);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getSODashboard);
                if (dialog != null && !isSwipeDown)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject createJSON(String startDate, String endDate, String appliedFilters) {
        JSONObject dataObj = null;
        try {
            if (!TextUtils.isEmpty(appliedFilters)) {
                dataObj = new JSONObject(appliedFilters);
            } else {
                dataObj = new JSONObject();
            }
            if (null == startDate)
                startDate = getDate();
            if (null == endDate)
                endDate = getDate();
            dataObj.put("start_date", startDate);//2016-09-30
            dataObj.put("end_date", endDate);
            dataObj.put("flag", String.valueOf(flag));
            dataObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataObj;
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.calendar) {
            showDatePicker();
        } else if (item.getItemId() == R.id.refresh) {
            getSODashboard(createJSON(null, null, ""), false);
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDatePicker() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getChildFragmentManager(), "datePicker");
        newFragment.setListener(new DatePickerDialog.OnDateSetListener() {
            /**
             * @param view        The view associated with this listener.
             * @param year        The year that was set.
             * @param monthOfYear The month that was set (0-11) for compatibility
             *                    with {@link Calendar}.
             * @param dayOfMonth  The day of the month that was set.
             */
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String date = format.format(calendar.getTime());
                getSODashboard(createJSON(date, date, ""), false);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String appliedFilters = "";
                if (data.hasExtra("type")) {
                    type = data.getIntExtra("type", DashBoardTypes.ORGANIZATION);
                }
                if (data.hasExtra("start_date")) {
                    String strStart = data.getStringExtra("start_date");
                    startDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, strStart);
                }
                if (data.hasExtra("end_date")) {
                    String strEnd = data.getStringExtra("end_date");
                    endDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, strEnd);
                }

                if (data.hasExtra("applied_filters")) {
                    appliedFilters = data.getStringExtra("applied_filters");
                }

                switch (type) {
                    case DashBoardTypes.ORGANIZATION:
                        parserType = PARSER_TYPE.RETAILER_DASHBOARD;
//                        pos = "";
                        break;
                    case DashBoardTypes.MY_DASHBOARD:
                        parserType = PARSER_TYPE.RETAILER_DASHBOARD;
//                        pos = "";
                        break;
                    case DashBoardTypes.MY_TEAM_DASHBOARD:
                        parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                        type = 3;
                        break;
                }
                getSODashboard(createJSON(startDate, endDate, appliedFilters), false);
            }
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
//                    if (mSwipeRefreshLayout != null)
//                        mSwipeRefreshLayout.setRefreshing(false);
                    getDashboard(response);
                    /*switch (type) {
                        case DashBoardTypes.ORGANIZATION:
                            getDashboard(response);
                            break;
                        case DashBoardTypes.MY_DASHBOARD:
                            getDashboard(response);
                            break;
                        case DashBoardTypes.MY_TEAM_DASHBOARD:
                            getDashboard(response);
                            break;
                    }*/

                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void getDashboard(Object response) {

        if (response instanceof ArrayList) {

            ArrayList<SODashboardModel> dashboardModelArrayList = (ArrayList<SODashboardModel>) response;

            if (dashboardModelArrayList != null && dashboardModelArrayList.size() > 0) {

                mChart.setVisibility(View.VISIBLE);
                mChart.setEntryLabelColor(Color.BLACK);
                mChart.setData(generatePieData(dashboardModelArrayList, flag));
                mChart.animateXY(1500, 1500);

                Legend l = mChart.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
                l.setOrientation(Legend.LegendOrientation.VERTICAL);

                l.setDrawInside(false);
                l.setEnabled(false);

                switch (flag) {
                    case SUMMARY:
                        llSummary.setVisibility(View.VISIBLE);
                        llPicker.setVisibility(View.GONE);
                        llDelivery.setVisibility(View.GONE);
                        SODashboardRecyclerAdapter mAdapter = new SODashboardRecyclerAdapter(getActivity(), dashboardModelArrayList);
                        mRecyclerView.setAdapter(mAdapter);
                        break;
                    case PICKER:
                        llSummary.setVisibility(View.GONE);
                        llPicker.setVisibility(View.VISIBLE);
                        llDelivery.setVisibility(View.GONE);
                        SOPickerDashboardAdapter soPickerDashboardAdapter = new SOPickerDashboardAdapter(getActivity(), dashboardModelArrayList);
                        rvPicker.setAdapter(soPickerDashboardAdapter);
                        break;
                    case DELIVERY:
                        llSummary.setVisibility(View.GONE);
                        llPicker.setVisibility(View.GONE);
                        llDelivery.setVisibility(View.VISIBLE);
                        SODeliveryDashboardAdapter soDeliveryDashboardAdapter = new SODeliveryDashboardAdapter(getActivity(), dashboardModelArrayList);
                        rvDelivery.setAdapter(soDeliveryDashboardAdapter);
                        break;
                }

            } else {
                llSummary.setVisibility(View.GONE);
                llPicker.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
            }
        } else {

        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private DatePickerDialog.OnDateSetListener listener;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            if (listener != null) {
                listener.onDateSet(view, year, month, day);
            }
        }

        public void setListener(DatePickerDialog.OnDateSetListener listener) {
            this.listener = listener;
        }
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

    public class MarginDecoration extends RecyclerView.ItemDecoration {
        private int margin;

        public MarginDecoration(Context context) {
            margin = context.getResources().getDimensionPixelSize(R.dimen.dp_1);
        }

        @Override
        public void getItemOffsets(
                Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(margin, margin, margin, margin);
        }
    }
}