package com.ebutor.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.HomeActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.CountriesListRecyclerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.RetailersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FFCommentsDialog;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.ebutor.utils.fastscroll.AlphabetItem;
import com.ebutor.utils.fastscroll.RecyclerViewFastScroller;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 09-Jun-16.
 */
public class RetailersListFragment extends Fragment implements CountriesListRecyclerAdapter.OnItemClickListener,
        Response.ErrorListener, VolleyHandler<Object>, UpdateRetailerFragment.OnDialogClosed,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    private static final int REQUEST_LOCATION = 0;
    public DisplayImageOptions options;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    RecyclerView buyersList;
    ArrayList<RetailersModel> retailersModelArrayList;
    CountriesListRecyclerAdapter adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private List<AlphabetItem> mAlphabetItems;
    private RecyclerViewFastScroller fastScroller;
    private Tracker mTracker;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private SharedPreferences mSharedPreferences;
    private RetailersModel retailersModel;
    private EditText searchET;
    private Dialog dialog;
    private String mLatitude, mLongitude;
    private GoogleApiClient mGoogleApiClient;
    private boolean isFF;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragments_retailers, container, false);

        // Create an instance of GoogleAPIClient.

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_profile_default)
                .showImageForEmptyUri(R.drawable.ic_profile_default)
                .showImageOnFail(R.drawable.ic_profile_default)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        fastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.fast_scroller);
        retailersModelArrayList = new ArrayList<>();
        buyersList = (RecyclerView) view.findViewById(R.id.buyers_list);
        buyersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        searchET = (EditText) view.findViewById(R.id.input_search_query);

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);

        requestLocationPermission();
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        checkLocationSettings();

//        retailersModelArrayList = DBHelper.getInstance().getRetailers();
//        if(retailersModelArrayList!=null && retailersModelArrayList.size()>0){
//            setAdapter(retailersModelArrayList);
//        }else{
        callWebService(false);
//        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callWebService(true);
            }
        });
        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        searchET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (adapter != null) {
                    String text = searchET.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.getFilter().filter(text);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });


        return view;
    }

    private void callWebService(boolean isSwipeDown) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllRetailersURL, map, RetailersListFragment.this, RetailersListFragment.this, PARSER_TYPE.GET_ALL_RETAILERS);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.getAllRetailersURL);

            if (dialog != null && !isSwipeDown)
                dialog.show();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    private void submitFFComments(String commentId) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
            obj.put("user_id", retailersModel.getCustomerId());
            obj.put("activity", commentId);
            obj.put("latitude", mSharedPreferences.getString(ConstantValues.KEY_LATITUDE, ""));
            obj.put("longitude", mSharedPreferences.getString(ConstantValues.KEY_LONGITUDE, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.ffCommentsURL, map, RetailersListFragment.this, RetailersListFragment.this, PARSER_TYPE.FF_COMMENTS);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.ffCommentsURL);

            if (dialog != null)
                dialog.show();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    public void OnBuyerSelected(int id, RetailersModel retailersModel) {
        if (retailersModel != null) {
            this.retailersModel = retailersModel;
            if (id == 1) {// Log Call Popup
                //todo Create an API call for reasons
                ArrayList<CustomerTyepModel> ffComments = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_FF_COMMENTS);
                if (ffComments != null && ffComments.size() > 0) {
                    FFCommentsDialog fragment = FFCommentsDialog.newInstance(ffComments);
                    fragment.setListener(new FFCommentsDialog.OnFFOptionSelectedListener() {
                        @Override
                        public void OnFFOptionSelected(Object object) {
                            if (object != null && object instanceof CustomerTyepModel) {
                                CustomerTyepModel model = (CustomerTyepModel) object;
                                String commentId = model.getCustomerGrpId();

                                submitFFComments(commentId);
                            } else {
                                Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
                            }
                        }
                    });
                    fragment.show(getFragmentManager(), "some tag");
                } else {
                    Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
                }

            } else if (id == 2) {// Place Order
                mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, retailersModel.getLatitude()).apply();
                mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, retailersModel.getLongitude()).apply();
                if (retailersModel.isPopup()) {// check if we need to show popup for the required fields
                    UpdateRetailerFragment fragment = UpdateRetailerFragment.newInstance(retailersModel);
                    fragment.setClickListener(RetailersListFragment.this);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    fragment.show(ft, "");
                } else {// Everything good.. Go place order.
                    getRetailerToken(retailersModel);
                }
            } else {
                Utils.showAlertDialog(getActivity(), getString(R.string.unknown_request));
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
    }


    private void getRetailerToken(RetailersModel retailersModel) {

        showToast(retailersModel.getCompany());
        mSharedPreferences.edit().putString(ConstantValues.KEY_SHOP_NAME, retailersModel.getCompany()).apply();
        try {
            JSONObject obj = new JSONObject();
//                obj.put("appId", mSharedPreferences.getString(ConstantValues.KEY_APP_ID, ""));
            obj.put("telephone", retailersModel.getTelephone());
            obj.put("latitude", mSharedPreferences.getString(ConstantValues.KEY_LATITUDE, ""));
            obj.put("longitude", mSharedPreferences.getString(ConstantValues.KEY_LONGITUDE, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.getRetailerTokenURL, map, RetailersListFragment.this, RetailersListFragment.this, PARSER_TYPE.GET_RETAILER_TOKEN);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.getRetailerTokenURL);

            if (dialog != null)
                dialog.show();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.sync) {
            callWebService(false);
        } else if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /*private void setListData(ArrayList<RetailersModel> buyerInfo) {
        if (buyerInfo != null && buyerInfo.size() > 0) {

            Collections.sort(buyerInfo, new Comparator<RetailersModel>() {
                        public int compare(RetailersModel lhs, RetailersModel rhs) {
                            return lhs.getCompany().compareTo(rhs.getCompany());
                        }
                    }
            );

            ArrayList<BuyerInfoAdapter.Row> rows = new ArrayList<BuyerInfoAdapter.Row>();
            int start = 0;
            int end = 0;
            String previousLetter = null;
            Object[] tmpIndexItem = null;
            Pattern numberPattern = Pattern.compile("[0-9]");

            for (RetailersModel country : buyerInfo) {

                try{
                    String firstLetter = country.getCompany().substring(0, 1);

                    // Group numbers together in the scroller
                    if (numberPattern.matcher(firstLetter).matches()) {
                        firstLetter = "#";
                    }

                    // If we've changed to a new letter, add the previous letter to the alphabet scroller
                    if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                        end = rows.size() - 1;
                        tmpIndexItem = new Object[3];
                        tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                        tmpIndexItem[1] = start;
                        tmpIndexItem[2] = end;
                        alphabet.add(tmpIndexItem);

                        start = end + 1;
                    }

                    // Check if we need to add a header row
                    if (!firstLetter.equals(previousLetter)) {
                        rows.add(country);
                        sections.put(firstLetter, start);
                    }

                    // Add the country to the list
                    rows.add(new BuyerInfoAdapter.Item(country));
                    previousLetter = firstLetter;
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            if (previousLetter != null) {
                // Save the last letter
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = rows.size() - 1;
                alphabet.add(tmpIndexItem);
            }

            adapter = new BuyerInfoAdapter(getActivity(), rows);
            adapter.setOnBuyerSelectedListener(this);
            buyersList.setAdapter(adapter);

        } else {
            Toast.makeText(getActivity(), "No Retailers found", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(int id, Object object) {
        if (object != null && object instanceof RetailersModel) {
            OnBuyerSelected(id, (RetailersModel) object);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Retailer Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();

            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }

//            if (error != null)
//                Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
        } catch (Exception e) {
        }

    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        mSwipeRefreshLayout.setRefreshing(false);
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_ALL_RETAILERS) {
                    DBHelper.getInstance().deleteTable(DBHelper.TABLE_RETAILERS);
                    if (response instanceof ArrayList) {
                        retailersModelArrayList = (ArrayList<RetailersModel>) response;

                        Collections.sort(retailersModelArrayList, new Comparator<RetailersModel>() {
                            public int compare(RetailersModel r1, RetailersModel r2) {
                                return r1.getCompany().compareToIgnoreCase(r2.getCompany());
                            }
                        });

//                        retailersModelArrayList = DBHelper.getInstance().getRetailers();
                        setAdapter(retailersModelArrayList);
//                                setListData(retailersModelArrayList);

                    }

                } else if (requestType == PARSER_TYPE.GET_RETAILER_TOKEN) {
                    if (response instanceof RetailersModel) {

                        RetailersModel retailersModel = (RetailersModel) response;

                        String customerToken = retailersModel.getCustomerToken();
                        String customerId = retailersModel.getCustomerId();
                        String firstName = retailersModel.getFirstName();
                        String lastName = retailersModel.getLastName();
                        String image = /*ConstantValues.NEW_BASE_IMAGE_AUTH + */retailersModel.getImage();
                        String legalEntityId = retailersModel.getLegalEntityId();
                        String beatId = retailersModel.getBeatId();
                        String latitude = retailersModel.getLatitude();
                        String longitude = retailersModel.getLongitude();

                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString(ConstantValues.KEY_CUSTOMER_ID, customerId);
                        editor.putString(ConstantValues.KEY_RETAILER_NAME, firstName + " " + lastName);
                        editor.putString(ConstantValues.KEY_FIRST_NAME, firstName + " " + lastName);
                        editor.putString(ConstantValues.KEY_PROFILE_IMAGE, image);
                        editor.putString(ConstantValues.KEY_CUSTOMER_TOKEN, customerToken);
                        mSharedPreferences.edit().putString(ConstantValues.KEY_LEGAL_ENTITY_ID, legalEntityId).apply();
                        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, true).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, beatId).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, latitude).apply();
                        mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, longitude).apply();
                        editor.apply();

                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } else if (requestType == PARSER_TYPE.FF_COMMENTS) {
                    Utils.showAlertDialog(getActivity(), message);
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }


    private void setAdapter(ArrayList<RetailersModel> retailersModelArrayList) {
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();

        for (int i = 0; i < retailersModelArrayList.size(); i++) {
            String name = retailersModelArrayList.get(i).getCompany();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1).toUpperCase();
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }

        adapter = new CountriesListRecyclerAdapter(getActivity(), retailersModelArrayList);
        adapter.setOnItemClickListener(this);
        buyersList.setAdapter(adapter);

        fastScroller.setRecyclerView(buyersList);
        fastScroller.setUpAlphabet(mAlphabetItems);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void dialogClose() {
        getRetailerToken(retailersModel);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        /*Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
            mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
        }*/
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(20000);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(10000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void requestLocationPermission() {
        Log.i("", "CAMERA permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Log.i("",
                    "Displaying camera permission rationale to provide additional context.");
//            Snackbar.make(findViewById(R.id.tv_capture), R.string.permission_location_rationale,
//                    Snackbar.LENGTH_INDEFINITE)
//                    .setAction(android.R.string.ok, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            ActivityCompat.requestPermissions(BuyerDetailsActivity.this,
//                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
//                                    REQUEST_LOCATION);
//                        }
//                    })
//                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
        // END_INCLUDE(camera_permission_request)
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        } else {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mCurrentLocation != null) {

                double latitude = mCurrentLocation.getLatitude();
                double longitude = mCurrentLocation.getLongitude();
                mLatitude = String.valueOf(latitude);
                mLongitude = String.valueOf(longitude);

//                try {
//                    Address address = getAddress(new LatLng(latitude, longitude));
//                    if (address != null) {
//                        int maxAddressLine = address.getMaxAddressLineIndex();
//                        String strAddress = "";
//                        String addressLine2 = "";
//                        if (maxAddressLine > 2) {
//                            strAddress = address.getAddressLine(0) + "," + address.getAddressLine(1);
//
//                            for (int i = 2; i < maxAddressLine; i++) {
//                                addressLine2 = addressLine2 + address.getAddressLine(i) + ", ";
//                            }
//                        } else {
//                            for (int i = 2; i < maxAddressLine; i++) {
//                                strAddress = strAddress + address.getAddressLine(i) + ", ";
//                            }
//                        }

//                        strAddress = address.getAddressLine(0);
//                        addressLine2 = address.getAddressLine(1);
//
//                        String lastLine = address.getAddressLine(maxAddressLine - 1);
//                        if (lastLine.length() >= 6) {
//                            lastLine = lastLine.substring(lastLine.length() - 6, lastLine.length());
//                        }
//                        if (lastLine.matches(".*\\d+.*")) {
//                            etPinCode.setText(lastLine);
//                        }
//
//
//                        String area = address.getSubAdminArea();
//                        String adminArea = address.getAdminArea();
//                        String locality = address.getLocality();
//                        String postalCode = address.getPostalCode();
//                        String state = address.getAdminArea();
//                        checkIfStateAvaiable(state);
//                        if (postalCode != null)
//                            etPinCode.setText(postalCode);
//
//                        if (locality != null)
//                            etCity.setText(locality);
//
//                        etAddress1.setText(strAddress);
//                        etAddress2.setText(addressLine2);
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

//                mSharedPreferences.edit().putString(ConstantValues.KEY_LATITUDE, mLatitude).apply();
//                mSharedPreferences.edit().putString(ConstantValues.KEY_LONGITUDE, mLongitude).apply();
                //todo Service call to pass location coordinates
//                Toast.makeText(getApplicationContext(), mCurrentLocation.getLatitude() + "  " + mCurrentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.unable_find_location), Toast.LENGTH_LONG).show();
            }
//            MyApplication.getInstance().setSelectedArea(null);
//            finish();
        }

    }
}
