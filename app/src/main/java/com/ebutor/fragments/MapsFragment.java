package com.ebutor.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.appolica.interactiveinfowindow.InfoWindow;
import com.appolica.interactiveinfowindow.InfoWindowManager;
import com.appolica.interactiveinfowindow.fragment.MapInfoWindowFragment;
import com.ebutor.BeatFilterActivity;
import com.ebutor.MapsActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.MapsAdapter;
import com.ebutor.adapters.OutletsAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.BeatModel;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.OutletsModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.GPSTracker;
import com.ebutor.utils.Networking;
import com.ebutor.utils.OnInfoWindowTouchListener;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class MapsFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, OnMapReadyCallback, FormFragment.OnClickListener, GoogleMap.OnMarkerClickListener {

    private static final int REQUEST_LOCATION = 0;
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private Thread thread;
    private ArrayList<String> arrSelectedBeats = new ArrayList<>();
    private ArrayList<OutletsModel> arrMapOutlets;
    private String beatId, selBeatId = "", selBeatName = "", prevBeatId = "";
    private GoogleMap map;
    private HashMap<String, ArrayList<OutletsModel>> hashMap;
    private ImageView ivFilter;
    private MarkerOptions options;
    private ArrayList<OutletsModel> retailersModelArrayList, arrOutlets;
    private OutletsModel selRetailerModel;
    BeatListFragment.OnSelListener okListener = new BeatListFragment.OnSelListener() {

        @Override
        public void onSelected(CustomerTyepModel customerTyepModel) {
            if (customerTyepModel != null) {
                MapsFragment.this.selBeatId = customerTyepModel.getCustomerGrpId();
                MapsFragment.this.selBeatName = customerTyepModel.getCustomerName();
                updateBeat();
            }
        }
    };
    private int count = 0, arrCount = 0;
    private HashMap<OutletsModel, MarkerOptions> mapMarkerOptions = new HashMap<>();
    private CustomerTyepModel selCustomerTyepModel;
    private Marker selectedMarker;
    private InfoWindow selInfoWindow;
    private OnInfoWindowTouchListener infoButtonListener;
    private RelativeLayout rlMap, rlSearch;
    private GPSTracker gpsTracker;
    private ProgressBar progressBar;
    private TextView tvProgress;
    private double currentLat = 0.0, currentLong = 0.0;
    private AutoCompleteTextView actSearch;
    private boolean isFF = false;
    private ArrayList<CustomerTyepModel> arrBeats, arrFilterBeats;
    private MapInfoWindowFragment mapInfoWindowFragment;
    private InfoWindowManager infoWindowManager;
    private MapsAdapter mapsAdapter;
    private BeatModel beatModel;
    private ArrayList<CustomerTyepModel> arrSelectedData, arrData = new ArrayList<>();
    Handler messageHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle b1 = msg.getData();
            try {
                OutletsModel _data = (OutletsModel) b1.getParcelable("data");

                double lat = 0.0, lon = 0.0;
                try {
                    lat = Double.parseDouble(_data.getLatitude());
                    lon = Double.parseDouble(_data.getLongitude());
                } catch (Exception e) {
                    lat = 0.0;
                    lon = 0.0;
                }

                if (lat != 0.0 && lon != 0.0) {
                    Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_pin);

                    Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
                    Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, _data.getColor());

                    options.position(new LatLng(lat, lon)).title(_data.getCompany()).
                            icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap));

                }

                count++;

                arrData = (arrSelectedData != null && arrSelectedData.size() > 0) ? arrSelectedData : arrFilterBeats;

                if (mapsAdapter != null) {

                    mapsAdapter = new MapsAdapter(getActivity(), arrData);
                    if (((MapsActivity) getActivity()).rvMaps != null) {
                        ((MapsActivity) getActivity()).rvMaps.setAdapter(mapsAdapter);
                        ((MapsActivity) getActivity()).rvMaps.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }
                }

                try {
                    Marker marker = map.addMarker(options.infoWindowAnchor(1, 1));
                    marker.setTag(_data);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            /*if (arrCount == arrMapOutlets.size()) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
//                llLoader.setVisibility(View.GONE);
            }*/

//            LatLng sydney = new LatLng(Double.valueOf(_data.getLatitude()), Double.valueOf(_data.getLongitude()));
//            Marker marker = map.addMarker(new MarkerOptions().position(sydney).title("Company  " + _data.getCompany())
//                    .snippet("Lat " + _data.getLatitude() + "\nLon : " + _data.getLongitude()));

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        if (marker.getTag() instanceof OutletsModel) {
                            OutletsModel outletsModel = (OutletsModel) marker.getTag();

                            InfoWindow.MarkerSpecification markerSpec =
                                    new InfoWindow.MarkerSpecification(20, 90);

                            selectedMarker = marker;

                            FormFragment fragment = new FormFragment();
                            Bundle args = new Bundle();
                            args.putParcelable("Outlet", outletsModel);
                            fragment.setArguments(args);
                            fragment.setClickListener(MapsFragment.this);

                            InfoWindow infoWindow = new InfoWindow(marker, markerSpec, fragment);
                            infoWindowManager.toggle(infoWindow, true);

                            selInfoWindow = infoWindow;
                        }

                        return true;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    };
    private Object mPauseLock;
    private View mapView;
    private boolean isFromManageBeats = false;
    private JSONArray beatsArray = new JSONArray();
    private String selDcId, selHubIds, selBeatIds, selBeatNames, selHubNames, selSpokeIds, selSpokeNames;
    private int colorCount = 0;
    BeatFilterFragment.OnSelListener multiSelListener = new BeatFilterFragment.OnSelListener() {

        @Override
        public void onSelected(ArrayList<String> selectedBeats, ArrayList<CustomerTyepModel> arrSelectedData) {
            arrSelectedBeats = selectedBeats;
            MapsFragment.this.arrSelectedData = arrSelectedData;
            loadMap();
        }
    };

    public static MapsFragment newInstance() {
        MapsFragment fragment = new MapsFragment();
        return fragment;
    }

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static ArrayList<String> filterNames(Collection<OutletsModel> target, Predicate<OutletsModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (OutletsModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getBeatName());
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_maps, container, false);
        setHasOptionsMenu(true);
        if (getArguments().containsKey("beatModel")) {
            beatModel = (BeatModel) getArguments().getSerializable("beatModel");
        }
        if (getArguments().containsKey("isFromManageBeats")) {
            isFromManageBeats = getArguments().getBoolean("isFromManageBeats", false);
        }
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        actSearch = (AutoCompleteTextView) view.findViewById(R.id.actSearch);
        rlMap = (RelativeLayout) view.findViewById(R.id.rl_map);
        rlSearch = (RelativeLayout) view.findViewById(R.id.rl_search);
        ivFilter = (ImageView) view.findViewById(R.id.iv_filter);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        tvProgress = (TextView) view.findViewById(R.id.tv_progress);

        mapInfoWindowFragment =
                (MapInfoWindowFragment) getChildFragmentManager().findFragmentById(R.id.infoWindowMap);

        infoWindowManager = mapInfoWindowFragment.infoWindowManager();
        infoWindowManager.setHideOnFling(false);

        retailersModelArrayList = new ArrayList<>();
        arrMapOutlets = new ArrayList<>();
        arrBeats = new ArrayList<>();
        arrFilterBeats = new ArrayList<>();
        hashMap = new HashMap<>();

        beatId = mSharedPreferences.getString(ConstantValues.KEY_BEAT_ID, "");
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);


        selDcId = MyApplication.getInstance().getDcId();
        if (TextUtils.isEmpty(selDcId)) {
            selDcId = mSharedPreferences.getString(ConstantValues.KEY_LE_WH_IDS, "");
            if (selDcId.contains(",")) {
                selDcId = selDcId.split(",")[0];
            }

            MyApplication.getInstance().setDcId(selDcId);
        }

        selHubIds = MyApplication.getInstance().getSelHubIds();
        if (TextUtils.isEmpty(selHubIds)) {
            selHubIds = mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, "");
            MyApplication.getInstance().setSelHubIds(selHubIds);
        }

        if (gpsTracker == null) {
            gpsTracker = new GPSTracker(getActivity());
        }
        if (!gpsTracker.getIsGPSTrackingEnabled()) {
            gpsTracker.showSettingsAlert();

        }
        actSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view != null && view.getTag() != null) {
                    OutletsModel outletsModel = (OutletsModel) view.getTag();
                    if (outletsModel != null && map != null) {
                        double lat = 0.0, lon = 0.0;
                        try {
                            lat = Double.parseDouble(outletsModel.getLatitude());
                            lon = Double.parseDouble(outletsModel.getLongitude());
                        } catch (Exception e) {
                            lat = 0.0;
                            lon = 0.0;
                        }
                        if (lat != 0.0 && lon != 0.0) {

//                            Marker marker = retailersModel.getMarker();

                            Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_pin);

                            Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
                            Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, outletsModel.getColor());

                            MarkerOptions options = new MarkerOptions().position(new LatLng(lat, lon)).title(outletsModel.getCompany()).
                                    icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap));
                            Marker marker = map.addMarker(options);
                            marker.setTag(outletsModel);

                            if (marker != null) {

                                final InfoWindow.MarkerSpecification markerSpec =
                                        new InfoWindow.MarkerSpecification(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                                FormFragment fragment = new FormFragment();
                                Bundle args = new Bundle();
                                args.putParcelable("Outlet", outletsModel);
                                fragment.setArguments(args);
                                fragment.setClickListener(MapsFragment.this);

                                final InfoWindow infoWindow = new InfoWindow(marker, markerSpec, fragment);
                                infoWindow.setWindowState(InfoWindow.WindowState.SHOWING);
                                infoWindowManager.toggle(infoWindow, true);
                                CameraUpdate center =
                                        CameraUpdateFactory.newLatLng(new LatLng(lat, lon));
                                CameraUpdate zoom = CameraUpdateFactory.zoomTo(17);
                                map.moveCamera(center);
                                map.animateCamera(zoom);
                            }
                        }
                    }
                }
            }
        });

        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                actSearch.setText("");

                Intent intent = new Intent(getActivity(), BeatFilterActivity.class);
                intent.putExtra("FilterBeats", arrFilterBeats);
                intent.putExtra("SelectedBeats", arrSelectedBeats);
                intent.putExtra("selDcId", selDcId);
                intent.putExtra("selHubIds", selHubIds);
                intent.putExtra("selBeatIds", selBeatIds);
                intent.putExtra("selBeatNames", selBeatNames);
                intent.putExtra("selHubNames", selHubNames);
                intent.putExtra("selSpokeNames", selSpokeNames);
                intent.putExtra("selSpokeIds", selSpokeIds);
                intent.putExtra("beatsArray", selBeatIds);
                intent.putExtra("showBeats", true);
                startActivityForResult(intent, 11);

            }
        });

        if (isFromManageBeats && beatModel != null) {

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(beatModel.getBeatId());
            getOutlets(createJSON(jsonArray, "", "", ""), false);

//            getOutlets(createJSON(beatModel.getBeatId(), "", ""), false);
        } else {

//            JSONArray hubsArray = new JSONArray();
//            hubsArray.put(mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, ""));
//            JSONArray beatsArray = new JSONArray();
//            beatsArray.put("-1");
            String hubIds = MyApplication.getInstance().getSelHubIds();
            String spokeIds = MyApplication.getInstance().getSelSpokeIds();
            if (TextUtils.isEmpty(hubIds))
                hubIds = mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, "");

            if (TextUtils.isEmpty(mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, "")))
                mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_HUB_EMPTY, true).apply();
            else
                mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_HUB_EMPTY, false).apply();

            // considering only 3 hubs assigned to that user.
            /*if (!TextUtils.isEmpty(hubIds)) {
                ArrayList<String> arrHubs = new ArrayList<String>(Arrays.asList(hubIds.split("\\s*,\\s*")));
                hubIds = "";
                for (int i = 0; i < arrHubs.size(); i++) {
                    if (i == 1)
                        break;
                    hubIds += arrHubs.get(i) + ",";
                }
                if (hubIds.endsWith(","))
                    hubIds = hubIds.substring(0, hubIds.length() - 1);

                JSONArray jsonArray = new JSONArray();
                String beatIds = MyApplication.getInstance().getSelBeats();
                try {
                    if (TextUtils.isEmpty(beatIds)) {
                        beatIds = "-1";
                        jsonArray.put(beatIds);
                    } else {
                        jsonArray = new JSONArray(beatIds);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    beatIds = "-1";
                    jsonArray.put(beatIds);
                }
                getOutlets(createJSON(jsonArray, hubIds, spokeIds, "1"), false);

//            Intent intent = new Intent(getActivity(), BeatFilterActivity.class);
//            intent.putExtra("FilterBeats", arrFilterBeats);
//            intent.putExtra("SelectedBeats", arrSelectedBeats);
//            startActivityForResult(intent, 11);

//       try {
//            beatsArray = new JSONArray();
//            beatsArray.put("-1");
//        } catch (Exception e) {
//            e.printStackTrace();
//            beatsArray = new JSONArray();
//        }
//
//        getOutlets(createJSON(beatsArray), false);

            } else {*/
            mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_HUB_EMPTY, true).apply();
            Intent intent = new Intent(getActivity(), BeatFilterActivity.class);
            intent.putExtra("FilterBeats", arrFilterBeats);
            intent.putExtra("SelectedBeats", arrSelectedBeats);
            intent.putExtra("selDcId", selDcId);
            intent.putExtra("selHubIds", selHubIds);
            intent.putExtra("selBeatIds", selBeatIds);
            intent.putExtra("selBeatNames", selBeatNames);
            intent.putExtra("selHubNames", selHubNames);
            intent.putExtra("selSpokeNames", selSpokeNames);
            intent.putExtra("selSpokeIds", selSpokeIds);
            intent.putExtra("beatsArray", selBeatIds);
            intent.putExtra("showBeats", false);
            startActivityForResult(intent, 11);
        }


//        }

    }

    private void getOutlets(JSONObject dataObject, boolean isSwipeDown) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllRetailersURL, map, MapsFragment.this, MapsFragment.this, PARSER_TYPE.OUTLETS_MAPS);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.getAllRetailersURL);
                if (dialog != null && !isSwipeDown)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadMap() {
        // Getting reference to SupportMapFragment of the activity_main
//        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (map == null) {
            mapInfoWindowFragment.getMapAsync(MapsFragment.this);
        } else {
            setMap();
        }
    }

    private void setMap() {
        map.clear();
        options = new MarkerOptions();
        hashMap = new HashMap<>();
        arrFilterBeats = new ArrayList<>();
        arrData = new ArrayList<>();
        currentLat = gpsTracker.getLatitude();
        currentLong = gpsTracker.getLongitude();

        if (currentLat != 0.0 || currentLong != 0.0) {
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(currentLat, currentLong));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(17);

            map.moveCamera(center);
            map.animateCamera(zoom);
        }

        mapsAdapter = new MapsAdapter(getActivity(), arrData);
        if (((MapsActivity) getActivity()).rvMaps != null) {
            ((MapsActivity) getActivity()).rvMaps.setAdapter(mapsAdapter);
            ((MapsActivity) getActivity()).rvMaps.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

        if (arrSelectedBeats != null && arrSelectedBeats.size() > 0) {
            arrMapOutlets = new ArrayList<>();
            for (int i = 0; i < arrSelectedBeats.size(); i++) {
                ArrayList<OutletsModel> arr = hashMap.get(arrSelectedBeats.get(i));
                arrMapOutlets.addAll(arr);
            }
        } else {
            arrMapOutlets = arrOutlets;
        }

        Collections.sort(arrMapOutlets, new Comparator<OutletsModel>() {
            public int compare(OutletsModel c1, OutletsModel c2) {
                String s1 = c1.getBeatName() == null ? "" : c1.getBeatName();
                String s2 = c2.getBeatName() == null ? "" : c2.getBeatName();
                return s1.compareToIgnoreCase(s2);
            }
        });

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                count = 0;
                colorCount = 0;
                arrCount = 0;
                mPauseLock = new Object();
                TypedArray array = getResources().obtainTypedArray(R.array.arrayColors);
                for (int i = 0; i < arrMapOutlets.size(); i++) {
                    arrCount++;
                    ArrayList<OutletsModel> arrayList = new ArrayList<>();
                    final OutletsModel retailersModel = arrMapOutlets.get(i);

                    String key = retailersModel.getBeatId();
                    int color = 0;
                    if (hashMap.containsKey(key)) {
                        arrayList = hashMap.get(key);
                        color = arrayList.get(0).getColor();
                    } else {

                        if (array != null && colorCount < array.length()) {
                            color = array.getColor(colorCount, 0);
                            colorCount++;
                        } else {
                            Random rand = new Random();

                            color = Color.argb(255, rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
                        }

                        selCustomerTyepModel = new CustomerTyepModel();
                        selCustomerTyepModel.setCustomerGrpId(key);
                        selCustomerTyepModel.setCustomerName(arrMapOutlets.get(i).getBeatName());
                        selCustomerTyepModel.setColor(color);
                        arrFilterBeats.add(selCustomerTyepModel);

                    }
                    arrMapOutlets.get(i).setColor(color);

                    arrayList.add(retailersModel);
                    hashMap.put(key, arrayList);

                    double lat = 0.0, lon = 0.0;
                    try {
                        lat = Double.parseDouble(retailersModel.getLatitude());
                        lon = Double.parseDouble(retailersModel.getLongitude());
                    } catch (Exception e) {
                        lat = 0.0;
                        lon = 0.0;
                    }

                    final int finalI = i;

                    if (lat != 0.0 && lon != 0.0) {

//                        Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_pin);
//
//                        Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
//                        Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, color);
//
//                        options.position(new LatLng(lat, lon)).title(retailersModel.getCompany()).
//                                icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap));
//
//                        mapMarkerOptions.put(retailersModel, options);
                        Message m = Message.obtain();
                        prepareMessage(m, retailersModel);

                        messageHandler.sendMessage(m);

                    }
                    try {
                        Thread.sleep(100);
                        ((Activity) getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                float size = arrMapOutlets.size();
                                float fI = finalI + 1;
                                float fVal = fI / size;
                                float progressValue = fVal * 100;
                                if (size == 0) {
                                    progressBar.setProgress(0);
                                    tvProgress.setText("0/0");
                                } else {
                                    progressBar.setProgress((int) progressValue);
                                    tvProgress.setText(count + "/" + arrMapOutlets.size());
                                }
                                if (finalI + 1 == arrMapOutlets.size() && count < arrMapOutlets.size()) {
                                /*if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();*/
                                    Utils.showAlertWithMessage(getActivity(), (arrMapOutlets.size() - count) + " outlets are invalid");

                                }


//                                if (mapsAdapter != null && selCustomerTyepModel != null) {
//                                    mapsAdapter.addItem(selCustomerTyepModel);
//                                }
//                                Marker marker = map.addMarker(options.infoWindowAnchor(1, 1));
//                                marker.setTag(retailersModel);
////                                arrMapOutlets.get(finalI).setMarker(marker);
//
//                                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                                    @Override
//                                    public boolean onMarkerClick(Marker marker) {
//
//                                        if (marker.getTag() instanceof RetailersModel) {
//                                            RetailersModel retailersModel = (RetailersModel) marker.getTag();
//
//                                            InfoWindow.MarkerSpecification markerSpec =
//                                                    new InfoWindow.MarkerSpecification(20, 90);
//
//                                            selectedMarker = marker;
//
//                                            FormFragment fragment = new FormFragment();
//                                            Bundle args = new Bundle();
//                                            args.putParcelable("Outlet", retailersModel);
//                                            fragment.setArguments(args);
//                                            fragment.setClickListener(MapsFragment.this);
//
//                                            InfoWindow infoWindow = new InfoWindow(marker, markerSpec, fragment);
//                                            infoWindowManager.toggle(infoWindow, true);
//
//                                            selInfoWindow = infoWindow;
//                                        }
//
//                                        return true;
//                                    }
//                                });
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        });

        thread.start();

        arrData = (arrSelectedData != null && arrSelectedData.size() > 0) ? arrSelectedData : arrFilterBeats;

        if (arrData != null) {
            Collections.sort(arrData, new Comparator<CustomerTyepModel>() {
                public int compare(CustomerTyepModel c1, CustomerTyepModel c2) {
                    String s1 = c1.getCustomerName() == null ? "" : c1.getCustomerName();
                    String s2 = c2.getCustomerName() == null ? "" : c2.getCustomerName();
                    return s1.compareToIgnoreCase(s2);
                }
            });
        }


    }

    private JSONObject createJSON(Object object, String hubIds, String spokeIds, String flag) {
        JSONObject dataObj = null;
        try {
            dataObj = new JSONObject();
            dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
            dataObj.put("offset", "0");
            dataObj.put("offset_limit", "0");
            dataObj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
//            if (object instanceof JSONArray) {
            dataObj.put("flag", flag);
//            }
            dataObj.put("beat_id", object);
            dataObj.put("hub", hubIds);
            dataObj.put("spoke", spokeIds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataObj;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 11) {
                String ids = MyApplication.getInstance().getSelBeats();
                selDcId = MyApplication.getInstance().getDcId();
                selHubIds = MyApplication.getInstance().getSelHubIds();
                selBeatIds = MyApplication.getInstance().getSelBeatIds();
                selBeatNames = MyApplication.getInstance().getSelBeatNames();
                selHubNames = MyApplication.getInstance().getSelHubNames();
                selSpokeIds = MyApplication.getInstance().getSelSpokeIds();
                selSpokeNames = MyApplication.getInstance().getSelSpokeNames();

                if (thread != null) {
                    thread.interrupt();
                }

                if (TextUtils.isEmpty(selHubIds)) {
                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_HUB_EMPTY, true).apply();
                    Intent intent = new Intent(getActivity(), BeatFilterActivity.class);
                    intent.putExtra("FilterBeats", arrFilterBeats);
                    intent.putExtra("SelectedBeats", arrSelectedBeats);
                    intent.putExtra("selDcId", selDcId);
                    intent.putExtra("selHubIds", selHubIds);
                    intent.putExtra("selBeatIds", selBeatIds);
                    intent.putExtra("selBeatNames", selBeatNames);
                    intent.putExtra("selHubNames", selHubNames);
                    intent.putExtra("selSpokeNames", selSpokeNames);
                    intent.putExtra("selSpokeIds", selSpokeIds);
                    intent.putExtra("beatsArray", selBeatIds);
                    intent.putExtra("showBeats", false);
                    startActivityForResult(intent, 11);
                } else {
                    Object object;
                    try {
                        if (TextUtils.isEmpty(ids)) {
                            beatsArray = new JSONArray();
                            beatsArray.put("-1");
                            object = beatsArray;
                        } else {
                            beatsArray = new JSONArray(ids);
                            object = beatsArray;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        beatsArray = new JSONArray();
                        object = beatsArray.put("-1");
                    }
                    getOutlets(createJSON(object, selHubIds, selSpokeIds, "1"), false);

                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()/* && requestType != PARSER_TYPE.OUTLETS_MAPS*/)
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.OUTLETS_MAPS) {
                        if (response instanceof OutletsModel.Retailers) {
                            OutletsModel.Retailers retailersModel = (OutletsModel.Retailers) response;
                            arrOutlets = retailersModel.getArrRetailers();

                            arrMapOutlets = (ArrayList<OutletsModel>) arrOutlets.clone();

                            OutletsAdapter adapter = new OutletsAdapter(getActivity(), R.layout.row_area_name_number, arrMapOutlets);
                            actSearch.setAdapter(adapter);

                            if (arrMapOutlets.size() == 0) {
                                progressBar.setProgress(0);
                                tvProgress.setText("0/0");
                            }
                            loadMap();

                        } else {
                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                        }
                    } else if (requestType == PARSER_TYPE.UPDATE_BEAT) {
                        if (response instanceof CustomerTyepModel) {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            if (selInfoWindow != null) {
                                infoWindowManager.hide(selInfoWindow);
                            }
                            selectedMarker.remove();

                            Predicate<OutletsModel> isCheckedData = new Predicate<OutletsModel>() {
                                @Override
                                public boolean apply(OutletsModel retailersModel) {
                                    return retailersModel.getBeatId().equalsIgnoreCase(selBeatId);
                                }
                            };

                            if (arrMapOutlets.contains(selRetailerModel)) {
                                arrMapOutlets.remove(selRetailerModel);
                                count--;
                            }

                            ArrayList<String> checkedFilters = filterNames(arrMapOutlets, isCheckedData);

                            if (checkedFilters != null && checkedFilters.size() > 0) {

                                selRetailerModel.setBeatId(selBeatId);
                                selRetailerModel.setBeatName(selBeatName);
                                arrMapOutlets.add(selRetailerModel);
                                count++;

                                int color = 0;
                                if (hashMap.get(selBeatId) != null && hashMap.get(selBeatId).size() > 0) {
                                    color = hashMap.get(selBeatId).get(0).getColor();
                                    selRetailerModel.setColor(color);
                                }

                                Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_pin);

                                Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
                                Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, color);

                                double lat = 0.0, lon = 0.0;
                                try {
                                    lat = Double.parseDouble(selRetailerModel.getLatitude());
                                    lon = Double.parseDouble(selRetailerModel.getLongitude());
                                } catch (Exception e) {
                                    lat = 0.0;
                                    lon = 0.0;
                                }

                                if (lat != 0.0 && lon != 0.0) {
                                    currentLat = lat;
                                    currentLong = lon;
                                }

                                options.position(new LatLng(lat, lon)).title(selRetailerModel.getCompany()).
                                        icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap));
                                Marker marker = map.addMarker(options);
                                marker.setTag(selRetailerModel);

                            }

                            OutletsAdapter adapter = new OutletsAdapter(getActivity(), R.layout.row_area_name_number, arrMapOutlets);
                            actSearch.setAdapter(adapter);

                            float size = arrMapOutlets.size();
                            float fVal = size / size;
                            float progressValue = fVal * 100;
                            if (size == 0) {
                                progressBar.setProgress(0);
                                tvProgress.setText("0/0");
                            } else {
                                progressBar.setProgress((int) progressValue);
                                if (count > arrMapOutlets.size())
                                    count = arrMapOutlets.size();
                                tvProgress.setText(count + "/" + arrMapOutlets.size());
                            }

                        }
                    } else if (requestType == PARSER_TYPE.GET_BEATS) {
                        if (response instanceof ArrayList) {
                            arrBeats = (ArrayList<CustomerTyepModel>) response;
                            if (arrBeats != null) {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                BeatListFragment beatListFragment = BeatListFragment.newInstance(arrBeats);
                                beatListFragment.setCancelable(true);
                                beatListFragment.setConfirmListener(okListener);
                                beatListFragment.setConfirmListener(okListener);
                                beatListFragment.show(fm, "beat_fragment");
                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void updateBeat() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("customer_id", selRetailerModel.getLegalEntityId());
                jsonObject.put("beat_id", selBeatId);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateBeatURL, map, MapsFragment.this, MapsFragment.this, PARSER_TYPE.UPDATE_BEAT);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.updateBeatURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void getBeats() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                jsonObject.put("hub", mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getbeatsURL, map, MapsFragment.this, MapsFragment.this, PARSER_TYPE.GET_BEATS);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getbeatsURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void showBeatUpdateDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage(getString(R.string.please_update_beat));
        dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getBeats();
            }
        });
        dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            this.map = googleMap;
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestLocationPermission();
                return;
            }

            try {
                mapView = mapInfoWindowFragment.getMapview();
                if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
                    // Get the button view
                    View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    // and next place it, on top right (as Google Maps app)
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    // position on top bottom
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            map.setMyLocationEnabled(true);

            setMap();
        }
    }

    private void requestLocationPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
        // END_INCLUDE(camera_permission_request)
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            Log.e("", "Received response for Location permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Location permission has been granted
                Log.e("", "Locations permission has now been granted.");

            } else {
                Log.e("", "Location permission was NOT granted.");
//                Snackbar.make(findViewById(R.id.button), R.string.permissions_not_granted,
//                        Snackbar.LENGTH_SHORT).show();

            }
            // END_INCLUDE(permission_result)

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onClicked(Object object) {
        if (object instanceof OutletsModel) {
            selRetailerModel = (OutletsModel) object;
            prevBeatId = selRetailerModel.getBeatId();
            actSearch.setText("");
            getBeats();
        }
    }

    @Override
    public void onClose() {
        if (selInfoWindow != null) {
            infoWindowManager.hide(selInfoWindow);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTag() instanceof OutletsModel) {
            OutletsModel outletsModel = (OutletsModel) marker.getTag();

            final InfoWindow.MarkerSpecification markerSpec =
                    new InfoWindow.MarkerSpecification(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            FormFragment fragment = new FormFragment();
            Bundle args = new Bundle();
            args.putParcelable("Outlet", outletsModel);
            fragment.setArguments(args);
            fragment.setClickListener(MapsFragment.this);

            final InfoWindow infoWindow = new InfoWindow(marker, markerSpec, fragment);
            infoWindowManager.toggle(infoWindow, true);
        }

        return true;
    }

    public void prepareMessage(Message m, OutletsModel _data) {
        Bundle b = new Bundle();
        b.putParcelable("data", _data);
        m.setData(b);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (thread != null)
            thread.interrupt();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (thread != null)
            thread.interrupt();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (thread != null)
                thread.interrupt();
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }
}