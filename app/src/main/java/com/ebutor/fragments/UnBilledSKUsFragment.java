package com.ebutor.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.FFDashboardActivity;
import com.ebutor.FilterActivity;
import com.ebutor.MyApplication;
import com.ebutor.ProductDetailsActivity;
import com.ebutor.R;
import com.ebutor.adapters.ProductsExpandableAdapterNew;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.SimpleTagImageView;
import com.ebutor.customview.expandable.library.AbstractSlideExpandableListAdapter;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.interfaces.ProductSKUClickListener;
import com.ebutor.models.AddCart1Response;
import com.ebutor.models.CartModel;
import com.ebutor.models.CashBackDetailsModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.NewPacksModel;
import com.ebutor.models.ProductSlabData;
import com.ebutor.models.ProductsResponse;
import com.ebutor.models.SortModel;
import com.ebutor.models.TabModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.models.TopBrandsModel;
import com.ebutor.models.WishListModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by 300024 on 3/20/2016.
 */
public class UnBilledSKUsFragment extends Fragment implements ProductSKUClickListener, ProductsExpandableAdapterNew.ProductImageClickListener, VolleyHandler<Object>, Response.ErrorListener {

    ProductsExpandableAdapterNew expandableAdapter;
    ArrayList<TestProductModel> productsArrayList;
    MenuItem item;
    ViewGroup mContainer;
    ArrayList<NewPacksModel> arrSlabRates;
    JSONArray variantsJSONArray = new JSONArray();
    DisplayImageOptions options;
    private CartModel cartModel;
    private Dialog dialog, progressDialog;
    private ActionSlideExpandableListView actionSlideExpandableListView;
    private View expandedItemView;
    private int expandedPosition = -1;
    private int cartCount = 0;
    private int currentOffset = 0;
    private int totalItemsCount = 0;
    private int offsetLimit = Utils.offsetLimit;// you can change this acg to requirement
    private String requestTypeTemp = "", sortString = "", marginToApply, unitPriceToApply;
    private TestProductModel selectedProductModel;
    private NewPacksModel selectedPackModel;
    private String listType = "list";
    private ProductsResponse productsResponse;
    private SharedPreferences mSharedPreferences;
    private TextView tvSort, tvFilter, tvThumbnail, tvEcashDesc;
    private View offerDescView, offerEcashView;
    private Button btnOfferImage;
    private View tvNoItems;
    private String filterString, unAvailableProducts, allProductIds;
    private JSONObject inputJson;
    private String buyerListingId = "";
    private boolean isShoppingList = false, isFF = false, isSort = false,isSalesAgent = false;
    private String selectedProductId, selSortId = "65001", productId;
    private double totalPrice;
    private ArrayList<WishListModel> wishListArray;
    private TextView tvBadge;
    private String type;
    private String businessFilter = "", productFilter = "";
    private boolean isLoadMore, isFreebie = false, selPackIsSlab = false;
    private LinearLayout llCashBack, llFooter, llFooterView, slabsLayout, variantsLayout1, variantsLayout2, innerVariantsLayout1, innerVariantsLayout2;
    private FrameLayout flMain;
    private Tracker mTracker;
    private TextView tvAlertMsg;
    private RelativeLayout rlAlert;
    private String variant_1_name, variant_2_name, variant_3_name;
    private ArrayList<String> arrVariant2;
    private String requestTypeAlert = "", selWishListId = "";
    WishListFragment.OkListener confirmListener = new WishListFragment.OkListener() {
        @Override
        public void onOkClicked(String wishListId) {
            selWishListId = wishListId;
            addProductToWishList(wishListId);
        }
    };
    private TextView tvFreebieDesc;
    private ProgressBar progress;
    private TextView tvNoPacks;
    private ArrayList<ProductSlabData> arrFreebieSlabData;
    private int totalQty = 0, selPackBlockedQty = 0, selPackPromotionId = 0;
    private int freebieMpq;
    private ArrayList<SortModel> arrSort;
    private String freebieProductId, freebieQty, freebieDesc;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private LoadType loadType;
    private String flag = "", flagSort = "asc";
    SortFragment.OnSelListener okListener = new SortFragment.OnSelListener() {

        @Override
        public void onSelected(String sortId) {

            int syncTimeInterval = mSharedPreferences.getInt(ConstantValues.KEY_SYNC_INTERVAL, ConstantValues.KEY_SYNC_INTERVAL_DURATION);
            String past = Utils.getPastDateTime(syncTimeInterval);

            switch (sortId) {
                case "1":
                    flag = "1"; // for brands
                    flagSort = "asc"; // for asc
                    if (DBHelper.getInstance().getBrandCount(past) > 0) {
                        bindListData(true, "Brand", flagSort);
                    } else {
                        getTopBrands();
                    }
                    break;
                case "2":
                    flag = "1"; // for brands
                    flagSort = "desc"; // for desc
                    if (DBHelper.getInstance().getBrandCount(past) > 0) {
                        bindListData(true, "Brand", flagSort);
                    } else {
                        getTopBrands();
                    }
                    break;
                case "3":
                    flagSort = "asc"; // for asc
                    flag = "3"; // for category
                    if (DBHelper.getInstance().getCategoryCount(past) > 0) {
                        bindListData(true, "Category", flagSort);
                    } else {
                        getTopBrands();
                    }
                    break;
                case "4":
                    flag = "3"; // for category
                    flagSort = "desc"; // for desc
                    if (DBHelper.getInstance().getCategoryCount(past) > 0) {
                        bindListData(true, "Category", flagSort);
                    } else {
                        getTopBrands();
                    }
                    break;
                case "5":
                    flag = "2"; // for manufacturer
                    flagSort = "asc"; // for asc
                    if (DBHelper.getInstance().getManufacturerCOunt(past) > 0) {
                        bindListData(true, "Manufacturer", flagSort);
                    } else {
                        getTopBrands();
                    }
                    break;
                case "6":
                    flag = "2"; // for manufacturer
                    flagSort = "desc"; // for desc
                    if (DBHelper.getInstance().getManufacturerCOunt(past) > 0) {
                        bindListData(true, "Manufacturer", flagSort);
                    } else {
                        getTopBrands();
                    }
                    break;
            }
        }
    };
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isClear = false;
    private String packType = "", prevClickedProductId = "";
    private Button rupee;
    View.OnClickListener onVariantClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            Toast.makeText(getActivity(), "Variant Clicked", Toast.LENGTH_SHORT).show();
            marginToApply = "0";
            unitPriceToApply = "0";
            if (v != null && v.getId() == R.id.id_variant_one) {
                int count = variantsLayout1.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout1.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                Object object = v.getTag();
                if (object != null && object instanceof String) {
                    variant_2_name = (String) object;
                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    if (expandedItemView != null) {
                        final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                        ll.removeAllViews();

                        innerVariantsLayout2 = (LinearLayout) expandedItemView.findViewById(R.id.inner_variants_2);
                        innerVariantsLayout2.removeAllViews();

                        TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(selectedProductId, variant_1_name, variant_2_name, "");
                        if (testProductModel != null) {
//                        View childItemView = actionSlideExpandableListView.getChildAt(expandedPosition);

                            View childItemView = getChildItemView(expandedPosition);
                            if (childItemView == null) {
                                Toast.makeText(getActivity(), R.string.oops_try_again, Toast.LENGTH_SHORT).show();
                                return;
                            }

                            TextView tvProduct = (TextView) childItemView.findViewById(R.id.tvProductName);
                            TextView tvMrp = (TextView) childItemView.findViewById(R.id.tvProductMRP);
                            SimpleTagImageView ivProduct = (SimpleTagImageView) childItemView.findViewById(R.id.productImage);
                            tvProduct.setText(testProductModel.getProductTitle());
                            packType = testProductModel.getPackType();
                            setOfferImageBackground(testProductModel.getPackType(), childItemView);
                            double mrp = 0;
                            try {
                                mrp = Double.parseDouble(testProductModel.getMrp());
                            } catch (Exception e) {

                            }
                            final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                            setMRP(sb, tvMrp);
                            ImageLoader.getInstance().displayImage(testProductModel.getThumbnailImage(), ivProduct, options, animateFirstListener);
                        }

                        String variant2list = DBHelper.getInstance().getVariant3List(selectedProductId, variant_1_name, variant_2_name);
                        if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                            if (variant2list != null && variant2list.length() > 0)
                                arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));
                            if (arrVariant2 != null && arrVariant2.size() > 0) {
                                View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                                variantsLayout2 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout2 != null) {
                                    variantsLayout2.removeAllViews();

                                    int selectedPos = 0;
                                    ArrayList<String> arrTemp = new ArrayList<>();
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "", color = "#FFFFFF";
                                        if (arrVariant2.get(i).split(":").length > 0) {
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        }
                                        if (arrVariant2.get(i).split(":").length > 1) {
                                            color = arrVariant2.get(i).split(":")[1];
                                        }
                                        if (!arrTemp.contains(childVariant)) {
                                            arrTemp.add(childVariant);
                                            LinearLayout childVariantLabel = createChildVariant(childVariant, color, R.id.id_variant_two);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout2.addView(childVariantLabel);
                                        }

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout2.getChildCount() > 0) {
                                        variantsLayout2.getChildAt(selectedPos).performClick();
                                        variantsLayout2.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout2.addView(innerVariantsView);

                                }
                            } else {
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //create packs
                            if (testProductModel != null) {
                                productId = testProductModel.getProductId();
                                selectedProductModel = testProductModel;
                                LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                                slabRatesLayout.removeAllViews();
                                TextView tvEsp = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvESP);
                                TextView tvInv = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvInv);
                                TextView tvCfc = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvcfc);
                                ImageView ivCashback = (ImageView) getChildItemView(expandedPosition).findViewById(R.id.iv_cashback);
                                if (testProductModel.isCashback()) {
                                    ivCashback.setVisibility(View.VISIBLE);
                                } else {
                                    ivCashback.setVisibility(View.GONE);
                                }
                                if (tvEsp != null)
                                    tvEsp.setText("");
                                if (tvInv != null)
                                    tvInv.setVisibility(View.GONE);
                                if (tvCfc != null)
                                    tvCfc.setVisibility(View.GONE);
                                getSlabRates(productId);
                            }
                        }
                    }
                }
            } else if (v.getId() == R.id.id_variant_two) {
                //todo
                int count = variantsLayout2.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout2.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                if (v.getTag() != null)
                    variant_3_name = (String) v.getTag();
                if (expandedItemView != null) {
                    final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                    ll.removeAllViews();
                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(selectedProductId, variant_1_name, variant_2_name, variant_3_name);
                    if (testProductModel != null) {
//                    View childItemView = actionSlideExpandableListView.getChildAt(expandedPosition);
                        View childItemView = getChildItemView(expandedPosition);
                        TextView tvProduct = (TextView) childItemView.findViewById(R.id.tvProductName);
                        TextView tvMrp = (TextView) childItemView.findViewById(R.id.tvProductMRP);
                        SimpleTagImageView ivProduct = (SimpleTagImageView) childItemView.findViewById(R.id.productImage);
                        tvProduct.setText(testProductModel.getProductTitle());
                        packType = testProductModel.getPackType();
                        setOfferImageBackground(testProductModel.getPackType(), childItemView);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(testProductModel.getMrp());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb, tvMrp);
                        ImageLoader.getInstance().displayImage(testProductModel.getThumbnailImage(), ivProduct, options, animateFirstListener);
                        productId = testProductModel.getProductId();
                        selectedProductModel = testProductModel;
                        LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                        slabRatesLayout.removeAllViews();
                        TextView tvEsp = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvESP);
                        TextView tvInv = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvInv);
                        TextView tvCfc = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvcfc);
                        ImageView ivCashback = (ImageView) getChildItemView(expandedPosition).findViewById(R.id.iv_cashback);
                        if (testProductModel.isCashback()) {
                            ivCashback.setVisibility(View.VISIBLE);
                        } else {
                            ivCashback.setVisibility(View.GONE);
                        }
                        if (tvEsp != null)
                            tvEsp.setText("");
                        if (tvInv != null)
                            tvInv.setVisibility(View.GONE);
                        if (tvCfc != null)
                            tvCfc.setVisibility(View.GONE);
                        getSlabRates(productId);

                    }
                }
            } else if (v.getId() == R.id.id_slab_qty) {
                if (v.getTag() != null) {
                    NewPacksModel newPacksModel = (NewPacksModel) v.getTag();
                    selectedPackModel = newPacksModel;
                    int count = slabsLayout.getChildCount();
                    for (int i = 0; i < count; i++) {
                        View btnView = slabsLayout.getChildAt(i);
                        btnView.setSelected(false);
                        selectedPackModel.setSelected(false);
                    }
                    v.setSelected(true);
                    selectedPackModel.setSelected(true);

                    TextView tvEsp = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvESP);
                    TextView tvInv = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvInv);
                    TextView tvCfc = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvcfc);
                    if (isFF) {
                        tvInv.setVisibility(View.VISIBLE);
                        tvCfc.setVisibility(View.VISIBLE);
                    } else {
                        tvInv.setVisibility(View.GONE);
                        tvCfc.setVisibility(View.GONE);
                    }
                    if (tvEsp != null) {

                        double ptr = 0;
                        try {
                            ptr = Double.parseDouble(newPacksModel.getPtr());
                        } catch (Exception e) {

                        }
                        if (ptr > 0) {
                            final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getPtr() == null ? getResources().getString(R.string.ptr) + " : " : getResources().getString(R.string.ptr) + " : " + String.format(Locale.CANADA, "%.2f", ptr));
                            setMRP(sb, tvEsp);

                        } else {
                            tvEsp.setText("");
                        }
                    }
                    if (tvInv != null) {

                        int inv = 0;
                        try {
                            inv = Integer.parseInt(newPacksModel.getStock());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getStock() == null ? getResources().getString(R.string.inv) + " : " : getResources().getString(R.string.inv) + " : " + inv);
                        setMRP(sb, tvInv);
                    }

                    if (tvCfc != null) {

                        int cfc = 0;
                        try {
                            cfc = Integer.parseInt(newPacksModel.getCfc());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getCfc() == null ? getResources().getString(R.string.cfc) + " : " : getResources().getString(R.string.cfc) + " : " + cfc);
                        setMRP(sb, tvCfc);
                    }
                    if (newPacksModel != null && expandedItemView != null) {
                        final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                        ll.removeAllViews();
                        createPacksForVariant(newPacksModel, ll, expandedPosition);
                    }
                }
            }
        }
    };

    public static UnBilledSKUsFragment newInstance() {
        UnBilledSKUsFragment frag = new UnBilledSKUsFragment();
        return frag;
    }

    public void getTopBrands() {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestTypeAlert = "GetTopBrands";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject detailsObj = new JSONObject();
                detailsObj.put("flag", flag);
                detailsObj.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("offset", "0");
                detailsObj.put("offset_limit", "0");

                map.put("data", detailsObj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.homePageBannersURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.GET_TOP_BRANDS);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.homePageBannersURL);
            if (dialog != null)
                dialog.show();
        } else {
            requestTypeAlert = "GetTopBrands";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }

    }

    private View getChildItemView(int index) {
        View v = actionSlideExpandableListView.getChildAt(index -
                actionSlideExpandableListView.getFirstVisiblePosition());

        return v;
    }

    private void setMRP(SpannableStringBuilder sb, TextView textView) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
//        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 3, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public void setOfferImageBackground(String packType, View view) {

        if (view == null) {
            return;
        }
        Button ivOffer = (Button) view.findViewById(R.id.iv_offer);

        switch (packType) {
            case "regular":
            case "REGULAR":
                ivOffer.setVisibility(View.GONE);
                break;
            case "cpoffer":
            case "CPOFFER":
            case "CP Inside":
            case "CP Outside":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_green);
                ivOffer.setText("CP Offer");
                break;
            case "freebie":
            case "FREEBIE":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_red);
                ivOffer.setText("Freebie");
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fast_moving, container, false);
        setHasOptionsMenu(true);
        mContainer = container;
//        requestType = getArguments().getString("Type");
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TABS_PROGRESS);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        actionSlideExpandableListView = (ActionSlideExpandableListView) view.findViewById(R.id.list);
        tvNoItems = (View) view.findViewById(R.id.tvNoItems);
        llFooter = (LinearLayout) view.findViewById(R.id.llLoadMore);
        llFooterView = (LinearLayout) view.findViewById(R.id.ll_footer_view);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        flMain = (FrameLayout) view.findViewById(R.id.fl_main);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        tvSort = (TextView) view.findViewById(R.id.tv_sort);
        tvFilter = (TextView) view.findViewById(R.id.tv_filter);
        tvThumbnail = (TextView) view.findViewById(R.id.tv_thumbnail);

        arrSort = new ArrayList<>();

        SortModel sortModel = new SortModel();
        sortModel.setSortId("1");
        sortModel.setName("By Brand A - Z");
        arrSort.add(sortModel);

        sortModel = new SortModel();
        sortModel.setSortId("2");
        sortModel.setName("By Brand Z - A");
        arrSort.add(sortModel);

        sortModel = new SortModel();
        sortModel.setSortId("3");
        sortModel.setName("By Category A - Z");
        arrSort.add(sortModel);

        sortModel = new SortModel();
        sortModel.setSortId("4");
        sortModel.setName("By Category Z - A");
        arrSort.add(sortModel);

        sortModel = new SortModel();
        sortModel.setSortId("5");
        sortModel.setName("By Manufacturer A - Z");
        arrSort.add(sortModel);

        sortModel = new SortModel();
        sortModel.setSortId("6");
        sortModel.setName("By Manufacturer Z - A");
        arrSort.add(sortModel);

        productsArrayList = new ArrayList<>();
        wishListArray = new ArrayList<WishListModel>();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        isSalesAgent = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_SALES_AGENT, false);

        MyApplication application = (MyApplication) getActivity().getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        expandableAdapter = new ProductsExpandableAdapterNew(getActivity(), productsArrayList, actionSlideExpandableListView);
        expandableAdapter.setProductSKUClickListener(UnBilledSKUsFragment.this);
        expandableAdapter.setProductImageClickListener(UnBilledSKUsFragment.this);
        actionSlideExpandableListView.setOnScrollListener(onScrollListener());
        actionSlideExpandableListView.setAdapter(expandableAdapter);

        actionSlideExpandableListView.setExpandCollapseListener(new AbstractSlideExpandableListAdapter.OnItemExpandCollapseListener() {
            @Override
            public void onExpand(View itemView, int position) {
                expandedItemView = itemView;
                expandedPosition = position;
                if (productsArrayList.size() > position) {
                    selectedProductId = productsArrayList.get(position).getProductId();

                    if (expandedItemView != null) {
                        LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                        if (ll != null)
                            ll.removeAllViews();

                        TestProductModel productModel = productsArrayList.get(expandedPosition);
                        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {

                            if (productModel.getSelectedVariant() != null) {
                                OnProductSKUClicked(productModel.getSelectedVariant(), expandedPosition, productModel.isChild());
                            }
                        }
                    }

                }

            }

            @Override
            public void onCollapse(View itemView, int position) {


                expandedPosition = -1;
                expandedItemView = null;
            }
        });

        getUnBilledSKUs(false);
//        getFilterData();
//        getSortData();

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestTypeAlert) {
                        case "WishList":
                            getWishList();
                            break;
                        case "AddCart":
                            updateToCart(selectedProductModel);
                            break;
                        case "FilterData":
                            getFilterData();
                            break;
                        case "ApplyFilter":
                            if (inputJson != null)
                                applyFilter();
                            break;
                        case "AddWishList":
                            addProductToWishList(selWishListId);
                            break;
                        default:
                            getFilterData();
                            getUnBilledSKUs(false);
                            break;
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLoadMore = false;
                requestTypeTemp = "";
                showSortDialog();
            }
        });

        tvThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSlideExpandableListView.collapse();
                expandedPosition = -1;

                switch (listType) {
                    case "list":
                        listType = "list1";
                        expandableAdapter.changeViewType(2);
                        expandableAdapter.notifyDataSetChanged();
                        break;
                    case "list1":
                        listType = "list";
                        expandableAdapter.changeViewType(1);
                        expandableAdapter.notifyDataSetChanged();
                        break;
                    /*case "grid":
                        listType = "list";
                        break;*/
                }
            }
        });

        tvFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (requestTypeTemp != null && requestTypeTemp.equalsIgnoreCase("Filter")) {
                    requestTypeTemp = type;
                }

                isLoadMore = false;

                getFilterData();

            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSort = false;
                selSortId = "";
                actionSlideExpandableListView.collapse();
                getUnBilledSKUs(true);
            }
        });

    }

    private void showSortDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        SortFragment sortDialog = SortFragment.newInstance(arrSort);
        sortDialog.setCancelable(true);
        sortDialog.setConfirmListener(okListener);
        sortDialog.show(fm, "sort_fragment");
    }

    private void loadMoreFastMoving() {
        llFooter.setVisibility(View.VISIBLE);
        isLoadMore = true;

        if (requestTypeTemp.equalsIgnoreCase("Filter")) {
            if (inputJson != null) {
                filterLoadMore();
            } else {
                isLoadMore = false;
                llFooter.setVisibility(View.GONE);
            }
        } else {
//            getHighMarginData(false);
//            if (TextUtils.isEmpty(selSortId))
            loadType = LoadType.LOAD_MORE;
//            else
//                loadType = LoadType.SORT;
//            requestTypeAlert = requestType;
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);

            HashMap<String, String> map = new HashMap<>();
            ArrayList<TabModel> arrTabs = MyApplication.getInstance().getArrTabs();
            try {
                JSONObject obj = new JSONObject();

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date = sdf.format(cal.getTime());

                obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                obj.put("id", "");
                obj.put("flag", "0");
                obj.put("is_billed", "0");
                obj.put("beat_id", mSharedPreferences.getString(ConstantValues.KEY_BEAT_ID, ""));
                obj.put("start_date", date);
                obj.put("end_date", date);
                obj.put("sort_id", selSortId);
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("offset", String.valueOf(currentOffset));
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("offset_limit", String.valueOf(offsetLimit));

                map.put("data", obj.toString());
            } catch (Exception e) {

                e.printStackTrace();
            }

            VolleyBackgroundTask highMarginRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.unBilledSKUsURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.UNBILLED_SKUS);
            highMarginRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyApplication.getInstance().addToRequestQueue(highMarginRequest, AppURL.unBilledSKUsURL);
            if (dialog != null && !isLoadMore)
                dialog.show();

        }
    }

    private void getSortData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestTypeAlert = "SortData";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                map.put("data", obj.toString());

                VolleyBackgroundTask sortDataRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.sortingURL, map,
                        UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.GET_SORT_DATA);
                sortDataRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(sortDataRequest, AppURL.sortingURL);
//                if (dialog != null)
//                    dialog.show();

            } catch (Exception e) {

            }
        } else {
            requestTypeAlert = "SortData";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    public void getUnBilledSKUs(boolean isSwipeDown) {
        loadType = LoadType.DEFAULT;
        currentOffset = 0;
        rlAlert.setVisibility(View.GONE);
        flMain.setVisibility(View.VISIBLE);

        HashMap<String, String> map = new HashMap<>();
        ArrayList<TabModel> arrTabs = MyApplication.getInstance().getArrTabs();
        try {
            JSONObject obj = new JSONObject();

            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sdf.format(cal.getTime());

            obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
            obj.put("id", "");
            obj.put("flag", "0");
            obj.put("is_billed", "0");
            obj.put("beat_id", mSharedPreferences.getString(ConstantValues.KEY_BEAT_ID, ""));
            obj.put("start_date", date);
            obj.put("end_date", date);
            obj.put("sort_id", selSortId);
            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
            obj.put("offset", String.valueOf(currentOffset));
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("offset_limit", String.valueOf(offsetLimit));

            map.put("data", obj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        VolleyBackgroundTask highMarginRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.unBilledSKUsURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.UNBILLED_SKUS);
        highMarginRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyApplication.getInstance().addToRequestQueue(highMarginRequest, AppURL.unBilledSKUsURL);
        if (dialog != null && !isSwipeDown && !isLoadMore && isSort)
            dialog.show();
    }

    private void getFilterData() {

        requestTypeAlert = "FilterData";
        rlAlert.setVisibility(View.GONE);
        flMain.setVisibility(View.VISIBLE);

        HashMap<String, String> map = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
            jsonObject.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
            jsonObject.put("flag", "");
            jsonObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            jsonObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
            jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

//            if (requestType.equalsIgnoreCase("Search")) {
//                jsonObject.put("search", getActivity().getIntent().getStringExtra("Key"));
//            } else {
//                jsonObject.put("search", "");
//            }
            map.put("data", jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        VolleyBackgroundTask filterDataReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.filterDataURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.GET_FILTER_DATA_NEW);
        filterDataReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(filterDataReq, AppURL.filterDataURL);
        if (dialog != null)
            dialog.show();
    }

    public void addProductToWishList(final String wishListId) {
        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                requestTypeAlert = "AddWishList";
                rlAlert.setVisibility(View.GONE);
                flMain.setVisibility(View.VISIBLE);

                HashMap<String, String> map = new HashMap<>();
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("flag", "1");
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    obj.put("buyer_listing_id", wishListId);
                    obj.put("product_id", selectedProductId);

                    map.put("data", obj.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                VolleyBackgroundTask addWishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productListOperationsURL, map,
                        UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.ADDTO_WISHLIST);
                addWishListRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(addWishListRequest, AppURL.productListOperationsURL);
                if (dialog != null)
                    dialog.show();

            } else {
                requestTypeAlert = "AddWishList";
                rlAlert.setVisibility(View.VISIBLE);
                flMain.setVisibility(View.GONE);
            }

        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_addto_wishlist), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void OnProductSKUClicked(Object object, final long position, boolean isChild) {
       /* if (expandedItemView == null)
            return;
        if (expandedPosition == -1)
            return;*/
        if (object == null)
            return;

        if (position != expandedPosition) {
            View view = actionSlideExpandableListView.getViewByPosition((int) position, actionSlideExpandableListView);
            if (view != null) {
                View expandToggle = view.findViewById(R.id.expandable_toggle_button);
                if (expandToggle != null) {
                    expandToggle.performClick();
                    OnProductSKUClicked(object, position, isChild);
                }
            }
            return;
        }

        if (object instanceof String) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            variant_1_name = (String) object;
            marginToApply = "0";
            unitPriceToApply = "0";
            if ((int) position != expandedPosition)
                return;
            if (offerDescView != null)
                offerDescView.setVisibility(View.GONE);

            if (expandedItemView != null) {
                final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                ll.removeAllViews();

                innerVariantsLayout1 = (LinearLayout) expandedItemView.findViewById(R.id.inner_variants);
                if (innerVariantsLayout1 != null) {
                    innerVariantsLayout1.removeAllViews();

                    if (isChild) {
                        TestProductModel testProductModel = DBHelper.getInstance().getProductById(selectedProductId, true);
                        if (testProductModel != null) {
                            productId = testProductModel.getProductId();
                            this.selectedProductModel = testProductModel;
                            LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            TextView tvEsp = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvESP);
                            TextView tvInv = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvInv);
                            TextView tvCfc = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvcfc);
                            ImageView ivCashback = (ImageView) getChildItemView(expandedPosition).findViewById(R.id.iv_cashback);
                            if (testProductModel.isCashback()) {
                                ivCashback.setVisibility(View.VISIBLE);
                            } else {
                                ivCashback.setVisibility(View.GONE);
                            }
                            if (tvEsp != null)
                                tvEsp.setText("");
                            if (tvInv != null)
                                tvInv.setVisibility(View.GONE);
                            if (tvCfc != null)
                                tvCfc.setVisibility(View.GONE);
                            getSlabRates(productId);
                        }
                    } else {
                        String variant2list = DBHelper.getInstance().getVariant2List(selectedProductId, variant_1_name);
                        if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                            if (variant2list != null && variant2list.length() > 0)
                                arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));
                            if (arrVariant2 != null && arrVariant2.size() > 0) {
                                View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                                variantsLayout1 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout1 != null) {
                                    variantsLayout1.removeAllViews();

                                    int selectedPos = 0;
                                    ArrayList<String> arrTemp = new ArrayList<>();
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "", color = "#FFFFFF";
                                        if (arrVariant2.get(i).split(":").length > 0) {
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        }
                                        if (arrVariant2.get(i).split(":").length > 1) {
                                            color = arrVariant2.get(i).split(":")[1];
                                        }
                                        if (!arrTemp.contains(childVariant)) {
                                            arrTemp.add(childVariant);
                                            LinearLayout childVariantLabel = createChildVariant(childVariant, color, R.id.id_variant_one);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout1.addView(childVariantLabel);
                                        }

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout1.getChildCount() > 0) {
                                        variantsLayout1.getChildAt(selectedPos).performClick();
                                        variantsLayout1.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout1.addView(innerVariantsView);

                                }
                            } else {
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //create packs
                            TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(selectedProductId, variant_1_name, "", "");
                            if (testProductModel != null) {
                                productId = testProductModel.getProductId();
                                selectedProductModel = testProductModel;
                                LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                                slabRatesLayout.removeAllViews();
                                TextView tvEsp = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvESP);
                                TextView tvInv = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvInv);
                                TextView tvCfc = (TextView) getChildItemView(expandedPosition).findViewById(R.id.tvcfc);
                                ImageView ivCashback = (ImageView) getChildItemView(expandedPosition).findViewById(R.id.iv_cashback);
                                if (testProductModel.isCashback()) {
                                    ivCashback.setVisibility(View.VISIBLE);
                                } else {
                                    ivCashback.setVisibility(View.GONE);
                                }
                                if (tvEsp != null)
                                    tvEsp.setText("");
                                if (tvInv != null)
                                    tvInv.setVisibility(View.GONE);
                                if (tvCfc != null)
                                    tvCfc.setVisibility(View.GONE);
                                getSlabRates(productId);
                            }
                        }
                    }
                }
            }
        }

    }

    private void getSlabRates(String productId) {
        try {
            if (!prevClickedProductId.equalsIgnoreCase(productId)) {
                prevClickedProductId = productId;
                JSONObject obj = new JSONObject();
                PARSER_TYPE parserType = PARSER_TYPE.SLAB_RATES;
                obj.put("product_id", productId);
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                //to cancel already running request(if running)
                MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.getSlabRatesURL);

                VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSlabRatesURL, map,
                        UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, parserType);
                productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getSlabRatesURL);
                if (expandedItemView != null) {
                    final FrameLayout fl = (FrameLayout) expandedItemView.findViewById(R.id.fl_expand_content);
                    progress = (ProgressBar) fl.findViewById(R.id.loading);
                    tvNoPacks = (TextView) fl.findViewById(R.id.tv_no_packs);
                    tvNoPacks.setVisibility(View.GONE);
                    progress.setVisibility(View.VISIBLE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private LinearLayout createChildVariant(String text, String color, int id) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        LinearLayout skuView = new LinearLayout(getActivity());
        skuView.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params1.setMargins(0, 0, 20, 0);
        skuView.setLayoutParams(params1);

        ImageView imageView = new ImageView(getActivity());
        Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_star);

        Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
        Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(color));
        imageView.setImageBitmap(mFinalBitmap);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params2.gravity = Gravity.CENTER_VERTICAL;
        params2.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(params2);

        TextView textView = new TextView(getActivity());
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.gravity = Gravity.CENTER_VERTICAL;
        params.setMargins(5, 0, 0, 0);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        skuView.setId(id);
        skuView.setTag(text);
        textView.setText(text == null ? "" : text.toLowerCase());

        skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
        skuView.setPadding(10, 5, 10, 5);
        skuView.addView(imageView);
        skuView.addView(textView);

        return skuView;
    }

    private void createPacksForVariant(final NewPacksModel skuPacksModel, final LinearLayout linearLayout, final long position) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (skuPacksModel != null) {
            View packView = inflater.inflate(R.layout.row_packs_header, null);

            TextView tvPackOf = (TextView) packView.findViewById(R.id.tvPackOf);
            TextView tvPackPrice = (TextView) packView.findViewById(R.id.tvPackPrice);
            TextView tvUnitPrice = (TextView) packView.findViewById(R.id.tvUnitPrice);
            TextView tvMargin = (TextView) packView.findViewById(R.id.tvMargin);

            tvPackOf.setText(String.valueOf(skuPacksModel.getPackSize() == null ? "--" : skuPacksModel.getPackSize()));

            double packPrice = 0;
            try {
                packPrice = (Double.parseDouble(skuPacksModel.getPackSize()) * Double.parseDouble(skuPacksModel.getUnitPrice()));
            } catch (Exception e) {
            }
            tvPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

            double unitPrice = 0;
            try {
                unitPrice = Double.parseDouble(skuPacksModel.getUnitPrice());
            } catch (Exception e) {
            }
            tvUnitPrice.setText(skuPacksModel.getUnitPrice() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", unitPrice));

            double margin = 0;
            try {
                margin = Double.parseDouble(skuPacksModel.getMargin());
            } catch (Exception e) {
            }
            tvMargin.setText(skuPacksModel.getMargin() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", margin));

            final TextView tvQty = (TextView) packView.findViewById(R.id.tvQuantity);
            tvQty.setText(skuPacksModel.getEsuQty() + "");
            ImageButton ibMinus = (ImageButton) packView.findViewById(R.id.ibMinus);
            ImageButton ibPlus = (ImageButton) packView.findViewById(R.id.ibPlus);

            ibPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_add_packs), Toast.LENGTH_LONG).show();
                        return;
                    }
//                    String availableQty = model.getAvailableQuantity();
                    String availableQty = "100"; //temp
                    if (availableQty != null && !TextUtils.isEmpty(availableQty)) {
                        int variantQuantity = 0;
                        try {
                            variantQuantity = Integer.parseInt(availableQty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (variantQuantity > 0) {
                            int qty = skuPacksModel.getQty();
                            int esuQty = skuPacksModel.getEsuQty();

                            int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                    0 : Integer.parseInt(skuPacksModel.getPackSize());
                            int slabEsu = skuPacksModel.getSlabEsu() == 0 ? 1 : skuPacksModel.getSlabEsu();
                            int updatedQty = qty + (packSize * slabEsu);
                            esuQty = esuQty + 1;
                            tvQty.setText("" + esuQty);
                            skuPacksModel.setQty(updatedQty);
                            skuPacksModel.setEsuQty(esuQty);

                            updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                        } else {
                            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.quantity_not_available));
                        }
                    }


                }
            });

            ibMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_add_packs), Toast.LENGTH_LONG).show();
                        return;
                    }
                    int qty = skuPacksModel.getQty();
                    int esuQty = skuPacksModel.getEsuQty();

                    if (esuQty <= 0) {
                        tvQty.setText("0");
                    } else {
                        int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                0 : Integer.parseInt(skuPacksModel.getPackSize());
                        int slabEsu = skuPacksModel.getSlabEsu() == 0 ? 1 : skuPacksModel.getSlabEsu();
                        int updatedQty = qty - (packSize * slabEsu);
                        esuQty = esuQty - 1;
                        if (esuQty < 0) {
                            tvQty.setText("0");
                        } else {
                            tvQty.setText("" + esuQty);
                        }
                        skuPacksModel.setQty(updatedQty);
                        skuPacksModel.setEsuQty(esuQty);
                    }
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                }
            });

            linearLayout.addView(packView);
        }

        View footerView = inflater.inflate(R.layout.row_packs_footer, null);
        linearLayout.addView(footerView);

        footerView.findViewById(R.id.tvAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_shop), Toast.LENGTH_LONG).show();
                    return;
                } else if (mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "").equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""))) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_select_retailer), Toast.LENGTH_SHORT).show();
                    return;
                }
                OnCartItemAdded(productsArrayList.get(expandedPosition));
            }
        });
        updateFooter(footerView, skuPacksModel);

    }

    private void updateFooter(View footerView, NewPacksModel newPacksModel) {
        if (footerView == null) {
            return;
        }
        if (newPacksModel == null)
            return;

        int totalQty = 0;
        marginToApply = "0";
        unitPriceToApply = "0";
        for (NewPacksModel skuPacksModel : arrSlabRates) {
            totalQty = totalQty + skuPacksModel.getQty();
        }

        for (NewPacksModel skuPacksmodel : arrSlabRates) {
            int packQty = Integer.parseInt(skuPacksmodel.getPackSize());
            int esu = skuPacksmodel.getSlabEsu();
            if (totalQty >= packQty * esu) {
                marginToApply = skuPacksmodel.getMargin();
                unitPriceToApply = skuPacksmodel.getUnitPrice();
                selPackBlockedQty = skuPacksmodel.getBlockedQty();
                selPackPromotionId = skuPacksmodel.getPrmtDetId();
                selPackIsSlab = skuPacksmodel.isSlab();
                selectedPackModel = skuPacksmodel;

                freebieProductId = skuPacksmodel.getFreebieProductId();
                freebieMpq = skuPacksmodel.getFreebieMpq();
                //                freebieQty = String.valueOf((totalQty / packQty) * skuPacksmodel.getFreebieQty());
                double _qty = 0;
                if (freebieMpq > 0) {
                    _qty = (int) (totalQty / freebieMpq) * skuPacksmodel.getFreebieQty();
                }
                freebieQty = String.valueOf((int) _qty);
            }
        }

        double unitPrice = TextUtils.isEmpty(unitPriceToApply) ? 0.00 : Double.parseDouble(unitPriceToApply);
        double margin = TextUtils.isEmpty(marginToApply) ? 0.00 : Double.parseDouble(marginToApply);

//            skuModel.setAppliedMargin(marginToApply);
//            skuModel.setAppliedMRP(unitPriceToApply);

        TextView tvTotalQty = (TextView) footerView.findViewById(R.id.tvTotalQty);
        TextView tvMargin = (TextView) footerView.findViewById(R.id.tvTotalMargin);
        TextView tvTotalPrice = (TextView) footerView.findViewById(R.id.tvTotalPrice);
        if (tvTotalQty != null) {
//                tvTotalQty.setText("TOTAL QTY\n" + totalQty);
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.qty) + "\n" + totalQty);
            setSpannedText(sb, tvTotalQty, 3);
        }

        if (tvMargin != null) {
//                tvMargin.setText("MARGIN\n" + String.format(Locale.CANADA, "%.2f", margin));
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.markup_margin) + "\n" + String.format(Locale.CANADA, "%.2f", margin) + "%");
            setSpannedText(sb, tvMargin, 8);
        }
        totalPrice = unitPrice * totalQty;
//        skuModel.setTotalPrice(String.format(Locale.CANADA, "%.2f", totalPrice));
        if (tvTotalPrice != null) {
//                tvTotalPrice.setText("TOTAL PRICE\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            setSpannedText(sb, tvTotalPrice, 11);

        }

        if (expandedItemView != null) {
            offerEcashView = expandedItemView.findViewById(R.id.ll_cashback);
            tvFreebieDesc = (TextView) expandedItemView.findViewById(R.id.tvFreebieDesc);
            offerDescView = expandedItemView.findViewById(R.id.ll_freebie_desc);
            btnOfferImage = (Button) expandedItemView.findViewById(R.id.iv_offer_desc);
            if (selectedPackModel != null && selectedPackModel.getFreebieDesc() != null &&
                    !TextUtils.isEmpty(selectedPackModel.getFreebieDesc()) && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("null") && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("0")) {
                tvFreebieDesc.setText(selectedPackModel.getFreebieDesc());
                offerDescView.setVisibility(View.VISIBLE);
                packType = selectedProductModel.getPackType();
                setOfferBanner(selectedProductModel.getPackType(), btnOfferImage);
            } else
                offerDescView.setVisibility(View.GONE);

            //&& !selectedPackModel.getCashBackDetailsModels().get(0).getCashback_description().equalsIgnoreCase("null") && !selectedPackModel.getCashBackDetailsModels().get(0).getCashback_description().equalsIgnoreCase("0")  )
            if (selectedPackModel != null && selectedPackModel.getArrCashBackDetails() != null && selectedPackModel.getArrCashBackDetails().size() > 0) {
                int count = selectedPackModel.getArrCashBackDetails().size();
                llCashBack = (LinearLayout) expandedItemView.findViewById(R.id.ll_cash_back);
                llCashBack.removeAllViews();

                for (int i = 0; i < count; i++) {
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    View v = inflater.inflate(R.layout.cashback_dashborad, null);
                    offerEcashView = v.findViewById(R.id.ll_cashback);
                    tvEcashDesc = (TextView) v.findViewById(R.id.tv_ecash);

                    rupee = (Button) v.findViewById(R.id.btn_rupee);

                    int cbktyp = selectedPackModel.getArrCashBackDetails().get(i).getCbk_source_type();
                    int bentype = selectedPackModel.getArrCashBackDetails().get(i).getBenificiary_type();
                    if (!isFF){
                        if (bentype == ConstantValues.CUSTOMER) {
                            tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                            tvEcashDesc.setBackgroundResource(R.drawable.cashback_bg_retailer);
                            tvEcashDesc.setTextColor(getResources().getColor(R.color.ecash_desc));
                            rupee.setBackgroundResource(R.drawable.ic_rupee);
                            offerEcashView.setVisibility(View.VISIBLE);
                            rupee.setVisibility(View.VISIBLE);
                            llCashBack.addView(v);
                        }
                    }else if (isFF && isSalesAgent){
                        if (bentype == ConstantValues.SALES_AGENT || bentype == ConstantValues.CUSTOMER){
                            tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                            tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                            tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                            rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                            offerEcashView.setVisibility(View.VISIBLE);
                            rupee.setVisibility(View.VISIBLE);
                            llCashBack.addView(v);
                        }
                    }else if (isFF){
                        if (bentype == ConstantValues.FF_ASSOCIATE || bentype == ConstantValues.FF_MANAGER || bentype == ConstantValues.CUSTOMER){
                            tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                            tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                            tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                            rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                            offerEcashView.setVisibility(View.VISIBLE);
                            rupee.setVisibility(View.VISIBLE);
                            llCashBack.addView(v);
                        }
                    }else {
                        tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                        tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                        tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                        rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                        offerEcashView.setVisibility(View.VISIBLE);
                        rupee.setVisibility(View.VISIBLE);
                        llCashBack.addView(v);
                    }

                   /* if (!isFF) {
                        if (bentype == ConstantValues.CUSTOMER) {
                            tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                            tvEcashDesc.setBackgroundResource(R.drawable.cashback_bg_retailer);
                            tvEcashDesc.setTextColor(getResources().getColor(R.color.ecash_desc));
                            rupee.setBackgroundResource(R.drawable.ic_rupee);
                            offerEcashView.setVisibility(View.VISIBLE);
                            rupee.setVisibility(View.VISIBLE);
                            llCashBack.addView(v);

                        }
                    } else {
                       // if (bentype == ConstantValues.CUSTOMER || bentype == ConstantValues.FF_MANAGER || bentype == ConstantValues.FF_ASSOCIATE) {
                            tvEcashDesc.setText(selectedPackModel.getArrCashBackDetails().get(i).getCashback_description());
                            tvEcashDesc.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.cashback_bg_retailer : R.drawable.cashback_bg_ff);
                            tvEcashDesc.setTextColor(bentype == ConstantValues.CUSTOMER ? getResources().getColor(R.color.ecash_desc) : getResources().getColor(R.color.cashback_ff_text));
                            rupee.setBackgroundResource(bentype == ConstantValues.CUSTOMER ? R.drawable.ic_rupee : R.drawable.ic_rupee_silver);
                            offerEcashView.setVisibility(View.VISIBLE);
                            rupee.setVisibility(View.VISIBLE);
                            llCashBack.addView(v);
                      //  }
                    }*/

                }

            } else {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View v = inflater.inflate(R.layout.cashback_dashborad, null);
                v.setVisibility(View.GONE);
                offerEcashView = v.findViewById(R.id.ll_cashback);
                offerEcashView.setVisibility(View.GONE);
            }

        }
    }

    public void setOfferBanner(String packType, View view) {

        if (view == null) {
            return;
        }
        Button ivOffer = (Button) view;

        switch (packType) {

            case "Consumer Pack":
            case "CONSUMER PACK":
            case "CP Inside":
            case "CP Outside":
            case "Consumer Pack Inside":
            case "Consumer Pack Outside":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("CP Offer");
                break;
            case "Freebie":
            case "FREEBIE":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Freebie");
                break;
            default:
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Offer");
                break;
        }
    }

    private void setSpannedText(SpannableStringBuilder sb, TextView textView, int start) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getActivity().getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new RelativeSizeSpan(1.2f), start, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(sb);
    }

    @Override
    public void OnCartItemAdded(Object object) {
        if (expandedItemView == null)
            return;
        if (expandedPosition == -1)
            return;
        if (object == null)
            return;

        if (object instanceof TestProductModel) {
            TestProductModel productModel = (TestProductModel) object;

            ArrayList<NewPacksModel> packs = arrSlabRates;
            totalQty = 0;
            if (packs != null && packs.size() > 0) {
                for (NewPacksModel packModel : packs) {
                    int qty = packModel.getQty();
                    totalQty += qty;
                }
            }

            if (totalQty <= 0) {
                // show alert
                Utils.showAlertDialog(getActivity(), getString(R.string.please_add_packs));
                return;
            }

            if (totalPrice <= 0) {
                Utils.showAlertDialog(getActivity(), getString(R.string.oops_try_again));
                return;
            }

            /*if (selPackIsSlab && selPackBlockedQty > 0 && totalQty > selPackBlockedQty) {
                Utils.showAlertDialog(getActivity(), getString(R.string.qty_grt_blck));
                return;
            }*/

            checkInventory(productId, totalQty);

        }
    }

    private void checkInventory(String productId, int quantity) {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestTypeAlert = "checkInventory";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("product_id", productId);
                obj.put("quantity", quantity);
                obj.put("is_slab", selPackIsSlab == true ? 1 : 0);
                obj.put("blocked_qty", selPackBlockedQty);
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCart1URL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.ADD_CART_1);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.addCart1URL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestTypeAlert = "GetWishList";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void OnItemOptionClicked(Object model, int type) {
        if (model != null && model instanceof TestProductModel) {
            final TestProductModel productModel = (TestProductModel) model;

            if (Networking.isNetworkAvailable(getActivity())) {

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("product_id", productModel.getProductId());
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("quantity", "0");
                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCart1URL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.CHECK_INVENTORY);
                    getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.addCart1URL);
                    progressDialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                showToast(getString(R.string.no_network_connection));
            }


//            if (!isShoppingList) {
//                // Wish list
//                this.selectedProductId = productModel.getProductId();
//                if (wishListArray.size() == 0) {
//                    getWishList();
//                } else {
//                    showWishListDialog();
//                }
//            }
        }
        return;

    }

    @Override
    public void onSKUClicked(String productId) {

    }

    private void getWishList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestTypeAlert = "GetWishList";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "1");
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.shoppinglistURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.WISHLIST);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.shoppinglistURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestTypeAlert = "GetWishList";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    private void updateCartCount() {
        int _tempCartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        this.cartCount = _tempCartCount;
        ((FFDashboardActivity) getActivity()).updateCart(cartCount);
        if (tvBadge != null) {
            tvBadge.setText(String.valueOf(this.cartCount));
        }

    }

    private void updateToCart(TestProductModel productModel) {
        variantsJSONArray = new JSONArray();
        if (Networking.isNetworkAvailable(getActivity())) {
            requestTypeAlert = "AddCart";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            try {
                JSONArray productsArray = new JSONArray();
                JSONObject productObj = new JSONObject();

                JSONObject dataObj = new JSONObject();
                JSONObject variantObj = new JSONObject();
                JSONArray variantArr = new JSONArray();

                ArrayList<NewPacksModel> packs = arrSlabRates;
                JSONArray packsArray = new JSONArray();
                int totalQty = 0;
                if (packs != null && packs.size() > 0) {
                    for (NewPacksModel packModel : packs) {
                        int qty = packModel.getQty();
//                                    if (qty > 0) {
                        JSONObject packObj = new JSONObject();
                        packObj.put("pack_qty", qty);
                        packsArray.put(packObj);
//                                    }
                        totalQty += qty;
                    }
                }

                if (totalQty > 0) {
                    variantObj.put("variant_id", productId);
                    variantObj.put("total_qty", totalQty);
                    variantObj.put("total_price", totalPrice);
                    variantObj.put("applied_margin", marginToApply);
                    variantObj.put("unit_price", unitPriceToApply);
                    variantObj.put("packs", packsArray);
                    variantArr.put(variantObj);
                    productObj.put("product_id", productId);
                    productObj.put("variants", variantArr);
                    productsArray.put(productObj);
                }

                if (productsArray.length() > 0) {
                    dataObj.put("products", productsArray);
                    dataObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    dataObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    dataObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                    Log.e("Cart Obj", dataObj.toString());

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", dataObj.toString());

                    VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCartURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.ADD_TO_CART);
                    viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.addCartURL);
                    if (dialog != null)
                        dialog.show();

                } else {
                    Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.please_add_packs));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            requestTypeAlert = "AddCart";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    public void showWishListDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        WishListFragment editNameDialog = WishListFragment.newInstance(wishListArray);
        editNameDialog.setCancelable(true);
        editNameDialog.setConfirmListener(confirmListener);
        editNameDialog.show(fm, "areas_fragment_dialog");
    }

    private void setSKUListData(ArrayList<TestProductModel> arrProducts) {

        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }

        if (arrProducts != null && arrProducts.size() > 0) {
            switch (loadType) {
                case APPLY_FILTER:
                case DEFAULT:
                case SWIPEDOWN:
                    productsArrayList.clear();
                    expandableAdapter.removeAllItems();
                    break;
                case LOAD_MORE:
                    break;
            }

            for (int i = 0; i < arrProducts.size(); i++) {
                expandableAdapter.addItem(arrProducts.get(i));
                if (!productsArrayList.contains(arrProducts.get(i)))
                    productsArrayList.add(arrProducts.get(i));
            }

            expandableAdapter.notifyDataSetChanged();

        } else {
            // temp list is null
            if (productsArrayList.size() < 1) {
                actionSlideExpandableListView.setVisibility(View.INVISIBLE);
                tvNoItems.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onResume() {
        super.onResume();
//        getFilterData();
        boolean isDayChanged = MyApplication.getInstance().isDayChanged();
        if (isDayChanged) {
            MyApplication.getInstance().setDayChanged(false);
            selSortId = "";
            productsArrayList = new ArrayList<>();
            actionSlideExpandableListView.collapse();
            getUnBilledSKUs(false);
        }
        updateCartCount();

        mTracker.setScreenName("High Margin Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());


    }

    private void applyFilter() {
        loadType = LoadType.DEFAULT;
        isLoadMore = false;
        currentOffset = 0;
        if (Networking.isNetworkAvailable(getActivity())) {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);

            HashMap<String, String> map = new HashMap<>();
            try {
                inputJson.put("offset", String.valueOf(currentOffset));
                inputJson.put("offset_limit", String.valueOf(offsetLimit));
                inputJson.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                inputJson.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                inputJson.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                inputJson.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                inputJson.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                map.put("data", inputJson.toString());
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception Filter", e.getMessage());
            }

            VolleyBackgroundTask applyFilterRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.applyFilterURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.APPLY_FILTER);
            applyFilterRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyApplication.getInstance().addToRequestQueue(applyFilterRequest, AppURL.applyFilterURL);
            if (dialog != null)
                dialog.show();

        } else {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    private LinearLayout createSlabRates(final String text, String slabColor, int slabEsu, int id) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        LinearLayout skuView = new LinearLayout(getActivity());
        skuView.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params1.setMargins(0, 0, 20, 0);
        skuView.setLayoutParams(params1);

        ImageView imageView = new ImageView(getActivity());
        Drawable sourceDrawable = getResources().getDrawable(R.drawable.ic_star);

        Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
        Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(slabColor));
        imageView.setImageBitmap(mFinalBitmap);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        params2.gravity = Gravity.CENTER_VERTICAL;
        params2.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(params2);

        TextView textView = new TextView(getActivity());
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.gravity = Gravity.CENTER_VERTICAL;

        textView.setTypeface(Typeface.DEFAULT_BOLD);
        params.setMargins(10, 0, 5, 0);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        skuView.setId(id);
        skuView.setTag(text);
        if (slabEsu > 1)
            textView.setText(text == null ? "" : text + " x " + slabEsu);
        else
            textView.setText(text == null ? "" : text);

        skuView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
        skuView.setPadding(10, 5, 10, 5);
        skuView.addView(imageView);
        skuView.addView(textView);

        return skuView;
    }

    public void filterLoadMore() {
        loadType = LoadType.LOAD_MORE;
        if (Networking.isNetworkAvailable(getActivity())) {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);

            HashMap<String, String> map = new HashMap<>();
            try {
                inputJson.put("offset", String.valueOf(currentOffset));
                inputJson.put("offset_limit", String.valueOf(offsetLimit));
                inputJson.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                inputJson.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                inputJson.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                inputJson.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                inputJson.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                if (inputJson != null && inputJson.length() > 0) {
                    map.put("data", inputJson.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            VolleyBackgroundTask applyFilterRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.applyFilterURL, map, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.APPLY_FILTER);
            applyFilterRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            if (dialog != null)
                dialog.show();
            MyApplication.getInstance().addToRequestQueue(applyFilterRequest, AppURL.applyFilterURL);

        } else {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

//    public void refresh(String response) {
//        fabFilter.setVisibility(View.VISIBLE);
//
//        if (response != null && response.length() > 0) {
//            try {
//                productModelArrayList = new ArrayList<ProductModel>();
//                expandableAdapter.notifyDataSetInvalidated();
//                expandableAdapter.removeAllItems();
//                JSONObject filterDataObj = new JSONObject(response);
//
//                setSKUListData(filterDataObj);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }

    @Override
    public void OnImageClicked(int position, String rating) {
        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
        intent.putExtra("ProductId", TextUtils.isEmpty(productId) ? productsArrayList.get(position).getProductId() : productId);
        intent.putExtra("ParentId", productsArrayList.get(position).getProductId());
        intent.putExtra("position", position + "");
        startActivityForResult(intent, 12);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int position = 0;
        if (requestCode == 12 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                if (data.hasExtra("position")) {
                    position = Integer.parseInt(data.getStringExtra("position"));
                }
                if (data.hasExtra("rating")) {
                    String rating = data.getStringExtra("rating");
//                    productModelArrayList.get(position).setProductRating(rating);
                    expandableAdapter.notifyDataSetInvalidated();
                }
            }
        } else if (requestCode == 22 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                isSort = false;
                selSortId = "";
                String responseType = "";
                if (data.hasExtra("clear")) {
                    isClear = data.getBooleanExtra("clear", false);
                }
                if (data.hasExtra("requestType")) {
                    responseType = data.getStringExtra("requestType");
                }
                if (data.hasExtra("inputJson")) {
                    String inputString = data.getStringExtra("inputJson");
                    try {
                        inputJson = new JSONObject(inputString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (data.hasExtra("type")) {
                    type = data.getStringExtra("type");
                }
                if (isClear) {
                    getUnBilledSKUs(false);
                } else {
                    if (responseType != null) {
                        this.requestTypeTemp = responseType;
                    } else {
                        this.requestTypeTemp = "";
                    }
                    if (inputJson != null)
                        applyFilter();
                }
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), error.getMessage());
//        }
    }

    private void getFreebiePacks(String productId) {
        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.FREEBIE_SLABS;
            obj.put("product_id", productId);
            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            //to cancel already running request(if running)
            MyApplication.getInstance().getRequestQueue().cancelAll(AppURL.getSlabRatesURL);

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSlabRatesURL, map,
                    UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getSlabRatesURL);

            if (dialog != null)
                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing() && requestType != PARSER_TYPE.GET_SORT_DATA)
            dialog.dismiss();
        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }
        if (response != null) {
            try {
                if (requestType == PARSER_TYPE.ADD_TO_CART) {
                    if (status.equalsIgnoreCase("success")) {

                        if (response instanceof Integer) {
                            cartCount = (int) response;
                            MyApplication.getInstance().setCartCount(cartCount);
                            updateCartCount();

//                            MyApplication.getInstance().addToCart(productModel, mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.product_added_cart), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                } else if (requestType == PARSER_TYPE.CHECK_INVENTORY) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response instanceof AddCart1Response) {
                        AddCart1Response addCart1Response = (AddCart1Response) response;
                        String _message = String.format(getString(R.string.available_inventory_products), addCart1Response.getAvailableQty());

                        if (!addCart1Response.isStatus())
                            _message = getString(R.string.check_inventory_unavailable);

                        new AlertDialog.Builder(getActivity())
                                .setTitle(getString(R.string.app_name))
                                .setMessage(_message)
                                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //dismiss
                                    }
                                })
                                .show();
                    } else {
                        showToast(getString(R.string.oops));
                    }
                } else if (requestType == PARSER_TYPE.FREEBIE_SLABS) {
                    if (response instanceof ArrayList) {

                        ArrayList<NewPacksModel> arrFreebieSlabs = (ArrayList<NewPacksModel>) response;
                        arrFreebieSlabData = new ArrayList<>();
                        if (arrFreebieSlabs != null && arrFreebieSlabs.size() > 0) {
                            for (int i = 0; i < arrFreebieSlabs.size(); i++) {
                                NewPacksModel newPacksModel = arrFreebieSlabs.get(i);
                                int freePackQty = 0, freebie = 0, freeQty = 0, freeSlabEsu = newPacksModel.getSlabEsu();
                                try {
                                    freebie = Integer.parseInt(freebieQty);
                                    freePackQty = Integer.parseInt(newPacksModel.getPackSize());
                                    freePackQty = freePackQty * freeSlabEsu;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (freebie >= freePackQty) {
                                    ProductSlabData productSlabData = new ProductSlabData();
                                    freeQty = freebie / freePackQty;
                                    productSlabData.setLevel(newPacksModel.getPackLevel());
                                    productSlabData.setEsu(newPacksModel.getSlabEsu());
                                    productSlabData.setQty(freeQty);
                                    productSlabData.setStar(newPacksModel.getStar());
                                    productSlabData.setProductId(freebieProductId);
                                    productSlabData.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                    arrFreebieSlabData.add(productSlabData);
                                    freebie = freebie % freePackQty;
                                    i++;
                                    continue;
                                }
                            }
                            cartModel.setArrFreebieSlabData(arrFreebieSlabData);
                        }

                        long rowId = Utils.insertOrUpdateCart(cartModel);
                        if (rowId == -1) {
                            showToast("Failed to insert in Cart");
                        } else if (rowId == -2) {
                            showToast("Failed to insert Freebie in Cart.");
                        } else if (rowId == -3) {
                            showToast("Failed to insert. Please try again");
                        }

                        updateCartCount();
                    }

                } else if (requestType == PARSER_TYPE.ADD_CART_1) {
                    if (response instanceof AddCart1Response) {
                        AddCart1Response addCart1Response = (AddCart1Response) response;
                        if (addCart1Response.isStatus()) {
                            cartModel = new CartModel();

//                            this.selectedProductModel = productModel;

                            cartModel.setProductId(productId);
                            cartModel.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                            cartModel.setQuantity(totalQty);
                            cartModel.setUnitPrice(unitPriceToApply);
                            cartModel.setMargin(marginToApply);
                            cartModel.setTotalPrice(totalPrice);
                            cartModel.setWarehouseId("");//todo
                            cartModel.setDiscount("");//todo
                            cartModel.setPackType(packType);
                            cartModel.setIsSlab(selPackIsSlab == true ? 1 : 0);
                            cartModel.setBlockedQty(selPackBlockedQty);
                            cartModel.setStar(selectedProductModel.getStar());
                            cartModel.setPrmtDetId(selPackPromotionId);
                            cartModel.setPackStar(selectedPackModel.getStar());

                            ArrayList<ProductSlabData> arrProductSlabData = new ArrayList<>();
                            int qty = 0, totalQty = this.totalQty;
                            double cashBackAmount = 0;
                            String retailerCbIds = "", ffManagerCbIds = "", ffAssCbIds = "";
                            for (int i = arrSlabRates.size() - 1; i >= 0; i--) {
                                NewPacksModel newPacksModel = arrSlabRates.get(i);
                                int packQty = 0, slabEsu = newPacksModel.getSlabEsu();
                                try {
                                    packQty = Integer.parseInt(newPacksModel.getPackSize());
                                    packQty = packQty * slabEsu;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (totalQty >= packQty) {
                                    ProductSlabData productSlabData = new ProductSlabData();
                                    qty = totalQty / packQty;
                                    if (selectedPackModel.getPackLevel() == newPacksModel.getPackLevel()) {

                                        ArrayList<String> arrCashBackIds = new ArrayList<>();

                                        if (newPacksModel.getArrCashBackDetails() != null && newPacksModel.getArrCashBackDetails().size() > 0) {
                                            for (int j = 0; j < newPacksModel.getArrCashBackDetails().size(); j++) {
                                                CashBackDetailsModel cashBackDetailsModel = newPacksModel.getArrCashBackDetails().get(j);
                                                double torange = cashBackDetailsModel.getFf_qty_to_range();
                                                if (totalQty >= torange) {
                                                    if (cashBackDetailsModel.getCbk_source_type() == ConstantValues.PRODUCT) {

                                                        if (!isFF) {
                                                            if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER && cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                                retailerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                            }

                                                        } else {
                                                            if (cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                                if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER) {

                                                                    retailerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                                } else if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.FF_MANAGER) {
                                                                    ffManagerCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                                } else if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.FF_ASSOCIATE) {
                                                                    ffAssCbIds = String.valueOf(cashBackDetailsModel.getCashback_id());
                                                                }
                                                            }

                                                        }

                                                        if (cashBackDetailsModel.getBenificiary_type() == ConstantValues.CUSTOMER && cashBackDetailsModel.getCashbackStar().equalsIgnoreCase(newPacksModel.getStar())) {
                                                            double price = 0;
                                                            try {
                                                                price = Double.parseDouble(newPacksModel.getUnitPrice());
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }

                                                            if (cashBackDetailsModel.getCashback_type() == ConstantValues.CASHBACK_TYPE_VALUE) {
                                                                cashBackAmount = cashBackDetailsModel.getCbk_value();
                                                            } else {
                                                                cashBackAmount = (totalQty * price * cashBackDetailsModel.getCbk_value()) / 100;
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (!TextUtils.isEmpty(retailerCbIds))
                                            arrCashBackIds.add(retailerCbIds);
                                        if (!TextUtils.isEmpty(ffAssCbIds))
                                            arrCashBackIds.add(ffAssCbIds);
                                        if (!TextUtils.isEmpty(ffManagerCbIds))
                                            arrCashBackIds.add(ffManagerCbIds);

                                        String comma_cashBackIds = TextUtils.join(",", arrCashBackIds);
                                        productSlabData.setCashBackIds(comma_cashBackIds);

                                    }
                                    productSlabData.setLevel(newPacksModel.getPackLevel());
                                    productSlabData.setEsu(newPacksModel.getSlabEsu());
                                    productSlabData.setQty(qty);
                                    productSlabData.setPackQty(qty * packQty);
                                    productSlabData.setPackSize(packQty / newPacksModel.getSlabEsu());
                                    productSlabData.setStar(newPacksModel.getStar());
                                    productSlabData.setProductId(productId);
                                    productSlabData.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                    arrProductSlabData.add(productSlabData);
                                    totalQty = totalQty % packQty;  // need to cal using another variable
                                    i++;
                                    continue;
                                }
                            }

                            cartModel.setCashbackAmount(cashBackAmount);
                            cartModel.setArrProductSlabData(arrProductSlabData); // set slabs data for product
                            int _productEsu = selectedPackModel.getSlabEsu();
                            int esu = 1, packQty = 0;
                            if (_productEsu != 0) {
                                try {
                                    packQty = Integer.parseInt(selectedPackModel.getPackSize());
                                    esu = this.totalQty / (_productEsu * packQty);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    esu = 1;
                                }
                            }
                            cartModel.setEsu(String.valueOf(esu));

                            /*String _productEsu = selectedProductModel.getEsu();
                            int esu = 1;
                            if (_productEsu != null && !_productEsu.equals("0") && !TextUtils.isEmpty(_productEsu)) {
                                try {
                                    int tempEsu = Integer.parseInt(_productEsu);
                                    esu = totalQty / tempEsu;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    esu = 1;
                                }
                            }
                            cartModel.setEsu(String.valueOf(esu));*/

                            cartModel.setSlabEsu(selectedProductModel.getEsu());
                            cartModel.setParentId(productId);
                            //check freebie Id in local DB
                            if (freebieProductId != null && !freebieProductId.equals("0") && freebieProductId != null && !freebieProductId.equals("0") && DBHelper.getInstance().getProductById(freebieProductId, false) == null) {
                                isFreebie = true;
                                getProducts(freebieProductId, isFreebie);
                            }

                            //check for freebie
                            if (freebieProductId != null && freebieQty != null && !freebieQty.equals("0") &&
                                    this.totalQty >= freebieMpq) {
                                cartModel.setFreebieQty(freebieQty);
                            }
                            cartModel.setFreebieProductId(freebieProductId);

                            TestProductModel freebieModel = DBHelper.getInstance().getFreebieData(freebieProductId);
                            if (freebieModel != null) {
                                cartModel.setFreebieStar(freebieModel.getStar().isEmpty() ? selectedProductModel.getStar() : freebieModel.getStar());
                                cartModel.setFreebieEsu(freebieModel.getEsu().isEmpty() ? selectedProductModel.getEsu() : freebieModel.getEsu());
                            }
                            cartModel.setFreebieMpq(selectedPackModel.getFreebieMpq());
                            cartModel.setFreebieFq(selectedPackModel.getFreebieQty());

                            /*if (freebieProductId != null && !freebieProductId.equals("0") && !freebieQty.equals("0") && this.totalQty >= freebieMpq) {
                                getFreebiePacks(freebieProductId);
                            } else {*/

                            long rowId = Utils.insertOrUpdateCart(cartModel);
                            if (rowId == -1) {
                                showToast("Failed to insert in Cart");
                            } else if (rowId == -2) {
                                showToast("Failed to insert Freebie in Cart.");
                            } else if (rowId == -3) {
                                showToast("Failed to insert. Please try again");
                            }

                            updateCartCount();
//                            }
                        } else {
                            Utils.showAlertWithMessage(getActivity(), String.format(getString(R.string.unavailable_inventory_product), addCart1Response.getAvailableQty()));
                        }
                    } else {

                    }
                } else if (requestType == PARSER_TYPE.GET_SORT_DATA) {
                    if (response instanceof JSONArray) {
                        JSONArray dataArray = (JSONArray) response;
                        sortString = dataArray.toString();
                    }
                } else if (requestType == PARSER_TYPE.CART_COUNT) {
                    if (response instanceof String) {
                        try {
                            cartCount = Integer.parseInt((String) response);
                        } catch (Exception e) {
                            cartCount = 0;
                            e.printStackTrace();
                        }
                    }
                    MyApplication.getInstance().setCartCount(cartCount);
                    updateCartCount();
                } else if (requestType == PARSER_TYPE.OFFLINE_PRODUCTS) {
                    if (response instanceof ArrayList) {

                        ArrayList<TestProductModel> tempList = new ArrayList<>();
                        if (!isFreebie) {
                            if (isSort)
                                tempList = DBHelper.getInstance().getProductsByProductIds(allProductIds, false, false, "", "");
                            else
                                tempList = DBHelper.getInstance().getProductsByProductIds(unAvailableProducts, false, false, "", "");
//                        productModelArrayList.addAll(tempList);
                            if (tempList.size() > 0)
                                setSKUListData(tempList);
                        }
                    }
                } else if (requestType == PARSER_TYPE.GET_FILTER_DATA_NEW) {
                    if (status.equalsIgnoreCase("success")) {
                        if (response instanceof JSONObject) {
                            JSONObject dataObj = (JSONObject) response;
                            filterString = dataObj.toString();

                            Intent intent = new Intent(getActivity(), FilterActivity.class);
                            intent.putExtra("FilterString", filterString);
                            intent.putExtra("Tabs", true);
                            intent.putExtra("displayStar", true);
                            startActivityForResult(intent, 22);
                        }
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }

                }
                if (requestType == PARSER_TYPE.GET_TOP_BRANDS) {
                    if (response instanceof ArrayList) {
                        ArrayList<TopBrandsModel> arrTopBrands = (ArrayList<TopBrandsModel>) response;
                        switch (flag) {
                            case "1":
                                DBHelper.getInstance().deleteTable(DBHelper.TABLE_BRANDS);
                                for (int i = 0; i < arrTopBrands.size(); i++) {
                                    DBHelper.getInstance().insertBrand(arrTopBrands.get(i));
                                }
                                bindListData(true, "Brand", flagSort);
                                break;
                            case "2":
                                DBHelper.getInstance().deleteTable(DBHelper.TABLE_MANUFACTURERS);
                                for (int i = 0; i < arrTopBrands.size(); i++) {
                                    DBHelper.getInstance().insertManufacturer(arrTopBrands.get(i));
                                }
                                bindListData(true, "Manufacturer", flagSort);
                                break;
                            case "3":
                                DBHelper.getInstance().deleteCategories();
                                for (int i = 0; i < arrTopBrands.size(); i++) {
                                    DBHelper.getInstance().insertHomeCategory(arrTopBrands.get(i));
                                }
                                bindListData(true, "Category", flagSort);
                                break;
                        }

                    }

                } else if (requestType == PARSER_TYPE.WISHLIST) {

                    if (response instanceof ArrayList) {
                        wishListArray = (ArrayList<WishListModel>) response;
                        if (wishListArray != null && wishListArray.size() > 0) {
                            showWishListDialog();
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_create_list), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (requestType == PARSER_TYPE.ADDTO_WISHLIST) {
                    Utils.showAlertDialog(getActivity(), message);
                } else if (requestType == PARSER_TYPE.SLAB_RATES) {
                    if (progress != null)
                        progress.setVisibility(View.GONE);
                    prevClickedProductId = "";
                    if (response instanceof ArrayList) {
                        arrSlabRates = (ArrayList<NewPacksModel>) response;
                        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        if (expandedItemView != null) {
                            LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            if (arrSlabRates != null && arrSlabRates.size() > 0) {
                                tvNoPacks.setVisibility(View.GONE);
                                View slabRatesView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                                slabsLayout = (LinearLayout) slabRatesView.findViewById(R.id.variants_layout);
                                if (slabsLayout != null) {
                                    slabsLayout.removeAllViews();

                                    int selectedPos = 0;
                                    for (int i = 0; i < arrSlabRates.size(); i++) {
                                        selectedPackModel = arrSlabRates.get(i);
                                        String slab = arrSlabRates.get(i).getPackSize();
                                        String star = arrSlabRates.get(i).getStar();
                                        String slabColor = selectedProductModel.getStar();
                                        int slabEsu = selectedPackModel.getSlabEsu();
                                        CustomerTyepModel customerTyepModel = DBHelper.getInstance().getStarColorName(star);
                                        if (customerTyepModel != null)
                                            slabColor = customerTyepModel.getDescription();
                                        LinearLayout childVariantLabel = createSlabRates(slab, slabColor, slabEsu, R.id.id_slab_qty);
                                        childVariantLabel.setTag(arrSlabRates.get(i));
                                        childVariantLabel.setOnClickListener(onVariantClickListener);
                                        slabsLayout.addView(childVariantLabel);

                                        if (selectedPackModel.isSelected())
                                            selectedPos = i;

                                    }

                                    if (slabsLayout.getChildCount() > 0) {
                                        slabsLayout.getChildAt(selectedPos).performClick();
                                        slabsLayout.getChildAt(selectedPos).setSelected(true);
                                    }

                                    slabRatesLayout.addView(slabRatesView);

                                }
                            } else {
                                if (tvNoPacks != null)
                                    tvNoPacks.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                } else {
                    if (status.equalsIgnoreCase("success")) {
                        if (swipeRefreshLayout != null)
                            swipeRefreshLayout.setRefreshing(false);
                        if (response instanceof ProductsResponse) {
                            currentOffset += offsetLimit;
                            productsResponse = (ProductsResponse) response;
                            if (loadType != LoadType.LOAD_MORE)
                                totalItemsCount = productsResponse.getTotalItemsCount();
                            allProductIds = productsResponse.getProductIds();
                            ArrayList<TestProductModel> productTempList = DBHelper.getInstance().getProductsByProductIds(productsResponse.getProductIds(), false, false, "", "");
                            unAvailableProducts = DBHelper.getInstance().getNotAvailableProductIds();

                            ArrayList<String> missedParentList = DBHelper.getInstance().getParentNotAvailableProductIds(allProductIds);
//                        ArrayList<TestProductModel> parentUnavailableProducts = new ArrayList<>();
                            String ids = TextUtils.join(",", missedParentList);
                            productTempList.addAll(DBHelper.getInstance().getProductsByProductIds(ids, true, false, "", ""));

                            if (productTempList != null && productTempList.size() > 0 && !isSort) {
                                setSKUListData(productTempList);
                            }

                            if (!TextUtils.isEmpty(unAvailableProducts)) {
                                isFreebie = false;
                                getProducts(unAvailableProducts, isFreebie);
                            }

                            if (totalItemsCount == 0) {
                                actionSlideExpandableListView.setVisibility(View.INVISIBLE);
                                tvNoItems.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void getProducts(String productIds, boolean isFreebie) {

//        switch(loadType){
//            case APPLY_FILTER:
//            case SORT:
//            case DEFAULT:
//            case SWIPEDOWN:
//                productsArrayList.clear();
//                expandableAdapter.removeAllItems();
//                break;
//            case LOAD_MORE:
//                break;
//        }
//        if (requestTypeTemp.equalsIgnoreCase("Filter")) {
//            loadType = LoadType.APPLY_FILTER;
//        } else {
//            loadType = LoadType.LOAD_MORE;
//        }
        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.OFFLINE_PRODUCTS;
            String appUrl = AppURL.offlineProductsURL;

            obj.put("offset", "0");
            obj.put("offset_limit", "20");
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
            obj.put("product_ids", productIds);

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, appUrl, map,
                    UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, appUrl);
//            if (dialog != null && !isLoadMore && !isFreebie)
//                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCartCount() {
        if (mSharedPreferences.getBoolean("IsLoggedIn", false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                requestTypeAlert = "AddWishList";
                rlAlert.setVisibility(View.GONE);
                flMain.setVisibility(View.VISIBLE);

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    HashMap<String, String> map1 = new HashMap<>();
                    map1.put("data", obj.toString());

                    VolleyBackgroundTask cartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.cartCountURL, map1, UnBilledSKUsFragment.this, UnBilledSKUsFragment.this, PARSER_TYPE.CART_COUNT);
                    cartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(cartRequest, AppURL.cartCountURL);
                    if (dialog != null)
                        dialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                requestTypeAlert = "AddWishList";
                rlAlert.setVisibility(View.VISIBLE);
                flMain.setVisibility(View.GONE);
            }
        }
    }

    private AbsListView.OnScrollListener onScrollListener() {
        return new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 4;
                int count = actionSlideExpandableListView.getCount();


                if (scrollState == SCROLL_STATE_IDLE) {
                    if (actionSlideExpandableListView.getLastVisiblePosition() >= count - threshold && currentOffset < totalItemsCount) {

                        Log.i("Scroll Listener " + currentOffset, "loading more data");
                        if (Networking.isNetworkAvailable(getActivity())) {
                            rlAlert.setVisibility(View.GONE);
                            flMain.setVisibility(View.VISIBLE);

                            loadMoreFastMoving();

                        } else {
                            rlAlert.setVisibility(View.VISIBLE);
                            flMain.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                if (mLastFirstVisibleItem < firstVisibleItem) {
                    llFooterView.setVisibility(View.VISIBLE);
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    llFooterView.setVisibility(View.GONE);
                }
                mLastFirstVisibleItem = firstVisibleItem;
            }
        };
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    public void bindListData(boolean isSortApplied, String sortParam, String sortOrder) {
        ArrayList<TestProductModel> productTempList = DBHelper.getInstance().getProductsByProductIds(productsResponse.getProductIds(), false, isSortApplied, sortParam, sortOrder);
        unAvailableProducts = DBHelper.getInstance().getNotAvailableProductIds();

        ArrayList<String> missedParentList = DBHelper.getInstance().getParentNotAvailableProductIds(allProductIds);
//                        ArrayList<TestProductModel> parentUnavailableProducts = new ArrayList<>();
        String ids = TextUtils.join(",", missedParentList);
        productTempList.addAll(DBHelper.getInstance().getProductsByProductIds(ids, true, isSortApplied, sortParam, sortOrder));

        if (productTempList != null && productTempList.size() > 0 && !isSort) {
            setSKUListData(productTempList);
        }

        if (!TextUtils.isEmpty(unAvailableProducts)) {
            isFreebie = false;
            getProducts(unAvailableProducts, isFreebie);
        }

        if (totalItemsCount == 0) {
            actionSlideExpandableListView.setVisibility(View.INVISIBLE);
            tvNoItems.setVisibility(View.VISIBLE);
        }
    }

    private enum LoadType {DEFAULT, APPLY_FILTER, LOAD_MORE, SWIPEDOWN}

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}