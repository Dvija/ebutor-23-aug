package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MainActivityNew;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.WishListAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.WishListModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DividerItemDecoration;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyWishListFragment extends Fragment implements WishListAdapter.OnItemClickListener, Response.ErrorListener, VolleyHandler<Object> {

    Button btnCreateList;
    EditText etListName, etSearch;
    WishListAdapter adapter;
    RecyclerView wishList;
    private SharedPreferences mSharedPreferences;
    private String listName, requestType = "";
    private ArrayList<WishListModel> wishListArray;
    private TextView tvAlertMsg;
    private View tvNoItems;
    private RelativeLayout rlAlert;
    private Tracker mTracker;
    private LinearLayout llMain;
    private Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_wishlist, container, false);

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        btnCreateList = (Button) v.findViewById(R.id.btn_createList);
        etListName = (EditText) v.findViewById(R.id.et_createList);
        etSearch = (EditText) v.findViewById(R.id.searchView1);
        tvNoItems = (View) v.findViewById(R.id.tvNoItems);
        tvAlertMsg = (TextView) v.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) v.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) v.findViewById(R.id.ll_main);
        wishList = (RecyclerView) v.findViewById(R.id.rv_wishlist);
        wishList.addItemDecoration(
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        wishListArray = new ArrayList<>();
        getWishList();
        MyApplication application = (MyApplication) getActivity().getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        btnCreateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                listName = etListName.getText().toString().trim();
                if (!TextUtils.isEmpty(listName)) {
                    createWishList();

                } else {

                    etListName.setError(getActivity().getResources().getString(R.string.please_enter_list_name));
                    //etListName.requestFocus();
                    return;
                }

            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestType) {
                        case "WishList":
                            getWishList();
                            break;
                        case "CreateWishList":
                            createWishList();
                            break;
                        default:
                            getWishList();
                            break;
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null) {
                    adapter.getFilter().filter(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return v;
    }

    private void createWishList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "CreateWishList";
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "2");
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE,""));
                obj.put("le_wh_id",mSharedPreferences.getString(ConstantValues.KEY_WH_IDS,""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("Listname", listName);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask createShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productListOperationsURL, map, MyWishListFragment.this, MyWishListFragment.this, PARSER_TYPE.CREATE_WISHLIST);
                createShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(createShoppingListRequest, AppURL.productListOperationsURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "CreateWishList";
        }
    }

    private void getWishList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "WishList";
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "1");
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE,""));
                obj.put("le_wh_id",mSharedPreferences.getString(ConstantValues.KEY_WH_IDS,""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID,""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.shoppinglistURL, map, MyWishListFragment.this, MyWishListFragment.this, PARSER_TYPE.WISHLIST);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.shoppinglistURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "WishList";
        }
    }

    @Override
    public void onItemClick(int position, Object object) {
        if (object != null) {
            if (object instanceof WishListModel) {
                Intent intent = new Intent(getActivity(), MainActivityNew.class);
                intent.putExtra("WishListName", ((WishListModel) object).getListName());
                intent.putExtra("buyer_listing_id", ((WishListModel) object).getListId());
                intent.putExtra("Type", "WishList");
                startActivity(intent);
            } else {
                return;
            }

        } else {
            return;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("My WishList Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if(error!=null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e){
        }
//        if (error != null)
//            Utils.showAlertWithMessage(getActivity(),getActivity().getResources().getString(R.string.unexpected_error));

    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.CREATE_WISHLIST) {
                    if (results instanceof ArrayList) {
                        wishListArray = (ArrayList<WishListModel>) results;
                        etListName.setText("");
//                            adapter.notifyDataSetChanged();
                        if (wishListArray != null && wishListArray.size() > 0) {
                            adapter = new WishListAdapter(getActivity(), wishListArray);
                            adapter.setClickListener(MyWishListFragment.this);
                            wishList.setLayoutManager(new LinearLayoutManager(getActivity()));
                            wishList.setAdapter(adapter);
                            wishList.setVisibility(View.VISIBLE);
                            tvNoItems.setVisibility(View.GONE);
                        } else {
                            wishList.setVisibility(View.GONE);
                            tvNoItems.setVisibility(View.VISIBLE);
                        }
                    }
                } else if (requestType == PARSER_TYPE.WISHLIST) {
                    if (results instanceof ArrayList) {
                        wishListArray = (ArrayList<WishListModel>) results;
                        if (wishListArray != null && wishListArray.size() > 0) {
                            adapter = new WishListAdapter(getActivity(), wishListArray);
                            adapter.setClickListener(MyWishListFragment.this);
                            wishList.setLayoutManager(new LinearLayoutManager(getActivity()));
                            wishList.setAdapter(adapter);
                            wishList.setVisibility(View.VISIBLE);
                            tvNoItems.setVisibility(View.GONE);
                        } else {
                            wishList.setVisibility(View.GONE);
                            tvNoItems.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(),message);
    }

}
