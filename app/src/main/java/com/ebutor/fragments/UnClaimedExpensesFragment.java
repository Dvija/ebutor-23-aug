package com.ebutor.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.SubmitExpenseActivity;
import com.ebutor.adapters.UnClaimedExpensesRecyclerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.ExpenseModel;
import com.ebutor.models.NewExpenseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class UnClaimedExpensesFragment extends Fragment implements UnClaimedExpensesRecyclerAdapter.OnItemClickListener,
        Callback, RecyclerView.OnItemTouchListener, View.OnClickListener, Response.ErrorListener, VolleyHandler<Object> {

    GestureDetectorCompat gestureDetector;
    Context mContext;
    ActionMode actionMode;
    //    public void getOrdersList() {
    ArrayList<NewExpenseModel> data = DBHelper.getInstance().getUnClaimedExpenses();
    private SharedPreferences mSharedPreferences;
    private ArrayList<ExpenseModel> expenseModelArrayList;
    private UnClaimedExpensesRecyclerAdapter adapter;
    //    private FloatingActionButton btnAddExpense;
//    private String customerToken = "", custId = "", leEntityId = "";
    private RecyclerView rvOrderList;
    private Dialog dialog;
    private TextView tvNoOrders;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_expense_tracker, container, false);


        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        rvOrderList = (RecyclerView) v.findViewById(R.id.rvOrdersList);
        //  rvOrderList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        //   rvOrderList.setItemAnimator(new DefaultItemAnimator());
        rvOrderList.addOnItemTouchListener(UnClaimedExpensesFragment.this);
        gestureDetector = new GestureDetectorCompat(getActivity(), new RecyclerViewDemoOnGestureListener());

        tvNoOrders = (TextView) v.findViewById(R.id.tvNoOrders);

        return v;
    }

    public void getOrdersList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
                String url = AppURL.getExpensesLineItemsURL + "?UserID=" + userId + "&RecordType=0";
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, url, null, UnClaimedExpensesFragment.this, UnClaimedExpensesFragment.this, PARSER_TYPE.DEFAULT);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
//        if (data != null) {
//            adapter = new UnClaimedExpensesRecyclerAdapter(getActivity(), data);
//            adapter.setClickListener(UnClaimedExpensesFragment.this);
//            rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
//            rvOrderList.setAdapter(adapter);
//        }
//    }

    @Override
    public void onItemClick(int position) {
//        Intent intent = new Intent(getActivity(), ExpenseDetailActivity.class);
//        intent.putExtra("ExpenseCode", expenseModelArrayList.get(position).getExpenseCode());
//        intent.putExtra("ExpenseModel", expenseModelArrayList.get(position));
//        getActivity().startActivity(intent);

    }


    @Override
    public void onResume() {
        super.onResume();
        getOrdersList();
    }


    /**
     * Silently observe and/or take over touch events sent to the RecyclerView
     * before they are handled by either the RecyclerView itself or its child views.
     * <p>
     * <p>The onInterceptTouchEvent methods of each attached OnItemTouchListener will be run
     * in the order in which each listener was added, before any other touch processing
     * by the RecyclerView itself or child views occurs.</p>
     *
     * @param rv
     * @param e  MotionEvent describing the touch event. All coordinates are in
     *           the RecyclerView's coordinate system.
     * @return true if this OnItemTouchListener wishes to begin intercepting touch events, false
     * to continue with the current behavior and continue observing future events in
     * the gesture.
     */
    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    /**
     * Process a touch event as part of a gesture that was claimed by returning true from
     * a previous call to {@link #onInterceptTouchEvent}.
     *
     * @param rv
     * @param e  MotionEvent describing the touch event. All coordinates are in
     */
    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    /**
     * Called when action mode is first created. The menu supplied will be used to
     * generate action buttons for the action mode.
     *
     * @param actionMode ActionMode being created
     * @param menu       Menu used to populate action buttons
     * @return true if the action mode should be created, false if entering this
     * mode should be aborted.
     */
    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        // Inflate a menu resource providing context menu items
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.menu_action_mode, menu);
        getActivity().findViewById(R.id.menu_yellow).setVisibility(View.GONE);
        return true;
    }

    /**
     * Called to refresh an action mode's action menu whenever it is invalidated.
     *
     * @param mode ActionMode being prepared
     * @param menu Menu used to populate action buttons
     * @return true if the menu or action mode was updated, false otherwise.
     */
    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    /**
     * Called to report a user click on an action button.
     *
     * @param actionMode The current ActionMode
     * @param menuItem   The item that was clicked
     * @return true if this callback handled the event, false if the standard MenuItem
     * invocation should continue.
     */
    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.upload:
                ArrayList<NewExpenseModel> selectedItems = adapter.getSelectedItems();
                Intent intent = new Intent(getActivity(), SubmitExpenseActivity.class);
                intent.putExtra("Type", 3);//for Attaching line items with a new Main Item
                intent.putParcelableArrayListExtra("ExpensesList", selectedItems);
                startActivity(intent);
                actionMode.finish();
                return true;
            case R.id.delete:
                String selectedIds = adapter.getSelectedIds();
                deleteUnClaimedExpenses(selectedIds);
//                Toast.makeText(getActivity(), selectedIds, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
//        actionMode.finish();
//        return true;
    }

    /**
     * Called when an action mode is about to be exited and destroyed.
     *
     * @param actionMode The current ActionMode being destroyed
     */
    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        this.actionMode = null;
        adapter.clearSelections();
        getActivity().findViewById(R.id.menu_yellow).setVisibility(View.VISIBLE);
    }

    private void myToggleSelection(int idx) {
        adapter.toggleSelection(idx);
        if (adapter.getSelectedItemCount() == 0)
            actionMode.finish();
        else {
            String title = getString(R.string.selected_count, adapter.getSelectedItemCount());

            actionMode.setTitle(title);
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param view The view that was clicked.
     */
    @Override
    public void onClick(View view) {
        int idx = rvOrderList.getChildLayoutPosition(view);
        if (actionMode != null) {
            myToggleSelection(idx);
        }
    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    private void deleteUnClaimedExpenses(String commaSeparatedIds) {
        if (Networking.isNetworkAvailable(getActivity())) {
            String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
            try {
                JSONObject obj = new JSONObject();
                obj.put("unclaimedID", commaSeparatedIds);
                obj.put("UserID", userId);

                Log.e("request Obj", obj.toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.deleteUnClaimedExpensesURL, obj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();

                        String status = response.optString("status");
                        String message = response.optString("message");
                        if (status.equalsIgnoreCase("success")) {
                            Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            actionMode.finish();
                            getOrdersList();
                        } else {
                            Utils.showAlertDialog(getActivity(), message);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        // something to do here ??
                        return ConstantValues.getRequestHeaders();
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(request, AppURL.deleteUnClaimedExpensesURL);

                if (dialog != null && !dialog.isShowing())
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (requestType == PARSER_TYPE.DEFAULT) {
            try {

                if (data != null)
                    data.clear();
                else
                    data = new ArrayList<>();

                JSONObject obj = new JSONObject((String) response);

                String _status = obj.optString("status");
                if (_status.equals("success")) {
                    JSONArray dataArray = obj.optJSONArray("data");
                    if (dataArray != null && dataArray.length() > 0) {
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject object = dataArray.optJSONObject(i);

                            String expDetId = object.optString("exp_det_id");
                            String amount = object.optString("ExpDetAmount");
                            String date = object.optString("ExpDetDate");
                            String expenseType = object.optString("ExpDetType");
//                            model.setExpenseTypeId(cursor.getString(cursor.getColumnIndex(KEY_EXPENSE_TYPE_ID)));
                            String desc = object.optString("Description");
                            String attachments = object.optString("ProofImageKey");

                            NewExpenseModel model = new NewExpenseModel(expDetId, amount, date, expenseType, "", desc, attachments);
                            data.add(model);
                        }

                    }
                }

                if (data != null && data.size() > 0) {
                    rvOrderList.setVisibility(View.VISIBLE);
                    tvNoOrders.setVisibility(View.GONE);

                    adapter = new UnClaimedExpensesRecyclerAdapter(getActivity(), data);
                    adapter.setClickListener(UnClaimedExpensesFragment.this);
                    rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
                    rvOrderList.setAdapter(adapter);
                } else {
                    rvOrderList.setVisibility(View.GONE);
                    tvNoOrders.setVisibility(View.VISIBLE);
                    tvNoOrders.setText(getString(R.string.no_results));
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSessionError(String message) {

    }

    private class RecyclerViewDemoOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = rvOrderList.findChildViewUnder(e.getX(), e.getY());
            onClick(view);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            View view = rvOrderList.findChildViewUnder(e.getX(), e.getY());
            if (actionMode != null) {
                return;
            }
            // Start the CAB using the ActionMode.Callback defined above
            actionMode = getActivity().startActionMode(UnClaimedExpensesFragment.this);
            int idx = rvOrderList.getChildPosition(view);
            myToggleSelection(idx);
            super.onLongPress(e);
        }
    }
}
