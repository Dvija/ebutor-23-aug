package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.OrderDetailActivity;
import com.ebutor.R;
import com.ebutor.adapters.OrdersListAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.OrdersModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class OrdersListFragment extends Fragment implements OrdersListAdapter.OnItemClickListener, Response.ErrorListener, VolleyHandler<Object> {

    private SharedPreferences mSharedPreferences;
    private ArrayList<OrdersModel> ordersList = new ArrayList<>();
    private OrdersListAdapter adapter;
    private RecyclerView rvOrderList;
    private TextView tvNoOrders, tvAlertMsg;
    private RelativeLayout rlAlert;
    private LinearLayout llMain, llFooter, llHeader;
    private Tracker mTracker;
    private Dialog dialog;
    private String customerToken = "", custId = "", leEntityId = "", selStatusId = "";
    private int currentOffset = 0;
    private int totalItemsCount = 0;
    private int offsetLimit = Utils.offsetLimit;
    private boolean isLoadMore = false, isRequestInProgress = false;
    StatusFragment.OnSelListener okListener = new StatusFragment.OnSelListener() {

        @Override
        public void onSelected(String id) {
            selStatusId = id;
            currentOffset = 0;
            isLoadMore = false;
            adapter.removeAllItems();
            ordersList = new ArrayList<>();
            getOrdersList(customerToken, custId, leEntityId);
        }
    };
    private Button btnFilter;
    private ArrayList<CustomerTyepModel> arrStatus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_orders_list, container, false);
        if (getArguments().containsKey("token")) {
            customerToken = getArguments().getString("token");
        } else {
            customerToken = "";
        }
        if (getArguments().containsKey("cust_id")) {
            custId = getArguments().getString("cust_id");
        } else {
            custId = "";
        }
        if (getArguments().containsKey("le_entity_id")) {
            leEntityId = getArguments().getString("le_entity_id");
        } else {
            leEntityId = "";
        }

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        rvOrderList = (RecyclerView) v.findViewById(R.id.rvOrdersList);
        tvNoOrders = (TextView) v.findViewById(R.id.tvNoOrders);
        tvAlertMsg = (TextView) v.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) v.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) v.findViewById(R.id.ll_main);
        llFooter = (LinearLayout) v.findViewById(R.id.ll_footer);
        llHeader = (LinearLayout) v.findViewById(R.id.ll_header);
        btnFilter = (Button) v.findViewById(R.id.btn_filter);

        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false))
            btnFilter.setVisibility(View.VISIBLE);
        else
            btnFilter.setVisibility(View.GONE);

        adapter = new OrdersListAdapter(getActivity(), ordersList);
        adapter.setClickListener(OrdersListFragment.this);
        rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvOrderList.setAdapter(adapter);

//        getOrdersList();

        rvOrderList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int count = recyclerView.getLayoutManager().getItemCount();
                int pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                isLoadMore = true;

                if ((visibleItemCount + pastVisiblesItems) >= count && currentOffset < totalItemsCount) {
                    if (Networking.isNetworkAvailable(getActivity())) {
                        llFooter.setVisibility(View.VISIBLE);
                        getOrdersList(customerToken, custId, leEntityId);
                    } else {
                        Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    getOrdersList(customerToken, custId, leEntityId);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFilterOrderStatus();
            }
        });

        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        return v;
    }

    private void getFilterOrderStatus() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getfilterorderstatusURL, map, OrdersListFragment.this, OrdersListFragment.this, PARSER_TYPE.FILTER_ORDER_STATUS);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getfilterorderstatusURL);
                if (dialog != null)
                    dialog.show();

//                HTTPBackgroundTask task = new HTTPBackgroundTask(OrdersListFragment.this, getActivity(), PARSER_TYPE.ORDERS_LIST, APIREQUEST_TYPE.HTTP_POST);
//                task.execute(map);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    public void getOrdersList(String customerToken, String custId, String leEntityId) {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                if (TextUtils.isEmpty(customerToken)) {
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("customerId", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                } else {
                    obj.put("customer_token", customerToken);
                    obj.put("customerId", custId);
                }
                obj.put("legal_entity_id", TextUtils.isEmpty(leEntityId) ? mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, "") : leEntityId);
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    obj.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                    obj.put("status_id", selStatusId);
                }
                obj.put("offset", currentOffset);
                obj.put("offset_limit", offsetLimit);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.ordersListURL, map, OrdersListFragment.this, OrdersListFragment.this, PARSER_TYPE.ORDERS_LIST);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                if (!isRequestInProgress)
                    MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.ordersListURL);
                isRequestInProgress = true;

                if (dialog != null && !isLoadMore)
                    dialog.show();

//                HTTPBackgroundTask task = new HTTPBackgroundTask(OrdersListFragment.this, getActivity(), PARSER_TYPE.ORDERS_LIST, APIREQUEST_TYPE.HTTP_POST);
//                task.execute(map);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    @Override
    public void onItemClick(int position) {
        if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.ORDER_DETAILS_FEATURE_CODE) >= 0) {
            Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
            intent.putExtra("orderId", ordersList.get(position).getOrderNumber());
            intent.putExtra("status", ordersList.get(position).getOrderStatus());
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        currentOffset = 0;
        isLoadMore = false;
        ordersList = new ArrayList<>();
        adapter.removeAllItems();
        getOrdersList(customerToken, custId, leEntityId);
        mTracker.setScreenName("Orders List Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if(error!=null){
//            Utils.showAlertWithMessage(getActivity(),getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.ORDERS_LIST) {
                    if (results instanceof OrdersModel) {
                        currentOffset += offsetLimit;
                        isRequestInProgress = false;
                        OrdersModel ordersModel = (OrdersModel) results;
                        if (ordersModel != null) {
                            ArrayList<OrdersModel> tempList = ordersModel.getArrOrders();
                            totalItemsCount = ordersModel.getCount();
                            if (tempList != null && tempList.size() > 0) {
                                llHeader.setVisibility(View.VISIBLE);
                                tvNoOrders.setVisibility(View.GONE);
                                setDataAdapter(tempList);

                            }
                        }
                    }
                } else if (requestType == PARSER_TYPE.FILTER_ORDER_STATUS) {
                    if (results instanceof ArrayList) {
                        arrStatus = (ArrayList<CustomerTyepModel>) results;
                        if (arrStatus != null && arrStatus.size() > 0) {
                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            StatusFragment statusFragment = StatusFragment.newInstance(arrStatus);
                            statusFragment.setCancelable(true);
                            statusFragment.setConfirmListener(okListener);
                            statusFragment.show(fm, "status_fragment");
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_results), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void setDataAdapter(ArrayList<OrdersModel> tempList) {
        if (tempList != null && tempList.size() > 0) {
            for (int i = 0; i < tempList.size(); i++) {
                adapter.addItem(tempList.get(i));
                if (!ordersList.contains(tempList.get(i)))
                    ordersList.add(tempList.get(i));
            }

            adapter.notifyDataSetChanged();
        } else {
            if (ordersList.size() < 1) {
                llHeader.setVisibility(View.GONE);
                tvNoOrders.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}
