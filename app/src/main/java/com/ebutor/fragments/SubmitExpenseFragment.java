package com.ebutor.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.AttachmentsActivity;
import com.ebutor.activities.SubmitExpenseActivity;
import com.ebutor.adapters.ExpenseTypeSpinnerAdapter;
import com.ebutor.adapters.UnclaimedRecyclerAdapter;
import com.ebutor.backgroundtask.MultipartEntity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.ExpenseMasterLookupData;
import com.ebutor.models.ExpenseMasterlookupModel;
import com.ebutor.models.ImagePathData;
import com.ebutor.models.NewExpenseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.RuntimePermissionUtils;
import com.ebutor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Srikanth Nama on 29-Dec-16.
 */

public class SubmitExpenseFragment extends Fragment implements VolleyHandler<Object>, Response.ErrorListener, UnclaimedRecyclerAdapter.OnItemClickListener {

    public static final int CAMERA_PERMISSION_REQUEST_CODE = 100;
    public static final int WRITE_EXTERNAL_PERMISSION_REQUEST_CODE = 101;
    public static final int GALLERY_PERMISSION_REQUEST_CODE = 102;
    private static final int REQUEST_PERMISSION_SETTING = 125;

    //newly added
    ArrayList<NewExpenseModel> data;
    SwipeRefreshLayout swipeRefreshLayout;
    Button allsubmit;
    TextView textView;
    ImageView attachment;
    TextView badgecount;
    int count = 0;
    Intent intent;
    View galmargin;
    LinearLayout fragsubmitheader;
    ProgressDialog dialog;
    String mCurrentPhotoPath;
    String desc;
    //    ArrayList<ImagePathData> catchpath;
    TextView textdesc;
    Dialog dialog1;
    ProgressDialog progressDialog;
    private RecyclerView rvOrderList;
    private UnclaimedRecyclerAdapter adapter = null;
    private ArrayList<NewExpenseModel> expenseModelArrayList = new ArrayList<>();
    private EditText etAmount, etDate, etDesc;
    private Spinner spinExpType;
    private ImageView btnSubmit;
    private ImageButton ibCamera, ibGallery;
    private String expenseTypeId, expenseType;
    private String expenseDate;
    private int _submitType = 0;
    private LinearLayout llimages;
    private HorizontalScrollView imagesScrollView;
    private ArrayList<ImagePathData> imagePathDataArrayList;
    View.OnClickListener onDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getTag() != null) {
                ImagePathData path = (ImagePathData) view.getTag();

                if (imagePathDataArrayList != null && imagePathDataArrayList.contains(path)) {
                    imagePathDataArrayList.remove(path);
                    count--;
                    badgecount.setText(count + "");
                }
                Toast.makeText(getContext(), "delete", Toast.LENGTH_SHORT).show();

                if (imagePathDataArrayList == null || imagePathDataArrayList.size() == 0) {
                    imagesScrollView.setVisibility(View.GONE);
                    galmargin.setVisibility(View.GONE);
                }
                llimages.removeAllViews();

                for (ImagePathData s : imagePathDataArrayList) {
                    llimages.addView(setPic(s));
                }


            }
        }
    };
    private String attachmentIds;
    private NewExpenseModel newExpenseModel;
    private SharedPreferences mSharedPreferences;
    private int position = 0;

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_submit_expense, container, false);


        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //  f.set
        //  fragsubmitheader= (LinearLayout) view.findViewById(R.id.fragement_submit_header);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("Type")) {
                //type 1 == for Expense
                //type 2 == for Advance
                //type 3 == for Attach Unclaimed expenses with Main Record
                _submitType = extras.getInt("Type", 1);// default value 1


            }
        }


        attachment = (ImageView) view.findViewById(R.id.attachment);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefreshlayout2);

        rvOrderList = (RecyclerView) view.findViewById(R.id.recycler_view_unclaimed);

        textView = (TextView) view.findViewById(R.id.no_result);

        badgecount = (TextView) view.findViewById(R.id.badge_notification_1);


        data = new ArrayList<NewExpenseModel>();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrdersList(false);

            }
        });

        allsubmit = (Button) view.findViewById(R.id.allsubmit);


        imagePathDataArrayList = new ArrayList<>();

        etAmount = (EditText) view.findViewById(R.id.et_amount);
        etDate = (EditText) view.findViewById(R.id.et_date);
        //   etDesc = (EditText) view.findViewById(R.id.et_desc);
        //  ibCamera = (ImageButton) view.findViewById(R.id.camera);
        //  ibGallery = (ImageButton) view.findViewById(R.id.gallery);

        //   imagesScrollView = (HorizontalScrollView) view.findViewById(R.id.horizontal_scroll_view);
        //   imageContainer = (LinearLayout) view.findViewById(R.id.ll_images);



        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            etDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_calender, 0);
        }


        spinExpType = (Spinner) view.findViewById(R.id.spin_expense_type);
        textdesc = (TextView) view.findViewById(R.id.textdesc);
        btnSubmit = (ImageView) view.findViewById(R.id.btn_submit);

        // fragsubmitheader.setVisibility(View.GONE);

        Calendar calendar = Calendar.getInstance();
        expenseDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5,
                calendar.get(Calendar.DAY_OF_MONTH) + " " + (calendar.get(Calendar.MONTH) + 1) + " " + calendar.get(Calendar.YEAR));
        etDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN,
                calendar.get(Calendar.DAY_OF_MONTH) + " " + (calendar.get(Calendar.MONTH) + 1) + " " + calendar.get(Calendar.YEAR)));


        attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog1 = new Dialog(getActivity());
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.gallery_layout);
                //  dialog1.setTitle("Select the Options");

                ImageView gallery = (ImageView) dialog1.findViewById(R.id.dia_gal);
                ImageView camera = (ImageView) dialog1.findViewById(R.id.dia_camera);
                // final TextView sample= (TextView) dialog1.findViewById(R.id.samplecheck);
                llimages = (LinearLayout) dialog1.findViewById(R.id.ll_images);
                imagesScrollView = (HorizontalScrollView) dialog1.findViewById(R.id.horizontal_scroll_view);
                galmargin = dialog1.findViewById(R.id.gal_margin);


                if (imagePathDataArrayList != null && imagePathDataArrayList.size() > 0) {
                    for (int j = 0; j < imagePathDataArrayList.size(); j++) {
                        galmargin.setVisibility(View.VISIBLE);
                        imagesScrollView.setVisibility(View.VISIBLE);
                        llimages.addView(setPic(imagePathDataArrayList.get(j)));
                    }

                }

                gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                        //GalleryAction();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.READ_EXTERNAL_STORAGE);

                            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                    RuntimePermissionUtils.showRationale(getActivity(),
                                            SubmitExpenseFragment.this,
                                            Manifest.permission.READ_EXTERNAL_STORAGE,
                                            GALLERY_PERMISSION_REQUEST_CODE,
                                            "You need to allow access to Gallery");
                                } else {
                                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                            GALLERY_PERMISSION_REQUEST_CODE);

                                }
                                return;
                            }
                            startGallery();
                            //  sample.setText(444+"");

                        }
                    }
                });

                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                       /* picType = "shop";
                        takePicture();*/

                        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA);

                        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                                RuntimePermissionUtils.showRationale(getActivity(),
                                        SubmitExpenseFragment.this,
                                        Manifest.permission.CAMERA,
                                        CAMERA_PERMISSION_REQUEST_CODE,
                                        "You need to allow access to Camera");
                            } else {
                                requestPermissions(new String[]{Manifest.permission.CAMERA},
                                        CAMERA_PERMISSION_REQUEST_CODE);

                            }
                            return;
                        }

                        int _permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        if (_permissionCheck != PackageManager.PERMISSION_GRANTED) {
                            if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                RuntimePermissionUtils.showRationale(getActivity(),
                                        SubmitExpenseFragment.this,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        WRITE_EXTERNAL_PERMISSION_REQUEST_CODE,
                                        "You need to allow access to Storage");
                            } else {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        WRITE_EXTERNAL_PERMISSION_REQUEST_CODE);

                            }
                            return;
                        }

                        dispatchTakePictureIntent();


                        dialog1.dismiss();
                    }
                });

                dialog1.show();
                Window window = dialog1.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            }
        });


        textdesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(getActivity());

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.customalertdialog);
                final EditText editText = (EditText) dialog.findViewById(R.id.et_desc);
                editText.setText(desc);
                editText.setSelection(editText.getText().length());


                Button ok = (Button) dialog.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        desc = editText.getText().toString();
                        if (desc != null) {
                            textdesc.setText(desc);


                        }

                        if (desc.isEmpty())
                            textdesc.setText("Tap to Enter Description");
                        dialog.dismiss();
                    }
                });

                dialog.show();

                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String amount = etAmount.getText().toString().trim();
                //    String desc = etDesc.getText().toString().trim();

               /* etAmount.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        *//*StringBuffer stringBuffer=new StringBuffer();
                        if(count<=6)
                        {
                            for(int i=0;i<=count;i++)
                            {
                                stringBuffer.append(s.charAt(i));
                            }

                        }
                        stringBuffer.append('.');
                         if(count>7 && count<9){

                            for(int i=0;i<=count;i++)
                            {
                                stringBuffer.append(s.charAt(i));
                            }
                        }*//*
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        Pattern ps=Pattern.compile("^[0-9][0-9][0-9][0-9][0-9][0-9][.][0-9][0-9]");
                        ps.matcher(amount);



                    }
                });
*/                /*String a=amount.substring(0,6);


                String p=amount.substring(amount.indexOf('.')+1,amount.indexOf('.')+3);
             //   String  add=a+"."+p;
                int decimal=Integer.parseInt(p);*/

                if(TextUtils.isEmpty(expenseTypeId)){
                    Utils.showAlertWithMessage(getActivity(),getString(R.string.please_select_expense_type));
                    return;
                }

                if (amount.isEmpty()) {
                    Utils.showAlertDialog(getActivity(),getString(R.string.please_enter_amount));
                    return;
                }
                boolean isValidAmount = true;
                try {
                    double _amount = 0;
                    _amount = Double.parseDouble(amount);
                    if (_amount <= 0.0) {
                        isValidAmount = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isValidAmount = false;
                }

                if (!isValidAmount) {
                    etAmount.requestFocus();
                    etAmount.setError("Please enter valid Amount");
                    return;
                }

               /*if (TextUtils.isEmpty(desc)) {
                    etDesc.requestFocus();
                    etDesc.setError("Please enter Description");
                    return;
                }*/
                btnSubmit.setEnabled(false);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.show();

                newExpenseModel = new NewExpenseModel("0", amount, expenseDate, expenseType, expenseTypeId, desc, "");

                if (imagePathDataArrayList != null && imagePathDataArrayList.size() > 0) {
                    //we have images to be uploaded, please upload
                    if (Networking.isNetworkAvailable(getActivity())) {
                       /* for (ImagePathData imagePath : imagePathDataArrayList) {
                            beginUpload(imagePath);
                        }*/
                        new UploadImageTask().execute(imagePathDataArrayList.get(position).getImagePath());
                    } else {
                        attachmentIds = TextUtils.join(",", imagePathDataArrayList);
                        // Internet is not available, Please store the info in Local DB
                        newExpenseModel.setAttachments(attachmentIds);
                        //todo store in DB
                        submitRequest(newExpenseModel);

                    }

                } else {
                    //todo submit request to server/Local DB
                    submitRequest(newExpenseModel);
                }


            }
        });


        spinExpType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null && view.getTag() instanceof ExpenseMasterLookupData) {
                    ExpenseMasterLookupData data = (ExpenseMasterLookupData) view.getTag();
                    expenseType = data.getMasterLookupName();
                    expenseTypeId = data.getValue();

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

     /*   ibCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                SubmitExpenseFragment.this,
                                Manifest.permission.CAMERA,
                                CAMERA_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Camera");
                    } else {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                CAMERA_PERMISSION_REQUEST_CODE);

                    }
                    return;
                }

                int _permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (_permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                SubmitExpenseFragment.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                WRITE_EXTERNAL_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Storage");
                    } else {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                WRITE_EXTERNAL_PERMISSION_REQUEST_CODE);

                    }
                    return;
                }

                dispatchTakePictureIntent();
            }
        });

        ibGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            RuntimePermissionUtils.showRationale(getActivity(),
                                    SubmitExpenseFragment.this,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    GALLERY_PERMISSION_REQUEST_CODE,
                                    "You need to allow access to Gallery");
                        } else {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    GALLERY_PERMISSION_REQUEST_CODE);

                        }
                        return;
                    }
                    startGallery();

                }
            }
        });*/

        allsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ArrayList<NewExpenseModel> selectedItems = adapter.getSelectedItems();
                if (selectedItems.size() > 0) {

                    Intent intent = new Intent(getActivity(), SubmitExpenseActivity.class);
                    intent.putExtra("Type", 3);
                    intent.putParcelableArrayListExtra("ExpensesList", selectedItems);
                    startActivity(intent);
                } else {

                    Toast.makeText(getActivity(), "Please Select Expenses", Toast.LENGTH_SHORT).show();

                }

            }
        });

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year, month, day;
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        expenseDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                        etDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN, dayOfMonth + " " + (monthOfYear + 1) + " " + year));

                    }
                }, year, month, day);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 21);
                calendar.set(Calendar.MINUTE, 59);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        getMasterLookupData();

    }

    public String uploadFile(String filePath) {

        String stringResponse = "";
        String charset = "UTF-8";

        try {

            MultipartEntity multipart = new MultipartEntity(AppURL.uploadImagesURL, charset);

            multipart.addFormField("_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            multipart.addFormField("entity", "expenses");
            multipart.addFilePart("image", new File(filePath));


            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");
            StringBuilder sb = new StringBuilder();
            for (String line : response) {
                System.out.println(line);
                sb.append(line);
            }

            return sb.toString();
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void submitRequest(NewExpenseModel model) {
        if (Networking.isNetworkAvailable(getActivity())) {
            saveExpenseItem(model);

        } else {
            long rowId = DBHelper.getInstance().insertExpenseInDB(model);
            if (rowId < 0) {
                btnSubmit.setEnabled(true);
                //oops failed to insert
                Toast.makeText(getActivity(), "oops..! failed to Save record. Please try again.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Successfully Saved record.", Toast.LENGTH_SHORT).show();
                // getActivity().finish();
            }
        }
    }

    private void saveExpenseItem(NewExpenseModel model) {
        if (Networking.isNetworkAvailable(getActivity())) {

            if (imagePathDataArrayList != null && imagePathDataArrayList.size() > 0)
                attachmentIds = TextUtils.join(",", imagePathDataArrayList);
            else
                count = 0;
            String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
            try {
                JSONObject obj = new JSONObject();
                obj.put("ExpID", "0");
                obj.put("ExpDetActualAmount", model.getAmount());
                obj.put("ExpDetType", model.getExpenseTypeId());
                obj.put("ExpDetDate", model.getDate());
                obj.put("Description", model.getDesc());//model.getDesc()
                obj.put("ExpDetProofKey", attachmentIds);
                obj.put("UserID", userId);
                obj.put("ExpDetRecordType", "0"); // 0-for untracked 1-for tracked
//                HashMap<String, String> map1 = new HashMap<>();
//                map1.put("data", obj.toString());

                Log.e("Save Expense Line Item", obj.toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.saveExpenseLineItemURL, obj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        /*if (dialog != null && dialog.isShowing())
                            dialog.dismiss();*/
                        btnSubmit.setEnabled(true);
                        Log.e("Save Expense Line Item", response.toString());

                        String status = response.optString("status");
                        String message = response.optString("message");
                        if (status.equalsIgnoreCase("success")) {
                            getOrdersList(false);
                            Toast.makeText(getActivity(), "Request created successfully", Toast.LENGTH_SHORT).show();
                            //  getActivity().finish();
                            imagePathDataArrayList.clear();
                            count = 0;
                            attachmentIds = "";
                            badgecount.setText(0 + "");
                            etAmount.setText("");
                            spinExpType.setSelection(0);

                            Calendar calendar = Calendar.getInstance();
                            expenseDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5,
                                    calendar.get(Calendar.DAY_OF_MONTH) + " " + (calendar.get(Calendar.MONTH) + 1) + " " + calendar.get(Calendar.YEAR));
                            etDate.setText(Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN,
                                    calendar.get(Calendar.DAY_OF_MONTH) + " " + (calendar.get(Calendar.MONTH) + 1) + " " + calendar.get(Calendar.YEAR)));
                            textdesc.setText("Tap to Enter Description");
                            position = 0;
                            desc = "";
                        } else {
                            Utils.showAlertDialog(getActivity(), message);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        btnSubmit.setEnabled(true);
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        // something to do here ??
                        return ConstantValues.getRequestHeaders();
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(request, AppURL.saveExpenseLineItemURL);

                /*if (dialog != null && !dialog.isShowing())
                    dialog.show();*/

            } catch (Exception e) {
                e.printStackTrace();
                btnSubmit.setEnabled(true);
            }

        }
    }

    public void getMasterLookupData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, AppURL.getExpenseMasterValuesURL, null, SubmitExpenseFragment.this, SubmitExpenseFragment.this, PARSER_TYPE.GET_EXPENSE_TYPES);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getExpenseMasterValuesURL);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, 1);
    }

    private void startGallery() {
        /*Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 2);*/

        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 19) {
            // For Android versions of KitKat or later, we use a
            // different intent to ensure
            // we can get the file path from the returned intent URI
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }

        intent.setType("image/*");
        startActivityForResult(intent, 2);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    int _permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if (_permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            RuntimePermissionUtils.showRationale(getActivity(),
                                    SubmitExpenseFragment.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    WRITE_EXTERNAL_PERMISSION_REQUEST_CODE,
                                    "You need to allow access to Storage");
                        } else {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    WRITE_EXTERNAL_PERMISSION_REQUEST_CODE);

                        }
                        return;
                    }

//                    startCamera();
                    dispatchTakePictureIntent();
                } else {
                    boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.CAMERA);
                    if (!showRationale) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                SubmitExpenseFragment.this,
                                Manifest.permission.READ_CONTACTS,
                                REQUEST_PERMISSION_SETTING,
                                "You need to allow permission manually from app setting",
                                true);

                    } else if (Manifest.permission.CAMERA.equals(permissions[0])) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                SubmitExpenseFragment.this,
                                Manifest.permission.READ_CONTACTS,
                                CAMERA_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Camera");

                    }

                }
                break;
            case WRITE_EXTERNAL_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startCamera();
                    dispatchTakePictureIntent();
                } else {
                    if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permissions[0])) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                SubmitExpenseFragment.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                WRITE_EXTERNAL_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Camera");

                    }

                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startGallery();
                } else {
                    if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(permissions[0])) {
                        RuntimePermissionUtils.showRationale(getActivity(),
                                SubmitExpenseFragment.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                GALLERY_PERMISSION_REQUEST_CODE,
                                "You need to allow access to Gallery");

                    }

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            int hasPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
            if (hasPermission == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            }

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            ImagePathData imagePathData = new ImagePathData();
            imagePathData.setImagePath(mCurrentPhotoPath);
            imagePathData.setImageKey(Utils.getAWSFilePath() + ".png");
//            catchpath.add(imagePathData);


            //    imagesScrollView.setVisibility(View.VISIBLE);
            //   imageContainer.addView(setPic(imagePathData));


            imagePathDataArrayList.add(imagePathData);// Add Captured image path to list
            count = imagePathDataArrayList.size();
            badgecount.setText(count + "");


        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();

            try {
                String path = getPath(uri);

                if (data != null) {
                   /* Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContext().getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();*/
                    ImagePathData imagePathData = new ImagePathData();
                    imagePathData.setImagePath(path);
                    imagePathData.setImageKey(Utils.getAWSFilePath() + ".png");
                    imagePathDataArrayList.add(imagePathData);


                    count = imagePathDataArrayList.size();

                    badgecount.setText(count + " ");
                }

                // Add picked image path to the list
//                catchpath.add(imagePathData);

                //      imagesScrollView.setVisibility(View.VISIBLE);
                //      imageContainer.addView(setPic(imagePathData));


//                beginUpload(path);
            } catch (URISyntaxException e) {
                Toast.makeText(getActivity(),
                        "Unable to get the file from the given URI.  See error log for details",
                        Toast.LENGTH_LONG).show();
                Log.e("onActivityResult", "Unable to upload file from the given uri", e);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private View setPic(ImagePathData imagePath) {
        // Get the dimensions of the View
        int targetW = (int) (100 * Resources.getSystem().getDisplayMetrics().density);
        int targetH = (int) (75 * Resources.getSystem().getDisplayMetrics().density);

        View child = getActivity().getLayoutInflater().inflate(R.layout.image_container_layout, null);
        ImageView mImageView = (ImageView) child.findViewById(R.id.image_preview);
        ImageButton mClose = (ImageButton) child.findViewById(R.id.button_close);
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (bmOptions.outWidth / scale / 2 >= REQUIRED_SIZE
                && bmOptions.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        bmOptions.inSampleSize = scale;
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath.getImagePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath.getImagePath(), bmOptions);
        mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
        mClose.setTag(imagePath);
        mClose.setOnClickListener(onDeleteClickListener);

        return child;

    }

    /*
   * Gets the file path of the given Uri.
   */
    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getActivity(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /*
     * Begins to upload the file specified by the file path.
     */
    private void beginUpload(ImagePathData filePath) {
        if (filePath == null) {
            Toast.makeText(getActivity(), "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath.getImagePath());
//        TransferObserver observer = transferUtility.upload(ConstantValues.AWS_BUCKET_NAME, filePath.getImageKey(), file);
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
//        observer.setTransferListener(new UploadListener());
//        observerArrayList.add(observer);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
        }
        Uri photoURI = Uri.fromFile(photoFile); // create
        if (photoFile != null) {
            try {
                mCurrentPhotoPath = getPath(photoURI);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            i.putExtra(MediaStore.EXTRA_OUTPUT, photoURI); // set the image file
            startActivityForResult(i, 1);
        } else {
            Toast.makeText(getActivity(), "Unable to create URI", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
        if (response != null) {

            if (requestType == PARSER_TYPE.DEFAULT) {
                try {

                    if (data != null)
                        data.clear();
                    else
                        data = new ArrayList<>();

                    //    data=new ArrayList<NewExpenseModel>();
                    JSONObject obj = new JSONObject((String) response);

                    String _status = obj.optString("status");
                    if (_status.equals("success")) {
                        JSONArray dataArray = obj.optJSONArray("data");
                        if (dataArray != null && dataArray.length() > 0) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject object = dataArray.optJSONObject(i);

                                String expDetId = object.optString("exp_det_id");
                                String amount = object.optString("ExpDetAmount");
                                String date = object.optString("ExpDetDate");
                                String expenseType = object.optString("ExpDetType");
//                            model.setExpenseTypeId(cursor.getString(cursor.getColumnIndex(KEY_EXPENSE_TYPE_ID)));
                                String desc = object.optString("Description");
                                String attachments = object.optString("ProofImageKey");

                                NewExpenseModel model = new NewExpenseModel(expDetId, amount, date, expenseType, "", desc, attachments);
                                data.add(model);
                            }

                        }
                    }

                    if (data != null && data.size() > 0) {
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        rvOrderList.setVisibility(View.VISIBLE);

                        adapter = new UnclaimedRecyclerAdapter(getActivity(), data, 1);
                        adapter.setClickListener(SubmitExpenseFragment.this);
                        rvOrderList.setLayoutManager(new LinearLayoutManager(getActivity()));
                        rvOrderList.setAdapter(adapter);
                        textView.setVisibility(View.GONE);
                        allsubmit.setVisibility(View.VISIBLE);

                    } else {
                        rvOrderList.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        allsubmit.setVisibility(View.GONE);
                        textView.setVisibility(View.VISIBLE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_EXPENSE_TYPES) {
                    if (response instanceof ExpenseMasterlookupModel) {

                        ExpenseMasterlookupModel model = (ExpenseMasterlookupModel) response;
                        ArrayList<ExpenseMasterLookupData> expenses = model.getExpenseTypes();
                        ArrayList<ExpenseMasterLookupData> requestTypes = model.getRequestTypes();

                        if (_submitType == 1) {//type 1 == for Expense
                            if (expenses != null && expenses.size() > 0) {
                                ExpenseTypeSpinnerAdapter adapter = new ExpenseTypeSpinnerAdapter(getActivity(), expenses);
                                spinExpType.setAdapter(adapter);
                            }
                        } else if (_submitType == 2) {// type 2 == for Advance
                            if (requestTypes != null && requestTypes.size() > 0) {
                                ExpenseTypeSpinnerAdapter adapter = new ExpenseTypeSpinnerAdapter(getActivity(), requestTypes);
                                spinExpType.setAdapter(adapter);

                            }
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }

        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {

    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {

        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onItemClick(String ids) {

        // String getattachments=expenseModelArrayList.get(position).getAttachments();
        if (ids.length() > 0) {
            Intent intent = new Intent(getActivity(), AttachmentsActivity.class);
            intent.putExtra("Attachments", ids);
            startActivity(intent);
        } else {

            Toast.makeText(getActivity(), " No Images are Attached ", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onItemDel(String ids) {

        deleteUnClaimedExpenses(ids);
    }

    @Override
    public void onItemClick(int adapterPosition) {

    }

    public void getOrdersList(boolean showLoader) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
                String url = AppURL.getExpensesLineItemsURL + "?UserID=" + userId + "&RecordType=0";
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, url, null, SubmitExpenseFragment.this, SubmitExpenseFragment.this, PARSER_TYPE.DEFAULT);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);

                if (showLoader) {
                    dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                    dialog.show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void deleteUnClaimedExpenses(String commaSeparatedIds) {
        if (Networking.isNetworkAvailable(getActivity())) {

            dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
            dialog.show();

            String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
            try {
                JSONObject obj = new JSONObject();
                obj.put("unclaimedID", commaSeparatedIds);
                obj.put("UserID", userId);

                Log.e("request Obj", obj.toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, AppURL.deleteUnClaimedExpensesURL, obj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        /*if (dialog != null && dialog.isShowing())
                            dialog.dismiss();*/

                        String status = response.optString("status");
                        String message = response.optString("message");
                        if (status.equalsIgnoreCase("success")) {
                            getOrdersList(false);
                            Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();

                        } else {
                            Utils.showAlertDialog(getActivity(), message);
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*if (dialog != null && dialog.isShowing())
                            dialog.dismiss();*/
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        // something to do here ??
                        return ConstantValues.getRequestHeaders();
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(request, AppURL.deleteUnClaimedExpensesURL);

                /*if (dialog != null && !dialog.isShowing())
                    dialog.show();*/

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getOrdersList(false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private class UploadImageTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*if (position == 0)
                progressDialog = ProgressDialog.show(getActivity(), "", "Please wait..!", true);*/
            /*else
                progressDialog = new ProgressDialog(getContext());*/
        }

        @Override
        protected String doInBackground(String... strings) {
            return uploadFile(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s != null) {
                try {
                    JSONObject resultObj = new JSONObject(s);
                    String status = resultObj.optString("status");
                    String message = resultObj.optString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject dataObj = resultObj.optJSONObject("data");
                        String url = dataObj.optString("url");
                        imagePathDataArrayList.get(position).setImageKey(url);
                        position++;
                        if (position < imagePathDataArrayList.size()) {
                            new UploadImageTask().execute(imagePathDataArrayList.get(position).getImagePath());
                        } else {
                            //upload images are done.. Now upload the expense
                            /*if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();*/

                            if (count == position)
                                submitRequest(newExpenseModel);
                        }
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}

