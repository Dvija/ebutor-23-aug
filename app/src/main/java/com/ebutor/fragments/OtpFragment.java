package com.ebutor.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;

public class OtpFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object> {

    private EditText etOtp;
    private Button btnOk, btnResendOtp, btnGetOtp;
    private String mobileNo, otp;
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    //    private OnUpdate onUpdate;
    private boolean isFromPayment = false;
    private TextView tvCancel;

//    public static OtpFragment newInstance(String MobileNo, boolean isFromPayment) {
//        OtpFragment frag = new OtpFragment();
//        Bundle args = new Bundle();
//        args.putString("MobileNo", MobileNo);
//        args.putBoolean("isFromPayment", isFromPayment);
//        frag.setArguments(args);
//        frag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
//        return frag;
//    }

//    public void setClickListener(OnUpdate listener) {
//        onUpdate = listener;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        if (getArguments().containsKey("MobileNo")) {
            mobileNo = getArguments().getString("MobileNo");
        }
        if (getArguments().containsKey("isFromPayment"))
            isFromPayment = getArguments().getBoolean("isFromPayment");
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);
        return inflater.inflate(R.layout.fragment_otp, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etOtp = (EditText) view.findViewById(R.id.et_otp);
        btnOk = (Button) view.findViewById(R.id.bt_confirm_otp);
        btnResendOtp = (Button) view.findViewById(R.id.bt_resend_otp);
        btnGetOtp = (Button) view.findViewById(R.id.bt_get_otp);
        tvCancel = (TextView) view.findViewById(R.id.tv_cancel);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                btnResendOtp.setAlpha(1);
                btnResendOtp.setEnabled(true);

            }
        }, 30 * 1000);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                etOtp.setText("");
                resendOtp();
                btnResendOtp.setAlpha(0.5f);
                btnResendOtp.setEnabled(false);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        btnResendOtp.setEnabled(true);
                        btnResendOtp.setAlpha(1);

                    }
                }, 30 * 1000);
            }
        });

        btnGetOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                getOtp();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null)
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                otp = etOtp.getText().toString();
                if (TextUtils.isEmpty(otp)) {
                    etOtp.setError(getResources().getString(R.string.please_enter_otp));
                    etOtp.requestFocus();
                    return;
                }
                if (etOtp.length() < 6) {
                    etOtp.setError(getResources().getString(R.string.please_enter_valid_otp));
                    etOtp.requestFocus();
                    return;
                }
                if (isFromPayment) {
                    btnGetOtp.setVisibility(View.GONE);
                    orderOtpConfirm();
//                    onUpdate.updateOtp(etOtp.getText().toString());
                } else {
                    btnGetOtp.setVisibility(View.VISIBLE);
                    Otp();
                }
            }
        });
    }

    private void getOtp() {
        if (Networking.isNetworkAvailable(getActivity())) {

            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                String customerToken = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, "");
                jsonObject.put("customer_token", customerToken);
                if (mobileNo != null && mobileNo.length() > 0) {
                    jsonObject.put("telephone", mobileNo);
                }
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getOTPURL, map, OtpFragment.this, OtpFragment.this, PARSER_TYPE.GET_OTP);
                wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.getOTPURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {

            }

        } else {

        }

    }

    private void orderOtpConfirm() {
        if (Networking.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, null));
                jsonObject.put("telephone", mobileNo);
                jsonObject.put("otp", otp);
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.orderotpconfirmURL, map,
                        OtpFragment.this, OtpFragment.this, PARSER_TYPE.ORDER_OTP_CONFIRM);
                productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.orderotpconfirmURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {

            }

        } else {
        }
    }

    public void Otp() {
        if (Networking.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject jsonObject = new JSONObject();
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, null));
                jsonObject.put("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));
                jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                if (otp != null && otp.length() > 0) {
                    jsonObject.put("otp_sent", otp);
                }
                jsonObject.put("flag", "3");
                if (mobileNo != null && mobileNo.length() > 0) {
                    jsonObject.put("telephone", mobileNo);
                }
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateProfile1URL, map,
                        OtpFragment.this, OtpFragment.this, PARSER_TYPE.OTP);
                productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.updateProfile1URL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {

            }

        } else {
        }
    }

    public void resendOtp() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {

                JSONObject obj = new JSONObject();

                try {
                    obj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("telephone", mSharedPreferences.getString(ConstantValues.KEY_MOBILE, ""));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.generateotpURL, map, OtpFragment.this, OtpFragment.this, PARSER_TYPE.GENERATE_OTP);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.generateotpURL);

                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.OTP) {
                        if (response instanceof String) {
                            String mobile = (String) response;

                            Intent intent = getActivity().getIntent();
                            intent.putExtra("mobile", mobile);
                            getActivity().setResult(Activity.RESULT_OK, intent);
                            getActivity().finish();
//                            onUpdate.updateOtp(mobile);
                        }
                    } else if (requestType == PARSER_TYPE.ORDER_OTP_CONFIRM) {
                        if (response instanceof String) {
                            Intent intent = getActivity().getIntent();
                            getActivity().setResult(Activity.RESULT_OK, intent);
                            getActivity().finish();
                        }
                    } else if (requestType == PARSER_TYPE.RESEND_OTP) {

                    } else if (requestType == PARSER_TYPE.GET_OTP) {
                        if (status.equalsIgnoreCase("success")) {
                            if (response instanceof String) {
                                otp = (String) response;
                                etOtp.setText(otp);
                                etOtp.setSelection(etOtp.length());
                            }
                        } else {
                            Utils.showAlertWithMessage(getActivity(), message);
                        }
                    }
                } else {
                    if (requestType == PARSER_TYPE.ORDER_OTP_CONFIRM)
                        etOtp.setText("");
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    /*public interface OnUpdate {
        public void updateOtp(String mobile);
    }*/
}
