package com.ebutor.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.ExpenseDetailActivity;
import com.ebutor.adapters.AllExpensesAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ExpenseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DividerItemDecoration;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import java.util.ArrayList;

public class GetAllExpensesFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, AllExpensesAdapter.OnItemClickListener {

    private SharedPreferences mSharedPreferences;
    private RecyclerView rvExpenses;
    private Dialog dialog;
    private TextView tvNoResults;
    private ArrayList<ExpenseModel> arrExpenses = new ArrayList<>();
    private AllExpensesAdapter allExpensesAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_expense_tracker, container, false);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        tvNoResults = (TextView) view.findViewById(R.id.tvNoOrders);
        rvExpenses = (RecyclerView) view.findViewById(R.id.rvOrdersList);
        rvExpenses.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        rvExpenses.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                String userId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
                String url = AppURL.getAllUsersExpensesURL + "?UserID=" + userId;
                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.GET, url, null, GetAllExpensesFragment.this, GetAllExpensesFragment.this, PARSER_TYPE.GET_ALL_EXPENSES);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, url);

                dialog = ProgressDialog.show(getActivity(), "", "Please wait..", true);
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }

    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {

                if (requestType == PARSER_TYPE.GET_ALL_EXPENSES) {
                    if (response instanceof ArrayList) {
                        arrExpenses = (ArrayList<ExpenseModel>) response;

                        if (arrExpenses != null && arrExpenses.size() > 0) {
                            rvExpenses.setVisibility(View.VISIBLE);
                            tvNoResults.setVisibility(View.GONE);

                            allExpensesAdapter = new AllExpensesAdapter(getActivity(), arrExpenses);
                            allExpensesAdapter.setClickListener(GetAllExpensesFragment.this);
                            rvExpenses.setLayoutManager(new LinearLayoutManager(getActivity()));
                            rvExpenses.setAdapter(allExpensesAdapter);

                        } else {
                            rvExpenses.setVisibility(View.GONE);
                            rvExpenses.setVisibility(View.VISIBLE);
                            tvNoResults.setText(getString(R.string.no_results));
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.something_wrong));
        }

    }

    @Override
    public void onSessionError(String message) {

    }

    @Override
    public void onItemClick(Object object) {
        if (object != null && object instanceof ExpenseModel) {
            ExpenseModel expenseModel = (ExpenseModel) object;
            Intent intent = new Intent(getActivity(), ExpenseDetailActivity.class);
            intent.putExtra("ExpenseCode", expenseModel.getExpenseCode());
            intent.putExtra("ExpenseModel", expenseModel);
            getActivity().startActivity(intent);
        }

    }
}
