package com.ebutor.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ExpandableListView;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.ExpandListAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.TeamDashboard;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class MyTeamDashBoardFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object> {


    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    //    private TextView tvTBV, tvUOB, tvABV, tvTLC, tvULC, tvALC;
    private ExpandListAdapter mAdapter;
    private ExpandableListView expandableListView;

    private int type = DashBoardTypes.ORGANIZATION;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);

    public static MyTeamDashBoardFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("type", page);
        MyTeamDashBoardFragment fragment = new MyTeamDashBoardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_team_dashboard, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        expandableListView = (ExpandableListView) view.findViewById(R.id.exp_list);

        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);
        if (getArguments() != null && getArguments().containsKey("type")) {
            type = getArguments().getInt("type");
        }


        getDashboard(createJSON(null));

    }

    private void getDashboard(JSONObject dataObject) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.retailerDashboardURL, map, MyTeamDashBoardFragment.this, MyTeamDashBoardFragment.this, PARSER_TYPE.MY_TEAM_DASHBOARD);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.retailerDashboardURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.calendar) {
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.show(getChildFragmentManager(), "datePicker");
            newFragment.setListener(new DatePickerDialog.OnDateSetListener() {
                /**
                 * @param view        The view associated with this listener.
                 * @param year        The year that was set.
                 * @param monthOfYear The month that was set (0-11) for compatibility
                 *                    with {@link Calendar}.
                 * @param dayOfMonth  The day of the month that was set.
                 */
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String date = format.format(calendar.getTime());
                    getDashboard(createJSON(date));
                }
            });
        } else if (item.getItemId() == R.id.refresh) {
            getDashboard(createJSON(null));
        }
        return super.onOptionsItemSelected(item);
    }

    private JSONObject createJSON(String date) {
        String customerId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
        JSONObject dataObj = null;
        if (!TextUtils.isEmpty(customerId)) {
            try {
                dataObj = new JSONObject();
                if (null == date)
                    date = getDate();
                dataObj.put("start_date", date);
                dataObj.put("end_date", date);
                dataObj.put("flag", type);
                dataObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
        return dataObj;
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    switch (type) {

                        case DashBoardTypes.MY_TEAM_DASHBOARD:
                            getTeamDashboard(response);
                            break;
                    }

                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void getTeamDashboard(Object response) {

        if (response instanceof ArrayList) {
//            RetailerDashboard retailerDashboard = (RetailerDashboard) response;
            ArrayList<TeamDashboard> dashboardModelArrayList = (ArrayList<TeamDashboard>) response;

            // Initialize a new instance of RecyclerView Adapter instance
            mAdapter = new ExpandListAdapter(getActivity(), dashboardModelArrayList);
            expandableListView.setAdapter(mAdapter);


        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private DatePickerDialog.OnDateSetListener listener;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            if (listener != null) {
                listener.onDateSet(view, year, month, day);
            }
        }


        public void setListener(DatePickerDialog.OnDateSetListener listener) {
            this.listener = listener;
        }
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

    public class MarginDecoration extends RecyclerView.ItemDecoration {
        private int margin;

        public MarginDecoration(Context context) {
            margin = context.getResources().getDimensionPixelSize(R.dimen.dp_10);
        }

        @Override
        public void getItemOffsets(
                Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(margin, margin, margin, margin);
        }
    }

}