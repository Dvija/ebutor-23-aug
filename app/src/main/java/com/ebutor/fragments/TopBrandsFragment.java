package com.ebutor.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.BrandProductsActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.TopBrandsActivity;
import com.ebutor.adapters.TopBrandsAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.ResultHandler;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.TopBrandsModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.ebutor.utils.fastscroll.AlphabetItem;
import com.ebutor.utils.fastscroll.RecyclerViewFastScroller;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by 300024 on 3/17/2016.
 */
public class TopBrandsFragment extends Fragment implements ResultHandler, TopBrandsAdapter.OnItemClickListener, VolleyHandler<Object>, Response.ErrorListener {

    RecyclerView topBrandsGridView;
    ArrayList<TopBrandsModel> arrTopBrands;
    GridLayoutManager manager;
    TopBrandsAdapter topBrandsAdapter;
    RecyclerViewFastScroller fastScroller;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<AlphabetItem> mAlphabetItems;
    //    ViewPager viewPager;
    private Tracker mTracker;
    private SharedPreferences mSharedPreferences;
    private TextView tvAlertMsg;
    private RelativeLayout rlAlert;
    private LinearLayout llMain;
    private String requestType = "", flag = "", keyId = "";
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private Dialog dialog;
    private int cartCount = 0;
    private DBHelper dataBaseObj;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        flag = getArguments().getString("Flag");
        keyId = getArguments().getString("key_id");
        View view = null;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_top_brand, container, false);

            topBrandsGridView = (RecyclerView) view.findViewById(R.id.products_grid_view);
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
            mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
            tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
            rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
            llMain = (LinearLayout) view.findViewById(R.id.ll_main);
            fastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.fast_scroller);

            dataBaseObj = new DBHelper(getActivity());
            dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

            int syncTimeInterval = mSharedPreferences.getInt(ConstantValues.KEY_SYNC_INTERVAL, ConstantValues.KEY_SYNC_INTERVAL_DURATION);
            String past = Utils.getPastDateTime(syncTimeInterval);

            switch (keyId) {
                case "brand_id":
                    arrTopBrands = dataBaseObj.getHomeBrands(past);
                    break;
                case "manufacturer_id":
                    arrTopBrands = dataBaseObj.getHomeManufacturers(past);
                    break;
                case "category_id":
                    arrTopBrands = dataBaseObj.getHomeCategories(past);
                    break;
            }

            if (arrTopBrands != null && arrTopBrands.size() > 0) {
                setData(false);
            } else {
                getTopBrands(false);
            }

            MyApplication application = (MyApplication) getActivity().getApplication();

//            arrTopBrands = new ArrayList<>();

//Calling getDefaultTracker method.It returns tracker
            mTracker = application.getDefaultTracker();


            tvAlertMsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Networking.isNetworkAvailable(getActivity())) {
                        switch (requestType) {
                            case "GetTopBrands":
                                getTopBrands(false);
                                break;
                            default:
                                getTopBrands(false);
                                break;
                        }
                    } else {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getTopBrands(true);
                }
            });

        } catch (InflateException e) {
            e.printStackTrace();
        }
        return view;
    }


    public void getTopBrands(boolean isSwipeDown) {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "GetTopBrands";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject detailsObj = new JSONObject();
                detailsObj.put("flag", flag);
                detailsObj.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                detailsObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("offset", "0");
                detailsObj.put("offset_limit", "0");

                map.put("data", detailsObj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.homePageBannersURL, map, TopBrandsFragment.this, TopBrandsFragment.this, PARSER_TYPE.GET_TOP_BRANDS);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.homePageBannersURL);
            if (dialog != null && !isSwipeDown)
                dialog.show();
        } else {
            requestType = "GetTopBrands";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }

    }

    @Override
    public void onFinish(Object results, PARSER_TYPE requestType) {
        if (results != null) {
            String response = (String) results;
            if (!TextUtils.isEmpty(response) && requestType == PARSER_TYPE.GET_TOP_BRANDS) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray topBrandsArray = jsonObject.getJSONArray("data");
                        if (topBrandsArray != null && topBrandsArray.length() > 0) {
                            for (int i = 0; i < topBrandsArray.length(); i++) {
                                JSONObject topBrandObject = topBrandsArray.getJSONObject(i);
                                if (topBrandObject != null) {
                                    String manufacturedId = topBrandObject.getString("manufacturer_id");
                                    String name = topBrandObject.getString("name");
                                    String image = topBrandObject.getString("image");

                                    TopBrandsModel model = new TopBrandsModel();
                                    model.setManufacturerId(manufacturedId);
                                    model.setTopBrandName(name);
                                    arrTopBrands.add(model);
                                }
                            }

                        }

                        topBrandsAdapter = new TopBrandsAdapter(getActivity(), arrTopBrands);
                        topBrandsAdapter.setClickListener(TopBrandsFragment.this);
//                        topBrandsAdapter.setEventListener(TopBrandsFragment.this);
                        manager = new GridLayoutManager(getActivity(), 3);
                        topBrandsGridView.setLayoutManager(manager);

                        topBrandsGridView.setAdapter(topBrandsAdapter);

                    } else {
                        Utils.showAlertDialog(getActivity(), message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(String errorCode, PARSER_TYPE requestType) {
        if (errorCode.equalsIgnoreCase("IOException") || errorCode.equalsIgnoreCase("Connection Error")) {
            if (requestType == PARSER_TYPE.GET_TOP_BRANDS) {
                this.requestType = "GetTopBrands";
            }
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(int pos) {

        Utils.callCrashlyticsViewEvent(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "0"), keyId, arrTopBrands.get(pos).getManufacturerId());

        Intent intent = new Intent(getActivity(), BrandProductsActivity.class);
        intent.putExtra("ManufacturerId", arrTopBrands.get(pos).getManufacturerId());
        intent.putExtra("Flag", flag);
        intent.putExtra("key_id", keyId);
        startActivity(intent);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null)
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        swipeRefreshLayout.setRefreshing(false);
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_TOP_BRANDS) {
                    if (results instanceof ArrayList) {
                        arrTopBrands = (ArrayList<TopBrandsModel>) results;
                        switch (keyId) {
                            case "brand_id":
                                dataBaseObj.deleteTable(DBHelper.TABLE_BRANDS);
                                break;
                            case "manufacturer_id":
                                dataBaseObj.deleteTable(DBHelper.TABLE_MANUFACTURERS);
                                break;
                            case "category_id":
                                dataBaseObj.deleteCategories();
                                break;
                        }

                        setData(true);
                    }

                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void setData(boolean flag) {
        Collections.sort(arrTopBrands, new Comparator<TopBrandsModel>() {
            public int compare(TopBrandsModel v1, TopBrandsModel v2) {
                return v1.getTopBrandName().compareToIgnoreCase(v2.getTopBrandName());
            }
        });

        //Alphabet fast scroller data
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < arrTopBrands.size(); i++) {
            if (flag) {
                switch (keyId) {
                    case "brand_id":
                        dataBaseObj.insertBrand(arrTopBrands.get(i));
                        break;
                    case "manufacturer_id":
                        dataBaseObj.insertManufacturer(arrTopBrands.get(i));
                        break;
                    case "category_id":
                        dataBaseObj.insertHomeCategory(arrTopBrands.get(i));
                        break;
                }
            }
            String name = arrTopBrands.get(i).getTopBrandName();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1).toUpperCase();
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }

        topBrandsAdapter = new TopBrandsAdapter(getActivity(), arrTopBrands);
        topBrandsAdapter.setClickListener(TopBrandsFragment.this);
        manager = new GridLayoutManager(getActivity(), 3);
        topBrandsGridView.setLayoutManager(manager);

        topBrandsGridView.setAdapter(topBrandsAdapter);

        fastScroller.setRecyclerView(topBrandsGridView);
        fastScroller.setUpAlphabet(mAlphabetItems);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartCount();
        mTracker.setScreenName("Top Brands Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        ((TopBrandsActivity) getActivity()).updateCart(cartCount);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}