package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.PaymentActivity;
import com.ebutor.R;
import com.ebutor.adapters.CartListAdapter;
import com.ebutor.adapters.CartListAdapter.onCartItemDeleteListener;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CartModel;
import com.ebutor.models.CheckCartInventoryModel;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.DiscountModel;
import com.ebutor.models.NewProductModel;
import com.ebutor.models.OrderLevelCashBackModel;
import com.ebutor.models.ProductSlabData;
import com.ebutor.models.StarLevelCashBackDetailsModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class CartFragment extends Fragment implements onCartItemDeleteListener, ItemCartFragment.OnDialogClosed, Response.ErrorListener, VolleyHandler<Object> {

    double additionalcashBackValue = 0, cashbackValue = 0,passingcashback=0;
    private RecyclerView mRecyclerView;
    private CartListAdapter cartListAdapter;
    //    private ArrayList<ProductModel> arrCart;
//    private ProductModel selectedProductObject = null, productModel;
//    private ImageView ivCart;
    private Tracker mTracker;
    private ArrayList<String> cbk_ids_list;
    private Button btnProceed;
    private LinearLayout llCart;
    private TextView tvGrandTotal, tvNoCart, tvTotalAmount, tvDiscountAmount, tvAdditonalCashBack, tvCashBack;
    private int totalItems = 0;
    private double totalCartPrice = 0, totalDiscount = 0;
    private SharedPreferences mSharedPreferences;
    private int cartCount = 0;
    private String cartId = "";
    private TextView tvBadge, tvAlertMsg;
    private RelativeLayout rlAlert;
    private LinearLayout llMain, llDiscount;
    private String requestType = "", beatId = "";
    private Dialog dialog;
    private ArrayList<StarLevelCashBackDetailsModel> starLevelCashBackArray;
    BeatFragment.OnSelListener okListener = new BeatFragment.OnSelListener() {

        @Override
        public void onSelected(String beatId) {
            CartFragment.this.beatId = beatId;
            if (TextUtils.isEmpty(beatId)) {
                Toast.makeText(getActivity(), getString(R.string.beat_empty), Toast.LENGTH_SHORT).show();
            } else if (beatId.equalsIgnoreCase("0")) {
                Toast.makeText(getActivity(), getString(R.string.could_not_place_order_beat), Toast.LENGTH_SHORT).show();
            } else {
                mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, beatId).apply();
                updateBeat();
            }
        }
    };
    private String hubId = "";
    private ArrayList<CartModel> cartItemsList;
    private CartModel selectedCartModel;
    private DiscountModel orderDiscountModel;
    private boolean isClicked = false, unavailable = false, missMatch = false, valueMissMatch = false,isSalesAgent = false, isFF/*, isOrderDiscount = false*/;
    private ArrayList<CustomerTyepModel> arrBeats;
    private ArrayList<OrderLevelCashBackModel> arrOrderLevelCashBackModel;

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        starLevelCashBackArray = new ArrayList<>();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cartItemsList = new ArrayList<>();
        cbk_ids_list = new ArrayList<>();
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);
        isSalesAgent = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_SALES_AGENT, false);
        hubId = mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "");

        mRecyclerView = (RecyclerView) view.findViewById(R.id.scrollable);
        llCart = (LinearLayout) view.findViewById(R.id.llCart);
        tvNoCart = (TextView) view.findViewById(R.id.tvNoCart);
        btnProceed = (Button) view.findViewById(R.id.btnProceed);
//        tvTotalItems = (TextView) view.findViewById(R.id.tvTotalItems);
        tvGrandTotal = (TextView) view.findViewById(R.id.tvGrandTotal);
        tvDiscountAmount = (TextView) view.findViewById(R.id.tvDiscountAmount);
        tvTotalAmount = (TextView) view.findViewById(R.id.tvTotalAmount);
        tvCashBack = (TextView) view.findViewById(R.id.tv_cashback);
        tvAdditonalCashBack = (TextView) view.findViewById(R.id.tv_additional_cashback);
//        ivCart = (ImageView) view.findViewById(R.id.ivCart);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        llDiscount = (LinearLayout) view.findViewById(R.id.ll_discount);
        exportDatabase(DBHelper.DATABASE_NAME);

//        if (isFF) {
        tvDiscountAmount.setVisibility(View.GONE);
//        } else {
//            llDiscount.setVisibility(View.VISIBLE);
//        }

        MyApplication application = (MyApplication) getActivity().getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        arrBeats = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        tvBadge = (TextView) view.findViewById(R.id.tvBadge);
//        tvBadge.setText(String.valueOf(MyApplication.getInstance().cartSize(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""))));

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((TextUtils.isEmpty(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_BEAT_ID, "")) || mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_BEAT_ID, "").equalsIgnoreCase("0"))) {
                    if (isFF) {
                        showBeatUpdateDialog();
                    } else {
                        showBeatDialog();
                    }

                    return;
                }


                checkInventoryForCart();
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestType) {
                        case "ViewCart":
                            viewCart();
                            break;
                        case "Proceed":
                            checkInventoryForCart();
                            break;
                        default:
                            viewCart();
                            break;
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cartListAdapter = new CartListAdapter(getActivity(), cartItemsList);
        cartListAdapter.setListener(CartFragment.this);
        mRecyclerView.setAdapter(cartListAdapter);
    }

    private void proceedToNextOld() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "Proceed";
            Intent intent = new Intent(getActivity(), PaymentActivity.class);
            if (cartId != null) {
                intent.putExtra("cartId", cartId);
            }
            intent.putExtra("TotalAmount", totalCartPrice);
            intent.putExtra("TotalItems", totalItems);
            startActivity(intent);
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "Proceed";
        }
    }

    private void checkInventoryForCart() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject dataObject = createJSONForCheckInventory();
            if (dataObject == null) {
                return;
            }
            callCheckInventoryAPI(dataObject);

        } else {
            //todo show network alert
        }
    }

    private void callCheckInventoryAPI(JSONObject dataObject) {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "CheckInventory";
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                isClicked = true;
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.checkCartInventoryURL, map, CartFragment.this, CartFragment.this, PARSER_TYPE.CHECK_CART_INVENTORY);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.checkCartInventoryURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "CheckInventory";
        }
    }

    private JSONObject createJSONForCheckInventory() {
        String customerId = mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "");
        JSONObject dataObj = null;
        if (!TextUtils.isEmpty(customerId)) {
            try {
                dataObj = new JSONObject();
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    dataObj.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    dataObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }
                dataObj.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                dataObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                dataObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                dataObj.put("hub", hubId);
                /*if (orderDiscountModel != null) {
                    dataObj.put("discount", orderDiscountModel.getDiscount());
                    dataObj.put("discount_type", orderDiscountModel.getDiscountType());
                    dataObj.put("discount_on", orderDiscountModel.getDiscountOn());
                    dataObj.put("discount_on_values", orderDiscountModel.getDiscountValues());
                }*/

                ArrayList<CartModel> cartItems = DBHelper.getInstance().getCartItems(customerId, true);
                JSONArray productsArray = new JSONArray();
                if (cartItems != null && cartItems.size() > 0) {
                    for (CartModel model : cartItems) {
                        JSONObject object = new JSONObject();
                        object.put("product_id", model.getProductId());
                        object.put("parent_id", model.getParentId());
                        if (model.getQuantity() == 0) {
                            Utils.showAlertWithMessage(getActivity(), getString(R.string.qty_0));
                            return null;
                        }
                        object.put("total_qty", model.getQuantity());
                        object.put("esu_quantity", model.getEsu());
                        object.put("total_price", model.getTotalPrice());
                        object.put("applied_margin", model.getMargin());
                        object.put("unit_price", model.getUnitPrice());
                        object.put("is_slab", model.getIsSlab());
                        object.put("blocked_qty", model.getBlockedQty());
                        object.put("star", model.getStar());
                        object.put("hub", hubId);
                        object.put("prmt_det_id", String.valueOf(model.getPrmtDetId()));
                        object.put("product_slab_id", String.valueOf(model.getProductSlabId()));
                        object.put("pack_level", String.valueOf(model.getPackLevel()));
                        object.put("esu", String.valueOf(model.getSlabEsu()));
                        object.put("freebee_mpq", String.valueOf(model.getFreebieMpq()));
                        object.put("freebee_qty", String.valueOf(model.getFreebieFq()));
                        /*if (!isFF*//* && !isOrderDiscount*//*) {
                            String starValue = DBHelper.getInstance().getStarColorValue(model.getStar());
                            DiscountModel discountModel = DBHelper.getInstance().getDiscountData("star", starValue);
                            if (discountModel != null) {
                                object.put("discount", discountModel.getDiscount());
                                object.put("discount_type", discountModel.getDiscountType());
                                object.put("discount_on", discountModel.getDiscountOn());
                                object.put("discount_on_values", discountModel.getDiscountValues());
                            }
                        }*/
                        JSONArray productSlabDataArray = new JSONArray();
                        ArrayList<ProductSlabData> arrProductSlabData = DBHelper.getInstance().getProductSlabData(model.getProductId(), mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                        if (arrProductSlabData != null && arrProductSlabData.size() > 0) {
                            for (ProductSlabData productSlabData : arrProductSlabData) {
                                JSONObject productSlabObj = new JSONObject();

                                productSlabObj.put("esu", String.valueOf(productSlabData.getEsu()));
                                productSlabObj.put("qty", String.valueOf(productSlabData.getQty()));
                                productSlabObj.put("pack_qty", String.valueOf(productSlabData.getPackQty()));
                                productSlabObj.put("pack_size", String.valueOf(productSlabData.getPackSize()));
                                productSlabObj.put("pack_level", String.valueOf(productSlabData.getLevel()));
                                productSlabObj.put("star", String.valueOf(productSlabData.getStar()));
                                productSlabObj.put("pack_cashback", String.valueOf(productSlabData.getCashBackIds()));

                                productSlabDataArray.put(productSlabObj);
                            }
                        }
                        object.put("packs", productSlabDataArray);
                        productsArray.put(object);
                    }
                }
                dataObj.put("products", productsArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
        return dataObj;
    }

    private void showBeatDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage(getString(R.string.your_account_under_activation));
        dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                sendMailToFF();
                deleteCart();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private void showBeatUpdateDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage(getString(R.string.please_update_beat));
        dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getBeats();
            }
        });
        dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private void updateBeat() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("customer_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));
                jsonObject.put("beat_id", beatId);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updateBeatURL, map, CartFragment.this, CartFragment.this, PARSER_TYPE.UPDATE_BEAT);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.updateBeatURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void getBeats() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getbeatsURL, map, CartFragment.this, CartFragment.this, PARSER_TYPE.GET_BEATS);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getbeatsURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    public void exportDatabase(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" +
                        getActivity().getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "Ebutor.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    private void sendMailToFF() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                jsonObject.put("customer_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.sendMailToFFURL, map, CartFragment.this, CartFragment.this, PARSER_TYPE.SEND_MAIL_TO_FF);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.sendMailToFFURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        getActivity().invalidateOptionsMenu();
        if (tvBadge != null) {
            tvBadge.setText(String.valueOf(this.cartCount));
        }
        totalCartPrice = Utils.getCartValue(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        cashbackValue = Utils.getCashbackValue(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        tvCashBack.setText(getString(R.string.cashback) + " : " + String.format(Locale.CANADA, "%.2f", cashbackValue));
        tvTotalAmount.setText(getString(R.string.total_value_amount) + " : " + String.format(Locale.CANADA, "%.2f", totalCartPrice));
        tvGrandTotal.setText(String.format(Locale.CANADA, "%.2f", totalCartPrice));
    }

    private void viewCart() {
        if (cartItemsList != null)
            cartItemsList.clear();
        if (cartListAdapter != null)
            cartListAdapter.clearData();

        additionalcashBackValue = 0;
        cartItemsList.addAll(DBHelper.getInstance().getCartItems(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""), false));

        if (cartItemsList != null && cartItemsList.size() > 0) {
            if (isClicked && unavailable) {
                Utils.showAlertDialog(getActivity(), getString(R.string.unavailable_inventory));
                isClicked = false;
            }
            if (isClicked && missMatch) {
                Utils.showAlertDialog(getActivity(), getString(R.string.price_miss_match));
                isClicked = false;
            }
            if (isClicked && valueMissMatch) {
                Utils.showAlertDialog(getActivity(), getString(R.string.value_miss_match));
                isClicked = false;
            }
            cartListAdapter.setArrCart(cartItemsList);
            cartListAdapter.notifyDataSetChanged();

            calculateAdditionalCashback();

            /*if (!isFF) {
                totalDiscount = 0;
                totalCartPrice = Utils.getCartValue(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                orderDiscountModel = DBHelper.getInstance().getOrderDiscountData("order", totalCartPrice);

                *//*for (int i = 0; i < cartItemsList.size(); i++) {
                    CartModel cartModel = cartItemsList.get(i);
                    String starValue = DBHelper.getInstance().getStarColorValue(cartModel.getStar());
                    DiscountModel discountModel = DBHelper.getInstance().getDiscountData("star", starValue);
                    if (discountModel != null) {
                        double discountPrice = 0, discountAmt = 0;
                        try {
                            discountPrice = Double.parseDouble(discountModel.getDiscount());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (discountModel.getDiscountType().equalsIgnoreCase("percentage"))
                            discountAmt = discountPrice * cartModel.getTotalPrice() / 100;
                        else if (discountModel.getDiscountType().equalsIgnoreCase("value"))
                            discountAmt = discountPrice;
                        discountAmt = ((cartModel.getTotalPrice() - discountAmt) <= 0) ? 0 : discountAmt;

                        totalDiscount += discountAmt;
                    }
                }*//*

                if (orderDiscountModel != null) {
//                    isOrderDiscount = true;
                    double discountPrice = 0, discountAmt = 0;
                    try {
                        discountPrice = Double.parseDouble(orderDiscountModel.getDiscount());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (orderDiscountModel.getDiscountType().equalsIgnoreCase("percentage"))
                        discountAmt = discountPrice * (totalCartPrice - totalDiscount) / 100;
                    else if (orderDiscountModel.getDiscountType().equalsIgnoreCase("value"))
                        discountAmt = discountPrice;
                    discountAmt = ((totalCartPrice - totalDiscount - discountAmt) <= 0) ? 0 : discountAmt;

                    totalDiscount += discountAmt;
                }

                tvDiscountAmount.setText(getString(R.string.discount_value_amount) + " : " + String.format(Locale.CANADA, "%.2f", totalDiscount));
            }*/
        } else {
            // no items available in cart. Show message
            llCart.setVisibility(View.GONE);
            tvNoCart.setVisibility(View.VISIBLE);
        }

        updateCartCount();

    }

    public void calculateAdditionalCashback() {
        double price;
        starLevelCashBackArray.clear();
        if (arrOrderLevelCashBackModel != null && arrOrderLevelCashBackModel.size() > 0) {
            // ll_cashback.removeAllViews();

            for (int i = 0; i < arrOrderLevelCashBackModel.size(); i++) {

                                   /* LayoutInflater inflater = LayoutInflater.from(getContext());
                                    View v = inflater.inflate(R.layout.cashback_dashborad, null);
                                    offerEcashView = v.findViewById(R.id.ll_rel_cashback);
                                    tvEcashDesc = (TextView) v.findViewById(R.id.tv_ecash);

                                    rupee = (Button) v.findViewById(R.id.iv_ecash_desc);*/

                if (arrOrderLevelCashBackModel.get(i).getProduct_star() > 0) {

                    price = Utils.getCartValue(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""), arrOrderLevelCashBackModel.get(i).getProduct_star() + "");
                    if (price > arrOrderLevelCashBackModel.get(i).getQty_from_range() && price <= arrOrderLevelCashBackModel.get(i).getQty_to_range()) {
                        double cbkValue = 0;
                        try {
                            cbkValue = Double.parseDouble(arrOrderLevelCashBackModel.get(i).getCbk_value());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (arrOrderLevelCashBackModel.get(i).getCashback_type() == ConstantValues.CASHBACK_TYPE_PERCENTAGE) {


                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER)
                            {
                                double cashbackPrice = (price / 100) * cbkValue;
                                addStarToStarLevelCashBackArrayWith(arrOrderLevelCashBackModel.get(i).getProduct_star()+"", price,cashbackPrice);
                              //  arrOrderLevelCashBackModel.get(i).getCbk_value();

                                additionalcashBackValue = additionalcashBackValue + cashbackPrice;
                                }
                                        if (isSalesAgent){
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.SALES_AGENT){
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }else if (isFF){
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_ASSOCIATE || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_MANAGER){
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }else{
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER) {
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }
                        } else if (arrOrderLevelCashBackModel.get(i).getCashback_type() == ConstantValues.CASHBACK_TYPE_VALUE) {

                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER){
                                additionalcashBackValue = additionalcashBackValue + cbkValue;
                                addStarToStarLevelCashBackArrayWith(arrOrderLevelCashBackModel.get(i).getProduct_star()+"", price,cbkValue);
                            }

                         //addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));

                                    if (isSalesAgent){
                                        if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.SALES_AGENT){
                                            addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                        }
                                    }else if (isFF){
                                        if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_ASSOCIATE || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_MANAGER){
                                            addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                        }
                                    }else{
                                        if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER) {
                                            addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                        }
                                    }
                        }

                    } else {

//                        additionalcashBackValue = 0;
                    }

                } else {

                    price = Utils.getCartValue(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                    if (price > arrOrderLevelCashBackModel.get(i).getQty_from_range() && price <= arrOrderLevelCashBackModel.get(i).getQty_to_range()) {

                        double cbkValue = 0;
                        try {
                            cbkValue = Double.parseDouble(arrOrderLevelCashBackModel.get(i).getCbk_value());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (arrOrderLevelCashBackModel.get(i).getCashback_type() == ConstantValues.CASHBACK_TYPE_PERCENTAGE) {


                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER){
                                double cashbackValue = (price / 100) * cbkValue;
                                additionalcashBackValue = additionalcashBackValue + cashbackValue;
                                addStarToStarLevelCashBackArrayWith(arrOrderLevelCashBackModel.get(i).getProduct_star()+"",price,cashbackValue);
                            }

                           // addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                        if (isSalesAgent){
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.SALES_AGENT){
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }else if (isFF){
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_ASSOCIATE || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_MANAGER){
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }else{
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER) {
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }

                        } else if (arrOrderLevelCashBackModel.get(i).getCashback_type() == ConstantValues.CASHBACK_TYPE_VALUE) {
                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER){
                                additionalcashBackValue = additionalcashBackValue + cbkValue;
                                addStarToStarLevelCashBackArrayWith(arrOrderLevelCashBackModel.get(i).getProduct_star()+"",price,cbkValue);
                            }

                           /* addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));*/

                                        if (isSalesAgent){
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.SALES_AGENT){
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }else if (isFF){
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_ASSOCIATE || arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.FF_MANAGER){
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }else{
                                            if (arrOrderLevelCashBackModel.get(i).getBenificiary_type() == ConstantValues.CUSTOMER) {
                                                addOrderCashbackIds(String.valueOf(arrOrderLevelCashBackModel.get(i).getCashback_id()));
                                            }
                                        }
                        }

                    } else {
//                                            additionalcashBackValue = 0;
                    }

                }

            }
            tvAdditonalCashBack.setText(getString(R.string.order_cashback) + " : " + String.format(Locale.CANADA, "%.2f", additionalcashBackValue));
            tvAdditonalCashBack.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            tvAdditonalCashBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AdditionalCashBackFragment additionalCashBackFragment=AdditionalCashBackFragment.newInstance(starLevelCashBackArray);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    additionalCashBackFragment.show(ft, "cashDialog");


                }
            });
        } else {
            // additonalCashBack.setText();
        }
    }

    private void addOrderCashbackIds(String cashback_id) {
        if (!cbk_ids_list.contains(cashback_id))
            cbk_ids_list.add(cashback_id);
    }

    private void addStarToStarLevelCashBackArrayWith(String color, double price, double cbkValue)
    {
        CustomerTyepModel customer = DBHelper.getInstance().getStarColorName(color);
        StarLevelCashBackDetailsModel starLevelCashBackDetailsModel = new StarLevelCashBackDetailsModel();
        starLevelCashBackDetailsModel.setStarColor(customer.getCustomerName());
        starLevelCashBackDetailsModel.setStarTotalAppliedAmount(price);
        starLevelCashBackDetailsModel.setStarCashbackAmount(cbkValue);
        starLevelCashBackArray.add(starLevelCashBackDetailsModel);

    }
    @Override
    public void onItemClick(Object object, int position, int type) {
        if (type == 1) {//view details
            if (object instanceof CartModel) {
                CartModel cartModel = (CartModel) object;
                if (cartModel != null) {
                    boolean isChild = cartModel.isChild() == 1 ? true : false;
                    String parentId = cartModel.getParentId();
                    if (!isChild) {
                        parentId = DBHelper.getInstance().getParentId(cartModel.getParentId());
                    }
                    TestProductModel testProductModel = DBHelper.getInstance().getProductById(parentId, isChild);
                    if (testProductModel != null) {
                        ItemCartFragment fragment = ItemCartFragment.newInstance(testProductModel, cartModel.getProductId(), parentId, cartModel.getQuantity(), false);
                        fragment.setClickListener(CartFragment.this);
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        fragment.show(ft, "CartDialog");
                    } else {

                    }
                }
//                getEditCartData(cartModel);
            }
        } else if (type == 0) {//delete cart item
            showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.are_you_sure_delete), object, position);
        }

    }

    private void getEditCartData(CartModel cartModel) {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "ViewCart";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
//                obj.put("cartId", productModel.getCartId());
                obj.put("product_id", cartModel.getProductId());
                obj.put("quantity", cartModel.getQuantity());

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.editCartDataURL, map, CartFragment.this, CartFragment.this, PARSER_TYPE.EDIT_CART_DATA);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.editCartDataURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "ViewCart";
        }
    }

    public void showAlertDialog(Context context, String message, final Object object, final int pos) {

        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(getActivity().getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (object instanceof CartModel) {
                            selectedCartModel = (CartModel) object;
//                            selectedProductObject = productModel;
                            int results = deleteCart(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                            if (results > 0) {//deleted Successfully.. Refresh the list
                                if (cartItemsList != null && cartItemsList.size() > 0) {
                                    try {
//                                        cartItemsList.remove(selectedCartModel);
                                        cartListAdapter.deleteItem(pos);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
//                                        viewCart();
                                        viewCart();
                                    }
                                }
                            } else {// oops...! Something is wrong. Check Why deleting is failed.
                                Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
                            }
                        }
                    }
                })
                .setNegativeButton(getActivity().getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void deleteCart() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "DeleteCart";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("cartId", selectedCartModel.getCartId());
                obj.put("isClearCart", "false");
//                obj.put("product_id", selectedCartModel.getProductId());
//                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
//                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
//                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
//                obj.put("variant_id", productModel.getSkuModelArrayList().get(0).getSkuId());

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.deleteCartURL, map, CartFragment.this, CartFragment.this, PARSER_TYPE.DELETE_CART);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.deleteCartURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "DeleteCart";
        }
    }

    private int deleteCart(String customerId) {
        int result = -1;
        if (TextUtils.isEmpty(customerId)) {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
            return result;
        }

        String cartId = DBHelper.getInstance().getCartId(customerId, selectedCartModel.getProductId());
        if (cartId != null && !TextUtils.isEmpty(cartId) && !cartId.equalsIgnoreCase("null")) {
            deleteCart();
        }

        result = Utils.deleteCartRow(customerId, selectedCartModel.getProductId(), false);

        return result;
    }

    @Override
    public void dialogClose() {
        viewCart();
    }

    @Override
    public void onResume() {
        super.onResume();
        getOrderLevelCashBack();
        viewCart();
        updateCartCount();

        mTracker.setScreenName("Cart Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    private void getOrderLevelCashBack() {

        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "getOrderCashbackData";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                jsonObject.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));

                HashMap<String, String> map1 = new HashMap<>();
                map1.put("data", jsonObject.toString());


                VolleyBackgroundTask cartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getOrderCashbackData, map1, CartFragment.this, CartFragment.this, PARSER_TYPE.ORDER_CASHBACK);
                cartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(cartRequest, AppURL.getOrderCashbackData);

                if (dialog != null)
                    dialog.show();


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {

            requestType = "getOrderCashbackData";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.EDIT_CART_DATA) {
                        if (response instanceof NewProductModel) {
                        }

                    } else if (requestType == PARSER_TYPE.CHECK_CART_INVENTORY) {
                        if (response instanceof CheckCartInventoryModel) {
                            unavailable = false;
                            missMatch = false;
                            valueMissMatch = false;
                            CheckCartInventoryModel checkCartInventoryModel = (CheckCartInventoryModel) response;
                            if (checkCartInventoryModel != null) {
                                totalDiscount = checkCartInventoryModel.getDiscountAmount();
                                ArrayList<CheckInventoryModel> list = checkCartInventoryModel.getArrCartItems();

                                Predicate<CheckInventoryModel> inventoryModelPredicate = new Predicate<CheckInventoryModel>() {
                                    @Override
                                    public boolean apply(CheckInventoryModel brandModel) {
                                        return brandModel.getStatus() == 0;
                                    }
                                };

                                Predicate<CheckInventoryModel> priceMissMatchPredicate = new Predicate<CheckInventoryModel>() {
                                    @Override
                                    public boolean apply(CheckInventoryModel brandModel) {
                                        return brandModel.getStatus() == -1;
                                    }
                                };

                                Predicate<CheckInventoryModel> valuesMissMatchPredicate = new Predicate<CheckInventoryModel>() {
                                    @Override
                                    public boolean apply(CheckInventoryModel brandModel) {
                                        return brandModel.getStatus() == -2;
                                    }
                                };

                                Collection<CheckInventoryModel> unavailableProducts = filter(list, inventoryModelPredicate);
                                Collection<CheckInventoryModel> priceMissMatchProducts = filter(list, priceMissMatchPredicate);
                                Collection<CheckInventoryModel> valuesMissMatchProducts = filter(list, valuesMissMatchPredicate);

                                if (unavailableProducts != null && unavailableProducts.size() > 0) {
                                    unavailable = true;
                                    viewCart();
                                } else if (priceMissMatchProducts != null && priceMissMatchProducts.size() > 0) {
                                    missMatch = true;
                                    viewCart();
                                } else if (valuesMissMatchProducts != null && valuesMissMatchProducts.size() > 0) {
                                    valueMissMatch = true;
                                    viewCart();
                                } else {
                                    unavailable = false;
                                    missMatch = false;
                                    valueMissMatch = false;
                                    //proceed to next screen as there is sufficient inventory for all
                                    String cartIds = DBHelper.getInstance().getCartIds(mSharedPreferences.
                                            getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                                    JSONArray cartArray = new JSONArray();
                                    if (cartIds != null && !TextUtils.isEmpty(cartIds)) {
                                        for (String s : cartIds.split(",")) {
                                            cartArray.put(s);
                                        }
                                    }

                                    /*ArrayList<String> cbk_ids_list = new ArrayList<>();
                                    ArrayList<OrderLevelCashBackModel> orderLevelCashBackModelArrayList = checkCartInventoryModel.getArrOrderLevelCashback();
                                    if (orderLevelCashBackModelArrayList != null && orderLevelCashBackModelArrayList.size() > 0) {
                                        for (int j = 0; j < orderLevelCashBackModelArrayList.size(); j++) {

                                            OrderLevelCashBackModel orderLevelCashBackModel = orderLevelCashBackModelArrayList.get(j);
                                            cbk_ids_list.add(orderLevelCashBackModel.getCbk_id());
                                        }

                                    }*/


                                    String cbk_ids = TextUtils.join(",", cbk_ids_list);

                                    Intent intent = new Intent(getActivity(), PaymentActivity.class);
                                    if (cartArray.length() > 0) {
                                        intent.putExtra("cartId", cartArray.toString());
                                        intent.putExtra("cashbackAmount", additionalcashBackValue + cashbackValue);

                                    }
                                    if (/*isOrderDiscount &&*/ orderDiscountModel != null) {
//                                    intent.putExtra("IsOrderDiscount", isOrderDiscount);
                                        intent.putExtra("OrderDiscountmodel", orderDiscountModel);
                                    }

                                    intent.putExtra("cbkIds", cbk_ids);
                                    intent.putExtra("TotalAmount", totalCartPrice);
                                    intent.putExtra("DiscountAmount", totalDiscount);
                                    intent.putExtra("TotalItems", totalItems);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("StarColorModelArray",starLevelCashBackArray);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                            }

                        }

                    } else if (requestType == PARSER_TYPE.DELETE_CART) {
                        viewCart();
                    } else if (requestType == PARSER_TYPE.UPDATE_BEAT) {
                        if (response instanceof String) {
                            hubId = (String) response;
                        }
                    } else if (requestType == PARSER_TYPE.SEND_MAIL_TO_FF) {
                        if (response instanceof String) {

                        }
                    } else if (requestType == PARSER_TYPE.GET_BEATS) {
                        if (response instanceof ArrayList) {
                            arrBeats = (ArrayList<CustomerTyepModel>) response;
                            if (arrBeats != null) {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                BeatFragment beatFragment = BeatFragment.newInstance(arrBeats);
                                beatFragment.setCancelable(true);
                                beatFragment.setConfirmListener(okListener);
                                beatFragment.show(fm, "beat_fragment");
                            }
                        }
                    } else if (requestType == PARSER_TYPE.ORDER_CASHBACK) {

                        Log.e("order_cashback", response.toString());
                        if (response instanceof ArrayList) {
                            arrOrderLevelCashBackModel = (ArrayList<OrderLevelCashBackModel>) response;
                            additionalcashBackValue = 0;
                            calculateAdditionalCashback();
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void getCartCount() {
        if (mSharedPreferences.getBoolean("IsLoggedIn", false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                requestType = "CartCount";
                rlAlert.setVisibility(View.GONE);
                llMain.setVisibility(View.VISIBLE);

                try {

                    JSONObject obj = new JSONObject();
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    HashMap<String, String> map1 = new HashMap<>();
                    map1.put("data", obj.toString());

                    VolleyBackgroundTask cartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.cartCountURL, map1, CartFragment.this, CartFragment.this, PARSER_TYPE.CART_COUNT);
                    cartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(cartRequest, AppURL.cartCountURL);
                    if (dialog != null)
                        dialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                requestType = "CartCount";
                rlAlert.setVisibility(View.VISIBLE);
                llMain.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}