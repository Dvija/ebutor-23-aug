package com.ebutor.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.FilterFFActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.DashboardRecyclerAdapter;
import com.ebutor.adapters.ExpandListAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.DashboardModelNew;
import com.ebutor.models.TeamDashboard;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class DashBoardFragmentNew extends Fragment implements Response.ErrorListener, VolleyHandler<Object> {


    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private TextView tvFilter, tvRefresh;
    //    private SwipeRefreshLayout mSwipeRefreshLayout;
    //    private TextView tvTBV, tvUOB, tvABV, tvTLC, tvULC, tvALC;
    private boolean hasChilds;
    private RecyclerView mRecyclerView;
    private ExpandableListView expandableListView;
    private String startDate, endDate;

    private ExpandListAdapter expandableAdapter;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private PARSER_TYPE parserType = PARSER_TYPE.RETAILER_DASHBOARD;
    private ArrayList<String> arrFilters = new ArrayList<String>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int type = DashBoardTypes.ORGANIZATION;
    private String userId = "";
    private String pos = "";
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);
    DashboardFilterFragment.OnSelListener okListener = new DashboardFilterFragment.OnSelListener() {

        @Override
        public void onSelected(int pos) {
            switch (pos) {
                case 0:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.ORGANIZATION;
                    break;
                case 1:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.MY_DASHBOARD;
                    break;
                case 2:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.MY_DASHBOARD;
                    showDatePicker();
                    break;
                case 3:
                    parserType = PARSER_TYPE.MY_TEAM_DASHBOARD;
                    type = DashBoardTypes.MY_TEAM_DASHBOARD;
                    break;
                default:
                    parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                    type = DashBoardTypes.ORGANIZATION;
                    break;
            }
            getDashboard(createJSON(null, null, ""), false);
        }
    };

    public static DashBoardFragmentNew newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("type", page);
        DashBoardFragmentNew fragment = new DashBoardFragmentNew();
        fragment.setArguments(args);
        return fragment;
    }

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard_new, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        hasChilds = mSharedPreferences.getBoolean(ConstantValues.KEY_HAS_CHILD, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        expandableListView = (ExpandableListView) view.findViewById(R.id.exp_list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        tvFilter = (TextView) view.findViewById(R.id.tv_filter);
        tvRefresh = (TextView) view.findViewById(R.id.tv_refresh);
//        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        arrFilters = new ArrayList<>();
        arrFilters.add(getString(R.string.organization));
        arrFilters.add(getString(R.string.my_dashboard));
        arrFilters.add(getString(R.string.day));
        if (hasChilds) {
            arrFilters.add(getString(R.string.team_dashboard));
        }
        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.please_wait), true);
        if (getArguments() != null && getArguments().containsKey("type")) {
            type = getArguments().getInt("type");
            parserType = PARSER_TYPE.RETAILER_DASHBOARD;
        }
        /*
            GridLayoutManager
                A RecyclerView.LayoutManager implementations that lays out items in a grid.
                By default, each item occupies 1 span. You can change it by providing a custom
                GridLayoutManager.SpanSizeLookup instance via setSpanSizeLookup(SpanSizeLookup).
        */
        /*
            public GridLayoutManager (Context context, int spanCount)
                Creates a vertical GridLayoutManager

            Parameters
                context : Current context, will be used to access resources.
                spanCount : The number of columns in the grid
        */
        // Define a layout for RecyclerView
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new MarginDecoration(getActivity()));
        mRecyclerView.setHasFixedSize(true);

        getDashboard(createJSON(null, null, ""), false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startDate = null;
                endDate = null;
                mSwipeRefreshLayout.setRefreshing(true);
                getDashboard(createJSON(null, null, ""), true);
            }
        });

        tvRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = null;
                endDate = null;
                getDashboard(createJSON(null, null, ""), false);
            }
        });

        tvFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), FilterFFActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("pos", pos);
                intent.putExtra("is_main", true);
                if (startDate != null)
                    intent.putExtra("start_date", Utils.parseDate(Utils.DATE_STD_PATTERN5, Utils.DATE_STD_PATTERN3, startDate));
                if (endDate != null)
                    intent.putExtra("end_date", Utils.parseDate(Utils.DATE_STD_PATTERN5, Utils.DATE_STD_PATTERN3, endDate));
                startActivityForResult(intent, 1);
//                FragmentManager fm = getActivity().getSupportFragmentManager();
//                DashboardFilterFragment dashboardFilterFragment = DashboardFilterFragment.newInstance(arrFilters);
//                dashboardFilterFragment.setCancelable(true);
//                dashboardFilterFragment.setConfirmListener(okListener);
//                dashboardFilterFragment.show(fm, "filter dashboard");
            }
        });

    }

    private void getDashboard(JSONObject dataObject, boolean isSwipeDown) {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.retailerDashboardURL, map, DashBoardFragmentNew.this, DashBoardFragmentNew.this, parserType);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.retailerDashboardURL);
                if (dialog != null && !isSwipeDown)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject createJSON(String startDate, String endDate, String appliedFilters) {
        JSONObject dataObj = null;
        try {
            if (!TextUtils.isEmpty(appliedFilters)) {
                dataObj = new JSONObject(appliedFilters);
            } else {
                dataObj = new JSONObject();
            }
            if (null == startDate)
                startDate = getDate();
            if (null == endDate)
                endDate = getDate();
            dataObj.put("start_date", startDate);//2016-09-30
            dataObj.put("end_date", endDate);
            dataObj.put("flag", String.valueOf(type));
            if (type == 3) {
                dataObj.put("user_id", userId);
            }

            dataObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataObj;
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (mSwipeRefreshLayout != null)
                mSwipeRefreshLayout.setRefreshing(false);
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.calendar) {
            showDatePicker();
        } else if (item.getItemId() == R.id.refresh) {
            getDashboard(createJSON(null, null, ""));
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDatePicker() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getChildFragmentManager(), "datePicker");
        newFragment.setListener(new DatePickerDialog.OnDateSetListener() {
            /**
             * @param view        The view associated with this listener.
             * @param year        The year that was set.
             * @param monthOfYear The month that was set (0-11) for compatibility
             *                    with {@link Calendar}.
             * @param dayOfMonth  The day of the month that was set.
             */
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String date = format.format(calendar.getTime());
                getDashboard(createJSON(date, date, ""), false);
            }
        });
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.setRefreshing(false);
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
//                    if (mSwipeRefreshLayout != null)
//                        mSwipeRefreshLayout.setRefreshing(false);
                    switch (type) {
                        case DashBoardTypes.ORGANIZATION:
                            getDashboard(response);
                            break;
                        case DashBoardTypes.MY_DASHBOARD:
                            getDashboard(response);
                            break;
                        case DashBoardTypes.MY_TEAM_DASHBOARD:
                            getDashboard(response);
                            break;
                        case 3:
                            getDashboard(response);
                            break;
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void getDashboard(Object response) {

        if (response instanceof ArrayList) {

            if (type != DashBoardTypes.MY_TEAM_DASHBOARD) {
                ArrayList<DashboardModelNew> dashboardModelArrayList = (ArrayList<DashboardModelNew>) response;

                mRecyclerView.setVisibility(View.VISIBLE);
                expandableListView.setVisibility(View.GONE);
                // Initialize a new instance of RecyclerView Adapter instance
                mAdapter = new DashboardRecyclerAdapter(getActivity(), dashboardModelArrayList);

                // Set the adapter for RecyclerView
                mRecyclerView.setAdapter(mAdapter);
            } else {
                ArrayList<TeamDashboard> dashboardModelArrayList = (ArrayList<TeamDashboard>) response;

                mRecyclerView.setVisibility(View.GONE);
                expandableListView.setVisibility(View.VISIBLE);
                // Initialize a new instance of RecyclerView Adapter instance
                expandableAdapter = new ExpandListAdapter(getActivity(), dashboardModelArrayList);
                expandableListView.setAdapter(expandableAdapter);
            }

        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String appliedFilters = "";
                if (data.hasExtra("type")) {
                    type = data.getIntExtra("type", DashBoardTypes.ORGANIZATION);
                }
                if (data.hasExtra("user_id")) {
                    userId = data.getStringExtra("user_id");
                }
                if (data.hasExtra("pos")) {
                    pos = data.getStringExtra("pos");
                }
                if (data.hasExtra("start_date")) {
                    String strStart = data.getStringExtra("start_date");
                    startDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, strStart);
                }
                if (data.hasExtra("end_date")) {
                    String strEnd = data.getStringExtra("end_date");
                    endDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN5, strEnd);
                }

                if (data.hasExtra("applied_filters")) {
                    appliedFilters = data.getStringExtra("applied_filters");
                }

                switch (type) {
                    case DashBoardTypes.ORGANIZATION:
                        parserType = PARSER_TYPE.RETAILER_DASHBOARD;
//                        pos = "";
                        break;
                    case DashBoardTypes.MY_DASHBOARD:
                        parserType = PARSER_TYPE.RETAILER_DASHBOARD;
//                        pos = "";
                        break;
                    case DashBoardTypes.MY_TEAM_DASHBOARD:
                        parserType = PARSER_TYPE.RETAILER_DASHBOARD;
                        type = 3;
                        break;
                }
                getDashboard(createJSON(startDate, endDate, appliedFilters), false);
            }
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private DatePickerDialog.OnDateSetListener listener;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            if (listener != null) {
                listener.onDateSet(view, year, month, day);
            }
        }


        public void setListener(DatePickerDialog.OnDateSetListener listener) {
            this.listener = listener;
        }
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

    public class MarginDecoration extends RecyclerView.ItemDecoration {
        private int margin;

        public MarginDecoration(Context context) {
            margin = context.getResources().getDimensionPixelSize(R.dimen.dp_5);
        }

        @Override
        public void getItemOffsets(
                Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(margin, margin, margin, margin);
        }
    }
}