package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.AddBeatActivity;
import com.ebutor.MapsActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.adapters.ManageBeatAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.BeatModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ManageBeatFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, /*BeatManageAdapter.OnItemClickListener,*/ ManageBeatAdapter.OnItemClickListener {

    private final int[] widths_delivery = {20, 60, 30, 30, 50, 50, 50};
    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    //    private TableFixHeaders tableBeats;
    private ManageBeatAdapter manageBeatAdapter;
    //    private String[][] arrdata = new String[][]{};
    private ArrayList<BeatModel> arrBeats = new ArrayList<>();
    private RecyclerView rvBeats;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_manage_beats, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        tableBeats = (TableFixHeaders) view.findViewById(R.id.table_beats);
        rvBeats = (RecyclerView) view.findViewById(R.id.rv_beats);

        manageBeatAdapter = new ManageBeatAdapter(getActivity(), arrBeats);
        manageBeatAdapter.setClickListener(ManageBeatFragment.this);
        rvBeats.setAdapter(manageBeatAdapter);
        rvBeats.setLayoutManager(new LinearLayoutManager(getActivity()));

//        tableBeats.setAdapter(manageBeatAdapter);

    }

    private void getBeats() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("ff_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                jsonObject.put("flag", "1");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getbeatsURL, map, ManageBeatFragment.this, ManageBeatFragment.this, PARSER_TYPE.GET_BEATS_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getbeatsURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getBeats();
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.GET_BEATS_DATA) {
                        if (response instanceof ArrayList) {

                            arrBeats = (ArrayList<BeatModel>) response;
                            manageBeatAdapter.setData(arrBeats);
//                            String[][] array = (String[][]) response;
//                            manageBeatAdapter.setData(array);
//                            manageBeatAdapter.notifyDataSetChanged();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        } else if (item.getItemId() == R.id.add) {
            Intent intent = new Intent(getActivity(), AddBeatActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onItemClick(BeatModel beatModel) {

        if (beatModel != null) {
            Intent intent = new Intent(getActivity(), AddBeatActivity.class);
            intent.putExtra("beatModel", beatModel);
            startActivity(intent);
        }
    }

    @Override
    public void onMapCLick(BeatModel beatModel) {
        if (beatModel != null) {
            Intent intent = new Intent(getActivity(), MapsActivity.class);
            intent.putExtra("beatModel", beatModel);
            intent.putExtra("isFromManageBeats", true);
            startActivity(intent);
        }
    }
}
