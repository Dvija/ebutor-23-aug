package com.ebutor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.BeatModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AddBeatFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, AddSpokeFragment.OnSpokeClose {

    private SharedPreferences mSharedPreferences;
    private Dialog dialog;
    private Spinner /*spDC,*/ spHub, spSpoke, spRM;
    private RadioButton rbMon, rbTue, rbWed, rbThu, rbFri, rbSat;
    private RadioGroup rg1, rg2;
    private EditText etBeat;
    private ImageView ivSpoke;
    private Button btnAdd;
    private CustomerTyepModel spokeModel;
    private String beatName, /*dcId,*/
            hubId, spokeId, rmId, selectedDays = "", type = ConstantValues.HUB_TYPE, createdSpokeId = "";
    private ArrayList<CustomerTyepModel> /*arrDcs,*/ arrHubs, arrSpokes, array, arrFFs;
    private BeatModel beatModel;
    private boolean isChange = true;
//    private ArrayList<String> arrSelected;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_beats, container, false);
        setHasOptionsMenu(true);
        if (getArguments().containsKey("beatModel")) {
            beatModel = (BeatModel) getArguments().getSerializable("beatModel");
        }
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etBeat = (EditText) view.findViewById(R.id.et_beat);

        ivSpoke = (ImageView) view.findViewById(R.id.iv_spoke);

//        spDC = (Spinner) view.findViewById(R.id.sp_dc);
        spHub = (Spinner) view.findViewById(R.id.sp_hub);
        spSpoke = (Spinner) view.findViewById(R.id.sp_spoke);
        spRM = (Spinner) view.findViewById(R.id.sp_rm);

        rg1 = (RadioGroup) view.findViewById(R.id.rg_1);
        rg2 = (RadioGroup) view.findViewById(R.id.rg_2);
        rbMon = (RadioButton) view.findViewById(R.id.rb_mon);
        rbTue = (RadioButton) view.findViewById(R.id.rb_tue);
        rbWed = (RadioButton) view.findViewById(R.id.rb_wed);
        rbThu = (RadioButton) view.findViewById(R.id.rb_thu);
        rbFri = (RadioButton) view.findViewById(R.id.rb_fri);
        rbSat = (RadioButton) view.findViewById(R.id.rb_sat);

        btnAdd = (Button) view.findViewById(R.id.btn_add);

        spokeModel = new CustomerTyepModel();
//        arrSelected = new ArrayList<>();

        if (beatModel != null) {
            etBeat.setText(beatModel.getBeatName());
            etBeat.setSelection(etBeat.getText().length());
            selectedDays = beatModel.getDay();
        }

//        if (selectedDays != null && selectedDays.length() > 0)
//            arrSelected = new ArrayList<String>(Arrays.asList(selectedDays.split("\\s*,\\s*")));

//        for (int i = 0; i < arrSelected.size(); i++) {
        switch (selectedDays) {
            case "Mon":
                rbMon.setChecked(true);
                break;
            case "Tue":
                rbTue.setChecked(true);
                break;
            case "Wed":
                rbWed.setChecked(true);
                break;
            case "Thu":
                rbThu.setChecked(true);
                break;
            case "Fri":
                rbFri.setChecked(true);
                break;
            case "Sat":
                rbSat.setChecked(true);
                break;
        }
//        }

        getAllData(ConstantValues.HUB_TYPE, "");
        getRM();

        ivSpoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (hubId != null && !TextUtils.isEmpty(hubId)) {

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    AddSpokeFragment addSpokeFragment = AddSpokeFragment.newInstance(spokeModel, hubId);
                    addSpokeFragment.setClickListener(AddBeatFragment.this);
                    addSpokeFragment.setCancelable(true);
                    addSpokeFragment.show(fm, "spoke_fragment");
                }

            }
        });

//        spDC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                dcId = arrDcs.get(i).getCustomerGrpId();
//                if (i != 0) {
//                    type = ConstantValues.HUB_TYPE;
//                    arrHubs = new ArrayList<CustomerTyepModel>();
//                    arrSpokes = new ArrayList<CustomerTyepModel>();
//                    spHub.setAdapter(null);
//                    spSpoke.setAdapter(null);
//                    hubId = "";
//                    spokeId = "";
//                    getAllData(ConstantValues.HUB_TYPE, dcId);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        spHub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                hubId = arrHubs.get(i).getCustomerGrpId();
                if (i != 0) {
                    type = ConstantValues.SPOKE_TYPE;
                    spokeId = "";
                    arrSpokes = new ArrayList<CustomerTyepModel>();
                    spSpoke.setAdapter(null);
                    getAllData(ConstantValues.SPOKE_TYPE, hubId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spSpoke.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spokeId = arrSpokes.get(i).getCustomerGrpId();
                spokeModel = arrSpokes.get(i);
                if (i == 0) {
                    ivSpoke.setImageResource(R.drawable.ic_add_beat);
                } else {
                    ivSpoke.setImageResource(R.drawable.ic_edit_beat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spRM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                rmId = arrFFs.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                if (isChange) {
//                    isChange = false;
//                    rg2.clearCheck();
//                } else {
//                    isChange = true;
//                }
//            }
//        });
//
//        rg2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                if (isChange) {
//                    isChange = false;
//                    rg1.clearCheck();
//                } else {
//                    isChange = true;
//                }
//            }
//        });

        rbMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDays = getResources().getString(R.string.mon);
                rg2.clearCheck();
            }
        });

        rbTue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDays = getResources().getString(R.string.tue);
                rg2.clearCheck();
            }
        });

        rbWed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDays = getResources().getString(R.string.wed);
                rg2.clearCheck();
            }
        });

        rbThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDays = getResources().getString(R.string.thu);
                rg1.clearCheck();
            }
        });

        rbFri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDays = getResources().getString(R.string.fri);
                rg1.clearCheck();
            }
        });

        rbSat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDays = getResources().getString(R.string.sat);
                rg1.clearCheck();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                beatName = etBeat.getText().toString();

//                if (null == dcId || TextUtils.isEmpty(dcId)) {
//                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_dc));
//                    return;
//                }
                if (null == hubId || TextUtils.isEmpty(hubId)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_hub));
                    return;
                }
                if (null == spokeId || TextUtils.isEmpty(spokeId)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_spoke));
                    return;
                }
                if (TextUtils.isEmpty(beatName)) {
                    etBeat.setError(getString(R.string.please_enter_beat_name));
                    return;
                }
                if (null == rmId || TextUtils.isEmpty(rmId)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_rm));
                    return;
                }
                if (null == selectedDays || TextUtils.isEmpty(selectedDays)) {
                    Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_atleast_one_day));
                    return;
                }
                addBeat();
            }
        });

    }

    private void getRM() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                jsonObject.put("flag", "1");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getfieldforcelistURL, map, AddBeatFragment.this, AddBeatFragment.this, PARSER_TYPE.RM_LIST);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getfieldforcelistURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void getAllData(String type, String id) {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("dc_type", type);
                jsonObject.put("request_id", id);
                if (type.equalsIgnoreCase(ConstantValues.SPOKE_TYPE))
                    jsonObject.put("returnBeats", 0);
                if (TextUtils.isEmpty(mSharedPreferences.getString(ConstantValues.KEY_FF_HUB_ID, "")))
                    jsonObject.put("return_all", "1");
                else
                    jsonObject.put("return_all", "0");
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllDataURL, map, AddBeatFragment.this, AddBeatFragment.this, PARSER_TYPE.GET_ALL_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getAllDataURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    private void addBeat() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                jsonObject.put("name", beatName);
                jsonObject.put("days", selectedDays);
                jsonObject.put("rm_id", rmId);
                jsonObject.put("le_wh_id", hubId);
                jsonObject.put("spoke_id", spokeId);
                jsonObject.put("id", (beatModel == null || TextUtils.isEmpty(beatModel.getBeatId())) ? "0" : beatModel.getBeatId());
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addBeatURL, map, AddBeatFragment.this, AddBeatFragment.this, PARSER_TYPE.ADD_BEAT);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.addBeatURL);
                if (dialog != null)
                    dialog.show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.RM_LIST) {
                        if (response instanceof ArrayList) {
                            arrFFs = (ArrayList<CustomerTyepModel>) response;
                            CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                            customerTyepModel.setCustomerGrpId("");
                            customerTyepModel.setCustomerName(getString(R.string.please_select));
                            arrFFs.add(0, customerTyepModel);
                            ArrayAdapter<CustomerTyepModel> ffAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, arrFFs);
                            spRM.setAdapter(ffAdapter);
                            int pos = 0;
                            if (beatModel != null) {
                                for (int i = 0; i < arrFFs.size(); i++) {
                                    CustomerTyepModel customerTyepModel1 = arrFFs.get(i);
                                    if (customerTyepModel1.getCustomerGrpId().equalsIgnoreCase(beatModel.getRmId())) {
                                        pos = i;
                                        break;
                                    }
                                }
                            }
                            spRM.setSelection(pos);

                        }
                    } else if (requestType == PARSER_TYPE.ADD_BEAT) {
                        if (response instanceof String) {
                            showAlertWithMessage(message);

                        }
                    } else if (requestType == PARSER_TYPE.GET_ALL_DATA) {
                        if (response instanceof ArrayList) {
                            switch (type) {
//                                case ConstantValues.DC_TYPE:
//                                    arrDcs = (ArrayList<CustomerTyepModel>) response;
//
//                                    CustomerTyepModel _CustomerTyepModel = new CustomerTyepModel();
//                                    _CustomerTyepModel.setCustomerGrpId("");
//                                    _CustomerTyepModel.setCustomerName("Please select");
//                                    arrDcs.add(0, _CustomerTyepModel);
//
//                                    ArrayAdapter<CustomerTyepModel> dcAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, arrDcs);
//                                    spDC.setAdapter(dcAdapter);
//                                    int pos = 0;
//                                    if (beatModel != null) {
//                                        for (int i = 0; i < arrDcs.size(); i++) {
//                                            CustomerTyepModel customerTyepModel1 = arrDcs.get(i);
//                                            String dcId = beatModel.getDcId();
//                                            if (dcId.contains(",")) {
//                                                dcId = dcId.split(",")[0];
//                                            }
//                                            if (customerTyepModel1.getCustomerGrpId().equalsIgnoreCase(dcId)) {
//                                                pos = i;
//                                                break;
//                                            }
//                                        }
//                                    }
//                                    spDC.setSelection(pos);
//                                    break;
                                case ConstantValues.HUB_TYPE:
                                    arrHubs = (ArrayList<CustomerTyepModel>) response;

                                    CustomerTyepModel _CustomerTyepModel1 = new CustomerTyepModel();
                                    _CustomerTyepModel1.setCustomerGrpId("");
                                    _CustomerTyepModel1.setCustomerName("Please select");
                                    arrHubs.add(0, _CustomerTyepModel1);

                                    ArrayAdapter<CustomerTyepModel> hubAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, arrHubs);
                                    spHub.setAdapter(hubAdapter);
                                    int pos1 = 0;
                                    if (beatModel != null) {
                                        for (int i = 0; i < arrHubs.size(); i++) {
                                            CustomerTyepModel customerTyepModel1 = arrHubs.get(i);
                                            String hubId = beatModel.getHubId();
                                            if (hubId.contains(",")) {
                                                hubId = hubId.split(",")[0];
                                            }
                                            if (customerTyepModel1.getCustomerGrpId().equalsIgnoreCase(hubId)) {
                                                pos1 = i;
                                                break;
                                            }
                                        }
                                    }
                                    spHub.setSelection(pos1);
                                    break;
                                case ConstantValues.SPOKE_TYPE:
                                    arrSpokes = (ArrayList<CustomerTyepModel>) response;

                                    CustomerTyepModel _CustomerTyepModel2 = new CustomerTyepModel();
                                    _CustomerTyepModel2.setCustomerGrpId("");
                                    _CustomerTyepModel2.setCustomerName("Please select");
                                    arrSpokes.add(0, _CustomerTyepModel2);

                                    ArrayAdapter<CustomerTyepModel> beatAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, arrSpokes);
                                    spSpoke.setAdapter(beatAdapter);
                                    int pos2 = 0;
//                                    if (beatModel != null) {
                                    for (int i = 0; i < arrSpokes.size(); i++) {
                                        CustomerTyepModel customerTyepModel1 = arrSpokes.get(i);
                                        if (customerTyepModel1.getCustomerGrpId().equalsIgnoreCase(TextUtils.isEmpty(createdSpokeId) ? beatModel == null ? "" : beatModel.getSpokeId() : createdSpokeId)) {
                                            pos2 = i;
                                            break;
                                        }
                                    }
//                                    }
                                    spSpoke.setSelection(pos2);
                                    break;
                                default:
                                    array = (ArrayList<CustomerTyepModel>) response;

                                    CustomerTyepModel _CustomerTyepModel3 = new CustomerTyepModel();
                                    _CustomerTyepModel3.setCustomerGrpId("");
                                    _CustomerTyepModel3.setCustomerName("Please select");
                                    array.add(0, _CustomerTyepModel3);

                                    ArrayAdapter<CustomerTyepModel> adapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_dropdown_item, array);
                                    spHub.setAdapter(adapter);
                                    break;
                            }
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    public void showAlertWithMessage(String string) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(string);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void spokeClose(Object object) {

        if (object instanceof CustomerTyepModel) {
            CustomerTyepModel spokeModel = (CustomerTyepModel) object;
            getAllData(ConstantValues.SPOKE_TYPE, spokeModel.getHub());
            createdSpokeId = spokeModel.getCustomerGrpId();
        }
    }
}
