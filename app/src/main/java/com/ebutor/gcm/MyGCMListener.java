package com.ebutor.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.ebutor.HomeActivity;
import com.ebutor.R;
import com.google.android.gms.gcm.GcmListenerService;

public class MyGCMListener extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        try {
            String message = data.getString("message");
            Log.d("GCM ", "From: " + from);
            Log.d("GCM ", "Message: " + message);
//            sendNotification(message);
        } catch (Exception e) {

        }
    }

    private void sendNotification(String message) {
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;

        Intent backIntent = new Intent(MyGCMListener.this, HomeActivity.class); //to be changed
        backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent resultIntent = new Intent(MyGCMListener.this, HomeActivity.class); // to be changed
        final PendingIntent pendingIntent = PendingIntent.getActivities(MyGCMListener.this, (int) (System.currentTimeMillis()),
                new Intent[]{backIntent, resultIntent}, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mNotifyBuilder = null;
        NotificationManager mNotificationManager;
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_capture);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.drawable.ic_capture)
                .setDefaults(defaults)
                .setContentText(message);

        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
        bigPicStyle.bigPicture(largeIcon);
        bigPicStyle.setBigContentTitle(message);
        mNotifyBuilder.setDefaults(defaults);
        mNotifyBuilder.setStyle(bigPicStyle);
        mNotifyBuilder.setContentIntent(pendingIntent);
        mNotificationManager.notify((int) (System.currentTimeMillis()), mNotifyBuilder.build());

    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String msgId) {
        super.onMessageSent(msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        super.onSendError(msgId, error);
    }

}
