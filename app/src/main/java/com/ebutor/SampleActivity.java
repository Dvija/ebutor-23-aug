package com.ebutor;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class SampleActivity extends ParentActivity {

    private RecyclerView recyclerView;
    private Sampleadapter sampleadapter;
    private String[] array = new String[]{"M", "M,Tu", "M,W,Th", "Tu,Sa", "M", "M,Tu", "M,Tu,W,Th,F,Sa", "Tu,Sa", "M", "M,Tu", "M,W,Th", "Tu,Sa", "M", "M,Tu", "M,W,Th", "Tu,Sa", "M", "M,Tu", "M,W,Th", "Tu,Sa", "M", "M,Tu", "M,Tu,W,Th,F,Sa", "Tu,Sa", "M", "M,Tu", "M,W,Th", "Tu,Sa", "M", "M,Tu", "M,Tu,W,Th,F,Sa", "Tu,Sa"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sample);

        recyclerView = (RecyclerView) findViewById(R.id.rv);
        sampleadapter = new Sampleadapter();
        recyclerView.setAdapter(sampleadapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(SampleActivity.this));
    }

    private class Sampleadapter extends RecyclerView.Adapter<Sampleadapter.MyViewHolder> {
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final View v = inflater.inflate(R.layout.row_managebeat, parent, false);
            v.setTag(new Sampleadapter.MyViewHolder(v));
            return new Sampleadapter.MyViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            holder.tvBeatName.setText("Beat Name " + position);
            holder.tvRM.setText("Reporting Manager " + position);
            holder.tvDays.setText(array[position]);
            holder.tvNoOfOutlets.setText(position + 1 + "");

        }

        @Override
        public int getItemCount() {
            return 20;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvBeatName, tvRM, tvNoOfOutlets, tvDays;
            private ImageView ivMap;

            public MyViewHolder(View view) {
                super(view);

                tvBeatName = (TextView) view.findViewById(R.id.tv_beat_name);
                tvRM = (TextView) view.findViewById(R.id.tv_rm);
                tvNoOfOutlets = (TextView) view.findViewById(R.id.tv_no_of_outlets);
                tvDays = (TextView) view.findViewById(R.id.tv_days);
                ivMap = (ImageView) view.findViewById(R.id.iv_map);

            }

        }
    }
}
