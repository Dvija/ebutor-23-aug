package com.ebutor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.ebutor.fragments.OtpFragment;

public class ConfirmOtpActivity extends ParentActivity {

    private String mobileNo = "";
    private boolean isFromPayment = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.confirm_otp);

        if (getIntent().hasExtra("MobileNo"))
            mobileNo = getIntent().getStringExtra("MobileNo");

        if (getIntent().hasExtra("isFromPayment"))
            isFromPayment = getIntent().getBooleanExtra("isFromPayment", false);

        if (savedInstanceState == null) {
            Fragment fragment = new OtpFragment();
            Bundle bundle = new Bundle();
            bundle.putString("MobileNo", mobileNo);
            bundle.putBoolean("isFromPayment", isFromPayment);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        //disable device back button
    }
}
