package com.ebutor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.LayerDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SearchEvent;
import com.ebutor.activities.ExpenseModuleMainActivity;
import com.ebutor.adapters.ProfileAdapter;
import com.ebutor.database.DBHelper;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CategoryModel;
import com.ebutor.models.FeatureData;
import com.ebutor.models.SearchModel;
import com.ebutor.searchview.MaterialSearchView;
import com.ebutor.srm.activities.InventoryActivity;
import com.ebutor.srm.activities.SupplierListActivity;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.RoundedImageView;
import com.ebutor.utils.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class BaseActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION = 0;
    public FrameLayout flbody;
    public ViewGroup.LayoutParams baseLayoutParams;
    public DrawerLayout mDrawerLayout;
    public ActionBarDrawerToggle mDrawerToggle;
    public ListView rvProfile;
    public ProfileAdapter profileAdapter;
    public TextView tvLogout;
    public int previousGroup, selectedPos;
    public TextView tvPerson, tvEcash, tvPaymentDue;
    public String personName = "";
    public LinearLayout llProfile, llLogin, llRight, llFF, llSuppliers;
    public RoundedImageView ivperson;
    public DisplayImageOptions options;
    public SharedPreferences mSharedPreferences;
    public ArrayList<SearchModel> suggestionsModelList;
    public MaterialSearchView searchView;
    public int cartCount = 0;
    public LayoutInflater inflater;
    public ExpandableListView mCategoryList;
    public Spinner businessTypeSpinner;
    public DBHelper databaseObj;
    public ArrayList<BusinessTypeModel> businessTypes = new ArrayList<>();
    public LinkedHashMap<String, ArrayList<CategoryModel>> navigationItemsList = new LinkedHashMap<>();
    public LinkedHashMap<String, ArrayList<CategoryModel>> navigationItemsListTemp = new LinkedHashMap<>();
    public SwipeRefreshLayout swipeRefreshLayout;
    public TextView tvRetailerName;
    private Context mContext;
    //    private View ffOptionsView, changeRetailer, addNewRetailer, tvSuppliers, tvInventory, clearData, dashboardView, checkout;
    private String appId = "", selectedSegmentId = "";
    private View childView, vCheckout;
    private CategoryModel selCategoryModel;
    private Dialog dialog;
    private boolean isFF;
    private double currentLat = 0.0, currentLong = 0.0;
    private boolean isCheckIn = false;
    private GoogleApiClient googleApiClient;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base);

        mContext = this;
        dialog = Utils.createLoader(BaseActivity.this, ConstantValues.PROGRESS);

        flbody = (FrameLayout) findViewById(R.id.flBody);
        baseLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        inflater = this.getLayoutInflater();
        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        isCheckIn = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_CHECKIN, false);

        appId = mSharedPreferences.getString(ConstantValues.KEY_APP_ID, null);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        selectedSegmentId = mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, "");
        isFF = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false);

        View toolbarLogo = findViewById(R.id.iv_toolbar_logo);

        if (toolbarLogo != null) {
            toolbarLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivityAfterCleanup(HomeActivity.class);
                    finish();
                }
            });
        }

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        businessTypeSpinner = (Spinner) findViewById(R.id.spinner_business_types);
        mCategoryList = (ExpandableListView) findViewById(R.id.list_navigation_items);
        llProfile = (LinearLayout) findViewById(R.id.llProfile);
        llLogin = (LinearLayout) findViewById(R.id.llLogin);
        llRight = (LinearLayout) findViewById(R.id.drawer);
        llFF = (LinearLayout) findViewById(R.id.ll_ff);
        llSuppliers = (LinearLayout) findViewById(R.id.ll_suppliers);
        tvLogout = (TextView) findViewById(R.id.tvLogout);
        tvPerson = (TextView) findViewById(R.id.tvPerson);
        tvEcash = (TextView) findViewById(R.id.tv_ecash);
        tvPaymentDue = (TextView) findViewById(R.id.tv_payment_due);
        ivperson = (RoundedImageView) findViewById(R.id.ivPerson);

        businessTypeSpinner.setEnabled(false);


//        ffOptionsView = findViewById(R.id.change_retailer);
//        if (ffOptionsView != null) {
//            changeRetailer = ffOptionsView.findViewById(R.id.tv_change_retailer);
//            dashboardView = ffOptionsView.findViewById(R.id.tv_dashboard);
//            addNewRetailer = ffOptionsView.findViewById(R.id.tv_add_retailer);
//            tvSuppliers = ffOptionsView.findViewById(R.id.tv_suppliers);
//            tvInventory = ffOptionsView.findViewById(R.id.tv_inventory);
//            clearData = ffOptionsView.findViewById(R.id.tv_clear_data);
//            checkout = ffOptionsView.findViewById(R.id.tv_check_out);
//            vCheckout = ffOptionsView.findViewById(R.id.v_checkout);
//        }

        tvRetailerName = (TextView) findViewById(R.id.tv_retailer_name);

        if (isFF) {
            boolean isSelectedRetailer = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, false);
            if (isSelectedRetailer) {
                String retailerName = mSharedPreferences.getString(ConstantValues.KEY_SHOP_NAME, "");
                tvRetailerName.setText(getResources().getString(R.string.shop_nmae) + ": " + retailerName);
            } else {
                tvRetailerName.setText("");
            }
        }

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_profile_default)
                .showImageForEmptyUri(R.drawable.ic_profile_default)
                .showImageOnFail(R.drawable.ic_profile_default)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
        invalidateOptionsMenu();

        suggestionsModelList = new ArrayList<>();

        databaseObj = new DBHelper(BaseActivity.this);
        businessTypes = databaseObj.getAllSegments();

        if (businessTypes != null && businessTypes.size() > 0) {
            ArrayAdapter<BusinessTypeModel> adapter = new ArrayAdapter<BusinessTypeModel>(this, android.R.layout.simple_spinner_dropdown_item, businessTypes);
            businessTypeSpinner.setAdapter(adapter);
            int segmentPosition = 0;
            for (int i = 0; i < businessTypes.size(); i++) {
                BusinessTypeModel businessTypeModel = businessTypes.get(i);
                if (businessTypeModel.getSegmentId().equalsIgnoreCase(selectedSegmentId)) {
                    segmentPosition = i;
                    break;
                }
            }
            businessTypeSpinner.setSelection(segmentPosition);
        }

        boolean isToolTip = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_TOOL_TIP, true);

        if (isToolTip) {
            if (mContext instanceof HomeActivity) {
                mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_TOOL_TIP, false).apply();
                startActivity(new Intent(BaseActivity.this, ToolTipActivity.class));
            }
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                selectedPos = businessTypeSpinner.getSelectedItemPosition();
                getCategories();
                getSegments();
            }
        });

        /*checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BaseActivity.this, FeedBackActivity.class);
                intent.putExtra("isCheckOut", true);
                intent.putExtra("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));

                startActivity(intent);

            }
        });
        changeRetailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseActivity.this, ChooseRetailerActivity.class));
            }
        });
        dashboardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(BaseActivity.        this, DashboardActivityNew.class));
                startActivity(new Intent(BaseActivity.this, FFDashboardActivity.class));
            }
        });
        addNewRetailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCheckIn)
                    startActivity(new Intent(BaseActivity.this, FirstSignupActivity.class));
                else
                    Utils.showAlertWithMessage(BaseActivity.this, "Please Check out");
            }
        });
        tvSuppliers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseActivity.this, SupplierListActivity.class));
            }
        });
        tvInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseActivity.this, InventoryActivity.class));
            }
        });
        clearData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
//                startActivity(new Intent(BaseActivity.this, FirstSignupActivity.class));
                new AlertDialog.Builder(BaseActivity.this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.are_you_sure_clear))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                DBHelper dbHelper = DBHelper.getInstance();
                                dbHelper.deleteTable(DBHelper.TABLE_PRODUCTS);
                                dbHelper.deleteTable(DBHelper.TABLE_MANUFACTURERS);
                                dbHelper.deleteTable(DBHelper.TABLE_BRANDS);
                                dbHelper.deleteTable(DBHelper.TABLE_CART);
                                dbHelper.deleteTable(DBHelper.TABLE_HOME);
                                dbHelper.deleteTable(DBHelper.TABLE_HOME_CHILD);
                                dbHelper.deleteCategories();
                                Snackbar.make(v, getResources().getString(R.string.success_clear), Snackbar.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });*/

        tvEcash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BaseActivity.this, CashBackHistoryActivity.class);
                startActivity(intent);
            }
        });

        mCategoryList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View view,
                                        int groupPosition, long id) {


                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                } else {
                    if (groupPosition != previousGroup) {
                        parent.collapseGroup(previousGroup);
                    }
                    previousGroup = groupPosition;
                    parent.expandGroup(groupPosition);
                }

                parent.smoothScrollToPosition(groupPosition);
                return true;
            }

        });

        businessTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences.Editor editor = mSharedPreferences.edit();

                BusinessTypeModel businessTypeModel = businessTypes.get(i);
                String businessId = businessTypeModel.getSegmentId();
                if (!businessId.equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""))) {
                    navigationItemsList.clear();
                    editor.putString(ConstantValues.KEY_SEGMENT_ID, businessId);
                    editor.apply();

                    getCategories();

                } else {
                    ArrayList<CategoryModel> categoriesArray = databaseObj.getNavigationCategories();
                    for (int j = 0; j < categoriesArray.size(); j++) {
                        String categoryName = databaseObj.getCategoryName(categoriesArray.get(j).getCategoryId());
                        String categoryId = categoriesArray.get(j).getCategoryId();
                        if (categoryName != null && !TextUtils.isEmpty(categoryName)) {
                            ArrayList<CategoryModel> subcategoriesList = databaseObj.getNavigationSubcategories(categoriesArray.get(j).getCategoryId());
                            if (subcategoriesList != null) {
                                navigationItemsList.put(categoryId, subcategoriesList);
                            }
                        }
                    }

                    mCategoryList.setAdapter(new NavigationExpandableAdapter(BaseActivity.this, navigationItemsList));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
        searchView.setSuggestions(suggestionsModelList);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(SearchModel model) {
                searchView.closeSearch();
                if (model != null) {
                    callCrashlyticsSearchEvent(model.getName());
                    String productId = model.getProductId();
                    if (!TextUtils.isEmpty(productId)) {
                        Intent intent = new Intent(BaseActivity.this, ProductDetailsActivity.class);
                        intent.putExtra("ProductId", productId);
                        intent.putExtra("ParentId", model.getParentId());
                        if (mContext instanceof ProductDetailsActivity)
                            ((Activity) mContext).finish();
                        startActivity(intent);
                    } else {

                        Intent intent = new Intent(BaseActivity.this, MainActivityNew.class);
                        String categoryId = model.getCategoryId();
                        String brandId = model.getBrandId();
                        String manfId = model.getManufacturerId();
                        String key = "category_id", id = "";

                        if (!TextUtils.isEmpty(categoryId)) {
                            key = "category_id";
                            id = categoryId;
                        } else if (!TextUtils.isEmpty(brandId)) {
                            key = "brand_id";
                            id = brandId;
                        } else if (!TextUtils.isEmpty(manfId)) {
                            key = "manufacturer_id";
                            id = manfId;
                        }

                        Utils.callCrashlyticsViewEvent(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "0"), key, id);
                        intent.putExtra("Id", id);
                        intent.putExtra("key_id", key);
                        intent.putExtra("Type", "Products");

                        if (mContext instanceof MainActivityNew)
                            ((Activity) mContext).finish();
                        startActivity(intent);
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic

                if (newText.length() > 0) {
                    myAsyncTask m = (myAsyncTask) new myAsyncTask().execute(newText);
                }
                return false;
            }
        });

        searchView.mSearchSrcTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    SearchModel model = null;
                    String text = searchView.mSearchSrcTextView.getText().toString();
                    for (int i = 0; i < suggestionsModelList.size(); i++) {
                        if (text.length() > 0) {
                            if (suggestionsModelList.get(i).getName().equalsIgnoreCase(text)) {
                                searchView.closeSearch();
                                model = suggestionsModelList.get(i);
                            }
                        }

                    }
                    if (model != null) {
                        searchView.mSuggestionsListView.setVisibility(View.VISIBLE);
                        searchView.mNoSuggestionsTextView.setVisibility(View.GONE);
                        String productId = model.getProductId();
                        if (!TextUtils.isEmpty(productId)) {
                            Intent intent = new Intent(BaseActivity.this, ProductDetailsActivity.class);
                            intent.putExtra("ProductId", productId);
                            intent.putExtra("ParentId", model.getParentId());
                            if (mContext instanceof ProductDetailsActivity)
                                ((Activity) mContext).finish();
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(BaseActivity.this, MainActivityNew.class);
                            intent.putExtra("Type", "Search");
                            intent.putExtra("Key", model.getName());
                            if (mContext instanceof MainActivityNew)
                                ((Activity) mContext).finish();
                            startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(BaseActivity.this, MainActivityNew.class);
                        intent.putExtra("Type", "Search");
                        intent.putExtra("Key", text);
                        if (mContext instanceof MainActivityNew)
                            ((Activity) mContext).finish();
                        startActivity(intent);
                    }
                    return true; // Focus will do whatever you put in the logic.
                }
                return false;  // Focus will change according to the actionId
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle = new ActionBarDrawerToggle(
                BaseActivity.this,                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                BaseActivity.this.supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                BaseActivity.this.supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()

            }
        };

        findViewById(R.id.iv_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {

                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                }
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {

                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {

                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        personName = mSharedPreferences.getString(ConstantValues.KEY_FIRST_NAME, "");
        if (personName != null && !TextUtils.isEmpty(personName))
            tvPerson.setText(personName);

        if (isFF && !isCheckIn) {
            tvEcash.setText(getString(R.string.ffEcash) + " : " + String.format(Locale.CANADA, "%.2f", mSharedPreferences.getFloat(ConstantValues.KEY_FF_ECASH, 0)));
            tvPaymentDue.setText(getString(R.string.due) + " : " + String.format(Locale.CANADA, "%.2f", mSharedPreferences.getFloat(ConstantValues.KEY_FF_PAYMENT_DUE, 0)));
        } else {
            tvEcash.setText(getString(R.string.ecash) + " : " + String.format(Locale.CANADA, "%.2f", mSharedPreferences.getFloat(ConstantValues.KEY_ECASH, 0)));
            tvPaymentDue.setText(getString(R.string.due) + " : " + String.format(Locale.CANADA, "%.2f", mSharedPreferences.getFloat(ConstantValues.KEY_PAYMENT_DUE, 0)));
        }


        llRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        boolean isLoggedIn = mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false);
        if (isLoggedIn) {
            llProfile.setVisibility(View.VISIBLE);
            llLogin.setVisibility(View.GONE);
            tvLogout.setVisibility(View.VISIBLE);
        } else {
            llProfile.setVisibility(View.GONE);
            llLogin.setVisibility(View.VISIBLE);
            tvLogout.setVisibility(View.GONE);
        }

        findViewById(R.id.tvLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseActivity.this, FirstSignupActivity.class));
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });

        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                }


                if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EDIT_PROFILE_FEATURE_CODE) > 0) {
                    if (mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, "").equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""))) {
                        return;
                    }
                    if (!(mContext instanceof ProfileActivity))
                        startActivity(new Intent(BaseActivity.this, ProfileActivity.class));
                } else {
                    return;// no permission do nothingggg
                }

            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(BaseActivity.this)) {

                    if (isCheckIn) {
                        Utils.showAlertWithMessage(BaseActivity.this, getString(R.string.please_checkout));
                    } else {
                        showAlertDialog(getResources().getString(R.string.are_you_sure_logout));
                    }
                } else {
                    Utils.showAlertWithMessage(BaseActivity.this, getResources().getString(R.string.no_network_connection));
                }
            }
        });

        ArrayList<FeatureData> featureDataArrayList = Utils.getRightMenuOptions();
        if (featureDataArrayList == null)
            featureDataArrayList = new ArrayList<>();
        if (isCheckIn) {
            String retailerName = mSharedPreferences.getString(ConstantValues.KEY_SHOP_NAME, "No Retailer Selected");
            tvRetailerName.setText(retailerName);

            FeatureData data = new FeatureData(FeatureCodes.CHECKOUT_FEATURE_CODE, "Checkout", "000", "1", "1");
            featureDataArrayList.add(0, data);

        }
        FeatureData termsData = new FeatureData(FeatureCodes.PRIVACY_POLICY_FEATURE_CODE, getString(R.string.terms_conditions), "000", "1", "1");
        featureDataArrayList.add(termsData);

        FeatureData rateAppData = new FeatureData(FeatureCodes.RATE_US_FEATURE_CODE, getString(R.string.rate_our_app), "000", "1", "1");
        featureDataArrayList.add(rateAppData);

        /*FeatureData paymentDueData = new FeatureData(FeatureCodes.PAYMENT_DUE_FEATURE_CODE, getString(R.string.payment_due), "000", "1", "1");
        featureDataArrayList.add(0,paymentDueData);

        FeatureData creditLimitData = new FeatureData(FeatureCodes.CREDIT_LIMIT_FEATURE_CODE, getString(R.string.credit_limit), "000", "1", "1");
        featureDataArrayList.add(0,creditLimitData);

        FeatureData eCashData = new FeatureData(FeatureCodes.ECASH_FEATURE_CODE, getString(R.string.ecash), "000", "1", "1");
        featureDataArrayList.add(0,eCashData);*/

        rvProfile = (ListView) findViewById(R.id.rvProfile);

        String[] arrOptions = new String[]{getResources().getString(R.string.orders), /*"Lists", "Shipment Tracking",*/ getResources().getString(R.string.terms_conditions), getResources().getString(R.string.rate_our_app)/*, "Change Password"*/};

        profileAdapter = new ProfileAdapter(BaseActivity.this, featureDataArrayList);
        rvProfile.setAdapter(profileAdapter);
        Utils.setListViewHeightBasedOnItems(rvProfile);

        rvProfile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                }

                Object obj = view.getTag();
                if (obj != null && obj instanceof FeatureData) {
                    FeatureData data = (FeatureData) obj;

                    String featureCode = data.getFeatureCode();
                    switch (featureCode) {
                        case FeatureCodes.DASHBOARD_FEATURE_CODE:
                            if (!(mContext != null && mContext instanceof FFDashboardActivity))
                                startActivityAfterCleanup(FFDashboardActivity.class);
                            break;
                        case FeatureCodes.ADD_RETAILER_FEATURE_CODE:
                            if (!isCheckIn)
                                startActivity(new Intent(BaseActivity.this, FirstSignupActivity.class));
                            else
                                Utils.showAlertWithMessage(BaseActivity.this, getString(R.string.please_checkout));
                            break;
                        case FeatureCodes.SUPPLIERS_FEATURE_CODE:
                            startActivity(new Intent(BaseActivity.this, SupplierListActivity.class));
                            break;
                        case FeatureCodes.INVENTORY_FEATURE_CODE:
                            startActivity(new Intent(BaseActivity.this, InventoryActivity.class));
                            break;
                        case FeatureCodes.EXPENSES_FEATURE_CODE:
                            startActivity(new Intent(BaseActivity.this, ExpenseModuleMainActivity.class));
                            break;
                        case FeatureCodes.ORDERS_FEATURE_CODE:
                            if (!(mContext != null && mContext instanceof OrdersListActivity))
                                startActivity(new Intent(BaseActivity.this, OrdersListActivity.class));
                            break;
                        case FeatureCodes.PRIVACY_POLICY_FEATURE_CODE:
                            break;
                        case FeatureCodes.RATE_US_FEATURE_CODE:
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                            startActivity(i);
                            break;
                        case FeatureCodes.CHECKOUT_FEATURE_CODE:
                            checkout();
                            break;
                        case FeatureCodes.CLEAR_DATA_FEATURE_CODE:
                            clearData();
                            break;
                        case FeatureCodes.UPDATE_BEATS_FEATURE_CODE:
                            /*if (!hasGPSDevice(BaseActivity.this))
                                Toast.makeText(BaseActivity.this, "Gps not Supported", Toast.LENGTH_SHORT).show();
                            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(BaseActivity.this)) {
                                enableLoc();
                            } else {*/
                            Intent intent1 = new Intent(BaseActivity.this, MapsActivity.class);
                            startActivity(intent1);
//                            }

                            break;
                        /*case FeatureCodes.MANAGE_BEATS_FEATURE_CODE:

                            Intent _intent = new Intent(BaseActivity.this, ManageBeatActivity.class);
                            startActivity(_intent);
                            break;*/
                        case FeatureCodes.APPROVE_PO_FEATURE_CODE:
                            /*Intent poIntent = new Intent(BaseActivity.this, POApprovalsActivity.class);
                            startActivity(poIntent);*/
                            break;
                        case FeatureCodes.SCAN_FEATURE_CODE:
                            Intent poIntent = new Intent(BaseActivity.this, ScanActivity.class);
                            startActivity(poIntent);
                            break;
                        default:
                            break;
                    }

                } else {
                    Snackbar.make(rvProfile, getString(R.string.oops), Snackbar.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void clearData() {
        new AlertDialog.Builder(BaseActivity.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(getResources().getString(R.string.are_you_sure_clear))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DBHelper dbHelper = DBHelper.getInstance();
                        dbHelper.deleteTable(DBHelper.TABLE_PRODUCTS);
                        dbHelper.deleteTable(DBHelper.TABLE_MANUFACTURERS);
                        dbHelper.deleteTable(DBHelper.TABLE_BRANDS);
                        dbHelper.deleteTable(DBHelper.TABLE_CART);
                        dbHelper.deleteTable(DBHelper.TABLE_HOME);
                        dbHelper.deleteTable(DBHelper.TABLE_HOME_CHILD);
                        dbHelper.deleteCategories();

                        Snackbar.make(rvProfile, getResources().getString(R.string.success_clear), Snackbar.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void callCrashlyticsSearchEvent(String name) {
        Answers.getInstance().logSearch(new SearchEvent()
                .putQuery(name)
                .putCustomAttribute("Customer Id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, "0")));
    }

    private void checkout() {

        Intent intent = new Intent(BaseActivity.this, FeedBackActivity.class);
        intent.putExtra("isCheckOut", true);
        intent.putExtra("legal_entity_id", mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, ""));

        startActivity(intent);
    }

    private void getCategories() {

        myNewAsyncTask m = (myNewAsyncTask) new myNewAsyncTask().execute();

    }

    private void getSegments() {
        mySegmentsAsyncTask m = (mySegmentsAsyncTask) new mySegmentsAsyncTask().execute();

    }

    private void startActivityAfterCleanup(Class<?> cls) {
//        if (projectsDao != null) projectsDao.close();
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void showAlertDialog(String message) {

        new AlertDialog.Builder(BaseActivity.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {

                            mDrawerLayout.closeDrawer(Gravity.RIGHT);
                        }

                        Utils.deleteLocalData(BaseActivity.this);

                        Intent intent = new Intent(BaseActivity.this, PincodeAvailabiltyActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    public void setContext(Context context) {
        mContext = context;
    }

    public void invalidate(int cartCount) {
        this.cartCount = cartCount;
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        MenuItem searchItem = menu.findItem(R.id.menu_search);
        searchView.setMenuItem(searchItem);

        Utils.setBadgeCount(BaseActivity.this, icon, cartCount);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cart) {

            if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                startActivity(new Intent(BaseActivity.this, CartActivity.class));
            } else {
                Toast.makeText(BaseActivity.this, getResources().getString(R.string.please_login_view_cart), Toast.LENGTH_LONG).show();
            }

        } else if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_settings) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {

                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
            if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {

                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            } else {

                mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
        } else if (item.getItemId() == R.id.notification) {
            /*if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                startActivity(new Intent(BaseActivity.this, NotificationsActivity.class));
            } else {
                Toast.makeText(BaseActivity.this, getResources().getString(R.string.please_login_view_notifications), Toast.LENGTH_LONG).show();
            }*/
        }
        return super.onOptionsItemSelected(item);
    }

    public void exportDatabase(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "Factail.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }


    public String performPostCall(String requestURL,
                                  String postDataParams) throws IOException {

        URL url;
        String response = "";

        url = new URL(requestURL);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(9000000);
        conn.setConnectTimeout(9000000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);


        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(postDataParams);

        writer.flush();
        writer.close();
        os.close();
        int responseCode = conn.getResponseCode();

        if (responseCode == HttpsURLConnection.HTTP_OK) {
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }
        } else {
            response = "";

        }
        return response;
    }

    @Override
    protected void onResume() {
        super.onResume();

        int syncTimeInterval = ConstantValues.KEY_SYNC_CART_INTERVAL;
        String past = Utils.getPastDateTime(syncTimeInterval);

        DBHelper.getInstance().deleteOldCart(past);

        personName = mSharedPreferences.getString(ConstantValues.KEY_FIRST_NAME, "");
        if (personName != null && !TextUtils.isEmpty(personName))
            tvPerson.setText(personName);

        String profileImage = mSharedPreferences.getString(ConstantValues.KEY_PROFILE_IMAGE, null);
        if (profileImage != null && !TextUtils.isEmpty(profileImage)) {
            ImageLoader.getInstance().displayImage(profileImage, ivperson, options);
        }

        // refresh products data for first time in a day
        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String currentDate = df.format(c.getTime());
        Date pastDate = null, currDate = null;
        try {
            currDate = df.parse(df.format(c.getTime()));
            pastDate = df.parse(mSharedPreferences.getString(ConstantValues.KEY_LAST_UPDATED_DATE, currentDate));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((int) ((currDate.getTime() - pastDate.getTime()) / (1000 * 60 * 60 * 24)) > 0) {
            DBHelper dbHelper = DBHelper.getInstance();
            dbHelper.deleteTable(DBHelper.TABLE_PRODUCTS);
            dbHelper.deleteTable(DBHelper.TABLE_CART);
            MyApplication.getInstance().setDayChanged(true);

        }
        mSharedPreferences.edit().putString(ConstantValues.KEY_LAST_UPDATED_DATE, currentDate).apply();

    }

    public boolean equalLists(ArrayList<CategoryModel> first, ArrayList<CategoryModel> second) {
        ArrayList<String> one = new ArrayList<>();
        ArrayList<String> two = new ArrayList<>();
        if ((first == null && second != null)
                || first != null && second == null
                || first.size() != second.size()) {
            return false;
        }
        for (int i = 0; i < first.size(); i++) {
            one.add(first.get(i).getCategoryId());
        }
        for (int i = 0; i < second.size(); i++) {
            two.add(second.get(i).getCategoryId());
        }
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        one = new ArrayList<String>(one);
        two = new ArrayList<String>(two);

        Collections.sort(one);
        Collections.sort(two);
        return one.equals(two);
    }

    /*private void enableLoc() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.e("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(BaseActivity.this, REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }

    }*/

    class myNewAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialog != null)
                dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            String response = "";
            try {
                url = new URL(AppURL.categoriesURL);
                HashMap<String, String> map = new HashMap<>();
                JSONObject object = new JSONObject();
                object.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                object.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                object.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                object.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                map.put("data", object.toString());

                StringBuilder result = new StringBuilder();
                boolean first = true;
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (first)
                        first = false;
                    else
                        result.append("&");

                    result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    result.append("=");
                    result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                }


                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(9000000);
                conn.setConnectTimeout(9000000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);


                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(result.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                } else {
                    response = "";

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            databaseObj.deleteTable(DBHelper.TABLE_CATEGORY);
            swipeRefreshLayout.setRefreshing(false);
            if (response != null && !TextUtils.isEmpty(response)) {
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.optString("status");
                    String message = jsonObject.optString("message");
                    if (status.equalsIgnoreCase("success")) {

                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        if (dataArray != null && dataArray.length() > 0) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject categoryObj = dataArray.getJSONObject(i);
                                String categoryId = categoryObj.optString("id");
                                String segmentId = categoryObj.optString("segment_id");
                                String parentId = categoryObj.optString("parent_cat_id");
                                String categoryImage = categoryObj.optString("image");
                                String categoryName = categoryObj.optString("name");
                                String productNo = categoryObj.optString("productNo");

                                CategoryModel model = new CategoryModel();
                                model.setCategoryId(categoryId);
                                model.setCategoryImage(categoryImage);
                                model.setSegmentId(segmentId);
                                model.setParentId(parentId);
                                model.setCategoryName(categoryName);
                                model.setProductNo(productNo);

                                databaseObj.insertCategory(model);
                            }
                        }
                        Log.e("Count 4 :", databaseObj.getCount() + "");
                        navigationItemsList.clear();

                        ArrayList<CategoryModel> categoriesArray = databaseObj.getNavigationCategories();
                        for (int j = 0; j < categoriesArray.size(); j++) {
                            String categoryName = databaseObj.getCategoryName(categoriesArray.get(j).getCategoryId());
                            String categoryId = categoriesArray.get(j).getCategoryId();
                            if (categoryName != null && !TextUtils.isEmpty(categoryName)) {
                                ArrayList<CategoryModel> subcategoriesList = databaseObj.getNavigationSubcategories(categoriesArray.get(j).getCategoryId());
                                if (subcategoriesList != null) {
                                    navigationItemsList.put(categoryId, subcategoriesList);
                                }
                            }
                        }

                /*Collections.sort(mainCategories, new Comparator<String>() {
                    @Override
                    public int compare(String s1, String s2) {
                        return s1.compareToIgnoreCase(s2);
                    }
                });*/
                        mCategoryList.setAdapter(new NavigationExpandableAdapter(BaseActivity.this, navigationItemsList));

                    } else {
                        Utils.showAlertWithMessage(BaseActivity.this, message);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class mySegmentsAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialog != null)
                dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            String response = "";
            try {
                url = new URL(AppURL.masterLookUpURL);
                HashMap<String, String> map = new HashMap<>();
                JSONObject object = new JSONObject();
                object.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                object.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                object.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                map.put("data", object.toString());

                StringBuilder result = new StringBuilder();
                boolean first = true;
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (first)
                        first = false;
                    else
                        result.append("&");

                    result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    result.append("=");
                    result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                }


                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(9000000);
                conn.setConnectTimeout(9000000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);


                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(result.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                } else {
                    response = "";

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            databaseObj.deleteTable(DBHelper.TABLE_BUSINESS_TYPE);
            swipeRefreshLayout.setRefreshing(false);
            if (response != null && !TextUtils.isEmpty(response)) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject dataObject = jsonObject.optJSONObject("data");
                        if (dataObject != null && dataObject.length() > 0) {
                            JSONArray segmentsArray = dataObject.optJSONArray("segments");

                            if (segmentsArray != null && segmentsArray.length() > 0) {
                                for (int i = 0; i < segmentsArray.length(); i++) {

                                    JSONObject segmentTypeObj = segmentsArray.optJSONObject(i);
                                    if (segmentTypeObj != null) {
                                        String segmentName = segmentTypeObj.optString("segment_name");
                                        String segmentId = segmentTypeObj.optString("segment_id");
                                        JSONArray categoryArray = segmentTypeObj.optJSONArray("category");
                                        String commaSeperatedCategories = "";
                                        if (categoryArray != null && categoryArray.length() > 0) {
                                            for (int j = 0; j < categoryArray.length(); j++) {
                                                JSONObject categoryObject = categoryArray.optJSONObject(j);
                                                String categoryId = categoryObject.optString("category_id");
                                                commaSeperatedCategories += categoryId + ",";
                                            }
                                        }
                                        if (commaSeperatedCategories != null && commaSeperatedCategories.length() > 0) {
                                            commaSeperatedCategories = commaSeperatedCategories.substring(0, commaSeperatedCategories.length() - 1);
                                        }
                                        Log.e("comm seperated", "" + commaSeperatedCategories);
                                        BusinessTypeModel model = new BusinessTypeModel();
                                        model.setSegmentId(segmentId);
//                                    model.setCommaSeperatedCategories(commaSeperatedCategories);
                                        model.setSegmentName(segmentName);

                                        databaseObj.insertSegment(model);
                                    }
                                }
                            }
                        }
                        getCategories();
                    } else {
                        Utils.showAlertWithMessage(BaseActivity.this, message);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class myAsyncTask extends AsyncTask<String, Void, String> {
        JSONArray suggestionsList;
        String url = new String();
        String textSearch;
//        ProgressDialog pd;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            suggestionsList = new JSONArray();
            suggestionsModelList = new ArrayList<>();
//            pd = new ProgressDialog(HomeActivity.this);
//            pd.setCancelable(false);
//            pd.setMessage("Searching...");
//            pd.getWindow().setGravity(Gravity.CENTER);
//            pd.show();
        }

        @Override
        protected String doInBackground(String... sText) {

            url = AppURL.searchURL;
            JSONObject object = new JSONObject();
            try {
                object.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                object.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                object.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                object.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                object.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                object.put("keyword", sText[0]);

                Log.e("Search Ajax", object.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            String returnResult = getProductList(url, "data=" + object.toString());
            this.textSearch = sText[0];
            return returnResult;

        }

        public String getProductList(String url, String jsonData) {

            SearchModel tempProduct;
            String matchFound = "N";
            //productResults is an arraylist with all product details for the search criteria
            //productResults.clear();

            try {


                JSONObject json = new JSONObject(performPostCall(url, jsonData));

                JSONObject dataObj = json.optJSONObject("data");
                if (dataObj != null) {
                    suggestionsList = dataObj.getJSONArray("data");
                    suggestionsModelList.clear();

                    //parse date for dateList
                    for (int i = 0; i < suggestionsList.length(); i++) {
                        tempProduct = new SearchModel();

                        JSONObject obj = suggestionsList.getJSONObject(i);

                        tempProduct.setCategoryId(obj.optString("category_id"));
                        tempProduct.setManufacturerId(obj.optString("manufacturer_id"));
                        tempProduct.setBrandId(obj.optString("brand_id"));
                        tempProduct.setProductId(obj.optString("product_id"));
                        tempProduct.setParentId(obj.optString("parent_id"));
                        tempProduct.setName(obj.optString("name"));

                        suggestionsModelList.add(tempProduct);
                    }

                    return ("OK");
                } else {
                    return ("No Products Found");
                }

            } catch (Exception e) {
                e.printStackTrace();
                return ("Exception Caught");
            }
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
//            pd.dismiss();

            if (result.equalsIgnoreCase("Exception Caught")) {
                Toast.makeText(BaseActivity.this, getResources().getString(R.string.unable_connect_server), Toast.LENGTH_LONG).show();
            } else if (result.equalsIgnoreCase("No Products Found")) {
                searchView.setSuggestions(null);
//                Toast.makeText(BaseActivity.this, "No Products Found", Toast.LENGTH_SHORT).show();
            } else {
                searchView.setSuggestions(suggestionsModelList);
            }
        }
    }

    public class NavigationAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;
        private ArrayList<CategoryModel> arrSubCategories;

        public NavigationAdapter(Context context, ArrayList<CategoryModel> subcategoriesList) {
            layoutInflater = LayoutInflater.from(context);
            arrSubCategories = subcategoriesList;
        }

        @Override
        public int getCount() {
            return arrSubCategories.size();
        }

        @Override
        public Object getItem(int position) {
            return arrSubCategories.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.expandablelistviewsubcat, null);
                holder = new ViewHolder();
                holder.tvname = (TextView) convertView.findViewById(R.id.subcat_name);
                holder.listView = (ListView) convertView.findViewById(R.id.list_items);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvname.setText(arrSubCategories.get(position).getCategoryName());
            holder.tvname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String categoryId = arrSubCategories.get(position).getCategoryId();
                    String categoryName = arrSubCategories.get(position).getCategoryName();
                    if (categoryName != null && !TextUtils.isEmpty(categoryName)) {
                        ArrayList<CategoryModel> subcategoriesList = databaseObj.getNavigationSubcategories(categoryId);
                        if (subcategoriesList != null && subcategoriesList.size() > 0) {

//                            navigationItemsList.put(categoryId, subcategoriesList);
                            holder.listView.setVisibility(View.VISIBLE);
                            holder.listView.setAdapter(new NavigationAdapter(BaseActivity.this, subcategoriesList));
                            Utils.setListViewHeightBasedOnItems(holder.listView);

                        } else {
                            holder.listView.setVisibility(View.GONE);
                            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {

                                mDrawerLayout.closeDrawer(Gravity.LEFT);
                            } else {

                                mDrawerLayout.openDrawer(Gravity.LEFT);
                            }

                            Utils.callCrashlyticsViewEvent(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "0"), "category_id", categoryId);

                            Intent intent = new Intent(BaseActivity.this, MainActivityNew.class);
                            intent.putExtra("Type", "SubCategories");
                            intent.putExtra("Id", categoryId);
                            intent.putExtra("Name", categoryName);
                            if (mContext instanceof MainActivityNew)
                                ((Activity) mContext).finish();
                            startActivity(intent);

                        }
                    }

                }
            });

            return convertView;
        }

        class ViewHolder {
            TextView tvname;
            ListView listView;
        }
    }

    public class NavigationExpandableAdapter extends BaseExpandableListAdapter {

        LinkedHashMap<String, ArrayList<CategoryModel>> totalNavItems;
        private LayoutInflater layoutInflater;
        private List<String> categories;

        public NavigationExpandableAdapter(Context context, LinkedHashMap<String, ArrayList<CategoryModel>> totalItems) {

            layoutInflater = LayoutInflater.from(context);
            this.totalNavItems = totalItems;
            categories = new ArrayList<String>(totalItems.keySet());
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
        }

        @Override
        public int getGroupCount() {
            if (categories != null)
                return categories.size();
            else
                return 0;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            // if total items are null, returning ZERO
            if (totalNavItems == null)
                return 0;

            // get Sub categories based on group position
            ArrayList<CategoryModel> subCategories = totalNavItems.get(categories.get(groupPosition));
            if (subCategories == null)
                return 0;

            // returning actual value
            return subCategories.size();

        }

        @Override
        public Object getGroup(int groupPosition) {
            if (totalNavItems != null) {
                return categories.get(groupPosition);
            } else {
                return null;
            }
        }

        @Override
        public CategoryModel getChild(int groupPosition, int childPosition) {

            ArrayList<CategoryModel> subCategories = totalNavItems.get(categories.get(groupPosition));
            if (subCategories != null) {
                return subCategories.get(childPosition);
            } else {
                return null;
            }
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded,
                                 View view, final ViewGroup parent) {

            if (view == null) {
                view = layoutInflater.inflate(R.layout.expandablelistcategory, parent, false);
            }

            TextView textView = (TextView) view.findViewById(R.id.cat_desc_1);
            Object obj = getGroup(groupPosition);
            if (obj != null) {
                final String categoryId = (String) obj;
                final String categoryName = databaseObj.getCategoryName(categoryId);
                Spanned string = Html.fromHtml(categoryName);

                textView.setText(string);

                view.setTag(categoryId);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (((ExpandableListView) parent).isGroupExpanded(groupPosition)) {
                            ((ExpandableListView) parent).collapseGroup(groupPosition);
                        } else {
                            if (groupPosition != previousGroup) {
                                ((ExpandableListView) parent).collapseGroup(previousGroup);
                            }
                            previousGroup = groupPosition;
                            ((ExpandableListView) parent).expandGroup(groupPosition);
                        }

                        ((ExpandableListView) parent).smoothScrollToPosition(groupPosition);

                        ArrayList<CategoryModel> arrCat = navigationItemsList.get(categoryId);
                        if (arrCat != null && arrCat.size() > 0) {
                            for (int i = 0; i < arrCat.size(); i++) {
                                String catName = arrCat.get(i).getCategoryName();
                                String catId = arrCat.get(i).getCategoryId();
                                if (catName != null && !TextUtils.isEmpty(catName)) {
                                    ArrayList<CategoryModel> subcategoriesList = databaseObj.getNavigationSubcategories(catId);
                                    if (subcategoriesList != null) {
                                        navigationItemsListTemp.put(catId, subcategoriesList);
                                    }
                                }
                            }
                        } else {
                            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {

                                mDrawerLayout.closeDrawer(Gravity.LEFT);
                            } else {

                                mDrawerLayout.openDrawer(Gravity.LEFT);
                            }

                            Utils.callCrashlyticsViewEvent(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "0"), "category_id", categoryId);

                            Intent intent = new Intent(BaseActivity.this, MainActivityNew.class);
                            intent.putExtra("Type", "SubCategories");
                            intent.putExtra("Id", categoryId);
                            intent.putExtra("key_id", "category_id");
                            intent.putExtra("Name", categoryName);
                            if (mContext instanceof MainActivityNew)
                                ((Activity) mContext).finish();
                            startActivity(intent);

                        }
                    }
                });
            }

            return view;

        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition,
                                 boolean isLastChild, View view, final ViewGroup parent) {
            if (view == null) {
                view = layoutInflater.inflate(R.layout.expandablelistviewsubcat, parent, false);
            }
            final CategoryModel singleChild = getChild(groupPosition, childPosition);

            TextView childSubCategoryName = (TextView) view.findViewById(R.id.subcat_name);
            final ListView childListView = (ListView) view.findViewById(R.id.list_items);

            final String categoryName = singleChild.getCategoryName();
            Spanned string = Html.fromHtml(categoryName);

            childView = view;
            childSubCategoryName.setText(string);

            view.setTag(singleChild);

            final View finalView = view;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String categoryId = singleChild.getCategoryId();
                    Log.e("child clicked", "" + categoryName + "-----" + categoryId);

                    if (categoryName != null && !TextUtils.isEmpty(categoryName)) {
                        ArrayList<CategoryModel> subcategoriesList = databaseObj.getNavigationSubcategories(categoryId);
                        ArrayList<CategoryModel> subCatTemp = (ArrayList<CategoryModel>) childListView.getTag();
                        if (subcategoriesList != null && subcategoriesList.size() > 0)
                            childListView.setTag(subcategoriesList);

                        if (equalLists(subcategoriesList, subCatTemp)) {
                            childListView.setVisibility(View.GONE);
                            childListView.setTag(new ArrayList<CategoryModel>());
                        } else {
                            childListView.setVisibility(View.VISIBLE);
                        }
                        if (subcategoriesList != null && subcategoriesList.size() > 0) {

                            childListView.setAdapter(new NavigationAdapter(BaseActivity.this, subcategoriesList));
                            Utils.setListViewHeightBasedOnItems(childListView);

                        } else {
                            childListView.setVisibility(View.GONE);
                            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {

                                mDrawerLayout.closeDrawer(Gravity.LEFT);
                            } else {

                                mDrawerLayout.openDrawer(Gravity.LEFT);
                            }

                            Utils.callCrashlyticsViewEvent(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, "0"), "category_id", categoryId);

                            Intent intent = new Intent(BaseActivity.this, MainActivityNew.class);
                            intent.putExtra("Type", "SubCategories");
                            intent.putExtra("Id", categoryId);
                            intent.putExtra("Name", categoryName);
                            if (mContext instanceof MainActivityNew)
                                ((Activity) mContext).finish();
                            startActivity(intent);

                        }
                    }

                }
            });

            return view;

        }

        @Override
        public boolean isChildSelectable(int i, int i1) {
            return true;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

    }

}
