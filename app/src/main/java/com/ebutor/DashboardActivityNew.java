package com.ebutor;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ebutor.fragments.DashBoardFragmentNew;
import com.ebutor.fragments.MyTeamDashBoardFragment;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.DashBoardTypes;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 26-Sep-16.
 */

public class DashboardActivityNew extends ParentActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private int[] imageResId = {
            R.drawable.icon_overall,
            R.drawable.icon_user,
            R.drawable.icon_users
    };

    private ArrayList<Integer> imageIds;
    boolean hasChilds = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Dashboard");


        imageIds = new ArrayList<>();
        SharedPreferences mSharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        hasChilds = mSharedPreferences.getBoolean(ConstantValues.KEY_HAS_CHILD, false);
        imageIds.add(R.drawable.icon_overall);
        imageIds.add(R.drawable.icon_user);
        if(hasChilds){
            imageIds.add(R.drawable.icon_users);
        }

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        setupTabIcons(tabLayout);

    }
    private void setupTabIcons(TabLayout tabLayout) {
        tabLayout.getTabAt(0).setIcon(imageResId[0]);
        tabLayout.getTabAt(1).setIcon(imageResId[1]);
        if(hasChilds)
            tabLayout.getTabAt(2).setIcon(imageResId[2]);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Fragment fragment = null;

            switch (position) {
                case 0:
                    fragment = DashBoardFragmentNew.newInstance(DashBoardTypes.ORGANIZATION);
                    break;
                case 1:
                    fragment = DashBoardFragmentNew.newInstance(DashBoardTypes.MY_DASHBOARD);
                    break;
                case 2:
                    fragment = MyTeamDashBoardFragment.newInstance(DashBoardTypes.MY_TEAM_DASHBOARD);
                    break;
                default:
                    break;
            }

            return fragment;
        }


        @Override
        public int getCount() {
            // Show 3 total pages.
            return imageIds.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
        /*@Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            // return tabTitles[position];

            // getDrawable(int i) is deprecated, use getDrawable(int i, Theme theme) for min SDK >=21
            // or ContextCompat.getDrawable(Context context, int id) if you want support for older versions.
            // Drawable image = context.getResources().getDrawable(iconIds[position], context.getTheme());
            // Drawable image = context.getResources().getDrawable(imageResId[position]);

            Drawable image = ContextCompat.getDrawable(DashboardActivityNew.this, imageResId[position]);
            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            SpannableString sb = new SpannableString(" ");
            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return sb;
        }*/
    }
}
