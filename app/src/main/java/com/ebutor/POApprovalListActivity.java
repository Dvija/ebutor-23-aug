package com.ebutor;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeTransform;
import android.view.MenuItem;
import android.view.Window;

import com.ebutor.fragments.POApprovalListFragment;
import com.ebutor.models.StatusModel;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class POApprovalListActivity extends ParentActivity {

    private String code = "", name = "";
    private int count;
    private StatusModel statusModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setAllowReturnTransitionOverlap(true);
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setSharedElementExitTransition(new ChangeTransform());
        }

        setContentView(R.layout.empty_fragment_layout_suppliers);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name = getString(R.string.app_name);

        if (getIntent().hasExtra("model"))
            statusModel = (StatusModel) getIntent().getSerializableExtra("model");
        if (statusModel != null) {
            name = statusModel.getName();
            code = statusModel.getCode();
        }

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(name);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDefaultDisplayHomeAsUpEnabled(true);
        }
        if (savedInstanceState == null) {
            Fragment fragment = new POApprovalListFragment();
            Bundle bundle = new Bundle();
            bundle.putString("code", code);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
