package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.LinearLayout;

import com.ebutor.fragments.FilterFFFragment;
import com.ebutor.fragments.FilterFFFragmentNew;
import com.ebutor.utils.DashBoardTypes;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FilterFFActivity extends BaseActivity {

    private LinearLayout llFilterFF;
    private int type = DashBoardTypes.ORGANIZATION;
    private String pos = "", startDate, endDate;
    private boolean isMain = false, isFromSO = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llFilterFF = (LinearLayout) inflater.inflate(R.layout.empty_fragment_layout, null, false);
        flbody.addView(llFilterFF, baseLayoutParams);

        setContext(FilterFFActivity.this);

        if (getIntent().hasExtra("type"))
            type = getIntent().getIntExtra("type", DashBoardTypes.ORGANIZATION);
        if (getIntent().hasExtra("pos"))
            pos = getIntent().getStringExtra("pos");
        if (getIntent().hasExtra("start_date"))
            startDate = getIntent().getStringExtra("start_date");
        if (getIntent().hasExtra("end_date"))
            endDate = getIntent().getStringExtra("end_date");
        if (getIntent().hasExtra("is_main"))
            isMain = getIntent().getBooleanExtra("is_main", false);
        if (getIntent().hasExtra("isFromSO"))
            isFromSO = getIntent().getBooleanExtra("isFromSO", false);

        Fragment fragment = null;
        if (isFromSO) {
            fragment = new FilterFFFragmentNew();
        } else {
            fragment = new FilterFFFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("pos", pos);
        if (startDate != null)
            bundle.putString("start_date", startDate);
        if (endDate != null)
            bundle.putString("end_date", endDate);
        bundle.putBoolean("is_main", isMain);
        bundle.putBoolean("isFromSO", isFromSO);
        fragment.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
