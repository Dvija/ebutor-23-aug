package com.ebutor;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.ebutor.fragments.BrandProductFragment;

public class BrandProductsActivity extends BaseActivity {

    public String manufacturerId,manufacturerName,flag,key;
    private LinearLayout llBrandProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llBrandProducts = (LinearLayout) inflater.inflate(R.layout.activity_brand_product,null,false);
        flbody.addView(llBrandProducts,baseLayoutParams);

        setContext(BrandProductsActivity.this);

        if(getIntent().getStringExtra("ManufacturerId")!=null)
            manufacturerId = getIntent().getStringExtra("ManufacturerId");
        if(getIntent().getStringExtra("Flag")!=null)
            flag = getIntent().getStringExtra("Flag");
        if(getIntent().getStringExtra("key_id")!=null)
            key = getIntent().getStringExtra("key_id");
        if(getIntent().getStringExtra("ManufacturerName")!=null)
            manufacturerName = getIntent().getStringExtra("ManufacturerName");

        BrandProductFragment fr = new BrandProductFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ManufacturerId", manufacturerId);
        bundle.putString("ManufacturerName",manufacturerName);
        bundle.putString("Flag",flag);
        bundle.putString("key_id",key);
        fr.setArguments(bundle);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frag_brand_product, fr);
        fragmentTransaction.commit();
    }
}
