package com.ebutor;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.fragments.OrderDetailFragment;
import com.ebutor.fragments.ShipmentTrackingFragment;
import com.ebutor.models.TabModel;
import com.ebutor.tabspager.SlidingTabLayout;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderDetailActivity extends BaseActivity {

    private String orderId = "", status;
    private LinearLayout llOrderDetail;
    private Dialog dialog;
//    private List<SamplePagerItem> mTabs = new ArrayList<SamplePagerItem>();
//    private ViewPager mViewPager;
//    private ArrayList<TabModel> arrTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llOrderDetail = (LinearLayout) inflater.inflate(R.layout.empty_fragment_layout,null,false);
        flbody.addView(llOrderDetail,baseLayoutParams);

//        arrTabs = new ArrayList<>();

        dialog = Utils.createLoader(OrderDetailActivity.this,ConstantValues.TABS_PROGRESS);

        setContext(OrderDetailActivity.this);

        if (getIntent().hasExtra("orderId"))
            orderId = getIntent().getStringExtra("orderId");
        if (getIntent().hasExtra("status")) {
            status = getIntent().getStringExtra("status");
        }

//        mViewPager = (ViewPager) findViewById(R.id.pager);
//        mViewPager.setOffscreenPageLimit(1);
//
//        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//
//        TabModel model = new TabModel();
//        model.setTabId("1");
//        model.setTabName("Detail");
//        model.setTabValue("");
//        arrTabs.add(model);
//        TabModel model1 = new TabModel();
//        model1.setTabId("2");
//        model1.setTabName("Track");
//        model1.setTabValue("");
//        arrTabs.add(model1);
//        if (arrTabs != null && arrTabs.size() > 0) {
//            for (int i = 0; i < arrTabs.size(); i++) {
//                mTabs.add(new SamplePagerItem(
//                        arrTabs.get(i).getTabName().toUpperCase(), // Title
//                        this.getResources().getColor(R.color.primary), // Indicator color
//                        Color.GRAY // Divider color
//                ));
//            }
//        }
//        mViewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
//        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        mSlidingTabLayout.setCustomTabView(R.layout.tab_layout, R.id.tab_heading);
//        mSlidingTabLayout.setDistributeEvenly(true);
//        mSlidingTabLayout.setViewPager(mViewPager);
//        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//
//            @Override
//            public int getIndicatorColor(int position) {
//                return mTabs.get(position).getIndicatorColor();
//            }
//
//        });

        if (savedInstanceState == null) {
            Fragment fragment = new OrderDetailFragment();
            Bundle args = new Bundle();
            args.putString("orderId", orderId);
            args.putString("status", status);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    static class SamplePagerItem {
        private final CharSequence mTitle;
        private final int mIndicatorColor;
        private final int mDividerColor;

        SamplePagerItem(CharSequence title, int indicatorColor, int dividerColor) {
            mTitle = title;
            mIndicatorColor = indicatorColor;
            mDividerColor = dividerColor;
        }

        CharSequence getTitle() {
            return mTitle;
        }

        int getIndicatorColor() {
            return mIndicatorColor;
        }

        int getDividerColor() {
            return mDividerColor;
        }
    }

//    public class SectionsPagerAdapter extends FragmentPagerAdapter {
//        public SectionsPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public int getItemPosition(Object object) {
//            return POSITION_NONE;
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            Fragment fragment = null;
//            String tabType = "";
//            for (int i = 0; i < arrTabs.size(); i++) {
//                String tabName = arrTabs.get(i).getTabName();
//                if(!TextUtils.isEmpty(tabName)){
//                    if(position== i){
//                        tabType = tabName;
//                    }
//                }
//            }
//
//            switch (position) {
//                case 0:
//                    fragment = new OrderDetailFragment();
//                    Bundle args = new Bundle();
//                    args.putString("orderId", orderId);
//                    args.putString("status", status);
//                    fragment.setArguments(args);
//                    break;
//                case 1:
//                    fragment = new ShipmentTrackingFragment();
//                    break;
//            }
//            return fragment;
//        }
//
//        @Override
//        public int getCount() {
//            return mTabs.size();
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mTabs.get(position).getTitle();
//        }
//    }
}
