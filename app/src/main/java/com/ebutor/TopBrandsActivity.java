package com.ebutor;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import android.widget.LinearLayout;


import com.ebutor.fragments.TopBrandsFragment;

/**
 * Created by 300024 on 3/17/2016.
 */
public class TopBrandsActivity extends BaseActivity {

    private LinearLayout llTopBrands;
    private String flag,keyId;
    private int cartCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llTopBrands = (LinearLayout) inflater.inflate(R.layout.activity_recommended_products,null,false);
        flbody.addView(llTopBrands,baseLayoutParams);

        setContext(TopBrandsActivity.this);

        if(getIntent().getStringExtra("key_id")!=null)
            keyId = getIntent().getStringExtra("key_id");
        if(getIntent().getStringExtra("Flag")!=null)
            flag = getIntent().getStringExtra("Flag");

        TopBrandsFragment fragment = new TopBrandsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("Flag",flag);
        bundle.putString("key_id",keyId);
        fragment.setArguments(bundle);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();

    }

    public void updateCart(int cartCount) {
        this.cartCount = cartCount;
        invalidate(cartCount);
    }
}
