package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.ebutor.fragments.ProductDetailFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetailsActivity extends BaseActivity {

    private String productId = "",parentId = "";
    private int cartCount = 0;
    private String position;
    private LinearLayout llProductDetail;
    private ProductDetailFragment fr;
    private boolean isChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llProductDetail = (LinearLayout) inflater.inflate(R.layout.activity_product_detail, null, false);
        flbody.addView(llProductDetail, baseLayoutParams);

        setContext(ProductDetailsActivity.this);

        if (getIntent().getStringExtra("ProductId") != null)
            productId = getIntent().getStringExtra("ProductId");
        if (getIntent().getStringExtra("ParentId") != null)
            parentId = getIntent().getStringExtra("ParentId");
        if (getIntent().getBooleanExtra("isChild",false))
            isChild = getIntent().getBooleanExtra("isChild",false);
        if (getIntent().getStringExtra("position") != null)
            position = getIntent().getStringExtra("position");

        fr = new ProductDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ProductId", productId);
        bundle.putString("ParentId", parentId);
        bundle.putBoolean("isChild", isChild);
        bundle.putString("position", position);
        fr.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frag_product, fr);
        fragmentTransaction.commit();
    }

    public void updateCart(int cartCount) {
        this.cartCount = cartCount;
        invalidate(cartCount);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (fr != null) {
//            fr.onBackPressed();
//        } else {
//            super.onBackPressed();
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            if (fr != null) {
//                fr.onBackPressed();
//            } else {
//                finish();
//            }
//        }
        return super.onOptionsItemSelected(item);
    }
}
