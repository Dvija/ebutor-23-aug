package com.ebutor.services;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by 480116 on 6/7/2017.
 */

public class Locations extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    public static final String
            ACTION_LOCATION_BROADCAST = Locations.class.getName() + "LocationBroadcast",
            EXTRA_LATITUDE = "extra_latitude",
            EXTRA_LONGITUDE = "extra_longitude",
            POSTAL_CODE = "postal_code",
            STREET = "street",
            ADDRESS_LINE1 = "addressline1",
            ADDRESS_LINE2 = "addressline2",
            SUB_Locality = "sub_locaity";
    static final String tag = "message";
    public static Location mlocation = null;
    private static boolean isRunning = false;
    GoogleApiClient googleapiclient;
    LocationRequest locationRequest = null;
    NotificationManager manager;
    Geocoder geo = null;
    List<Address> address = null;
    String country, street, way, postalcode, addressLine, addressLine2;
    //    ArrayList<Messenger> mclients=new ArrayList<Messenger>();
    //private Timer timer = new Timer();
    int counter = 0;
    int incrementby = 1;
    int mValue = 0;
    int c = 0;


    //  final Messenger mMessanger=new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(tag, "oncreate");
        googleapiclient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        // locationRequest.setFastestInterval(1000);
        // locationRequest.setSmallestDisplacement(1);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!googleapiclient.isConnected()) {
            googleapiclient.connect();
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }
        mlocation = LocationServices.FusedLocationApi.getLastLocation(googleapiclient);
        Log.d(tag, "onstartcommand");
        return Service.START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(tag, "ondestroy");
        if (googleapiclient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleapiclient, this);
            googleapiclient.disconnect();
        }
        //  Process.killProcess(Process.myPid());
        this.stopSelf();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.d(tag, "onconnected");
       /* if (mlocation == null) {
            mlocation = LocationServices.FusedLocationApi.getLastLocation(googleapiclient);
            handlelocation();
        }*/
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ActivityCompat.requestPermissions((Activity) getBaseContext(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 111);
                }
                return;
            }
            mlocation = LocationServices.FusedLocationApi.getLastLocation(googleapiclient);
            if (mlocation != null) {
                handlelocation();
            }
            PendingResult<Status> status = LocationServices.FusedLocationApi.requestLocationUpdates(googleapiclient, locationRequest, this);

        } catch (Throwable t) {
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        this.stopSelf();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(tag, "onconection faled");
        this.stopSelf();
    }

    @Override
    public void onLocationChanged(Location location) {
       /* if(c==1){
            LocationServices.FusedLocationApi.removeLocationUpdates(googleapiclient,this);
            this.stopSelf();
            c=0;
        }*/
        Log.d(tag, "on location change");
        mlocation = location;
        geo = new Geocoder(this, Locale.getDefault());
        try {
            address = geo.getFromLocation(mlocation.getLatitude(), mlocation.getLongitude(), 1);
            addressLine = address.get(0).getAddressLine(0);
            postalcode = address.get(0).getPostalCode();
            country = address.get(0).getCountryName();
            street = address.get(0).getLocality();
            way = address.get(0).getPremises();
            addressLine2 = address.get(0).getAddressLine(1);

        } catch (IOException e) {
            e.printStackTrace();
        }

        // sendLogMessageToUi("location is "+mlocation.getLatitude()+mlocation.getLongitude());
        handlelocation();
    }

    public void handlelocation() {
        if (mlocation == null) {
        } else {
            Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
            intent.putExtra(EXTRA_LATITUDE, mlocation.getLatitude());
            intent.putExtra(EXTRA_LONGITUDE, mlocation.getLongitude());
            intent.putExtra(POSTAL_CODE, postalcode);
            intent.putExtra(ADDRESS_LINE1, addressLine);
            intent.putExtra(ADDRESS_LINE2, addressLine2);
            intent.putExtra(STREET, street);
            intent.putExtra(SUB_Locality, street);

            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            LocationServices.FusedLocationApi.removeLocationUpdates(googleapiclient, this);
        }
    }
}
