package com.ebutor.searchview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.SearchModel;

import java.text.Normalizer;
import java.util.ArrayList;

/**
 * @author Miguel Catalan Bañuls
 */
public class SearchAdapter extends BaseAdapter {

    LayoutInflater inflater;
    private ArrayList<SearchModel> data;
    private ArrayList<SearchModel> typeAheadData;
    private Drawable suggestionIcon;
    private String searchText = "";

    public SearchAdapter(Context context, ArrayList<SearchModel> typeAheadData) {
        inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
        this.typeAheadData = typeAheadData;
    }

    public SearchAdapter(Context context, ArrayList<SearchModel> typeAheadData, Drawable suggestionIcon) {
        inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
        this.typeAheadData = typeAheadData;
        this.suggestionIcon = suggestionIcon;
    }

    public static CharSequence highlight(String search, String originalText) {
        // ignore case and accents
        // the same thing should have been done for the search text
        String normalizedText = Normalizer.normalize(originalText, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();

        int start = normalizedText.indexOf(search);
        if (start < 0) {
            // not found, nothing to to
            return originalText;
        } else {
            // highlight each appearance in the original text
            // while searching in normalized text
            Spannable highlighted = new SpannableString(originalText);
            while (start >= 0) {
                int spanStart = Math.min(start, originalText.length());
                int spanEnd = Math.min(start + search.length(), originalText.length());

                highlighted.setSpan(new ForegroundColorSpan(Color.RED), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                start = normalizedText.indexOf(search, spanEnd);
            }

            return highlighted;
        }
    }

    /*@Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (!TextUtils.isEmpty(constraint)) {
                    // Retrieve the autocomplete results.
                    List<SearchModel> searchData = new ArrayList<>();
                    searchText = constraint.toString();

                    for (SearchModel model : typeAheadData) {
                        searchData.add(model);
                    }

                    // Assign the data to the FilterResults
                    filterResults.values = searchData;
                    filterResults.count = searchData.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.values != null) {
                    data = (ArrayList<SearchModel>) results.values;
                    notifyDataSetChanged();
                }
            }
        };
        return filter;
    }*/

    @Override
    public int getCount() {
        return typeAheadData.size();
    }

    @Override
    public Object getItem(int position) {
        return typeAheadData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void refreshDataSet(ArrayList<SearchModel> updatedData){
        data = new ArrayList<>();
        this.typeAheadData = updatedData;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.suggest_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        String currentListData = ((SearchModel) typeAheadData.get(position)).getName();
//        mViewHolder.textView.setText(currentListData);
        if (!TextUtils.isEmpty(searchText)) {
            CharSequence listdata = highlight(searchText, currentListData);
            mViewHolder.textView.setText(listdata);
        }

        return convertView;
    }

    public void setSearchText(CharSequence charSequence){
        this.searchText = charSequence.toString();
    }

    private class MyViewHolder {
        TextView textView;
        ImageView imageView;

        public MyViewHolder(View convertView) {
            textView = (TextView) convertView.findViewById(R.id.suggestion_text);


            if (suggestionIcon != null) {
                imageView = (ImageView) convertView.findViewById(R.id.suggestion_icon);
                imageView.setImageDrawable(suggestionIcon);
            }
        }
    }
}