package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.widget.LinearLayout;

import com.ebutor.fragments.OrdersListFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrdersListActivity extends BaseActivity {

    private LinearLayout llOrderList;
    private String customerToken, custId, leEntityId;
    private OrdersListFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llOrderList = (LinearLayout) inflater.inflate(R.layout.empty_fragment_layout, null, false);
        flbody.addView(llOrderList, baseLayoutParams);

        setContext(OrdersListActivity.this);

        if (getIntent().hasExtra("token")) {
            customerToken = getIntent().getStringExtra("token");
            custId = getIntent().getStringExtra("cust_id");
            leEntityId = getIntent().getStringExtra("le_entity_id");
        }

        if (savedInstanceState == null) {
            fragment = new OrdersListFragment();
            Bundle bundle = new Bundle();
            bundle.putString("token", customerToken);
            bundle.putString("cust_id", custId);
            bundle.putString("le_entity_id", leEntityId);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }

    }

    public void replaceFragment() {
        if (fragment != null) {
            fragment.getOrdersList("", "", "");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
