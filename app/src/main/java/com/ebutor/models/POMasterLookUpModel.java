package com.ebutor.models;


import java.util.ArrayList;

public class POMasterLookUpModel {

    private ArrayList<CustomerTyepModel> arrPaymentType,arrPaymentMode,arrPaidThrough;

    public ArrayList<CustomerTyepModel> getArrPaymentType() {
        return arrPaymentType;
    }

    public void setArrPaymentType(ArrayList<CustomerTyepModel> arrPaymentType) {
        this.arrPaymentType = arrPaymentType;
    }

    public ArrayList<CustomerTyepModel> getArrPaymentMode() {
        return arrPaymentMode;
    }

    public void setArrPaymentMode(ArrayList<CustomerTyepModel> arrPaymentMode) {
        this.arrPaymentMode = arrPaymentMode;
    }

    public ArrayList<CustomerTyepModel> getArrPaidThrough() {
        return arrPaidThrough;
    }

    public void setArrPaidThrough(ArrayList<CustomerTyepModel> arrPaidThrough) {
        this.arrPaidThrough = arrPaidThrough;
    }
}
