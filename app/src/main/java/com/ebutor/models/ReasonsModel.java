package com.ebutor.models;

import java.io.Serializable;

public class ReasonsModel implements Serializable{
//    private String reasonId;
    private String reason;
    private String reasonValue;

//    public String getReasonId() {
//        return reasonId;
//    }
//
//    public void setReasonId(String reasonId) {
//        this.reasonId = reasonId;
//    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonValue() {
        return reasonValue;
    }

    public void setReasonValue(String reasonValue) {
        this.reasonValue = reasonValue;
    }
}
