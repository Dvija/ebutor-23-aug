package com.ebutor.models;


public class FilterDataModel {

    private String key;
    private String value;
    private String id;
    private boolean isChecked;

    @Override
    public String toString() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public boolean equals(Object o) {
        return id.equalsIgnoreCase(o.toString());
    }
}
