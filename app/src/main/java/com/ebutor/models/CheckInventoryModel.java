package com.ebutor.models;

/**
 * Created by Srikanth Nama on 16-Sep-16.
 */
public class CheckInventoryModel {
    private String productId;
    private String cartId;
    private String warehouseId, star;
    private int status;
    private int availableQty;
    private int isSlab, blockedQty;
    private int promotionId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(int availableQty) {
        this.availableQty = availableQty;
    }

    public int getIsSlab() {
        return isSlab;
    }

    public void setIsSlab(int isSlab) {
        this.isSlab = isSlab;
    }

    public int getBlockedQty() {
        return blockedQty;
    }

    public void setBlockedQty(int blockedQty) {
        this.blockedQty = blockedQty;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }
}
