package com.ebutor.models;

import java.util.ArrayList;

public class NotificationModel {

    private String referenceId, message, id, date;
    private int count;
    private ArrayList<NotificationModel> arrNotifications;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<NotificationModel> getArrNotifications() {
        return arrNotifications;
    }

    public void setArrNotifications(ArrayList<NotificationModel> arrNotifications) {
        this.arrNotifications = arrNotifications;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
