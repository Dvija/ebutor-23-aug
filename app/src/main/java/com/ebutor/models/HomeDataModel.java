package com.ebutor.models;

import java.util.ArrayList;

public class HomeDataModel {

    String id;
    String name;
    String image;
    String flag;
    String key;
    String displayTitle;
    ArrayList<String> arrImages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<String> getArrImages() {
        return arrImages;
    }

    public void setArrImages(ArrayList<String> arrImages) {
        this.arrImages = arrImages;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }
}
