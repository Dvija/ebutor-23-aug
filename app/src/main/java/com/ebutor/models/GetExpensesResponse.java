package com.ebutor.models;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 27-Jan-17.
 */

public class GetExpensesResponse {

    private String walletTotal;
    private ArrayList<ExpenseModel> expenses;

    public String getWalletTotal() {
        return walletTotal;
    }

    public void setWalletTotal(String walletTotal) {
        this.walletTotal = walletTotal;
    }

    public ArrayList<ExpenseModel> getExpenses() {
        return expenses;
    }

    public void setExpenses(ArrayList<ExpenseModel> expenses) {
        this.expenses = expenses;
    }
}
