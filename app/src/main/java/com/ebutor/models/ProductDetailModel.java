package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 3/17/2016.
 */
public class ProductDetailModel implements Serializable {

    private String productName;
    private String productId;
    private int position;
    private String productRating;
    private String productDescription;
    private ArrayList<SKUModel> skuModelArrayList;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ArrayList<SKUModel> getSkuModelArrayList() {
        return skuModelArrayList;
    }

    public String getProductRating() {
        return productRating;
    }

    public void setProductRating(String productRating) {
        this.productRating = productRating;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setSkuModelArrayList(ArrayList<SKUModel> skuModelArrayList) {
        this.skuModelArrayList = skuModelArrayList;
    }
}
