package com.ebutor.models;

/**
 * Created by Srikanth Nama on 15-Sep-16.
 */
public class AddCart1Response {
    private boolean status;
    private String availableQty;
    private String productId;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(String availableQty) {
        this.availableQty = availableQty;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
