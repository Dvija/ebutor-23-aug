package com.ebutor.models;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 29-Dec-16.
 */

public class ExpenseMasterlookupModel {

    private ArrayList<ExpenseMasterLookupData> requestTypes;
    private ArrayList<ExpenseMasterLookupData> expenseTypes;
    private ArrayList<ExpenseMasterLookupData> requestForTypes;

    public ArrayList<ExpenseMasterLookupData> getRequestTypes() {
        return requestTypes;
    }

    public void setRequestTypes(ArrayList<ExpenseMasterLookupData> requestTypes) {
        this.requestTypes = requestTypes;
    }

    public ArrayList<ExpenseMasterLookupData> getExpenseTypes() {
        return expenseTypes;
    }

    public void setExpenseTypes(ArrayList<ExpenseMasterLookupData> expenseTypes) {
        this.expenseTypes = expenseTypes;
    }

    public ArrayList<ExpenseMasterLookupData> getRequestForTypes() {
        return requestForTypes;
    }

    public void setRequestForTypes(ArrayList<ExpenseMasterLookupData> requestForTypes) {
        this.requestForTypes = requestForTypes;
    }
}
