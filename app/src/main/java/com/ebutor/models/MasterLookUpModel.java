package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 300024 on 7/26/2016.
 */
public class MasterLookUpModel implements Serializable {
    private ArrayList<BusinessTypeModel> businessTypeArr;
    private ArrayList<CustomerTyepModel> arrCustomers;
    private ArrayList<CustomerTyepModel> arrVolumes;
    private ArrayList<CustomerTyepModel> arrLicense;
    private ArrayList<CustomerTyepModel> arrManf;

    public ArrayList<BusinessTypeModel> getBusinessTypeArr() {
        return businessTypeArr;
    }

    public void setBusinessTypeArr(ArrayList<BusinessTypeModel> businessTypeArr) {
        this.businessTypeArr = businessTypeArr;
    }

    public ArrayList<CustomerTyepModel> getArrCustomers() {
        return arrCustomers;
    }

    public void setArrCustomers(ArrayList<CustomerTyepModel> arrCustomers) {
        this.arrCustomers = arrCustomers;
    }

    public ArrayList<CustomerTyepModel> getArrVolumes() {
        return arrVolumes;
    }

    public void setArrVolumes(ArrayList<CustomerTyepModel> arrVolumes) {
        this.arrVolumes = arrVolumes;
    }

    public ArrayList<CustomerTyepModel> getArrLicense() {
        return arrLicense;
    }

    public void setArrLicense(ArrayList<CustomerTyepModel> arrLicense) {
        this.arrLicense = arrLicense;
    }

    public ArrayList<CustomerTyepModel> getArrManf() {
        return arrManf;
    }

    public void setArrManf(ArrayList<CustomerTyepModel> arrManf) {
        this.arrManf = arrManf;
    }
}
