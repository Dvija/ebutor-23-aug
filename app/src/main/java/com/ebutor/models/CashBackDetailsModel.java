package com.ebutor.models;

import java.io.Serializable;

/**
 * Created by 480108 on 8/3/2017.
 */

public class CashBackDetailsModel implements Serializable {

    double cbk_value;
    int cashback_id;
    int reference_id;
    int cashback_type;
    double qty_from_range;
    int cbk_source_type;
    double ff_qty_to_range;
    int benificiary_type;
    String cashback_description;
    String cashbackStar;

    public double getCbk_value() {
        return cbk_value;
    }

    public void setCbk_value(double cbk_value) {
        this.cbk_value = cbk_value;
    }

    public int getCashback_id() {
        return cashback_id;
    }

    public void setCashback_id(int cashback_id) {
        this.cashback_id = cashback_id;
    }

    public int getReference_id() {
        return reference_id;
    }

    public void setReference_id(int reference_id) {
        this.reference_id = reference_id;
    }

    public int getCashback_type() {
        return cashback_type;
    }

    public void setCashback_type(int cashback_type) {
        this.cashback_type = cashback_type;
    }

    public double getQty_from_range() {
        return qty_from_range;
    }

    public void setQty_from_range(double qty_from_range) {
        this.qty_from_range = qty_from_range;
    }

    public int getCbk_source_type() {
        return cbk_source_type;
    }

    public void setCbk_source_type(int cbk_source_type) {
        this.cbk_source_type = cbk_source_type;
    }

    public double getFf_qty_to_range() {
        return ff_qty_to_range;
    }

    public void setFf_qty_to_range(double ff_qty_to_range) {
        this.ff_qty_to_range = ff_qty_to_range;
    }

    public int getBenificiary_type() {
        return benificiary_type;
    }

    public void setBenificiary_type(int benificiary_type) {
        this.benificiary_type = benificiary_type;
    }

    public String getCashback_description() {
        return cashback_description;
    }

    public void setCashback_description(String cashback_description) {
        this.cashback_description = cashback_description;
    }

    public String getCashbackStar() {
        return cashbackStar;
    }

    public void setCashbackStar(String cashbackStar) {
        this.cashbackStar = cashbackStar;
    }
}
