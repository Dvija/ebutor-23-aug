package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 300024 on 3/29/2016.
 */
public class ProductFilterModel implements Serializable{
    private String attributeId,groupName,minValue = "",maxValue = "",selMin = "",selMax = "",FilterType = "",fastMoving = "";
    ArrayList<FilterModel> arrFilters;
    ArrayList<ManufacturerModel> arrManf;
    ArrayList<BrandModel> arrBrands;
    ArrayList<CategoryModel> arrCat;

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<FilterModel> getArrFilters() {
        return arrFilters;
    }

    public void setArrFilters(ArrayList<FilterModel> arrFilters) {
        this.arrFilters = arrFilters;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getSelMin() {
        return selMin;
    }

    public void setSelMin(String selMin) {
        this.selMin = selMin;
    }

    public String getSelMax() {
        return selMax;
    }

    public void setSelMax(String selMax) {
        this.selMax = selMax;
    }

    public String getFilterType() {
        return FilterType;
    }

    public void setFilterType(String filterType) {
        FilterType = filterType;
    }

    public String getFastMoving() {
        return fastMoving;
    }

    public void setFastMoving(String fastMoving) {
        this.fastMoving = fastMoving;
    }

    public ArrayList<ManufacturerModel> getArrManf() {
        return arrManf;
    }

    public void setArrManf(ArrayList<ManufacturerModel> arrManf) {
        this.arrManf = arrManf;
    }

    public ArrayList<BrandModel> getArrBrands() {
        return arrBrands;
    }

    public void setArrBrands(ArrayList<BrandModel> arrBrands) {
        this.arrBrands = arrBrands;
    }

    public ArrayList<CategoryModel> getArrCat() {
        return arrCat;
    }

    public void setArrCat(ArrayList<CategoryModel> arrCat) {
        this.arrCat = arrCat;
    }
}
