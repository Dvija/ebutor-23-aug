package com.ebutor.models;

/**
 * Created by Srikanth Nama on 28-Sep-16.
 */
public class DashboardModel {
    private String key;
    private String value;
    private String per;

    public DashboardModel(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getPer() {
        return per;
    }

    public void setPer(String per) {
        this.per = per;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
