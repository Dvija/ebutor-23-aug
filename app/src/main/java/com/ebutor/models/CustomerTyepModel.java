package com.ebutor.models;

        import java.io.Serializable;

/**
 * Created by 300024 on 3/9/2016.
 */
public class CustomerTyepModel implements Serializable {
    public String customerName,customerGrpName;
    public String customerGrpId;
    public String customerToken;
    public String customerId, firstName, lastName, profilePic, segmentId, businessStartTime, businessEndTime;
    public int isFF, isSRM, hasChild;
    public int isDashboard;
    private boolean isLogin, isSmartPhone, isInternet,isPremium;
    private String documents, company, address1, address2, city, country, postcode, telephone, email, addressId, businessType, buyerType, whId, manfIds;
    private boolean isActive, isChecked, isParent;
    private String noOfShutters, areaId, area, state, volumeClass, deliveryTime, contactName1, contactNo1, contactName2, contactNo2, userId1, userId2;
    private String legalEntityId, prefSlot1, prefSlot2;
    private String locality, landmark,beatId,latitude,longitude;
    private String hub,key,userId,description,gst,arn;
    private int color;
    private double creditLimit,ffCreditLimit;
    private double ecash,ffecash;
    private double paymentDue,ffPaymentDue;

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(String customerToken) {
        this.customerToken = customerToken;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public String getBuyerType() {
        return buyerType;
    }

    public void setBuyerType(String buyerType) {
        this.buyerType = buyerType;
    }

    public int getIsFF() {
        return isFF;
    }

    public void setIsFF(int isFF) {
        this.isFF = isFF;
    }

    public int getIsSRM() {
        return isSRM;
    }

    public void setIsSRM(int isSRM) {
        this.isSRM = isSRM;
    }

    public int getHasChild() {
        return hasChild;
    }

    public void setHasChild(int hasChild) {
        this.hasChild = hasChild;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerGrpId() {
        return customerGrpId;
    }

    public void setCustomerGrpId(String customerGrpId) {
        this.customerGrpId = customerGrpId;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setIsLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public String getWhId() {
        return whId;
    }

    public void setWhId(String whId) {
        this.whId = whId;
    }

    @Override
    public String toString() {
        if (customerName != null && customerName.length() > 0) {
            return customerName.toString();
        } else {
            return " ";
        }
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public int getIsDashboard() {
        return isDashboard;
    }

    public void setIsDashboard(int isDashboard) {
        this.isDashboard = isDashboard;
    }

    public String getNoOfShutters() {
        return noOfShutters;
    }

    public void setNoOfShutters(String noOfShutters) {
        this.noOfShutters = noOfShutters;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVolumeClass() {
        return volumeClass;
    }

    public void setVolumeClass(String volumeClass) {
        this.volumeClass = volumeClass;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getContactName1() {
        return contactName1;
    }

    public void setContactName1(String contactName1) {
        this.contactName1 = contactName1;
    }

    public String getContactNo1() {
        return contactNo1;
    }

    public void setContactNo1(String contactNo1) {
        this.contactNo1 = contactNo1;
    }

    public String getContactName2() {
        return contactName2;
    }

    public void setContactName2(String contactName2) {
        this.contactName2 = contactName2;
    }

    public String getContactNo2() {
        return contactNo2;
    }

    public void setContactNo2(String contactNo2) {
        this.contactNo2 = contactNo2;
    }

    public String getUserId1() {
        return userId1;
    }

    public void setUserId1(String userId1) {
        this.userId1 = userId1;
    }

    public String getUserId2() {
        return userId2;
    }

    public void setUserId2(String userId2) {
        this.userId2 = userId2;
    }

    public boolean isSmartPhone() {
        return isSmartPhone;
    }

    public void setIsSmartPhone(boolean isSmartPhone) {
        this.isSmartPhone = isSmartPhone;
    }

    public boolean isInternet() {
        return isInternet;
    }

    public void setIsInternet(boolean isInternet) {
        this.isInternet = isInternet;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }

    public String getManfIds() {
        return manfIds;
    }

    public void setManfIds(String manfIds) {
        this.manfIds = manfIds;
    }

    public String getBusinessStartTime() {
        return businessStartTime;
    }

    public void setBusinessStartTime(String businessStartTime) {
        this.businessStartTime = businessStartTime;
    }

    public String getBusinessEndTime() {
        return businessEndTime;
    }

    public void setBusinessEndTime(String businessEndTime) {
        this.businessEndTime = businessEndTime;
    }

    public String getLegalEntityId() {
        return legalEntityId;
    }

    public void setLegalEntityId(String legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public String getPrefSlot1() {
        return prefSlot1;
    }

    public void setPrefSlot1(String prefSlot1) {
        this.prefSlot1 = prefSlot1;
    }

    public String getPrefSlot2() {
        return prefSlot2;
    }

    public void setPrefSlot2(String prefSlot2) {
        this.prefSlot2 = prefSlot2;
    }

    public String getBeatId() {
        return beatId;
    }

    public void setBeatId(String beatId) {
        this.beatId = beatId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCustomerGrpName() {
        return customerGrpName;
    }

    public void setCustomerGrpName(String customerGrpName) {
        this.customerGrpName = customerGrpName;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public double getEcash() {
        return ecash;
    }

    public void setEcash(double ecash) {
        this.ecash = ecash;
    }

    public double getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(double paymentDue) {
        this.paymentDue = paymentDue;
    }

    public double getFfCreditLimit() {
        return ffCreditLimit;
    }

    public void setFfCreditLimit(double ffCreditLimit) {
        this.ffCreditLimit = ffCreditLimit;
    }

    public double getFfecash() {
        return ffecash;
    }

    public void setFfecash(double ffecash) {
        this.ffecash = ffecash;
    }

    public double getFfPaymentDue() {
        return ffPaymentDue;
    }

    public void setFfPaymentDue(double ffPaymentDue) {
        this.ffPaymentDue = ffPaymentDue;
    }
}