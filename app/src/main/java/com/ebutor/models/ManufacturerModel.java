package com.ebutor.models;

/**
 * Created by Srikanth Nama on 12-Aug-16.
 */
public class ManufacturerModel {

    private String manufacturerId;
    private String manufacturerName;

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

}
