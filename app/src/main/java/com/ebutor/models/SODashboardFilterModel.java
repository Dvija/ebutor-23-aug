package com.ebutor.models;


import java.util.ArrayList;

public class SODashboardFilterModel {

    private ArrayList<CustomerTyepModel> arrWarehouses,arrHubs,arrBeats,arrFFs,arrBusinessUnits;

    public ArrayList<CustomerTyepModel> getArrWarehouses() {
        return arrWarehouses;
    }

    public void setArrWarehouses(ArrayList<CustomerTyepModel> arrWarehouses) {
        this.arrWarehouses = arrWarehouses;
    }

    public ArrayList<CustomerTyepModel> getArrHubs() {
        return arrHubs;
    }

    public void setArrHubs(ArrayList<CustomerTyepModel> arrHubs) {
        this.arrHubs = arrHubs;
    }

    public ArrayList<CustomerTyepModel> getArrBeats() {
        return arrBeats;
    }

    public void setArrBeats(ArrayList<CustomerTyepModel> arrBeats) {
        this.arrBeats = arrBeats;
    }

    public ArrayList<CustomerTyepModel> getArrFFs() {
        return arrFFs;
    }

    public void setArrFFs(ArrayList<CustomerTyepModel> arrFFs) {
        this.arrFFs = arrFFs;
    }

    public ArrayList<CustomerTyepModel> getArrBusinessUnits() {
        return arrBusinessUnits;
    }

    public void setArrBusinessUnits(ArrayList<CustomerTyepModel> arrBusinessUnits) {
        this.arrBusinessUnits = arrBusinessUnits;
    }
}
