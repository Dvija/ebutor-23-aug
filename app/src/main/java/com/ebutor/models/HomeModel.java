package com.ebutor.models;

import java.util.ArrayList;

public class HomeModel {

    String flag;
    String key;
    String title;
    ArrayList<HomeDataModel> modelArrayList;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<HomeDataModel> getModelArrayList() {
        return modelArrayList;
    }

    public void setModelArrayList(ArrayList<HomeDataModel> modelArrayList) {
        this.modelArrayList = modelArrayList;
    }
}
