package com.ebutor.models;

/**
 * Created by Srikanth Nama on 04-Jan-17.
 */

public class ImagePathData {

    private String imagePath;
    private String imageKey;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageKey() {
        return imageKey;
    }

    public void setImageKey(String imageKey) {
        this.imageKey = imageKey;
    }

    @Override
    public String toString() {
        return imageKey;
    }
}
