package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ApprovalOptionsModel implements Serializable {

    private String currentStatus,approvalUniqueId,approvalModule,tableName,uniqueColumn;
    private ArrayList<ApprovalDataModel> arrApprovalOptions;

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getApprovalUniqueId() {
        return approvalUniqueId;
    }

    public void setApprovalUniqueId(String approvalUniqueId) {
        this.approvalUniqueId = approvalUniqueId;
    }

    public String getApprovalModule() {
        return approvalModule;
    }

    public void setApprovalModule(String approvalModule) {
        this.approvalModule = approvalModule;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getUniqueColumn() {
        return uniqueColumn;
    }

    public void setUniqueColumn(String uniqueColumn) {
        this.uniqueColumn = uniqueColumn;
    }

    public ArrayList<ApprovalDataModel> getArrApprovalOptions() {
        return arrApprovalOptions;
    }

    public void setArrApprovalOptions(ArrayList<ApprovalDataModel> arrApprovalOptions) {
        this.arrApprovalOptions = arrApprovalOptions;
    }
}
