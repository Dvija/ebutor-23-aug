package com.ebutor.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BrandProductsModel {

    @SerializedName("product_id")
    public String product_id;
    @SerializedName("model")
    public String model;
    @SerializedName("sku")
    public String sku;
    @SerializedName("upc")
    public String upc;
    @SerializedName("ean")
    public String ean;
    @SerializedName("jan")
    public String jan;
    @SerializedName("isbn")
    public String isbn;
    @SerializedName("mpn")
    public String mpn;
    @SerializedName("location")
    public String location;
    @SerializedName("quantity")
    public String quantity;
    @SerializedName("stock_status_id")
    public String stockStatusId;
    @SerializedName("image")
    public String imagePath;
    @SerializedName("manufacturerId")
    public String manufacturerId;
    @SerializedName("shipping")
    public String shipping;
    @SerializedName("price")
    public String price;
    @SerializedName("mrp")
    public String mrp;
    @SerializedName("unitPrice")
    public String unitPrice;
    @SerializedName("sellerEbutorYourprice")
    public String sellerEbutorYourprice;
    @SerializedName("column18")
    public String column18;
    @SerializedName("points")
    public String points;
    @SerializedName("taxCLassId")
    public String taxCLassId;
    @SerializedName("dateAvailable")
    public String dateAvailable;
    @SerializedName("weight")
    public String weight;
    @SerializedName("weightClassId")
    public String weightClassId;
    @SerializedName("length")
    public String length;
    @SerializedName("width")
    public String width;
    @SerializedName("height")
    public String height;
    @SerializedName("lengthClassId")
    public String lengthClassId;
    @SerializedName("subtract")
    public String subtract;
    @SerializedName("minimum")
    public String minimum;
    @SerializedName("sortOrder")
    public String sortOrder;
    @SerializedName("status")
    public String status;
    @SerializedName("viewed")
    public String viewed;
    @SerializedName("dateAdded")
    public String dateAdded;
    @SerializedName("dateModified")
    public String dateModified;
    @SerializedName("browseNodeId")
    public String browseNodeId;
    @SerializedName("margin")
    public String margin;
    @SerializedName("FIN")
    public String FIN;
    @SerializedName("packQuantity")
    public String packQuantity;
    @SerializedName("languageId")
    public String languageId;
    @SerializedName("name")
    public String name;
    @SerializedName("description")
    public String description;
    @SerializedName("tag")
    public String tag;
    @SerializedName("metaTitle")
    public String metaTitle;
    @SerializedName("metDescriptionmetaKeyword")
    public String metDescriptionmetaKeyword;
    @SerializedName("storeId")
    public String storeId;
    @SerializedName("manufacturer")
    public String manufacturer;
    @SerializedName("special")
    public String special;
    @SerializedName("discount")
    public String discount;
    @SerializedName("reward")
    public String reward;
    @SerializedName("stockStatus")
    public String stockStatus;
    @SerializedName("rating")
    public String rating;
    @SerializedName("reviews")
    public String reviews;


    public class MenuFieldHolder {

        @SerializedName("Status")
        private int status;
        @SerializedName("Message")
        private String message;
        @SerializedName("data")
        private MenuFileData data;

        public MenuFileData getData() {
            return data;
        }

        public void setData(MenuFileData data) {
            this.data = data;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class MenuFileData {
        @SerializedName("products")
        private ArrayList<BrandProductsModel> brandProductsModels;

        public ArrayList<BrandProductsModel> getBrandProductsModels() {
            return brandProductsModels;
        }

        public void setBrandProductsModels(ArrayList<BrandProductsModel> brandProductsModels) {
            this.brandProductsModels = brandProductsModels;
        }
    }

}
