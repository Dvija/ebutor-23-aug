package com.ebutor.models;

import java.util.ArrayList;

public class OrderDetailModel {

    String orderId, orderDate, totalStatus, subTotal, grandTotal, coupon, shippingFirstName, shippingLastName, shippingEmail, shippingTelephone, shippingAddress,
            shippingCity, shippingPin, shippingCountry;
    ArrayList<OrdersModel> arrOrders;
    ArrayList<ShipmentTrackingModel> arrTracking;
    private String orderCode;
    private double discountAmount;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getTotalStatus() {
        return totalStatus;
    }

    public void setTotalStatus(String totalStatus) {
        this.totalStatus = totalStatus;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getShippingFirstName() {
        return shippingFirstName;
    }

    public void setShippingFirstName(String shippingFirstName) {
        this.shippingFirstName = shippingFirstName;
    }

    public String getShippingLastName() {
        return shippingLastName;
    }

    public void setShippingLastName(String shippingLastName) {
        this.shippingLastName = shippingLastName;
    }

    public String getShippingEmail() {
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        this.shippingEmail = shippingEmail;
    }

    public String getShippingTelephone() {
        return shippingTelephone;
    }

    public void setShippingTelephone(String shippingTelephone) {
        this.shippingTelephone = shippingTelephone;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingPin() {
        return shippingPin;
    }

    public void setShippingPin(String shippingPin) {
        this.shippingPin = shippingPin;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public ArrayList<OrdersModel> getArrOrders() {
        return arrOrders;
    }

    public void setArrOrders(ArrayList<OrdersModel> arrOrders) {
        this.arrOrders = arrOrders;
    }

    public ArrayList<ShipmentTrackingModel> getArrTracking() {
        return arrTracking;
    }

    public void setArrTracking(ArrayList<ShipmentTrackingModel> arrTracking) {
        this.arrTracking = arrTracking;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }
}
