package com.ebutor.models;

import java.io.Serializable;

public class SpokeModel implements Serializable {

    private String pincode;
    private String spokeName;
    private String spokeId;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSpokeName() {
        return spokeName;
    }

    public void setSpokeName(String spokeName) {
        this.spokeName = spokeName;
    }

    public String getSpokeId() {
        return spokeId;
    }

    public void setSpokeId(String spokeId) {
        this.spokeId = spokeId;
    }
}
