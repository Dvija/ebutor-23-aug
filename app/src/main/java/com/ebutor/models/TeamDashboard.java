package com.ebutor.models;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 30-Sep-16.
 */

public class TeamDashboard {
    private String name;
    private String userId;
    private ArrayList<DashboardModel> dashboardModelArrayList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<DashboardModel> getDashboardModelArrayList() {
        return dashboardModelArrayList;
    }

    public void setDashboardModelArrayList(ArrayList<DashboardModel> dashboardModelArrayList) {
        this.dashboardModelArrayList = dashboardModelArrayList;
    }
}
