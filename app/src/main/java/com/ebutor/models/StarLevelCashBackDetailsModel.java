package com.ebutor.models;

import java.io.Serializable;

/**
 * Created by 480108 on 8/21/2017.
 */

public class StarLevelCashBackDetailsModel implements Serializable {
    public String getStarColor() {
        return starColor;
    }

    public void setStarColor(String starColor) {
        this.starColor = starColor;
    }

    public double getStarTotalAppliedAmount() {
        return starTotalAppliedAmount;
    }

    public void setStarTotalAppliedAmount(double starTotalAppliedAmount) {
        this.starTotalAppliedAmount = starTotalAppliedAmount;
    }

    public double getStarCashbackAmount() {
        return starCashbackAmount;
    }

    public void setStarCashbackAmount(double starCashbackAmount) {
        this.starCashbackAmount = starCashbackAmount;
    }

    private String starColor;
    private double starTotalAppliedAmount;
    private double starCashbackAmount;
}
