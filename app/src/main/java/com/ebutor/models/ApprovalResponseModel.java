package com.ebutor.models;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 09-Jan-17.
 */

public class ApprovalResponseModel {
    private String currentStatusName;
    private String currentStatusId;
    private ArrayList<ApprovalDataModel> approvalDataModels;

    public String getCurrentStatusName() {
        return currentStatusName;
    }

    public void setCurrentStatusName(String currentStatusName) {
        this.currentStatusName = currentStatusName;
    }

    public String getCurrentStatusId() {
        return currentStatusId;
    }

    public void setCurrentStatusId(String currentStatusId) {
        this.currentStatusId = currentStatusId;
    }

    public ArrayList<ApprovalDataModel> getApprovalDataModels() {
        return approvalDataModels;
    }

    public void setApprovalDataModels(ArrayList<ApprovalDataModel> approvalDataModels) {
        this.approvalDataModels = approvalDataModels;
    }
}
