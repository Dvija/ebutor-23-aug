package com.ebutor.models;

import java.io.Serializable;

public class BeatModel implements Serializable {

    private String address,day,beatId,totalOutlets,rmName,rmId,dcId,hubId,beatName,spokeId,spokeName;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getBeatId() {
        return beatId;
    }

    public void setBeatId(String beatId) {
        this.beatId = beatId;
    }

    public String getTotalOutlets() {
        return totalOutlets;
    }

    public void setTotalOutlets(String totalOutlets) {
        this.totalOutlets = totalOutlets;
    }

    public String getRmName() {
        return rmName;
    }

    public void setRmName(String rmName) {
        this.rmName = rmName;
    }

    public String getRmId() {
        return rmId;
    }

    public void setRmId(String rmId) {
        this.rmId = rmId;
    }

    public String getDcId() {
        return dcId;
    }

    public void setDcId(String dcId) {
        this.dcId = dcId;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getBeatName() {
        return beatName;
    }

    public void setBeatName(String beatName) {
        this.beatName = beatName;
    }

    public String getSpokeId() {
        return spokeId;
    }

    public void setSpokeId(String spokeId) {
        this.spokeId = spokeId;
    }

    public String getSpokeName() {
        return spokeName;
    }

    public void setSpokeName(String spokeName) {
        this.spokeName = spokeName;
    }
}
