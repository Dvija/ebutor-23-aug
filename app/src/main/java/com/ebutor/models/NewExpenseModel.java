package com.ebutor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Srikanth Nama on 29-Dec-16.
 */

public class NewExpenseModel implements Parcelable {

    public static final Creator<NewExpenseModel> CREATOR = new Creator<NewExpenseModel>() {
        @Override
        public NewExpenseModel createFromParcel(Parcel in) {
            return new NewExpenseModel(in);
        }

        @Override
        public NewExpenseModel[] newArray(int size) {
            return new NewExpenseModel[size];
        }
    };
    private String expenseId;
    private String amount;
    private String date;
    private String expenseType;
    private String expenseTypeId;
    private String desc;
    private String attachments;
    private boolean isChecked;

    public NewExpenseModel(String expenseId, String amount, String date, String expenseType, String expenseTypeId, String desc, String attachments) {
        this.expenseId = expenseId;
        this.amount = amount;
        this.date = date;
        this.expenseType = expenseType;
        this.expenseTypeId = expenseTypeId;
        this.desc = desc;
        this.attachments = attachments;
    }

    protected NewExpenseModel(Parcel in) {
        expenseId = in.readString();
        amount = in.readString();
        date = in.readString();
        expenseType = in.readString();
        expenseTypeId = in.readString();
        desc = in.readString();
        attachments = in.readString();
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseTypeId() {
        return expenseTypeId;
    }

    public void setExpenseTypeId(String expenseTypeId) {
        this.expenseTypeId = expenseTypeId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(expenseId);
        parcel.writeString(amount);
        parcel.writeString(date);
        parcel.writeString(expenseType);
        parcel.writeString(expenseTypeId);
        parcel.writeString(desc);
        parcel.writeString(attachments);
    }

    @Override
    public String toString() {
        return this.expenseId;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
