package com.ebutor.models;

/**
 * Created by Srikanth Nama on 17-Jan-17.
 */

public class ApprovalHistoryModel {

    private String awf_history_id;
    private String profile_picture;
    private String firstname;
    private String lastname;
    private String name;
    private String created_at;
    private String status_to_id;
    private String status_from_id;
    private String awf_comment;
    private String master_lookup_name;

    public String getAwf_history_id() {
        return awf_history_id;
    }

    public void setAwf_history_id(String awf_history_id) {
        this.awf_history_id = awf_history_id;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus_to_id() {
        return status_to_id;
    }

    public void setStatus_to_id(String status_to_id) {
        this.status_to_id = status_to_id;
    }

    public String getStatus_from_id() {
        return status_from_id;
    }

    public void setStatus_from_id(String status_from_id) {
        this.status_from_id = status_from_id;
    }

    public String getAwf_comment() {
        return awf_comment;
    }

    public void setAwf_comment(String awf_comment) {
        this.awf_comment = awf_comment;
    }

    public String getMaster_lookup_name() {
        return master_lookup_name;
    }

    public void setMaster_lookup_name(String master_lookup_name) {
        this.master_lookup_name = master_lookup_name;
    }
}
