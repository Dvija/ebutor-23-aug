package com.ebutor.models;

public class ProductSlabData {

    private int level, qty, esu,packQty,packSize;
    private String productId, customerId, star,cashBackIds;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getEsu() {
        return esu;
    }

    public void setEsu(int esu) {
        this.esu = esu;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public int getPackQty() {
        return packQty;
    }

    public void setPackQty(int packQty) {
        this.packQty = packQty;
    }

    public int getPackSize() {
        return packSize;
    }

    public void setPackSize(int packSize) {
        this.packSize = packSize;
    }

    public String getCashBackIds() {
        return cashBackIds;
    }

    public void setCashBackIds(String cashBackIds) {
        this.cashBackIds = cashBackIds;
    }
}
