package com.ebutor.models;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 17-Jan-17.
 */
public class ApprovalHistoryResponseModel {

    private String code;
    private ArrayList<ApprovalHistoryModel> approvalHistoryModelArrayList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<ApprovalHistoryModel> getApprovalHistoryModelArrayList() {
        return approvalHistoryModelArrayList;
    }

    public void setApprovalHistoryModelArrayList(ArrayList<ApprovalHistoryModel> approvalHistoryModelArrayList) {
        this.approvalHistoryModelArrayList = approvalHistoryModelArrayList;
    }
}
