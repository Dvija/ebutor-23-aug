package com.ebutor.models;

import java.io.Serializable;

/**
 * Created by Srikanth Nama on 09-Jan-17.
 */

public class ApprovalDataModel implements Serializable{
    private String conditionId;
    private String condition;
    private String nextStatusId;
    private String nextStatus;
    private String isFinalStep;

    public String getConditionId() {
        return conditionId;
    }

    public void setConditionId(String conditionId) {
        this.conditionId = conditionId;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getNextStatusId() {
        return nextStatusId;
    }

    public void setNextStatusId(String nextStatusId) {
        this.nextStatusId = nextStatusId;
    }

    public String getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(String nextStatus) {
        this.nextStatus = nextStatus;
    }

    public String getIsFinalStep() {
        return isFinalStep;
    }

    public void setIsFinalStep(String isFinalStep) {
        this.isFinalStep = isFinalStep;
    }

    @Override
    public String toString() {
        return condition;
    }
}
