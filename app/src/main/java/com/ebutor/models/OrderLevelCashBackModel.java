package com.ebutor.models;

/**
 * Created by 480108 on 8/9/2017.
 */

public class OrderLevelCashBackModel {

    String cbk_id;
    String cbk_label;
    String cbk_value;
    int cashback_type;
    double qty_from_range;
    double qty_to_range;
    String cashback_description;
    int cashback_id;
    String cbk_source_type;
    int benificiary_type;
    int product_star;

    public String getCbk_id() {
        return cbk_id;
    }

    public void setCbk_id(String cbk_id) {
        this.cbk_id = cbk_id;
    }

    public String getCbk_label() {
        return cbk_label;
    }

    public void setCbk_label(String cbk_label) {
        this.cbk_label = cbk_label;
    }

    public int getCashback_type() {
        return cashback_type;
    }

    public void setCashback_type(int cashback_type) {
        this.cashback_type = cashback_type;
    }

    public String getCbk_value() {
        return cbk_value;
    }

    public void setCbk_value(String cbk_value) {
        this.cbk_value = cbk_value;
    }

    public double getQty_from_range() {
        return qty_from_range;
    }

    public void setQty_from_range(double qty_from_range) {
        this.qty_from_range = qty_from_range;
    }

    public double getQty_to_range() {
        return qty_to_range;
    }

    public void setQty_to_range(double qty_to_range) {
        this.qty_to_range = qty_to_range;
    }

    public String getCashback_description() {
        return cashback_description;
    }

    public void setCashback_description(String cashback_description) {
        this.cashback_description = cashback_description;
    }

    public int getCashback_id() {
        return cashback_id;
    }

    public void setCashback_id(int cashback_id) {
        this.cashback_id = cashback_id;
    }

    public String getCbk_source_type() {
        return cbk_source_type;
    }

    public void setCbk_source_type(String cbk_source_type) {
        this.cbk_source_type = cbk_source_type;
    }

    public int getBenificiary_type() {
        return benificiary_type;
    }

    public void setBenificiary_type(int benificiary_type) {
        this.benificiary_type = benificiary_type;
    }

    public int getProduct_star() {
        return product_star;
    }

    public void setProduct_star(int product_star) {
        this.product_star = product_star;
    }
}
