package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 3/17/2016.
 */
public class SKUModel implements Serializable {

    private String skuId;
    private String productVariantId;
    private String skuImage;
    private ArrayList<String> arrImages;
    private ArrayList<SpecificationsModel> arrSpecs;
    private String sku;
    private String skuName;
    private int position;
    private boolean isDefault;
    private String model;
    private String mrp;
    private String appliedMargin;
    private String appliedMRP;
    private boolean isSelected;
    private String totalQuantity;
    private String totalPrice;
    private ArrayList<SKUPacksModel> skuPacksModelArrayList;
    private String availableQuantity;
    private String productName;
    private String productDescription;

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getProductVariantId() {
        return productVariantId;
    }

    public void setProductVariantId(String productVariantId) {
        this.productVariantId = productVariantId;
    }

    public String getSkuImage() {
        return skuImage;
    }

    public void setSkuImage(String skuImage) {
        this.skuImage = skuImage;
    }

    public ArrayList<String> getArrImages() {
        return arrImages;
    }

    public void setArrImages(ArrayList<String> arrImages) {
        this.arrImages = arrImages;
    }

    public ArrayList<SpecificationsModel> getArrSpecs() {
        return arrSpecs;
    }

    public void setArrSpecs(ArrayList<SpecificationsModel> arrSpecs) {
        this.arrSpecs = arrSpecs;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getAppliedMargin() {
        return appliedMargin;
    }

    public void setAppliedMargin(String appliedMargin) {
        this.appliedMargin = appliedMargin;
    }

    public String getAppliedMRP() {
        return appliedMRP;
    }

    public void setAppliedMRP(String appliedMRP) {
        this.appliedMRP = appliedMRP;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public ArrayList<SKUPacksModel> getSkuPacksModelArrayList() {
        return skuPacksModelArrayList;
    }

    public void setSkuPacksModelArrayList(ArrayList<SKUPacksModel> skuPacksModelArrayList) {
        this.skuPacksModelArrayList = skuPacksModelArrayList;
    }

    public String getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(String availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}
