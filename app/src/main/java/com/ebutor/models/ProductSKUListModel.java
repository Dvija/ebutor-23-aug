package com.ebutor.models;

import java.util.ArrayList;

public class ProductSKUListModel {
    private int totatItemsCount;
    private double totalCartPrice;
    private ArrayList<ProductModel> arrProducts;

    public int getTotatItemsCount() {
        return totatItemsCount;
    }

    public void setTotatItemsCount(int totatItemsCount) {
        this.totatItemsCount = totatItemsCount;
    }

    public double getTotalCartPrice() {
        return totalCartPrice;
    }

    public void setTotalCartPrice(double totalCartPrice) {
        this.totalCartPrice = totalCartPrice;
    }

    public ArrayList<ProductModel> getArrProducts() {
        return arrProducts;
    }

    public void setArrProducts(ArrayList<ProductModel> arrProducts) {
        this.arrProducts = arrProducts;
    }
}
