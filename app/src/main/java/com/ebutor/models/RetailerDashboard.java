package com.ebutor.models;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 20-Sep-16.
 */
public class RetailerDashboard {

    private String totalBillValue;
    private String uniqueOutletsBilled;
    private String averageBillValue;
    private String totalLineCut;
    private String uniqueLineCut;
    private String averageLineCut;

    private ArrayList<DashboardModel> dashboardModelArrayList;

    public String getTotalBillValue() {
        return totalBillValue;
    }

    public void setTotalBillValue(String totalBillValue) {
        this.totalBillValue = totalBillValue;
    }

    public String getUniqueOutletsBilled() {
        return uniqueOutletsBilled;
    }

    public void setUniqueOutletsBilled(String uniqueOutletsBilled) {
        this.uniqueOutletsBilled = uniqueOutletsBilled;
    }

    public String getAverageBillValue() {
        return averageBillValue;
    }

    public void setAverageBillValue(String averageBillValue) {
        this.averageBillValue = averageBillValue;
    }

    public String getTotalLineCut() {
        return totalLineCut;
    }

    public void setTotalLineCut(String totalLineCut) {
        this.totalLineCut = totalLineCut;
    }

    public String getUniqueLineCut() {
        return uniqueLineCut;
    }

    public void setUniqueLineCut(String uniqueLineCut) {
        this.uniqueLineCut = uniqueLineCut;
    }

    public String getAverageLineCut() {
        return averageLineCut;
    }

    public void setAverageLineCut(String averageLineCut) {
        this.averageLineCut = averageLineCut;
    }

    public ArrayList<DashboardModel> getDashboardModelArrayList() {
        return dashboardModelArrayList;
    }

    public void setDashboardModelArrayList(ArrayList<DashboardModel> dashboardModelArrayList) {
        this.dashboardModelArrayList = dashboardModelArrayList;
    }
}
