package com.ebutor.models;

import com.android.internal.util.Predicate;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 12-Aug-16.
 */
public class TestModel implements Predicate<TestModel>{

    private String title;
    private String manufacturerId;
    private String brandId;
    private String categoryId;
    private ArrayList<String> ids;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }


    @Override
    public boolean apply(TestModel testModel) {
        return false;
    }

    public ArrayList<String> getIds() {
        return ids;
    }

    public void setIds(ArrayList<String> ids) {
        this.ids = ids;
    }
}
