package com.ebutor.models;

import java.util.ArrayList;

public class PincodeDataModel {

    private ArrayList<String> arrAreas;
    private String stateId,stateName;

    public ArrayList<String> getArrAreas() {
        return arrAreas;
    }

    public void setArrAreas(ArrayList<String> arrAreas) {
        this.arrAreas = arrAreas;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
