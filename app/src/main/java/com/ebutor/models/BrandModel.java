package com.ebutor.models;

import com.android.internal.util.Predicate;

/**
 * Created by Srikanth Nama on 12-Aug-16.
 */
public class BrandModel extends ManufacturerModel implements Predicate{

    private String brandId;
    private String brandName;
    private String categoryId;

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean apply(Object o) {
        return false;
    }
}
