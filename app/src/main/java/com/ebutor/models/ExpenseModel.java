package com.ebutor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Srikanth Nama on 27-Dec-16.
 */

public class ExpenseModel implements Parcelable {

    public static final Creator<ExpenseModel> CREATOR = new Creator<ExpenseModel>() {
        @Override
        public ExpenseModel createFromParcel(Parcel in) {
            return new ExpenseModel(in);
        }

        @Override
        public ExpenseModel[] newArray(int size) {
            return new ExpenseModel[size];
        }
    };
    private String expenseId;
    private String expenseCode;
    private String requestFor;
    private String expenseSubject;
    private String actualAmount;
    private String approvedAmount;
    private String expenseDate;
    private String currentStatus;
    private String currentStatusId;
    private String submittedByName;
    private String expenseReqType;
    private String expenseReqTypeForId;
    private String tallyLedgerName;
    private String expenseReffId;
    private String submittedById;
    private String approvalStatus;
    private boolean isActive;

    public ExpenseModel() {
    }

    public ExpenseModel(String expenseId, String expenseCode, String requestFor,
                        String expenseSubject, String actualAmount, String approvedAmount,
                        String expenseDate, String currentStatus, String currentStatusId, String submittedByName) {
        this.expenseId = expenseId;
        this.expenseCode = expenseCode;
        this.requestFor = requestFor;
        this.expenseSubject = expenseSubject;
        this.actualAmount = actualAmount;
        this.approvedAmount = approvedAmount;
        this.expenseDate = expenseDate;
        this.currentStatus = currentStatus;
        this.currentStatusId = currentStatusId;
        this.submittedByName = submittedByName;
    }

    protected ExpenseModel(Parcel in) {
        expenseId = in.readString();
        expenseCode = in.readString();
        requestFor = in.readString();
        expenseSubject = in.readString();
        actualAmount = in.readString();
        approvedAmount = in.readString();
        expenseDate = in.readString();
        currentStatus = in.readString();
        currentStatusId = in.readString();
        submittedByName = in.readString();
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCurrentStatusId() {
        return currentStatusId;
    }

    public void setCurrentStatusId(String currentStatusId) {
        this.currentStatusId = currentStatusId;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseCode() {
        return expenseCode;
    }

    public void setExpenseCode(String expenseCode) {
        this.expenseCode = expenseCode;
    }

    public String getRequestFor() {
        return requestFor;
    }

    public void setRequestFor(String requestFor) {
        this.requestFor = requestFor;
    }

    public String getExpenseSubject() {
        return expenseSubject;
    }

    public void setExpenseSubject(String expenseSubject) {
        this.expenseSubject = expenseSubject;
    }

    public String getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(String actualAmount) {
        this.actualAmount = actualAmount;
    }

    public String getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(String approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the set of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(expenseId);
        dest.writeString(expenseCode);
        dest.writeString(requestFor);
        dest.writeString(expenseSubject);
        dest.writeString(actualAmount);
        dest.writeString(approvedAmount);
        dest.writeString(expenseDate);
        dest.writeString(currentStatus);
        dest.writeString(currentStatusId);
        dest.writeString(submittedByName);
    }


    public String getSubmittedByName() {
        return submittedByName;
    }

    public void setSubmittedByName(String submittedByName) {
        this.submittedByName = submittedByName;
    }

    public String getExpenseReqType() {
        return expenseReqType;
    }

    public void setExpenseReqType(String expenseReqType) {
        this.expenseReqType = expenseReqType;
    }

    public String getExpenseReqTypeForId() {
        return expenseReqTypeForId;
    }

    public void setExpenseReqTypeForId(String expenseReqTypeForId) {
        this.expenseReqTypeForId = expenseReqTypeForId;
    }

    public String getTallyLedgerName() {
        return tallyLedgerName;
    }

    public void setTallyLedgerName(String tallyLedgerName) {
        this.tallyLedgerName = tallyLedgerName;
    }

    public String getExpenseReffId() {
        return expenseReffId;
    }

    public void setExpenseReffId(String expenseReffId) {
        this.expenseReffId = expenseReffId;
    }

    public String getSubmittedById() {
        return submittedById;
    }

    public void setSubmittedById(String submittedById) {
        this.submittedById = submittedById;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
