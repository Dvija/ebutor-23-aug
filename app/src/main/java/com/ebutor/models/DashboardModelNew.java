package com.ebutor.models;

import java.util.ArrayList;

public class DashboardModelNew {

    private ArrayList<DashboardModel> arrData;
    private int count;

    public ArrayList<DashboardModel> getArrData() {
        return arrData;
    }

    public void setArrData(ArrayList<DashboardModel> arrData) {
        this.arrData = arrData;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
