package com.ebutor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Srikanth Nama on 27-Dec-16.
 */

public class ExpenseDetailsModel implements Parcelable {

    public static final Creator<ExpenseDetailsModel> CREATOR = new Creator<ExpenseDetailsModel>() {
        @Override
        public ExpenseDetailsModel createFromParcel(Parcel in) {
            return new ExpenseDetailsModel(in);
        }

        @Override
        public ExpenseDetailsModel[] newArray(int size) {
            return new ExpenseDetailsModel[size];
        }
    };
    private String expenseDetailId;
    private String detailActualAmount;
    private String detailApprovedAmount;
    private String detailExpenseType;
    private String detailDescription;
    private String attachedFiles;

    public ExpenseDetailsModel(String expenseDetailId, String detailActualAmount, String detailApprovedAmount,
                               String detailExpenseType, String detailDescription, String attachedFiles) {
        this.expenseDetailId = expenseDetailId;
        this.detailActualAmount = detailActualAmount;
        this.detailApprovedAmount = detailApprovedAmount;
        this.detailExpenseType = detailExpenseType;
        this.detailDescription = detailDescription;
        this.attachedFiles = attachedFiles;
    }

    protected ExpenseDetailsModel(Parcel in) {
        expenseDetailId = in.readString();
        detailActualAmount = in.readString();
        detailApprovedAmount = in.readString();
        detailExpenseType = in.readString();
        detailDescription = in.readString();
        attachedFiles = in.readString();
    }

    public String getExpenseDetailId() {
        return expenseDetailId;
    }

    public void setExpenseDetailId(String expenseDetailId) {
        this.expenseDetailId = expenseDetailId;
    }

    public String getDetailActualAmount() {
        return detailActualAmount;
    }

    public void setDetailActualAmount(String detailActualAmount) {
        this.detailActualAmount = detailActualAmount;
    }

    public String getDetailApprovedAmount() {
        return detailApprovedAmount;
    }

    public void setDetailApprovedAmount(String detailApprovedAmount) {
        this.detailApprovedAmount = detailApprovedAmount;
    }

    public String getDetailExpenseType() {
        return detailExpenseType;
    }

    public void setDetailExpenseType(String detailExpenseType) {
        this.detailExpenseType = detailExpenseType;
    }

    public String getDetailDescription() {
        return detailDescription;
    }

    public void setDetailDescription(String detailDescription) {
        this.detailDescription = detailDescription;
    }

    public String getExpenseId() {
        return expenseDetailId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseDetailId = expenseId;
    }


    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the set of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(expenseDetailId);
        dest.writeString(detailActualAmount);
        dest.writeString(detailApprovedAmount);
        dest.writeString(detailExpenseType);
        dest.writeString(detailDescription);
        dest.writeString(attachedFiles);

    }

    public String getAttachedFiles() {
        return attachedFiles;
    }

    public void setAttachedFiles(String attachedFiles) {
        this.attachedFiles = attachedFiles;
    }


}
