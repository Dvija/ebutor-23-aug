package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 300024 on 7/25/2016.
 */
public class SubCategoryModel implements Serializable{
    private String isSubCategory;
    private ArrayList<CategoryModel> arrSubCategories;

    public String getIsSubCategory() {
        return isSubCategory;
    }

    public void setIsSubCategory(String isSubCategory) {
        this.isSubCategory = isSubCategory;
    }

    public ArrayList<CategoryModel> getArrSubCategories() {
        return arrSubCategories;
    }

    public void setArrSubCategories(ArrayList<CategoryModel> arrSubCategories) {
        this.arrSubCategories = arrSubCategories;
    }
}
