package com.ebutor.models;

import java.io.Serializable;

/**
 * Created by 300024 on 3/15/2016.
 */
public class TabModel implements Serializable {
    String tabId,tabName,tabValue;

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String tabId) {
        this.tabId = tabId;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public String getTabValue() {
        return tabValue;
    }

    public void setTabValue(String tabValue) {
        this.tabValue = tabValue;
    }
}
