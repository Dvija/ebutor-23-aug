package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 29-Jul-16.
 */
public class NewProductModel implements Serializable {
    private String productId;
    private String cartId;
    private String productName;
    private String productRating = "0.0";
    private String productDescription;
    private ArrayList<NewVariantModel> variantModelArrayList;
    ArrayList<ReviewModel> arrReviews;
    ArrayList<ProductsModel> arrRelatedPrds;
    private boolean isClicked = false;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ArrayList<NewVariantModel> getVariantModelArrayList() {
        return variantModelArrayList;
    }

    public void setVariantModelArrayList(ArrayList<NewVariantModel> variantModelArrayList) {
        this.variantModelArrayList = variantModelArrayList;
    }

    public String getProductRating() {
        return productRating;
    }

    public void setProductRating(String productRating) {
        this.productRating = productRating;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public ArrayList<ReviewModel> getArrReviews() {
        return arrReviews;
    }

    public void setArrReviews(ArrayList<ReviewModel> arrReviews) {
        this.arrReviews = arrReviews;
    }

    public ArrayList<ProductsModel> getArrRelatedPrds() {
        return arrRelatedPrds;
    }

    public void setArrRelatedPrds(ArrayList<ProductsModel> arrRelatedPrds) {
        this.arrRelatedPrds = arrRelatedPrds;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setIsClicked(boolean isClicked) {
        this.isClicked = isClicked;
    }
}
