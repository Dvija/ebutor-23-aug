package com.ebutor.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 300024 on 3/17/2016.
 */
public class TopBrandsModel implements Serializable{

    @SerializedName("manufacturer_id")
    private String manufacturerId;
    @SerializedName("name")
    private String topBrandName;
    @SerializedName("image")
    private ArrayList<String> topBrandImages;

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getTopBrandName() {
        return topBrandName;
    }

    public void setTopBrandName(String topBrandName) {
        this.topBrandName = topBrandName;
    }

    public ArrayList<String> getTopBrandImages() {
        return topBrandImages;
    }

    public void setTopBrandImages(ArrayList<String> topBrandImages) {
        this.topBrandImages = topBrandImages;
    }
}
