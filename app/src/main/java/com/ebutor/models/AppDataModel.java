package com.ebutor.models;

import java.io.Serializable;

/**
 * Created by 300024 on 7/26/2016.
 */
public class AppDataModel implements Serializable{
    private String  versionUpdateStatus,versionNumber;

    public String getVersionUpdateStatus() {
        return versionUpdateStatus;
    }

    public void setVersionUpdateStatus(String versionUpdateStatus) {
        this.versionUpdateStatus = versionUpdateStatus;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }
}
