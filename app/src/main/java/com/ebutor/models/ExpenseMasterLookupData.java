package com.ebutor.models;

/**
 * Created by Srikanth Nama on 29-Dec-16.
 */

public class ExpenseMasterLookupData {

    private String masterLookupName;
    private String value;

    public String getMasterLookupName() {
        return masterLookupName;
    }

    public void setMasterLookupName(String masterLookupName) {
        this.masterLookupName = masterLookupName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
