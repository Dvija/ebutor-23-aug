package com.ebutor.models;

import java.io.Serializable;

public class DiscountModel implements Serializable{

    private String discountType, discountOn, discount, discountValues;

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountOn() {
        return discountOn;
    }

    public void setDiscountOn(String discountOn) {
        this.discountOn = discountOn;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountValues() {
        return discountValues;
    }

    public void setDiscountValues(String discountValues) {
        this.discountValues = discountValues;
    }
}
