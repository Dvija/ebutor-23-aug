package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ContainerModel implements Serializable {

    private String containerBarcode, containerType, status, weight, isVerified;
    private ArrayList<TestProductModel> arrProducts;

    public String getContainerBarcode() {
        return containerBarcode;
    }

    public void setContainerBarcode(String containerBarcode) {
        this.containerBarcode = containerBarcode;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public ArrayList<TestProductModel> getArrProducts() {
        return arrProducts;
    }

    public void setArrProducts(ArrayList<TestProductModel> arrProducts) {
        this.arrProducts = arrProducts;
    }
}
