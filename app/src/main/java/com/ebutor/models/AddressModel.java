package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

public class AddressModel implements Serializable {

    public ArrayList<UserAddressModel> arrUserAddr;
    public boolean flag;
    private double eCashBal;

    public ArrayList<UserAddressModel> getArrUserAddr() {
        return arrUserAddr;
    }

    public void setArrUserAddr(ArrayList<UserAddressModel> arrUserAddr) {
        this.arrUserAddr = arrUserAddr;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public double geteCashBal() {
        return eCashBal;
    }

    public void seteCashBal(double eCashBal) {
        this.eCashBal = eCashBal;
    }
}
