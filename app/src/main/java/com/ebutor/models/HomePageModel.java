package com.ebutor.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class HomePageModel implements Serializable{


    @SerializedName("status")
    public String status;
    @SerializedName("message")
    private String message;
    @SerializedName("topBrands")
    private ArrayList<TopBrandsModel> arrtopBrands;
    @SerializedName("recommendedProducts")
    private ArrayList<RecommendedProductModel> arrRecommendedProducts;
    @SerializedName("featuredOffers")
    private ArrayList<FeaturedOfferModel> arrFeaturedOffers;
    @SerializedName("recommendedCategories")
    private ArrayList<RecommendedCategoryModel> arrRecommendedCategories;
    @SerializedName("getManufacturer")
    private ArrayList<TopBrandsModel> arrManufacturers;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TopBrandsModel> getArrtopBrands() {
        return arrtopBrands;
    }

    public void setArrtopBrands(ArrayList<TopBrandsModel> arrtopBrands) {
        this.arrtopBrands = arrtopBrands;
    }

    public ArrayList<RecommendedProductModel> getArrRecommendedProducts() {
        return arrRecommendedProducts;
    }

    public void setArrRecommendedProducts(ArrayList<RecommendedProductModel> arrRecommendedProducts) {
        this.arrRecommendedProducts = arrRecommendedProducts;
    }

    public ArrayList<FeaturedOfferModel> getArrFeaturedOffers() {
        return arrFeaturedOffers;
    }

    public void setArrFeaturedOffers(ArrayList<FeaturedOfferModel> arrFeaturedOffers) {
        this.arrFeaturedOffers = arrFeaturedOffers;
    }

    public ArrayList<RecommendedCategoryModel> getArrRecommendedCategories() {
        return arrRecommendedCategories;
    }

    public void setArrRecommendedCategories(ArrayList<RecommendedCategoryModel> arrRecommendedCategories) {
        this.arrRecommendedCategories = arrRecommendedCategories;
    }

    public ArrayList<TopBrandsModel> getArrManufacturers() {
        return arrManufacturers;
    }

    public void setArrManufacturers(ArrayList<TopBrandsModel> arrManufacturers) {
        this.arrManufacturers = arrManufacturers;
    }
}
