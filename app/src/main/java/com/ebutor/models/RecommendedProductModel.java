package com.ebutor.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 300024 on 3/15/2016.
 */
public class RecommendedProductModel implements Serializable {

    @SerializedName("product_id")
    String productId;
    @SerializedName("category_id")
    String categoryId;
    @SerializedName("name")
    String recommendedProdName;
    @SerializedName("image")
    String recommendedProdImage;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getRecommendedProdName() {
        return recommendedProdName;
    }

    public void setRecommendedProdName(String recommendedProdName) {
        this.recommendedProdName = recommendedProdName;
    }

    public String getRecommendedProdImage() {
        return recommendedProdImage;
    }

    public void setRecommendedProdImage(String recommendedProdImage) {
        this.recommendedProdImage = recommendedProdImage;
    }
}
