package com.ebutor.models;

import android.text.Html;
import android.text.Spanned;

import java.io.Serializable;

/**
 * Created by 300024 on 3/10/2016.
 */
public class BusinessTypeModel implements Serializable{
    public String segmentId;
    public String segmentName;

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    @Override
    public String toString() {
        Spanned string = Html.fromHtml(segmentName);
        return ""+string;
    }
}


