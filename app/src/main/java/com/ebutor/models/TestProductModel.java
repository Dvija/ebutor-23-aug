package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 29-Aug-16.
 */
public class TestProductModel implements Serializable {

    private String productId;
    private String primaryImage;
    private String thumbnailImage;
    private String description;
    private String productTitle;
    private String categoryId;
    private String brandId;
    private String manufacturerId;
    private String mrp;
    private String variantValue1;
    private String variantValue2;
    private String variantValue3;
    private String freebie;
    private String keyValueIndex;
    private String parentId;
    private String metaKeywords;
    private String inventory;
    private int isParent;
    private String primaryVariants;
    private ArrayList<String> variantModelArrayList;
    private String esu;
    private ArrayList<String> arrImages;
    private int totalQty;
    private String appliedMrp;
    private String appliedMargin;
    private ArrayList<SpecificationsModel> arrSpecs;
    private ArrayList<ReviewModel> arrReviews;
    private String packType;
    private boolean isChild;
    private String sKuCode;
    private boolean checked;
    private String selectedVariant;
    private String flag;
    private String pickedQty;
    private String star;
    private String promotionId;
    private boolean isCashback;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPrimaryImage() {
        return primaryImage;
    }

    public void setPrimaryImage(String primaryImage) {
        this.primaryImage = primaryImage;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getVariantValue1() {
        return variantValue1;
    }

    public void setVariantValue1(String variantValue1) {
        this.variantValue1 = variantValue1;
    }

    public String getVariantValue2() {
        return variantValue2;
    }

    public void setVariantValue2(String variantValue2) {
        this.variantValue2 = variantValue2;
    }

    public String getVariantValue3() {
        return variantValue3;
    }

    public void setVariantValue3(String variantValue3) {
        this.variantValue3 = variantValue3;
    }

    public String getKeyValueIndex() {
        return keyValueIndex;
    }

    public void setKeyValueIndex(String keyValueIndex) {
        this.keyValueIndex = keyValueIndex;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public int getIsParent() {
        return isParent;
    }

    public void setIsParent(int isParent) {
        this.isParent = isParent;
    }

    public String getPrimaryVariants() {
        return primaryVariants;
    }

    public void setPrimaryVariants(String primaryVariants) {
        this.primaryVariants = primaryVariants;
    }

    public ArrayList<String> getVariantModelArrayList() {
        return variantModelArrayList;
    }

    public void setVariantModelArrayList(ArrayList<String> variantModelArrayList) {
        this.variantModelArrayList = variantModelArrayList;
    }

    public String getFreebie() {
        return freebie;
    }

    public void setFreebie(String freebie) {
        this.freebie = freebie;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getAppliedMrp() {
        return appliedMrp;
    }

    public void setAppliedMrp(String appliedMrp) {
        this.appliedMrp = appliedMrp;
    }

    public String getAppliedMargin() {
        return appliedMargin;
    }

    public void setAppliedMargin(String appliedMargin) {
        this.appliedMargin = appliedMargin;
    }

    public String getEsu() {
        return esu;
    }

    public void setEsu(String esu) {
        this.esu = esu;
    }

    public ArrayList<String> getArrImages() {
        return arrImages;
    }

    public void setArrImages(ArrayList<String> arrImages) {
        this.arrImages = arrImages;
    }

    public ArrayList<SpecificationsModel> getArrSpecs() {
        return arrSpecs;
    }

    public void setArrSpecs(ArrayList<SpecificationsModel> arrSpecs) {
        this.arrSpecs = arrSpecs;
    }

    public ArrayList<ReviewModel> getArrReviews() {
        return arrReviews;
    }

    public void setArrReviews(ArrayList<ReviewModel> arrReviews) {
        this.arrReviews = arrReviews;
    }

    public String getPackType() {
        return packType;
    }

    public void setPackType(String packType) {
        this.packType = packType;
    }

    public boolean isChild() {
        return isChild;
    }

    public void setIsChild(boolean isChild) {
        this.isChild = isChild;
    }

    public String getSKUCode() {
        return sKuCode;
    }

    public void setSKUCode(String SKUCode) {
        this.sKuCode = SKUCode;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getSelectedVariant() {
        return selectedVariant;
    }

    public void setSelectedVariant(String selectedVariant) {
        this.selectedVariant = selectedVariant;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getPickedQty() {
        return pickedQty;
    }

    public void setPickedQty(String pickedQty) {
        this.pickedQty = pickedQty;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public boolean isCashback() {
        return isCashback;
    }

    public void setCashback(boolean cashback) {
        isCashback = cashback;
    }
}
