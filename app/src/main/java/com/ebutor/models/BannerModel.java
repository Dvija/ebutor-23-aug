package com.ebutor.models;

public class BannerModel {

    private String bannerId,image,title,navigatorObject,navigatorObjectId,frequency;

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNavigatorObject() {
        return navigatorObject;
    }

    public void setNavigatorObject(String navigatorObject) {
        this.navigatorObject = navigatorObject;
    }

    public String getNavigatorObjectId() {
        return navigatorObjectId;
    }

    public void setNavigatorObjectId(String navigatorObjectId) {
        this.navigatorObjectId = navigatorObjectId;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }
}
