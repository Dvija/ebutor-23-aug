package com.ebutor.models;

import java.io.Serializable;

/**
 * Created by 300024 on 7/27/2016.
 */
public class CancelOrderModel implements Serializable{
    private String cancelRequestId,cancelStatus;

    public String getCancelRequestId() {
        return cancelRequestId;
    }

    public void setCancelRequestId(String cancelRequestId) {
        this.cancelRequestId = cancelRequestId;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }
}
