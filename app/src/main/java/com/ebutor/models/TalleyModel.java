package com.ebutor.models;

/**
 * Created by Srikanth Nama on 16-Mar-17.
 */

public class TalleyModel {
    private String tlm_name;
    private String tlm_group;
    private String tlm_id;

    public TalleyModel(String tlm_name, String tlm_group, String tlm_id) {
        this.tlm_name = tlm_name;
        this.tlm_group = tlm_group;
        this.tlm_id = tlm_id;
    }

    public String getTlm_name() {
        return tlm_name;
    }

    public void setTlm_name(String tlm_name) {
        this.tlm_name = tlm_name;
    }

    public String getTlm_group() {
        return tlm_group;
    }

    public void setTlm_group(String tlm_group) {
        this.tlm_group = tlm_group;
    }

    public String getTlm_id() {
        return tlm_id;
    }

    public void setTlm_id(String tlm_id) {
        this.tlm_id = tlm_id;
    }
}
