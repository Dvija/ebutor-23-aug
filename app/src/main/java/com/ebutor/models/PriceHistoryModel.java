package com.ebutor.models;


import java.io.Serializable;
import java.util.ArrayList;

public class PriceHistoryModel implements Serializable {

    private ArrayList<NewPacksModel> arrPrices;
    private String minPrice,maxPrice,avgPrice;

    public ArrayList<NewPacksModel> getArrPrices() {
        return arrPrices;
    }

    public void setArrPrices(ArrayList<NewPacksModel> arrPrices) {
        this.arrPrices = arrPrices;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(String avgPrice) {
        this.avgPrice = avgPrice;
    }
}
