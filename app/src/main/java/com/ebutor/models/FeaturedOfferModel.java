package com.ebutor.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 300024 on 3/17/2016.
 */
public class FeaturedOfferModel implements Serializable {

    @SerializedName("margin")
    String margin;
    @SerializedName("categoryID")
    String categoryId;
    @SerializedName("Categoryname")
    String categoryName;
    @SerializedName("image")
    String categoryImage;

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }
}
