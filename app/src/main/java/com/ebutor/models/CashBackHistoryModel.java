package com.ebutor.models;

import java.io.Serializable;

/**
 * Created by 480108 on 8/16/2017.
 */

public class CashBackHistoryModel implements Serializable {

    String ordercode;
    double cashbackamount;
    String cashbacktype;
    String transactionDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    String orderId;

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public double getCashbackamount() {
        return cashbackamount;
    }

    public void setCashbackamount(double cashbackamount) {
        this.cashbackamount = cashbackamount;
    }

    public String getCashbacktype() {
        return cashbacktype;
    }

    public void setCashbacktype(String cashbacktype) {
        this.cashbacktype = cashbacktype;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
}
