package com.ebutor.models;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 29-Jul-16.
 */
public class NewPacksModel implements Serializable, Comparable<NewPacksModel> {
    private String productId;
    private String packSize;
    private String margin;
    private String unitPrice;
    private String isMarkup;
//    private String productSlabId;
    private String ptr;
    private boolean isSelected;

    private int freebieMpq;
    private int qty;
    private int esuQty;
    //    private int qtyToBeSent;
    private int freeQty, freeQtyTotal;
    private String freebieDesc;
    private String freebieProductId;
    private double freebieQty;
    private String level;
    private String date, price;
    private String stock;
    private String productPackId;
    private String productPriceId;
    private String freebiePackId;
    private String productName;
    private String supplierName;
    private String levelName;
    private String noOfEaches;
    private String cfc;
    private boolean isSlab;
    private int blockedQty;
    private int prmtDetId, slabEsu, packLevel,productSlabId;
    private String star;
    private ArrayList<CashBackDetailsModel> arrCashBackDetails;


  /*  public int getQtyToBeSent() {
        return qtyToBeSent;
    }

    public void setQtyToBeSent(int qtyToBeSent) {
        this.qtyToBeSent = qtyToBeSent;
    }*/

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPackSize() {
        return packSize;
    }

    public void setPackSize(String packSize) {
        this.packSize = packSize;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getIsMarkup() {
        return isMarkup;
    }

    public void setIsMarkup(String isMarkup) {
        this.isMarkup = isMarkup;
    }

    /*public String getProductSlabId() {
        return productSlabId;
    }

    public void setProductSlabId(String productSlabId) {
        this.productSlabId = productSlabId;
    }*/

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public int compareTo(NewPacksModel another) {
        return ((TextUtils.isEmpty(packSize) || packSize == null || packSize.equalsIgnoreCase("null")) ? 0 : Double.parseDouble(packSize)) < ((TextUtils.isEmpty(another.getPackSize()) || another.getPackSize() == null || another.getPackSize().equalsIgnoreCase("null")) ? 0 : Double.parseDouble(another.getPackSize())) ? -1 : 1;
    }

    public int getEsuQty() {
        return esuQty;
    }

    public void setEsuQty(int esuQty) {
        this.esuQty = esuQty;
    }

    public String getFreebieDesc() {
        return freebieDesc;
    }

    public void setFreebieDesc(String freebieDesc) {
        this.freebieDesc = freebieDesc;
    }

    public String getFreebieProductId() {
        return freebieProductId;
    }

    public void setFreebieProductId(String freebieProductId) {
        this.freebieProductId = freebieProductId;
    }

    public double getFreebieQty() {
        return freebieQty;
    }

    public void setFreebieQty(double freebieQty) {
        this.freebieQty = freebieQty;
    }

    public int getFreebieMpq() {
        return freebieMpq;
    }

    public void setFreebieMpq(int freebieMpq) {
        this.freebieMpq = freebieMpq;
    }

    public String getPtr() {
        return ptr;
    }

    public void setPtr(String ptr) {
        this.ptr = ptr;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getProductPackId() {
        return productPackId;
    }

    public void setProductPackId(String productPackId) {
        this.productPackId = productPackId;
    }

    public String getProductPriceId() {
        return productPriceId;
    }

    public void setProductPriceId(String productPriceId) {
        this.productPriceId = productPriceId;
    }

    public String getFreebiePackId() {
        return freebiePackId;
    }

    public void setFreebiePackId(String freebiePackId) {
        this.freebiePackId = freebiePackId;
    }

    public int getFreeQty() {
        return freeQty;
    }

    public void setFreeQty(int freeQty) {
        this.freeQty = freeQty;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public int getFreeQtyTotal() {
        return freeQtyTotal;
    }

    public void setFreeQtyTotal(int freeQtyTotal) {
        this.freeQtyTotal = freeQtyTotal;
    }

    public String getNoOfEaches() {
        return noOfEaches;
    }

    public void setNoOfEaches(String noOfEaches) {
        this.noOfEaches = noOfEaches;
    }

    public String getCfc() {
        return cfc;
    }

    public void setCfc(String cfc) {
        this.cfc = cfc;
    }

    public boolean isSlab() {
        return isSlab;
    }

    public void setSlab(boolean slab) {
        isSlab = slab;
    }

    public int getBlockedQty() {
        return blockedQty;
    }

    public void setBlockedQty(int blockedQty) {
        this.blockedQty = blockedQty;
    }

    public int getPrmtDetId() {
        return prmtDetId;
    }

    public void setPrmtDetId(int prmtDetId) {
        this.prmtDetId = prmtDetId;
    }

    public int getSlabEsu() {
        return slabEsu;
    }

    public void setSlabEsu(int slabEsu) {
        this.slabEsu = slabEsu;
    }

    public int getPackLevel() {
        return packLevel;
    }

    public void setPackLevel(int packLevel) {
        this.packLevel = packLevel;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public int getProductSlabId() {
        return productSlabId;
    }

    public void setProductSlabId(int productSlabId) {
        this.productSlabId = productSlabId;
    }

    public ArrayList<CashBackDetailsModel> getArrCashBackDetails() {
        return arrCashBackDetails;
    }

    public void setArrCashBackDetails(ArrayList<CashBackDetailsModel> arrCashBackDetails) {
        this.arrCashBackDetails = arrCashBackDetails;
    }
}
