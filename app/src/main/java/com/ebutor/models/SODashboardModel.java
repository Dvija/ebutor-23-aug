package com.ebutor.models;

/**
 * Created by Srikanth Nama on 20-Dec-16.
 */

public class SODashboardModel {
    private String count;
    private String status;
    private String total;
    private String percentage;
    private String pickername, pickerId, readyToDispatch, assigned, percentCompleted, contribution;
    private String deliverName, delivered, hold, returned, deliverySuccessRate;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getPickername() {
        return pickername;
    }

    public void setPickername(String pickername) {
        this.pickername = pickername;
    }

    public String getReadyToDispatch() {
        return readyToDispatch;
    }

    public void setReadyToDispatch(String readyToDispatch) {
        this.readyToDispatch = readyToDispatch;
    }

    public String getAssigned() {
        return assigned;
    }

    public void setAssigned(String assigned) {
        this.assigned = assigned;
    }

    public String getPercentCompleted() {
        return percentCompleted;
    }

    public void setPercentCompleted(String percentCompleted) {
        this.percentCompleted = percentCompleted;
    }

    public String getDeliverName() {
        return deliverName;
    }

    public void setDeliverName(String deliverName) {
        this.deliverName = deliverName;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getHold() {
        return hold;
    }

    public void setHold(String hold) {
        this.hold = hold;
    }

    public String getReturned() {
        return returned;
    }

    public void setReturned(String returned) {
        this.returned = returned;
    }

    public String getDeliverySuccessRate() {
        return deliverySuccessRate;
    }

    public void setDeliverySuccessRate(String deliverySuccessRate) {
        this.deliverySuccessRate = deliverySuccessRate;
    }

    public String getPickerId() {
        return pickerId;
    }

    public void setPickerId(String pickerId) {
        this.pickerId = pickerId;
    }

    public String getContribution() {
        return contribution;
    }

    public void setContribution(String contribution) {
        this.contribution = contribution;
    }
}
