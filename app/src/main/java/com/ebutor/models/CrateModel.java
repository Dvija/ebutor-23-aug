package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

public class CrateModel implements Serializable {

    private String gdsOrderId,orderCode,orderDate,whName,hubName,orderStatus,soName,total,shopName,beat,invoiceCode,grandTotal,
            createdAt,createdBy,pickedBy,pickerId,pickedAt;
    private ArrayList<ContainerModel> arrContainers;
    private int bag,cfc,crate;

    public String getGdsOrderId() {
        return gdsOrderId;
    }

    public void setGdsOrderId(String gdsOrderId) {
        this.gdsOrderId = gdsOrderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getSoName() {
        return soName;
    }

    public void setSoName(String soName) {
        this.soName = soName;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getBeat() {
        return beat;
    }

    public void setBeat(String beat) {
        this.beat = beat;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getPickedBy() {
        return pickedBy;
    }

    public void setPickedBy(String pickedBy) {
        this.pickedBy = pickedBy;
    }

    public String getPickerId() {
        return pickerId;
    }

    public void setPickerId(String pickerId) {
        this.pickerId = pickerId;
    }

    public String getPickedAt() {
        return pickedAt;
    }

    public void setPickedAt(String pickedAt) {
        this.pickedAt = pickedAt;
    }

    public ArrayList<ContainerModel> getArrContainers() {
        return arrContainers;
    }

    public void setArrContainers(ArrayList<ContainerModel> arrContainers) {
        this.arrContainers = arrContainers;
    }

    public int getBag() {
        return bag;
    }

    public void setBag(int bag) {
        this.bag = bag;
    }

    public int getCfc() {
        return cfc;
    }

    public void setCfc(int cfc) {
        this.cfc = cfc;
    }

    public int getCrate() {
        return crate;
    }

    public void setCrate(int crate) {
        this.crate = crate;
    }
}
