package com.ebutor.models;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by Srikanth Nama on 3/17/2016.
 */
public class SKUPacksModel implements Serializable, Comparable<SKUPacksModel> {

    private String variantId;
//    private String packQty;
    private String packSize;
    private String margin;
    private String unitPrice;
    private String packPrice;//mrp
//    private String dealerPrice;
    private int qty;
    private String variantPriceId;

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

   /* public String getPackQty() {
        return packQty;
    }

    public void setPackQty(String packQty) {
        this.packQty = packQty;
    }*/

    public String getPackPrice() {
        return packPrice;
    }

    public void setPackPrice(String packPrice) {
        this.packPrice = packPrice;
    }

    public String getPackSize() {
        return packSize;
    }

    public void setPackSize(String packSize) {
        this.packSize = packSize;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getVariantPriceId() {
        return variantPriceId;
    }

    public void setVariantPriceId(String variantPriceId) {
        this.variantPriceId = variantPriceId;
    }

    @Override
    public int compareTo(SKUPacksModel another) {
        return (TextUtils.isEmpty(margin) ? 0 : Double.parseDouble(margin)) < (TextUtils.isEmpty(another.getMargin()) ? 0 :Double.parseDouble(another.getMargin())) ? -1:1;
    }
}
