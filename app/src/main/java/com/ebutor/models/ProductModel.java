package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 3/17/2016.
 */
public class ProductModel implements Serializable {

    ArrayList<ReviewModel> arrReviews;
    ArrayList<ProductsModel> arrRelatedPrds;
    private String productName;
    private String productId;
    private String cartId;
    //    private int position;
    private String productRating;
    private String productDescription;
    private ArrayList<SKUModel> skuModelArrayList;
    private String cartAddedDate;
    private int totalItems;
    private double totalPrice;
    private boolean isClicked = false;

    public boolean isClicked() {
        return isClicked;
    }

    public void setIsClicked(boolean isClicked) {
        this.isClicked = isClicked;
    }

    public String getCartAddedDate() {
        return cartAddedDate;
    }

    public void setCartAddedDate(String cartAddedDate) {
        this.cartAddedDate = cartAddedDate;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public ArrayList<ProductsModel> getArrRelatedPrds() {
        return arrRelatedPrds;
    }

    public void setArrRelatedPrds(ArrayList<ProductsModel> arrRelatedPrds) {
        this.arrRelatedPrds = arrRelatedPrds;
    }

    public ArrayList<ReviewModel> getArrReviews() {
        return arrReviews;
    }

    public void setArrReviews(ArrayList<ReviewModel> arrReviews) {
        this.arrReviews = arrReviews;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

   /* public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }*/

    public ArrayList<SKUModel> getSkuModelArrayList() {
        return skuModelArrayList;
    }

    public void setSkuModelArrayList(ArrayList<SKUModel> skuModelArrayList) {
        this.skuModelArrayList = skuModelArrayList;
    }

    public String getProductRating() {
        return productRating;
    }

    public void setProductRating(String productRating) {
        this.productRating = productRating;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }


}
