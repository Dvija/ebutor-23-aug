package com.ebutor.models;

/**
 * Created by Srikanth Nama on 14-Dec-16.
 */

public class FeatureData {
    private String featureCode;
    private String featureName;
    private String featureParent;
    private String featureIsParent;
    private String featureId;

    public FeatureData(String featureCode, String featureName, String featureParent, String featureIsParent, String featureId) {
        this.featureCode = featureCode;
        this.featureName = featureName;
        this.featureParent = featureParent;
        this.featureIsParent = featureIsParent;
        this.featureId = featureId;
    }

    public String getFeatureCode() {
        return featureCode;
    }

    public String getFeatureName() {
        return featureName;
    }

    public String getFeatureParent() {
        return featureParent;
    }

    public String getFeatureIsParent() {
        return featureIsParent;
    }

    public String getFeatureId() {
        return featureId;
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }
}
