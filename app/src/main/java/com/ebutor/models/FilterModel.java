package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 300024 on 3/29/2016.
 */
public class FilterModel implements Serializable{
    private String filterId,filterName;
    private String manufacturerId;
    private String manufacturerName;
    private String brandId;
    private String brandName;
    private String categoryId,categoryName;
    private ArrayList<String> categoryIds,brandIds,manfIds;
    boolean isChecked,isStar;

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getFilterId() {
        return filterId;
    }

    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(ArrayList<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public ArrayList<String> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(ArrayList<String> brandIds) {
        this.brandIds = brandIds;
    }

    public ArrayList<String> getManfIds() {
        return manfIds;
    }

    public void setManfIds(ArrayList<String> manfIds) {
        this.manfIds = manfIds;
    }

    public boolean isStar() {
        return isStar;
    }

    public void setStar(boolean star) {
        isStar = star;
    }
}
