package com.ebutor.models;

import java.util.ArrayList;

public class CheckCartInventoryModel {

    private double discountAmount;
    private ArrayList<CheckInventoryModel> arrCartItems;
    private ArrayList<OrderLevelCashBackModel> arrOrderLevelCashback;

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public ArrayList<CheckInventoryModel> getArrCartItems() {
        return arrCartItems;
    }

    public void setArrCartItems(ArrayList<CheckInventoryModel> arrCartItems) {
        this.arrCartItems = arrCartItems;
    }

    public ArrayList<OrderLevelCashBackModel> getArrOrderLevelCashback() {
        return arrOrderLevelCashback;
    }

    public void setArrOrderLevelCashback(ArrayList<OrderLevelCashBackModel> arrOrderLevelCashback) {
        this.arrOrderLevelCashback = arrOrderLevelCashback;
    }
}
