package com.ebutor.models;

/**
 * Created by Srikanth Nama on 3/30/2016.
 */
public class CountryModel {

    private String countryId;
    private String countryName;


    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
