package com.ebutor.models;

import java.util.ArrayList;

public class NewProductSKUListModel {
    private int totalItemsCount;
    private double totalCartPrice;
    private ArrayList<NewProductModel> productModelArrayList;

    public int getTotalItemsCount() {
        return totalItemsCount;
    }

    public void setTotalItemsCount(int totalItemsCount) {
        this.totalItemsCount = totalItemsCount;
    }

    public double getTotalCartPrice() {
        return totalCartPrice;
    }

    public void setTotalCartPrice(double totalCartPrice) {
        this.totalCartPrice = totalCartPrice;
    }

    public ArrayList<NewProductModel> getProductModelArrayList() {
        return productModelArrayList;
    }

    public void setProductModelArrayList(ArrayList<NewProductModel> productModelArrayList) {
        this.productModelArrayList = productModelArrayList;
    }
}
