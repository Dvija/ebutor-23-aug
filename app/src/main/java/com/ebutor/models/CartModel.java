package com.ebutor.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 12-Sep-16.
 */
public class CartModel implements Serializable {

    private String customerId;
    private String productId;
    private int quantity;
    private String esu;
    private String unitPrice;
    private String margin;
    private double totalPrice;
    private String warehouseId;//
    private int status;//
    private int availableQty;//
    private String discount;
    private String parentId;
    private String remarks;//
    private String updatedDate;
    private String cartId;//
    private String freebieProductId;
    private String freebieQty;
    private String packType;
    private int freebieMpq;
    private double freebieFq;

    private String productTitle;
    private String productImage;
    private String freebieTitle;

    private String isFreebie;
    private int freeqty;

    private String packSize, freepacksize, productPackId, freebiePackId;
    private int isChild;
    private int isSlab, blockedQty;
    private String star, freebieStar,freebieEsu,packStar;
    private int prmtDetId;
    private int packLevel,productSlabId;
    private String slabEsu;
    private double cashbackAmount;
    private ArrayList<ProductSlabData> arrProductSlabData;
    private ArrayList<ProductSlabData> arrFreebieSlabData;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getEsu() {
        return esu;
    }

    public void setEsu(String esu) {
        this.esu = esu;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(int availableQty) {
        this.availableQty = availableQty;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getFreebieProductId() {
        return freebieProductId;
    }

    public void setFreebieProductId(String freebieProductId) {
        this.freebieProductId = freebieProductId;
    }

    public String getFreebieQty() {
        return freebieQty;
    }

    public void setFreebieQty(String freebieQty) {
        this.freebieQty = freebieQty;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getFreebieTitle() {
        return freebieTitle;
    }

    public void setFreebieTitle(String freebieTitle) {
        this.freebieTitle = freebieTitle;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getIsFreebie() {
        return isFreebie;
    }

    public void setIsFreebie(String isFreebie) {
        this.isFreebie = isFreebie;
    }

    public String getPackSize() {
        return packSize;
    }

    public void setPackSize(String packSize) {
        this.packSize = packSize;
    }

    public String getProductPackId() {
        return productPackId;
    }

    public void setProductPackId(String productPackId) {
        this.productPackId = productPackId;
    }

    public int getIsChild() {
        return isChild;
    }

    public void setIsChild(int isChild) {
        this.isChild = isChild;
    }

    public int getFreeqty() {
        return freeqty;
    }

    public void setFreeqty(int freeqty) {
        this.freeqty = freeqty;
    }

    public String getFreepacksize() {
        return freepacksize;
    }

    public void setFreepacksize(String freepacksize) {
        this.freepacksize = freepacksize;
    }

    public String getPackType() {
        return packType;
    }

    public void setPackType(String packType) {
        this.packType = packType;
    }

    public int isChild() {
        return isChild;
    }

    public String getFreebiePackId() {
        return freebiePackId;
    }

    public void setFreebiePackId(String freebiePackId) {
        this.freebiePackId = freebiePackId;
    }

    public int getIsSlab() {
        return isSlab;
    }

    public void setIsSlab(int isSlab) {
        this.isSlab = isSlab;
    }

    public int getBlockedQty() {
        return blockedQty;
    }

    public void setBlockedQty(int blockedQty) {
        this.blockedQty = blockedQty;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getFreebieStar() {
        return freebieStar;
    }

    public void setFreebieStar(String freebieStar) {
        this.freebieStar = freebieStar;
    }

    public int getPrmtDetId() {
        return prmtDetId;
    }

    public void setPrmtDetId(int prmtDetId) {
        this.prmtDetId = prmtDetId;
    }

    public int getPackLevel() {
        return packLevel;
    }

    public void setPackLevel(int packLevel) {
        this.packLevel = packLevel;
    }

    public String getSlabEsu() {
        return slabEsu;
    }

    public void setSlabEsu(String slabEsu) {
        this.slabEsu = slabEsu;
    }

    public ArrayList<ProductSlabData> getArrProductSlabData() {
        return arrProductSlabData;
    }

    public void setArrProductSlabData(ArrayList<ProductSlabData> arrProductSlabData) {
        this.arrProductSlabData = arrProductSlabData;
    }

    public int getFreebieMpq() {
        return freebieMpq;
    }

    public void setFreebieMpq(int freebieMpq) {
        this.freebieMpq = freebieMpq;
    }

    public double getFreebieFq() {
        return freebieFq;
    }

    public void setFreebieFq(double freebieFq) {
        this.freebieFq = freebieFq;
    }

    public int getProductSlabId() {
        return productSlabId;
    }

    public void setProductSlabId(int productSlabId) {
        this.productSlabId = productSlabId;
    }

    public String getFreebieEsu() {
        return freebieEsu;
    }

    public void setFreebieEsu(String freebieEsu) {
        this.freebieEsu = freebieEsu;
    }

    public ArrayList<ProductSlabData> getArrFreebieSlabData() {
        return arrFreebieSlabData;
    }

    public void setArrFreebieSlabData(ArrayList<ProductSlabData> arrFreebieSlabData) {
        this.arrFreebieSlabData = arrFreebieSlabData;
    }

    public double getCashbackAmount() {
        return cashbackAmount;
    }

    public void setCashbackAmount(double cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }

    public String getPackStar() {
        return packStar;
    }

    public void setPackStar(String packStar) {
        this.packStar = packStar;
    }
}
