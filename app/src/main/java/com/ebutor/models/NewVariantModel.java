package com.ebutor.models;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 29-Jul-16.
 */
public class NewVariantModel {
    private String variantName;
    private String skuId;
    private String productName;
    private String productDescription;
    private int hasInnerVariants;
    private String availableQuantity;
    private String orderedQuantity;
    private String image;
    private String rating;
    private boolean isSelected;
    private boolean isDefault;
    private String appliedMargin;
    private String appliedMRP;
    private String totalQuantity;
    private String totalPrice;
    private ArrayList<NewPacksModel> packsModelArrayList;
    private ArrayList<String> arrImages;
    private ArrayList<SpecificationsModel> arrSpecs;

    private String product_id, primary_image,product_title,category_id,brand_id,manufacturer_id,mrp,variant_value1,
            variant_value2,variant_value3,key_value_index,parent_id,meta_keywords;
    private int is_parent;
    private String primaryVariants;
    private ArrayList<NewVariantModel> variantModelArrayList;

    public String getVariantName() {
        return variantName;
    }

    public void setVariantName(String variantName) {
        this.variantName = variantName;
    }

    public int getHasInnerVariants() {
        return hasInnerVariants;
    }

    public void setHasInnerVariants(int hasInnerVariants) {
        this.hasInnerVariants = hasInnerVariants;
    }

    public ArrayList<NewVariantModel> getVariantModelArrayList() {
        return variantModelArrayList;
    }

    public void setVariantModelArrayList(ArrayList<NewVariantModel> variantModelArrayList) {
        this.variantModelArrayList = variantModelArrayList;
    }

    public ArrayList<NewPacksModel> getPacksModelArrayList() {
        return packsModelArrayList;
    }

    public void setPacksModelArrayList(ArrayList<NewPacksModel> packsModelArrayList) {
        this.packsModelArrayList = packsModelArrayList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(String availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public ArrayList<String> getArrImages() {
        return arrImages;
    }

    public void setArrImages(ArrayList<String> arrImages) {
        this.arrImages = arrImages;
    }

    public ArrayList<SpecificationsModel> getArrSpecs() {
        return arrSpecs;
    }

    public void setArrSpecs(ArrayList<SpecificationsModel> arrSpecs) {
        this.arrSpecs = arrSpecs;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getAppliedMargin() {
        return appliedMargin;
    }

    public void setAppliedMargin(String appliedMargin) {
        this.appliedMargin = appliedMargin;
    }

    public String getAppliedMRP() {
        return appliedMRP;
    }

    public void setAppliedMRP(String appliedMRP) {
        this.appliedMRP = appliedMRP;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(String orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPrimary_image() {
        return primary_image;
    }

    public void setPrimary_image(String primary_image) {
        this.primary_image = primary_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getManufacturer_id() {
        return manufacturer_id;
    }

    public void setManufacturer_id(String manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }

    public String getVariant_value1() {
        return variant_value1;
    }

    public void setVariant_value1(String variant_value1) {
        this.variant_value1 = variant_value1;
    }

    public String getVariant_value2() {
        return variant_value2;
    }

    public void setVariant_value2(String variant_value2) {
        this.variant_value2 = variant_value2;
    }

    public String getVariant_value3() {
        return variant_value3;
    }

    public void setVariant_value3(String variant_value3) {
        this.variant_value3 = variant_value3;
    }

    public String getKey_value_index() {
        return key_value_index;
    }

    public void setKey_value_index(String key_value_index) {
        this.key_value_index = key_value_index;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getMeta_keywords() {
        return meta_keywords;
    }

    public void setMeta_keywords(String meta_keywords) {
        this.meta_keywords = meta_keywords;
    }

    public int getIs_parent() {
        return is_parent;
    }

    public void setIs_parent(int is_parent) {
        this.is_parent = is_parent;
    }

    public String getPrimaryVariants() {
        return primaryVariants;
    }

    public void setPrimaryVariants(String primaryVariants) {
        this.primaryVariants = primaryVariants;
    }
}
