package com.ebutor.models;

import java.util.ArrayList;

public class TabData {

    private ArrayList<TabModel> arrTabs;
    private boolean isEcash;
    private double creditLimit;
    private double ecash;
    private double paymentDue;

    public ArrayList<TabModel> getArrTabs() {
        return arrTabs;
    }

    public void setArrTabs(ArrayList<TabModel> arrTabs) {
        this.arrTabs = arrTabs;
    }

    public boolean isEcash() {
        return isEcash;
    }

    public void setEcash(boolean ecash) {
        isEcash = ecash;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public double getEcash() {
        return ecash;
    }

    public void setEcash(double ecash) {
        this.ecash = ecash;
    }

    public double getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(double paymentDue) {
        this.paymentDue = paymentDue;
    }
}
