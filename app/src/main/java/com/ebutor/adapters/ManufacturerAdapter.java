package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.ebutor.R;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.RetailersModel;

import java.util.ArrayList;
import java.util.Arrays;

public class ManufacturerAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private Context context;
    private OnSelectListener onSelectListener;
    private ArrayList<CustomerTyepModel> arrManf;
    private RetailersModel retailersModel;
    private ArrayList<String> arrIds;

    public ManufacturerAdapter(Context context, ArrayList<CustomerTyepModel> arrManf, RetailersModel retailersModel) {
        this.context = context;
        this.arrManf = arrManf;
        this.retailersModel = retailersModel;
        String ids = retailersModel.getMasterManfIds();
        if (ids != null && ids.length() > 0)
            arrIds = new ArrayList<String>(Arrays.asList(ids.split("\\s*,\\s*")));
        mInflater = LayoutInflater.from(context);
    }

    public ManufacturerAdapter(Context context, ArrayList<CustomerTyepModel> arrManf, String manfIds) {
        this.context = context;
        this.arrManf = arrManf;
        if (manfIds != null && manfIds.length() > 0)
            arrIds = new ArrayList<String>(Arrays.asList(manfIds.split("\\s*,\\s*")));
        mInflater = LayoutInflater.from(context);
    }

    public void setSelectListener(OnSelectListener listener) {
        this.onSelectListener = listener;
    }

    @Override
    public int getCount() {
        return arrManf.size();
    }

    @Override
    public Object getItem(int position) {
        return arrManf.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_master_manf, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.ctvManf.setText(arrManf.get(position).getCustomerName());
        try {
            if (arrIds != null && arrIds.size() > 0) {
                if (arrIds.contains(arrManf.get(position).getCustomerGrpId()) || arrManf.get(position).isChecked())
                    holder.ctvManf.setChecked(true);
                else
                    holder.ctvManf.setChecked(false);
            } else {
                holder.ctvManf.setChecked(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final ViewHolder finalHolder = holder;
        holder.ctvManf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    if (finalHolder.ctvManf.isChecked()) {
                        onSelectListener.onSelected(false);
                    } else {
                        onSelectListener.onSelected(true);
                    }
                } else if (position == arrManf.size() - 1) {
                    if (finalHolder.ctvManf.isChecked()) {
                        arrManf.get(position).setIsChecked(false);
                        onSelectListener.onUnSelected(false, true);
                    } else {
                        onSelectListener.onUnSelected(true, true);
                        arrManf.get(position).setIsChecked(true);
                    }
                } else {
                    onSelectListener.onUnSelected(false, false);
                    arrManf.get(arrManf.size() - 1).setIsChecked(false);
                    arrManf.get(0).setIsChecked(false);
                    if (finalHolder.ctvManf.isChecked()) {
                        finalHolder.ctvManf.setChecked(false);
                        arrManf.get(position).setIsChecked(false);
                    } else {
                        finalHolder.ctvManf.setChecked(true);
                        arrManf.get(position).setIsChecked(true);
                    }
                }
            }
        });

        return convertView;
    }

    public interface OnSelectListener {
        void onSelected(boolean flag);

        void onUnSelected(boolean flag, boolean isUnSelectAll);//Unselect all the fields or particular fields based on flag isUnSelectAll on clicking none
    }

    class ViewHolder {
        CheckedTextView ctvManf;

        ViewHolder(View view) {
            ctvManf = (CheckedTextView) view.findViewById(R.id.ctv_manf);
        }
    }
}
