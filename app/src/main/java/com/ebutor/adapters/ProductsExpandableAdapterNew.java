package com.ebutor.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.customview.SimpleTagImageView;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.interfaces.ProductSKUClickListener;
import com.ebutor.models.TestProductModel;
import com.ebutor.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class ProductsExpandableAdapterNew extends BaseAdapter {

    public int viewType = 1;
    public ArrayList<TestProductModel> mItemsList = null;
    LayoutInflater inflater;
    Context context;
    boolean isShoppingList = false;
    private ProductSKUClickListener productSKUClickListener;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    private String rating, name;
    private ProductImageClickListener productImageClickListener;
    private ActionSlideExpandableListView actionSlideExpandableListView;

    public ProductsExpandableAdapterNew(Context context, ArrayList<TestProductModel> itemsList, ActionSlideExpandableListView actionSlideExpandableListView) {
        this.mItemsList = itemsList;
        this.context = context;
        this.actionSlideExpandableListView = actionSlideExpandableListView;
        inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();

    }

    @Override
    public int getCount() {
        return mItemsList.size();
    }

    public void setProductSKUClickListener(ProductSKUClickListener productSKUClickListener) {
        this.productSKUClickListener = productSKUClickListener;
    }

    public void setProductImageClickListener(ProductImageClickListener productImageClickListener) {
        this.productImageClickListener = productImageClickListener;
    }

    public void removeAllItems() {
        this.mItemsList.clear();
        notifyDataSetChanged();
    }

    public void addItem(TestProductModel model) {
        if (mItemsList != null && !mItemsList.contains(model)) {
            this.mItemsList.add(model);
        }
        notifyDataSetChanged();
    }

    public void removeItem(TestProductModel model) {
        if (mItemsList != null)
            this.mItemsList.remove(model);
        notifyDataSetChanged();
    }

    public void setIsShoppingList(boolean flag) {
        this.isShoppingList = flag;
    }

    public void setData(ArrayList<TestProductModel> productModelArrayList) {
        this.mItemsList = productModelArrayList;
        notifyDataSetInvalidated();
    }

    @Override
    public Object getItem(int position) {
        return mItemsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        int resId = R.layout.row_new_products_expandable;

//        if (convertView == null) {
        switch (viewType) {
            case 1:
                resId = R.layout.row_new_products_expandable;
                break;
            case 2:
                resId = R.layout.row_products_expandable_new;
                break;
        }
        view = inflater.inflate(resId, parent, false);
        holder = new ViewHolder();
        holder.ivOffer = (Button) view.findViewById(R.id.iv_offer);
        holder.tvProductName = (TextView) view.findViewById(R.id.tvProductName);
        holder.tvMrp = (TextView) view.findViewById(R.id.tvProductMRP);
        holder.tvESP = (TextView) view.findViewById(R.id.tvESP);
        holder.toggleButton = (LinearLayout) view.findViewById(R.id.expandable_toggle_button);
        holder.innerVariantsLayout = (LinearLayout) view.findViewById(R.id.inner_variants);
        holder.expandContentLayout = (LinearLayout) view.findViewById(R.id.ll_expand_content);
        holder.skuItemsLayout = (LinearLayout) view.findViewById(R.id.skuItemsLayout);
        holder.draweeView = (SimpleTagImageView) view.findViewById(R.id.productImage);
        holder.tvInventory = (TextView) view.findViewById(R.id.tvInventory);
        holder.ivWishlist = (ImageView) view.findViewById(R.id.ivWishlist);
        holder.ivCashback = (ImageView) view.findViewById(R.id.iv_cashback);
        view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }

        final TestProductModel productModel = mItemsList.get(position);
        setOfferImageBackground(productModel.getPackType(), holder);
//        if(position%2 == 0)
//            setOfferImageBackground(productModel.getPackType(),holder);
//        else
//            setOfferImageBackground("CPOFFER", holder);

        holder.tvProductName.setText(productModel.getProductTitle().trim());
        ImageLoader.getInstance().displayImage(productModel.getThumbnailImage(), holder.draweeView, options, animateFirstListener);
        holder.draweeView.setTagBackgroundColor(Color.TRANSPARENT);
        holder.draweeView.setTagText("");
        double mrp = 0;
        try {
            mrp = Double.parseDouble(productModel.getMrp());
        } catch (Exception e) {
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(productModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
        setMRP(sb, holder.tvMrp);
        if (isShoppingList) {
            holder.ivWishlist.setImageResource(R.drawable.ic_delete);
            holder.ivWishlist.setVisibility(View.VISIBLE);
        } else {
            holder.ivWishlist.setVisibility(View.GONE);
//            holder.tvRating.setVisibility(View.GONE);
//            holder.ivWishlist.setImageResource(R.drawable.ic_wishlist_gray);
        }
        if (productModel.isCashback()) {
            holder.ivCashback.setVisibility(View.VISIBLE);
        } else {
            holder.ivCashback.setVisibility(View.GONE);
        }
//        rating = productModel.getProductRating();
        rating = "0.0"; //temp
        if (rating == null || TextUtils.isEmpty(rating)) {
            rating = "0.0";
        }
        if (rating != null && !TextUtils.isEmpty(rating) && !rating.equalsIgnoreCase("null")) {
            DecimalFormat format = new DecimalFormat("0.0");
            double newmargin = Double.parseDouble(rating);
            rating = format.format(newmargin);
        }

//        holder.tvInventory.setText(rating.equalsIgnoreCase("null") ? "0.0" : rating);

        holder.tvInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productSKUClickListener != null) {
                    productSKUClickListener.OnItemOptionClicked(productModel, 3);
                }
            }
        });
        holder.ivWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productSKUClickListener != null) {
                    productSKUClickListener.OnItemOptionClicked(productModel, 1);
                }
            }
        });

        holder.draweeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productImageClickListener.OnImageClicked(position, rating);
            }
        });

        holder.tvProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productImageClickListener.OnImageClicked(position, rating);
            }
        });

//        holder.setTag();

        holder.skuItemsLayout.removeAllViews();
        if (productModel != null && productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
            ArrayList<String> arrTemp = new ArrayList<>();
            for (int j = 0; j < productModel.getVariantModelArrayList().size(); j++) {
//            final NewVariantModel skuModel = productModel.getVariantModelArrayList().get(j);
                DisplayMetrics displaymetrics = new DisplayMetrics();
                WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                windowManager.getDefaultDisplay().getMetrics(displaymetrics);
                int height = displaymetrics.heightPixels;

                final LinearLayout skuView = new LinearLayout(context);
                skuView.setOrientation(LinearLayout.HORIZONTAL);
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                params1.setMargins(0, 0, 20, 0);
                skuView.setLayoutParams(params1);

                ArrayList<String> variantsList = productModel.getVariantModelArrayList();
                name = "";
                String color = "#FFFFFF";
                if (variantsList.get(j).split(":").length > 0) {
                    name = variantsList.get(j).split(":")[0];
                }

                if (variantsList.get(j).split(":").length > 1) {
                    color = variantsList.get(j).split(":")[1];
                }

                if (!arrTemp.contains(name)) {
                    arrTemp.add(name);
                    ImageView imageView = new ImageView(context);
                    Drawable sourceDrawable = context.getResources().getDrawable(R.drawable.ic_star);

                    Bitmap sourceBitmap = Utils.convertDrawableToBitmap(sourceDrawable);
                    Bitmap mFinalBitmap = Utils.changeImageColor(sourceBitmap, Color.parseColor(color));
                    imageView.setImageBitmap(mFinalBitmap);
                    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                    params2.gravity = Gravity.CENTER_VERTICAL;
                    params2.setMargins(0, 0, 0, 0);
                    imageView.setLayoutParams(params2);

                /*ImageView imageView1 = new ImageView(context);
                Drawable sourceDrawable1 = context.getResources().getDrawable(R.drawable.ic_star);

                Bitmap sourceBitmap1 = Utils.convertDrawableToBitmap(sourceDrawable1);
                Bitmap mFinalBitmap1 = Utils.changeImageColor(sourceBitmap1, context.getResources().getColor(R.color.text_color_red));
                imageView1.setImageBitmap(mFinalBitmap1);
                RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, height / 25);
                params3.setMargins(5, 0, 0, 0);
                imageView1.setLayoutParams(params3);

                ImageView imageView2 = new ImageView(context);
                Drawable sourceDrawable2 = context.getResources().getDrawable(R.drawable.ic_star);

                Bitmap sourceBitmap2 = Utils.convertDrawableToBitmap(sourceDrawable2);
                Bitmap mFinalBitmap2 = Utils.changeImageColor(sourceBitmap2, context.getResources().getColor(R.color.primary));
                imageView2.setImageBitmap(mFinalBitmap2);
                RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, height / 25);
                params4.setMargins(10, 0, 0, 0);
                imageView2.setLayoutParams(params4);

                FrameLayout relativeLayout = new FrameLayout(context);
                FrameLayout.LayoutParams params5 = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params5.gravity = Gravity.CENTER_VERTICAL;
                relativeLayout.setLayoutParams(params5);
                relativeLayout.addView(imageView);
                relativeLayout.addView(imageView1);
                relativeLayout.addView(imageView2);*/

                    TextView textView = new TextView(context);
                    textView.setTextSize(12);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.gravity = Gravity.CENTER_VERTICAL;
                    params.setMargins(5, 0, 0, 0);
                    textView.setTextColor(ContextCompat.getColor(context, R.color.sku_button_color_selector));
                    textView.setGravity(Gravity.CENTER);
                    textView.setLayoutParams(params);
                    skuView.setId(R.id.id_one);
                    skuView.setTag(name);
                    textView.setText(name == null ? "" : name);
                    if (productModel.isChild() && j == 0) {
                        skuView.setSelected(true);
                        productModel.setSelectedVariant(name);
                    } else {
                        if (productModel.getVariantValue1().equalsIgnoreCase(name)) {
                            skuView.setSelected(true);
                            productModel.setSelectedVariant(name);
//                skuModel.setSelected(true);
//                mrp = 0;
//                try {
//                    mrp = Double.parseDouble(skuModel.getMrp());
//                } catch (Exception e) {
//                }
//                final SpannableStringBuilder sb = new SpannableStringBuilder(skuModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
//            final SpannableStringBuilder sb = new SpannableStringBuilder("MRP : " + String.format(Locale.CANADA, "%.2f", mrp)); // todo
//                setMRP(sb, holder);
                        } else {
                            skuView.setSelected(false);
                        }
                    }
//
//
//            String availableQty = skuModel.getAvailableQuantity();
////            String availableQty = "100"; // todo
//            if (availableQty != null && !TextUtils.isEmpty(availableQty)) {
//                int qty = 0;
//                try {
//                    qty = Integer.parseInt(availableQty);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (qty > 0) {
//                    holder.draweeView.setTagBackgroundColor(Color.TRANSPARENT);
//                    holder.draweeView.setTagText("");
//                } else {
//                    holder.draweeView.setTagBackgroundColor(Color.parseColor("#afd3d3d3"));
//                    holder.draweeView.setTagText("Out of Stock");
//                }
//
//            }
////            }
                    //
//            final int finalJ = j;
                    skuView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int count = holder.skuItemsLayout.getChildCount();
                            for (int i = 0; i < count; i++) {
                                View btnView = holder.skuItemsLayout.getChildAt(i);
                                btnView.setSelected(false);
                            }
                            v.setSelected(true);
                            TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(productModel.getProductId(), name, "", "");
                            if (testProductModel != null) {
                                productModel.setSelectedVariant((String) skuView.getTag());
                                setOfferImageBackground(testProductModel.getPackType(), holder);
                                double mrp = 0;
                                try {
                                    mrp = Double.parseDouble(testProductModel.getMrp());
                                } catch (Exception e) {
                                }
                                final SpannableStringBuilder sb = new SpannableStringBuilder(productModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
                                setMRP(sb, holder.tvMrp);
                                ImageLoader.getInstance().displayImage(testProductModel.getThumbnailImage(), holder.draweeView, options, animateFirstListener);
                                holder.tvProductName.setText(testProductModel.getProductTitle().trim());
                                if (testProductModel.isCashback()) {
                                    holder.ivCashback.setVisibility(View.VISIBLE);
                                } else {
                                    holder.ivCashback.setVisibility(View.GONE);
                                }
                            }
                            if (productSKUClickListener != null) {
                                productSKUClickListener.onSKUClicked(productModel.getProductId());
                                productSKUClickListener.OnProductSKUClicked(skuView.getTag(), getItemId(position), productModel.isChild());
                            }
                        }
                    });
                    skuView.setBackground(ContextCompat.getDrawable(context, R.drawable.sku_button_selector));
                    skuView.setPadding(10, 5, 10, 5);
                /*skuView.addView(imageView);
                skuView.addView(imageView1);
                skuView.addView(imageView2);*/
                    skuView.addView(imageView);
                    skuView.addView(textView);
                    holder.skuItemsLayout.addView(skuView);
                }
            }
        }

        view.setTag(holder);
        return view;
    }

    public void setOfferImageBackground(String packType, ViewHolder holder) {
        switch (packType) {
            case "Regular":
            case "REGULAR":
                holder.ivOffer.setVisibility(View.GONE);
                break;
            case "Consumer Pack":
            case "CONSUMER PACK":
            case "CP Inside":
            case "CP Outside":
            case "Consumer Pack Inside":
            case "Consumer Pack Outside":
                holder.ivOffer.setVisibility(View.VISIBLE);
                holder.ivOffer.setBackgroundResource(R.drawable.ic_offer_green);
                holder.ivOffer.setText("CP Offer");
                break;
            case "Freebie":
            case "FREEBIE":
                holder.ivOffer.setVisibility(View.VISIBLE);
                holder.ivOffer.setBackgroundResource(R.drawable.ic_offer_red);
                holder.ivOffer.setText("Freebie");
                break;
            default:
                holder.ivOffer.setVisibility(View.GONE);
                break;
        }
    }

    public void notifyChangedItem(int pos, TestProductModel testProductModel) {
        mItemsList.set(pos, testProductModel);
        notifyDataSetChanged();
    }

    private void setMRP(SpannableStringBuilder sb, TextView textView) {
        /*// Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));*/
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        /*// Set the text color for first 4 characters
        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 0, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);*/
        // Create the Typeface you want to apply to certain text
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, 3, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(sb);
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return viewType;
    }

    public void changeViewType(int type) {
        viewType = type;
        notifyDataSetChanged();
    }

    public interface ProductImageClickListener {
        void OnImageClicked(int position, String rating);
    }

    private class ViewHolder {
        public TextView tvProductName, tvInventory;
        public LinearLayout toggleButton, expandContentLayout, skuItemsLayout;
        private TextView tvMrp, tvESP;
        private ImageView ivWishlist, ivCashback;
        private Button ivOffer;
        private SimpleTagImageView draweeView;
        private LinearLayout innerVariantsLayout;
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}