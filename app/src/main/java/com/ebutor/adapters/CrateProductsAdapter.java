package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.TestProductModel;

import java.util.ArrayList;

public class CrateProductsAdapter extends RecyclerView.Adapter<CrateProductsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<TestProductModel> arrProducts;

    public CrateProductsAdapter(Context context, ArrayList<TestProductModel> arrProducts) {
        this.context = context;
        this.arrProducts = arrProducts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_crate_product, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TestProductModel testProductModel = arrProducts.get(position);
        holder.tvproduct.setText(testProductModel.getProductTitle());
        holder.tvQty.setText(testProductModel.getPickedQty());
    }

    @Override
    public int getItemCount() {
        if (arrProducts != null && arrProducts.size() > 0)
            return arrProducts.size();
        else
            return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvproduct, tvQty;

        public MyViewHolder(View view) {
            super(view);
            tvproduct = (TextView) view.findViewById(R.id.tv_product);
            tvQty = (TextView) view.findViewById(R.id.tv_qty);
        }
    }
}
