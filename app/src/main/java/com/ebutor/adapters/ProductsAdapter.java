package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ebutor.R;
import com.ebutor.customview.SimpleTagImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class ProductsAdapter extends PagerAdapter {

    //    private NewVariantModel skuModel;
    OnImageClickListner onImageClickListner;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private Context context;
    private ArrayList<String> arrProductImgs;

    public ProductsAdapter(Context context, ArrayList<String> arrProductImgs/*, NewVariantModel skuModel*/) {
        this.context = context;
        this.arrProductImgs = arrProductImgs;
//        this.skuModel = skuModel;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();
    }

    public void setOnImageClickListener(OnImageClickListner listener) {
        this.onImageClickListner = listener;
    }

    @Override
    public int getCount() {
        if (arrProductImgs != null && arrProductImgs.size() > 0) {
            return arrProductImgs.size();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product, null);
        SimpleTagImageView ivProduct = (SimpleTagImageView) view.findViewById(R.id.ivProduct);
//        String availableQty = skuModel.getAvailableQuantity();
//        if(availableQty!=null && !TextUtils.isEmpty(availableQty)){
//            int qty = 0;
//            try {
//                qty = Integer.parseInt(availableQty);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//            if(qty>0){
        ivProduct.setTagBackgroundColor(Color.TRANSPARENT);
        ivProduct.setTagText("");
//            }else{
//                ivProduct.setTagBackgroundColor(Color.parseColor("#afd3d3d3"));
//                ivProduct.setTagText("Out of Stock");
//            }

//        }
        ImageLoader.getInstance().displayImage(arrProductImgs.get(position), ivProduct, options, animateFirstListener);

        ivProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageClickListner.onImageClicked(arrProductImgs, position);
            }
        });
        ((ViewPager) container).addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    public interface OnImageClickListner {
        public void onImageClicked(ArrayList<String> arrImages, int position);
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
