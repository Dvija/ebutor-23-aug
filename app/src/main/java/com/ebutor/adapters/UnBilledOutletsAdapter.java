package com.ebutor.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.RetailersModel;
import com.ebutor.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class UnBilledOutletsAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private LayoutInflater inflater;
    private OnCheckInListener listener;
    private ArrayList<RetailersModel> arrOutlets;
    private ModelFilter filter;
    private List<RetailersModel> filteredModelItemsArray;
    private SharedPreferences mSharedPreferences;

    public UnBilledOutletsAdapter(Context context, ArrayList<RetailersModel> arrOutlets) {
        this.context = context;
        this.arrOutlets = arrOutlets;
        mSharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        this.filteredModelItemsArray = new ArrayList<RetailersModel>();
        filteredModelItemsArray.addAll(arrOutlets);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModelFilter();
        }
        return filter;
    }

    public void setCheckinListener(OnCheckInListener listener) {
        this.listener = listener;
    }

    public void addItem(RetailersModel model) {
        if (filteredModelItemsArray != null && !filteredModelItemsArray.contains(model)) {
            this.filteredModelItemsArray.add(model);
        }
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        this.filteredModelItemsArray.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return filteredModelItemsArray.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredModelItemsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_unbilled_outlets, parent, false);
            holder = new ViewHolder();

            holder.expandContentLayout = (LinearLayout) convertView.findViewById(R.id.ll_expand_content);
            holder.toggleButton = (LinearLayout) convertView.findViewById(R.id.expandable_toggle_button);
            holder.btnNavigation = (Button) convertView.findViewById(R.id.btn_navigation);
            holder.btnCheckin = (Button) convertView.findViewById(R.id.btn_check_in);
            holder.btnTotalOrders = (Button) convertView.findViewById(R.id.btn_total_orders);
            holder.btnFeedback = (Button) convertView.findViewById(R.id.btn_feed_back);
            holder.tvStoreName = (TextView) convertView.findViewById(R.id.tv_store_name);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
            holder.tvMobile = (TextView) convertView.findViewById(R.id.tv_mobile);
            holder.tvLastVisit = (TextView) convertView.findViewById(R.id.tv_last_visit);
            holder.tvTotalBusiness = (TextView) convertView.findViewById(R.id.tv_total_business);
            holder.tvAvgBillValue = (TextView) convertView.findViewById(R.id.tv_avg_bill_value);
            holder.ivRank = (ImageView) convertView.findViewById(R.id.iv_rank);
            holder.ivRank.setVisibility(View.VISIBLE);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final RetailersModel retailersModel = filteredModelItemsArray.get(position);

        holder.tvStoreName.setText(retailersModel.getCompany());
        holder.tvAddress.setText(retailersModel.getAddress1());
        holder.tvMobile.setText(retailersModel.getTelephone());
        final SpannableStringBuilder sb = new SpannableStringBuilder(context.getString(R.string.total_business) + " : " + retailersModel.getTotalBusiness());
        setSpannable(sb, holder.tvTotalBusiness, 0, sb.length());
        final SpannableStringBuilder sb1 = new SpannableStringBuilder(context.getString(R.string.avg_bill_value) + " : " + retailersModel.getAvgBillValue());
        setSpannable(sb1, holder.tvAvgBillValue, 0, sb1.length());

        double rank = Double.parseDouble(retailersModel.getRank());

        if (rank >= 75){
            // Platinum
           holder.ivRank.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_platinum24));
          //  holder.ivRank.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_platinum24));

        }else if (rank >= 50 && rank < 75){
            // Gold
            holder.ivRank.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_gold24));
        }else{
            // Silver
            holder.ivRank.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_silver24));
        }
        //holder.tvRank.setText(context.getString(R.string.rank) + " : " + retailersModel.getRank());
        holder.btnTotalOrders.setText(context.getString(R.string.total_orders) + "\n" + retailersModel.getReturnOrders() + "/" + retailersModel.getNoOfOrders());
        String lastVisit = "";
        try {
            if (!TextUtils.isEmpty(retailersModel.getLastVisit()))
                lastVisit = Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN5, retailersModel.getLastVisit());
        } catch (Exception e) {
            lastVisit = "";
        }
        holder.tvLastVisit.setText(lastVisit);
//        holder.btnCheckin.setTag(retailersModel);
        holder.btnCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCheckIn(position, retailersModel);
            }
        });
        holder.btnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNavigate(position, retailersModel);
            }
        });

        holder.btnTotalOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onOrdersClick(position, retailersModel);
            }
        });
        holder.btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFeedback(position, retailersModel);
            }
        });

        return convertView;
    }

    private void setSpannable(SpannableStringBuilder sb, TextView textView, int start, int end) {
        /*// Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));*/
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        /*// Set the text color for first 4 characters
        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 0, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);*/
        // Create the Typeface you want to apply to certain text
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(sb);
    }

    public interface OnCheckInListener {
        void onCheckIn(int pos, Object object);

        void onNavigate(int pos, Object object);

        void onOrdersClick(int pos, Object object);

        void onFeedback(int pos, Object object);
    }

    private class ViewHolder {
        public LinearLayout expandContentLayout, toggleButton;
        Button btnNavigation, btnCheckin, btnTotalOrders, btnFeedback;
        TextView tvStoreName, tvAddress, tvMobile, tvLastVisit, tvTotalBusiness, tvAvgBillValue ;
        ImageView ivRank;
    }

    private class ModelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (!TextUtils.isEmpty(constraint)) {
                if (Character.isDigit(constraint.charAt(0))) {
                    if (constraint != null && constraint.toString().length() > 0) {
                        constraint = constraint.toString();
                        ArrayList<RetailersModel> filteredItems = new ArrayList<RetailersModel>();
                        for (int i = 0, l = arrOutlets.size(); i < l; i++) {
                            RetailersModel m = arrOutlets.get(i);
                            if (m.getTelephone().contains(constraint))
                                filteredItems.add(m);
                        }
                        result.count = filteredItems.size();
                        result.values = filteredItems;
                    } else {
                        result.values = arrOutlets;
                        result.count = arrOutlets.size();

                    }
                } else {
                    if (constraint != null && constraint.toString().length() > 0) {
                        constraint = constraint.toString().toLowerCase();
                        ArrayList<RetailersModel> filteredItems = new ArrayList<RetailersModel>();

                        for (int i = 0, l = arrOutlets.size(); i < l; i++) {
                            RetailersModel m = arrOutlets.get(i);
                            if (m.getCompany().toLowerCase().contains(constraint))
                                filteredItems.add(m);
                        }
                        result.count = filteredItems.size();
                        result.values = filteredItems;
                    } else {
                        result.values = arrOutlets;
                        result.count = arrOutlets.size();

                    }
                }
            } else {
                result.values = arrOutlets;
                result.count = arrOutlets.size();
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredModelItemsArray.clear();
            if (filteredModelItemsArray != null && results != null && results.count > 0) {
                filteredModelItemsArray.addAll((ArrayList<RetailersModel>) results.values);
            }
            notifyDataSetChanged();
        }
    }
}
