package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.ProductFilterModel;

import java.util.ArrayList;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ProductFilterModel> arrProductFilters;
    private OnItemClickListener listener;
    private int count = 0, count1 = 0;
    private ViewGroup viewGroup;

    public FilterAdapter(Context context, ArrayList<ProductFilterModel> arrProductFilters) {
        this.context = context;
        this.arrProductFilters = arrProductFilters;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        viewGroup = parent;
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_filter_name_new, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvFilter.setText(arrProductFilters.get(position).getGroupName());
        if (position == 0) {
            holder.tvFilter.setBackgroundColor(context.getResources().getColor(R.color.btn_active));
            holder.tvFilter.setTextColor(context.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return arrProductFilters.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvFilter;

        public MyViewHolder(View view) {
            super(view);
            tvFilter = (TextView) view.findViewById(R.id.tv_group_name);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (viewGroup != null) {
                for (int i = 0; i < ((ViewGroup) viewGroup).getChildCount(); i++) {
                    View view = ((ViewGroup) viewGroup).getChildAt(i);
                    view.setBackgroundColor(context.getResources().getColor(R.color.btn_inactive));
                    ((TextView) view).setTextColor(context.getResources().getColor(R.color.text_color));
                }
                v.setBackgroundColor(context.getResources().getColor(R.color.btn_active));
                ((TextView) v).setTextColor(context.getResources().getColor(R.color.white));
                listener.onItemClick(getPosition());
            }
        }
    }
}
