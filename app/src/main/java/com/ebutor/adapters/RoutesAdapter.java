package com.ebutor.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.database.DBHelper;
import com.ebutor.models.BeatModel;
import com.ebutor.utils.FeatureCodes;

import java.util.ArrayList;
import java.util.List;

public class RoutesAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<BeatModel> arrBeats;
    private OnBeatClickListener listener;
    private ModelFilter filter;
    private List<BeatModel> filteredModelItemsArray;

    public RoutesAdapter(Context context, ArrayList<BeatModel> arrBeats) {
        this.context = context;
        this.arrBeats = arrBeats;
        this.filteredModelItemsArray = new ArrayList<BeatModel>();
        filteredModelItemsArray.addAll(arrBeats);
        inflater = LayoutInflater.from(context);
        getFilter();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModelFilter();
        }
        return filter;
    }

    public void setBeatClickListener(OnBeatClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return filteredModelItemsArray.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredModelItemsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_routes, parent, false);
            holder = new ViewHolder();

            holder.expandContentLayout = (LinearLayout) convertView.findViewById(R.id.ll_expand_content);
            holder.toggleButton = (LinearLayout) convertView.findViewById(R.id.expandable_toggle_button);
            holder.btnNavigation = (Button) convertView.findViewById(R.id.btn_navigation);
            holder.btnOutlets = (Button) convertView.findViewById(R.id.btn_outlets);
            holder.btnEdit = (Button) convertView.findViewById(R.id.btn_edit);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
            holder.tvRM = (TextView) convertView.findViewById(R.id.tv_rm);
            holder.tvDay = (TextView) convertView.findViewById(R.id.tv_day);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final BeatModel beatModel = filteredModelItemsArray.get(position);

        holder.tvAddress.setText(beatModel.getAddress());
        holder.tvRM.setText(beatModel.getRmName());
        holder.tvDay.setText(beatModel.getDay());
        holder.btnOutlets.setText(context.getResources().getString(R.string.outlets) + "(" + beatModel.getTotalOutlets() + ")");

        if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.MANAGE_BEATS_FEATURE_CODE) > 0) {
            holder.btnEdit.setVisibility(View.VISIBLE);
            if (beatModel.getBeatId().equalsIgnoreCase("0") || beatModel.getBeatId().equalsIgnoreCase("-1")) {
                holder.btnEdit.setEnabled(false);
            } else {
                holder.btnEdit.setEnabled(true);
            }
        } else {
            holder.btnEdit.setVisibility(View.GONE);
        }

        holder.btnOutlets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBeatClick(beatModel);
            }
        });

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBeatEdit(beatModel);
            }
        });

        holder.btnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMapCLick(beatModel);
            }
        });

        return convertView;
    }

    public interface OnBeatClickListener {
        void onBeatClick(Object object);

        void onBeatEdit(Object object);

        void onMapCLick(Object object);
    }

    private class ViewHolder {
        public LinearLayout expandContentLayout, toggleButton;
        Button btnOutlets, btnNavigation, btnEdit;
        TextView tvAddress, tvRM, tvDay;
    }

    private class ModelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (!TextUtils.isEmpty(constraint)) {
                if (constraint != null && constraint.toString().length() > 0) {
                    constraint = constraint.toString().toLowerCase();
                    ArrayList<BeatModel> filteredItems = new ArrayList<BeatModel>();

                    for (int i = 0, l = arrBeats.size(); i < l; i++) {
                        BeatModel m = arrBeats.get(i);
                        if (m.getAddress().toLowerCase().contains(constraint))
                            filteredItems.add(m);
                    }
                    result.count = filteredItems.size();
                    result.values = filteredItems;
                } else {
                    result.values = arrBeats;
                    result.count = arrBeats.size();

                }
            } else {
                result.values = arrBeats;
                result.count = arrBeats.size();
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredModelItemsArray.clear();
            if (filteredModelItemsArray != null && results != null && results.count > 0) {
                filteredModelItemsArray.addAll((ArrayList<BeatModel>) results.values);
            }
            notifyDataSetInvalidated();
        }
    }
}
