package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.ReasonsModel;

import java.util.ArrayList;
import java.util.List;

public class ReasonsSpinnerAdapter extends BaseAdapter {
    private ArrayList<ReasonsModel> reasons;
    private Context context;
    LayoutInflater inflater;

    public ReasonsSpinnerAdapter(Context context, ArrayList<ReasonsModel> reasons) {
        super();
        this.reasons = reasons;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void clear() {
        reasons.clear();
    }

    public void addItem(ReasonsModel yourObject) {
        reasons.add(yourObject);
    }

    public void addItems(List<ReasonsModel> yourObjectList) {
        reasons.addAll(yourObjectList);
    }

    @Override
    public int getCount() {
        return reasons.size();
    }

    @Override
    public Object getItem(int position) {
        return reasons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.spinner_item_dropdown, parent, false);

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));
        view.setTag(reasons.get(position));

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.spinner_item_layout, parent, false);
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));
        view.setTag(reasons.get(position));
        return view;
    }

    private String getTitle(int position) {
        return position >= 0 && position < reasons.size() ? reasons.get(position).getReason() : "";
    }
}
