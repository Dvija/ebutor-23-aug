package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.NotificationModel;
import com.ebutor.utils.Utils;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private ArrayList<NotificationModel> arrNotifications;
    private Context context;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> arrNotifications) {
        this.context = context;
        this.arrNotifications = arrNotifications;
    }

   /* public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
       vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });


      vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
          @Override
          public boolean onPreDraw() {

        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {



                ViewTreeObserver obs = tv.getViewTreeObserver();
              obs.removeGlobalOnLayoutListener(this);
             // obs.removeOnPreDrawListener(this);
              if (maxLine == 0) {
                  int lineEndIndex = tv.getLayout().getLineEnd(0);
                  String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + "\n " + expandText;
                  tv.setText(text);
                  tv.setMovementMethod(LinkMovementMethod.getInstance());
                  tv.setText(
                          addClickablePartTextViewResizable(tv.getText().toString(), tv, maxLine, expandText,
                                  viewMore), TextView.BufferType.SPANNABLE);
              } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                  int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                  String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + "\n " + expandText;
                  tv.setText(text);
                  tv.setMovementMethod(LinkMovementMethod.getInstance());
                  tv.setText(
                          addClickablePartTextViewResizable(tv.getText().toString(), tv, maxLine, expandText,
                                  viewMore), TextView.BufferType.SPANNABLE);
              } else {
                  int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                  String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                  tv.setText(text);
                  tv.setMovementMethod(LinkMovementMethod.getInstance());
                  tv.setText(
                          addClickablePartTextViewResizable(tv.getText().toString(), tv, lineEndIndex, expandText,
                                  viewMore), TextView.BufferType.SPANNABLE);
              }



          }
      });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final String strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned;
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {

                    if (viewMore) {
                        ViewGroup.LayoutParams params=tv.getLayoutParams();
                        if(params!=null){
                            params=new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        }
                        int width_view= View.MeasureSpec.makeMeasureSpec(tv.getWidth(), View.MeasureSpec.UNSPECIFIED);
                        int height_view= View.MeasureSpec.makeMeasureSpec(tv.getHeight(), View.MeasureSpec.UNSPECIFIED);
                        tv.measure(width_view,height_view);

                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }*/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_notifications, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        NotificationModel notificationModel = arrNotifications.get(position);
        if (notificationModel != null) {
            holder.tvMsg.setText(notificationModel.getMessage());
            ViewTreeObserver tree = holder.tvMsg.getViewTreeObserver();
            tree.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    Layout layout = holder.tvMsg.getLayout();
                    if (layout != null) {
                        int line = layout.getLineCount();
                        if (line > 2) {
                            holder.more.setVisibility(View.VISIBLE);
                        } else {
                            holder.more.setVisibility(View.INVISIBLE);
                        }
                    }
                    return true;
                }
            });

            //int line=holder.tvMsg.getLineCount();

            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (holder.c == 0) {
                        holder.tvMsg.setMaxLines(Integer.MAX_VALUE);
                        holder.more.setText(context.getString(R.string.show_less));
                        holder.more.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_less, 0);

                        holder.c = 1;
                    } else {
                        holder.tvMsg.setMaxLines(2);
                        holder.more.setText(context.getString(R.string.show_more));
                        holder.more.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_more, 0);
                        holder.c = 0;
                    }
                }
            });
            holder.tvDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_PATTERN, notificationModel.getDate()));
            holder.tvTime.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.TIME_STD_PATTERN, notificationModel.getDate()));
        }

        //   makeTextViewResizable(holder.tvMsg, 3, "View More", true);

        if (position % 2 == 0) {
            holder.ivType.setImageResource(R.drawable.ic_approve);
        } else {
            holder.ivType.setImageResource(R.drawable.ic_announce);
        }
        holder.ivType.setVisibility(View.GONE);
    }

    public void addItem(NotificationModel notificationModel) {
        if (arrNotifications != null && !arrNotifications.contains(notificationModel)) {
            this.arrNotifications.add(notificationModel);
        }
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        this.arrNotifications.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (arrNotifications != null && arrNotifications.size() > 0)
            return arrNotifications.size();
        else
            return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivType;
        TextView tvMsg, tvDate, tvTime, more;
        int c = 0;

        public MyViewHolder(View view) {
            super(view);
            tvMsg = (TextView) view.findViewById(R.id.tv_msg);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            more = (TextView) view.findViewById(R.id.tv_more);
            ivType = (ImageView) view.findViewById(R.id.iv_type);
        }

    }

}
