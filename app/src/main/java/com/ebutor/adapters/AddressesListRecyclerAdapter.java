package com.ebutor.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.UserAddressModel;

import java.util.ArrayList;
import java.util.List;

public class AddressesListRecyclerAdapter extends RecyclerView.Adapter<AddressesListRecyclerAdapter.ViewHolder> implements Filterable {

    private LayoutInflater mInflater;
    private ArrayList<UserAddressModel> mItems;
    private List<UserAddressModel> filteredModelItemsArray;
    private Context mContext;
    //    private DealItemClickListener dealItemClickListener;
    private View.OnClickListener mItemViewOnClickListener;
    private ModelFilter filter;
    private OnItemClickListener onItemClickListener;

    public AddressesListRecyclerAdapter(Context context, ArrayList<UserAddressModel> items) {
        mInflater = LayoutInflater.from(context);

        mItems = new ArrayList<>();
        mItems.addAll(items);
        this.filteredModelItemsArray = new ArrayList<UserAddressModel>();
        filteredModelItemsArray.addAll(mItems);

        mContext = context;

        getFilter();

        mItemViewOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemViewClick(v);
            }
        };
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModelFilter();
        }
        return filter;
    }

    @Override
    public int getItemCount() {
        return filteredModelItemsArray.size();
    }

//    private void onItemViewClick(View v) {
//        if (dealItemClickListener != null) dealItemClickListener.OnItemClickListener(v.getTag());
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.row_address, parent, false));
    }

//    public void setOnClickListener(Fragment fragment) {
//        this.dealItemClickListener = (DealItemClickListener) fragment;
//    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final UserAddressModel model = filteredModelItemsArray.get(position);
        holder.itemView.setTag(model);

        if (TextUtils.isEmpty(model.getAddress1())) {
            holder.tvAddress.setText(String.format(mContext.getResources().getString(R.string.userAddress1), model.getFirstName(),
                    model.getLastName(), model.getAddress(), model.getCity(),
                    model.getState(), model.getPin(), model.getTelephone()));
        } else {
            holder.tvAddress.setText(String.format(mContext.getResources().getString(R.string.userAddress), model.getFirstName(),
                    model.getLastName(), model.getAddress(), model.getAddress1(), model.getCity(),
                    model.getState(), model.getPin(), model.getTelephone()));
        }

//        holder.tvArea.setText(model.getCity());

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onEditClick(model, position);
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    private void handleOnClick(View v) {
        if (v != null && onItemClickListener != null) {
            onItemClickListener.onItemClick(v.getTag());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Object object);

        void onEditClick(Object object, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvAddress, tvArea;
        ImageView ivEdit;

        public ViewHolder(View convertView) {
            super(convertView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleOnClick(v);
                }
            });

            tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
            tvArea = (TextView) convertView.findViewById(R.id.tv_area);
            ivEdit = (ImageView) convertView.findViewById(R.id.ivEdit);
        }
    }

    private class ModelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                constraint = constraint.toString().toLowerCase();
                ArrayList<UserAddressModel> filteredItems = new ArrayList<UserAddressModel>();

                for (int i = 0, l = mItems.size(); i < l; i++) {
                    UserAddressModel m = mItems.get(i);
                    if (m.getAddress().toLowerCase().contains(constraint))
                        filteredItems.add(m);
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                result.values = mItems;
                result.count = mItems.size();

            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredModelItemsArray.clear();
            filteredModelItemsArray.addAll((ArrayList<UserAddressModel>) results.values);
            notifyDataSetChanged();
        }
    }
}
