package com.ebutor.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.OrderDetailActivity;
import com.ebutor.R;
import com.ebutor.models.CashBackHistoryModel;
import com.ebutor.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by 480108 on 8/16/2017.
 */

public class CashBackHistoryAdapter extends RecyclerView.Adapter<CashBackHistoryAdapter.MyViewHolder> {

    ArrayList<CashBackHistoryModel> cashBackHistoryModelsArrlist;
    private Context context;
    private CashBackHistoryAdapter.OnItemClickListener listener;
    public CashBackHistoryAdapter(Context context, ArrayList<CashBackHistoryModel> cashBackHistoryModelsArrlist) {
        this.context = context;
        this.cashBackHistoryModelsArrlist = cashBackHistoryModelsArrlist;

    }
    public void setClickListener(CashBackHistoryAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.cashbackhistory, parent, false);
        v.setTag(new MyViewHolder(v));

        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CashBackHistoryModel cashBackHistoryModel = cashBackHistoryModelsArrlist.get(position);
        if (cashBackHistoryModel != null) {


            holder.orderCode.setText(cashBackHistoryModel.getOrdercode());
            holder.orderCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, OrderDetailActivity.class);
                    intent.putExtra("orderId",cashBackHistoryModel.getOrderId());
                    context.startActivity(intent);
                }
            });
            holder.orderType.setText(cashBackHistoryModel.getCashbacktype());
            holder.orderAmount.setText(String.format(Locale.CANADA, "%.2f", cashBackHistoryModel.getCashbackamount()));
            holder.tvDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_STD_PATTERN5, cashBackHistoryModel.getTransactionDate()));

        }

    }

    @Override
    public int getItemCount() {
        return cashBackHistoryModelsArrlist.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView orderCode, orderType, orderAmount, tvDate;

        public MyViewHolder(View itemView) {
            super(itemView);

            orderCode = (TextView) itemView.findViewById(R.id.ordercode);
            orderAmount = (TextView) itemView.findViewById(R.id.orderamount);
            orderType = (TextView) itemView.findViewById(R.id.ordertype);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }


}
