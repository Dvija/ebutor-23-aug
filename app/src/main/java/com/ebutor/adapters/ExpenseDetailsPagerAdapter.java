package com.ebutor.adapters;

/**
 * Created by Srikanth Nama on 26-Dec-16.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ebutor.fragments.ApprovalHistoryFragment;
import com.ebutor.fragments.ExpenseDetailsFragment;

import java.util.ArrayList;

/**
 * Created by Admin on 11-12-2015.
 */
public class ExpenseDetailsPagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<String> mTabs;


    public ExpenseDetailsPagerAdapter(FragmentManager fm, ArrayList<String> tabs) {
        super(fm);
        this.mTabs = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new ExpenseDetailsFragment();
                break;
            case 1:
                fragment = new ApprovalHistoryFragment();
                break;
            default:
                fragment = new ExpenseDetailsFragment();
                break;
        }

        return fragment;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position);
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

}
