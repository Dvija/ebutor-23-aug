package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.ExpenseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ExpensesRecyclerAdapter extends RecyclerView.Adapter<ExpensesRecyclerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ExpenseModel> expenseModelArrayList = new ArrayList<>();
    private OnItemClickListener listener;
    private int viewType = 1;


    public ExpensesRecyclerAdapter(Context context, ArrayList<ExpenseModel> expenseModelArrayList) {
        this.context = context;
        this.expenseModelArrayList = expenseModelArrayList;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_expense, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ExpenseModel expenseModel = expenseModelArrayList.get(position);
        holder.tvExpenseCode.setText(String.format(context.getResources().getString(R.string.expense_code), expenseModel.getExpenseCode()));
        String date = parseDate(expenseModel.getExpenseDate(), "dd/MM/yyyy");//2016-12-26 16:14:23
//        holder.tvDate.setText(context.getResources().getString(R.string.date) + date);
        holder.tvExpenseSubject.setText(expenseModel.getExpenseSubject());
//        holder.tvActualAmount.setText(String.format(context.getResources().getString(R.string.actual_amount), expenseModel.getActualAmount()));
//        holder.tvApprovedAmount.setText(String.format(context.getResources().getString(R.string.approved_amount), expenseModel.getApprovedAmount()));

        final SpannableStringBuilder _date = new SpannableStringBuilder(date);
        setTextStyles(_date, holder.tvDate, 0, Color.parseColor("#000000"));

        SpannableStringBuilder _actualAmount = new SpannableStringBuilder("Actuals:" + expenseModel.getActualAmount());
        setTextStyles(_actualAmount, holder.tvActualAmount, 8, ContextCompat.getColor(context, R.color.primary));
        String approvedAmount = expenseModel.getApprovedAmount();

        SpannableStringBuilder _approvedAmount;
        if (null == approvedAmount || approvedAmount.equalsIgnoreCase("null")) {
            _approvedAmount = new SpannableStringBuilder("Approved: 0");
        } else {
            _approvedAmount = new SpannableStringBuilder("Approved: " + expenseModel.getApprovedAmount());
        }

        String status = expenseModel.getCurrentStatus();
        if (status != null && !status.equalsIgnoreCase("null")) {
            holder.tvStatus.setText("Status : " + status);
        } else {
            holder.tvStatus.setText("Status : ");
        }

        if (!expenseModel.getCurrentStatusId().equals("1")) {
            setTextStyles(_approvedAmount, holder.tvApprovedAmount, 9, ContextCompat.getColor(context, R.color.text_color_red));
        } else {
            holder.tvStatus.setText("Status : Approved");
            setTextStyles(_approvedAmount, holder.tvApprovedAmount, 9, ContextCompat.getColor(context, R.color.primary));
        }

        SpannableStringBuilder _reqFor = new SpannableStringBuilder("Type: " + expenseModel.getRequestFor());
        setTextStyles(_reqFor, holder.tvRequestFor, 5, ContextCompat.getColor(context, R.color.black));
//        holder.tvRequestFor.setText("Type: " +expenseModel.getRequestFor());

        if (getViewType() == 2) {
            holder.tvSubmittedBy.setVisibility(View.VISIBLE);
            holder.tvSubmittedBy.setText(expenseModel.getSubmittedByName() == null ? "Test User" : expenseModel.getSubmittedByName());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getViewType();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return expenseModelArrayList.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    private void setTextStyles(SpannableStringBuilder sb, TextView textView, int start, int color) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvExpenseCode, tvDate, tvExpenseSubject, tvActualAmount, tvApprovedAmount, tvStatus;
        TextView tvSubmittedBy, tvRequestFor;

        public MyViewHolder(View view) {
            super(view);
            tvExpenseCode = (TextView) view.findViewById(R.id.tv_expense_code);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvExpenseSubject = (TextView) view.findViewById(R.id.tv_expense_subject);
            tvActualAmount = (TextView) view.findViewById(R.id.tv_actuals);
            tvApprovedAmount = (TextView) view.findViewById(R.id.tv_approved);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvSubmittedBy = (TextView) view.findViewById(R.id.tv_submitted_by);
            tvRequestFor = (TextView) view.findViewById(R.id.tv_request_for);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(this.getAdapterPosition());
        }
    }
}
