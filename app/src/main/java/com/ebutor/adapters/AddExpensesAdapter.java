package com.ebutor.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.activities.AttachmentsActivity;
import com.ebutor.models.NewExpenseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class AddExpensesAdapter extends BaseAdapter {

    ArrayList<String> array = new ArrayList<>();
    private ArrayList<NewExpenseModel> expenseModelArrayList = new ArrayList<>();
    private OnItemClickListener listener;
    private Context context;
    private SparseBooleanArray selectedItems;
    private String attachments;

    public AddExpensesAdapter(Context context, ArrayList<NewExpenseModel> expenseModelArrayList) {
        this.context = context;
        this.expenseModelArrayList = expenseModelArrayList;
        selectedItems = new SparseBooleanArray();
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        if (expenseModelArrayList != null && expenseModelArrayList.size() > 0)
            return expenseModelArrayList.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return expenseModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MyViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_add_expense_potrait, parent, false);
            holder = new MyViewHolder(convertView);

            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }

        final NewExpenseModel expenseModel = expenseModelArrayList.get(position);
        holder.tvExpenseType.setText(expenseModel.getExpenseType());
//        holder.tvRefId.setText(expenseModel.getExpenseTypeId());
        holder.tvDesc.setText(expenseModel.getDesc());
        attachments = expenseModelArrayList.get(position).getAttachments();
        array = new ArrayList<>();
        if (!TextUtils.isEmpty(attachments))
            array = new ArrayList<String>(Arrays.asList(attachments.split("\\s*,\\s*")));
        if (array != null) {
            holder.tvBadge.setText(String.valueOf(array.size()));
        }

        SpannableStringBuilder _actualAmount = new SpannableStringBuilder(expenseModel.getAmount());
        setTextStyles(_actualAmount, holder.tvAmount, 0, Color.parseColor("#000000"));

        final SpannableStringBuilder _date = new SpannableStringBuilder(parseDate(expenseModel.getDate(), "dd/MM/yyyy"));
        setTextStyles(_date, holder.tvDate, 0, Color.parseColor("#000000"));

        holder.ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (array != null && array.size() > 0) {
                    Intent intent = new Intent(context, AttachmentsActivity.class);
                    intent.putExtra("Attachments", attachments);
                    context.startActivity(intent);
                }
            }
        });

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                expenseModel.setChecked(buttonView.isChecked());
                listener.onCheckChanged();
            }
        });

        return convertView;
    }


    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat(format, Locale.US);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    private void setTextStyles(SpannableStringBuilder sb, TextView textView, int start, int color) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public ArrayList<NewExpenseModel> getSelectedItems() {
        ArrayList<NewExpenseModel> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(expenseModelArrayList.get(selectedItems.keyAt(i)));
        }
        return items;
    }

    public String getSelectedIds() {
        StringBuilder ids = new StringBuilder();
        ArrayList<NewExpenseModel> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(expenseModelArrayList.get(selectedItems.keyAt(i)));
            ids.append(expenseModelArrayList.get(selectedItems.keyAt(i)).getExpenseId());
            ids.append(",");
        }

        String result = ids.toString();
        if (result.endsWith(",")) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public interface OnItemClickListener {
        void onCheckChanged();
    }

    public class MyViewHolder {

        TextView tvExpenseType, tvDate, tvAmount, tvDesc, /*tvRefId,*/
                tvBadge;
        ImageView ivAttachment;
        CheckBox checkbox;

        public MyViewHolder(View convertView) {
            tvExpenseType = (TextView) convertView.findViewById(R.id.tv_expense_type);
            tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            tvAmount = (TextView) convertView.findViewById(R.id.tv_amount);
            tvDesc = (TextView) convertView.findViewById(R.id.tv_desc);
            tvBadge = (TextView) convertView.findViewById(R.id.tvBadge);
            ivAttachment = (ImageView) convertView.findViewById(R.id.iv_attachment);
            checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);

        }
    }
}
