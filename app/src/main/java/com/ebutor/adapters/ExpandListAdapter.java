package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.customview.CustomGridView;
import com.ebutor.models.DashboardModel;
import com.ebutor.models.TeamDashboard;

import java.util.ArrayList;

public class ExpandListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<TeamDashboard> groups;
    LayoutInflater mInflater;

    public ExpandListAdapter(Context context, ArrayList<TeamDashboard> groups) {
        this.context = context;
        this.groups = groups;

        mInflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<DashboardModel> chList = groups.get(groupPosition).getDashboardModelArrayList();
        return chList.size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

//        DashboardModel child = (DashboardModel) getChild(groupPosition, childPosition);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.team_child_item, null);
        }
        CustomGridView gridView = (CustomGridView) convertView;

        gridView.setNumColumns(3);// gridView.setGravity(Gravity.CENTER);//
        gridView.setHorizontalSpacing(20);// SimpleAdapter adapter =
        gridView.setVerticalSpacing(20);
        GridAdapter adapter = new GridAdapter(context, groups.get(groupPosition).getDashboardModelArrayList());
        gridView.setAdapter(adapter);// Adapter

        int totalHeight = 0;
        for (int size = 0; size < adapter.getCount(); size++) {
            LinearLayout relativeLayout = (LinearLayout) adapter.getView(
                    size, null, gridView);
//            TextView textView = (TextView) relativeLayout.getChildAt(0);
            relativeLayout.measure(0, 0);
            totalHeight += relativeLayout.getMeasuredHeight();
        }
//        totalHeight+=100;
        gridView.SetHeight(totalHeight);

        return convertView;
    }




    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        TeamDashboard group = (TeamDashboard) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.team_group_item, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.group_name);
        tv.setText(group.getName());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
