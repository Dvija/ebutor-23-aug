package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.FilterDataModel;

import java.util.ArrayList;

public class ChipAdapter extends RecyclerView.Adapter<ChipAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<FilterDataModel> arrFilters;
    private LayoutInflater inflater;
    private onClickListener listener;

    public ChipAdapter(Context context, ArrayList<FilterDataModel> arrFilters) {
        this.context = context;
        this.arrFilters = arrFilters;
        inflater = LayoutInflater.from(context);
    }

    public void setListener(onClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = inflater.inflate(R.layout.row_chip, parent, false);

        return new MyViewHolder(v);
    }

    public void remove(FilterDataModel filterDataModel) {
        if (arrFilters != null && arrFilters.contains(filterDataModel)) {
            arrFilters.remove(filterDataModel);
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.itemView.setTag(arrFilters.get(position));
        holder.tvChipName.setText(arrFilters.get(position).getValue());

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemDelete(position, holder.itemView.getTag());
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrFilters.size();
    }

    public interface onClickListener {
        void onItemDelete(int position, Object object);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvChipName;
        ImageView ivDelete;

        public MyViewHolder(View view) {
            super(view);
            tvChipName = (TextView) view.findViewById(R.id.tv_chip_name);
            ivDelete = (ImageView) view.findViewById(R.id.iv_delete);
        }
    }
}
