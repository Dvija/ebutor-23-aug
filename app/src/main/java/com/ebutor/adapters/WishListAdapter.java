package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.WishListModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private ArrayList<WishListModel> arrWishList;
    private ModelFilter filter;

    private ArrayList<WishListModel> filteredModelItemsArray;
    private OnItemClickListener listener;

    public WishListAdapter(Context context, ArrayList<WishListModel> arrWishList) {
        this.context = context;

        this.arrWishList = new ArrayList<>();
        this.arrWishList.addAll(arrWishList);
        this.filteredModelItemsArray = new ArrayList<WishListModel>();
        filteredModelItemsArray.addAll(this.arrWishList);

        getFilter();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModelFilter();
        }
        return filter;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.wishlist_single_item, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvListName.setText(filteredModelItemsArray.get(position).getListName());
        String date = parseDate(filteredModelItemsArray.get(position).getCreatedDate(), "dd-MM-yyyy HH:mm:ss");
        holder.tvDate.setText(context.getResources().getString(R.string.date) + ":" + date);
        holder.setObject(filteredModelItemsArray.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return filteredModelItemsArray.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public interface OnItemClickListener {
        public void onItemClick(int position, Object object);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvListName, tvDate;
        Object listObj;

        public MyViewHolder(View view) {
            super(view);
            tvListName = (TextView) view.findViewById(R.id.tv_purchaseList_item);
            tvDate = (TextView) view.findViewById(R.id.tv_purchase_date);
            view.setOnClickListener(this);
        }

        public void setObject(Object object) {
            listObj = object;
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(this.getPosition(), listObj);
        }
    }

    private class ModelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                constraint = constraint.toString().toLowerCase();
                ArrayList<WishListModel> filteredItems = new ArrayList<WishListModel>();

                for (int i = 0, l = arrWishList.size(); i < l; i++) {
                    WishListModel m = arrWishList.get(i);
                    if (m.getListName().toLowerCase().contains(constraint))
                        filteredItems.add(m);
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                result.values = arrWishList;
                result.count = arrWishList.size();

            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

           /* filteredModelItemsArray = (ArrayList<RedeemHistoryModel>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filteredModelItemsArray.size(); i < l; i++)
                add(filteredModelItemsArray.get(i));
            notifyDataSetInvalidated();*/
            filteredModelItemsArray.clear();
            filteredModelItemsArray.addAll((ArrayList<WishListModel>) results.values);
            notifyDataSetChanged();
        }
    }
}
