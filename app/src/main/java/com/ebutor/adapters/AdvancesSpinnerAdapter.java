package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.ExpenseMasterLookupData;
import com.ebutor.models.ExpenseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 300041 on 20-Aug-15.
 */
public class AdvancesSpinnerAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private List<ExpenseModel> mItems = new ArrayList<>();
    private Context mContext;

    public AdvancesSpinnerAdapter(Context context, ArrayList<ExpenseModel> items) {
        this.mItems = items;
        mContext = context;
        mInflater = LayoutInflater.from(context);

    }


    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
//        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
        view = mInflater.inflate(R.layout.spinner_item_dropdown, parent, false);
//            view.setTag("DROPDOWN");
//        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
//        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
        view = mInflater.inflate(R.layout.
                spinner_item_layout, parent, false);
//            view.setTag("NON_DROPDOWN");
//        }
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));
        view.setTag(getItem(position));
        return view;
    }

    private String getTitle(int position) {
        return position >= 0 && position < mItems.size() ? mItems.get(position).getExpenseSubject() : "";
    }
}
