package com.ebutor.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.RetailersModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by 100251 on 6/16/2015.
 */
public class BuyerInfoAdapter extends BaseAdapter {
    OnBuyerSelectedListener onBuyerSelectedListener;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private ArrayList<Row> rows;
    private ArrayList<Row> arraylist;

    /*public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }*/

    public BuyerInfoAdapter(Context con, ArrayList<Row> rows) {
        this.rows = rows;

        this.arraylist = new ArrayList<Row>();
        this.arraylist.addAll(rows);
//    	this.arraylist = rows;
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    public void setOnBuyerSelectedListener(Fragment fragment) {
        try {
            onBuyerSelectedListener = (OnBuyerSelectedListener) fragment;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Row getItem(int position) {
        return rows.get(position);
    }

 /*   public void setRows(List<Row> rows) {
        this.rows = rows;
    }*/

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof RetailersModel) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (getItemViewType(position) == 0) { // Item
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_retailer_item, parent, false);
            }

            final Item item = (Item) getItem(position);
            TextView retailerShopName = (TextView) view.findViewById(R.id.tv_retailer_shop_name);
            TextView retailerLocation = (TextView) view.findViewById(R.id.tv_retailer_location);
            TextView retailerMobile = (TextView) view.findViewById(R.id.tv_mobile_number);


            retailerShopName.setText(item.text.getCompany());

            retailerLocation.setText(item.text.getAddress1());
            retailerMobile.setText(item.text.getTelephone());


            view.setTag(item.text);
            view.findViewById(R.id.mainView).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onBuyerSelectedListener != null) {
                        onBuyerSelectedListener.OnBuyerSelected(item.text);
                    }
                }
            });


        } else { // Section
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_section, parent, false);
            }

            RetailersModel section = (RetailersModel) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.textView1);
            textView.setText(section.getCompany().substring(0, 1));

        }

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        ArrayList<RetailersModel> locationList = new ArrayList<RetailersModel>();
        rows.clear();
        if (charText.length() == 0) {
            rows.addAll(arraylist);
        } else {
            for (Row wp : arraylist) {
                if (wp instanceof RetailersModel) {
                    if (((RetailersModel) wp).getCompany().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        locationList.add((RetailersModel) wp);
                    }
                }

            }

            Collections.sort(locationList, new Comparator<RetailersModel>() {
                        public int compare(RetailersModel lhs, RetailersModel rhs) {
                            return lhs.getCompany().compareTo(rhs.getCompany());
                        }
                    }
            );

            ArrayList<Row> rows = new ArrayList<Row>();
            int start = 0;
            int end = 0;
            String previousLetter = null;
            Object[] tmpIndexItem = null;
            Pattern numberPattern = Pattern.compile("[0-9]");

            for (RetailersModel country : locationList) {

                String firstLetter = country.getCompany().substring(0, 1);

                // Group numbers together in the scroller
                if (numberPattern.matcher(firstLetter).matches()) {
                    firstLetter = "#";
                }

                // If we've changed to a new letter, add the previous letter to the alphabet scroller
                if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                    end = rows.size() - 1;
                    tmpIndexItem = new Object[3];
                    tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                    tmpIndexItem[1] = start;
                    tmpIndexItem[2] = end;
                    alphabet.add(tmpIndexItem);

                    start = end + 1;
                }

                // Check if we need to add a header row
                if (!firstLetter.equals(previousLetter)) {
                    rows.add(country);
                    sections.put(firstLetter, start);
                }

                // Add the country to the list
                rows.add(new Item(country));
                previousLetter = firstLetter;
            }

            if (previousLetter != null) {
                // Save the last letter
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = rows.size() - 1;
                alphabet.add(tmpIndexItem);
            }
            this.rows = rows;
        }
        notifyDataSetChanged();
    }

    public interface OnBuyerSelectedListener {
        public void OnBuyerSelected(RetailersModel retailersModel);
    }

    public static abstract class Row {
    }

    public static final class Item extends Row {
        public final RetailersModel text;

        public Item(RetailersModel text) {
            this.text = text;
        }
    }

}