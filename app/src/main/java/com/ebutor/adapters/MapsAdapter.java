package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;

public class MapsAdapter extends RecyclerView.Adapter<MapsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<CustomerTyepModel> arrBeats;

    public MapsAdapter(Context context, ArrayList<CustomerTyepModel> arrBeats) {
        this.context = context;
        this.arrBeats = arrBeats;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_map_beat, parent, false);
        v.setTag(new MapsAdapter.MyViewHolder(v));
        return new MapsAdapter.MyViewHolder(v);
    }

    public void addItem(CustomerTyepModel customerTyepModel) {
        if (arrBeats != null && !arrBeats.contains(customerTyepModel)) {
            arrBeats.add(customerTyepModel);
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvBeat.setText(arrBeats.get(position).getCustomerName());
//        Bitmap bmp = Bitmap.createBitmap(20, 20, Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bmp);
//        canvas.drawColor(arrBeats.get(position).getColor());
        holder.ivBeat.setBackgroundColor(arrBeats.get(position).getColor());
    }

    @Override
    public int getItemCount() {
        return arrBeats.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvBeat;
        ImageView ivBeat;

        public MyViewHolder(View view) {
            super(view);
            tvBeat = (TextView) view.findViewById(R.id.tv_beat);
            ivBeat = (ImageView) view.findViewById(R.id.iv_legend);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
