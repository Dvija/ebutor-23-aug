package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.ebutor.R;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.RetailersModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class MultiSelectAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private Context context;
    private OnSelectListener onSelectListener;
    private ArrayList<CustomerTyepModel> arrManf;
    private RetailersModel retailersModel;
    private ArrayList<String> arrIds = new ArrayList<>();
    private String type;

    public MultiSelectAdapter(Context context, ArrayList<CustomerTyepModel> arrManf, RetailersModel retailersModel) {
        this.context = context;
        this.arrManf = arrManf;
        this.retailersModel = retailersModel;
        String ids = retailersModel.getMasterManfIds();
        if (ids != null && ids.length() > 0)
            arrIds = new ArrayList<String>(Arrays.asList(ids.split("\\s*,\\s*")));
        mInflater = LayoutInflater.from(context);
    }

    public MultiSelectAdapter(Context context, ArrayList<CustomerTyepModel> arrManf, String manfIds, String type) {
        this.context = context;
        this.arrManf = arrManf;
        this.type = type;
        if (manfIds != null && manfIds.length() > 0)
            arrIds = new ArrayList<String>(Arrays.asList(manfIds.split("\\s*,\\s*")));
        mInflater = LayoutInflater.from(context);
    }

    public static ArrayList<String> filterNames(Collection<CustomerTyepModel> target, Predicate<CustomerTyepModel> predicate) {
        ArrayList<String> result = new ArrayList<String>();
        for (CustomerTyepModel element : target) {
            if (predicate.apply(element)) {
                result.add(element.getCustomerName());
            }
        }
        return result;
    }

    public void setSelectListener(OnSelectListener listener) {
        this.onSelectListener = listener;
    }

    @Override
    public int getCount() {
        return arrManf.size();
    }

    @Override
    public Object getItem(int position) {
        return arrManf.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_master_manf, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.ctvManf.setText(arrManf.get(position).getCustomerName());
        try {
            if (arrIds != null && arrIds.size() > 0) {
                if (arrIds.contains(arrManf.get(position).getCustomerGrpId()) || arrManf.get(position).isChecked()) {
                    holder.ctvManf.setChecked(true);
                    arrManf.get(position).setIsChecked(true);
                } else {
                    holder.ctvManf.setChecked(false);
                    arrManf.get(position).setIsChecked(false);
                }
            } else {
                holder.ctvManf.setChecked(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final ViewHolder finalHolder = holder;
        holder.ctvManf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalHolder.ctvManf.isChecked()) {
                    finalHolder.ctvManf.setChecked(false);
                    onSelectListener.onSelected(false, position);
//                    arrManf.get(position).setIsChecked(false);
                    if (arrIds != null && arrIds.contains(arrManf.get(position).getCustomerGrpId())) {
                        arrIds.remove(arrManf.get(position).getCustomerGrpId());
                    }
                } else {
                    Predicate<CustomerTyepModel> isCheckedData = new Predicate<CustomerTyepModel>() {
                        @Override
                        public boolean apply(CustomerTyepModel customerTyepModel) {
                            return customerTyepModel.isChecked();
                        }
                    };

                    ArrayList<String> checkedFilters = filterNames(arrManf, isCheckedData);
                    if (checkedFilters.size() == 3 && !type.equalsIgnoreCase("Beat")) {
                        Toast.makeText(context, "max 3 can be selected", Toast.LENGTH_SHORT).show();
                    } else {
                        finalHolder.ctvManf.setChecked(true);
//                        arrManf.get(position).setIsChecked(true);
                        onSelectListener.onSelected(true, position);
                        if (arrIds != null && !arrIds.contains(arrManf.get(position).getCustomerGrpId()))
                            arrIds.add(arrManf.get(position).getCustomerGrpId());
                    }
                }
            }
        });

        return convertView;
    }


    public interface OnSelectListener {

        void onSelected(boolean isChecked, int pos);//Unselect all the fields or particular fields based on flag isUnSelectAll on clicking none
    }

    class ViewHolder {
        CheckedTextView ctvManf;

        ViewHolder(View view) {
            ctvManf = (CheckedTextView) view.findViewById(R.id.ctv_manf);
        }
    }
}
