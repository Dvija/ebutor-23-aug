package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.FilterDataModel;
import com.ebutor.models.StarLevelCashBackDetailsModel;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by 480116 on 8/21/2017.
 */

public class CashbackAdapter extends RecyclerView.Adapter<CashbackAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<StarLevelCashBackDetailsModel> starLevelCashBackDetailsModels;

    private LayoutInflater inflater;

    public CashbackAdapter(Context context, ArrayList<StarLevelCashBackDetailsModel> amount) {
        this.context = context;
        this.starLevelCashBackDetailsModels=amount;
        inflater = LayoutInflater.from(context);
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = inflater.inflate(R.layout.cashback_adapter, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        StarLevelCashBackDetailsModel starLevel=starLevelCashBackDetailsModels.get(position);

        holder.tvamount.setText( String.format(Locale.CANADA, "%.2f",starLevel.getStarTotalAppliedAmount()));
        holder.tvstar.setText( starLevel.getStarColor());
        holder.tvcashback.setText(String.format(Locale.CANADA, "%.2f",starLevel.getStarCashbackAmount()));


    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return starLevelCashBackDetailsModels.size();
    }

    public interface onClickListener {
        void onItemDelete(int position, Object object);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvamount,tvstar,tvcashback;

        public MyViewHolder(View view) {
            super(view);
            tvamount = (TextView) view.findViewById(R.id.tv_amount);
            tvstar = (TextView) view.findViewById(R.id.tv_star);
            tvcashback = (TextView) view.findViewById(R.id.tv_cashback);

        }
    }
}

