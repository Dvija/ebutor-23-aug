package com.ebutor.adapters;

/**
 * Created by Srikanth Nama on 26-Dec-16.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ebutor.fragments.POApprovalListFragment;

import java.util.ArrayList;

/**
 * Created by Admin on 11-12-2015.
 */
public class POApprovalViewPagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<String> mTabs;

    public POApprovalViewPagerAdapter(FragmentManager fm, ArrayList<String> tabs) {
        super(fm);
        this.mTabs = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new POApprovalListFragment();
                break;
            case 1:
                fragment = new POApprovalListFragment();
                break;
            default:
                fragment = new POApprovalListFragment();
                break;
        }

        return fragment;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position);
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

}
