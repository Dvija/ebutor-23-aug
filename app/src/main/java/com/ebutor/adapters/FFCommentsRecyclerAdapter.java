package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;

public class FFCommentsRecyclerAdapter extends RecyclerView.Adapter<FFCommentsRecyclerAdapter.MyViewHolder> {

    final LayoutInflater inflater;
    private Context context;
    private ArrayList<CustomerTyepModel> arrCartList;
    private onOptionSelectedListener listener;

    public FFCommentsRecyclerAdapter(Context context, ArrayList<CustomerTyepModel> arrCartList) {
        this.context = context;
        this.arrCartList = arrCartList;
        inflater = LayoutInflater.from(context);
    }

    public void setListener(onOptionSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View v = inflater.inflate(R.layout.row_ff_comments, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        CustomerTyepModel model = arrCartList.get(position);

        holder.tvproduct.setText(model.getCustomerName());
        holder.tvproduct.setTag(model);
        holder.tvproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null)
                    listener.onOptionSelected(v.getTag(), position);
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (arrCartList != null)
            return arrCartList.size();
        else
            return 0;
    }

    public interface onOptionSelectedListener {
        void onOptionSelected(Object object, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvproduct;

        public MyViewHolder(View view) {
            super(view);
            tvproduct = (TextView) view.findViewById(R.id.textView2);

        }
    }

}
