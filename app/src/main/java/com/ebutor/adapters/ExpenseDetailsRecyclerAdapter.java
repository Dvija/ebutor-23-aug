package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.ExpenseDetailsModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ExpenseDetailsRecyclerAdapter extends RecyclerView.Adapter<ExpenseDetailsRecyclerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ExpenseDetailsModel> expenseModelArrayList = new ArrayList<>();
    private OnItemClickListener listener;

    public ExpenseDetailsRecyclerAdapter(Context context, ArrayList<ExpenseDetailsModel> expenseModelArrayList) {
        this.context = context;
        this.expenseModelArrayList = expenseModelArrayList;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_expense_detail, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        ExpenseDetailsModel expenseModel = expenseModelArrayList.get(position);
        holder.tvType.setText(expenseModel.getDetailExpenseType());
        holder.tvAmount.setText(expenseModel.getDetailActualAmount());
        holder.tvApproved.setText(expenseModel.getDetailApprovedAmount());
        if(expenseModel.getDetailDescription().isEmpty())
        {
            holder.tvDesc.setText(context.getString(R.string.no_description));
            holder.tvDesc.setAlpha((float)0.5);
        }
        else
        {

            holder.tvDesc.setText(expenseModel.getDetailDescription());
            holder.tvDesc.setAlpha(1);
        }

        holder.tvDesc.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.ck == 0) {
                    holder.tvDesc.setVisibility(View.VISIBLE);
                    holder.tvDesc.setMaxLines(Integer.MAX_VALUE);

                    holder.ck = 1;
                } else {
                    holder.tvDesc.setVisibility(View.GONE);
                    holder.ck = 0;
                }
            }
        });


        holder.tvComment.setText("");
        if (TextUtils.isEmpty(expenseModel.getAttachedFiles())) {
            holder.ibAttachment.setVisibility(View.INVISIBLE);
        }
        holder.ibAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null){
                    listener.onItemClick(position);
                }
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return expenseModelArrayList.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    private void setTextStyles(SpannableStringBuilder sb, TextView textView, int start, int color) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvType, tvAmount, tvApproved, tvDesc, tvComment;
        ImageButton ibAttachment;
        int ck = 0;


        public MyViewHolder(View view) {
            super(view);
            tvType = (TextView) view.findViewById(R.id.tv_type);
            tvAmount = (TextView) view.findViewById(R.id.tv_amount);
            tvApproved = (TextView) view.findViewById(R.id.tv_approved);
            tvDesc = (TextView) view.findViewById(R.id.tv_desc);
            tvComment = (TextView) view.findViewById(R.id.tv_comments);
            ibAttachment = (ImageButton) view.findViewById(R.id.attachment);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(this.getAdapterPosition());
        }
    }
}
