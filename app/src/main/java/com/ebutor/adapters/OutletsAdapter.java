package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.OutletsModel;

import java.util.ArrayList;

/**
 * Created by 300041 on 23-Sep-15.
 */
public class OutletsAdapter extends ArrayAdapter<OutletsModel> {
    private final String MY_DEBUG_TAG = "ContactModelAdapter";
    private ArrayList<OutletsModel> items;
    private ArrayList<OutletsModel> itemsAll;
    private ArrayList<OutletsModel> suggestions;
    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((OutletsModel) (resultValue)).getCompany();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null) {
                suggestions.clear();
                for (OutletsModel retailersModel : itemsAll) {
                    if (retailersModel.getCompany().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            retailersModel.getTelephone().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(retailersModel);
                    }
                }
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
            } else {
                filterResults.values = itemsAll;
                filterResults.count = itemsAll.size();
            }
            return filterResults;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<OutletsModel> filteredList = (ArrayList<OutletsModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                addAll(filteredList);
                notifyDataSetChanged();
            }
        }
    };
    private int viewResourceId;

    public OutletsAdapter(Context context, int viewResourceId, ArrayList<OutletsModel> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<OutletsModel>) items.clone();
        this.suggestions = new ArrayList<OutletsModel>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);

        }
        OutletsModel retailersModel = items.get(position);
        if (retailersModel != null) {
            TextView contactName = (TextView) v.findViewById(R.id.textView1);
            TextView contactNumber = (TextView) v.findViewById(R.id.textView2);
            contactName.setText(retailersModel.getCompany());
            contactNumber.setText(retailersModel.getTelephone());
        }
        v.setTag(retailersModel);
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

}
