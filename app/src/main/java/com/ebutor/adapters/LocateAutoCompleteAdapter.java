package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.RetailersModel;

import java.util.ArrayList;

/**
 * Created by 300041 on 23-Sep-15.
 */
public class LocateAutoCompleteAdapter extends ArrayAdapter<RetailersModel> {
    private final String MY_DEBUG_TAG = "ContactModelAdapter";
    private ArrayList<RetailersModel> items;
    private ArrayList<RetailersModel> itemsAll;
    private ArrayList<RetailersModel> suggestions;
    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((RetailersModel) (resultValue)).getCompany();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null) {
                suggestions.clear();
                for (RetailersModel retailersModel : itemsAll) {
                    if (retailersModel.getCompany().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            retailersModel.getTelephone().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(retailersModel);
                    }
                }
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
            } else {
                filterResults.values = itemsAll;
                filterResults.count = itemsAll.size();
            }
            return filterResults;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<RetailersModel> filteredList = (ArrayList<RetailersModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                addAll(filteredList);
                notifyDataSetChanged();
            }
        }
    };
    private int viewResourceId;

    public LocateAutoCompleteAdapter(Context context, int viewResourceId, ArrayList<RetailersModel> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<RetailersModel>) items.clone();
        this.suggestions = new ArrayList<RetailersModel>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);

        }
        RetailersModel retailersModel = items.get(position);
        if (retailersModel != null) {
            TextView contactName = (TextView) v.findViewById(R.id.textView1);
            TextView contactNumber = (TextView) v.findViewById(R.id.textView2);
            contactName.setText(retailersModel.getCompany());
            contactNumber.setText(retailersModel.getTelephone());
        }
        v.setTag(retailersModel);
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

}
