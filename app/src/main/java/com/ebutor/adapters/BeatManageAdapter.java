package com.ebutor.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.BeatModel;
import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;

public class BeatManageAdapter extends BaseTableAdapter {

    private final static int WIDTH_DIP = 110;
    private final static int HEIGHT_DIP = 50;
    private final float density;
    private final int[] widths = {130, 150, 200, 80, 80, 80, 80};
    private final int width, height;
    private Context context;
    private String[][] array;
    private OnItemClickListener listener;

    public BeatManageAdapter(Context context, String[][] array) {
        super();
        this.context = context;
        this.array = array;
        density = context.getResources().getDisplayMetrics().density;
        Resources r = context.getResources();
        width = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, WIDTH_DIP, r.getDisplayMetrics()));
        height = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, HEIGHT_DIP, r.getDisplayMetrics()));
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setData(String[][] array) {
        this.array = array;
        notifyDataSetChanged();
    }

    @Override
    public int getRowCount() {
        if (array.length > 0)
            return array.length - 1;
        else
            return 0;
    }

    @Override
    public int getColumnCount() {
        if (array.length > 0 && array[0].length > 4)
            return array[0].length - 5;
        else
            return 0;
    }

    @Override
    public View getView(final int row, final int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_table, parent, false);
        }

        if (array.length > 0 && array[0].length > 2) {
            TextView textView = (TextView) convertView.findViewById(R.id.textview);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_map);
            if (row == -1 || column == -1) {
                textView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
                textView.setBackgroundColor(context.getResources().getColor(R.color.light_gray));
                textView.setTypeface(Typeface.DEFAULT_BOLD);
            } else if (column == 3) {
                textView.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
            } else {
                textView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
                textView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                textView.setTypeface(Typeface.DEFAULT);
            }
            textView.setText(array[row + 1][column + 1].toString());

        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (array.length > row && array[0].length > 6) {
                    BeatModel beatModel = new BeatModel();
                    beatModel.setBeatName(array[row + 1][0]);
                    beatModel.setRmName(array[row + 1][1]);
                    beatModel.setDay(array[row + 1][2]);
                    beatModel.setTotalOutlets(array[row + 1][3]);
                    beatModel.setBeatId(array[row + 1][5]);
                    beatModel.setRmId(array[row + 1][6]);
                    beatModel.setHubId(array[row + 1][7]);
                    beatModel.setDcId(array[row + 1][8]);

                    if (column == 3) {
                        listener.onMapCLick(beatModel);
                    } else {

                        listener.onItemClick(beatModel);
                    }
                }
            }
        });
        return convertView;
    }

    @Override
    public int getWidth(int column) {
        return Math.round(widths[column + 1] * density);
    }

    @Override
    public int getHeight(int row) {
        return height;
    }

    @Override
    public int getItemViewType(int row, int column) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public interface OnItemClickListener {
        public void onItemClick(BeatModel beatModel);

        public void onMapCLick(BeatModel beatModel);
    }
}
