package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.CartModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.MyViewHolder> {

    final LayoutInflater inflater;
    private Context context;
    private ArrayList<CartModel> arrCartList;
    private onCartItemDeleteListener listener;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    public CartListAdapter(Context context, ArrayList<CartModel> arrCartList) {
        this.context = context;
        this.arrCartList = arrCartList;
        inflater = LayoutInflater.from(context);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new SimpleBitmapDisplayer())
                .build();
    }

    public void setListener(onCartItemDeleteListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View v = inflater.inflate(R.layout.row_cart, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        CartModel model = arrCartList.get(position);
        holder.llCart.setTag(model);
        holder.tvproduct.setText(model.getProductTitle());
        setOfferImageBackground(arrCartList.get(position).getPackType(), holder);

        if (model.getStatus() == 0) {
//            holder.tvproduct.setTextColor(ContextCompat.getColor(context, R.color.text_color_red));
            holder.tvproduct.setPaintFlags(holder.tvproduct.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else if (model.getStatus() == -1) {
            holder.tvproduct.setTextColor(Color.RED);
            holder.tvproduct.setPaintFlags(0);
        } else {
            holder.tvproduct.setTextColor(Color.BLACK);
            holder.tvproduct.setPaintFlags(0);
        }

        holder.ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v.getTag(), position, 0);
            }
        });
        holder.llCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onItemClick(v.getTag(), position, 1);
            }
        });
        holder.ivCross.setTag(model);
        holder.tvQty.setText(String.valueOf(model.getQuantity()));
        holder.tvCashback.setText(String.format(Locale.CANADA, "%.2f", model.getCashbackAmount()));
        holder.tvTotalPrice.setText(String.format(Locale.CANADA, "%.2f", model.getTotalPrice()));
        ImageLoader.getInstance().displayImage(model.getProductImage(), holder.ivProduct, options, animateFirstListener);

        if (model.getFreebieProductId() != null && model.getFreebieQty() != null && !model.getFreebieQty().equals("0")) {
            holder.freebieLayout.setVisibility(View.VISIBLE);
            holder.tvFreebieProduct.setText(model.getFreebieTitle());
            holder.tvFreebieQty.setText(model.getFreebieQty());
            holder.tvFreebieTotalPrice.setText("0");
        } else {
            holder.freebieLayout.setVisibility(View.GONE);
            holder.tvFreebieProduct.setText("");
            holder.tvFreebieQty.setText("");
            holder.tvFreebieTotalPrice.setText("0");
        }

        /*if (model.getCashbackAmount() > 0) {
            holder.llCashback.setVisibility(View.VISIBLE);
            holder.btnEcash.setVisibility(View.VISIBLE);
            holder.tvEcash.setVisibility(View.VISIBLE);
            holder.tvEcash.setText(String.format(context.getString(R.string.cashback_will_be_added), String.format("%.2f", model.getCashbackAmount())));
        } else {
            holder.llCashback.setVisibility(View.GONE);
            holder.btnEcash.setVisibility(View.GONE);
            holder.tvEcash.setVisibility(View.GONE);

        }*/

    }

    public void setOfferImageBackground(String packType, MyViewHolder holder) {
        if (packType != null) {
            switch (packType) {
                case "Regular":
                case "REGULAR":
                    holder.ivOffer.setVisibility(View.GONE);
                    break;
                case "Consumer Pack":
                case "CONSUMER PACK":
                case "CP Inside":
                case "CP Outside":
                case "Consumer Pack Inside":
                case "Consumer Pack Outside":
                    holder.ivOffer.setVisibility(View.VISIBLE);
                    holder.ivOffer.setBackgroundResource(R.drawable.ic_offer_green);
                    holder.ivOffer.setText("CP Offer");
                    break;
                case "Freebie":
                case "FREEBIE":
                    holder.ivOffer.setVisibility(View.VISIBLE);
                    holder.ivOffer.setBackgroundResource(R.drawable.ic_offer_red);
                    holder.ivOffer.setText("Freebie");
                    break;
                default:
                    holder.ivOffer.setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (arrCartList != null)
            return arrCartList.size();
        else
            return 0;
    }

    public void setArrCart(ArrayList<CartModel> arrCartList) {
        this.arrCartList = arrCartList;
        this.notifyDataSetChanged();
    }

    public void clearData() {
        int size = this.arrCartList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.arrCartList.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void deleteItem(int pos) {
        int size = this.arrCartList.size();
        if (size > 0) {
//            for (int i = 0; i < size; i++) {

//            }
            try {

                this.arrCartList.remove(pos);
                this.notifyItemRangeRemoved(pos, size);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public interface onCartItemDeleteListener {
        void onItemClick(Object object, int position, int type);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProduct, ivCross;
        TextView tvproduct, tvTotalPrice, tvQty, tvCashback;
        TextView tvFreebieProduct, tvFreebieTotalPrice, tvFreebieQty, tvEcash;
        View freebieLayout, llCart, llCashback;
        Button ivOffer, btnEcash;

        public MyViewHolder(View view) {
            super(view);
            ivProduct = (ImageView) view.findViewById(R.id.ivProduct);
            ivCross = (ImageView) view.findViewById(R.id.ivCross);
            tvproduct = (TextView) view.findViewById(R.id.tvProduct);
            ivOffer = (Button) view.findViewById(R.id.iv_offer);
            tvTotalPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
            tvQty = (TextView) view.findViewById(R.id.tvQty);
            tvCashback = (TextView) view.findViewById(R.id.tv_cashback);
            llCart = view.findViewById(R.id.ll_cart);
            freebieLayout = view.findViewById(R.id.freebie_layout);

            tvFreebieProduct = (TextView) view.findViewById(R.id.tvFreebie);
            tvFreebieTotalPrice = (TextView) view.findViewById(R.id.tvFreebieTotalPrice);
            tvFreebieQty = (TextView) view.findViewById(R.id.tvFreebieQty);

            llCashback = view.findViewById(R.id.ll_cashback);
            btnEcash = (Button) view.findViewById(R.id.btn_ecash);
            tvEcash = (TextView) view.findViewById(R.id.tv_ecash);

        }
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
