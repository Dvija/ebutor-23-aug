package com.ebutor.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.BeatModel;

import java.util.ArrayList;

public class ManageBeatAdapter extends RecyclerView.Adapter<ManageBeatAdapter.MyViewHolder> {

    private ArrayList<BeatModel> arrBeats;
    private Context context;
    private OnItemClickListener listener;

    public ManageBeatAdapter(Context context, ArrayList arrBeats) {

        this.context = context;
        this.arrBeats = arrBeats;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_managebeat, parent, false);
        v.setTag(new ManageBeatAdapter.MyViewHolder(v));
        return new ManageBeatAdapter.MyViewHolder(v);
    }

    public void setData(ArrayList<BeatModel> arrBeats) {
        this.arrBeats = arrBeats;
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final BeatModel beatModel = arrBeats.get(position);
        holder.tvBeatName.setText(beatModel.getBeatName());
        holder.tvRM.setText(beatModel.getRmName());
        holder.tvDays.setText(beatModel.getDay());
        holder.tvNoOfOutlets.setText(beatModel.getTotalOutlets());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(beatModel);
            }
        });

        holder.ivMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMapCLick(beatModel);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (arrBeats != null)
            return arrBeats.size();
        else return 0;
    }

    public interface OnItemClickListener {
        public void onItemClick(BeatModel beatModel);

        public void onMapCLick(BeatModel beatModel);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvBeatName, tvRM, tvNoOfOutlets, tvDays;
        private ImageView ivMap;

        public MyViewHolder(View view) {
            super(view);

            tvBeatName = (TextView) view.findViewById(R.id.tv_beat_name);
            tvRM = (TextView) view.findViewById(R.id.tv_rm);
            tvNoOfOutlets = (TextView) view.findViewById(R.id.tv_no_of_outlets);
            tvDays = (TextView) view.findViewById(R.id.tv_days);
            ivMap = (ImageView) view.findViewById(R.id.iv_map);

        }

    }
}
