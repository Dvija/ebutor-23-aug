package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.StatusModel;

import java.util.ArrayList;

public class POStatusAdapter extends RecyclerView.Adapter<POStatusAdapter.MyViewHolder> {

    private ArrayList<StatusModel> arrStatus;
    private Context context;
    private OnItemClickListener listener;

    public POStatusAdapter(Context context, ArrayList<StatusModel> arrStatus) {
        this.context = context;
        this.arrStatus = arrStatus;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.po_status_single_item, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        StatusModel statusModel = arrStatus.get(position);
        if (statusModel != null) {
            holder.itemView.setTag(statusModel);
            holder.tvStatus.setText(statusModel.getName());
            holder.tvCount.setText(statusModel.getCount());
        }
    }

    @Override
    public int getItemCount() {
        if (arrStatus != null && arrStatus.size() > 0)
            return arrStatus.size();
        else
            return 0;
    }

    public interface OnItemClickListener {
        public void onItemClick(Object object);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvStatus, tvCount;

        public MyViewHolder(View view) {
            super(view);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getTag() instanceof StatusModel) {
                StatusModel statusModel = (StatusModel) v.getTag();
                if (statusModel != null)
                    listener.onItemClick(statusModel);
            }
        }
    }
}
