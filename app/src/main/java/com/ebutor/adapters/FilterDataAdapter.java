package com.ebutor.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.ebutor.R;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.FilterModel;

import java.util.ArrayList;

public class FilterDataAdapter extends RecyclerView.Adapter<FilterDataAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<FilterModel> arrFilters;
    private int count = 0, count1 = 0;
    private OnCheckBoxClickListener listener;

    public FilterDataAdapter(Context context, ArrayList<FilterModel> arrFilters) {
        this.context = context;
        this.arrFilters = arrFilters;
    }

    public void setClickListener(OnCheckBoxClickListener listener) {
        this.listener = listener;
    }

    @Override
    public FilterDataAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("Oncreate", ++count + "");
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_filter, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        FilterModel filterModel = arrFilters.get(position);
        Log.e("OnBind", ++count1 + "");
        if (filterModel != null) {
            if (filterModel.isStar()) {
                CustomerTyepModel customerTyepModel = DBHelper.getInstance().getStarColorName(filterModel.getFilterName());
                if (customerTyepModel != null) {
                    String colorName = customerTyepModel.getCustomerName();
                    String description = customerTyepModel.getDescription();
                    if (colorName != null && description != null && !TextUtils.isEmpty(colorName) && !TextUtils.isEmpty(description)) {
                        holder.cbFilter.setText(colorName);

                        ColorStateList colorStateList = new ColorStateList(
                                new int[][]{

                                        new int[]{-android.R.attr.state_enabled}, //disabled
                                        new int[]{android.R.attr.state_enabled} //enabled
                                },
                                new int[]{

                                        Color.parseColor("#ffffff"), //disabled
                                        Color.parseColor(description), //enabled

                                }
                        );

                        CompoundButtonCompat.setButtonTintList(holder.cbFilter, colorStateList);
                    }
                }
            } else
                holder.cbFilter.setText(filterModel.getFilterName());
            if (arrFilters.get(position).isChecked()) {
                holder.cbFilter.setChecked(true);
            } else {
                holder.cbFilter.setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return arrFilters.size();
    }

    public interface OnCheckBoxClickListener {
        public void OnCheckBoxClick(ArrayList<FilterModel> arrFilters);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        CheckBox cbFilter;

        public MyViewHolder(View view) {
            super(view);
            cbFilter = (CheckBox) view.findViewById(R.id.checkbox_filter);
            cbFilter.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                arrFilters.get(getPosition()).setIsChecked(true);
            } else {
                arrFilters.get(getPosition()).setIsChecked(false);
            }
            listener.OnCheckBoxClick(arrFilters);
        }
    }
}
