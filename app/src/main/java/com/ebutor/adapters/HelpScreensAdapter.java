package com.ebutor.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.ebutor.R;

/**
 * Created by 300024 on 3/16/2016.
 */
public class HelpScreensAdapter extends PagerAdapter {

    private int[] helpLayouts;
    private Button btnSkip;
    private Context context;

    public HelpScreensAdapter(Context context, int[] helpLayouts, Button btnSkip) {
        this.context = context;
        this.helpLayouts= helpLayouts;
        this.btnSkip = btnSkip;
    }

    @Override
    public int getCount() {
        return helpLayouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_help_five,null);
        ImageView ivHelp = (ImageView) view.findViewById(R.id.ivHelp);
        ivHelp.setImageResource(helpLayouts[position]);
        ((ViewPager) container).addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}