package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ebutor.R;

import java.util.ArrayList;

/**
 * Created by 300041 on 23-Sep-15.
 */
public class AreaAutoCompleteAdapter extends ArrayAdapter<String> {
    private final String MY_DEBUG_TAG = "ContactModelAdapter";
    private ArrayList<String> items;
    private ArrayList<String> itemsAll;
    private ArrayList<String> suggestions;
    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((String) (resultValue));
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (String ContactModel : itemsAll) {
                    if (ContactModel.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(ContactModel);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<String> filteredList = (ArrayList<String>) results.values;
            if (results.count > 0) {
                clear();
                    for (String c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };
    private int viewResourceId;

    public AreaAutoCompleteAdapter(Context context, int viewResourceId, ArrayList<String> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<String>) items.clone();
        this.suggestions = new ArrayList<String>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        String contactModel = items.get(position);
        if (contactModel != null) {
            TextView contactName = (TextView) v.findViewById(R.id.textView1);
            contactName.setText(contactModel);
        }
        v.setTag(contactModel);
        return v;
    }

    public void updateValues(ArrayList<String> items) {
        this.items = items;
        this.itemsAll = (ArrayList<String>) items.clone();
        this.suggestions = new ArrayList<String>();
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

}
