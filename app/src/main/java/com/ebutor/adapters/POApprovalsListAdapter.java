package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.OrdersModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class POApprovalsListAdapter extends RecyclerView.Adapter<POApprovalsListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<OrdersModel> arrPOs = new ArrayList<>();
    private OnItemClickListener listener;

    public POApprovalsListAdapter(Context context, ArrayList<OrdersModel> arrPOs) {
        this.context = context;
        this.arrPOs = arrPOs;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_po_approval_item, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    public void addItem(OrdersModel ordersModel) {
        if (arrPOs != null && !arrPOs.contains(ordersModel)) {
            this.arrPOs.add(ordersModel);
        }
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        this.arrPOs.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        OrdersModel poModel = arrPOs.get(position);
        holder.tvSupplierName.setText(poModel.getSupplierName());
        holder.tvPOId.setText(poModel.getOrderCode());
        String date = parseDate(poModel.getDate(), "dd-MM-yyyy");
        holder.tvDate.setText(context.getResources().getString(R.string.date) + date);
        holder.tvStatus.setText(context.getResources().getString(R.string.status) + " : " + poModel.getOrderStatus());
        holder.tvApprovalStatus.setText(context.getResources().getString(R.string.approval_status) + " : " + poModel.getApprovalStatus());
        String amt = "";
        double amount = 0.0;
        try {
            amount = Double.parseDouble(poModel.getTotalAmount());
            amt = String.format(Locale.CANADA, "%.2f", amount);

        } catch (Exception e) {
            e.printStackTrace();
            amt = "0.0";
        }
        holder.tvAmt.setText(context.getResources().getString(R.string.amt) + " : " + amt);

        /*switch (arrPOs.get(position).getOrderStatus()) {
            case "OPEN ORDER":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.pending));
                break;
            case "PROCESSING":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processing));
                break;
            case "CONFIRMED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processed));
                break;
            case "PICKED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.cancel));
                break;
            case "PACKED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.shipping));
                break;
            case "HOLD":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.return_requested));
                break;
            case "READY TO DISPATCH":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.complete));
                break;
            case "SHIPPED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.complete));
                break;
            case "DELIVERED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.shipping));
                break;
            case "CANCELLED BY EBUTOR":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.cancel));
                break;
            case "CANCELLED BY CUSTOMER":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processing));
                break;
            case "RETURN INITIATED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processing));
                break;
            case "RETURNED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processing));
                break;
            case "REFUND INITIATED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processing));
                break;
            case "REFUNDED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processing));
                break;
            case "CLOSED":
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.processing));
                break;
            default:
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.pending));
                break;

        }*/
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrPOs.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat(format, Locale.US);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvSupplierName, tvPOId, tvDate, tvStatus, tvApprovalStatus, tvAmt;

        public MyViewHolder(View view) {
            super(view);
            tvSupplierName = (TextView) view.findViewById(R.id.tv_supplier_name);
            tvPOId = (TextView) view.findViewById(R.id.tv_poId);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvApprovalStatus = (TextView) view.findViewById(R.id.tv_approval_status);
            tvAmt = (TextView) view.findViewById(R.id.tv_amt);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(this.getAdapterPosition());
        }
    }
}
