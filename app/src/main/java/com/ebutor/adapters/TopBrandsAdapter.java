package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.TopBrandsModel;
import com.ebutor.utils.fastscroll.RecyclerViewFastScroller;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by 300024 on 3/17/2016.
 */
public class TopBrandsAdapter extends RecyclerView.Adapter<TopBrandsAdapter.MyViewHolder> implements
        RecyclerViewFastScroller.BubbleTextGetter{

    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;

    Context context;
    ArrayList<TopBrandsModel> arrTopBrands;
    private OnItemClickListener listener;
    LayoutInflater inflater;


    public TopBrandsAdapter(Context context, ArrayList<TopBrandsModel> arrTopBrands) {
        this.context = context;
        this.arrTopBrands = arrTopBrands;
        inflater = LayoutInflater.from(context);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_loading)
                .showImageForEmptyUri(R.drawable.ic_not_found)
                .showImageOnFail(R.drawable.ic_not_found)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .displayer(new SimpleBitmapDisplayer())
                .build();
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        public void onItemClick(int pos);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        ImageView image;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tv_top_brand);
            image = (ImageView) view.findViewById(R.id.iv_top_brand);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(this.getPosition());
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = inflater.inflate(R.layout.row_top_brand, parent, false);
        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvTitle.setText(arrTopBrands.get(position).getTopBrandName());
        ArrayList<String> arrImages = arrTopBrands.get(position).getTopBrandImages();
        if(arrImages!=null && arrImages.size()>0){
            for(int j=0;j<arrImages.size();j++){
                String image = arrImages.get(j);
                if (image != null) {
                    ImageLoader.getInstance().displayImage(arrImages.get(j), holder.image, options, animateFirstListener);
                }
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrTopBrands.size();
    }

    @Override
    public String getTextToShowInBubble(int pos) {
        if (pos < 0 || pos >= arrTopBrands.size())
            return null;

        String name = arrTopBrands.get(pos).getTopBrandName();
        if (name == null || name.length() < 1)
            return null;

        return arrTopBrands.get(pos).getTopBrandName().substring(0, 1);
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}
