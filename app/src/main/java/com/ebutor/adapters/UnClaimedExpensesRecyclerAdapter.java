package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.NewExpenseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class UnClaimedExpensesRecyclerAdapter extends RecyclerView.Adapter<UnClaimedExpensesRecyclerAdapter.MyViewHolder> {

    private ArrayList<NewExpenseModel> expenseModelArrayList = new ArrayList<>();
    private OnItemClickListener listener;
    private Context context;
    private SparseBooleanArray selectedItems;

    public UnClaimedExpensesRecyclerAdapter(Context context, ArrayList<NewExpenseModel> expenseModelArrayList) {
        this.context = context;
        this.expenseModelArrayList = expenseModelArrayList;
        selectedItems = new SparseBooleanArray();
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_unclaimed_expense, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.itemView.setSelected(selectedItems.get(position, false));

        NewExpenseModel expenseModel = expenseModelArrayList.get(position);
        holder.tvExpenseType.setText(expenseModel.getExpenseType());
//        holder.tvAmount.setText("Amount : " + expenseModel.getAmount());
        if (expenseModel.getDesc().isEmpty()) {
            holder.tvExpenseDesc.setText(context.getString(R.string.no_description));
            holder.tvExpenseDesc.setAlpha((float) 0.5);
        } else {
            holder.tvExpenseDesc.setText(expenseModel.getDesc());
            holder.tvExpenseDesc.setAlpha(1);
        }

        holder.tvExpenseDesc.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Toast.makeText(context, "item..", Toast.LENGTH_SHORT).show();
                if (holder.ck == 0) {
                    holder.tvExpenseDesc.setVisibility(View.VISIBLE);
                    holder.tvExpenseDesc.setMaxLines(Integer.MAX_VALUE);

                    holder.ck = 1;
                } else {
                    holder.tvExpenseDesc.setVisibility(View.GONE);
                    //  holder.tvExpenseDesc.setMaxLines(2);

                    holder.ck = 0;
                }
            }
        });

        SpannableStringBuilder _actualAmount = new SpannableStringBuilder("" + expenseModel.getAmount());
        setTextStyles(_actualAmount, holder.tvAmount, 0, ContextCompat.getColor(context, R.color.primary));

        final SpannableStringBuilder _date = new SpannableStringBuilder(parseDate(expenseModel.getDate(), "dd/MM/yyyy"));
        setTextStyles(_date, holder.tvDate, 0, Color.parseColor("#000000"));

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return expenseModelArrayList.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat(format, Locale.US);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    private void setTextStyles(SpannableStringBuilder sb, TextView textView, int start, int color) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public void toggleSelection(int pos) {
        try {
            if (selectedItems.get(pos, false)) {
                selectedItems.delete(pos);
            } else {
                selectedItems.put(pos, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public ArrayList<NewExpenseModel> getSelectedItems() {
        ArrayList<NewExpenseModel> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(expenseModelArrayList.get(selectedItems.keyAt(i)));
        }
        return items;
    }

    public String getSelectedIds() {
        StringBuilder ids = new StringBuilder();
        ArrayList<NewExpenseModel> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(expenseModelArrayList.get(selectedItems.keyAt(i)));
            ids.append(expenseModelArrayList.get(selectedItems.keyAt(i)).getExpenseId());
            ids.append(",");
        }

        String result = ids.toString();
        if (result.endsWith(",")) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvExpenseType, tvDate, tvExpenseDesc, tvAmount;
        int ck = 0;

        public MyViewHolder(View view) {
            super(view);
            itemView.setClickable(true);
            tvExpenseType = (TextView) view.findViewById(R.id.tv_expense_type);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvExpenseDesc = (TextView) view.findViewById(R.id.tv_expense_desc);
            tvAmount = (TextView) view.findViewById(R.id.tv_amount);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onItemClick(this.getAdapterPosition());
        }
    }
}
