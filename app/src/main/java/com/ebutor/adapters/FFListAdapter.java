package com.ebutor.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.ebutor.R;
import com.ebutor.models.FilterDataModel;

import java.util.ArrayList;

public class FFListAdapter extends RecyclerView.Adapter<FFListAdapter.MyViewHolder> {

    private ArrayList<FilterDataModel> arrFilters;
    private String selected = "";
    private String childPos = "";
    private LayoutInflater inflater;
    private onClickListener listener;
    private Context context;

    public FFListAdapter(Context context, ArrayList<FilterDataModel> arrFilters) {
        this.context = context;
        this.arrFilters = arrFilters;
        inflater = LayoutInflater.from(context);
    }

    public void setListener(onClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = inflater.inflate(R.layout.row_filter, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.cbFilter.setText(arrFilters.get(position).getValue());
        holder.cbFilter.setTag(arrFilters.get(position));
        holder.itemView.setTag(arrFilters.get(position));
//        if (arrFilters.get(position).isChecked() || arrSelected.contains(arrFilters.get(position).getId())) {
//            holder.cbFilter.setChecked(true);
//            arrFilters.get(position).setChecked(true);
//        } else {
//            holder.cbFilter.setChecked(false);
//            arrFilters.get(position).setChecked(false);
//        }
    }

    @Override
    public int getItemCount() {
        return arrFilters.size();
    }

    public interface onClickListener {
        void onFFChecked(int position, Object object, String key, boolean isChecked);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CheckBox cbFilter;

        public MyViewHolder(View view) {
            super(view);
            cbFilter = (CheckBox) view.findViewById(R.id.checkbox_filter);
            cbFilter.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            boolean isChecked;
            if (((CheckBox) v).isChecked()) {
                isChecked = true;
                arrFilters.get(getAdapterPosition()).setChecked(true);
            } else {
                isChecked = false;
                arrFilters.get(getAdapterPosition()).setChecked(false);
            }
            listener.onFFChecked(getAdapterPosition(), v.getTag(), arrFilters.get(getAdapterPosition()).getKey(), isChecked);

        }
    }

}
