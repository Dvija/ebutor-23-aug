package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.FeatureData;
import com.ebutor.utils.FeatureCodes;

import java.util.ArrayList;

public class ProfileAdapter extends BaseAdapter {

    private ArrayList<FeatureData> features;
    LayoutInflater inflater;

    public ProfileAdapter(Context context, ArrayList<FeatureData> arrOptions) {
        this.features = arrOptions;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return features.size();
    }

    @Override
    public FeatureData getItem(int position) {
        if (features == null)
            return null;

        if (position > features.size())
            return null;

        return features.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FeatureData featureData = getItem(position);
        if (featureData != null) {
            View v = inflater.inflate(R.layout.row_profile, parent, false);
            TextView tvOption = (TextView) v.findViewById(R.id.tvOption);
            TextView tvValue = (TextView) v.findViewById(R.id.tvValue);
            tvOption.setText(featureData.getFeatureName());

            if (featureData.getFeatureCode().equalsIgnoreCase(FeatureCodes.ECASH_FEATURE_CODE)) {
                tvValue.setVisibility(View.VISIBLE);
                tvValue.setText("100");
            } else if (featureData.getFeatureCode().equalsIgnoreCase(FeatureCodes.CREDIT_LIMIT_FEATURE_CODE)) {
                tvValue.setVisibility(View.VISIBLE);
                tvValue.setText("100000");
            } else if (featureData.getFeatureCode().equalsIgnoreCase(FeatureCodes.PAYMENT_DUE_FEATURE_CODE)) {
                tvValue.setVisibility(View.VISIBLE);
                tvValue.setText("1000");
            } else {
                tvValue.setVisibility(View.GONE);
            }

            v.setTag(featureData);
            return v;
        } else {
            return convertView;//todo
        }

    }
}
