package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.SODashboardModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;


/**
 * Created by Srikanth Nama on 28-Sep-16.
 */

public class SODashboardRecyclerAdapter extends RecyclerView.Adapter<SODashboardRecyclerAdapter.ViewHolder> {
    private List<SODashboardModel> mDataSet;
    private Context mContext;
    private Random mRandom = new Random();

    public SODashboardRecyclerAdapter(Context context, ArrayList<SODashboardModel> DataSet) {
        mDataSet = new ArrayList<>();
        mDataSet.addAll(DataSet);
        mContext = context;
    }

    @Override
    public SODashboardRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new View
        View v = LayoutInflater.from(mContext).inflate(R.layout.row_so_dashboard_item_new, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        String value = mDataSet.get(position).getTotal();
        Double _value = 0.00;
        int per = 0;
        if (null != value) {
            try {

                _value = Double.parseDouble(value);
                holder.mLabel.setText(String.format(Locale.CANADA, "%.2f", _value));
                per = (int) Double.parseDouble(mDataSet.get(position).getPercentage());
            } catch (Exception e) {
                e.printStackTrace();
                holder.mLabel.setText(String.format(Locale.CANADA, "%.2f", 0.00));
            }

        } else {
            holder.mLabel.setText(String.format(Locale.CANADA, "%.2f", 0.00));
        }
        holder.mTextView.setText(mDataSet.get(position).getStatus());
        holder.mCount.setText(mDataSet.get(position).getCount());
        holder.mPercentage.setText(mDataSet.get(position).getPercentage());
        // Generate a random color
        int color = getRandomHSVColor();

        // Set a random color for TextView background
//        holder.mTextView.setBackgroundColor(getLighterColor(color));

        // Set a text color for TextView
        holder.mTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
//        holder.mLabel.setTextColor(getReverseColor(color));


        // Set a gradient background for LinearLayout
        // holder.mLinearLayout.setBackground(getGradientDrawable());

        // Emboss the TextView text
//        applyEmbossMaskFilter(holder.mTextView);
    }

    @Override
    public int getItemCount() {
        if (mDataSet != null)
            return mDataSet.size();
        else
            return 0;
    }

    // Custom method to apply emboss mask filter to TextView
    protected void applyEmbossMaskFilter(TextView tv) {
        EmbossMaskFilter embossFilter = new EmbossMaskFilter(
                new float[]{1f, 5f, 1f}, // direction of the light source
                0.8f, // ambient light between 0 to 1
                8, // specular highlights
                7f // blur before applying lighting
        );
        tv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        tv.getPaint().setMaskFilter(embossFilter);
    }

    // Custom method to generate random HSV color
    protected int getRandomHSVColor() {
        // Generate a random hue value between 0 to 360
        int hue = mRandom.nextInt(361);
        // We make the color depth full
        float saturation = 1.0f;
        // We make a full bright color
        float value = 1.0f;
        // We avoid color transparency
        int alpha = 255;
        // Finally, generate the color
        int color = Color.HSVToColor(alpha, new float[]{hue, saturation, value});
        // Return the color
        return color;
    }


    // Custom method to get a darker color
    protected int getDarkerColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] = 0.8f * hsv[2];
        return Color.HSVToColor(hsv);
    }

    // Custom method to get a lighter color
    protected int getLighterColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] = 0.2f + 0.8f * hsv[2];
        return Color.HSVToColor(hsv);
    }

    // Custom method to get reverse color
    protected int getReverseColor(int color) {
        float[] hsv = new float[3];
        Color.RGBToHSV(
                Color.red(color), // Red value
                Color.green(color), // Green value
                Color.blue(color), // Blue value
                hsv
        );
        hsv[0] = (hsv[0] + 180) % 360;
        return Color.HSVToColor(hsv);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView, mLabel, mCount, mPercentage;

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.status);
            mCount = (TextView) v.findViewById(R.id.count);
            mPercentage = (TextView) v.findViewById(R.id.percentage);
            mLabel = (TextView) v.findViewById(R.id.total);//todo change id
        }
    }
}
