package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;

import java.util.ArrayList;

public class FilterFFAdapter extends RecyclerView.Adapter<FilterFFAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<String> arrFilters;
    private LayoutInflater inflater;
    private onClickListener listener;

    public FilterFFAdapter(Context context, ArrayList<String> arrFilters) {
        this.context = context;
        this.arrFilters = arrFilters;
        inflater = LayoutInflater.from(context);
    }

    public void setListener(onClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = inflater.inflate(R.layout.row_ff_filter_name, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String name = arrFilters.get(position);
        name = name.replace("_", " ");
        holder.tvFilter.setText(name);
    }

    @Override
    public int getItemCount() {
        return arrFilters.size();
    }

    public interface onClickListener {
        void onItemClick(String key);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvFilter;

        public MyViewHolder(View view) {
            super(view);
            tvFilter = (TextView) view.findViewById(R.id.tv_group_name);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(arrFilters.get(this.getPosition()));
        }
    }
}
