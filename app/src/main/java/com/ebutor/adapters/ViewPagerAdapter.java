package com.ebutor.adapters;

/**
 * Created by Srikanth Nama on 26-Dec-16.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ebutor.fragments.PendingApprovalsFragment;
import com.ebutor.fragments.ReimbursementsFragment;
import com.ebutor.fragments.UnClaimedExpensesFragment;

import java.util.ArrayList;

/**
 * Created by Admin on 11-12-2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<String> mTabs;
    boolean isFeatureAllowed;

    public ViewPagerAdapter(Context context, FragmentManager fm, ArrayList<String> tabs, boolean isFeatureAllowed) {
        super(fm);
        this.mTabs = tabs;
        this.isFeatureAllowed = isFeatureAllowed;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (isFeatureAllowed) {
            switch (position) {
            /*case 0:
                fragment = new AdvancesFragment();
                break;*/
                case 0:
                    fragment = PendingApprovalsFragment.newInstance(position);
                    break;
                case 1:
                    fragment = PendingApprovalsFragment.newInstance(position);
                    break;
                case 2:
                    fragment = new ReimbursementsFragment();
                    break;
                case 3:
                    fragment = new UnClaimedExpensesFragment();
                    break;
                default:
                    fragment = new ReimbursementsFragment();
                    break;
            }

            return fragment;
        } else {
            switch (position) {
            /*case 0:
                fragment = new AdvancesFragment();
                break;*/
                case 0:
                    fragment = new ReimbursementsFragment();
                    break;
                case 1:
                    fragment = new UnClaimedExpensesFragment();
                    break;
                default:
                    fragment = new ReimbursementsFragment();
                    break;
            }

            return fragment;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position);
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

}
