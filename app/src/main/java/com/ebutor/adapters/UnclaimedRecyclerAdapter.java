package com.ebutor.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.NewExpenseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by 480108 on 6/14/2017.
 */

public class UnclaimedRecyclerAdapter extends RecyclerView.Adapter<UnclaimedRecyclerAdapter.MyViewHolder> {

    ArrayList<NewExpenseModel> ckitems = new ArrayList<>();
    Dialog dialog;
    int c = 0, ck = 0;
    int flag = 0;
    private ArrayList<NewExpenseModel> expenseModelArrayList = new ArrayList<>();
    private OnItemClickListener listener;
    private Context context;
    private SparseBooleanArray selectedItems;

    public UnclaimedRecyclerAdapter(Context context, ArrayList<NewExpenseModel> expenseModelArrayList, int flag) {
        this.context = context;
        this.expenseModelArrayList = expenseModelArrayList;
        selectedItems = new SparseBooleanArray();
        this.flag = flag;


    }


    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_item_recyclerview2, parent, false);
        v.setTag(new MyViewHolder(v));

        return new MyViewHolder(v);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        int len;
        String attachids;
        String[] coma;


        //   holder.itemView.setSelected(selectedItems.get(position, false));
        NewExpenseModel newExpenseModel = expenseModelArrayList.get(position);
        holder.tvExpenseType.setText(newExpenseModel.getExpenseType());
        holder.tvAmount.setText(newExpenseModel.getAmount());

        if (newExpenseModel.getDesc().isEmpty()) {
            holder.tvExpenseDesc.setText(context.getString(R.string.no_description));
            holder.tvExpenseDesc.setAlpha((float)0.5);

        } else {

            holder.tvExpenseDesc.setText(newExpenseModel.getDesc());
            holder.tvExpenseDesc.setAlpha(1);
        }

        holder.mixid.setVisibility(View.VISIBLE);
        holder.mixid2.setVisibility(View.VISIBLE);
        holder.tvDate.setText(parseDate(newExpenseModel.getDate(), "dd-MM-yyyy"));
        // holder.tvDate.setText(newExpenseModel.getDate());
        //   holder.tvDate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN,Utils.DATE_STD_PATTERN,newExpenseModel.getDate()));
        attachids = newExpenseModel.getAttachments();
        if (!attachids.isEmpty() || attachids != null) {
            coma = TextUtils.split(attachids, ",");
            len = coma.length;
            holder.tvcount.setText(len + "");
        } else {
            holder.tvcount.setText(0 + "");
        }
        holder.tvExpenseDesc.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Toast.makeText(context, "item..", Toast.LENGTH_SHORT).show();
                if (ck == 0) {
                    holder.tvExpenseDesc.setVisibility(View.VISIBLE);
                    holder.tvExpenseDesc.setMaxLines(Integer.MAX_VALUE);

                    ck = 1;
                } else {
                    holder.tvExpenseDesc.setVisibility(View.GONE);
                    //  holder.tvExpenseDesc.setMaxLines(2);

                    ck = 0;
                }
            }
        });

        holder.rec_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (listener != null) {
                    listener.onItemClick(expenseModelArrayList.get(position).getAttachments());
                }

            }
        });
        holder.checkBoxholder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.checkBoxholder.isChecked()) {
                    c++;
                    ckitems.add(expenseModelArrayList.get(position));
                    Log.e("ckitems", ckitems + " ");
                }


            }
        });


        holder.rec_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onItemDel(expenseModelArrayList.get(position).getExpenseId());

            }
        });


    }

    public ArrayList<NewExpenseModel> getSelectedItems() {


        return ckitems;
    }


    @Override
    public int getItemCount() {
        return expenseModelArrayList.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public interface OnItemClickListener {
        void onItemClick(String getattachids);

        void onItemDel(String ids);


        void onItemClick(int adapterPosition);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvExpenseType, tvDate, tvExpenseDesc, tvAmount, tvrefid, tvcount;
        ImageView rec_img, rec_del;
        CheckBox checkBoxholder;
        LinearLayout mixid, mixid2, mixid3, mixid4;
        RelativeLayout relativeLayout;

        public MyViewHolder(View view) {
            super(view);
            itemView.setClickable(true);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.badge_layout1);
            mixid = (LinearLayout) view.findViewById(R.id.mix_id);
            //   mixid4 = (LinearLayout) view.findViewById(R.id.mixid4);
            //  mixid3 = (LinearLayout) view.findViewById(R.id.mixid3);
            mixid2 = (LinearLayout) view.findViewById(R.id.mix_id2);
            tvExpenseType = (TextView) view.findViewById(R.id.tv_expense_type2);
            tvDate = (TextView) view.findViewById(R.id.tv_date2);
            tvExpenseDesc = (TextView) view.findViewById(R.id.tv_desc2);
            tvAmount = (TextView) view.findViewById(R.id.tv_amount2);
            //   tvrefid = (TextView) view.findViewById(R.id.tv_ref_id2);
            rec_img = (ImageView) view.findViewById(R.id.rec_img2);
            rec_del = (ImageView) view.findViewById(R.id.rec_del2);
            tvcount = (TextView) view.findViewById(R.id.rec_bage_count2);
            checkBoxholder = (CheckBox) view.findViewById(R.id.rowcheck2);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onItemClick(this.getAdapterPosition());
        }
    }

}

