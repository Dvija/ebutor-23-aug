package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.ExpenseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class AllExpensesAdapter extends RecyclerView.Adapter<AllExpensesAdapter.MyViewHolder> {

    private ArrayList<ExpenseModel> arrExpenses;
    private OnItemClickListener listener;
    private Context context;

    public AllExpensesAdapter(Context context, ArrayList<ExpenseModel> arrExpenses) {
        this.context = context;
        this.arrExpenses = arrExpenses;
    }


    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_all_expense, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ExpenseModel expenseModel = arrExpenses.get(position);
        if (expenseModel != null) {
            holder.itemView.setTag(expenseModel);
            holder.tvExpenseCode.setText(expenseModel.getExpenseCode());
            holder.tvExpenseDesc.setText(expenseModel.getExpenseSubject());

            SpannableStringBuilder _actualAmount = new SpannableStringBuilder("Amount :" + expenseModel.getActualAmount());
            setTextStyles(_actualAmount, holder.tvAmount, 8, ContextCompat.getColor(context, R.color.primary));

            SpannableStringBuilder _date = new SpannableStringBuilder(parseDate(expenseModel.getExpenseDate(), "dd/MM/yyyy"));
            setTextStyles(_date, holder.tvDate, 0, Color.parseColor("#000000"));

        }
    }

    @Override
    public int getItemCount() {
        if (arrExpenses != null && arrExpenses.size() > 0)
            return arrExpenses.size();
        else
            return 0;
    }

    private void setTextStyles(SpannableStringBuilder sb, TextView textView, int start, int color) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat(format, Locale.US);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public interface OnItemClickListener {
        void onItemClick(Object object);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvExpenseCode, tvDate, tvExpenseDesc, tvAmount;

        public MyViewHolder(View view) {
            super(view);
            itemView.setClickable(true);
            tvExpenseCode = (TextView) view.findViewById(R.id.tv_expense_code);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvExpenseDesc = (TextView) view.findViewById(R.id.tv_expense_desc);
            tvAmount = (TextView) view.findViewById(R.id.tv_amount);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onItemClick(v.getTag());
        }
    }
}
