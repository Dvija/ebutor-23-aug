package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.DashboardModel;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class GridAdapter extends BaseAdapter {

    private Context mContext;
    private Random mRandom = new Random();
    private final ArrayList<DashboardModel> mMobileValues;

    public GridAdapter(Context context, ArrayList<DashboardModel> mobileValues) {
        mContext = context;
        mMobileValues = mobileValues;
    }

    @Override
    public int getCount() {
        return mMobileValues.size();
    }

    @Override
    public DashboardModel getItem(int position) {
        return mMobileValues.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_dashboard_item, parent, false);
            holder = new ViewHolder();

            holder.text = (TextView) convertView.findViewById(R.id.textView3);
            holder.text2 = (TextView) convertView.findViewById(R.id.textView4);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String value = mMobileValues.get(position).getValue();
        if(null != value){
            try{
                Double _value = Double.parseDouble(value);
                holder.text.setText(String.format(Locale.CANADA, "%.2f", _value));
            }catch (Exception e){
                e.printStackTrace();
                holder.text.setText(String.format(Locale.CANADA, "%.2f", 0.00));
            }

        }else{
            holder.text.setText(String.format(Locale.CANADA, "%.2f", 0.00));
        }
        holder.text.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        holder.text2.setText(mMobileValues.get(position).getKey());
        return convertView;
    }

    static class ViewHolder {
        TextView text, text2;
    }
    // Custom method to get reverse color
    protected int getReverseColor(int color) {
        float[] hsv = new float[3];
        Color.RGBToHSV(
                Color.red(color), // Red value
                Color.green(color), // Green value
                Color.blue(color), // Blue value
                hsv
        );
        hsv[0] = (hsv[0] + 180) % 360;
        return Color.HSVToColor(hsv);
    }
    // Custom method to generate random HSV color
    protected int getRandomHSVColor() {
        // Generate a random hue value between 0 to 360
        int hue = mRandom.nextInt(361);
        // We make the color depth full
        float saturation = 1.0f;
        // We make a full bright color
        float value = 1.0f;
        // We avoid color transparency
        int alpha = 255;
        // Finally, generate the color
        int color = Color.HSVToColor(alpha, new float[]{hue, saturation, value});
        // Return the color
        return color;
    }
}
