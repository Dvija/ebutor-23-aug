package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.OrdersModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class OrderDetailAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    private ArrayList<OrdersModel> arrOrders = null;

    public OrderDetailAdapter(Context context, ArrayList<OrdersModel> arrOrders) {
        this.context = context;
        this.arrOrders = arrOrders;
        inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();
    }

    @Override
    public int getCount() {
        return arrOrders.size();
    }

    @Override
    public Object getItem(int position) {
        return arrOrders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = inflater.inflate(R.layout.row_order_details, parent, false);
            holder = new ViewHolder();

            holder.ivProduct = (ImageView) view.findViewById(R.id.ivProduct);
            holder.tvProduct = (TextView) view.findViewById(R.id.tvProduct);
            holder.tvQty = (TextView) view.findViewById(R.id.tvQty);
            holder.btnCancel = (Button) view.findViewById(R.id.btn_cancel);
            holder.expandContentLayout = (LinearLayout) view.findViewById(R.id.ll_expand_content);
            holder.toggleButton = (LinearLayout) view.findViewById(R.id.expandable_toggle_button);
            holder.tvTotalPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
            holder.tvStatus = (TextView) view.findViewById(R.id.tvStatus);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final OrdersModel model = arrOrders.get(position);
        holder.tvProduct.setText(model.getName());
        holder.tvQty.setText(model.getTotalQuantity());
        String status = model.getOrderStatus();
        holder.tvStatus.setText(status);
        try {
            if (status != null && status.length() > 0) {
                holder.toggleButton.setEnabled(false);
                switch (status) {
                    case "DELIVERED":
                        holder.btnCancel.setText(context.getResources().getString(R.string.return_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.complete));
                        break;
                    case "CANCELLED BY EBUTOR":
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                        break;
                    case "CANCELLED BY CUSTOMER":
                        holder.toggleButton.setEnabled(false);
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                        break;
                    case "RETURN INITIATED":
                        holder.toggleButton.setEnabled(false);
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                        break;
                    case "RETURNED":
                        holder.toggleButton.setEnabled(false);
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                        break;
                    case "REFUND INITIATED":
                        holder.toggleButton.setEnabled(false);
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                        break;
                    case "REFUNDED":
                        holder.toggleButton.setEnabled(false);
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                        break;
                    case "CLOSED":
                        holder.toggleButton.setEnabled(false);
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                        break;
                    case "OPEN ORDER":
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.shipping));
                        break;
                    case "PROCESSING":
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.pending));
                        break;
                    case "CONFIRMED":
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                        break;
                    case "PICKED":
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processed));
                        break;
                    case "PACKED":
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processed));
                        break;
                    case "HOLD":
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processed));
                        break;
                    case "READY TO DISPATCH":
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processed));
                        break;
                    case "SHIPPED":
                        holder.toggleButton.setEnabled(false);
                        holder.toggleButton.setOnClickListener(null);
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processed));
                        break;
                    default:
                        holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
                        holder.tvStatus.setTextColor(context.getResources().getColor(R.color.pending));
                        break;
                }

            } else {
                holder.btnCancel.setText(context.getResources().getString(R.string.cancel_product));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String price = model.getTotalPrice();
            double value = Double.parseDouble(price);
            holder.tvTotalPrice.setText(String.format(Locale.CANADA, "%.2f", value));
        } catch (Exception e) {
            e.printStackTrace();
            holder.tvTotalPrice.setText("0.00");
        }

        ImageLoader.getInstance().displayImage(model.getImage(), holder.ivProduct, options, animateFirstListener);
        return view;
    }

    private class ViewHolder {
        public LinearLayout expandContentLayout, toggleButton;
        ImageView ivProduct;
        Button btnCancel;
        TextView tvProduct, tvQty, tvTotalPrice, tvStatus;
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
