package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.OrdersModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<OrdersModel> arrOrders = new ArrayList<>();
    private OnItemClickListener listener;

    public OrdersListAdapter(Context context, ArrayList<OrdersModel> arrOrders) {
        this.context = context;
        this.arrOrders = arrOrders;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.orders_list_single_item, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvOrderId.setText(context.getResources().getString(R.string.order_no) + "\n" + arrOrders.get(position).getOrderCode());
        String date = parseDate(arrOrders.get(position).getDate(), "dd-MM-yyyy");
        holder.tvDate.setText(context.getResources().getString(R.string.date) + date);
        holder.tvStatus.setText(arrOrders.get(position).getOrderStatus());

        switch (arrOrders.get(position).getOrderStatus()) {
            case "OPEN ORDER":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.pending));
                break;
            case "PROCESSING":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "CONFIRMED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processed));
                break;
            case "PICKED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.cancel));
                break;
            case "PACKED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.shipping));
                break;
            case "HOLD":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                break;
            case "READY TO DISPATCH":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.complete));
                break;
            case "SHIPPED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.complete));
                break;
            case "DELIVERED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.shipping));
                break;
            case "CANCELLED BY EBUTOR":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.cancel));
                break;
            case "CANCELLED BY CUSTOMER":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "RETURN INITIATED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "RETURNED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "REFUND INITIATED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "REFUNDED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "CLOSED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            default:
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.pending));
                break;

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrOrders.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public void addItem(OrdersModel ordersModel) {
        if (arrOrders != null && !arrOrders.contains(ordersModel)) {
            this.arrOrders.add(ordersModel);
        }
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        this.arrOrders.clear();
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvOrderId, tvDate, tvStatus;

        public MyViewHolder(View view) {
            super(view);
            tvOrderId = (TextView) view.findViewById(R.id.tv_orderId);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(this.getPosition());
        }
    }
}
