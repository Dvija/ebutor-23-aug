package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.RetailersModel;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Miguel Catalan Bañuls
 */
public class SearchAdapter extends BaseAdapter implements Filterable {

    LayoutInflater inflater;
    private ArrayList<RetailersModel> data;
    private ArrayList<RetailersModel> typeAheadData;
    private Drawable suggestionIcon;
    private String searchText = "";

    OnBuyerSelectedListener onBuyerSelectedListener;

    public interface OnBuyerSelectedListener {
        public void OnBuyerSelected(RetailersModel retailersModel);
    }

    public void setOnBuyerSelectedListener(Fragment fragment) {
        try {
            onBuyerSelectedListener = (OnBuyerSelectedListener) fragment;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public SearchAdapter(Context context, ArrayList<RetailersModel> typeAheadData) {
        inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
        this.typeAheadData = typeAheadData;
        notifyDataSetChanged();
    }

    public SearchAdapter(Context context, ArrayList<RetailersModel> typeAheadData, Drawable suggestionIcon) {
        inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
        this.typeAheadData = typeAheadData;
        this.suggestionIcon = suggestionIcon;
    }

    public static CharSequence highlight(String search, String originalText) {
        // ignore case and accents
        // the same thing should have been done for the search text
        String normalizedText = Normalizer.normalize(originalText, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();

        int start = normalizedText.indexOf(search);
        if (start < 0) {
            // not found, nothing to to
            return originalText;
        } else {
            // highlight each appearance in the original text
            // while searching in normalized text
            Spannable highlighted = new SpannableString(originalText);
            while (start >= 0) {
                int spanStart = Math.min(start, originalText.length());
                int spanEnd = Math.min(start + search.length(), originalText.length());

                highlighted.setSpan(new ForegroundColorSpan(Color.BLUE), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                start = normalizedText.indexOf(search, spanEnd);
            }

            return highlighted;
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (!TextUtils.isEmpty(constraint)) {
                    // Retrieve the autocomplete results.
                    List<RetailersModel> searchData = new ArrayList<>();
                    searchText = constraint.toString();

                    for (RetailersModel model : typeAheadData) {
                        String str = model.getCompany();
                        String mobileNumber = model.getTelephone();
                        String address = model.getAddress1();
                        if (str.toLowerCase().contains(constraint.toString().toLowerCase())
                                || mobileNumber.toLowerCase().contains(constraint.toString().toLowerCase())
                                || address.toLowerCase().contains(constraint.toString().toLowerCase())) {

                            searchData.add(model);
                        }
                    }

                    // Assign the data to the FilterResults
                    filterResults.values = searchData;
                    filterResults.count = searchData.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.values != null) {
                    data = (ArrayList<RetailersModel>) results.values;
                    notifyDataSetChanged();
                }
            }
        };
        return filter;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_retailer_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        RetailersModel model = (RetailersModel) getItem(position);
        String currentListData = model.getCompany();
        if (!TextUtils.isEmpty(searchText)) {
            CharSequence name = highlight(searchText, currentListData);
            mViewHolder.retailerShopName.setText(name);

            CharSequence mobileNumber = highlight(searchText, model.getTelephone());
            mViewHolder.retailerMobile.setText(mobileNumber);

            CharSequence address = highlight(searchText, model.getAddress1());
            mViewHolder.retailerLocation.setText(address);
        } else {

        }


        return convertView;
    }

    private class MyViewHolder {
        TextView retailerShopName;
        TextView retailerLocation, retailerMobile;

        public MyViewHolder(View view) {
            retailerShopName = (TextView) view.findViewById(R.id.tv_retailer_shop_name);
            retailerLocation = (TextView) view.findViewById(R.id.tv_retailer_location);
            retailerMobile = (TextView) view.findViewById(R.id.tv_mobile_number);
        }
    }

    private class ModelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                constraint = constraint.toString().toLowerCase();
                ArrayList<RetailersModel> filteredItems = new ArrayList<RetailersModel>();

                for (int i = 0, l = data.size(); i < l; i++) {
                    RetailersModel m = data.get(i);
                    if (m.getCompany().toLowerCase().contains(constraint))
                        filteredItems.add(m);
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                result.values = data;
                result.count = data.size();

            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            typeAheadData.clear();
            typeAheadData.addAll((ArrayList<RetailersModel>) results.values);
            notifyDataSetChanged();
        }
    }
}