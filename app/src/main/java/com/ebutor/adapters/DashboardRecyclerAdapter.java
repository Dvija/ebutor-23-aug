package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.DashboardModel;
import com.ebutor.models.DashboardModelNew;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by Srikanth Nama on 28-Sep-16.
 */

public class DashboardRecyclerAdapter extends RecyclerView.Adapter<DashboardRecyclerAdapter.ViewHolder> {
    private List<DashboardModelNew> mDataSet;
    private Context mContext;
    private Random mRandom = new Random();

    public DashboardRecyclerAdapter(Context context, ArrayList<DashboardModelNew> DataSet) {
        mDataSet = new ArrayList<>();
        mDataSet.addAll(DataSet);
        mContext = context;
    }

    @Override
    public DashboardRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new View
        View v = LayoutInflater.from(mContext).inflate(R.layout.row_dashboard_item_new, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        DashboardModelNew dashboardModelNew = mDataSet.get(position);
        if (dashboardModelNew != null) {
            ArrayList<DashboardModel> arrData = dashboardModelNew.getArrData();

            if (arrData != null && arrData.size() > 0)
                if (arrData.size() > 1) {
                    holder.mKey1.setVisibility(View.VISIBLE);
                    holder.mValue2.setVisibility(View.VISIBLE);
                    DashboardModel dashboardModel1 = arrData.get(0);
                    if (dashboardModel1 != null) {
                        holder.mValue1.setText(dashboardModel1.getValue());
                        holder.mKey1.setText(dashboardModel1.getKey());
                        holder.tvPer1.setText(dashboardModel1.getPer());
                    }
                    DashboardModel dashboardModel2 = arrData.get(1);
                    if (dashboardModel2 != null) {
                        holder.mValue2.setText(dashboardModel2.getValue());
                        holder.mKey2.setText(dashboardModel2.getKey());
                        holder.tvPer2.setText(dashboardModel2.getPer());
                    }
                } else {
                    holder.mKey1.setVisibility(View.GONE);
                    holder.mValue2.setVisibility(View.GONE);
                    holder.tvPer2.setVisibility(View.GONE);
                    DashboardModel dashboardModel1 = arrData.get(0);
                    if (dashboardModel1 != null) {
                        holder.mValue1.setText(dashboardModel1.getValue());
                        holder.mKey2.setText(dashboardModel1.getKey());
                        holder.tvPer1.setText(dashboardModel1.getPer());
                    }
                }
        }

    }

    @Override
    public int getItemCount() {
        if (mDataSet != null)
            return mDataSet.size();
        else
            return 0;
    }

    // Custom method to apply emboss mask filter to TextView
    protected void applyEmbossMaskFilter(TextView tv) {
        EmbossMaskFilter embossFilter = new EmbossMaskFilter(
                new float[]{1f, 5f, 1f}, // direction of the light source
                0.8f, // ambient light between 0 to 1
                8, // specular highlights
                7f // blur before applying lighting
        );
        tv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        tv.getPaint().setMaskFilter(embossFilter);
    }

    // Custom method to generate random HSV color
    protected int getRandomHSVColor() {
        // Generate a random hue value between 0 to 360
        int hue = mRandom.nextInt(361);
        // We make the color depth full
        float saturation = 1.0f;
        // We make a full bright color
        float value = 1.0f;
        // We avoid color transparency
        int alpha = 255;
        // Finally, generate the color
        int color = Color.HSVToColor(alpha, new float[]{hue, saturation, value});
        // Return the color
        return color;
    }


    // Custom method to get a darker color
    protected int getDarkerColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] = 0.8f * hsv[2];
        return Color.HSVToColor(hsv);
    }

    // Custom method to get a lighter color
    protected int getLighterColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] = 0.2f + 0.8f * hsv[2];
        return Color.HSVToColor(hsv);
    }

    // Custom method to get reverse color
    protected int getReverseColor(int color) {
        float[] hsv = new float[3];
        Color.RGBToHSV(
                Color.red(color), // Red value
                Color.green(color), // Green value
                Color.blue(color), // Blue value
                hsv
        );
        hsv[0] = (hsv[0] + 180) % 360;
        return Color.HSVToColor(hsv);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mValue1, mValue2, mKey1, mKey2, tvPer1, tvPer2;
        ProgressBar progressBar;

        public ViewHolder(View v) {
            super(v);
            mValue1 = (TextView) v.findViewById(R.id.tv_value_1);
            mValue2 = (TextView) v.findViewById(R.id.tv_value_2);
            mKey1 = (TextView) v.findViewById(R.id.tv_key_1);
            mKey2 = (TextView) v.findViewById(R.id.tv_key_2);
            tvPer1 = (TextView) v.findViewById(R.id.tv_per_1);
            tvPer2 = (TextView) v.findViewById(R.id.tv_per_2);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }
}
