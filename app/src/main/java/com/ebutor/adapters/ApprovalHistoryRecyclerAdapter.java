package com.ebutor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.ApprovalHistoryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ApprovalHistoryRecyclerAdapter extends RecyclerView.Adapter<ApprovalHistoryRecyclerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ApprovalHistoryModel> historyModelArrayList = new ArrayList<>();

    public ApprovalHistoryRecyclerAdapter(Context context, ArrayList<ApprovalHistoryModel> expenseModelArrayList) {
        this.context = context;
        this.historyModelArrayList = expenseModelArrayList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_approval_history, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ApprovalHistoryModel expenseModel = historyModelArrayList.get(position);
        holder.tvName.setText(expenseModel.getName());
        holder.tvDate.setText(parseDate(expenseModel.getCreated_at(), "dd/MM/yyyy HH:mm"));
        holder.tvStatus.setText(context.getString(R.string.status) + " : " + expenseModel.getMaster_lookup_name());
        holder.tvComment.setText(expenseModel.getAwf_comment());

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return historyModelArrayList.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    private void setTextStyles(SpannableStringBuilder sb, TextView textView, int start, int color) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvDate, tvStatus, tvComment;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvComment = (TextView) view.findViewById(R.id.tv_comment);
        }

    }
}
