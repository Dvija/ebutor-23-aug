package com.ebutor.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.InventoryModel;
import com.ebutor.models.RetailersModel;

import java.util.ArrayList;

public class InventoryRecyclerAdapter extends RecyclerView.Adapter<InventoryRecyclerAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ArrayList<InventoryModel> mItems;

    public InventoryRecyclerAdapter(Context context, ArrayList<InventoryModel> items) {
        mInflater = LayoutInflater.from(context);

        mItems = new ArrayList<>();
        mItems.addAll(items);


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.row_inventory_item, parent, false));
    }

//    public void setOnClickListener(Fragment fragment) {
//        this.dealItemClickListener = (DealItemClickListener) fragment;
//    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        InventoryModel model = mItems.get(position);
        holder.itemView.setTag(model);

        holder.tvKey.setText(model.getKey() == null ? "" : model.getKey());
        holder.tvValue.setText(model.getValue() == null ? "" : model.getValue());

//        holder.tvVolumeClass.setText(model.getVolumeClass() == null ? "":model.getVolumeClass());
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvKey, tvValue;

        public ViewHolder(View view) {
            super(view);

            tvKey = (TextView) view.findViewById(R.id.key);
            tvValue = (TextView) view.findViewById(R.id.value);

        }
    }

}
