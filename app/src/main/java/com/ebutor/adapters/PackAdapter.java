package com.ebutor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ebutor.R;
import com.ebutor.fragments.ProductDetailFragment;
import com.ebutor.models.SKUPacksModel;
import java.util.ArrayList;

public class PackAdapter extends BaseAdapter {

    private Context context;
    private int[] arrPack = new int[]{4, 12};
    private int[] arrPrice = new int[]{790, 6040};
    private int[] arrMargin = new int[]{12, 15};
    private int[] arrQty = new int[]{20, 10};
    private onPackItemClickListener listener;
    private ArrayList<SKUPacksModel> skuPacksModelArrayList;
    LayoutInflater inflater;

    public PackAdapter(Context context, ArrayList<SKUPacksModel> skuPacksModelArrayList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.skuPacksModelArrayList = skuPacksModelArrayList;
    }

    public interface onPackItemClickListener {
        public void onPackItemAdd(SKUPacksModel model, Object tag, int position, TextView tv);

        public void onPackItemDelete(SKUPacksModel model, Object tag, int position, TextView tv);
    }

    public void initializeListener(ProductDetailFragment fragment) {
        listener = (onPackItemClickListener) fragment;
    }

    public void refresh(ArrayList<SKUPacksModel> skuPacksModelArrayList) {
        this.skuPacksModelArrayList = skuPacksModelArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return skuPacksModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        if (skuPacksModelArrayList != null && skuPacksModelArrayList.size() > 0) {
            return skuPacksModelArrayList.get(position);
        } else {
            return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_pack, parent, false);
            holder = new ViewHolder();
            holder.tvPackOf = (TextView) convertView.findViewById(R.id.tvPackOf);
            holder.tvPackPrice = (TextView) convertView.findViewById(R.id.tvPackPrice);
            holder.tvUnitPrice = (TextView) convertView.findViewById(R.id.tvUnitPrice);
            holder.tvMargin = (TextView) convertView.findViewById(R.id.tvMargin);
            holder.tvQTY = (TextView) convertView.findViewById(R.id.tvQTY);
            holder.ivMinus = (ImageView) convertView.findViewById(R.id.ivMinus);
            holder.ivPlus = (ImageView) convertView.findViewById(R.id.ivPlus);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SKUPacksModel skuPacksModel = null;
        if (skuPacksModelArrayList != null && skuPacksModelArrayList.size() > 0) {
            skuPacksModel = skuPacksModelArrayList.get(position);

            holder.tvPackOf.setText(skuPacksModel.getPackSize() + "");
            holder.tvPackPrice.setText(skuPacksModel.getPackPrice() + "");
            holder.tvUnitPrice.setText(skuPacksModel.getUnitPrice() + "");
            holder.tvMargin.setText(skuPacksModel.getMargin() + "%");
            holder.tvQTY.setText(skuPacksModel.getQty() + "");
            holder.ivMinus.setTag(position);
            holder.ivPlus.setTag(position);

        }
        final SKUPacksModel finalSkuPacksModel = skuPacksModel;
        holder.ivMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPackItemDelete(finalSkuPacksModel, v.getTag(), position, holder.tvQTY);
            }
        });
        holder.ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPackItemAdd(finalSkuPacksModel, v.getTag(), position, holder.tvQTY);
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView tvPackOf, tvPackPrice,tvUnitPrice,tvMargin, tvQTY;
        ImageView ivMinus, ivPlus;
    }
}
