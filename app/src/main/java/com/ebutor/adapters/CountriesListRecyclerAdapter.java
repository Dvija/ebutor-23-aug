package com.ebutor.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import com.ebutor.R;
import com.ebutor.models.RetailersModel;
import com.ebutor.utils.fastscroll.RecyclerViewFastScroller;

import java.util.ArrayList;
import java.util.List;

public class CountriesListRecyclerAdapter extends RecyclerView.Adapter<CountriesListRecyclerAdapter.ViewHolder> implements Filterable,
        RecyclerViewFastScroller.BubbleTextGetter {

    private LayoutInflater mInflater;
    private ArrayList<RetailersModel> mItems;
    private List<RetailersModel> filteredModelItemsArray;
    private Context mContext;
    //    private DealItemClickListener dealItemClickListener;
    private View.OnClickListener mItemViewOnClickListener;
    private ModelFilter filter;
    private OnItemClickListener onItemClickListener;

    public CountriesListRecyclerAdapter(Context context, ArrayList<RetailersModel> items) {
        mInflater = LayoutInflater.from(context);

        mItems = new ArrayList<>();
        mItems.addAll(items);
        this.filteredModelItemsArray = new ArrayList<RetailersModel>();
        filteredModelItemsArray.addAll(mItems);

        mContext = context;

        getFilter();

        mItemViewOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemViewClick(v);
            }
        };
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModelFilter();
        }
        return filter;
    }

    @Override
    public int getItemCount() {
        return filteredModelItemsArray.size();
    }

//    private void onItemViewClick(View v) {
//        if (dealItemClickListener != null) dealItemClickListener.OnItemClickListener(v.getTag());
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.row_retailer_item, parent, false));
    }

//    public void setOnClickListener(Fragment fragment) {
//        this.dealItemClickListener = (DealItemClickListener) fragment;
//    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        RetailersModel model = filteredModelItemsArray.get(position);
        holder.itemView.setTag(model);

        holder.retailerShopName.setText(model.getCompany() == null ? "" : model.getCompany());
        holder.retailerName.setText(model.getFirstName() == null ? "":model.getFirstName());

        holder.retailerLocation.setText(model.getAddress1() == null ? "":model.getAddress1());
        holder.retailerMobile.setText(model.getTelephone() == null ? "":model.getTelephone());

//        holder.tvVolumeClass.setText(model.getVolumeClass() == null ? "":model.getVolumeClass());
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    private void handleOnClick(int id, View v) {
        if (v != null && onItemClickListener != null) {
            onItemClickListener.onItemClick(id,v.getTag());
        }
    }

    @Override
    public String getTextToShowInBubble(int pos) {
        if (pos < 0 || pos >= mItems.size())
            return null;

        String name = mItems.get(pos).getCompany();
        if (name == null || name.length() < 1)
            return null;

        return mItems.get(pos).getCompany().substring(0, 1);
    }

    public interface OnItemClickListener {
        void onItemClick(int id, Object object);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView retailerShopName, retailerName, retailerLocation,retailerMobile, tvVolumeClass;
        Button btnLogCall, btnPlaceOrder;

        public ViewHolder(View view) {
            super(view);


            retailerShopName = (TextView) view.findViewById(R.id.tv_retailer_shop_name);
            retailerName = (TextView) view.findViewById(R.id.tv_retailer_name);
            retailerLocation = (TextView) view.findViewById(R.id.tv_retailer_location);
            retailerMobile = (TextView) view.findViewById(R.id.tv_mobile_number);
            tvVolumeClass = (TextView) view.findViewById(R.id.tv_volume_class);

            btnLogCall = (Button)view.findViewById(R.id.btn_log_call);
            btnPlaceOrder = (Button)view.findViewById(R.id.btn_place_order);

            btnLogCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleOnClick(1, itemView);
                }
            });
            btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleOnClick(2, itemView);
                }
            });
        }
    }

    private class ModelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if(!TextUtils.isEmpty(constraint)){
                if(Character.isDigit(constraint.charAt(0))){
                    if (constraint != null && constraint.toString().length() > 0) {
                        constraint = constraint.toString();
                        ArrayList<RetailersModel> filteredItems = new ArrayList<RetailersModel>();
                        for (int i = 0, l = mItems.size(); i < l; i++) {
                            RetailersModel m = mItems.get(i);
                            if (m.getTelephone().contains(constraint))
                                filteredItems.add(m);
                        }
                        result.count = filteredItems.size();
                        result.values = filteredItems;
                    }
                    else {
                        result.values = mItems;
                        result.count = mItems.size();

                    }
                }else{
                    if (constraint != null && constraint.toString().length() > 0) {
                        constraint = constraint.toString().toLowerCase();
                        ArrayList<RetailersModel> filteredItems = new ArrayList<RetailersModel>();

                        for (int i = 0, l = mItems.size(); i < l; i++) {
                            RetailersModel m = mItems.get(i);
                            if (m.getCompany().toLowerCase().contains(constraint))
                                filteredItems.add(m);
                        }
                        result.count = filteredItems.size();
                        result.values = filteredItems;
                    } else {
                        result.values = mItems;
                        result.count = mItems.size();

                    }
                }
            }else{
                result.values = mItems;
                result.count = mItems.size();
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredModelItemsArray.clear();
            if(filteredModelItemsArray!=null&&results!=null&&results.count>0) {
                filteredModelItemsArray.addAll((ArrayList<RetailersModel>) results.values);
            }
            notifyDataSetChanged();
        }
    }
}
