package com.ebutor.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ebutor.R;
import com.ebutor.customview.SimpleTagImageView;
import com.ebutor.models.ProductModel;
import com.ebutor.models.SKUModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SKUGridAdapter extends RecyclerView.Adapter<SKUGridAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ProductModel> itemsList;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private OnGridItemClickListener listener;

    public SKUGridAdapter(Context context, ArrayList<ProductModel> itemsList) {
        this.context = context;
        this.itemsList = itemsList;
        listener = (OnGridItemClickListener) context;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_sku_grid, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ProductModel model = itemsList.get(position);
        SKUModel skuModel = model.getSkuModelArrayList().get(0);
        if (skuModel != null) {
            String availableQty = skuModel.getAvailableQuantity();
            if (availableQty != null && !TextUtils.isEmpty(availableQty)) {
                int qty = 0;
                try {
                    qty = Integer.parseInt(availableQty);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (qty > 0) {
                    holder.ivProduct.setTagBackgroundColor(Color.TRANSPARENT);
                    holder.ivProduct.setTagText("");
                } else {
                    holder.ivProduct.setTagBackgroundColor(Color.parseColor("#afd3d3d3"));
                    holder.ivProduct.setTagText("Out of Stock");
                }

            }
            holder.tvProduct.setText(skuModel.getProductName() == null ? "" : model.getSkuModelArrayList().get(0).getProductName());
            ImageLoader.getInstance().displayImage(skuModel.getSkuImage(), holder.ivProduct, options, animateFirstListener);

        }

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void removeAllItems() {
        this.itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItem(ProductModel model) {
        if (itemsList != null)
            this.itemsList.add(model);

        notifyDataSetChanged();
    }

    public interface OnGridItemClickListener {
        public void onGridItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        SimpleTagImageView ivProduct;
        TextView tvProduct;

        public MyViewHolder(View view) {
            super(view);
            ivProduct = (SimpleTagImageView) view.findViewById(R.id.iv_product);
            tvProduct = (TextView) view.findViewById(R.id.tv_product);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onGridItemClick(getPosition());
        }
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
