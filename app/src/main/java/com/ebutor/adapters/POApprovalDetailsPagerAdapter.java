package com.ebutor.adapters;

/**
 * Created by Srikanth Nama on 26-Dec-16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ebutor.fragments.POApprovalHistoryFragment;
import com.ebutor.models.OrdersModel;
import com.ebutor.srm.fragments.PODetailFragment;

import java.util.ArrayList;

/**
 * Created by Admin on 11-12-2015.
 */
public class POApprovalDetailsPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<String> mTabs;
    private OrdersModel poModel;

    public POApprovalDetailsPagerAdapter(FragmentManager fm, ArrayList<String> tabs, OrdersModel poModel) {
        super(fm);
        this.mTabs = tabs;
        this.poModel = poModel;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new PODetailFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFromApproval", true);
                fragment.setArguments(bundle);
                break;
            case 1:
                fragment = new POApprovalHistoryFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("POModel", poModel);
                fragment.setArguments(bundle1);
                break;
            default:
                fragment = new PODetailFragment();
                break;
        }

        return fragment;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position);
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

}
