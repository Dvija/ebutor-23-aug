package com.ebutor;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.backgroundtask.MultipartEntity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.services.Locations;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.GPSTracker;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FeedBackActivity extends ParentActivity implements PermissionCallback, ErrorCallback, Response.ErrorListener, VolleyHandler<Object>, MediaPlayer.OnCompletionListener {

    public static final int RequestPermissionCode = 1;
    private static final int REQUEST_PERMISSIONS = 20;
    public boolean permissions = false;
    public android.app.AlertDialog.Builder build = null;
    public android.app.AlertDialog alertDialog = null;
    public Double lan, lon;
    Location mlocation;
    LatLng latLng = null;
    double latitude;
    double longitude;
    public BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            latitude = intent.getDoubleExtra(Locations.EXTRA_LATITUDE, 0);
            longitude = intent.getDoubleExtra(Locations.EXTRA_LONGITUDE, 0);


            if (latitude != 0.0 && longitude != 0.0) {
                lan = latitude;
                lon = longitude;
            }
            latLng = new LatLng(latitude, longitude);

            if (latLng != null) {

                stopService(new Intent(getApplicationContext(), Locations.class));
            }
        }
    };
    //  public GoogleApiClient googleApiClient;
    // LocationRequest request;
    Location loc = null;
    String mLatitude, mLongitude;
    private Spinner spCustomerFeedback, spReason, spFFComments;
    private LinearLayout llMain;
    private RelativeLayout rlAlert;
    private FrameLayout flFeedback, flAudio;
    private EditText etFeedback;
    private Button btnSubmit;
    private ArrayList<CustomerTyepModel> arrCustFeedback, arrReasons, arrFFComments;
    private String custFeedbackId, reasonId, legalEntityId, fileName = "AudioRecording.3gp", ffCommentsId = "";
    private Dialog dialog;
    private SharedPreferences mSharedPreferences;
    private String feedback = "", imagePath = null, audioPath = null, _path = "", picturePath = "";
    private ImageView ivRecord, ivCapture, ivUpload, ivFeedback, ivDeleteImage, ivDeleteAudio, ivPlay;
    private MediaRecorder mediaRecorder;
    private boolean isRecording = false, isPlaying = false;
    private TextView tvAudioFile;
    private MediaPlayer mediaPlayer;
    private boolean isCheckOut = false;
    private TextView tvFFComments;
    private GPSTracker gpsTracker;

    public static boolean isDeviceLocationEnabled(Context mContext) {
        int locMode = 0;
        String locProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locMode = Settings.Secure.getInt(mContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locProviders = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locProviders);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feedback);

        reqPermission();

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.feedback));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }


        build = new android.app.AlertDialog.Builder(FeedBackActivity.this);
        build.setTitle(getString(R.string.app_name));
        build.setMessage(getString(R.string.please_enable_loc));
        build.setPositiveButton(getString(R.string.action_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callGPSSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);
            }
        });
        build.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    return true; // Consumed
                } else {
                    return false; // Not consumed
                }
            }
        });
        alertDialog = build.create();
        alertDialog.setCanceledOnTouchOutside(false);


        /*if (!isDeviceLocationEnabled(this)) {
            //buildapi();
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }*/

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);

        arrCustFeedback = new ArrayList<>();
        arrReasons = new ArrayList<>();
        arrFFComments = new ArrayList<>();

        dialog = Utils.createLoader(FeedBackActivity.this, ConstantValues.TOOLBAR_PROGRESS);
        mediaPlayer = new MediaPlayer();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if (getIntent().hasExtra("legal_entity_id")) {
            legalEntityId = getIntent().getStringExtra("legal_entity_id");
        }

        if (getIntent().hasExtra("isCheckOut")) {
            isCheckOut = getIntent().getBooleanExtra("isCheckOut", false);
        }

        //   gpsTracker = new GPSTracker(FeedBackActivity.this);
        //   if (!gpsTracker.getIsGPSTrackingEnabled()) {
        //     gpsTracker.showSettingsAlert();

        //  }


        llMain = (LinearLayout) findViewById(R.id.ll_main);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        flFeedback = (FrameLayout) findViewById(R.id.fl_feedback);
        flAudio = (FrameLayout) findViewById(R.id.fl_audio);
        spCustomerFeedback = (Spinner) findViewById(R.id.sp_customer_feedback);
        spReason = (Spinner) findViewById(R.id.sp_reason);
        spFFComments = (Spinner) findViewById(R.id.sp_ff_comments);
        etFeedback = (EditText) findViewById(R.id.et_feedback);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        ivRecord = (ImageView) findViewById(R.id.iv_record);
        ivCapture = (ImageView) findViewById(R.id.iv_capture);
        ivUpload = (ImageView) findViewById(R.id.iv_upload);
        tvAudioFile = (TextView) findViewById(R.id.tv_audio_file);
        ivFeedback = (ImageView) findViewById(R.id.iv_feedback);
        ivDeleteImage = (ImageView) findViewById(R.id.iv_delete_image);
        ivDeleteAudio = (ImageView) findViewById(R.id.iv_delete_audio);
        ivPlay = (ImageView) findViewById(R.id.iv_play);
        tvFFComments = (TextView) findViewById(R.id.tv_ff_comments);


        if (isCheckOut) {
            legalEntityId = mSharedPreferences.getString(ConstantValues.KEY_LEGAL_ENTITY_ID, "");
            tvFFComments.setVisibility(View.VISIBLE);
            spFFComments.setVisibility(View.VISIBLE);
            etFeedback.setEnabled(false);
            ivCapture.setEnabled(false);
            ivUpload.setEnabled(false);
            ivDeleteImage.setEnabled(false);
            ivDeleteAudio.setEnabled(false);
            ivRecord.setEnabled(false);
            ivPlay.setEnabled(false);
        } else {
            tvFFComments.setVisibility(View.GONE);
            spFFComments.setVisibility(View.GONE);
            etFeedback.setEnabled(true);
            ivCapture.setEnabled(true);
            ivUpload.setEnabled(true);
            ivDeleteImage.setEnabled(true);
            ivDeleteAudio.setEnabled(true);
            ivRecord.setEnabled(true);
            ivPlay.setEnabled(true);
        }

        etFeedback.clearFocus();

        arrCustFeedback = (ArrayList<CustomerTyepModel>) DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_CUSTOMER_FEEDBACK);
        if (arrCustFeedback != null && arrCustFeedback.size() > 0) {
            ArrayAdapter<CustomerTyepModel> adapter = new ArrayAdapter<>(FeedBackActivity.this, android.R.layout.simple_spinner_dropdown_item, arrCustFeedback);
            spCustomerFeedback.setAdapter(adapter);
            spCustomerFeedback.setSelection(0);
        }

        if (isCheckOut) {
            arrFFComments = DBHelper.getInstance().getMasterLookUpData(DBHelper.TABLE_FF_COMMENTS);
            CustomerTyepModel customerTyepModel_ = new CustomerTyepModel();
            customerTyepModel_.setCustomerName(getResources().getString(R.string.select_ff_comments));
            customerTyepModel_.setCustomerGrpId("");
            arrFFComments.add(0, customerTyepModel_);
            ArrayAdapter<CustomerTyepModel> adapter = new ArrayAdapter<>(FeedBackActivity.this, android.R.layout.simple_spinner_dropdown_item, arrFFComments);
            spFFComments.setAdapter(adapter);
            spFFComments.setSelection(0);
        }

        CustomerTyepModel customerTyepModel = new CustomerTyepModel();
        customerTyepModel.setCustomerName(getResources().getString(R.string.select_reasons));
        customerTyepModel.setCustomerGrpId("");
        arrReasons = new ArrayList<>();
        arrReasons.add(customerTyepModel);
        ArrayAdapter<CustomerTyepModel> reasonsAdapter = new ArrayAdapter<>(FeedBackActivity.this, android.R.layout.simple_spinner_dropdown_item, arrReasons);
        spReason.setAdapter(reasonsAdapter);
        spReason.setSelection(0);

        spCustomerFeedback.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                custFeedbackId = arrCustFeedback.get(i).getCustomerGrpId();
                etFeedback.setText("");
                flAudio.setVisibility(View.GONE);
                flFeedback.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                ivFeedback.setImageBitmap(null);
                tvAudioFile.setText("");
                imagePath = null;
                audioPath = null;
                if (!TextUtils.isEmpty(custFeedbackId))
                    getReasons();
                else {
                    CustomerTyepModel customerTyepModel = new CustomerTyepModel();
                    customerTyepModel.setCustomerName(getResources().getString(R.string.select_reasons));
                    customerTyepModel.setCustomerGrpId("");
                    arrReasons = new ArrayList<>();
                    arrReasons.add(customerTyepModel);
                    ArrayAdapter<CustomerTyepModel> reasonsAdapter = new ArrayAdapter<>(FeedBackActivity.this, android.R.layout.simple_spinner_dropdown_item, arrReasons);
                    spReason.setAdapter(reasonsAdapter);
                    spReason.setSelection(0);

                    if (isCheckOut) {
                        etFeedback.setEnabled(false);
                        ivCapture.setEnabled(false);
                        ivUpload.setEnabled(false);
                        ivDeleteImage.setEnabled(false);
                        ivDeleteAudio.setEnabled(false);
                        ivRecord.setEnabled(false);
                        ivPlay.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reasonId = arrReasons.get(i).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spFFComments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ffCommentsId = arrFFComments.get(position).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPlaying) {
                    isPlaying = true;
                    ivPlay.setImageResource(R.drawable.ic_stop);
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setOnCompletionListener(FeedBackActivity.this);
                    try {
                        mediaPlayer.setDataSource(audioPath);
                        mediaPlayer.prepare();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mediaPlayer.start();
                } else {
                    isPlaying = false;
                    ivPlay.setImageResource(R.drawable.ic_play);
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        MediaRecorderReady();
                    }
                }
            }
        });

        ivCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permissionCheck = ContextCompat.checkSelfPermission(FeedBackActivity.this,
                        Manifest.permission.CAMERA);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(FeedBackActivity.this,
                            new String[]{Manifest.permission.CAMERA}, 9);
                    return;
                }

                int permissionCheck1 = ContextCompat.checkSelfPermission(FeedBackActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

                if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(FeedBackActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                    return;
                }

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, 1);
            }
        });

        ivUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permissionCheck = ContextCompat.checkSelfPermission(FeedBackActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(FeedBackActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                    return;
                }
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);

            }
        });

        ivRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRecording) {
                    isRecording = true;
                    ivRecord.setImageResource(R.drawable.ic_stop);
                    ivPlay.setVisibility(View.GONE);
                    flAudio.setVisibility(View.GONE);
                    int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                            WRITE_EXTERNAL_STORAGE);
                    int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                            RECORD_AUDIO);

                    int permissionCheck1 = ContextCompat.checkSelfPermission(FeedBackActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(FeedBackActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                        return;
                    }

                    if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED) {

                        audioPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName;

                        MediaRecorderReady();

                        try {
                            if (mediaRecorder != null) {
                                mediaRecorder.prepare();
                                mediaRecorder.start();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        ActivityCompat.requestPermissions(FeedBackActivity.this, new
                                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
                    }
                } else {
                    isRecording = false;
                    try {
                        if (mediaRecorder != null) {
                            mediaRecorder.stop();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    flAudio.setVisibility(View.VISIBLE);
                    ivPlay.setVisibility(View.VISIBLE);
                    tvAudioFile.setText(fileName);
                    ivRecord.setImageResource(R.drawable.ic_record);
                }

            }
        });

        ivDeleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flFeedback.setVisibility(View.GONE);
                ivFeedback.setImageBitmap(null);
                imagePath = null;
            }
        });

        ivDeleteAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flAudio.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                tvAudioFile.setText("");
                audioPath = null;
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    MediaRecorderReady();
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                feedback = etFeedback.getText().toString();


                if (!isDeviceLocationEnabled(getApplicationContext())) {
                    if (!alertDialog.isShowing()) {
                        alertDialog.show();
                    }
                    return;
                }
                if (latitude == 0.0 && longitude == 0.0) {
                    Toast.makeText(getApplication(), getString(R.string.please_wait_loc_fetched), Toast.LENGTH_SHORT).show();
                    LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(message, new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
                    );

                    Intent in = new Intent(getApplicationContext(), Locations.class);
                    startService(in);
                    return;

                }

                if (isCheckOut) {


                    if (TextUtils.isEmpty(ffCommentsId)) {
                        Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.please_select_ff_comments));
                        return;
                    }


                } else {

                    if (TextUtils.isEmpty(custFeedbackId)) {
                        Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.please_select_customer_feedback));
                        return;
                    }
                    if (TextUtils.isEmpty(reasonId)) {
                        Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.please_select_feedback_reasons));
                        return;
                    }

                }
                new FeedbackTask().execute();
//                submitFeedback();


            }
        });

    }

    /* public void buildapi(){
         googleApiClient=new GoogleApiClient.Builder(this)
                 .addOnConnectionFailedListener(this)
                 .addConnectionCallbacks(this)
                 .addApi(LocationServices.API)
                 .build();

         request=new LocationRequest();
         request.setFastestInterval(1000);
         request.setInterval(10*1000);
         request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
     }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Bitmap bitmap, newBitmap;
                int rotate = 0;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                _path = f.getAbsolutePath();
                imagePath = _path;
                try {


                    ExifInterface exif = new ExifInterface(_path);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotate);
                    bitmap = BitmapFactory.decodeFile(_path, bitmapOptions);
                    newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    ivFeedback.setImageBitmap(newBitmap);
                    flFeedback.setVisibility(View.VISIBLE);
                    f.delete();
                    f = new File(_path);
                    f.createNewFile();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    newBitmap.compress(Bitmap.CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                picturePath = c.getString(columnIndex);
                imagePath = picturePath;
                c.close();
                int rotate = 0;
                Bitmap newBitmap;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                Matrix matrix = new Matrix();

                try {
                    ExifInterface exif = new ExifInterface(picturePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                    matrix.postRotate(rotate);
                    Bitmap bitmap = (BitmapFactory.decodeFile(picturePath));
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes1);
                    ivFeedback.setImageBitmap(newBitmap);
                    flFeedback.setVisibility(View.VISIBLE);
                    File f = new File(selectedImage.getPath());
                    f.delete();
                    f = new File(picturePath);
                    f.createNewFile();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    newBitmap.compress(Bitmap.CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    ivFeedback.setImageBitmap(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(audioPath);
    }

    private void submitFeedback() {
        if (Networking.isNetworkAvailable(FeedBackActivity.this)) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("legal_entity_id", legalEntityId);
                jsonObject.put("feedback_id", reasonId);
                jsonObject.put("feedback_groupid", custFeedbackId);
                jsonObject.put("comments", etFeedback.getText().toString());
                map.put("data", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask tabsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.saveFeedbackReasonsURL, map, FeedBackActivity.this, FeedBackActivity.this, PARSER_TYPE.SAVE_FEEDBACK);
            tabsRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(tabsRequest, AppURL.saveFeedbackReasonsURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void getReasons() {
        if (Networking.isNetworkAvailable(FeedBackActivity.this)) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                jsonObject.put("feedback_groupid", custFeedbackId);
                map.put("data", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask tabsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.feedbackReasonsURL, map, FeedBackActivity.this, FeedBackActivity.this, PARSER_TYPE.FEEDBACK_REASONS);
            tabsRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(tabsRequest, AppURL.feedbackReasonsURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.FEEDBACK_REASONS) {
                        if (response instanceof ArrayList) {
                            arrReasons = (ArrayList<CustomerTyepModel>) response;

                            if (arrReasons != null && arrReasons.size() > 0) {
                                ArrayAdapter<CustomerTyepModel> adapter = new ArrayAdapter<>(FeedBackActivity.this, android.R.layout.simple_spinner_dropdown_item, arrReasons);
                                spReason.setAdapter(adapter);
                                spReason.setSelection(0);

                                if (isCheckOut) {
                                    etFeedback.setEnabled(true);
                                    ivCapture.setEnabled(true);
                                    ivUpload.setEnabled(true);
                                    ivDeleteImage.setEnabled(true);
                                    ivDeleteAudio.setEnabled(true);
                                    ivRecord.setEnabled(true);
                                    ivPlay.setEnabled(true);
                                }
                            }
                        }
                    } else if (requestType == PARSER_TYPE.SAVE_FEEDBACK) {
                        if (response instanceof String) {
                            etFeedback.setText("");
                            showAlertWithMessage(message);
                        }
                    }
                } else {
                    Utils.showAlertWithMessage(FeedBackActivity.this, message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(FeedBackActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    private void showAlertWithMessage(String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(FeedBackActivity.this);
        dialog.setMessage(message);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (isCheckOut) {
                    DBHelper.getInstance().deleteTable(DBHelper.TABLE_OUTLETS);
                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_CHECKIN, false).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_TOKEN, mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, "")).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_ID, mSharedPreferences.getString(ConstantValues.KEY_FF_ID, "")).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_GRP_ID, mSharedPreferences.getString(ConstantValues.CUSTOMER_GRP_ID, "")).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_SEGMENT_ID, mSharedPreferences.getString(ConstantValues.SEGMENT_ID, "")).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_SHOP_NAME, "").apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_FIRST_NAME, mSharedPreferences.getString(ConstantValues.KEY_FF_NAME, "")).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_WH_IDS, mSharedPreferences.getString(ConstantValues.KEY_LE_WH_IDS, "")).apply();
                    mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, false).apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_PROFILE_IMAGE, "").apply();
                    mSharedPreferences.edit().putString(ConstantValues.KEY_CUSTOMER_BEAT_ID, "").apply();

                    MyApplication.getInstance().setArrProductFilters(null);

                    Intent intent = new Intent(FeedBackActivity.this, FFDashboardActivity.class);
                    intent.putExtra("isCheckout", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } else {
                    finish();
                }
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(FeedBackActivity.this, message);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        isPlaying = false;
        ivPlay.setImageResource(R.drawable.ic_play);
        if (mp != null && mp.isPlaying()) {
            mp.stop();
            mp.release();
            MediaRecorderReady();
        }
    }

    /* @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.i("message", "I'm connected now");
        if (ActivityCompat.checkSelfPermission(getApplication(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplication(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }
        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (currentLocation != null) {
            mlocation = currentLocation;
        } else {
            PendingResult<Status> pendingResult= LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, request, this);
            // Schedule a Thread to unregister location listeners

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d("message", "Firing onLocationChanged..............................................");
        Toast.makeText(this, "location changed", Toast.LENGTH_SHORT).show();
        mlocation = location;
        updateUI();
    }

    private Location updateUI() {
        Log.d("message", "UI update initiated .............");
        if (null != mlocation) {
            String lat = String.valueOf(mlocation.getLatitude());
            String lng = String.valueOf(mlocation.getLongitude());

                   } else {
            Log.d("message", "location is null ...............");
        }
        return mlocation;
    } */


    @Override
    protected void onResume() {
        super.onResume();
        if (!isDeviceLocationEnabled(this)) {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }

        if (permissions) {
            LocalBroadcastManager.getInstance(this).registerReceiver(message, new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
            );

            Intent in = new Intent(this, Locations.class);
            startService(in);


        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(message);
        stopService(new Intent(this, Locations.class));

    }

    private void reqPermission() {
        new AskPermission.Builder(this).setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)
                .setCallback(this)
                .setErrorCallback(this)
                .request(REQUEST_PERMISSIONS);
    }


    @Override
    public void onPermissionsGranted(int requestCode) {
        permissions = true;

      /*  LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        latitude = intent.getDoubleExtra(Locations.EXTRA_LATITUDE, 0);
                        longitude = intent.getDoubleExtra(Locations.EXTRA_LONGITUDE, 0);
                       /* city=intent.getStringExtra(Locations.STREET);
                        pin=intent.getStringExtra(Locations.POSTAL_CODE);
                        add1=intent.getStringExtra(Locations.ADDRESS_LINE1);
                        add2=intent.getStringExtra(Locations.ADDRESS_LINE2);
                        subloc=intent.getStringExtra(Locations.SUB_Locality);

                        latLng=new LatLng(latitude,longitude);
                      //  getGeocodes(latitude,longitude,city,pin,add1,add2,subloc);
                        //getGeocodes(latitude,longitude,BuyerDetailsActivity.this,new GeocoderHandler());
                        Toast.makeText(context, ""+latitude+" ,"+longitude, Toast.LENGTH_SHORT).show();
                        if(latLng!=null){
                            stopService(new Intent(getApplicationContext(),Locations.class));
                        }
                    }
                }, new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
        );

        Intent in=new Intent(this,Locations.class);
        startService(in); */
    }

    @Override
    public void onPermissionsDenied(int requestCode) {
    }

    @Override
    public void onShowRationalDialog(final PermissionInterface permissionInterface, int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("We need permissions for this app.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onDialogShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }

    @Override
    public void onShowSettings(final PermissionInterface permissionInterface, int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("We need permissions for this app. Open setting screen?");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionInterface.onSettingsShown();
            }
        });
        builder.setNegativeButton("cancel", null);
        builder.show();
    }


    class FeedbackTask extends AsyncTask<Void, Void, String> {
        String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialog != null)
                dialog.show();
        }

        @Override
        protected String doInBackground(Void... sText) {

            try {
                String url = AppURL.saveFeedbackReasonsURL;


                JSONObject jsonObject = new JSONObject();
                jsonObject.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));

                jsonObject.put("legal_entity_id", legalEntityId);
                jsonObject.put("feedback_id", reasonId);
                jsonObject.put("feedback_groupid", custFeedbackId);
                jsonObject.put("comments", feedback);


                if (isCheckOut) {
                    LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
                            new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    latitude = intent.getDoubleExtra(Locations.EXTRA_LATITUDE, 0);
                                    longitude = intent.getDoubleExtra(Locations.EXTRA_LONGITUDE, 0);

                                    latLng = new LatLng(latitude, longitude);
                                    if (latitude != 0.0 && longitude != 0.0) {
                                        lan = latitude;
                                        lon = longitude;
                                    }
                                    if (latLng != null) {
                                        stopService(new Intent(getApplicationContext(), Locations.class));
                                    }
                                }
                            }, new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
                    );

                    Intent in = new Intent(getApplicationContext(), Locations.class);
                    startService(in);
                }

                double currentLat = 0.0, currentLong = 0.0;
                //    loc=updateUI();

                currentLat = latitude;
                currentLong = longitude;


                jsonObject.put("user_id", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
                jsonObject.put("activity", ffCommentsId);
                jsonObject.put("flag", "1");
                if (latitude == 0.0 && longitude == 0.0) {

                    Toast.makeText(FeedBackActivity.this, getString(R.string.please_wait_loc_fetched), Toast.LENGTH_SHORT).show();
                    LocalBroadcastManager.getInstance(FeedBackActivity.this).registerReceiver(message
                            , new IntentFilter(Locations.ACTION_LOCATION_BROADCAST)
                    );

                    Intent in = new Intent(FeedBackActivity.this, Locations.class);
                    startService(in);
                }

                jsonObject.put("latitude", lan);
                jsonObject.put("longitude", lon);


                Log.e("Feedback", jsonObject.toString());

                MultipartEntity multipart = new MultipartEntity(url, "UTF-8");

                multipart.addFormField("data", jsonObject.toString());

                if (imagePath != null)
                    multipart.addFilePart("feedback_pic", new File(imagePath));
                if (audioPath != null)
                    multipart.addFilePart("feedback_audio", new File(audioPath));

                List<String> response = multipart.finish();

                System.out.println("SERVER REPLIED:");
                StringBuilder sb = new StringBuilder();
                for (String line : response) {
                    System.out.println(line);
                    sb.append(line);
                }

                return sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;

        }

        @Override
        protected void onPostExecute(String response) {

            super.onPostExecute(response);

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }

            if (response != null && !TextUtils.isEmpty(response)) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equalsIgnoreCase("success")) {
                        etFeedback.setText("");
                        showAlertWithMessage(message);
                    } else if (status.equalsIgnoreCase("session")) {
                        Utils.logout(FeedBackActivity.this, message);
                    } else {
                        Utils.showAlertWithMessage(FeedBackActivity.this, message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showAlertWithMessage(FeedBackActivity.this, getString(R.string.something_wrong));
            }

        }
    }
}
