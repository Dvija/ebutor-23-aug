package com.ebutor.database;

/**
 * Created by 300041 on 5/22/2015.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.ebutor.MyApplication;
import com.ebutor.models.BannerModel;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CartModel;
import com.ebutor.models.CategoryModel;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.DiscountModel;
import com.ebutor.models.FeatureData;
import com.ebutor.models.HomeDataModel;
import com.ebutor.models.HomeModel;
import com.ebutor.models.NewExpenseModel;
import com.ebutor.models.ProductSlabData;
import com.ebutor.models.RetailersModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.models.TopBrandsModel;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class DBHelper extends SQLiteOpenHelper {

    // Database Version
    public static final int DATABASE_VERSION = 13;
    // Database Name
    public static final String DATABASE_NAME = "factail_database";

    public static final String TABLE_CUSTOMER_TYPE = "table_customer_type";
    public static final String TABLE_BUSINESS_TYPE = "table_business_type";
    public static final String TABLE_VOLUME_CLASS = "table_volume_class";
    public static final String TABLE_LICENSE = "table_license";
    public static final String TABLE_MASTER_MANF = "table_master_manf";
    public static final String TABLE_FF_COMMENTS = "table_ff_comments";
    public static final String TABLE_PREF_SLOTS = "table_pref_slots";
    public static final String TABLE_CATEGORY = "table_category";
    public static final String TABLE_BRANDS = "table_brands";
    public static final String TABLE_MANUFACTURERS = "table_manufacturers";
    public static final String TABLE_BANNERS = "table_banners";
    public static final String TABLE_HOME = "table_home";
    public static final String TABLE_HOME_CHILD = "table_home_child";
    public static final String TABLE_PRODUCTS = "table_products";
    public static final String TABLE_TEMP_PRODUCTS = "temp_products";
    public static final String TABLE_CART = "table_cart";
    public static final String TABLE_PO_CART = "table_po_cart";
    public static final String TABLE_RETAILERS = "table_retailers";
    public static final String TABLE_OUTLETS = "table_outlets";
    public static final String TABLE_CUSTOMER_FEEDBACK = "table_customer_feedback";
    public static final String TABLE_UNCLAIMED_EXPENSES = "table_unclaimed_expenses";
    public static final String TABLE_FEATURES = "table_features";
    public static final String TABLE_STAR = "table_star";
    public static final String TABLE_PRODUCT_SLAB_DATA = "table_product_slab_data";
    public static final String TABLE_DISCOUNT = "table_disount";

    //customer_type table columns
    public static final String COL_CUSTOMER_NAME = "customer_name";
    public static final String COL_CUSTOMER_TYPE_ROW_ID = "customer_type_row_id";
    public static final String COL_CUSTOMER_GRP_ID = "customer_grp_id";
    public static final String COL_DESCRIPTION = "description";

    //category table columns
    public static final String COL_CATEGORY = "category_id";
    public static final String COL_CATEGORY_NAME = "category_name";
    public static final String COL_PARENT_ID = "parent_id";
    public static final String COL_CATEGORY_IMAGE = "category_image";
    public static final String COL_PRODUCT_NO = "product_no";
    public static final String COL_CATEGORY_ROW_ID = "category_row_id";
    public static final String COL_CATEGORY_SEGMENT_ID = "category_segment_id";
    public static final String COL_CATEGORY_FLAG = "category_flag";

    //business type table columns
    public static final String COL_SEGMENT_TYPE_ROW_ID = "segment_row_id";
    public static final String COL_SEGMENT_NAME = "segment_name";
    public static final String COL_SEGMENT_ID = "segment_id";
    public static final String COL_CATEGORY_ID = "category_id";

    //brands table columns
    public static final String COL_BRANDS_ROW_ID = "brand_row_id";
    public static final String COL_BRAND_NAME = "brand_name";
    public static final String COL_BRAND_ID = "brand_id";
    public static final String COL_BRAND_LOGO = "brand_logo";
    public static final String COL_BRAND_FLAG = "brand_flag";

    //Manufacturers table columns
    public static final String COL_MFG_ROW_ID = "mfg_row_id";
    public static final String COL_MFG_NAME = "mfg_name";
    public static final String COL_MFG_ID = "mfg_id";
    public static final String COL_MFG_LOGO = "mfg_logo";
    public static final String COL_MFG_FLAG = "mfg_flag";

    //Banners table columns
    public static final String COL_BNR_ROW_ID = "bnr_row_id";
    public static final String COL_BNR_NAME = "bnr_name";
    public static final String COL_BNR_ID = "bnr_id";
    public static final String COL_BNR_LOGO = "bnr_logo";
    public static final String COL_BNR_NAV_OBJ = "bnr_nav_obj";
    public static final String COL_BNR_NAV_OBJ_ID = "bnr_nav_obj_id";
    public static final String COL_BNR_FREQ = "bnr_freq";

    //Home table columns
    public static final String COL_HOME_ROW_ID = "home_row_id";
    public static final String COL_HOME_FLAG = "home_flag";
    public static final String COL_HOME_DISPLAY_TITLE = "home_display_title";
    public static final String COL_HOME_KEY = "home_key";
    public static final String COL_HOME_ITEMS = "home_items";

    //Home Child table columns
    public static final String COL_HOME_CHILD_ROW_ID = "home_child_row_id";
    public static final String COL_HOME_CHILD_ID = "home_child_id";
    public static final String COL_HOME_CHILD_NAME = "home_child_name";
    public static final String COL_HOME_CHILD_IMAGE = "home_child_image";
    public static final String COL_HOME_CHILD_TITLE = "home_child_title";
    public static final String COL_HOME_CHILD_FLAG = "home_child_flag";
    public static final String COL_HOME_CHILD_KEY = "home_child_key";

    //Product table columns
    public static final String COL_PRODUCT_ROW_ID = "product_row_id";
    public static final String COL_PRODUCT_ID = "product_id";
    public static final String COL_PRODUCT_IMAGE = "primary_image";
    public static final String COL_THUMBNAIl_IMAGE = "thumbnail_image";
    public static final String COL_PRODUCT_TITLE = "product_title";
    public static final String COL_PRODUCT_CAT_ID = "product_category_id";
    public static final String COL_PRODUCT_BRAND_ID = "product_brand_id";
    public static final String COL_PRODUCT_MANF_ID = "product_manufacturer_id";
    public static final String COL_PRODUCT_MRP = "product_mrp";
    public static final String COL_PRODUCT_VARIANT1 = "product_variant_value1";
    public static final String COL_PRODUCT_VARIANT2 = "product_variant_value2";
    public static final String COL_PRODUCT_VARIANT3 = "product_variant_value3";
    public static final String COL_PRODUCT_FREEBIE = "product_freebie";
    public static final String COL_PRODUCT_IS_PARENT = "product_is_parent";
    public static final String COL_PRODUCT_KEY_INDEX = "product_key_value_index";
    public static final String COL_PRODUCT_PARENT_ID = "product_parent_id";
    public static final String COL_PRODUCT_INV = "product_inv";
    public static final String COL_PRODUCT_META_KEYWORDS = "product_meta_keywords";
    public static final String COL_ESU = "product_esu";
    public static final String COL_PACK_TYPE = "pack_type";
    public static final String COL_SKU_CODE = "sku_code";
    public static final String COL_STAR = "star";
    public static final String COL_PACK_LEVEL = "pack_level";
    public static final String COL_SLAB_ESU = "slab_esu";
    public static final String COL_FREEBIE_MPQ = "free_mpq";
    public static final String COl_FREEBIE_FQ = "free_fq";
    public static final String COl_CASH_ID = "cashback_id";
    public static final String COl_CASHBACK_AMT = "cashback_amount";
    public static final String COl_IS_CASHBACK = "is_cashback";
    public static final String COL_PACK_STAR = "pack_star";

    public static final String COL_SORT_ORDER = "sort_order";
    public static final String COL_SYNC_TIME = "last_sync_date";

    public static final String KEY_ROW_ID = "row_id";
    public static final String KEY_CUSTOMER_ID = "customer_id";
    public static final String KEY_PRODUCT_ID = "product_id";
    public static final String KEY_QUANTITY = "quantity";
    public static final String KEY_ESU = "esu";
    public static final String KEY_UNIT_PRICE = "unit_price";
    public static final String KEY_MARGIN = "margin";
    public static final String KEY_TOTAL_PRICE = "total_price";
    public static final String KEY_WAREHOUSE_ID = "warehouse_id";
    public static final String KEY_PACK_SIZE = "pack_size";
    public static final String KEY_FB_PACK_SIZE = "fb_pack_size";
    public static final String KEY_STATUS = "status";
    public static final String KEY_AVAILABLE_QUANTITY = "available_qty";
    public static final String KEY_DISCOUNT = "discount";
    public static final String KEY_PARENT_ID = "parent_id";
    public static final String KEY_FREEBIE_ID = "freebie_id";
    public static final String KEY_FREEBIE_QTY = "freebie_qty";
    public static final String KEY_FREE_QTY = "free_qty";
    public static final String KEY_PACK_TYPE = "pack_type";
    public static final String KEY_IS_CHILD = "is_child";
    public static final String KEY_REMARKS = "remarks";
    public static final String KEY_UPDATED_DATE = "updated_at";
    public static final String KEY_CART_ID = "cart_id";
    public static final String KEY_IS_SLAB = "is_slab";
    public static final String KEY_BLOCK_QTY = "block_qty";
    public static final String KEY_PROMOTION_ID = "promotion_id";
    public static final String PRODUCT_PACK_ID = "product_pack_id";
    public static final String FREEBIE_PACK_ID = "freebie_pack_id";
    public static final String KEY_PRODUCT_SLAB_ID = "product_slab_id";
    public static final String KEY_PACK_QUANTITY = "pack_qty";

    public static final String KEY_COMPANY = "company";
    public static final String KEY_ADDRESS_1 = "address_1";
    public static final String KEY_ADDRESS_2 = "address_2";
    public static final String KEY_FIRST_NAME = "firstname";
    public static final String KEY_TELEPHONE = "telephone";
    //    public static final String KEY_CART_ID = "customer_id";
    public static final String KEY_NO_OF_SHUTTERS = "no_of_shutters";
    public static final String KEY_VOLUME_CLASS = "volume_class";
    public static final String KEY_BUSINESS_TYPE = "business_type_id";
    public static final String KEY_MASTER_MANF = "master_manf";
    public static final String KEY_POPUP = "popup";
    public static final String KEY_BEAT_ID = "beat_id";
    public static final String KEY_IS_CHECKIN = "is_checkin";
    public static final String KEY_IS_BILLED = "is_billed";

    public static final String KEY_IS_FREEBIE = "is_freebie";


    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_DATE = "date";
    public static final String KEY_EXPENSE_TYPE = "expense_type";
    public static final String KEY_EXPENSE_TYPE_ID = "expense_type_id";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_ATTACHMENTS = "attachments";
    public static final String KEY_CREATED_DATE_TIME = "created_at";

    // table_features
    public static final String KEY_FEATURE_CODE = "feature_code";
    public static final String KEY_FEATURE_NAME = "feature_name";
    public static final String KEY_FEATURE_IS_PARENT = "is_parent";
    public static final String KEY_FEATURE_PARENT = "parent_id";

    //table discount
    public static final String KEY_DISCOUNT_TYPE = "discount_type";
    public static final String KEY_DISCOUNT_ON = "discount_on";
    public static final String KEY_DISCOUNT_ON_VALUE = "discount_on_values";

    public static DBHelper instance = null;

    public DBHelper(Context context) {
        super(MyApplication.getInstance().getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DBHelper getInstance() {
        if (instance != null) {
            return instance;
        } else {
            return new DBHelper(null);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table
        String CREATE_CUSTOMER_TYPE = "CREATE TABLE " + TABLE_CUSTOMER_TYPE + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        // create books table
        db.execSQL(CREATE_CUSTOMER_TYPE);

        String CREATE_VOLUME_CLASS = "CREATE TABLE " + TABLE_VOLUME_CLASS + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_VOLUME_CLASS);

        String CREATE_LICENSE = "CREATE TABLE " + TABLE_LICENSE + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_LICENSE);

        String CREATE_MASTER_MANF = "CREATE TABLE " + TABLE_MASTER_MANF + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_MASTER_MANF);

        String CREATE_FF_COMMENTS = "CREATE TABLE " + TABLE_FF_COMMENTS + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_FF_COMMENTS);

        String CREATE_PREF_SLOTS = "CREATE TABLE " + TABLE_PREF_SLOTS + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_PARENT_ID + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_PREF_SLOTS);

        String CREATE_CATEGORY_TABLE = "CREATE TABLE " + TABLE_CATEGORY + "("
                + COL_CATEGORY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CATEGORY + " TEXT,"
                + COL_CATEGORY_SEGMENT_ID + " TEXT ,"
                + COL_CATEGORY_NAME + " TEXT ,"
                + COL_PARENT_ID + " TEXT ,"
                + COL_PRODUCT_NO + " TEXT ,"
                + COL_CATEGORY_IMAGE + " TEXT ,"
                + COL_CATEGORY_FLAG + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        // create books table
        db.execSQL(CREATE_CATEGORY_TABLE);

        String CREATE_SEGEMENT_TABLE = "CREATE TABLE " + TABLE_BUSINESS_TYPE + "("
                + COL_SEGMENT_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_SEGMENT_ID + " TEXT ,"
                + COL_SEGMENT_NAME + " TEXT ,"
                + COL_CATEGORY_ID + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_SEGEMENT_TABLE);

        //brands table
        String CREATE_BRANDS_TABLE = "CREATE TABLE " + TABLE_BRANDS + "("
                + COL_BRANDS_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_BRAND_ID + " TEXT ,"
                + COL_MFG_ID + " TEXT ,"
                + COL_BRAND_NAME + " TEXT ,"
                + COL_BRAND_LOGO + " TEXT ,"
                + COL_BRAND_FLAG + " TEXT ,"
                + COL_SORT_ORDER + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_BRANDS_TABLE);

        //Manufacturers table
        String CREATE_MFG_TABLE = "CREATE TABLE " + TABLE_MANUFACTURERS + "("
                + COL_MFG_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_MFG_ID + " TEXT ,"
                + COL_MFG_NAME + " TEXT ,"
                + COL_MFG_LOGO + " TEXT ,"
                + COL_MFG_FLAG + " TEXT ,"
                + COL_SORT_ORDER + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_MFG_TABLE);

        //Banners table
        String CREATE_BNR_TABLE = "CREATE TABLE " + TABLE_BANNERS + "("
                + COL_BNR_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_BNR_ID + " TEXT ,"
                + COL_BNR_NAME + " TEXT ,"
                + COL_BNR_LOGO + " TEXT ,"
                + COL_BNR_NAV_OBJ + " TEXT ,"
                + COL_BNR_NAV_OBJ_ID + " TEXT ,"
                + COL_BNR_FREQ + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_BNR_TABLE);

        //Home table
        String CREATE_HOME_TABLE = "CREATE TABLE " + TABLE_HOME + "("
                + COL_HOME_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_HOME_FLAG + " TEXT ,"
                + COL_HOME_DISPLAY_TITLE + " TEXT ,"
                + COL_HOME_KEY + " TEXT ,"
                + COL_HOME_ITEMS + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_HOME_TABLE);

        //Home Child table
        String CREATE_HOME_CHILD_TABLE = "CREATE TABLE " + TABLE_HOME_CHILD + "("
                + COL_HOME_CHILD_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_HOME_CHILD_ID + " TEXT ,"
                + COL_HOME_CHILD_NAME + " TEXT ,"
                + COL_HOME_CHILD_IMAGE + " TEXT ,"
                + COL_HOME_CHILD_TITLE + " TEXT ,"
                + COL_HOME_CHILD_FLAG + " TEXT ,"
                + COL_HOME_CHILD_KEY + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_HOME_CHILD_TABLE);

        //Products table
        String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_PRODUCTS + "("
                + COL_PRODUCT_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_PRODUCT_ID + " TEXT ,"
                + COL_PRODUCT_IMAGE + " TEXT ,"
                + COL_THUMBNAIl_IMAGE + " TEXT ,"
                + COL_PRODUCT_TITLE + " TEXT ,"
                + COL_PRODUCT_CAT_ID + " TEXT ,"
                + COL_PRODUCT_BRAND_ID + " TEXT ,"
                + COL_PRODUCT_MANF_ID + " TEXT ,"
                + COL_PRODUCT_MRP + " TEXT ,"
                + COL_PRODUCT_VARIANT1 + " TEXT ,"
                + COL_PRODUCT_VARIANT2 + " TEXT ,"
                + COL_PRODUCT_VARIANT3 + " TEXT ,"
                + COL_PRODUCT_FREEBIE + " TEXT ,"
                + COL_PRODUCT_IS_PARENT + " TEXT ,"
                + COL_PRODUCT_KEY_INDEX + " TEXT ,"
                + COL_PRODUCT_PARENT_ID + " TEXT ,"
                + COL_PRODUCT_INV + " TEXT ,"
                + COL_PRODUCT_META_KEYWORDS + " TEXT ,"
                + COL_ESU + " TEXT ,"
                + COL_PACK_TYPE + " TEXT ,"
                + COL_SKU_CODE + " TEXT ,"
                + COL_STAR + " TEXT ,"
                + COl_IS_CASHBACK + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_PRODUCT_TABLE);

        String temp_products = "CREATE TABLE " + TABLE_TEMP_PRODUCTS + "("
                + COL_PRODUCT_ID + " TEXT )";
        db.execSQL(temp_products);

        //Cart table
        String cart = "CREATE TABLE " + TABLE_CART + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_CUSTOMER_ID + " TEXT ,"
                + KEY_PRODUCT_ID + " TEXT ,"
                + KEY_QUANTITY + " TEXT ,"
                + KEY_ESU + " TEXT ,"
                + KEY_UNIT_PRICE + " TEXT ,"
                + KEY_MARGIN + " TEXT ,"
                + KEY_TOTAL_PRICE + " TEXT ,"
                + KEY_WAREHOUSE_ID + " TEXT ,"
                + KEY_STATUS + " TEXT ,"
                + KEY_AVAILABLE_QUANTITY + " TEXT ,"
                + KEY_DISCOUNT + " TEXT ,"
                + KEY_PARENT_ID + " TEXT ,"
                + KEY_FREEBIE_ID + " TEXT ,"
                + KEY_FREEBIE_QTY + " TEXT ,"
                + KEY_PACK_TYPE + " TEXT ,"
                + KEY_IS_CHILD + " TEXT ,"
                + KEY_CART_ID + " TEXT ,"
                + KEY_REMARKS + " TEXT ,"
                + KEY_IS_SLAB + " TEXT ,"
                + KEY_BLOCK_QTY + " TEXT ,"
                + KEY_PROMOTION_ID + " TEXT ,"
                + KEY_PRODUCT_SLAB_ID + " TEXT ,"
                + COL_STAR + " TEXT ,"
                + COL_PACK_LEVEL + " TEXT ,"
                + COL_SLAB_ESU + " TEXT ,"
                + COL_FREEBIE_MPQ + " TEXT ,"
                + COl_FREEBIE_FQ + " TEXT ,"
                + COl_CASHBACK_AMT + " TEXT ,"
                + COL_PACK_STAR + " TEXT ,"
                + KEY_UPDATED_DATE + " TEXT )";
        db.execSQL(cart);

        // Retailers table
        String retailers_table = "CREATE TABLE " + TABLE_RETAILERS + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_CUSTOMER_ID + " TEXT ,"
                + KEY_COMPANY + " TEXT ,"
                + KEY_ADDRESS_1 + " TEXT ,"
                + KEY_ADDRESS_2 + " TEXT ,"
                + KEY_FIRST_NAME + " TEXT ,"
                + KEY_TELEPHONE + " TEXT ,"
                + KEY_NO_OF_SHUTTERS + " TEXT ,"
                + KEY_VOLUME_CLASS + " TEXT ,"
                + KEY_BUSINESS_TYPE + " TEXT ,"
                + KEY_MASTER_MANF + " TEXT ,"
                + KEY_POPUP + " TEXT )";
        db.execSQL(retailers_table);

        //PO Cart table
        String po_cart = "CREATE TABLE " + TABLE_PO_CART + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_CUSTOMER_ID + " TEXT ,"
                + KEY_PRODUCT_ID + " TEXT ,"
                + KEY_QUANTITY + " TEXT ,"
                + KEY_UNIT_PRICE + " TEXT ,"
                + KEY_MARGIN + " TEXT ,"
                + KEY_TOTAL_PRICE + " TEXT ,"
                + KEY_PACK_SIZE + " TEXT ,"
                + KEY_FREEBIE_QTY + " TEXT ,"
                + KEY_FREE_QTY + " TEXT ,"
                + KEY_FB_PACK_SIZE + " TEXT ,"
                + KEY_WAREHOUSE_ID + " TEXT ,"
                + KEY_REMARKS + " TEXT ,"
                + KEY_IS_FREEBIE + " TEXT ,"
                + KEY_UPDATED_DATE + " TEXT ,"
                + KEY_PARENT_ID + " TEXT ,"
                + PRODUCT_PACK_ID + " TEXT ,"
                + FREEBIE_PACK_ID + " TEXT ,"
                + KEY_IS_CHILD + " TEXT )";
        db.execSQL(po_cart);

        // Outlets table
        String outlets_table = "CREATE TABLE " + TABLE_OUTLETS + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_CUSTOMER_ID + " TEXT ,"
                + KEY_COMPANY + " TEXT ,"
                + KEY_ADDRESS_1 + " TEXT ,"
                + KEY_ADDRESS_2 + " TEXT ,"
                + KEY_FIRST_NAME + " TEXT ,"
                + KEY_TELEPHONE + " TEXT ,"
                + KEY_NO_OF_SHUTTERS + " TEXT ,"
                + KEY_VOLUME_CLASS + " TEXT ,"
                + KEY_BUSINESS_TYPE + " TEXT ,"
                + KEY_MASTER_MANF + " TEXT ,"
                + KEY_BEAT_ID + " TEXT ,"
                + KEY_IS_CHECKIN + " TEXT ,"
                + KEY_IS_BILLED + " TEXT ,"
                + KEY_POPUP + " TEXT )";
        db.execSQL(outlets_table);

        String CREATE_CUSTOMER_FEEDBACK = "CREATE TABLE " + TABLE_CUSTOMER_FEEDBACK + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_CUSTOMER_FEEDBACK);

        String CREATE_EXPENSES_TABLE = "CREATE TABLE " + TABLE_UNCLAIMED_EXPENSES + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_AMOUNT + " TEXT ,"
                + KEY_DATE + " TEXT ,"
                + KEY_EXPENSE_TYPE + " TEXT ,"
                + KEY_EXPENSE_TYPE_ID + " TEXT ,"
                + KEY_DESCRIPTION + " TEXT ,"
                + KEY_ATTACHMENTS + " TEXT ,"
                + KEY_CREATED_DATE_TIME + " TEXT )";
        db.execSQL(CREATE_EXPENSES_TABLE);

        String CREATE_FEATURES_TABLE = "CREATE TABLE " + TABLE_FEATURES + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_FEATURE_CODE + " TEXT ,"
                + KEY_FEATURE_NAME + " TEXT ,"
                + KEY_FEATURE_IS_PARENT + " TEXT ,"
                + KEY_FEATURE_PARENT + " TEXT ,"
                + KEY_UPDATED_DATE + " TEXT )";
        db.execSQL(CREATE_FEATURES_TABLE);

        String CREATE_STAR = "CREATE TABLE " + TABLE_STAR + "("
                + COL_CUSTOMER_TYPE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + COL_CUSTOMER_GRP_ID + " TEXT ,"
                + COL_CUSTOMER_NAME + " TEXT ,"
                + COL_DESCRIPTION + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_STAR);

        String CREATE_PRODUCT_SLAB_DATA = "CREATE TABLE " + TABLE_PRODUCT_SLAB_DATA + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_CUSTOMER_ID + " TEXT ,"
                + KEY_PRODUCT_ID + " TEXT ,"
                + COL_PACK_LEVEL + " TEXT ,"
                + COL_SLAB_ESU + " TEXT ,"
                + KEY_QUANTITY + " TEXT ,"
                + KEY_PACK_QUANTITY + " TEXT ,"
                + KEY_PACK_SIZE + " TEXT ,"
                + COL_STAR + " TEXT ,"
                + COl_CASH_ID + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_PRODUCT_SLAB_DATA);

        String CREATE_DISCOUNT = "CREATE TABLE " + TABLE_DISCOUNT + "("
                + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_DISCOUNT_TYPE + " TEXT ,"
                + KEY_DISCOUNT_ON + " TEXT ,"
                + KEY_DISCOUNT_ON_VALUE + " INTEGER ,"
                + KEY_DISCOUNT + " TEXT ,"
                + COL_SYNC_TIME + " TEXT )";
        db.execSQL(CREATE_DISCOUNT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUSINESS_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRANDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HOME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HOME_CHILD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MANUFACTURERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BANNERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VOLUME_CLASS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LICENSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MASTER_MANF);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FF_COMMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PREF_SLOTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PO_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RETAILERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OUTLETS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_FEEDBACK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UNCLAIMED_EXPENSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FEATURES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STAR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_SLAB_DATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DISCOUNT);

        this.onCreate(db);
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getCategoryName(String categoryId) {
        String categoryName = "";
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_CATEGORY + " WHERE " + COL_CATEGORY + " = '" + categoryId + "' AND " + COL_PARENT_ID + " = '0'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        //Log.e("count",""+cursor.getCount());
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    categoryName = cursor.getString(cursor.getColumnIndex(COL_CATEGORY_NAME));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return categoryName;
    }

    public String getCategoryImage(String categoryId) {
        String categoryImage = null;
        String query = "SELECT " + COL_CATEGORY_IMAGE + " FROM " + TABLE_CATEGORY + " WHERE " + COL_CATEGORY + " = '" + categoryId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
//        Log.e("count", "" + cursor.getCount());
        try {
            if (cursor.moveToFirst()) {
                do {
                    categoryImage = cursor.getString(cursor.getColumnIndex(COL_CATEGORY_IMAGE));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return categoryImage;
    }

    public ArrayList<CustomerTyepModel> getAllCustomerTypes() {
        CustomerTyepModel model = null;
        ArrayList<CustomerTyepModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_CUSTOMER_TYPE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new CustomerTyepModel();
                    model.setCustomerName(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME)));
                    model.setCustomerGrpId(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_GRP_ID)));
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public int getCategoryCount(String pastDate) {
        int count = 0;
        String query = "SELECT  COUNT(*) FROM " + TABLE_CATEGORY + " WHERE (" + COL_CATEGORY_FLAG + " = '" + ConstantValues.SEE_ALL + "' and "
                + COL_SYNC_TIME + " >= '" + pastDate + "')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    count = cursor.getInt(cursor.getColumnIndex(COL_CATEGORY_ID));
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return count;
    }

    public ArrayList<TopBrandsModel> getHomeCategories(String pastDate) {
        TopBrandsModel model = null;
        ArrayList<TopBrandsModel> arr = new ArrayList<>();
        ArrayList<String> arrImages = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_CATEGORY + " WHERE (" + COL_CATEGORY_FLAG + " = '" + ConstantValues.SEE_ALL + "' and "
                + COL_SYNC_TIME + " >= '" + pastDate + "')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new TopBrandsModel();
                    model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_ID)));
                    model.setTopBrandName(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_NAME)));
                    String images = cursor.getString(cursor.getColumnIndex(COL_CATEGORY_IMAGE));
                    if (images != null && images.length() > 0)
                        arrImages = new ArrayList<String>(Arrays.asList(images.split("\\s*,\\s*")));
                    model.setTopBrandImages(arrImages);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public int getManufacturerCOunt(String pastDate) {
        int count = 0;
        String query = "SELECT  COUNT(*) FROM " + TABLE_MANUFACTURERS + " WHERE (" + COL_MFG_FLAG + " = '" + ConstantValues.SEE_ALL + "' and "
                + COL_SYNC_TIME + " >= '" + pastDate + "')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    count = cursor.getInt(cursor.getColumnIndex(COL_MFG_ID));
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return count;
    }

    public ArrayList<TopBrandsModel> getHomeManufacturers(String pastDate) {
        TopBrandsModel model = null;
        ArrayList<TopBrandsModel> arr = new ArrayList<>();
        ArrayList<String> arrImages = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_MANUFACTURERS + " WHERE (" + COL_MFG_FLAG + " = '" + ConstantValues.SEE_ALL + "' and "
                + COL_SYNC_TIME + " >= '" + pastDate + "')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new TopBrandsModel();
                    model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_MFG_ID)));
                    model.setTopBrandName(cursor.getString(cursor.getColumnIndex(COL_MFG_NAME)));
                    String images = cursor.getString(cursor.getColumnIndex(COL_MFG_LOGO));
                    if (images != null && images.length() > 0)
                        arrImages = new ArrayList<String>(Arrays.asList(images.split("\\s*,\\s*")));
                    model.setTopBrandImages(arrImages);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public int getBrandCount(String pastDate) {
        int count = 0;
        String query = "SELECT  COUNT(*) FROM " + TABLE_BRANDS + " WHERE (" + COL_BRAND_FLAG + " = '" + ConstantValues.SEE_ALL + "' and "
                + COL_SYNC_TIME + " >= '" + pastDate + "')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    count = cursor.getInt(cursor.getColumnIndex(COL_BRAND_ID));
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return count;
    }

    public ArrayList<TopBrandsModel> getHomeBrands(String pastDate) {
        TopBrandsModel model;
        ArrayList<TopBrandsModel> arr = new ArrayList<>();
        ArrayList<String> arrImages = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_BRANDS + " WHERE (" + COL_BRAND_FLAG + " = '" + ConstantValues.SEE_ALL + "' and "
                + COL_SYNC_TIME + " >= '" + pastDate + "')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new TopBrandsModel();
                    model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_BRAND_ID)));
                    model.setTopBrandName(cursor.getString(cursor.getColumnIndex(COL_BRAND_NAME)));
                    String images = cursor.getString(cursor.getColumnIndex(COL_BRAND_LOGO));
                    if (images != null && images.length() > 0)
                        arrImages = new ArrayList<>(Arrays.asList(images.split("\\s*,\\s*")));
                    model.setTopBrandImages(arrImages);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<BannerModel> getAllBanners() {
        BannerModel model;
        ArrayList<BannerModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_BANNERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new BannerModel();
                    model.setTitle(cursor.getString(cursor.getColumnIndex(COL_BNR_NAME)));
                    model.setBannerId(cursor.getString(cursor.getColumnIndex(COL_BNR_ID)));
                    model.setImage(cursor.getString(cursor.getColumnIndex(COL_BNR_LOGO)));
                    model.setNavigatorObject(cursor.getString(cursor.getColumnIndex(COL_BNR_NAV_OBJ)));
                    model.setNavigatorObjectId(cursor.getString(cursor.getColumnIndex(COL_BNR_NAV_OBJ_ID)));
                    model.setFrequency(cursor.getString(cursor.getColumnIndex(COL_BNR_FREQ)));
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public String getManfNames(String manfIds) {
        String manfNames = "";
        String query = " select group_concat(v1,',') from (select " + COL_CUSTOMER_NAME + " as v1 from " + TABLE_MASTER_MANF + " where " + COL_CUSTOMER_GRP_ID + " in (" + manfIds + "))";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                manfNames = cursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return manfNames;
    }

    public ArrayList<CustomerTyepModel> getMasterLookUpData(String tableName) {
        CustomerTyepModel model;
        ArrayList<CustomerTyepModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + tableName;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new CustomerTyepModel();
                    model.setCustomerName(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME)));
                    model.setCustomerGrpId(cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_GRP_ID)));
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<BusinessTypeModel> getAllSegments() {
        BusinessTypeModel model;
        ArrayList<BusinessTypeModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_BUSINESS_TYPE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new BusinessTypeModel();
                    model.setSegmentId(cursor.getString(cursor.getColumnIndex(COL_SEGMENT_ID)));
                    model.setSegmentName(cursor.getString(cursor.getColumnIndex(COL_SEGMENT_NAME)));
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<CategoryModel> getAllCategories() {
        CategoryModel model;
        ArrayList<CategoryModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_CATEGORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new CategoryModel();
                    model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_CATEGORY)));
                    model.setSegmentId(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_SEGMENT_ID)));
                    model.setCategoryImage(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_IMAGE)));
                    model.setCategoryName(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_NAME)));
                    model.setProductNo(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NO)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PARENT_ID)));
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public long insertSegment(BusinessTypeModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_SEGMENT_ID, model.getSegmentId());
            values.put(COL_SEGMENT_NAME, model.getSegmentName());
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_BUSINESS_TYPE, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("segment type inserted", "" + row_id);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertRetailer(RetailersModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_CUSTOMER_ID, model.getCustomerId());
            values.put(KEY_COMPANY, model.getCompany());
            values.put(KEY_FIRST_NAME, model.getFirstName());
            values.put(KEY_ADDRESS_1, model.getAddress1());
            values.put(KEY_ADDRESS_2, model.getAddress2());
            values.put(KEY_TELEPHONE, model.getTelephone());
            values.put(KEY_NO_OF_SHUTTERS, model.getNoOfShutters());
            values.put(KEY_VOLUME_CLASS, model.getVolumeClass());
            values.put(KEY_BUSINESS_TYPE, model.getBusinessTypeId());
            values.put(KEY_MASTER_MANF, model.getMasterManfIds());
            values.put(KEY_POPUP, model.isPopup() ? 1 : 0);


            try {
                row_id = db.insert(TABLE_RETAILERS, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertOutlet(RetailersModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_CUSTOMER_ID, model.getCustomerId());
            values.put(KEY_COMPANY, model.getCompany());
            values.put(KEY_FIRST_NAME, model.getFirstName());
            values.put(KEY_ADDRESS_1, model.getAddress1());
            values.put(KEY_ADDRESS_2, model.getAddress2());
            values.put(KEY_TELEPHONE, model.getTelephone());
            values.put(KEY_NO_OF_SHUTTERS, model.getNoOfShutters());
            values.put(KEY_VOLUME_CLASS, model.getVolumeClass());
            values.put(KEY_BUSINESS_TYPE, model.getBusinessTypeId());
            values.put(KEY_MASTER_MANF, model.getMasterManfIds());
            values.put(KEY_BEAT_ID, model.getBeatId());
            values.put(KEY_IS_CHECKIN, model.isCheckIn() ? 1 : 0);
            values.put(KEY_IS_BILLED, model.isBilled() ? 1 : 0);
            values.put(KEY_POPUP, model.isPopup() ? 1 : 0);

            try {
                row_id = db.insert(TABLE_OUTLETS, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }


    public long insertProduct(TestProductModel model, String pastDate) {
        long row_id = -1;

        if (model != null) {

            deleteRow(TABLE_PRODUCTS, COL_PRODUCT_ID, model.getProductId());

            // syncTime == null, Record does not exists in the DB, Insert now.
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_PRODUCT_ID, model.getProductId());
            values.put(COL_PRODUCT_IMAGE, model.getPrimaryImage());
            values.put(COL_THUMBNAIl_IMAGE, model.getThumbnailImage());
            values.put(COL_PRODUCT_TITLE, model.getProductTitle().trim());
            values.put(COL_PRODUCT_CAT_ID, model.getCategoryId());
            values.put(COL_PRODUCT_BRAND_ID, model.getBrandId());
            values.put(COL_PRODUCT_MANF_ID, model.getManufacturerId());
            values.put(COL_PRODUCT_MRP, model.getMrp());
            values.put(COL_PRODUCT_VARIANT1, model.getVariantValue1());
            values.put(COL_PRODUCT_VARIANT2, model.getVariantValue2());
            values.put(COL_PRODUCT_VARIANT3, model.getVariantValue3());
            values.put(COL_PRODUCT_FREEBIE, model.getFreebie());
            values.put(COL_PRODUCT_IS_PARENT, model.getIsParent());
            values.put(COL_PRODUCT_KEY_INDEX, model.getKeyValueIndex());
            values.put(COL_PRODUCT_PARENT_ID, model.getParentId());
            values.put(COL_PRODUCT_INV, model.getInventory());
            values.put(COL_PRODUCT_META_KEYWORDS, model.getMetaKeywords());
            values.put(COL_ESU, model.getEsu());
            values.put(COL_PACK_TYPE, model.getPackType());
            values.put(COL_SKU_CODE, model.getSKUCode());
            values.put(COL_STAR, model.getStar());
            values.put(COl_IS_CASHBACK, model.isCashback());
            values.put(COL_SYNC_TIME, getDateTime());

            try {
                row_id = db.insert(TABLE_PRODUCTS, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("product inserted", "" + row_id);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertProductInTemp(String model) {
        long row_id = -1;

        if (TextUtils.isEmpty(model)) {
            return row_id;
        }

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            for (String s : model.split(",")) {
                ContentValues values = new ContentValues();
                values.put(COL_PRODUCT_ID, s);
                row_id = db.insert(TABLE_TEMP_PRODUCTS, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
//                Log.e("product in temp", "" + row_id);
            }

        } catch (Exception e) {
            row_id = -1;
        } finally {
            db.close();
        }

        return row_id;
    }

    public TestProductModel getFreebieData(String productId) {
        TestProductModel testProductModel = null;
        String query = "SELECT " + COL_STAR + "," + COL_ESU + " FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_ID + " = '" + productId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                testProductModel = new TestProductModel();
                testProductModel.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)) == null ? "" : cursor.getString(cursor.getColumnIndex(COL_STAR)));
                testProductModel.setEsu(cursor.getString(cursor.getColumnIndex(COL_ESU)) == null ? "" : cursor.getString(cursor.getColumnIndex(COL_ESU)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return testProductModel;
    }

    public ArrayList<TestProductModel> getProducts(String parentId) {
        TestProductModel model;
        ArrayList<TestProductModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_PARENT_ID + " = '" + parentId + "'";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new TestProductModel();
                    model.setProductId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    model.setPrimaryImage(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_IMAGE)));
                    model.setThumbnailImage(cursor.getString(cursor.getColumnIndex(COL_THUMBNAIl_IMAGE)));
                    model.setProductTitle(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TITLE)));
                    model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CAT_ID)));
                    model.setBrandId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND_ID)));
                    model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MANF_ID)));
                    model.setMrp(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                    model.setVariantValue1(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT1)));
                    model.setVariantValue2(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT2)));
                    model.setVariantValue3(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT3)));
                    model.setFreebie(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_FREEBIE)));
                    model.setIsParent(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_IS_PARENT)));
                    model.setKeyValueIndex(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_KEY_INDEX)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_PARENT_ID)));
                    model.setInventory(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_INV)));
                    model.setMetaKeywords(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_META_KEYWORDS)));
                    model.setEsu(cursor.getString(cursor.getColumnIndex(COL_ESU)));
                    model.setPackType(cursor.getString(cursor.getColumnIndex(COL_PACK_TYPE)));
                    model.setSKUCode(cursor.getString(cursor.getColumnIndex(COL_SKU_CODE)));
                    model.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)));
                    model.setCashback(cursor.getInt(cursor.getColumnIndex(COl_IS_CASHBACK)) == 1);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<RetailersModel> getRetailers() {
        RetailersModel model = null;
        ArrayList<RetailersModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_RETAILERS + " ORDER BY " + KEY_COMPANY + " ASC";


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new RetailersModel();
                    model.setCompany(cursor.getString(cursor.getColumnIndex(KEY_COMPANY)));
                    model.setAddress1(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS_1)));
                    model.setAddress2(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS_2)));
                    model.setFirstName(cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME)));
                    model.setTelephone(cursor.getString(cursor.getColumnIndex(KEY_TELEPHONE)));
                    model.setCustomerId(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ID)));
                    model.setNoOfShutters(cursor.getString(cursor.getColumnIndex(KEY_NO_OF_SHUTTERS)));
                    model.setVolumeClass(cursor.getString(cursor.getColumnIndex(KEY_VOLUME_CLASS)));
                    model.setBusinessTypeId(cursor.getString(cursor.getColumnIndex(KEY_BUSINESS_TYPE)));
                    model.setMasterManfIds(cursor.getString(cursor.getColumnIndex(KEY_MASTER_MANF)));
                    model.setPopup(cursor.getInt(cursor.getColumnIndex(KEY_POPUP)) == 1);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<RetailersModel> getOutlets() {
        RetailersModel model = null;
        ArrayList<RetailersModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_OUTLETS + " ORDER BY " + KEY_COMPANY + " ASC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new RetailersModel();
                    model.setCompany(cursor.getString(cursor.getColumnIndex(KEY_COMPANY)));
                    model.setAddress1(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS_1)));
                    model.setAddress2(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS_2)));
                    model.setFirstName(cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME)));
                    model.setTelephone(cursor.getString(cursor.getColumnIndex(KEY_TELEPHONE)));
                    model.setCustomerId(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ID)));
                    model.setNoOfShutters(cursor.getString(cursor.getColumnIndex(KEY_NO_OF_SHUTTERS)));
                    model.setVolumeClass(cursor.getString(cursor.getColumnIndex(KEY_VOLUME_CLASS)));
                    model.setBusinessTypeId(cursor.getString(cursor.getColumnIndex(KEY_BUSINESS_TYPE)));
                    model.setMasterManfIds(cursor.getString(cursor.getColumnIndex(KEY_MASTER_MANF)));
                    model.setBeatId(cursor.getString(cursor.getColumnIndex(KEY_BEAT_ID)));
                    model.setIsCheckIn(cursor.getInt(cursor.getColumnIndex(KEY_IS_CHECKIN)) == 1);
                    model.setIsBilled(cursor.getInt(cursor.getColumnIndex(KEY_IS_BILLED)) == 1);
                    model.setPopup(cursor.getInt(cursor.getColumnIndex(KEY_POPUP)) == 1);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<CategoryModel> getNavigationCategories() {
        CategoryModel model = null;
        ArrayList<CategoryModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_CATEGORY + " WHERE " + COL_PARENT_ID + " = '0' order by " + COL_CATEGORY_NAME;


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new CategoryModel();
                    model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_CATEGORY)));
                    model.setCategoryName(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_NAME)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PARENT_ID)));
                    model.setSegmentId(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_SEGMENT_ID)));
                    model.setProductNo(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NO)));
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<HomeModel> getHomeData(String pastDate) {
        HomeModel model;
        ArrayList<HomeModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_HOME + " WHERE " + COL_SYNC_TIME + " >= '" + pastDate + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new HomeModel();
                    model.setFlag(cursor.getString(cursor.getColumnIndex(COL_HOME_FLAG)));
                    model.setTitle(cursor.getString(cursor.getColumnIndex(COL_HOME_DISPLAY_TITLE)));
                    model.setKey(cursor.getString(cursor.getColumnIndex(COL_HOME_KEY)));
                    ArrayList<HomeDataModel> arrHomeChild = getHomeDataChild(cursor.getString(cursor.getColumnIndex(COL_HOME_DISPLAY_TITLE)), pastDate);
                    model.setModelArrayList(arrHomeChild);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<HomeDataModel> getHomeDataChild(String displayTitle, String pastDate) {
        HomeDataModel model = null;
        ArrayList<HomeDataModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_HOME_CHILD + " WHERE " + COL_HOME_CHILD_TITLE + " = '" + displayTitle + "' and "
                + COL_SYNC_TIME + " >= '" + pastDate + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new HomeDataModel();
                    model.setId(cursor.getString(cursor.getColumnIndex(COL_HOME_CHILD_ID)));
                    model.setName(cursor.getString(cursor.getColumnIndex(COL_HOME_CHILD_NAME)));
                    model.setDisplayTitle(cursor.getString(cursor.getColumnIndex(COL_HOME_CHILD_TITLE)));
                    model.setFlag(cursor.getString(cursor.getColumnIndex(COL_HOME_CHILD_TITLE)));
                    model.setKey(cursor.getString(cursor.getColumnIndex(COL_HOME_CHILD_KEY)));
                    String images = cursor.getString(cursor.getColumnIndex(COL_HOME_CHILD_IMAGE));
                    ArrayList<String> arrImages = new ArrayList<String>(Arrays.asList(images.split("\\s*,\\s*")));
                    model.setArrImages(arrImages);
                    arr.add(model);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public ArrayList<CategoryModel> getNavigationSubcategories(String categoryId) {
        CategoryModel model;
        ArrayList<CategoryModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_CATEGORY + " WHERE " + COL_PARENT_ID + " = '" + categoryId + "' order by " + COL_CATEGORY_NAME;


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new CategoryModel();
                    model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_CATEGORY)));
                    model.setCategoryName(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_NAME)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PARENT_ID)));
                    model.setSegmentId(cursor.getString(cursor.getColumnIndex(COL_CATEGORY_SEGMENT_ID)));
                    model.setProductNo(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NO)));
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public long insertCategory(CategoryModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CATEGORY, model.getCategoryId());
            values.put(COL_CATEGORY_SEGMENT_ID, model.getSegmentId());
            values.put(COL_PARENT_ID, model.getParentId());
            values.put(COL_PARENT_ID, model.getParentId());
            values.put(COL_CATEGORY_IMAGE, model.getCategoryImage());
            values.put(COL_CATEGORY_NAME, model.getCategoryName());
            values.put(COL_PRODUCT_NO, model.getProductNo());
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_CATEGORY, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
//                Log.e("categry inserted", "" + row_id);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (db != null)
                    db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertHomeCategory(TopBrandsModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CATEGORY_ID, model.getManufacturerId());
            values.put(COL_CATEGORY_NAME, model.getTopBrandName());
            String images = TextUtils.join(",", model.getTopBrandImages());
            values.put(COL_CATEGORY_IMAGE, images);
            values.put(COL_CATEGORY_FLAG, ConstantValues.SEE_ALL);
            values.put(COL_SYNC_TIME, getDateTime());
            try {
                row_id = db.insert(TABLE_CATEGORY, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("category inserted", "" + row_id);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (db != null)
                    db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertManufacturer(TopBrandsModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_MFG_ID, model.getManufacturerId());
            values.put(COL_MFG_NAME, model.getTopBrandName());
            String images = TextUtils.join(",", model.getTopBrandImages());
            values.put(COL_MFG_LOGO, images);
            values.put(COL_MFG_FLAG, ConstantValues.SEE_ALL);
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_MANUFACTURERS, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("manufacturers inserted", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertBrand(TopBrandsModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_BRAND_ID, model.getManufacturerId());
            values.put(COL_BRAND_NAME, model.getTopBrandName());
            String images = TextUtils.join(",", model.getTopBrandImages());
            values.put(COL_BRAND_LOGO, images);
            values.put(COL_BRAND_FLAG, ConstantValues.SEE_ALL);
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_BRANDS, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("brand inserted", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertBanner(BannerModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_BNR_NAME, model.getTitle());
            values.put(COL_BNR_ID, model.getBannerId());
            values.put(COL_BNR_LOGO, model.getImage());
            values.put(COL_BNR_NAV_OBJ, model.getNavigatorObject());
            values.put(COL_BNR_NAV_OBJ_ID, model.getNavigatorObjectId());
            values.put(COL_BNR_FREQ, model.getFrequency());
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_BANNERS, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("banner inserted", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertHome(HomeModel model) {
        long row_id = -1;
        if (model != null && model.getModelArrayList() != null) {

            ArrayList<HomeDataModel> arrHomeChild = model.getModelArrayList();
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_HOME_FLAG, model.getFlag());
            values.put(COL_HOME_DISPLAY_TITLE, model.getTitle());
            values.put(COL_HOME_KEY, model.getKey());
            values.put(COL_HOME_ITEMS, arrHomeChild.size());
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_HOME, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("home inserted", "" + row_id);
                if (arrHomeChild != null && arrHomeChild.size() > 0) {
                    for (int i = 0; i < arrHomeChild.size(); i++) {
                        insertHomeChild(arrHomeChild.get(i));
                    }
                }
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public long insertHomeChild(HomeDataModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_HOME_CHILD_ID, model.getId());
            values.put(COL_HOME_CHILD_NAME, model.getName());
            String images = TextUtils.join(",", model.getArrImages());
            values.put(COL_HOME_CHILD_IMAGE, images);
            values.put(COL_HOME_CHILD_TITLE, model.getDisplayTitle());
            values.put(COL_HOME_CHILD_FLAG, model.getFlag());
            values.put(COL_HOME_CHILD_KEY, model.getKey());
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_HOME_CHILD, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("home child inserted", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            return -2;
        }
        return row_id;
    }

    public long insertDiscount(DiscountModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_DISCOUNT_TYPE, model.getDiscountType());
            values.put(KEY_DISCOUNT_ON, model.getDiscountOn());
            values.put(KEY_DISCOUNT_ON_VALUE, model.getDiscountValues());
            values.put(KEY_DISCOUNT, model.getDiscount());
            values.put(COL_SYNC_TIME, getDateTime());


            try {
                row_id = db.insert(TABLE_DISCOUNT, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("discount inserted", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            return -2;
        }
        return row_id;
    }

    public long insertManf(CustomerTyepModel model, String tableName) {
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COL_CUSTOMER_NAME, model.getCustomerName());
            values.put(COL_CUSTOMER_GRP_ID, model.getCustomerGrpId());
            values.put(COL_SYNC_TIME, getDateTime());
            if (tableName.equalsIgnoreCase(DBHelper.TABLE_STAR))
                values.put(COL_DESCRIPTION, model.getDescription());

            long row_id = -1;
            try {
                row_id = db.insert(tableName, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
            } catch (Exception e) {
                row_id = -1;
            } finally {

                db.close();
                return row_id;
            }
        } else {
            return -2;
        }

    }


    public void deleteTable(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + tableName);
    }

    public String getValue(String tableName, String colName, String whereArgs, String whereColumn, String customerId) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT " + colName + " FROM " + tableName + " WHERE " + whereColumn +
                " ='" + whereArgs + "' and customer_id = '" + customerId + "'";
        Cursor c = db.rawQuery(query, null);

        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                return c.getString(c.getColumnIndex(colName));
            } else {
                return null;
            }
        } finally {
            if (c != null)
                c.close();
        }

    }

    public long getCount() {
        long cnt = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT COUNT(*) FROM " + TABLE_CATEGORY;
        Cursor c = db.rawQuery(query, null);

        try {
            if (c != null && c.moveToFirst()) {
                cnt = Integer.parseInt(c.getString(0));
            }
        } finally {
            if (c != null)
                c.close();
        }

        db.close();

        return cnt;
    }

    public ArrayList<TestProductModel> getParentProducts(String key, String value) {
        TestProductModel model;
        ArrayList<TestProductModel> arr = new ArrayList<>();
        Cursor cursor = null;
        // 1. build the query
        String query = "";
        if (!TextUtils.isEmpty(key)) {
            query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_PARENT_ID + " = " + COL_PRODUCT_ID + " AND " + key + "='" + value + "'";
        } else {
            query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_PARENT_ID + " = " + COL_PRODUCT_ID;
        }

        SQLiteDatabase db = this.getWritableDatabase();

        try {
            cursor = db.rawQuery(query, null);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new TestProductModel();
                    model.setProductId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    model.setPrimaryImage(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_IMAGE)));
                    model.setThumbnailImage(cursor.getString(cursor.getColumnIndex(COL_THUMBNAIl_IMAGE)));
                    model.setProductTitle(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TITLE)));
                    model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CAT_ID)));
                    model.setBrandId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND_ID)));
                    model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MANF_ID)));
                    model.setMrp(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                    model.setVariantValue1(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT1)));
                    model.setVariantValue2(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT2)));
                    model.setVariantValue3(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT3)));
                    model.setFreebie(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_FREEBIE)));
                    model.setIsParent(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_IS_PARENT)));
                    model.setKeyValueIndex(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_KEY_INDEX)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_PARENT_ID)));
                    model.setInventory(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_INV)));
                    model.setMetaKeywords(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_META_KEYWORDS)));
                    model.setEsu(cursor.getString(cursor.getColumnIndex(COL_ESU)));
                    model.setPackType(cursor.getString(cursor.getColumnIndex(COL_PACK_TYPE)));
                    model.setSKUCode(cursor.getString(cursor.getColumnIndex(COL_SKU_CODE)));
                    model.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)));
                    model.setCashback(cursor.getInt(cursor.getColumnIndex(COl_IS_CASHBACK)) == 1);
                    String variantList = getVariant1List(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    model.setPrimaryVariants(variantList);
                    if (variantList != null && variantList.length() > 0)
                        model.setVariantModelArrayList(new ArrayList<String>(Arrays.asList(variantList.split("\\s*,\\s*"))));

                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public String getVariantName(String productId, String variantName1, String variantName2,
                                 String variantName3, String keyColumn) {
        String name = "";
        Cursor cursor;
        String query = "SELECT " + keyColumn + " FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_ID + " = '" + productId + "'";
        if (!TextUtils.isEmpty(variantName1)) {
            variantName1 = variantName1.replaceAll("'", "''");
            query += " and " + COL_PRODUCT_VARIANT1 + " = '" + variantName1 + "'";
        }
        if (!TextUtils.isEmpty(variantName2)) {
            variantName2 = variantName2.replaceAll("'", "''");
            query += " and " + COL_PRODUCT_VARIANT2 + " = '" + variantName2 + "'";
        }
        if (!TextUtils.isEmpty(variantName3)) {
            variantName3 = variantName3.replaceAll("'", "''");
            query += " and " + COL_PRODUCT_VARIANT3 + " = '" + variantName3 + "'";
        }

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            cursor = db.rawQuery(query, null);

            if (cursor != null && cursor.moveToFirst()) {
                name = cursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    public TestProductModel getParentProductDetails(String productId, String variantName1, String variantName2,
                                                    String variantName3) {
        TestProductModel model = null;
        Cursor cursor = null;
        variantName1 = variantName1.replace("'", "''");
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_PARENT_ID + " = '" + productId + "' and "
                + COL_PRODUCT_VARIANT1 + " = '" + variantName1 + "'";
        if (!TextUtils.isEmpty(variantName2)) {
            variantName2 = variantName2.replace("'", "''");
            query += " and " + COL_PRODUCT_VARIANT2 + " = '" + variantName2 + "'";
        }
        if (!TextUtils.isEmpty(variantName3)) {
            variantName3 = variantName3.replace("'", "''");
            query += " and " + COL_PRODUCT_VARIANT3 + " = '" + variantName3 + "'";
        }

        SQLiteDatabase db = this.getWritableDatabase();

        try {
            cursor = db.rawQuery(query, null);

            if (cursor != null && cursor.moveToFirst()) {
                model = new TestProductModel();
                model.setProductId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                model.setPrimaryImage(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_IMAGE)));
                model.setThumbnailImage(cursor.getString(cursor.getColumnIndex(COL_THUMBNAIl_IMAGE)));
                model.setProductTitle(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TITLE)));
                model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CAT_ID)));
                model.setBrandId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND_ID)));
                model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MANF_ID)));
                model.setMrp(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                model.setVariantValue1(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT1)));
                model.setVariantValue2(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT2)));
                model.setVariantValue3(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT3)));
                model.setFreebie(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_FREEBIE)));
                model.setIsParent(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_IS_PARENT)));
                model.setKeyValueIndex(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_KEY_INDEX)));
                model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_PARENT_ID)));
                model.setInventory(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_INV)));
                model.setMetaKeywords(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_META_KEYWORDS)));
                model.setEsu(cursor.getString(cursor.getColumnIndex(COL_ESU)));
                model.setPackType(cursor.getString(cursor.getColumnIndex(COL_PACK_TYPE)));
                model.setSKUCode(cursor.getString(cursor.getColumnIndex(COL_SKU_CODE)));
                model.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)));
                model.setCashback(cursor.getInt(cursor.getColumnIndex(COl_IS_CASHBACK)) == 1);
                String variantList = getVariant1List(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                model.setPrimaryVariants(variantList);
                if (variantList != null && variantList.length() > 0)
                    model.setVariantModelArrayList(new ArrayList<String>(Arrays.asList(variantList.split("\\s*,\\s*"))));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return model;
    }

    public String getVariant1List(String productId) {
        String variant = "";
        Cursor variantsCursor = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String variantsQuery = "select group_concat(v1 ||':'|| s1,',') from (SELECT distinct product_variant_value1 as v1,star as s1 FROM table_products WHERE " +
                    "(product_parent_id = '" + productId + "' or  product_id = '" + productId + "') )";
            variantsCursor = db.rawQuery(variantsQuery, null);

            if (variantsCursor != null && variantsCursor.moveToFirst()) {
                variant = variant + variantsCursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (variantsCursor != null)
                variantsCursor.close();
        }
        return variant;
    }

    public String getVariantForChild(String productId) {
        String variant = "";
        Cursor variantsCursor = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String variantsQuery = "select product_variant_value1, product_variant_value2, " +
                    "product_variant_value3,star from table_products where product_id = '" + productId + "'";
            variantsCursor = db.rawQuery(variantsQuery, null);

            if (variantsCursor != null && variantsCursor.moveToFirst()) {

                String var1 = variantsCursor.getString(0);
                String var2 = variantsCursor.getString(1);
                String var3 = variantsCursor.getString(2);
                String star = variantsCursor.getString(3);

                if (null != var3 && !TextUtils.isEmpty(var3)) {
                    variant = var3;
                } else if (null != var2 && !TextUtils.isEmpty(var2)) {
                    variant = var2;
                } else {
                    variant = var1;
                }
                if (!TextUtils.isEmpty(variant))
                    variant += ":" + star;
//                variant = variant + variantsCursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (variantsCursor != null)
                variantsCursor.close();
        }
        return variant;
    }

    public String getVariant2List(String productId, String varaint1Name) {
        String variant = "";
        varaint1Name = varaint1Name.replaceAll("'", "''");
        try {
            SQLiteDatabase db = this.getWritableDatabase();

//            String variantsQuery = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE (" + COL_PRODUCT_PARENT_ID + " = '"
//                    + productId + "' or  "+
//                    COL_PRODUCT_ID + " = '"+ productId + "') group by product_variant_value1";
            String variantsQuery = "select group_concat(v1 ||':'|| s1,',') from (SELECT distinct product_variant_value2 as v1,star as s1 FROM table_products WHERE " +
                    "(product_parent_id = '" + productId + "' and product_variant_value1 = '" + varaint1Name + "' and product_variant_value2 != 'null' and product_variant_value2 != ''))";
            Cursor variantsCursor = db.rawQuery(variantsQuery, null);

            if (variantsCursor != null && variantsCursor.moveToFirst()) {

                variant = variant + variantsCursor.getString(0);
            }

//                    String images = cursor.getString(cursor.getColumnIndex(COL_CATEGORY_IMAGE));
//                    if (images != null && images.length() > 0)
//                        arrImages = new ArrayList<String>(Arrays.asList(images.split("\\s*,\\s*")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return variant;
    }

    public String getVariant3List(String productId, String varaint1Name, String varaint2Name) {
        String variant = "";
        varaint1Name = varaint1Name.replaceAll("'", "''");
        varaint2Name = varaint2Name.replaceAll("'", "''");
        try {
            SQLiteDatabase db = this.getWritableDatabase();

//            String variantsQuery = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE (" + COL_PRODUCT_PARENT_ID + " = '"
//                    + productId + "' or  "+
//                    COL_PRODUCT_ID + " = '"+ productId + "') group by product_variant_value1";
            String variantsQuery = "select group_concat(v1 ||':'|| s1,',') from (SELECT distinct product_variant_value3 as v1,star as s1 FROM table_products WHERE " +
                    "(product_parent_id = '" + productId + "' and product_variant_value1 = '" + varaint1Name + "' and product_variant_value2 = '"
                    + varaint2Name + "' and product_variant_value3 != 'null'  and product_variant_value3 != ''))";
            Cursor variantsCursor = db.rawQuery(variantsQuery, null);

            if (variantsCursor != null && variantsCursor.moveToFirst()) {

                variant = variant + variantsCursor.getString(0);
            }

//                    String images = cursor.getString(cursor.getColumnIndex(COL_CATEGORY_IMAGE));
//                    if (images != null && images.length() > 0)
//                        arrImages = new ArrayList<String>(Arrays.asList(images.split("\\s*,\\s*")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return variant;
    }


    public void deleteRow(String tableName, String columnName, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + tableName + " WHERE " + columnName + "='" + value + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void deleteOldRecords(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("delete from table_products where last_sync_date <= '" + date + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void deleteOldCart(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("delete from " + TABLE_CART + " where " + KEY_UPDATED_DATE + " <= '" + date + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }


    public String checkTableForRecord(String key, String tableName, String columnName, String value) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
//        boolean foundRecord = false;
        String syncTime = null;
        try {
            cursor = db.rawQuery("SELECT " + key + " FROM " + tableName + " WHERE " + columnName + "=?",
                    new String[]{value});
            if (cursor != null && cursor.moveToFirst()) {
                syncTime = cursor.getString(0);
//                foundRecord = true;
            } else {
//                foundRecord = false;
                syncTime = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return syncTime;
    }

    public String getNotAvailableProductIds() {
        SQLiteDatabase db = getReadableDatabase();
        String missingIds = "";
        Cursor cursor = null;
        try {
            String query = "select group_concat(t.product_id) from " + TABLE_TEMP_PRODUCTS + " t  where t." + COL_PRODUCT_ID +
                    " not in (select " + COL_PRODUCT_ID + " from " + TABLE_PRODUCTS + ")";
            cursor = db.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                missingIds = cursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return missingIds;
    }

    public ArrayList<String> getParentNotAvailableProductIds(String commaSeperatedIds) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<String> missingIds = new ArrayList<>();
        Cursor cursor = null;
        try {

            /*select product_parent_id, count(product_parent_id), product_is_parent,
                    group_concat(product_id)  from table_products
            where product_id in
                    (13,16,33,58,77,97,124,142,17,34,59,79,98,126,144,18,35,60,80,102,127,2,3)
            group by product_parent_id
            having sum(product_is_parent)<1*/

            String query = "select product_parent_id, group_concat(distinct product_id) from "
                    + TABLE_PRODUCTS + " where product_id in (" + commaSeperatedIds + ") " +
                    "group by product_parent_id having sum(product_is_parent)<1";
            cursor = db.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    missingIds.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return missingIds;
    }

    public String getParentId(String productId) {
        String parentId = "";

        String query = "select " + COL_PRODUCT_PARENT_ID + " from " + TABLE_PRODUCTS + " where " + COL_PRODUCT_ID + " ='" + productId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                parentId = cursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parentId;
    }

    public TestProductModel getProductById(String productId, boolean isChild) {
        TestProductModel model = null;

        String query = "";

        if (!isChild) {
            query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_ID + " = '" + productId
                    + "' and " + COL_PRODUCT_PARENT_ID + " = " + COL_PRODUCT_ID;
        } else {
            query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COL_PRODUCT_ID + " = '" + productId
                    + "'";
        }
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                model = new TestProductModel();
                model.setProductId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                model.setPrimaryImage(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_IMAGE)));
                model.setThumbnailImage(cursor.getString(cursor.getColumnIndex(COL_THUMBNAIl_IMAGE)));
                model.setProductTitle(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TITLE)));
                model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CAT_ID)));
                model.setBrandId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND_ID)));
                model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MANF_ID)));
                model.setMrp(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                model.setVariantValue1(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT1)));
                model.setVariantValue2(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT2)));
                model.setVariantValue3(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT3)));
                model.setFreebie(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_FREEBIE)));
                model.setIsParent(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_IS_PARENT)));
                model.setKeyValueIndex(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_KEY_INDEX)));
                model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_PARENT_ID)));
                model.setInventory(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_INV)));
                model.setMetaKeywords(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_META_KEYWORDS)));
                model.setEsu(cursor.getString(cursor.getColumnIndex(COL_ESU)));
                model.setPackType(cursor.getString(cursor.getColumnIndex(COL_PACK_TYPE)));
                model.setSKUCode(cursor.getString(cursor.getColumnIndex(COL_SKU_CODE)));
                model.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)));
                model.setCashback(cursor.getInt(cursor.getColumnIndex(COl_IS_CASHBACK)) == 1);
                if (!isChild) {
                    String variantList = getVariant1List(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    model.setPrimaryVariants(variantList);
                    if (variantList != null && variantList.length() > 0)
                        model.setVariantModelArrayList(new ArrayList<String>(Arrays.asList(variantList.split("\\s*,\\s*"))));
                } else {
                    String variant = getVariantForChild(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    model.setPrimaryVariants(variant);
                    model.setIsChild(true);
                    ArrayList<String> varList = new ArrayList<String>();
                    varList.add(variant);
                    model.setVariantModelArrayList(varList);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    public ArrayList<TestProductModel> getProductsByProductIds(String productIds, boolean areChilds, boolean isSort, String sortParam, String sortOrder) {
        TestProductModel model = null;
        ArrayList<TestProductModel> arr = new ArrayList<>();
        // 1. build the query
//        String query = "select * from " + TABLE_PRODUCTS + " where " + COL_PRODUCT_ID + " in (" + productIds + ")";
        String query = Utils.getQuery(TABLE_PRODUCTS, COL_PRODUCT_ID, COL_PRODUCT_PARENT_ID, productIds, areChilds, isSort, sortParam, sortOrder);

        if (areChilds) {
            query = Utils.getQuery(TABLE_PRODUCTS, COL_PRODUCT_ID, COL_PRODUCT_ID, productIds, areChilds, isSort, sortParam, sortOrder);
        }

//        String query = "select * from " + TABLE_PRODUCTS + " where " + COL_PRODUCT_ID + " in (" + productIds + ") AND "
//                + COL_PRODUCT_PARENT_ID + " = " + COL_PRODUCT_ID;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new TestProductModel();
                    model.setProductId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                    model.setPrimaryImage(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_IMAGE)));
                    model.setThumbnailImage(cursor.getString(cursor.getColumnIndex(COL_THUMBNAIl_IMAGE)));
                    model.setProductTitle(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_TITLE)));
                    model.setCategoryId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_CAT_ID)));
                    model.setBrandId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_BRAND_ID)));
                    model.setManufacturerId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MANF_ID)));
                    model.setMrp(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_MRP)));
                    model.setVariantValue1(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT1)));
                    model.setVariantValue2(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT2)));
                    model.setVariantValue3(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_VARIANT3)));
                    model.setFreebie(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_FREEBIE)));
                    model.setIsParent(cursor.getInt(cursor.getColumnIndex(COL_PRODUCT_IS_PARENT)));
                    model.setKeyValueIndex(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_KEY_INDEX)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_PARENT_ID)));
                    model.setInventory(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_INV)));
                    model.setMetaKeywords(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_META_KEYWORDS)));
                    model.setEsu(cursor.getString(cursor.getColumnIndex(COL_ESU)));
                    model.setPackType(cursor.getString(cursor.getColumnIndex(COL_PACK_TYPE)));
                    model.setSKUCode(cursor.getString(cursor.getColumnIndex(COL_SKU_CODE)));
                    model.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)));
                    model.setCashback(cursor.getInt(cursor.getColumnIndex(COl_IS_CASHBACK)) == 1);
                    if (!areChilds) {
                        String variantList = getVariant1List(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                        model.setPrimaryVariants(variantList);
                        if (variantList != null && variantList.length() > 0)
                            model.setVariantModelArrayList(new ArrayList<String>(Arrays.asList(variantList.split("\\s*,\\s*"))));
                    } else {
                        String variant = getVariantForChild(cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID)));
                        model.setPrimaryVariants(variant);
                        model.setIsChild(true);
                        ArrayList<String> varList = new ArrayList<String>();
                        varList.add(variant);
                        model.setVariantModelArrayList(varList);
                    }


                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public void deleteCategories() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + TABLE_CATEGORY + " WHERE " + COL_CATEGORY_FLAG + "='" + ConstantValues.SEE_ALL + "'");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public long insertProductInCart(CartModel model) {
        if (model != null) {

            ArrayList<ProductSlabData> arrProductSlabData = model.getArrProductSlabData();

            int rowId = checkCartTableForCartId(model.getCustomerId(), KEY_PRODUCT_ID, model.getProductId());
            if (rowId > 0) {
                //Cart Item exists in table, update/delete the record
                deleteCartRow(rowId);
            }

            // rowId <0, Record does not exists in the DB, Insert now.
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_CUSTOMER_ID, model.getCustomerId());
            values.put(KEY_PRODUCT_ID, model.getProductId());
            values.put(KEY_QUANTITY, model.getQuantity());
            values.put(KEY_ESU, model.getEsu());
            values.put(KEY_UNIT_PRICE, model.getUnitPrice());
            values.put(KEY_MARGIN, model.getMargin());
            values.put(KEY_TOTAL_PRICE, model.getTotalPrice());
            values.put(KEY_WAREHOUSE_ID, model.getWarehouseId());
            values.put(KEY_STATUS, 1);
            values.put(KEY_AVAILABLE_QUANTITY, model.getAvailableQty());
            values.put(KEY_DISCOUNT, model.getDiscount());
            values.put(KEY_PARENT_ID, model.getParentId());
            values.put(KEY_FREEBIE_ID, model.getFreebieProductId());
            values.put(KEY_FREEBIE_QTY, model.getFreebieQty());
            values.put(KEY_PACK_TYPE, model.getPackType());
            values.put(KEY_IS_CHILD, model.isChild());
            values.put(KEY_REMARKS, model.getRemarks());
            values.put(KEY_UPDATED_DATE, getDateTime());
            values.put(KEY_CART_ID, model.getCartId());
            values.put(KEY_IS_SLAB, model.getIsSlab());
            values.put(KEY_PROMOTION_ID, model.getPrmtDetId());
            values.put(KEY_PRODUCT_SLAB_ID, model.getProductSlabId());
            values.put(KEY_BLOCK_QTY, model.getBlockedQty());
            values.put(COL_STAR, model.getStar());
            values.put(COL_PACK_LEVEL, model.getPackLevel());
            values.put(COL_SLAB_ESU, model.getSlabEsu());
            values.put(COL_FREEBIE_MPQ, model.getFreebieMpq());
            values.put(COl_FREEBIE_FQ, model.getFreebieFq());
            values.put(COl_CASHBACK_AMT, model.getCashbackAmount());
            values.put(COL_PACK_STAR, model.getPackStar());

            long row_id = -1;
            try {
                row_id = db.insert(TABLE_CART, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("inserted in Cart", "" + row_id);

                deletefromProductSlabs(model.getCustomerId(), KEY_PRODUCT_ID, model.getProductId());

                if (arrProductSlabData != null && arrProductSlabData.size() > 0) {
                    for (int i = 0; i < arrProductSlabData.size(); i++) {
                        insertProductSlabData(arrProductSlabData.get(i));
                    }
                }

                /*if (model.getFreebieProductId() != null && !TextUtils.isEmpty(model.getFreebieProductId()) && !model.getFreebieProductId().equals("0")) {
                    deletefromFreebieSlabs(model.getCustomerId(), KEY_PRODUCT_ID, model.getFreebieProductId());

                    if (arrFreebieSlabData != null && arrFreebieSlabData.size() > 0) {
                        for (int i = 0; i < arrFreebieSlabData.size(); i++) {
                            insertProductSlabData(arrFreebieSlabData.get(i));
                        }
                    }
                }*/

            } catch (Exception e) {
                row_id = -1;
            } finally {

                db.close();
                return row_id;
            }
        } else {
            return -2;
        }
    }

    public long insertProductSlabData(ProductSlabData model) {
        if (model != null) {

            /*int rowId = checkProductSlabTableForId(model.getCustomerId(), KEY_PRODUCT_ID, model.getProductId(), COL_PACK_LEVEL, String.valueOf(model.getLevel()));
            if (rowId > 0) {
                //Product Slab Item exists in table, update/delete the record
                deleteProductSlabRow(rowId);
            }*/

            // rowId <0, Record does not exists in the DB, Insert now.
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_CUSTOMER_ID, model.getCustomerId());
            values.put(KEY_PRODUCT_ID, model.getProductId());
            values.put(COL_PACK_LEVEL, model.getLevel());
            values.put(COL_SLAB_ESU, model.getEsu());
            values.put(KEY_QUANTITY, model.getQty());
            values.put(KEY_PACK_SIZE, model.getPackSize());
            values.put(KEY_PACK_QUANTITY, model.getPackQty());
            values.put(COL_STAR, model.getStar());
            values.put(COl_CASH_ID, model.getCashBackIds());
            values.put(COL_SYNC_TIME, getDateTime());

            long row_id = -1;
            try {
                row_id = db.insert(TABLE_PRODUCT_SLAB_DATA, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("inserted in slab data", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {

                db.close();
                return row_id;
            }
        } else {
            return -2;
        }
    }

    private void deletefromProductSlabs(String customerId, String columnName, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + TABLE_PRODUCT_SLAB_DATA + " WHERE " + KEY_CUSTOMER_ID + "='" + customerId + "' AND " + columnName + "='" + value + "'");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public long updateProductInCart(String customerId, CheckInventoryModel model) {
        long row_id = -1;
        if (model != null) {

            int rowId = checkCartTableForCartId(customerId, KEY_PRODUCT_ID, model.getProductId());
            if (rowId < 0) {
                //No record exists with the user id and product id combination.. Return error
                return -9;
            }

            // rowId >0, Record exists in the DB, Update now.
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_WAREHOUSE_ID, model.getWarehouseId());
            values.put(KEY_STATUS, model.getStatus());
            values.put(KEY_AVAILABLE_QUANTITY, model.getAvailableQty());
            values.put(KEY_CART_ID, model.getCartId());
            values.put(KEY_IS_SLAB, model.getIsSlab());
            values.put(KEY_BLOCK_QTY, model.getBlockedQty());
            values.put(KEY_UPDATED_DATE, getDateTime());

            try {
                row_id = db.update(TABLE_CART, values, KEY_PRODUCT_ID + " = ? AND " + KEY_CUSTOMER_ID + " = ?",
                        new String[]{model.getProductId(), customerId});
                Log.e("updated in Cart", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }

        return row_id;
    }

    public long insertFreeProductInCart(CartModel model) {
        long row_id = -1;
        if (model != null) {

            int rowId = checkCartTableForCartId(model.getCustomerId(), KEY_PRODUCT_ID, model.getFreebieProductId());
            if (rowId > 0) {
                //Cart Item exists in table, update/delete the record
                deleteCartRow(rowId);
            }

            // rowId <0, Record does not exists in the DB, Insert now.
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_CUSTOMER_ID, model.getCustomerId());
            values.put(KEY_PRODUCT_ID, model.getFreebieProductId());
            values.put(KEY_QUANTITY, model.getFreebieQty());
            values.put(KEY_ESU, "1");
            values.put(KEY_UNIT_PRICE, "");
            values.put(KEY_MARGIN, "");
            values.put(KEY_TOTAL_PRICE, "0");
            values.put(KEY_WAREHOUSE_ID, model.getWarehouseId());
            values.put(KEY_STATUS, 1);
            values.put(KEY_AVAILABLE_QUANTITY, "");
            values.put(KEY_DISCOUNT, "");
            values.put(KEY_PARENT_ID, model.getProductId());
            values.put(KEY_REMARKS, model.getRemarks());
            values.put(KEY_UPDATED_DATE, getDateTime());
            values.put(KEY_CART_ID, model.getCartId());
            values.put(COL_STAR, model.getFreebieStar());
            /*values.put(COL_PACK_LEVEL, model.getPackLevel());*/
            values.put(COL_SLAB_ESU, model.getFreebieEsu());

            try {
                row_id = db.insert(TABLE_CART, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("inserted in Cart", "" + row_id);

                deletefromProductSlabs(model.getCustomerId(), KEY_PRODUCT_ID, model.getFreebieProductId());

                ProductSlabData productSlabData = new ProductSlabData();
                productSlabData.setCustomerId(model.getCustomerId());
                productSlabData.setEsu(1);
                productSlabData.setLevel(16001);
                productSlabData.setProductId(model.getFreebieProductId());
                productSlabData.setStar(model.getFreebieStar());
                try {
                    productSlabData.setQty(Integer.parseInt(model.getFreebieQty()));

                } catch (Exception e) {
                    e.printStackTrace();
                    productSlabData.setQty(0);
                }

                insertProductSlabData(productSlabData);

            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            return -2;
        }
        return row_id;
    }

    public int checkProductSlabTableForId(String customerId, String columnName1, String productId, String columnName2, String packLevel) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        int rowId = -1;
        try {
            cursor = db.rawQuery("SELECT " + KEY_ROW_ID + " FROM " + TABLE_PRODUCT_SLAB_DATA + " WHERE " +
                            KEY_CUSTOMER_ID + "=? and " + columnName1 + "=? and " + columnName2 + "=?",
                    new String[]{customerId, productId, packLevel});
            if (cursor != null && cursor.moveToFirst()) {
                rowId = cursor.getInt(0);
            } else {
                rowId = -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }

    public int checkCartTableForCartId(String customerId, String columnName, String productId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        int rowId = -1;
        try {
            cursor = db.rawQuery("SELECT " + KEY_ROW_ID + " FROM " + TABLE_CART + " WHERE " +
                            KEY_CUSTOMER_ID + "=? and " + columnName + "=?",
                    new String[]{customerId, productId});
            if (cursor != null && cursor.moveToFirst()) {
                rowId = cursor.getInt(0);
            } else {
                rowId = -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }

    public int deleteProductSlabRow(int rowId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try {
            row = db.delete(TABLE_PRODUCT_SLAB_DATA, KEY_ROW_ID + "=" + rowId, null);
        } catch (Exception e) {
            e.printStackTrace();
            row = -1;
        } finally {
            db.close();
        }
        return row;
    }

    public int deleteCartRow(int rowId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try {
            row = db.delete(TABLE_CART, KEY_ROW_ID + "=" + rowId, null);
        } catch (Exception e) {
            e.printStackTrace();
            row = -1;
        } finally {
            db.close();
        }
        return row;
    }

    public int deleteCartRow(String customerId, String productId, boolean deleteAll, boolean isFreebie) {
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try {
//            row = db.delete(TABLE_CART, KEY_CUSTOMER_ID + "=" + rowId, null);
            if (deleteAll) {
                row = db.delete(TABLE_CART, KEY_CUSTOMER_ID + " = ? ", new String[]{customerId});
            } else {
                if (!isFreebie) {
                    row = db.delete(TABLE_CART, KEY_CUSTOMER_ID + " = ? and " + KEY_PARENT_ID + " = ?", new String[]{customerId, productId});
                } else {
                    row = db.delete(TABLE_CART, KEY_CUSTOMER_ID + " = ? and " + KEY_PRODUCT_ID + " = ?", new String[]{customerId, productId});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            row = -1;
        } finally {
            db.close();
        }
        return row;
    }

    public int deleteProductSlabRow(String customerId, String productId, boolean deleteAll) {
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try {
//            row = db.delete(TABLE_CART, KEY_CUSTOMER_ID + "=" + rowId, null);
            if (deleteAll) {
                row = db.delete(TABLE_PRODUCT_SLAB_DATA, KEY_CUSTOMER_ID + " = ? ", new String[]{customerId});
            } else {
                row = db.delete(TABLE_PRODUCT_SLAB_DATA, KEY_CUSTOMER_ID + " = ? and " + KEY_PARENT_ID + " = ?", new String[]{customerId, productId});
            }
        } catch (Exception e) {
            e.printStackTrace();
            row = -1;
        } finally {
            db.close();
        }
        return row;
    }

    public int getCartCount(String customerId) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        int count = 0;
        try {
            cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_CART + " WHERE " + KEY_CUSTOMER_ID + "=?",
                    new String[]{customerId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return count;
    }

    public int getCheckinOutletsCount() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        int count = 0;
        try {
            cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_OUTLETS, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return count;
    }

    public double getCartValue(String customerId) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        double count = 0;
        try {
            cursor = db.rawQuery("SELECT SUM (" + KEY_TOTAL_PRICE + ") FROM " + TABLE_CART + " WHERE " + KEY_CUSTOMER_ID + "=?",
                    new String[]{customerId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getDouble(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return count;
    }

    public double getCashbackValue(String customerId) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        double count = 0;
        try {
            cursor = db.rawQuery("SELECT SUM (" + COl_CASHBACK_AMT + ") FROM " + TABLE_CART + " WHERE " + KEY_CUSTOMER_ID + "=?",
                    new String[]{customerId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getDouble(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return count;
    }

    public ArrayList<CartModel> getCartItems(String customerId, boolean isForUpload) {

        CartModel model = null;
        ArrayList<CartModel> arr = new ArrayList<>();
        String query = "";
        if (isForUpload) {
            query = "select table_cart.*,table_products.product_title as product_title, " +
                    "table_products.primary_image as product_image,tp1.product_title as freebie_title from table_cart " +
                    "inner join table_products on table_cart.product_id=table_products.product_id " +
                    "left join table_products as tp1  on table_cart.freebie_id = tp1.product_id " +
                    "where table_cart.customer_id = '" + customerId + "'";
        } else {
            query = "select table_cart.*,table_products.product_title as product_title, " +
                    "table_products.primary_image as product_image,tp1.product_title as freebie_title from table_cart " +
                    "inner join table_products on table_cart.product_id=table_products.product_id " +
                    "left join table_products as tp1  on table_cart.freebie_id = tp1.product_id " +
                    "where table_cart.product_id=table_cart.parent_id and table_cart.customer_id = '" + customerId + "'";
        }


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new CartModel();
                    model.setProductId(cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_ID)));
                    model.setCustomerId(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ID)));
                    model.setQuantity(cursor.getInt(cursor.getColumnIndex(KEY_QUANTITY)));
                    model.setEsu(cursor.getString(cursor.getColumnIndex(KEY_ESU)));
                    model.setUnitPrice(cursor.getString(cursor.getColumnIndex(KEY_UNIT_PRICE)));
                    model.setMargin(cursor.getString(cursor.getColumnIndex(KEY_MARGIN)));
                    model.setTotalPrice(cursor.getDouble(cursor.getColumnIndex(KEY_TOTAL_PRICE)));
                    model.setWarehouseId(cursor.getString(cursor.getColumnIndex(KEY_WAREHOUSE_ID)));
                    model.setStatus(cursor.getInt(cursor.getColumnIndex(KEY_STATUS)));
                    model.setAvailableQty(cursor.getInt(cursor.getColumnIndex(KEY_AVAILABLE_QUANTITY)));
                    model.setDiscount(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(KEY_PARENT_ID)));
                    model.setRemarks(cursor.getString(cursor.getColumnIndex(KEY_REMARKS)));
                    model.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
                    model.setCartId(cursor.getString(cursor.getColumnIndex(KEY_CART_ID)));
                    model.setIsSlab(cursor.getInt(cursor.getColumnIndex(KEY_IS_SLAB)));
                    model.setBlockedQty(cursor.getInt(cursor.getColumnIndex(KEY_BLOCK_QTY)));
                    model.setPrmtDetId(cursor.getInt(cursor.getColumnIndex(KEY_PROMOTION_ID)));
                    model.setProductSlabId(cursor.getInt(cursor.getColumnIndex(KEY_PRODUCT_SLAB_ID)));
                    model.setFreebieProductId(cursor.getString(cursor.getColumnIndex(KEY_FREEBIE_ID)));
                    model.setFreebieQty(cursor.getString(cursor.getColumnIndex(KEY_FREEBIE_QTY)));
                    model.setPackType(cursor.getString(cursor.getColumnIndex(KEY_PACK_TYPE)));
                    model.setIsChild(cursor.getInt(cursor.getColumnIndex(KEY_IS_CHILD)));
                    model.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)));
                    model.setPackLevel(cursor.getInt(cursor.getColumnIndex(COL_PACK_LEVEL)));
                    model.setSlabEsu(cursor.getString(cursor.getColumnIndex(COL_SLAB_ESU)));
                    model.setFreebieMpq(cursor.getInt(cursor.getColumnIndex(COL_FREEBIE_MPQ)));
                    model.setFreebieFq(cursor.getInt(cursor.getColumnIndex(COl_FREEBIE_FQ)));
                    model.setCashbackAmount(cursor.getDouble(cursor.getColumnIndex(COl_CASHBACK_AMT)));
                    model.setPackStar(cursor.getString(cursor.getColumnIndex(COL_PACK_STAR)));

                    model.setProductTitle(cursor.getString(cursor.getColumnIndex("product_title")));
                    model.setFreebieTitle(cursor.getString(cursor.getColumnIndex("freebie_title")));
                    model.setProductImage(cursor.getString(cursor.getColumnIndex("product_image")));

                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return arr;
    }

    public String getCartIds(String customerId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String ids = "";
        try {
            cursor = db.rawQuery("SELECT group_concat(cart_id, ',') FROM " + TABLE_CART + " WHERE " + KEY_CUSTOMER_ID + "=?",
                    new String[]{customerId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                ids = cursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return ids;
    }

    public String getCartId(String customerId, String productId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String id = "";
        try {
            cursor = db.rawQuery("SELECT cart_id FROM " + TABLE_CART + " WHERE " + KEY_CUSTOMER_ID + "=? and " + KEY_PRODUCT_ID + " =?",
                    new String[]{customerId, productId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                id = cursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return id;
    }

    public String getPOCartId(String customerId, String productId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String id = "";
        try {
            cursor = db.rawQuery("SELECT cart_id FROM " + TABLE_PO_CART + " WHERE " + KEY_CUSTOMER_ID + "=? and " + KEY_PRODUCT_ID + " =?",
                    new String[]{customerId, productId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                id = cursor.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return id;
    }

    /*SRM Functionality*/
    public long insertProductInPOCart(CartModel model) {
        if (model != null) {

            int rowId = checkPOCartTableForCartId(model.getCustomerId(), KEY_PRODUCT_ID, model.getProductId());
            if (rowId > 0) {
                //Cart Item exists in table, update/delete the record
                deletePOCartRow(rowId);
            }

            // rowId <0, Record does not exists in the DB, Insert now.
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_CUSTOMER_ID, model.getCustomerId());
            values.put(KEY_PRODUCT_ID, model.getProductId());
            values.put(KEY_QUANTITY, model.getQuantity());
            values.put(KEY_UNIT_PRICE, model.getUnitPrice());
            values.put(KEY_MARGIN, model.getMargin());
            values.put(KEY_TOTAL_PRICE, model.getTotalPrice());
            values.put(KEY_PACK_SIZE, model.getPackSize());
            values.put(KEY_FREE_QTY, model.getFreeqty());
            values.put(KEY_FB_PACK_SIZE, model.getFreepacksize());
            values.put(KEY_WAREHOUSE_ID, model.getWarehouseId());
            values.put(KEY_REMARKS, model.getRemarks());
            values.put(KEY_IS_FREEBIE, model.getIsFreebie());
            values.put(KEY_UPDATED_DATE, getDateTime());
            values.put(KEY_PARENT_ID, model.getParentId());
            values.put(PRODUCT_PACK_ID, model.getProductPackId());
            values.put(FREEBIE_PACK_ID, model.getFreebiePackId());
            values.put(KEY_IS_CHILD, model.isChild());

            long row_id = -1;
            try {
                row_id = db.insert(TABLE_PO_CART, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
                Log.e("inserted in PO Cart", "" + row_id);
            } catch (Exception e) {
                row_id = -1;
            } finally {

                db.close();
                return row_id;
            }
        } else {
            return -2;
        }
    }

    public int checkPOCartTableForCartId(String customerId, String columnName, String productId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        int rowId = -1;
        try {
            cursor = db.rawQuery("SELECT " + KEY_ROW_ID + " FROM " + TABLE_PO_CART + " WHERE " +
                            KEY_CUSTOMER_ID + "=? and " + columnName + "=?",
                    new String[]{customerId, productId});
            if (cursor != null && cursor.moveToFirst()) {
                rowId = cursor.getInt(0);
            } else {
                rowId = -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }

    public int deletePOCartRow(int rowId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try {
            row = db.delete(TABLE_PO_CART, KEY_ROW_ID + "=" + rowId, null);
        } catch (Exception e) {
            e.printStackTrace();
            row = -1;
        } finally {
            db.close();
        }
        return row;
    }

    public int deletePOCartRow(String customerId, String productId, boolean deleteAll, String isFreebie) {
        SQLiteDatabase db = this.getWritableDatabase();
        int row = 0;
        try {
//            row = db.delete(TABLE_CART, KEY_CUSTOMER_ID + "=" + rowId, null);
            if (deleteAll) {
                row = db.delete(TABLE_PO_CART, KEY_CUSTOMER_ID + " = ? ", new String[]{customerId});
            } else {
                row = db.delete(TABLE_PO_CART, KEY_CUSTOMER_ID + " = ? and " + KEY_PRODUCT_ID +
                        " = ? and " + KEY_IS_FREEBIE + " =?", new String[]{customerId, productId, isFreebie});
            }
        } catch (Exception e) {
            e.printStackTrace();
            row = -1;
        } finally {
            db.close();
        }
        return row;
    }

    public int getPOCartCount(String customerId, String warehouseId) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        int count = 0;
        try {
            cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_PO_CART + " WHERE " +
                            KEY_CUSTOMER_ID + "=? and " + KEY_WAREHOUSE_ID + "=?",
                    new String[]{customerId, warehouseId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return count;
    }

    public double getPOCartValue(String customerId, String warehouseId) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        double count = 0;
        try {
            cursor = db.rawQuery("SELECT SUM (" + KEY_TOTAL_PRICE + ") FROM " + TABLE_PO_CART +
                            " WHERE " + KEY_CUSTOMER_ID + "=? and " + KEY_WAREHOUSE_ID + "=?",
                    new String[]{customerId, warehouseId});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getDouble(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return count;
    }

    public ArrayList<CartModel> getPOCartItems(String customerId, String warehouseId) {

        CartModel model = null;
        ArrayList<CartModel> arr = new ArrayList<>();
        String query = "select table_po_cart.*,table_products.product_title as product_title, " +
                "table_products.primary_image as product_image from table_po_cart " +
                "inner join table_products on table_po_cart.product_id=table_products.product_id " +
                "where table_po_cart.customer_id = '" + customerId + "' and table_po_cart." +
                KEY_WAREHOUSE_ID + " = '" + warehouseId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    model = new CartModel();
                    model.setProductId(cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_ID)));
                    model.setParentId(cursor.getString(cursor.getColumnIndex(KEY_PARENT_ID)));
                    model.setCustomerId(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ID)));
                    model.setQuantity(cursor.getInt(cursor.getColumnIndex(KEY_QUANTITY)));
                    model.setUnitPrice(cursor.getString(cursor.getColumnIndex(KEY_UNIT_PRICE)));
                    model.setMargin(cursor.getString(cursor.getColumnIndex(KEY_MARGIN)));
                    model.setTotalPrice(cursor.getDouble(cursor.getColumnIndex(KEY_TOTAL_PRICE)));
                    model.setPackSize(cursor.getString(cursor.getColumnIndex(KEY_PACK_SIZE)));
                    model.setFreeqty(cursor.getInt(cursor.getColumnIndex(KEY_FREE_QTY)));
                    model.setFreepacksize(cursor.getString(cursor.getColumnIndex(KEY_FB_PACK_SIZE)));
                    model.setWarehouseId(cursor.getString(cursor.getColumnIndex(KEY_WAREHOUSE_ID)));
                    model.setRemarks(cursor.getString(cursor.getColumnIndex(KEY_REMARKS)));
                    model.setUpdatedDate(cursor.getString(cursor.getColumnIndex(KEY_UPDATED_DATE)));
                    model.setIsFreebie(cursor.getString(cursor.getColumnIndex(KEY_IS_FREEBIE)));
                    model.setProductPackId(cursor.getString(cursor.getColumnIndex(PRODUCT_PACK_ID)));
                    model.setFreebiePackId(cursor.getString(cursor.getColumnIndex(FREEBIE_PACK_ID)));
                    model.setIsChild(cursor.getInt(cursor.getColumnIndex(KEY_IS_CHILD)));

                    model.setProductTitle(cursor.getString(cursor.getColumnIndex("product_title")));
                    model.setProductImage(cursor.getString(cursor.getColumnIndex("product_image")));

                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return arr;
    }


    //Expense Module
    public long insertExpenseInDB(NewExpenseModel model) {
        long row_id = -1;
        if (model != null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_AMOUNT, model.getAmount());
            values.put(KEY_DATE, model.getDate());
            values.put(KEY_EXPENSE_TYPE, model.getExpenseType());
            values.put(KEY_EXPENSE_TYPE_ID, model.getExpenseTypeId());
            values.put(KEY_DESCRIPTION, model.getDesc());
            values.put(KEY_ATTACHMENTS, model.getAttachments());
            values.put(KEY_CREATED_DATE_TIME, getDateTime());

            try {
                row_id = db.insert(TABLE_UNCLAIMED_EXPENSES, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db.close();
            }
        } else {
            row_id = -2;
        }
        return row_id;
    }

    public ArrayList<NewExpenseModel> getUnClaimedExpenses() {
        NewExpenseModel model = null;
        ArrayList<NewExpenseModel> arr = new ArrayList<>();
        // 1. build the query
        String query = "SELECT * FROM " + TABLE_UNCLAIMED_EXPENSES + " order by " + KEY_DATE + " desc";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {

                    String amount = cursor.getString(cursor.getColumnIndex(KEY_AMOUNT));
                    String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                    String expenseType = cursor.getString(cursor.getColumnIndex(KEY_EXPENSE_TYPE));
                    String expenseTypeId = cursor.getString(cursor.getColumnIndex(KEY_EXPENSE_TYPE_ID));
                    String desc = cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION));
                    String attachments = cursor.getString(cursor.getColumnIndex(KEY_ATTACHMENTS));

                    model = new NewExpenseModel("0", amount, date, expenseType, expenseTypeId, desc, attachments);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public long insertFeature(FeatureData featureData) {

        ContentValues values = new ContentValues();
        values.put(KEY_FEATURE_CODE, featureData.getFeatureCode());
        values.put(KEY_FEATURE_NAME, featureData.getFeatureName());
        values.put(KEY_FEATURE_IS_PARENT, featureData.getFeatureIsParent());
        values.put(KEY_FEATURE_PARENT, featureData.getFeatureParent());
        values.put(KEY_UPDATED_DATE, getDateTime());

        long row_id = -1;
        if (Build.VERSION.SDK_INT >= 19) {
            try (SQLiteDatabase db = this.getWritableDatabase()) {
                row_id = db.insert(TABLE_FEATURES, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
            } catch (Exception e) {
                row_id = -1;
            }

        } else {
            SQLiteDatabase db1 = this.getWritableDatabase();
            try {
                row_id = db1.insert(TABLE_FEATURES, // table
                        null, //nullColumnHack
                        values); // key/value -> keys = column names/ values = column values
            } catch (Exception e) {
                row_id = -1;
            } finally {
                db1.close();
            }
        }

        return row_id;
    }

    public ArrayList<ProductSlabData> getProductSlabData(String productId, String customerId) {
        ArrayList<ProductSlabData> slabDataArrayList = new ArrayList<>();
        String query = "select * from " + TABLE_PRODUCT_SLAB_DATA + " where " + KEY_PRODUCT_ID + "= '" + productId + "' and " + KEY_CUSTOMER_ID + "= '" + customerId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {

                    ProductSlabData productSlabData = new ProductSlabData();
                    productSlabData.setCustomerId(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ID)));
                    productSlabData.setProductId(cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_ID)));
                    productSlabData.setLevel(cursor.getInt(cursor.getColumnIndex(COL_PACK_LEVEL)));
                    productSlabData.setEsu(cursor.getInt(cursor.getColumnIndex(COL_SLAB_ESU)));
                    productSlabData.setQty(cursor.getInt(cursor.getColumnIndex(KEY_QUANTITY)));
                    productSlabData.setPackSize(cursor.getInt(cursor.getColumnIndex(KEY_PACK_SIZE)));
                    productSlabData.setPackQty(cursor.getInt(cursor.getColumnIndex(KEY_PACK_QUANTITY)));
                    productSlabData.setStar(cursor.getString(cursor.getColumnIndex(COL_STAR)));
                    productSlabData.setCashBackIds(cursor.getString(cursor.getColumnIndex(COl_CASH_ID)));

                    slabDataArrayList.add(productSlabData);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return slabDataArrayList;
    }

    public ArrayList<FeatureData> getRightMenuOptions() {
        FeatureData model = null;
        ArrayList<FeatureData> arr = new ArrayList<>();
        String query = "select * from " + TABLE_FEATURES + " where " + KEY_FEATURE_IS_PARENT + "= '1'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {

                    String featureCode = cursor.getString(cursor.getColumnIndex(KEY_FEATURE_CODE));
                    String name = cursor.getString(cursor.getColumnIndex(KEY_FEATURE_NAME));
                    String isMenu = cursor.getString(cursor.getColumnIndex(KEY_FEATURE_IS_PARENT));
                    String parentId = cursor.getString(cursor.getColumnIndex(KEY_FEATURE_PARENT));
                    String featureId = cursor.getString(cursor.getColumnIndex(KEY_FEATURE_CODE));//todo change this

                    model = new FeatureData(featureCode, name, parentId, isMenu, featureId);
                    arr.add(model);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return arr;
    }

    public int checkIfFeatureAllowed(String featureCode) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        int rowId = -1;
        try {
            cursor = db.rawQuery("SELECT " + KEY_ROW_ID + " FROM " + TABLE_FEATURES + " WHERE " +
                            KEY_FEATURE_CODE + "=?",
                    new String[]{featureCode});
            if (cursor != null && cursor.moveToFirst()) {
                rowId = cursor.getInt(0);
            } else {
                rowId = -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }

    public CustomerTyepModel getStarColorName(String starValue) {

        CustomerTyepModel customerTyepModel = null;
        String query = "select " + COL_CUSTOMER_NAME + ", " + COL_DESCRIPTION + " from " + TABLE_STAR + " where " + COL_CUSTOMER_GRP_ID + "='" + starValue + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                String colorName = "", description = "";

                customerTyepModel = new CustomerTyepModel();

                colorName = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME));
                description = cursor.getString(cursor.getColumnIndex(COL_DESCRIPTION));

                customerTyepModel.setCustomerName(colorName);
                customerTyepModel.setDescription(description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return customerTyepModel;
    }

    public String getStarColorValue(String starName) {

        String starValue = "";
        String query = "select " + COL_CUSTOMER_GRP_ID + " from " + TABLE_STAR + " where " + COL_DESCRIPTION + "='" + starName + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {

                starValue = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_GRP_ID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return starValue;
    }

    public DiscountModel getDiscountData(String disountOn, String disountOnValue) {
        DiscountModel model = null;
        String query = "select * from " + TABLE_DISCOUNT + " where " + KEY_DISCOUNT_ON + "= '" + disountOn + "' and " + KEY_DISCOUNT_ON_VALUE + "='" + disountOnValue + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                model = new DiscountModel();
                model.setDiscountType(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT_TYPE)));
                model.setDiscountOn(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT_ON)));
                model.setDiscountValues(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT_ON_VALUE)));
                model.setDiscount(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT)));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return model;
    }

    public DiscountModel getOrderDiscountData(String discountOn, double totalAmount) {
        DiscountModel model = null;
        String query = "select * from " + TABLE_DISCOUNT + " where " + KEY_DISCOUNT_ON + "= '" + discountOn + "' and " + KEY_DISCOUNT_ON_VALUE + " <= '" + totalAmount + "' order by " + KEY_DISCOUNT_ON_VALUE + " desc limit 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                model = new DiscountModel();
                model.setDiscountType(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT_TYPE)));
                model.setDiscountOn(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT_ON)));
                model.setDiscountValues(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT_ON_VALUE)));
                model.setDiscount(cursor.getString(cursor.getColumnIndex(KEY_DISCOUNT)));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return model;
    }

    public double getCartStarValue(String customerId, String starValue) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        double count = 0;
        /*String description = null;
        CustomerTyepModel model = this.getStarColorName(starValue);
        if (model != null) {
            description = model.getDescription();
        }*/
        try {
            cursor = db.rawQuery("SELECT SUM (" + KEY_TOTAL_PRICE + ") FROM " + TABLE_CART + " WHERE " + KEY_CUSTOMER_ID + "=? AND " + COL_PACK_STAR + " =?",
                    new String[]{customerId, starValue});

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getDouble(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
        return count;
    }

}
