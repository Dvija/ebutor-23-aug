package com.ebutor.srm.models;

import com.ebutor.models.CustomerTyepModel;

import java.util.ArrayList;

/**
 * Created by Srikanth Nama on 25-Nov-16.
 */

public class SupplierMasterLookupModel {
    private ArrayList<CustomerTyepModel> organizationTypes, supplierTypes, supplierRanks, relationshipManagers;

    public ArrayList<CustomerTyepModel> getOrganizationTypes() {
        return organizationTypes;
    }

    public void setOrganizationTypes(ArrayList<CustomerTyepModel> organizationTypes) {
        this.organizationTypes = organizationTypes;
    }

    public ArrayList<CustomerTyepModel> getSupplierTypes() {
        return supplierTypes;
    }

    public void setSupplierTypes(ArrayList<CustomerTyepModel> supplierTypes) {
        this.supplierTypes = supplierTypes;
    }

    public ArrayList<CustomerTyepModel> getSupplierRanks() {
        return supplierRanks;
    }

    public void setSupplierRanks(ArrayList<CustomerTyepModel> supplierRanks) {
        this.supplierRanks = supplierRanks;
    }

    public ArrayList<CustomerTyepModel> getRelationshipManagers() {
        return relationshipManagers;
    }

    public void setRelationshipManagers(ArrayList<CustomerTyepModel> relationshipManagers) {
        this.relationshipManagers = relationshipManagers;
    }
}
