package com.ebutor.srm.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Srikanth Nama on 30-Nov-16.
 */

public class SupplierModel implements Parcelable {

    public String supplierId;
    public String supplierERPCode;
    public String legalEntityId;


    public SupplierModel(){

    }

    public SupplierModel(String supplierId, String supplierERPCode, String legalEntityId) {
        this.supplierId = supplierId;
        this.supplierERPCode = supplierERPCode;
        this.legalEntityId = legalEntityId;
    }

    protected SupplierModel(Parcel in) {
        this.supplierId = in.readString();
        this.supplierERPCode = in.readString();
        this.legalEntityId = in.readString();
    }

    public static final Creator<SupplierModel> CREATOR = new Creator<SupplierModel>() {
        @Override
        public SupplierModel createFromParcel(Parcel in) {
            return new SupplierModel(in);
        }

        @Override
        public SupplierModel[] newArray(int size) {
            return new SupplierModel[size];
        }
    };

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the set of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.supplierId);
        dest.writeString(this.supplierERPCode);
        dest.writeString(this.legalEntityId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SupplierModel user = (SupplierModel) o;

        if (supplierId != null ? !supplierId.equals(user.supplierId) : user.supplierId != null) return false;
        if (supplierERPCode != null ? !supplierERPCode.equals(user.supplierERPCode) : user.supplierERPCode != null) return false;

        return !(legalEntityId != null ? !legalEntityId.equals(user.legalEntityId) : user.legalEntityId != null);

    }


}
