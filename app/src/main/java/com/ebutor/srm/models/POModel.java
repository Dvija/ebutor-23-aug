package com.ebutor.srm.models;

import com.ebutor.models.NewPacksModel;

import java.util.ArrayList;

public class POModel {
    private String productId, productName, skuCode, quantity, uom, noOfEaches, freeQty, freeUOM, freeEaches, mrp, unitPrice, price, isTax, taxName, taxPer, taxAmount, subTotal;
    private String packSizeValue, freePackSizeValue, packId, freePackId, parentId;
    private String productTotal, applyDiscount, discountType, discount,slp,std,thirtyd,availableInventory;
    private ArrayList<NewPacksModel> arrUOMs;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getNoOfEaches() {
        return noOfEaches;
    }

    public void setNoOfEaches(String noOfEaches) {
        this.noOfEaches = noOfEaches;
    }

    public String getFreeQty() {
        return freeQty;
    }

    public void setFreeQty(String freeQty) {
        this.freeQty = freeQty;
    }

    public String getFreeUOM() {
        return freeUOM;
    }

    public void setFreeUOM(String freeUOM) {
        this.freeUOM = freeUOM;
    }

    public String getFreeEaches() {
        return freeEaches;
    }

    public void setFreeEaches(String freeEaches) {
        this.freeEaches = freeEaches;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIsTax() {
        return isTax;
    }

    public void setIsTax(String isTax) {
        this.isTax = isTax;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public String getTaxPer() {
        return taxPer;
    }

    public void setTaxPer(String taxPer) {
        this.taxPer = taxPer;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getPackSizeValue() {
        return packSizeValue;
    }

    public void setPackSizeValue(String packSizeValue) {
        this.packSizeValue = packSizeValue;
    }

    public String getFreePackSizeValue() {
        return freePackSizeValue;
    }

    public void setFreePackSizeValue(String freePackSizeValue) {
        this.freePackSizeValue = freePackSizeValue;
    }

    public String getPackId() {
        return packId;
    }

    public void setPackId(String packId) {
        this.packId = packId;
    }

    public String getFreePackId() {
        return freePackId;
    }

    public void setFreePackId(String freePackId) {
        this.freePackId = freePackId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(String productTotal) {
        this.productTotal = productTotal;
    }

    public ArrayList<NewPacksModel> getArrUOMs() {
        return arrUOMs;
    }

    public void setArrUOMs(ArrayList<NewPacksModel> arrUOMs) {
        this.arrUOMs = arrUOMs;
    }

    public String getApplyDiscount() {
        return applyDiscount;
    }

    public void setApplyDiscount(String applyDiscount) {
        this.applyDiscount = applyDiscount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSlp() {
        return slp;
    }

    public void setSlp(String slp) {
        this.slp = slp;
    }

    public String getStd() {
        return std;
    }

    public void setStd(String std) {
        this.std = std;
    }

    public String getThirtyd() {
        return thirtyd;
    }

    public void setThirtyd(String thirtyd) {
        this.thirtyd = thirtyd;
    }

    public String getAvailableInventory() {
        return availableInventory;
    }

    public void setAvailableInventory(String availableInventory) {
        this.availableInventory = availableInventory;
    }
}
