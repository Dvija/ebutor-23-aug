package com.ebutor.srm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.HomeActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.RetailersModel;
import com.ebutor.srm.activities.POListActivity;
import com.ebutor.srm.activities.PriceHistoryActivity;
import com.ebutor.srm.activities.RegisterSupplierActivity;
import com.ebutor.srm.activities.SubscribeProductsActivity;
import com.ebutor.srm.activities.SupplierProductsActivity;
import com.ebutor.srm.adapters.SuppliersAdapter;
import com.ebutor.srm.models.WarehouseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.ebutor.utils.fastscroll.AlphabetItem;
import com.ebutor.utils.fastscroll.RecyclerViewFastScroller;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 09-Jun-16.
 */
public class SuppliersListFragment extends Fragment implements SuppliersAdapter.OnItemClickListener, Response.ErrorListener, VolleyHandler<Object> {

    private RecyclerView buyersList;
    private ArrayList<RetailersModel> suppliersModelArrayList;
    private SuppliersAdapter adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<AlphabetItem> mAlphabetItems;
    private LinearLayout llMain;
    private TextView tvAlertMsg;
    private RelativeLayout rlAlert;
    private RecyclerViewFastScroller fastScroller;
    private Tracker mTracker;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private SharedPreferences mSharedPreferences;
    private RetailersModel supplierModel;
    private boolean isPrice = false;
    WarehouseListFragment.OkListener confirmListener = new WarehouseListFragment.OkListener() {
        @Override
        public void onOkClicked(String whListId) {
            Intent intent = null;
            if (isPrice) {
                intent = new Intent(getActivity(), PriceHistoryActivity.class);
                intent.putExtra("SupplierId", supplierModel.getCustomerId());
                intent.putExtra("whId", whListId);
                mSharedPreferences.edit().putString(ConstantValues.KEY_SRM_WH_ID, whListId).apply();
                startActivity(intent);
            } else {
                if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.CREATE_PO_FEATURE_CODE) >= 0) {
                    intent = new Intent(getActivity(), SupplierProductsActivity.class);
                    intent.putExtra("SupplierId", supplierModel.getCustomerId());
                    intent.putExtra("whId", whListId);
                    mSharedPreferences.edit().putString(ConstantValues.KEY_SRM_WH_ID, whListId).apply();
                    startActivity(intent);
                }else{
                    Utils.showAlertDialog(getActivity(), "You don't have permission to create PO.");
                }
            }

        }
    };
    private EditText searchET;
    private Dialog dialog;
    private String requestType = "";
    private ArrayList<WarehouseModel> arrWhs;
    private FloatingActionButton btnCreateSupplier;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_suppliers, container, false);

        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        fastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.fast_scroller);
        suppliersModelArrayList = new ArrayList<>();
        buyersList = (RecyclerView) view.findViewById(R.id.buyers_list);
        buyersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        searchET = (EditText) view.findViewById(R.id.input_search_query);
        btnCreateSupplier = (FloatingActionButton) view.findViewById(R.id.fab);

        int featureCode = DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.CREATE_SUPPLIER_FEATURE_CODE);
        if (featureCode < 0) {
            btnCreateSupplier.setVisibility(View.GONE);
        }

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        arrWhs = new ArrayList<>();

//        retailersModelArrayList = DBHelper.getInstance().getRetailers();
//        if(retailersModelArrayList!=null && retailersModelArrayList.size()>0){
//            setAdapter(retailersModelArrayList);
//        }else{
        callWebService(false);
//        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callWebService(true);
            }
        });
        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        searchET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (adapter != null) {
                    String text = searchET.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.getFilter().filter(text);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

        buyersList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                if (dy > 0 || dy < 0 && btnCreateSupplier.isShown()) {
//                    btnCreateSupplier.hide();
//                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    btnCreateSupplier.show();
//                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        btnCreateSupplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.CREATE_SUPPLIER_FEATURE_CODE) >= 0) {
                    startActivityForResult(new Intent(getActivity(), RegisterSupplierActivity.class), 1);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            callWebService(false);
        }
    }

    private void callWebService(boolean isSwipeDown) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
            obj.put("offset", "0");
            obj.put("offset_limit", "0");

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.getAllSuppliersURL, map, SuppliersListFragment.this, SuppliersListFragment.this, PARSER_TYPE.GET_ALL_SUPPLIERS);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.getAllSuppliersURL);

            if (dialog != null && !isSwipeDown)
                dialog.show();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    public void OnBuyerSelected(int id, RetailersModel supplierModel) {
        if (supplierModel != null) {
            mSharedPreferences.edit().putString(ConstantValues.KEY_SUPPLIER_NAME, supplierModel.getFirstName()).apply();
            mSharedPreferences.edit().putString(ConstantValues.KEY_SEL_SUPPLIER_ID, supplierModel.getCustomerId()).apply();
            this.supplierModel = supplierModel;
            if (id == 1) {// create po
                isPrice = false;
//                if(arrWhs!=null && arrWhs.size()>0) {
//                    showWhDialog();
//                } else {
                getWhList();
//                }
            } else if (id == 2) {// price History
                isPrice = true;
//                if(arrWhs!=null && arrWhs.size()>0) {
//                    showWhDialog();
//                } else {
                getWhList();
//                }
            } else if (id == 3) {// po history
                Intent intent = new Intent(getActivity(), POListActivity.class);
                startActivity(intent);
            } else if (id == 4) {// subscribe products
                if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.SUBSCRIBE_PRODUCTS_FEATURE_CODE) >= 0) {
                    Intent intent = new Intent(getActivity(), SubscribeProductsActivity.class);
                    intent.putExtra("SupplierId", supplierModel.getCustomerId());
                    startActivity(intent);
                }else{
                    Utils.showAlertDialog(getActivity(), "You don't have permission to Subscribe Products.");
                }
            } else {
                Utils.showAlertDialog(getActivity(), getString(R.string.unknown_request));
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
    }

    public void getWhList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            requestType = "GetWhList";
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "1");
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("supplier_id", mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getWarehouseListURL, map, SuppliersListFragment.this, SuppliersListFragment.this, PARSER_TYPE.GET_WH_LIST);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.getWarehouseListURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestType = "GetWhList";
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void showWhDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        WarehouseListFragment editNameDialog = WarehouseListFragment.newInstance(arrWhs);
        editNameDialog.setCancelable(true);
        editNameDialog.setConfirmListener(confirmListener);
        editNameDialog.show(fm, "wh_fragment_dialog");
    }

    private void getRetailerToken(RetailersModel retailersModel) {

        mSharedPreferences.edit().putString(ConstantValues.KEY_SHOP_NAME, retailersModel.getCompany()).apply();
        try {
            JSONObject obj = new JSONObject();
//                obj.put("appId", mSharedPreferences.getString(ConstantValues.KEY_APP_ID, ""));
            obj.put("telephone", retailersModel.getTelephone());
            obj.put("latitude", mSharedPreferences.getString(ConstantValues.KEY_LATITUDE, ""));
            obj.put("longitude", mSharedPreferences.getString(ConstantValues.KEY_LONGITUDE, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask topBrandsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.getRetailerTokenURL, map, SuppliersListFragment.this, SuppliersListFragment.this, PARSER_TYPE.GET_RETAILER_TOKEN);
            topBrandsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(topBrandsReq, AppURL.getRetailerTokenURL);

            if (dialog != null)
                dialog.show();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.sync) {
            callWebService(false);
        } else if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int id, Object object) {
        if (object != null && object instanceof RetailersModel) {
            OnBuyerSelected(id, (RetailersModel) object);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Retailer Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        mSwipeRefreshLayout.setRefreshing(false);
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_ALL_SUPPLIERS) {
//                    DBHelper.getInstance().deleteTable(DBHelper.TABLE_RETAILERS);
                    if (response instanceof ArrayList) {
                        suppliersModelArrayList = (ArrayList<RetailersModel>) response;

                        Collections.sort(suppliersModelArrayList, new Comparator<RetailersModel>() {
                            public int compare(RetailersModel r1, RetailersModel r2) {
                                return r1.getFirstName().compareToIgnoreCase(r2.getFirstName());
                            }
                        });

//                        retailersModelArrayList = DBHelper.getInstance().getRetailers();
                        setAdapter(suppliersModelArrayList);
//                                setListData(retailersModelArrayList);

                    }

                } else if (requestType == PARSER_TYPE.GET_WH_LIST) {
                    if (response instanceof ArrayList) {
                        arrWhs = (ArrayList<WarehouseModel>) response;
                        if (arrWhs != null && arrWhs.size() > 0) {
                            showWhDialog();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_wh_available), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (requestType == PARSER_TYPE.GET_RETAILER_TOKEN) {
                    if (response instanceof RetailersModel) {

                        RetailersModel retailersModel = (RetailersModel) response;

                        String customerToken = retailersModel.getCustomerToken();
                        String customerId = retailersModel.getCustomerId();
                        String firstName = retailersModel.getFirstName();

                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString(ConstantValues.KEY_CUSTOMER_ID, customerId);
                        editor.putString(ConstantValues.KEY_RETAILER_NAME, firstName);
                        editor.putString(ConstantValues.KEY_CUSTOMER_TOKEN, customerToken);
                        mSharedPreferences.edit().putBoolean(ConstantValues.KEY_IS_SELECTED_RETAILER, true).apply();
                        editor.apply();

                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } else if (requestType == PARSER_TYPE.FF_COMMENTS) {
                    Utils.showAlertDialog(getActivity(), message);
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }


    private void setAdapter(ArrayList<RetailersModel> suppliersModelArrayList) {
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();

        for (int i = 0; i < suppliersModelArrayList.size(); i++) {
            String name = suppliersModelArrayList.get(i).getFirstName();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1).toUpperCase();
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }

        adapter = new SuppliersAdapter(getActivity(), suppliersModelArrayList);
        adapter.setOnItemClickListener(this);
        buyersList.setAdapter(adapter);

        fastScroller.setRecyclerView(buyersList);
        fastScroller.setUpAlphabet(mAlphabetItems);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }
}
