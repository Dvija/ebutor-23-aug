package com.ebutor.srm.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.adapters.WarehouseSpinnerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.ManufacturerModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.srm.adapters.ManufacturersSpinnerAdapter;
import com.ebutor.srm.adapters.SubscribeProductsAdapter;
import com.ebutor.srm.models.SupplierModel;
import com.ebutor.srm.models.WarehouseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 30-Nov-16.
 */

public class SubscribeProductsFragment extends Fragment implements VolleyHandler, Response.ErrorListener,
        SubscribeProductsAdapter.OnProductSubscribeChanged {
    ProgressDialog dialog;
    ArrayList<WarehouseModel> warehouseModelArrayList;
    ArrayList<ManufacturerModel> manufacturerModelArrayList;
    SubscribeProductsAdapter subscribeProductsAdapter;
    //    private SupplierModel supplierModel;
    private Spinner spinWarehouse, spinManufacturer;
    private RecyclerView recyclerView;
    private SharedPreferences mSharedPreferences;
    private String selectedWarehouseId, selectedManufacturerId;
    private ArrayList<TestProductModel> productModelArrayList;
    private String unAvailableProducts;
    private String supplierId;
    private EditText etSearch;

    public static SubscribeProductsFragment newInstance(SupplierModel supplierModel) {
        SubscribeProductsFragment fragment = new SubscribeProductsFragment();
        Bundle args = new Bundle();
        args.putParcelable("SupplierModel", supplierModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_subscribe_products, null);

//        if (getArguments() != null && getArguments().containsKey("SupplierModel")) {
//            supplierModel = getArguments().getParcelable("SupplierModel");
//        }
        //for crate home button
        ParentActivity activity = (ParentActivity) getActivity();
//        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Subscribe Products");
        }

        Bundle extras = getActivity().getIntent().getExtras();
        supplierId = extras.getString("SupplierId", null);

        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("");
        dialog.setMessage(getString(R.string.please_wait));
        dialog.setIndeterminate(true);

        warehouseModelArrayList = new ArrayList<>();
        manufacturerModelArrayList = new ArrayList<>();

        etSearch = (EditText) convertView.findViewById(R.id.et_search);
        recyclerView = (RecyclerView) convertView.findViewById(R.id.scrollable);
        spinManufacturer = (Spinner) convertView.findViewById(R.id.spin_manufacturer);
        spinWarehouse = (Spinner) convertView.findViewById(R.id.spin_warehouse);

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (subscribeProductsAdapter != null) {
                    String text = etSearch.getText().toString().toLowerCase(Locale.getDefault());
                    subscribeProductsAdapter.getFilter().filter(text);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

        spinWarehouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object obj = view.getTag();
                if (obj != null && obj instanceof WarehouseModel) {
                    WarehouseModel model = (WarehouseModel) obj;
                    selectedWarehouseId = (String) model.getWhId();
                }
                productModelArrayList.clear();
                subscribeProductsAdapter.removeAllItems();
                subscribeProductsAdapter.notifyDataSetChanged();
                spinManufacturer.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinManufacturer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object obj = view.getTag();
                if (obj != null && obj instanceof ManufacturerModel) {
                    ManufacturerModel model = (ManufacturerModel) obj;
                    selectedManufacturerId = (String) model.getManufacturerId();
                    if (!TextUtils.isEmpty(selectedManufacturerId))
                        getManufacturerProducts();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        productModelArrayList = new ArrayList<>();

        subscribeProductsAdapter = new SubscribeProductsAdapter(getActivity(), productModelArrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        subscribeProductsAdapter.setListener(SubscribeProductsFragment.this);
        recyclerView.setAdapter(subscribeProductsAdapter);

        getWarehouseList();
        getManufacturersList();

        return convertView;
    }

    private void subscribeProduct(TestProductModel model, int flag) {
        if (Networking.isNetworkAvailable(getActivity())) {

            try {
                JSONObject obj = new JSONObject();
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                obj.put("supplier_id", supplierId);
                obj.put("warehouse_id", selectedWarehouseId);
                obj.put("product_id", model.getProductId());
                obj.put("flag", "" + flag);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.subscribeProductURL, map, SubscribeProductsFragment.this, SubscribeProductsFragment.this, PARSER_TYPE.SUBSCRIBE_PRODUCT);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.subscribeProductURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //todo show network alert
        }
    }


    private void getWarehouseList() {
        if (Networking.isNetworkAvailable(getActivity())) {

            try {
                JSONObject obj = new JSONObject();
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                obj.put("supplier_id", supplierId);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getWarehouseListURL, map, SubscribeProductsFragment.this, SubscribeProductsFragment.this, PARSER_TYPE.GET_WH_LIST);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.getWarehouseListURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //todo show network alert
        }
    }

    private void getManufacturerProducts() {
        if (Networking.isNetworkAvailable(getActivity())) {

            try {
                JSONObject obj = new JSONObject();
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                obj.put("manufacturer_id", selectedManufacturerId);
                obj.put("supplier_id", mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
                obj.put("le_wh_id", selectedWarehouseId);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getallmanfproductsURL, map, SubscribeProductsFragment.this, SubscribeProductsFragment.this, PARSER_TYPE.GET_ALL_MANUFACTURER_PRODUCTS);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.getallmanfproductsURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //todo show network alert
        }
    }

    private void getManufacturersList() {
        if (Networking.isNetworkAvailable(getActivity())) {

            try {
                JSONObject obj = new JSONObject();
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getManufacturersURL, map, SubscribeProductsFragment.this, SubscribeProductsFragment.this, PARSER_TYPE.GET_MANUFACTURERS);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.getManufacturersURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //todo show network alert
        }
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     * <p>
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSubscriptionChanges(Object object, int position, int flag) {
        if (object != null && object instanceof TestProductModel) {
            TestProductModel model = (TestProductModel) object;
            subscribeProduct(model, flag);
        }
    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();

            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        if (response != null) {
            if (!status.equalsIgnoreCase("success")) {
                Utils.showAlertDialog(getActivity(), message);
                return;
            }

            if (requestType == PARSER_TYPE.GET_WH_LIST) {
                if (response instanceof ArrayList) {
                    warehouseModelArrayList = (ArrayList<WarehouseModel>) response;

                    if (warehouseModelArrayList != null && warehouseModelArrayList.size() > 0) {

                        WarehouseModel model = new WarehouseModel();
                        model.setWhId("");
                        model.setWhName("Select WareHouse");
                        warehouseModelArrayList.add(0, model);

                        WarehouseSpinnerAdapter adapter = new WarehouseSpinnerAdapter(getActivity(), warehouseModelArrayList);
                        spinWarehouse.setAdapter(adapter);
                    }

                } else {
                    Utils.showAlertDialog(getActivity(), getString(R.string.oops));
                }
            } else if (requestType == PARSER_TYPE.GET_MANUFACTURERS) {
                if (response instanceof ArrayList) {
                    manufacturerModelArrayList = (ArrayList<ManufacturerModel>) response;

                    if (manufacturerModelArrayList != null && manufacturerModelArrayList.size() > 0) {
                        ManufacturersSpinnerAdapter adapter = new ManufacturersSpinnerAdapter(getActivity(), manufacturerModelArrayList);
                        spinManufacturer.setAdapter(adapter);
                    }

                } else {
                    Utils.showAlertDialog(getActivity(), getString(R.string.oops));
                }
            } else if (requestType == PARSER_TYPE.GET_ALL_MANUFACTURER_PRODUCTS) {
                if (response instanceof ArrayList) {
                    productModelArrayList.clear();
                    subscribeProductsAdapter.removeAllItems();
                    subscribeProductsAdapter.notifyDataSetChanged();

                    ArrayList<TestProductModel> arrayList = (ArrayList<TestProductModel>) response;

                    setSKUListData(arrayList);

                }

            } else if (requestType == PARSER_TYPE.SUBSCRIBE_PRODUCT) {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.oops));
        }
    }

    private void setSKUListData(ArrayList<TestProductModel> arrProducts) {
        for (int i = 0; i < arrProducts.size(); i++) {
//            expandableAdapter.addItem(arrProducts.get(i));
            if (!productModelArrayList.contains(arrProducts.get(i))) {
                productModelArrayList.add(arrProducts.get(i));
                subscribeProductsAdapter.addItem(arrProducts.get(i));
            }
        }
        subscribeProductsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSessionError(String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        Utils.logout(getActivity(), message);
    }
}
