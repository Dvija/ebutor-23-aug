package com.ebutor.srm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.HomeActivity;
import com.ebutor.MyApplication;
import com.ebutor.PaymentActivity;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CartModel;
import com.ebutor.models.CheckInventoryModel;
import com.ebutor.models.NewProductModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.srm.activities.CreatePOActivity;
import com.ebutor.srm.adapters.POCartListAdapter;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class POCartFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, POCartListAdapter.onCartItemDeleteListener, EditPOCartFragment.OnDialogClosed {

    private RecyclerView mRecyclerView;
    private POCartListAdapter cartListAdapter;
    //    private ArrayList<ProductModel> arrCart;
//    private ProductModel selectedProductObject = null, productModel;
//    private ImageView ivCart;
    private Tracker mTracker;
    private Button btnProceed;
    private LinearLayout llCart;
    private TextView tvTotalAmount, tvNoCart;
    private int totalItems = 0;
    private double totalCartPrice = 0;
    private SharedPreferences mSharedPreferences;
    private int cartCount = 0;
    private String cartId = "";
    private TextView tvBadge, tvAlertMsg;
    private RelativeLayout rlAlert;
    private LinearLayout llMain;
    private String requestType = "";
    private Dialog dialog;

    private ArrayList<CartModel> cartItemsList;
    private CartModel selectedCartModel;
    private boolean isClicked = false, unavailable = false;

    public static Collection<CheckInventoryModel> filter(Collection<CheckInventoryModel> target,
                                                         Predicate<CheckInventoryModel> predicate) {
        Collection<CheckInventoryModel> result = new ArrayList<CheckInventoryModel>();

        for (CheckInventoryModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_po_cart, container, false);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cartItemsList = new ArrayList<>();
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.scrollable);
        llCart = (LinearLayout) view.findViewById(R.id.llCart);
        tvNoCart = (TextView) view.findViewById(R.id.tvNoCart);
        btnProceed = (Button) view.findViewById(R.id.btnProceed);
//        tvTotalItems = (TextView) view.findViewById(R.id.tvTotalItems);
        tvTotalAmount = (TextView) view.findViewById(R.id.tvGrandTotal);
//        ivCart = (ImageView) view.findViewById(R.id.ivCart);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        exportDatabase(DBHelper.DATABASE_NAME);

        MyApplication application = (MyApplication) getActivity().getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        tvBadge = (TextView) view.findViewById(R.id.tvBadge);
//        tvBadge.setText(String.valueOf(MyApplication.getInstance().cartSize(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""))));
        btnProceed.setText(getActivity().getResources().getString(R.string.proceed));

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String cartIds = DBHelper.getInstance().getCartIds(mSharedPreferences.
//                        getString(ConstantValues.KEY_CUSTOMER_ID, ""));
//                JSONArray cartArray = new JSONArray();
//                if(cartIds!=null && !TextUtils.isEmpty(cartIds)){
//                    for (String s : cartIds.split(",")){
//                        cartArray.put(s);
//                    }
//                }
                JSONObject dataObject = createJSONForCreatePO();
                if (dataObject == null) {
                    return;
                }
//
                Intent intent = new Intent(getActivity(), CreatePOActivity.class);
                intent.putExtra("pocart", dataObject.toString());
                startActivity(intent);
//                createPO();
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestType) {
                        case "ViewCart":
                            viewCart();
                            break;
                        case "Proceed":
                            createPO();
                            break;
                        default:
                            viewCart();
                            break;
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cartListAdapter = new POCartListAdapter(getActivity(), cartItemsList);
        cartListAdapter.setListener(POCartFragment.this);
        mRecyclerView.setAdapter(cartListAdapter);
    }

    private void proceedToNextOld() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "Proceed";
            Intent intent = new Intent(getActivity(), PaymentActivity.class);
            if (cartId != null) {
                intent.putExtra("cartId", cartId);
            }
            intent.putExtra("TotalAmount", totalCartPrice);
            intent.putExtra("TotalItems", totalItems);
            startActivity(intent);
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "Proceed";
        }
    }

    private void createPO() {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject dataObject = createJSONForCreatePO();
            if (dataObject == null) {
                return;
            }
            callCreatePOAPI(dataObject);

        } else {
            //todo show network alert
        }
    }

    private void callCreatePOAPI(JSONObject dataObject) {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "Create PO";
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("data", dataObject.toString());
                isClicked = true;
                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.createPOURL, map, POCartFragment.this, POCartFragment.this, PARSER_TYPE.CREATE_PO);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.createPOURL);
                if (dialog != null)
                    dialog.show();
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "Create PO";
        }
    }

    private JSONObject createJSONForCreatePO() {
        String supplierId = mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, "");
        JSONObject dataObj = null;
        double totalPrice = 0.0;
        Calendar calendar = Calendar.getInstance();
        if (!TextUtils.isEmpty(supplierId)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(Utils.DATE_TIME_STD_PATTERN, Locale.getDefault());
                dataObj = new JSONObject();
                dataObj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                dataObj.put("platform_id", "5004");
                dataObj.put("po_date", sdf.format(calendar.getTime()));
                dataObj.put("validity", "7");
                calendar.add(Calendar.DAY_OF_YEAR, 7);
                dataObj.put("delivery_before", sdf.format(calendar.getTime()));
                dataObj.put("indent_id", "Manual Indent");
                dataObj.put("supplier_id", mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
                dataObj.put("warehouse_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                dataObj.put("po_type", "2");
                dataObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                dataObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                dataObj.put("remarks", "");

                ArrayList<CartModel> cartItems = DBHelper.getInstance().getPOCartItems(supplierId, mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                JSONArray productsArray = new JSONArray();
                if (cartItems != null && cartItems.size() > 0) {
                    for (CartModel model : cartItems) {
                        JSONObject object = new JSONObject();
                        object.put("product_id", model.getProductId());
                        object.put("qty", model.getQuantity());
                        object.put("packsize", model.getPackSize());
                        object.put("freeqty", model.getFreeqty() == 0 ? "0" : model.getFreeqty());
                        object.put("freepacksize", model.getPackSize());
                        object.put("total", model.getTotalPrice() + "");
                        object.put("applied_margin", model.getMargin());
                        object.put("unit_price", model.getUnitPrice());
                        object.put("pack_id", model.getProductPackId());
                        object.put("freebie_pack_id", model.getFreebiePackId());

                        productsArray.put(object);

                        totalPrice += model.getTotalPrice();
                    }
                }
                dataObj.put("po_total", totalPrice + "");
                dataObj.put("products", productsArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
        }
        return dataObj;
    }

    public void exportDatabase(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" +
                        getActivity().getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "Factail.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    private void updateCartCount() {
        this.cartCount = Utils.getPOCartCount(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""),
                mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
        getActivity().invalidateOptionsMenu();
        if (tvBadge != null) {
            tvBadge.setText(String.valueOf(this.cartCount));
        }
        totalCartPrice = Utils.getPOCartValue(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""),
                mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
        tvTotalAmount.setText(String.format(Locale.CANADA, "%.2f", totalCartPrice));
    }

    private void viewCart() {
        if (cartItemsList != null)
            cartItemsList.clear();
        if (cartListAdapter != null)
            cartListAdapter.clearData();

        cartItemsList.addAll(DBHelper.getInstance().getPOCartItems(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""),
                mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, "")));

        if (cartItemsList != null && cartItemsList.size() > 0) {
            if (isClicked && unavailable) {
                Utils.showAlertDialog(getActivity(), getString(R.string.unavailable_inventory));
                isClicked = false;
            }
            cartListAdapter.setArrCart(cartItemsList);
            cartListAdapter.notifyDataSetChanged();
        } else {
            // no items available in cart. Show message
            llCart.setVisibility(View.GONE);
            tvNoCart.setVisibility(View.VISIBLE);
        }

        updateCartCount();

    }

    @Override
    public void onItemClick(Object object, int position, int type) {
        if (type == 1) {//view details
            if (object instanceof CartModel) {
                CartModel cartModel = (CartModel) object;
                if (cartModel != null) {
                    boolean isChild = false;
                    if (cartModel.isChild() == 1) {
                        isChild = true;
                    } else {
                        isChild = false;
                    }
                    String parentId = cartModel.getParentId();
                    if (!isChild) {
                        parentId = DBHelper.getInstance().getParentId(cartModel.getParentId());
                    }
                    TestProductModel testProductModel = DBHelper.getInstance().getProductById(parentId, isChild);
                    if (testProductModel != null) {
//                        EditPOCartFragment fragment = EditPOCartFragment
//                                .newInstance(testProductModel, cartModel.getProductId(), parentId, cartModel.getQuantity(), cartModel.getProductPackId(),cartModel.getPackSize(), false);
                        EditPOCartFragment fragment = EditPOCartFragment
                                .newInstance(testProductModel, cartModel, parentId, false);
                        fragment.setClickListener(POCartFragment.this);
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        fragment.show(ft, "CartDialog");
                    } else {

                    }
                }
            }
        } else if (type == 0) {//delete cart item
            showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.are_you_sure_delete), object, position);
        }

    }

    private void getEditCartData(CartModel cartModel) {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "ViewCart";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
//                obj.put("cartId", productModel.getCartId());
                obj.put("product_id", cartModel.getProductId());
                obj.put("quantity", cartModel.getQuantity());

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.editCartDataURL, map, POCartFragment.this, POCartFragment.this, PARSER_TYPE.EDIT_CART_DATA);
                viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.editCartDataURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "ViewCart";
        }
    }

    public void showAlertDialog(Context context, String message, final Object object, final int pos) {

        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(getActivity().getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (object instanceof CartModel) {
                            selectedCartModel = (CartModel) object;
//                            selectedProductObject = productModel;
                            int results = deleteCart(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
                            if (results > 0) {//deleted Successfully.. Refresh the list
                                if (cartItemsList != null && cartItemsList.size() > 0) {
                                    try {
//                                        cartItemsList.remove(selectedCartModel);
                                        cartListAdapter.deleteItem(pos);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        viewCart();
                                    }
                                }
                            } else {// oops...! Something is wrong. Check Why deleting is failed.
                                Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
                            }
                        }
                    }
                })
                .setNegativeButton(getActivity().getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void deleteCart() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "DeleteCart";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("cartId", selectedCartModel.getCartId());
                obj.put("isClearCart", "false");
//                obj.put("product_id", selectedCartModel.getProductId());
//                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
//                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
//                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
//                obj.put("variant_id", productModel.getSkuModelArrayList().get(0).getSkuId());

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.deleteCartURL, map, POCartFragment.this, POCartFragment.this, PARSER_TYPE.DELETE_CART);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.deleteCartURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "DeleteCart";
        }
    }

    private int deleteCart(String supplierId) {
        int result = -1;
        if (TextUtils.isEmpty(supplierId)) {
            Utils.showAlertDialog(getActivity(), getString(R.string.something_wrong));
            return result;
        }

//        String cartId = DBHelper.getInstance().getPOCartId(supplierId, selectedCartModel.getProductId());
//        if(cartId!=null && !TextUtils.isEmpty(cartId) && !cartId.equalsIgnoreCase("null")){
//            deleteCart();
//        }

        result = Utils.deletePOCartRow(supplierId, selectedCartModel.getProductId(), false, "0");

        return result;
    }

    @Override
    public void dialogClose() {
        viewCart();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewCart();
        updateCartCount();

        mTracker.setScreenName("PO Cart Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.EDIT_CART_DATA) {
                        if (response instanceof NewProductModel) {
                        }

                    } else if (requestType == PARSER_TYPE.CREATE_PO) {
                        if (response instanceof String) {

                            String string = (String) response;
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                            dialog.setMessage(string);
                            dialog.setTitle(getString(R.string.app_name));
                            dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    DBHelper.getInstance().deletePOCartRow(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""), "", true, "0");

                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            AlertDialog alert = dialog.create();
                            alert.setCanceledOnTouchOutside(false);
                            alert.setCancelable(false);
                            alert.show();

                        }

                    } else if (requestType == PARSER_TYPE.DELETE_CART) {
                        //todo
                        viewCart();
                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void getCartCount() {
        if (mSharedPreferences.getBoolean("IsLoggedIn", false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                requestType = "CartCount";
                rlAlert.setVisibility(View.GONE);
                llMain.setVisibility(View.VISIBLE);

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    HashMap<String, String> map1 = new HashMap<>();
                    map1.put("data", obj.toString());

                    VolleyBackgroundTask cartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.cartCountURL, map1, POCartFragment.this, POCartFragment.this, PARSER_TYPE.CART_COUNT);
                    cartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(cartRequest, AppURL.cartCountURL);
                    if (dialog != null)
                        dialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                requestType = "CartCount";
                rlAlert.setVisibility(View.VISIBLE);
                llMain.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}