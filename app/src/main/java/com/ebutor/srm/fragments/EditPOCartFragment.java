package com.ebutor.srm.fragments;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.SimpleTagImageView;
import com.ebutor.database.DBHelper;
import com.ebutor.fragments.WishListFragment;
import com.ebutor.models.AddCart1Response;
import com.ebutor.models.CartModel;
import com.ebutor.models.NewPacksModel;
import com.ebutor.models.NewProductModel;
import com.ebutor.models.NewVariantModel;
import com.ebutor.models.TestProductModel;
import com.ebutor.models.WishListModel;
import com.ebutor.srm.adapters.PackTypesSpinnerAdapter;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class EditPOCartFragment extends DialogFragment implements Response.ErrorListener, VolleyHandler<Object> {

    public LayoutInflater inflater;
    OnDialogClosed onDialogClosed;
    JSONArray variantsJSONArray = new JSONArray();
    private ProgressBar progress;
    private TextView tvFreebieDesc;
    private TextView tvNoPacks;
    private NewPacksModel selectedPackModel, selectedFreePackModel, selectedFreePackModelPrev;
    private TestProductModel productModel, selectedWishListProductModel;
    private CartModel cartModel;
    private SimpleTagImageView productImage;
    private LinearLayout skuItemsLayout, slabsLayout, variantsLayout1, variantsLayout2, innerVariantsLayout1, innerVariantsLayout2;
    private TextView tvProductName,/* tvProductSKU,*/
            tvProductMRP, tvESP;
    private String variant_1_name, variant_2_name, variant_3_name, marginToApply, unitPriceToApply, name;
    private View view;
    private String selectedProductId;
    private int cartCount = 0, prevPos = 0, currentPos = 0;
    private TestProductModel selectedProductModel;
    private SharedPreferences mSharedPreferences;
    private DisplayImageOptions options;
    private TextView tvRating, tvTotalQty, tvTotalMargin, tvTotalPrice, tvFbQty;
    private int totalQty = 0, totalQtyToSend = 0, fbQty = 0, fbQtyTotal = 0;
    private int freebieMpq;
    private double totalPrice = 0.0, totalUnitPrice, totalMargin;
    private Tracker mTracker;
    private View offerDescView;
    private Button btnOfferImage;
    private ImageView ivWishList;
    private String freebieProductId, freebieQty;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private ArrayList<WishListModel> wishListArray;
    private String rating = "", productId, parentId, slabProductId, productPackId, level;
    private int quantity, freeQty, freebieSelectedPos = 0;
    private Context context;
    private boolean isAddToCart = false, isChild = false;
    private ArrayList<String> arrVariant2;
    private Dialog dialog;
    WishListFragment.OkListener confirmListener = new WishListFragment.OkListener() {
        @Override
        public void onOkClicked(String wishListId) {
            addProductToWishList(wishListId);
        }
    };
    private ImageButton ibPriceHistory;
    private double packPrice = 0.00, unitPrice = 0.00;
    private boolean isFreebieChecked = false;
    private int esu = 0;
    private Spinner packTypeSpinner;
    private ArrayList<NewPacksModel> arrSlabRates;
    View.OnClickListener onVariantClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            Toast.makeText(getActivity(), "Variant Clicked", Toast.LENGTH_SHORT).show();
            marginToApply = "0";
            unitPriceToApply = "0";
            if (v != null && v.getId() == R.id.id_variant_one) {
                int count = variantsLayout1.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout1.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                Object object = v.getTag();
                if (object != null && object instanceof String) {
                    variant_2_name = (String) object;

                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                    ll.removeAllViews();

                    innerVariantsLayout2 = (LinearLayout) view.findViewById(R.id.inner_variants_2);
                    innerVariantsLayout2.removeAllViews();
                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, "");
                    if (testProductModel != null) {
                        if (testProductModel.getProductTitle() != null && !TextUtils.isEmpty(testProductModel.getProductTitle()))
                            tvProductName.setText(testProductModel.getProductTitle().trim());
                        ImageLoader.getInstance().displayImage(testProductModel.getPrimaryImage(), productImage, options, animateFirstListener);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(testProductModel.getMrp());
                        } catch (Exception e) {
                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb, tvProductMRP);
                    }
                    String variant2list = DBHelper.getInstance().getVariant3List(parentId, variant_1_name, variant_2_name);
                    if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                        if (variant2list != null && variant2list.length() > 0)
                            arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));
                        if (arrVariant2 != null && arrVariant2.size() > 0) {
                            // has inner variants, Create one more variants row
                            View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                        /*int childCount = innerVariantsLayout.getChildCount();
                        if(childCount >= 2){
                            innerVariantsLayout.removeViewAt(1);
                        }*/
                            if (innerVariantsView != null) {
                                variantsLayout2 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout2 != null) {
                                    variantsLayout2.removeAllViews();
                                    int selectedPos = 0;
                                    String name = DBHelper.getInstance().getVariantName(productId, variant_1_name, variant_2_name, "", DBHelper.COL_PRODUCT_VARIANT3);
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "";
                                        if (arrVariant2.get(i).split(":").length > 0)
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        if (childVariant.equalsIgnoreCase(name)) {
//                                            selectedPos = i;
                                            TextView childVariantLabel = createChildVariant(childVariant, R.id.id_variant_two);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout2.addView(childVariantLabel);
                                        }

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }
                                    if (variantsLayout2.getChildCount() > 0) {
                                        variantsLayout2.getChildAt(selectedPos).performClick();
                                        variantsLayout2.getChildAt(selectedPos).setSelected(true);
                                    }
                                    innerVariantsLayout2.addView(innerVariantsView);

                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //add packs
                        if (testProductModel != null) {
                            slabProductId = testProductModel.getProductId();
                            selectedProductModel = testProductModel;
                            LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            getSlabRates(slabProductId);
                        }
                    }
                }
            } else if (v.getId() == R.id.id_variant_two) {
                //todo
                Object object = v.getTag();
                int count = variantsLayout2.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout2.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (offerDescView != null)
                    offerDescView.setVisibility(View.GONE);
                if (v.getTag() != null)
                    variant_3_name = (String) v.getTag();
                final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                ll.removeAllViews();
                TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, variant_3_name);
                if (testProductModel != null) {

                    if (testProductModel.getProductTitle() != null && !TextUtils.isEmpty(testProductModel.getProductTitle()))
                        tvProductName.setText(testProductModel.getProductTitle().trim());
                    ImageLoader.getInstance().displayImage(testProductModel.getPrimaryImage(), productImage, options, animateFirstListener);
                    double mrp = 0;
                    try {
                        mrp = Double.parseDouble(testProductModel.getMrp());
                    } catch (Exception e) {
                    }
                    final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                    setMRP(sb, tvProductMRP);

                    slabProductId = testProductModel.getProductId();
                    selectedProductModel = testProductModel;
                    LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                    slabRatesLayout.removeAllViews();
                    getSlabRates(slabProductId);

                }
            } else if (v.getId() == R.id.id_slab_qty) {
                if (v.getTag() != null) {
                    int slabsCount = slabsLayout.getChildCount();
                    for (int i = 0; i < slabsCount; i++) {
                        View btnView = slabsLayout.getChildAt(i);
                        btnView.setSelected(false);
                        selectedPackModel.setSelected(false);
                    }
                    v.setSelected(true);
                    selectedPackModel.setSelected(true);

                    NewPacksModel newPacksModel = (NewPacksModel) v.getTag();
                    if (tvESP != null) {

                        double ptr = 0;
                        try {
                            ptr = Double.parseDouble(newPacksModel.getPtr());
                        } catch (Exception e) {

                        }
                        if (ptr > 0) {
                            final SpannableStringBuilder sb = new SpannableStringBuilder(newPacksModel.getPtr() == null ? getActivity().getResources().getString(R.string.ptr) + " : " : getActivity().getResources().getString(R.string.ptr) + " : " + String.format(Locale.CANADA, "%.2f", ptr));
                            setMRP(sb, tvESP);

                        } else {
                            tvESP.setText("");
                        }
                    }
                    if (newPacksModel != null) {
                        selectedPackModel = newPacksModel;
                        final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
                        ll.removeAllViews();
                        createPacksForVariant(newPacksModel, ll);
                    }
                }
            }
        }
    };

    public static EditPOCartFragment newInstance(TestProductModel testProductModel, CartModel cartModel, String parentId, boolean isAddToCart) {

        EditPOCartFragment f = new EditPOCartFragment();
        Bundle args = new Bundle();
        args.putSerializable("model", testProductModel);
        args.putSerializable("cartModel", cartModel);
        args.putString("parentId", parentId);
//        args.putString("productPackId", productPackId);
//        args.putInt("qty", quantity);
//        args.putString("level", packSize);
        args.putBoolean("isAdd", isAddToCart);
        f.setArguments(args);
        return f;
    }

    public void setClickListener(OnDialogClosed listener) {
        onDialogClosed = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        theme = android.R.style.Theme_Holo_Dialog_NoActionBar;
        setStyle(style, theme);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_po_cart, container);
        return view;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.getSerializable("model") != null) {
            productModel = (TestProductModel) args.getSerializable("model");
        }
        if (args != null && args.getSerializable("cartModel") != null) {
            cartModel = (CartModel) args.getSerializable("cartModel");
        }
        if (args != null) {
            isAddToCart = args.getBoolean("isAdd");
            parentId = args.getString("parentId");
        }
        if (cartModel != null) {
            productId = cartModel.getProductId();
            productPackId = cartModel.getProductPackId();
            quantity = cartModel.getQuantity();
            level = cartModel.getPackSize();
            freeQty = cartModel.getFreeqty();
        }
        this.view = view;
        inflater = getActivity().getLayoutInflater();
        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();
        dialog = Utils.createLoader(getActivity(), ConstantValues.PROGRESS);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        wishListArray = new ArrayList<WishListModel>();

        productImage = (SimpleTagImageView) view.findViewById(R.id.productImage);
        tvProductName = (TextView) view.findViewById(R.id.tvProductName);
//        tvProductSKU = (TextView) view.findViewById(R.id.tvProductSKU);
        tvProductMRP = (TextView) view.findViewById(R.id.tvProductMRP);
        tvESP = (TextView) view.findViewById(R.id.tvESP);
        skuItemsLayout = (LinearLayout) view.findViewById(R.id.skuItemsLayout);

        tvRating = (TextView) view.findViewById(R.id.tvRating);
        ivWishList = (ImageView) view.findViewById(R.id.ivWishlist);
        ibPriceHistory = (ImageButton) view.findViewById(R.id.ib_price_history);
        ibPriceHistory.setVisibility(View.GONE);

        ivWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedWishListProductModel = productModel;
                // Wish list
                selectedProductId = productModel.getProductId();
                if (wishListArray.size() == 0) {
                    if (Networking.isNetworkAvailable(getActivity())) {
                        try {
                            JSONObject obj = new JSONObject();
                            obj.put("flag", "1");
                            obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                            obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                            HashMap<String, String> map = new HashMap<>();
                            map.put("data", obj.toString());

                            VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.shoppinglistURL, map, EditPOCartFragment.this, EditPOCartFragment.this, PARSER_TYPE.WISHLIST);
                            getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.shoppinglistURL);
                            if (dialog != null)
                                dialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.no_network_connection));
                    }
                } else {
                    showWishListDialog();
                }

                return;


            }
        });

//        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0 && productModel.getVariantModelArrayList().get(0).getProductName() != null && !TextUtils.isEmpty(productModel.getVariantModelArrayList().get(0).getProductName()))
//            tvProductName.setText(productModel.getProductTitle());
//        else
        tvProductName.setText(productModel.getProductTitle().trim());
        ImageLoader.getInstance().displayImage(productModel.getPrimaryImage(), productImage, options, animateFirstListener);
        double mrp = 0;
        try {
            mrp = Double.parseDouble(productModel.getMrp());
        } catch (Exception e) {
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(productModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
        setMRP(sb, tvProductMRP);
        ImageLoader.getInstance().displayImage(productModel.getPrimaryImage(), productImage, options, animateFirstListener);
//        rating = productModel.getProductRating();
//        if (rating == null || TextUtils.isEmpty(rating)) {
//            rating = "0.0";
//        }
//        if (rating != null && !TextUtils.isEmpty(rating) && !rating.equalsIgnoreCase("null")) {
//            DecimalFormat format = new DecimalFormat("0.0");
//            double newmargin = Double.parseDouble(rating);
//            rating = format.format(newmargin);
//        }
//
//        tvRating.setText(rating.equalsIgnoreCase("null") ? "0.0" : rating);
        skuItemsLayout.removeAllViews();
        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
            for (int j = 0; j < productModel.getVariantModelArrayList().size(); j++) {
                String variantName = DBHelper.getInstance().getVariantName(productId, "", "", "", DBHelper.COL_PRODUCT_VARIANT1);
//                if (name.equalsIgnoreCase(variantName)) {

                if (productModel.getVariantModelArrayList().get(j).split(":").length > 0)
                    name = productModel.getVariantModelArrayList().get(j).split(":")[0];
                final TextView textView = new TextView(getActivity());
                DisplayMetrics displaymetrics = new DisplayMetrics();
                WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
                windowManager.getDefaultDisplay().getMetrics(displaymetrics);
                int height = displaymetrics.heightPixels;
                textView.setTextSize(12);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                textView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
                textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
                params.setMargins(0, 0, 20, 0);
                textView.setPadding(20, 5, 20, 5);
                textView.setGravity(Gravity.CENTER);
                textView.setLayoutParams(params);
                textView.setId(R.id.id_one);
                textView.setTag(name);
                textView.setText(name == null ? "" : name);
                if (productModel.isChild() && j == 0) {
                    textView.setSelected(true);
                    OnProductSKUClicked(name, productModel.isChild());
                } else {
                    if (name.equalsIgnoreCase(variantName)) {
                        textView.setSelected(true);
                        OnProductSKUClicked(name, productModel.isChild());
                    } else {
                        textView.setSelected(false);
                    }
                }
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int count = skuItemsLayout.getChildCount();
                        if (count > 0) {
                            for (int i = 0; i < count; i++) {
                                View btnView = skuItemsLayout.getChildAt(i);
                                btnView.setSelected(false);
                            }
                        }
                        v.setSelected(true);
                        TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, name, "", "");
                        if (testProductModel != null) {
                            double mrp = 0;
                            try {
                                mrp = Double.parseDouble(testProductModel.getMrp());
                            } catch (Exception e) {
                            }
                            final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                            setMRP(sb, tvProductMRP);
                            ImageLoader.getInstance().displayImage(testProductModel.getPrimaryImage(), productImage, options, animateFirstListener);
                            tvProductName.setText(testProductModel.getProductTitle().trim());
                        }

                        OnProductSKUClicked(textView.getTag(), isChild);
                    }
                });
                skuItemsLayout.addView(textView);
//                }

                /*if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
                    final NewVariantModel SKUModel = productModel.getVariantModelArrayList().get(0);
                    if (SKUModel.getPacksModelArrayList() != null && SKUModel.getPacksModelArrayList().size() > 0) {
                        NewPacksModel skuPacksModel = SKUModel.getPacksModelArrayList().get(0);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(skuModel.getMrp());
                        } catch (Exception e) {
                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(skuModel.getMrp() == null ? "" : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb);
                    }
                }*/
            }

        }

//        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
//                                        /*SKUModel skuModel = productModel.getSkuModelArrayList().get(0);
//                                        OnProductSKUClicked(skuModel, expandedPosition);*/
//            NewVariantModel selectedSKUModel = null;
//            for (NewVariantModel skuModel : productModel.getVariantModelArrayList()) {
//                if (skuModel.isSelected()) {
//                    selectedSKUModel = skuModel;
//                    break;
//                }
//            }
//
//            if (selectedSKUModel == null) {
//                for (NewVariantModel skuModel : productModel.getVariantModelArrayList()) {
//                    if (skuModel.isDefault()) {
//                        selectedSKUModel = skuModel;
//                        break;
//                    }
//                }
//            }
//
//            if (selectedSKUModel != null) {
//                OnProductSKUClicked(selectedSKUModel);
//            }
//        }
    }

    private void setMRP(SpannableStringBuilder sb, TextView textView) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
//        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 3, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    public void OnProductSKUClicked(Object object, boolean isChild) {

        if (object == null)
            return;

        if (object instanceof String) {
//            final NewVariantModel model = (NewVariantModel) object;

//            int qty = 0, temp = 0, totalQuantity = 0;
//            try {
//                totalQuantity = Integer.parseInt(model.getOrderedQuantity());
//            } catch (Exception e) {
//                totalQuantity = 0;
//            }
//
//            if (model.getPacksModelArrayList() != null && model.getPacksModelArrayList().size() > 0) {
//                for (int i = model.getPacksModelArrayList().size() - 1; i >= 0; i--) {
//                    NewPacksModel skuPacksModel = model.getPacksModelArrayList().get(i);
//                    int packQty = Integer.parseInt(skuPacksModel.getPackSize());
//                    if (totalQuantity >= packQty) {
//                        qty = totalQuantity / packQty;
//                        skuPacksModel.setQty(qty * packQty);
//                        skuPacksModel.setEsuQty(qty);
//                        totalQuantity = totalQuantity % packQty;
//                        i++;
//                        continue;
//                    }
//                }
//            }

            variant_1_name = (String) object;
            marginToApply = "0";
            unitPriceToApply = "0";
            if (offerDescView != null)
                offerDescView.setVisibility(View.GONE);

            final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_expand_content);
            ll.removeAllViews();

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            innerVariantsLayout1 = (LinearLayout) view.findViewById(R.id.inner_variants);
            if (innerVariantsLayout1 != null) {
                innerVariantsLayout1.removeAllViews();

                if (isChild) {
                    slabProductId = parentId;
                    this.isChild = isChild;
                    TestProductModel testProductModel = DBHelper.getInstance().getProductById(parentId, true);
                    if (testProductModel != null) {
                        productId = testProductModel.getProductId();
                        selectedProductModel = testProductModel;
                        LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                        slabRatesLayout.removeAllViews();
                        getSlabRates(productId);
                    }
                } else {

                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, variant_2_name, "");
                    if (testProductModel != null) {
                        if (testProductModel.getProductTitle() != null && !TextUtils.isEmpty(testProductModel.getProductTitle()))
                            tvProductName.setText(testProductModel.getProductTitle().trim());
                        ImageLoader.getInstance().displayImage(testProductModel.getPrimaryImage(), productImage, options, animateFirstListener);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(testProductModel.getMrp());
                        } catch (Exception e) {
                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? getActivity().getResources().getString(R.string.mrp) + " : " : getActivity().getResources().getString(R.string.mrp) + " : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb, tvProductMRP);
                    }

                    String variant2list = DBHelper.getInstance().getVariant2List(parentId, variant_1_name);
                    if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                        if (variant2list != null && variant2list.length() > 0)
                            arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));

                        if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                            // has inner variants, Create one more variants row
                            View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                            if (innerVariantsView != null) {
                                variantsLayout1 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout1 != null) {
                                    variantsLayout1.removeAllViews();

                                    int selectedPos = 0;
                                    String name = DBHelper.getInstance().getVariantName(productId, variant_1_name, "", "", DBHelper.COL_PRODUCT_VARIANT2);
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "";
                                        if (arrVariant2.get(i).split(":").length > 0)
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        if (childVariant.equalsIgnoreCase(name)) {
//                                            selectedPos = i;
                                            TextView childVariantLabel = createChildVariant(childVariant, R.id.id_variant_one);
                                            childVariantLabel.setTag(childVariant);
                                            childVariantLabel.setOnClickListener(onVariantClickListener);
                                            variantsLayout1.addView(childVariantLabel);
                                        }

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout1.getChildCount() > 0) {
                                        variantsLayout1.getChildAt(selectedPos).performClick();
                                        variantsLayout1.getChildAt(selectedPos).setSelected(true);
                                    }
                                    innerVariantsLayout1.addView(innerVariantsView);


                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //create packs
                        testProductModel = DBHelper.getInstance().getParentProductDetails(parentId, variant_1_name, "", "");
                        if (testProductModel != null) {
                            slabProductId = testProductModel.getProductId();
                            selectedProductModel = testProductModel;
                            LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            getSlabRates(slabProductId);
                        }
                    }
                }
            }
        }
    }

    private void createPacksForVariant(final NewPacksModel skuPacksModel, final LinearLayout linearLayout) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        int qty = 0, totalQuantity = 0;
//        totalQuantity = quantity;

//        if (arrSlabRates != null && arrSlabRates.size() > 0) {
//            for (int i = arrSlabRates.size() - 1; i >= 0; i--) {
//                NewPacksModel newPacksModel = arrSlabRates.get(i);
//                int packQty = 0;
//                try {
//                    packQty = Integer.parseInt(newPacksModel.getPackSize());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (totalQuantity >= packQty) {
//                    qty = totalQuantity / packQty;
//                    newPacksModel.setQty(qty * packQty);
//                    newPacksModel.setEsuQty(qty);
//                    totalQuantity = totalQuantity % packQty;
//                    i++;
//                    continue;
//                }
//            }
//        }

        if (skuPacksModel != null) {
            View packView = inflater.inflate(R.layout.row_packs_header_supplier, null);

//            CheckBox cbFreebie = (CheckBox) packView.findViewById(R.id.cbFreebie);
            final EditText etPackPrice = (EditText) packView.findViewById(R.id.etPackPrice);
            final TextView tvUnitPrice = (TextView) packView.findViewById(R.id.tvUnitPrice);
            final TextView tvMargin = (TextView) packView.findViewById(R.id.tvMargin);

            int packSize = 0;

            try {
                packSize = Integer.parseInt(skuPacksModel.getPackSize());
            } catch (Exception e) {
                packSize = 0;
            }
            skuPacksModel.setUnitPrice(cartModel.getUnitPrice());

            esu = packSize;

            double packPrice = 0;
            try {
                packPrice = (Double.parseDouble(skuPacksModel.getPackSize()) * Double.parseDouble(skuPacksModel.getUnitPrice()));
            } catch (Exception e) {
            }
            EditPOCartFragment.this.packPrice = packPrice;
            etPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

            double unitPrice = 0;
            try {
                unitPrice = Double.parseDouble(skuPacksModel.getUnitPrice());
            } catch (Exception e) {
            }
            EditPOCartFragment.this.unitPrice = unitPrice;
            tvUnitPrice.setText(skuPacksModel.getUnitPrice() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", unitPrice));

            double margin = 0;
            try {
                margin = Double.parseDouble(skuPacksModel.getMargin());
            } catch (Exception e) {
            }
            tvMargin.setText(skuPacksModel.getMargin() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", margin) + "%");

            final TextView tvQty = (TextView) packView.findViewById(R.id.tvQuantity);
            tvQty.setText(quantity + "");
            ImageButton ibMinus = (ImageButton) packView.findViewById(R.id.ibMinus);
            ImageButton ibPlus = (ImageButton) packView.findViewById(R.id.ibPlus);

//            cbFreebie.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        isFreebieChecked = true;
//                        etPackPrice.setText("0.00");
//                        tvUnitPrice.setText("0.00");
//                        etPackPrice.setEnabled(false);
//                        SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n0.00");
//                        setSpannedText(sb, tvTotalPrice, 11);
//                        SpannableStringBuilder sb1 = new SpannableStringBuilder(getResources().getString(R.string.markup_margin) + "\n0.00%");
//                        setSpannedText(sb1, tvTotalMargin, 8);
//                    } else {
//                        isFreebieChecked = false;
//                        etPackPrice.setText(String.format(Locale.CANADA, "%.2f", EditPOCartFragment.this.packPrice));
//                        tvUnitPrice.setText(String.format(Locale.CANADA, "%.2f", EditPOCartFragment.this.unitPrice));
//                        if (tvTotalMargin != null) {
//                            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.markup_margin) + "\n" + String.format(Locale.CANADA, "%.2f", tvTotalMargin) + "%");
//                            setSpannedText(sb, tvTotalMargin, 8);
//                        }
//                        if (tvTotalPrice != null) {
//                            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
//                            setSpannedText(sb, tvTotalPrice, 11);
//
//                        }
//
//                        etPackPrice.setEnabled(true);
//                        etPackPrice.setSelection(etPackPrice.getText().toString().length());
//                    }
//                }
//            });

            etPackPrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && !isFreebieChecked) {
                        double unitPrice = 0, packPrice = 0, mrp = 0;
                        try {
                            mrp = Double.parseDouble(selectedProductModel.getMrp());
                            packPrice = Double.parseDouble(s.toString());
                            if (packPrice > mrp) {

                                unitPrice = Double.parseDouble(selectedPackModel.getUnitPrice());
                                Toast.makeText(getActivity(), "enter price less than mrp", Toast.LENGTH_SHORT).show();
                                etPackPrice.setText(String.valueOf(unitPrice * esu));
                                etPackPrice.setSelection(etPackPrice.getText().toString().length());
                                packPrice = unitPrice * esu;
                                return;
                            }
                            if (esu != 0)
                                unitPrice = Double.parseDouble(s.toString()) / esu;
                            else
                                unitPrice = 0;
                        } catch (Exception e) {
                            packPrice = 0;
                            unitPrice = 0;
                        }
                        EditPOCartFragment.this.packPrice = packPrice;
                        EditPOCartFragment.this.unitPrice = unitPrice;
                        skuPacksModel.setUnitPrice(unitPrice + "");
                        unitPriceToApply = unitPrice + "";
                        tvUnitPrice.setText(String.format(Locale.CANADA, "%.2f", unitPrice));
                        totalPrice = unitPrice * totalQtyToSend;
                        if (tvTotalPrice != null) {
                            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
                            setSpannedText(sb, tvTotalPrice, 11);
                        }
                        updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                    } else {
                        tvUnitPrice.setText("");
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            ibPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    String availableQty = model.getAvailableQuantity();
                    String availableQty = "100";//temp

                    if (availableQty != null && !TextUtils.isEmpty(availableQty)) {
                        int variantQuantity = 0;
                        try {
                            variantQuantity = Integer.parseInt(availableQty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (variantQuantity > 0) {
                            int qty = skuPacksModel.getQty();
                            int esuQty = skuPacksModel.getEsuQty();
//                            int qtyToSent = skuPacksModel.getQtyToBeSent();

                            int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                    0 : Integer.parseInt(skuPacksModel.getPackSize());
                            int updatedQty = qty + packSize;
                            esuQty = esuQty + 1;
//                            int qtyToBeSent = qtyToSent + 1;
                            tvQty.setText("" + esuQty);
                            skuPacksModel.setQty(updatedQty);
                            skuPacksModel.setEsuQty(esuQty);
//                            skuPacksModel.setQtyToBeSent(qtyToBeSent);

                            double packPrice = 0;
                            try {
                                packPrice = esu * Double.parseDouble(skuPacksModel.getUnitPrice());
                            } catch (Exception e) {
                                packPrice = 0;
                            }

                            EditPOCartFragment.this.packPrice = packPrice;
                            etPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

                            updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                        } else {
                            Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.quantity_not_available));
                        }
                    }
                }
            });

            ibMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = skuPacksModel.getQty();
                    int esuQty = skuPacksModel.getEsuQty();
//                    int qtyToSent = skuPacksModel.getQtyToBeSent();

                    if (esuQty <= 0) {
                        tvQty.setText("0");
                        skuPacksModel.setQty(0);
                        skuPacksModel.setEsuQty(esuQty);
                    } else {
                        int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                0 : Integer.parseInt(skuPacksModel.getPackSize());
                        int updatedQty = qty - packSize;
//                        int qtyToBeSent = qtyToSent - 1;
                        esuQty = esuQty - 1;
                        int quantityInEaches = getProductsInEaches(selectedPackModel, esuQty + "");
                        int freeQuantityInEachesPrev = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

                        if (quantityInEaches <= freeQuantityInEachesPrev) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (esuQty < 0) {
                            tvQty.setText("0");
                        } else {
                            tvQty.setText("" + esuQty);
                        }
                        skuPacksModel.setQty(updatedQty);
                        skuPacksModel.setEsuQty(esuQty);
//                        skuPacksModel.setQtyToBeSent(qtyToBeSent);

                        double packPrice = 0;
                        try {
                            packPrice = esu * Double.parseDouble(skuPacksModel.getUnitPrice());
                        } catch (Exception e) {
                            packPrice = 0;
                        }

                        EditPOCartFragment.this.packPrice = packPrice;
                        etPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

                    }
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                }
            });

            linearLayout.addView(packView);

            View freebieView = inflater.inflate(R.layout.row_freebie, null);
            ImageButton ibFbPlus = (ImageButton) freebieView.findViewById(R.id.ibFbPlus);
            ImageButton ibFbMinus = (ImageButton) freebieView.findViewById(R.id.ibFbMinus);
            packTypeSpinner = (Spinner) freebieView.findViewById(R.id.packs_spinner);
            packTypeSpinner.setAdapter(new PackTypesSpinnerAdapter(getActivity(), arrSlabRates));
            packTypeSpinner.setSelection(freebieSelectedPos);
            packTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (view.getTag() != null && view.getTag() instanceof NewPacksModel) {
                        prevPos = currentPos;
                        currentPos = position;
                        selectedFreePackModelPrev = selectedPackModel;
                        selectedFreePackModel = (NewPacksModel) view.getTag();

//                        int freeQtyInESU = skuPacksModel.getFreeQty();
                        int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

                        int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, tvQty.getText().toString());

                        if (freeQuantityInEaches > 0 && mainProductQtyInEaches <= freeQuantityInEaches) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                            packTypeSpinner.setSelection(prevPos);
                            selectedFreePackModel = selectedFreePackModelPrev;
                            return;
                        }
                        updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            tvFbQty = (TextView) freebieView.findViewById(R.id.tvFbQuantity);
            tvFbQty.setText(freeQty + "");

            ibFbPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int freeQtyInESU = skuPacksModel.getFreeQty();
                    int freeQuantityInEachesPrev = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

                    int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, tvQty.getText().toString());

                    if (mainProductQtyInEaches <= freeQuantityInEachesPrev) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int packSize = TextUtils.isEmpty(selectedFreePackModel.getPackSize()) ?
                            0 : Integer.parseInt(selectedFreePackModel.getPackSize());

                    int fbQty = freeQtyInESU + 1;
                    int fbQtyTotal = freeQuantityInEachesPrev + packSize;

                    if (mainProductQtyInEaches <= fbQtyTotal) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    skuPacksModel.setFreeQty(fbQty);
                    skuPacksModel.setFreeQtyTotal(fbQtyTotal);

                    tvFbQty.setText("" + fbQty);
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);


//                    int freeQtyInESU = skuPacksModel.getFreeQty();
//                    int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());
//
//                    int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, tvQty.getText().toString());
//
//                    if (mainProductQtyInEaches <= freeQuantityInEaches) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//                    int packSize = TextUtils.isEmpty(selectedFreePackModel.getPackSize()) ?
//                            0 : Integer.parseInt(selectedFreePackModel.getPackSize());
//
//                    int fbQty = freeQtyInESU + 1;
//                    int fbQtyTotal = freeQuantityInEaches + packSize;
//
//                    if (mainProductQtyInEaches <= fbQtyTotal) {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//                    skuPacksModel.setFreeQty(fbQty);
//                    skuPacksModel.setFreeQtyTotal(fbQtyTotal);
//
//                    tvFbQty.setText("" + fbQty);
//                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);

                }
            });

            ibFbMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int freeQtyInESU = skuPacksModel.getFreeQty();

                    if (freeQtyInESU <= 0) {
                        tvFbQty.setText("0");
                        return;
                    }

                    int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

//                    int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, tvQty.getText().toString());
//                    if(mainProductQtyInEaches <= freeQuantityInEaches){
//                        Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
//                        return;
//                    }

                    int packSize = TextUtils.isEmpty(selectedFreePackModel.getPackSize()) ?
                            0 : Integer.parseInt(selectedFreePackModel.getPackSize());

                    int fbQty = freeQtyInESU - 1;
                    int fbQtyTotal = freeQuantityInEaches - packSize;

                    skuPacksModel.setFreeQty(fbQty);
                    skuPacksModel.setFreeQtyTotal(fbQtyTotal);

                    tvFbQty.setText("" + fbQty);
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);


//                    int freeQtyInESU = skuPacksModel.getFreeQty();
//
//                    if (freeQtyInESU <= 0) {
//                        tvFbQty.setText("0");
//                        return;
//                    }
//
//                    int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());
//
//                    int packSize = TextUtils.isEmpty(selectedFreePackModel.getPackSize()) ?
//                            0 : Integer.parseInt(selectedFreePackModel.getPackSize());
//
//                    int fbQty = freeQtyInESU - 1;
//                    int fbQtyTotal = freeQuantityInEaches - packSize;
//
//                    skuPacksModel.setFreeQty(fbQty);
//                    skuPacksModel.setFreeQtyTotal(fbQtyTotal);
//
//                    tvFbQty.setText("" + fbQty);
//                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);

                }
            });
            linearLayout.addView(freebieView);

        }

        View footerView = inflater.inflate(R.layout.row_packs_footer, null);
        linearLayout.addView(footerView);

        footerView.findViewById(R.id.tvAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OnCartItemAdded(productModel);
            }
        });
        updateFooter(footerView, skuPacksModel);

        TextView tvAddToCart = (TextView) footerView.findViewById(R.id.tvAddToCart);
        if (isAddToCart)
            tvAddToCart.setText(getActivity().getResources().getString(R.string.add_to_cart));
        else
            tvAddToCart.setText(getActivity().getResources().getString(R.string.update_cart));
        tvAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int totalQuantity = 0;
//                NewVariantModel skuModel = productModel.getVariantModelArrayList().get(0);
//                for (int i = 0; i < skuModel.getPacksModelArrayList().size(); i++) {
//                    NewPacksModel skuPacksModel = skuModel.getPacksModelArrayList().get(i);
//                    totalQuantity += skuPacksModel.getQty();
//                }
//                if (totalQuantity > 0)
                OnCartItemAdded(productModel);
//                else
//                    Toast.makeText(getActivity(), "Total Quantity is zero", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private TextView createChildVariant(final String name, int id) {

        TextView textView = new TextView(getActivity());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        textView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sku_button_selector));
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.setMargins(0, 0, 20, 0);
        textView.setPadding(20, 5, 20, 5);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        textView.setId(id);
        textView.setTag(name);
        textView.setText(name == null ? "" : name);

        return textView;
    }

    private int getProductsInEaches(NewPacksModel selectedPackModel, String quantityEntered) {

        int packSize = selectedPackModel.getPackSize() == null ? 0 : Integer.parseInt(selectedPackModel.getPackSize());
        int enteredQuantity = TextUtils.isEmpty(quantityEntered) ? 0 : Integer.parseInt(quantityEntered);

        return packSize * enteredQuantity;
    }

    private void updateFooter(View footerView, NewPacksModel newPacksModel) {
        if (footerView == null) {
            return;
        }
        if (newPacksModel == null)
            return;

        totalQty = 0;
        totalQtyToSend = 0;
        fbQty = 0;
        fbQtyTotal = 0;
        marginToApply = "0";
        unitPriceToApply = "0";

        totalQty = newPacksModel.getQty();
        totalQtyToSend = newPacksModel.getEsuQty();
        fbQty = newPacksModel.getFreeQty();
        fbQtyTotal = getProductsInEaches(selectedFreePackModel, fbQty + "");

        marginToApply = newPacksModel.getMargin();
        unitPriceToApply = newPacksModel.getUnitPrice();

        freebieProductId = newPacksModel.getFreebieProductId();
        freebieMpq = newPacksModel.getFreebieMpq();
        //                freebieQty = String.valueOf((totalQty / packQty) * skuPacksmodel.getFreebieQty());
        double _qty = 0;
        if (freebieMpq > 0) {
            _qty = (int) (totalQty / freebieMpq) * newPacksModel.getFreebieQty();
        }
        freebieQty = String.valueOf((int) _qty);

        totalUnitPrice = 0;
        if (!TextUtils.isEmpty(unitPriceToApply)) {
            totalUnitPrice = Double.parseDouble(unitPriceToApply);
        }

        totalMargin = 0;
        if (!TextUtils.isEmpty(marginToApply)) {
            totalMargin = Double.parseDouble(marginToApply);
        }

//            skuModel.setAppliedMargin(marginToApply);
//            skuModel.setAppliedMRP(unitPriceToApply);

        tvTotalQty = (TextView) footerView.findViewById(R.id.tvTotalQty);
        tvTotalMargin = (TextView) footerView.findViewById(R.id.tvTotalMargin);
        tvTotalPrice = (TextView) footerView.findViewById(R.id.tvTotalPrice);

        int packSize = 0;
        try {
            packSize = Integer.parseInt(newPacksModel.getPackSize());
        } catch (Exception e) {
            packSize = 0;
        }
        quantity = totalQty / packSize;
        if (tvTotalQty != null) {
//                tvTotalQty.setText("TOTAL QTY\n" + totalQty);
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.qty) + "\n" + (totalQtyToSend));
            setSpannedText(sb, tvTotalQty, 3);
        }

        if (tvTotalMargin != null) {
//                tvMargin.setText("MARGIN\n" + String.format(Locale.CANADA, "%.2f", margin));
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.markup_margin) + "\n" + String.format(Locale.CANADA, "%.2f", totalMargin) + "%");
            setSpannedText(sb, tvTotalMargin, 8);
        }
        totalPrice = totalUnitPrice * (totalQty - fbQtyTotal);
//        skuModel.setTotalPrice(String.format(Locale.CANADA, "%.2f", totalPrice));
        if (tvTotalPrice != null) {
//                tvTotalPrice.setText("TOTAL PRICE\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            SpannableStringBuilder sb = new SpannableStringBuilder(getActivity().getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            setSpannedText(sb, tvTotalPrice, 11);

        }

        tvFreebieDesc = (TextView) view.findViewById(R.id.tvFreebieDesc);
        offerDescView = view.findViewById(R.id.ll_freebie_desc);
        btnOfferImage = (Button) view.findViewById(R.id.iv_offer_desc);
        if (selectedPackModel != null && selectedPackModel.getFreebieDesc() != null &&
                !TextUtils.isEmpty(selectedPackModel.getFreebieDesc()) && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("null") && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("0")) {
            tvFreebieDesc.setText(selectedPackModel.getFreebieDesc());
            offerDescView.setVisibility(View.VISIBLE);
            setOfferBanner(selectedProductModel.getPackType(), btnOfferImage);
        } else
            offerDescView.setVisibility(View.GONE);

    }

    public void setOfferBanner(String packType, View view) {

        if (view == null) {
            return;
        }
        Button ivOffer = (Button) view;

        switch (packType) {

            case "Consumer Pack":
            case "CONSUMER PACK":
            case "CP Inside":
            case "CP Outside":
            case "Consumer Pack Inside":
            case "Consumer Pack Outside":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("CP Offer");
                break;
            case "Freebie":
            case "FREEBIE":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Freebie");
                break;
            default:
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_no_text);
                ivOffer.setText("Offer");
                break;
        }
    }

    private void setSpannedText(SpannableStringBuilder sb, TextView textView, int start) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getActivity().getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new RelativeSizeSpan(1.2f), start, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(sb);
    }

    private void getSlabRates(String productId) {

        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.PURCHASE_SLABS;
            obj.put("product_id", productId);
            obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
            obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            obj.put("supplier_id", mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPurchaseSlabsURL, map,
                    EditPOCartFragment.this, EditPOCartFragment.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getPurchaseSlabsURL);

            final FrameLayout fl = (FrameLayout) view.findViewById(R.id.fl_expand_content);
            progress = (ProgressBar) fl.findViewById(R.id.loading);
            tvNoPacks = (TextView) fl.findViewById(R.id.tv_no_packs);
            tvNoPacks.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
//            if (dialog != null)
//                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void OnCartItemAdded(Object model) {
        if (model == null)
            return;

        if (model instanceof TestProductModel) {
            ArrayList<NewPacksModel> packs = arrSlabRates;
            totalQty = 0;
            totalQtyToSend = 0;
            if (packs != null && packs.size() > 0) {
                for (NewPacksModel packModel : packs) {
                    int qty = packModel.getEsuQty();
                    totalQtyToSend += qty;
                }
            }

            if (totalQtyToSend <= 0) {
                // show alert
                Utils.showAlertDialog(getActivity(), getString(R.string.please_add_packs));
                return;
            }

            CartModel cartModel = new CartModel();
            cartModel.setProductId(productId);
            cartModel.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
            cartModel.setQuantity(totalQtyToSend);
            cartModel.setUnitPrice(unitPriceToApply);
            cartModel.setMargin(marginToApply);
            cartModel.setTotalPrice(totalPrice);
            cartModel.setWarehouseId(mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
            cartModel.setDiscount("");//todo
            cartModel.setPackSize(selectedPackModel.getLevel());
            cartModel.setRemarks(selectedPackModel.getLevelName());
            cartModel.setProductPackId(selectedPackModel.getProductPackId());
            cartModel.setFreebiePackId(selectedFreePackModel.getProductPackId());
            int freeQty = 0;
            try {
                freeQty = Integer.parseInt(tvFbQty.getText().toString());
            } catch (Exception e) {
                freeQty = 0;
            }
            cartModel.setFreeqty(freeQty);
            cartModel.setIsChild(isChild == true ? 1 : 0);

            String _productEsu = selectedProductModel.getEsu();
            int esu = 1;
            if (_productEsu != null && !_productEsu.equals("0") && !TextUtils.isEmpty(_productEsu)) {
                try {
                    int tempEsu = Integer.parseInt(_productEsu);
                    esu = totalQtyToSend / tempEsu;
                } catch (Exception e) {
                    e.printStackTrace();
                    esu = 1;
                }
            }
            cartModel.setEsu(String.valueOf(esu));
            cartModel.setParentId(productId);
            if (isFreebieChecked) {
                cartModel.setIsFreebie("1");
            } else {
                cartModel.setIsFreebie("0");
            }

//            this.selectedProductModel = productModel;

            long rowId = Utils.insertOrUpdatePOCart(cartModel);
//                                if (rowId < 0) {
//                                    showToast("Failed to insert in Cart");
//                                } else {
//                                    showToast("Inserted in Cart with row ID " + rowId);
//                                }
            dismiss();
            onDialogClosed.dialogClose();
            updateCartCount();

        }

        updateCartCount(MyApplication.getInstance());
    }

    /*private void updateToCart() {
        if (MyApplication.getInstance().cartSize() > 0) {
//            HashMap<String, ProductModel> map = MyApplication.getInstance().getCurrentCartItemsMap();
//            Set<String> keys = map.keySet();
            try {
                JSONArray productsArray = new JSONArray();
                for (String key : keys) {
                    ProductModel productModel = map.get(key);
                    JSONObject productObj = new JSONObject();
                    productObj.put("product_id", productModel.getProductId());
                    ArrayList<SKUModel> variants = productModel.getSkuModelArrayList();
                    JSONArray variantsJSONArray = new JSONArray();
                    if (variants != null && variants.size() > 0) {
                        for (SKUModel skuModel : variants) {

                            ArrayList<SKUPacksModel> packs = skuModel.getSkuPacksModelArrayList();
                            JSONArray packsArray = new JSONArray();
                            int totalQty = 0;
                            if (packs != null && packs.size() > 0) {
                                for (SKUPacksModel packModel : packs) {
                                    int qty = packModel.getQty();
                                    if (qty > 0) {
                                        JSONObject packObj = new JSONObject();
                                        packObj.put("pack_qty", qty);
                                        packObj.put("packprice_id", packModel.getVariantPriceId());
                                        packsArray.put(packObj);
                                    }
                                    totalQty += qty;
                                }
                                JSONObject variantObj = new JSONObject();
                                variantObj.put("variant_id", skuModel.getSkuId());
                                variantObj.put("total_qty", totalQty);
                                variantObj.put("total_price", skuModel.getTotalPrice());
                                variantObj.put("packs", packsArray);
                                if (totalQty > 0)
                                    variantsJSONArray.put(variantObj);
                            }
                        }
                    }
                    productObj.put("variants", variantsJSONArray);
                    productsArray.put(productObj);
                }

                JSONObject dataObj = new JSONObject();
                dataObj.put("products", productsArray);
                dataObj.put("appId", "1525956d");//todo
                dataObj.put("token", "1164a6b649925cd394228c008c827166");//todo

                Log.e("Cart Obj", dataObj.toString());

                HashMap<String, String> paramsMap = new HashMap<>();
                paramsMap.put("data", dataObj.toString());
                HTTPBackgroundTask cartTask = new HTTPBackgroundTask(this, getActivity(), PARSER_TYPE.EDIT_CART, APIREQUEST_TYPE.HTTP_POST);
                cartTask.execute(paramsMap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/

    public JSONArray getInnerVariantsArray(ArrayList<NewVariantModel> variants) {
        try {
            for (NewVariantModel skuModel : variants) {
                if (skuModel.getHasInnerVariants() == 1) {
                    ArrayList<NewVariantModel> arrInner = skuModel.getVariantModelArrayList();
                    getInnerVariantsArray(arrInner);
                } else {
                    if (variants != null && variants.size() > 0) {
                        for (NewVariantModel newVariantModel : variants) {
                            JSONObject variantObj = new JSONObject();
                            variantObj.put("variant_id", newVariantModel.getSkuId());

                            ArrayList<NewPacksModel> packs = newVariantModel.getPacksModelArrayList();
                            JSONArray packsArray = new JSONArray();
                            int totalQty = 0;
                            if (packs != null && packs.size() > 0) {
                                for (NewPacksModel packModel : packs) {
                                    int qty = packModel.getQty();
//                                    if (qty > 0) {
                                    JSONObject packObj = new JSONObject();
                                    packObj.put("pack_qty", qty);
//                                    packObj.put("packprice_id", packModel.getProductSlabId());
                                    packsArray.put(packObj);
//                                    }
                                    totalQty += qty;
                                }
                            }
                            variantObj.put("total_qty", totalQty);
                            variantObj.put("total_price", newVariantModel.getTotalPrice());
                            variantObj.put("applied_margin", newVariantModel.getAppliedMargin());
                            variantObj.put("unit_price", newVariantModel.getAppliedMRP());
                            variantObj.put("packs", packsArray);
                            if (totalQty > 0) {
                                variantsJSONArray.put(variantObj);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return variantsJSONArray;
    }

    private void updateToCart(NewProductModel productModel) {
        variantsJSONArray = new JSONArray();
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                JSONArray productsArray = new JSONArray();

                JSONObject productObj = new JSONObject();
                productObj.put("product_id", productModel.getProductId());
                ArrayList<NewVariantModel> variants = productModel.getVariantModelArrayList();
                if (variants != null && variants.size() > 0) {
                    variantsJSONArray = getInnerVariantsArray(variants);
                }
                productObj.put("variants", variantsJSONArray);
                if (variantsJSONArray != null && variantsJSONArray.length() > 0) {
                    productsArray.put(productObj);
                }

                if (productsArray.length() > 0) {
                    JSONObject dataObj = new JSONObject();
                    dataObj.put("products", productsArray);
                    dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    dataObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    dataObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                    dataObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    Log.e("Cart Obj", dataObj.toString());

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", dataObj.toString());

                    VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCartURL, map, EditPOCartFragment.this, EditPOCartFragment.this, PARSER_TYPE.EDIT_CART);
                    viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.addCartURL);
                    if (dialog != null)
                        dialog.show();

                } else {
                    Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.please_add_packs));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
        }
    }

    private void updateCartCount(MyApplication application) {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
    }

    public void addProductToWishList(String wishListId) {
        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
            if (Networking.isNetworkAvailable(getActivity())) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("flag", "1");
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("buyer_listing_id", wishListId);
                    obj.put("product_id", this.selectedProductId);

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask addWishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productListOperationsURL, map,
                            EditPOCartFragment.this, EditPOCartFragment.this, PARSER_TYPE.ADDTO_WISHLIST);
                    addWishListRequest.setRetryPolicy(new DefaultRetryPolicy(
                            AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(addWishListRequest, AppURL.productListOperationsURL);
                    if (dialog != null)
                        dialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.no_network_connection));
            }

        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_login_addto_wishlist), Toast.LENGTH_SHORT).show();
        }
    }

    public void showWishListDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        WishListFragment editNameDialog = WishListFragment.newInstance(wishListArray);
        editNameDialog.setCancelable(true);
        editNameDialog.setConfirmListener(confirmListener);
        editNameDialog.show(fm, "areas_fragment_dialog");
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName(" Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
//        if (error != null) {
//            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.unexpected_error));
//        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.EDIT_CART) {

                    if (results instanceof Integer) {
                        if (isAddToCart) {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.product_added_cart), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.cart_updated), Toast.LENGTH_LONG).show();
                        }
                        dismiss();
                        onDialogClosed.dialogClose();
//                        getCartCount();
//                        MyApplication.getInstance().addToCart(selectedProductModel, mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""))
                    }

                } else if (requestType == PARSER_TYPE.WISHLIST) {
                    if (results instanceof ArrayList) {
                        wishListArray = (ArrayList<WishListModel>) results;
                        if (wishListArray != null && wishListArray.size() > 0) {
                            showWishListDialog();
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.please_create_list), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (requestType == PARSER_TYPE.ADDTO_WISHLIST) {
                    Utils.showAlertDialog(getActivity(), message);
                } else if (requestType == PARSER_TYPE.PURCHASE_SLABS) {

                    if (results instanceof ArrayList) {
                        if (progress != null)
                            progress.setVisibility(View.GONE);

                        arrSlabRates = (ArrayList<NewPacksModel>) results;

                        LinearLayout slabRatesLayout = (LinearLayout) view.findViewById(R.id.slab_rates);
                        slabRatesLayout.removeAllViews();
                        if (arrSlabRates != null && arrSlabRates.size() > 0) {

                            int qty = 0, totalQuantity = 0;
                            totalQuantity = quantity;

//                            if (arrSlabRates != null && arrSlabRates.size() > 0) {
//                                for (int i = arrSlabRates.size() - 1; i >= 0; i--) {
//                                    NewPacksModel newPacksModel = arrSlabRates.get(i);
//                                    int packQty = 0;
//                                    try {
//                                        packQty = Integer.parseInt(newPacksModel.getPackSize());
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                    if (totalQuantity >= packQty) {
//                                        qty = totalQuantity / packQty;
//                                        newPacksModel.setQty(qty * packQty);
//                                        newPacksModel.setEsuQty(qty);
//                                        totalQuantity = totalQuantity % packQty;
//                                        i++;
//                                        continue;
//                                    }
//                                }
//                            }

                            tvNoPacks.setVisibility(View.GONE);
                            View slabRatesView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                            slabsLayout = (LinearLayout) slabRatesView.findViewById(R.id.variants_layout);
                            if (slabsLayout != null) {
                                slabsLayout.removeAllViews();

                                int selectedPos = 0;
                                for (int i = 0; i < arrSlabRates.size(); i++) {
                                    selectedPackModel = arrSlabRates.get(i);
                                    String slab = arrSlabRates.get(i).getLevel();
                                    if (slab.equalsIgnoreCase(level)) {
                                        int packQty = 0;
                                        try {
                                            packQty = Integer.parseInt(arrSlabRates.get(i).getPackSize());
                                        } catch (Exception e) {
                                            packQty = 0;
                                            e.printStackTrace();
                                        }
                                        quantity = cartModel.getQuantity();
                                        selectedPackModel.setQty(cartModel.getQuantity() * packQty);
                                        selectedPackModel.setFreeQty(freeQty);
                                        selectedPackModel.setEsuQty(cartModel.getQuantity());
//                                        selectedPackModel.setQtyToBeSent(quantity);
                                        selectedPackModel.setFreeQtyTotal(freeQty * packQty);
                                        TextView childVariantLabel = createSlabRates(arrSlabRates.get(i).getLevelName() + "(" + arrSlabRates.get(i).getNoOfEaches() + ")", R.id.id_slab_qty);
                                        childVariantLabel.setTag(arrSlabRates.get(i));
                                        childVariantLabel.setOnClickListener(onVariantClickListener);
                                        slabsLayout.addView(childVariantLabel);
                                    }


                                    if (cartModel.getFreebiePackId().equalsIgnoreCase(arrSlabRates.get(i).getProductPackId())) {
                                        freebieSelectedPos = i;
                                    }
//                                    if (selectedPackModel.isSelected())
//                                        selectedPos = i;

                                }

                                if (packTypeSpinner != null) {
                                    packTypeSpinner.setSelection(freebieSelectedPos);
                                }

                                slabRatesLayout.addView(slabRatesView);
                                TextView tv = (TextView) slabsLayout.getChildAt(selectedPos);
                                tv.performClick();
                                tv.setSelected(true);
//                                slabsLayout.getChildAt(selectedPos).performClick();
//                                slabsLayout.getChildAt(selectedPos).setSelected(true);


                            }
                        } else {
                            if (tvNoPacks != null)
                                tvNoPacks.setVisibility(View.VISIBLE);
                        }
                    }

                } else if (requestType == PARSER_TYPE.ADD_CART_1) {
                    if (results instanceof AddCart1Response) {
                        AddCart1Response addCart1Response = (AddCart1Response) results;
                        if (addCart1Response.isStatus()) {
                            CartModel cartModel = new CartModel();
                            cartModel.setProductId(slabProductId);
                            cartModel.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
                            cartModel.setQuantity(totalQty);
                            cartModel.setUnitPrice(unitPriceToApply);
                            cartModel.setMargin(marginToApply);
                            cartModel.setTotalPrice(totalPrice);
                            cartModel.setWarehouseId("");//todo
                            cartModel.setDiscount("");//todo
                            cartModel.setProductPackId(productPackId);
                            cartModel.setFreebiePackId(selectedFreePackModel.getFreebiePackId());

                            String _productEsu = selectedProductModel.getEsu();
                            int esu = 1;
                            if (_productEsu != null && !_productEsu.equals("0") && !TextUtils.isEmpty(_productEsu)) {
                                try {
                                    int tempEsu = Integer.parseInt(_productEsu);
                                    esu = totalQty / tempEsu;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    esu = 1;
                                }
                            }
                            cartModel.setEsu(String.valueOf(esu));
                            cartModel.setParentId(slabProductId);
                            cartModel.setFreebieProductId(freebieProductId);
                            //check for freebie
                            if (freebieProductId != null && freebieQty != null && !freebieQty.equals("0") &&
                                    totalQty >= freebieMpq) {
                                cartModel.setFreebieQty(freebieQty);
                            }

                            long rowId = Utils.insertOrUpdatePOCart(cartModel);
//                        if (rowId < 0) {
//                            Toast.makeText(getActivity(), "Failed to insert in Cart", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(getActivity(), "Inserted in Cart with row ID " + rowId, Toast.LENGTH_SHORT).show();
//                        }
                            dismiss();
                            onDialogClosed.dialogClose();
                            updateCartCount();
                        } else {
                            Utils.showAlertWithMessage(getActivity(), String.format(getString(R.string.unavailable_inventory_product), addCart1Response.getAvailableQty()));
                        }
                    } else {

                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    private void updateCartCount() {
        this.cartCount = Utils.getCartCount(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_ID, ""));
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    private TextView createSlabRates(final String text, int id) {

        TextView textView = new TextView(getActivity());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.slab_selector));
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.sku_button_color_selector));
        params.setMargins(0, 0, 20, 0);
        textView.setPadding(20, 5, 20, 5);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        textView.setId(id);
        textView.setTag(text);
        textView.setText(text == null ? "" : text);

        return textView;
    }

    public interface OnDialogClosed {
        public void dialogClose();
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}