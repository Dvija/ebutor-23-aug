package com.ebutor.srm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.OrdersModel;
import com.ebutor.srm.activities.PODetailActivity;
import com.ebutor.srm.adapters.POListAdapter;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class POListFragment extends Fragment implements Response.ErrorListener, VolleyHandler<Object>, POListAdapter.OnItemClickListener {

    private SharedPreferences mSharedPreferences;
    private ArrayList<OrdersModel> poList;
    private POListAdapter adapter;
    private RecyclerView rvPOList;
    private TextView tvNoPOs, tvAlertMsg;
    private RelativeLayout rlAlert;
    private LinearLayout llMain;
    private Tracker mTracker;
    private Dialog dialog;
    private boolean isAllPOs = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_po_list, container, false);

        if (getArguments().containsKey("AllPos")) {
            isAllPOs = getArguments().getBoolean("AllPos");
        }

        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        rvPOList = (RecyclerView) v.findViewById(R.id.rvPOList);
        tvNoPOs = (TextView) v.findViewById(R.id.tvNoPOs);
        tvAlertMsg = (TextView) v.findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) v.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) v.findViewById(R.id.ll_main);

//        getPOList();

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    getPOList();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });
        MyApplication application = (MyApplication) getActivity().getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        return v;
    }

    private void getPOList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                if (!isAllPOs)
                    obj.put("supplier_id", mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask ordersListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPOListURL, map, POListFragment.this, POListFragment.this, PARSER_TYPE.PO_LIST);
                ordersListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(ordersListRequest, AppURL.getPOListURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), PODetailActivity.class);
        intent.putExtra("poId", poList.get(position).getOrderNumber());
        intent.putExtra("status", poList.get(position).getOrderStatus());
        getActivity().startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume();
        getPOList();
        mTracker.setScreenName("PO List Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.PO_LIST) {
                    if (results instanceof ArrayList) {
                        poList = (ArrayList<OrdersModel>) results;
                        if (poList != null && poList.size() > 0) {
                            rvPOList.setVisibility(View.VISIBLE);
                            tvNoPOs.setVisibility(View.GONE);
                            adapter = new POListAdapter(getActivity(), poList);
                            adapter.setClickListener(POListFragment.this);
                            rvPOList.setLayoutManager(new LinearLayoutManager(getActivity()));
                            rvPOList.setAdapter(adapter);

                        } else {
                            rvPOList.setVisibility(View.GONE);
                            tvNoPOs.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}
