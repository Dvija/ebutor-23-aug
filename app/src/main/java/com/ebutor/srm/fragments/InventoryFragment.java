package com.ebutor.srm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.adapters.InventoryRecyclerAdapter;
import com.ebutor.adapters.WarehouseSpinnerAdapter;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.InventoryModel;
import com.ebutor.srm.adapters.PlacesAdapter;
import com.ebutor.srm.models.WarehouseModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srikanth Nama on 30-Sep-16.
 */

public class InventoryFragment extends Fragment implements VolleyHandler, Response.ErrorListener {

    InventoryRecyclerAdapter mAdapter;
    private SharedPreferences mSharedPreferences;
    private Spinner spinWarehouse;
    private String selectedWarehouseId;
    private RecyclerView recyclerView;
    private ArrayList<WarehouseModel> warehouseModelArrayList;
    private ArrayList<InventoryModel> inventoryModelArrayList;
    private Dialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
//        mSharedPreferences.edit().putString(ConstantValues.KEY_SRM_TOKEN, "87c5b75e519aa052129c70c1ba4ce485").apply();//todo uncomment
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.frag_inventory, null);
        Toolbar toolbar = (Toolbar) convertView.findViewById(R.id.toolbar);

        //for crate home button
        ParentActivity activity = (ParentActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(getResources().getString(R.string.inventory));
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        return convertView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        spinWarehouse = (Spinner) view.findViewById(R.id.warehouseSpinner);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        final AutoCompleteTextView searchLocation = (AutoCompleteTextView) view.findViewById(R.id.et_product_name);
        // We explicitly set the searchLocation adapter after calling setText, so setText dosen't trigger the dropdown.
        searchLocation.setAdapter(new PlacesAdapter(getActivity(), android.R.layout.simple_list_item_1));
        searchLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view != null && view.getTag() != null) {
                    String productId = (String) view.getTag();
                    if (selectedWarehouseId != null && !TextUtils.isEmpty(selectedWarehouseId))
                        getProductInventory(productId);
                    else
                        Utils.showAlertDialog(getActivity(), getString(R.string.please_select_wh_id));
                } else {
                    //todo
                }
            }
        });

        getWarehouseList();

        spinWarehouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object obj = view.getTag();
                if (obj != null && obj instanceof WarehouseModel) {
                    selectedWarehouseId = ((WarehouseModel) obj).getWhId();
                    PlacesAdapter.whId = selectedWarehouseId;
                    PlacesAdapter.hubId = mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0");
                    searchLocation.setAdapter(new PlacesAdapter(getActivity(), android.R.layout.simple_list_item_1));
                    searchLocation.setText("");
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        inventoryModelArrayList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new InventoryRecyclerAdapter(getActivity(), inventoryModelArrayList);
        recyclerView.setAdapter(mAdapter);
    }

    private void getProductInventory(String productId) {


        if (Networking.isNetworkAvailable(getActivity())) {

            try {
                JSONObject object = new JSONObject();
                object.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                object.put("product_id", productId);
                object.put("le_wh_id", selectedWarehouseId);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", object.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getInventoryURL, map, InventoryFragment.this, InventoryFragment.this, PARSER_TYPE.GET_INVENTORY);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.getInventoryURL);

                if (dialog != null) {
                    dialog.show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.no_network_connection));
        }


    }

    public void getWarehouseList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            try {
                JSONObject obj = new JSONObject();
//                obj.put("flag", "1");
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
//                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
//                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
//                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getWarehouseListURL, map, InventoryFragment.this, InventoryFragment.this, PARSER_TYPE.GET_WH_LIST);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.getWarehouseListURL);

                if (dialog != null) {
                    dialog.show();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.no_network_connection));
        }
    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (response != null) {
            if (requestType == PARSER_TYPE.GET_WH_LIST) {
                if (response instanceof ArrayList) {
                    warehouseModelArrayList = (ArrayList<WarehouseModel>) response;
                    if (warehouseModelArrayList != null && warehouseModelArrayList.size() > 0) {
                        setAdapter();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_wh_available), Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (requestType == PARSER_TYPE.GET_INVENTORY) {
                if (response instanceof ArrayList) {
                    recyclerView.setVisibility(View.VISIBLE);
                    inventoryModelArrayList = (ArrayList<InventoryModel>) response;
                    mAdapter = new InventoryRecyclerAdapter(getActivity(), inventoryModelArrayList);
                    recyclerView.setAdapter(mAdapter);
                }
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.oops));
        }
    }

    private void setAdapter() {
        WarehouseSpinnerAdapter adapter = new WarehouseSpinnerAdapter(getActivity(), warehouseModelArrayList);
        spinWarehouse.setAdapter(adapter);
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
