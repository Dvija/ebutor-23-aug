package com.ebutor.srm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.PincodeDataModel;
import com.ebutor.srm.adapters.SpinnerAdapter;
import com.ebutor.srm.models.SupplierMasterLookupModel;
import com.ebutor.srm.models.SupplierModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srikanth Nama on 24-Nov-16.
 */

public class CreateSupplierFragment extends Fragment implements VolleyHandler, Response.ErrorListener {
    private SharedPreferences mSharedPreferences;
    private EditText etOrganizationName, etFirstName, etLastName, etEmail, etMobile;
    private EditText etAddress1, etAddress2, etPinCode, etCity, etState, etCountry;
    private Button btnSubmit;
    private Spinner spinOrganizationType, spinSupplierType, spinSupplierRank, spinManager;
    private ArrayList<CustomerTyepModel> organizationTypes, supplierTypes, supplierRanks, relationshipManagers;
    private String organizationType, supplierType, supplierRank, relationshipManagerId;
    private String selectedState;
    private Dialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
//        mSharedPreferences.edit().putString(ConstantValues.KEY_SRM_TOKEN, "7ce1472b61281cd13b35f8ed366e9696").apply();//todo uncomment
//        mSharedPreferences.edit().putString(ConstantValues.KEY_SRM_TOKEN, "7ce1472b61281cd13b35f8ed366e9696").apply();//todo uncomment
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     * <p>
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_supplier_basic_info, null);
//        Toolbar toolbar = (Toolbar) convertView.findViewById(R.id.toolbar);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        //for crate home button
        ParentActivity activity = (ParentActivity) getActivity();
//        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Create Supplier");
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
        }

        return convertView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etAddress1 = (EditText) view.findViewById(R.id.et_address1);
        etAddress2 = (EditText) view.findViewById(R.id.et_address2);
        etCity = (EditText) view.findViewById(R.id.et_city);
        etEmail = (EditText) view.findViewById(R.id.et_email);
        etMobile = (EditText) view.findViewById(R.id.et_mobile);
        etFirstName = (EditText) view.findViewById(R.id.et_first_name);
        etLastName = (EditText) view.findViewById(R.id.et_last_name);
        etPinCode = (EditText) view.findViewById(R.id.et_pin_code);
        etOrganizationName = (EditText) view.findViewById(R.id.et_organization_name);

        btnSubmit = (Button) view.findViewById(R.id.btn_submit);

        etCountry = (EditText) view.findViewById(R.id.spinner_country);
        spinSupplierType = (Spinner) view.findViewById(R.id.supplier_type);
        spinOrganizationType = (Spinner) view.findViewById(R.id.organization_type);
        spinSupplierRank = (Spinner) view.findViewById(R.id.supplier_rank);
        spinManager = (Spinner) view.findViewById(R.id.spin_manager);
        etState = (EditText) view.findViewById(R.id.et_state);

        getSupplierMasterLookupData();

        spinOrganizationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object object = view.getTag();
                if (object != null && object instanceof CustomerTyepModel) {
                    CustomerTyepModel model = (CustomerTyepModel) object;
                    organizationType = model.getCustomerGrpId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinManager.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object object = view.getTag();
                if (object != null && object instanceof CustomerTyepModel) {
                    CustomerTyepModel model = (CustomerTyepModel) object;
                    relationshipManagerId = model.getCustomerGrpId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinSupplierRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object object = view.getTag();
                if (object != null && object instanceof CustomerTyepModel) {
                    CustomerTyepModel model = (CustomerTyepModel) object;
                    supplierRank = model.getCustomerGrpId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinSupplierType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object object = view.getTag();
                if (object != null && object instanceof CustomerTyepModel) {
                    CustomerTyepModel model = (CustomerTyepModel) object;
                    supplierType = model.getCustomerGrpId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo add validations
                registerSupplier();
            }
        });
        etPinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() == 6) {
                    getPincodeAreas(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void getPincodeAreas(String pincode) {
        if (Networking.isNetworkAvailable(getActivity())) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pincode", pincode);
                HashMap<String, String> map = new HashMap<>();
                map.put("data", jsonObject.toString());

                VolleyBackgroundTask getStateRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPinCodeDataURL,
                        map, CreateSupplierFragment.this, CreateSupplierFragment.this, PARSER_TYPE.GET_PINCODE_DATA);
                getStateRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getStateRequest, AppURL.getPinCodeDataURL);
//                if (dialog != null)
//                    dialog.show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void registerSupplier() {
        if (Networking.isNetworkAvailable(getActivity())) {
            String organizationName = etOrganizationName.getText().toString().trim();
            if (TextUtils.isEmpty(organizationName)) {
                etOrganizationName.setError(getString(R.string.please_enter_organization_name));
                etOrganizationName.requestFocus();
                return;
            }

            if (TextUtils.isEmpty(organizationType)) {
                Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_organization_type));
                return;
            }

            if (TextUtils.isEmpty(supplierType)) {
                Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_supplier_type));
                return;
            }

            if (TextUtils.isEmpty(supplierRank)) {
                Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_supplier_rank));
                return;
            }

            String address1 = etAddress1.getText().toString().trim();
            if (TextUtils.isEmpty(address1)) {
                etAddress1.setError(getString(R.string.please_enter_address1));
                etAddress1.requestFocus();
                return;
            }
            String address2 = etAddress2.getText().toString().trim();
            if (TextUtils.isEmpty(address2)) {
                etAddress2.setError(getString(R.string.please_enter_address2));
                etAddress2.requestFocus();
                return;
            }
            String pinCode = etPinCode.getText().toString().trim();
            if (TextUtils.isEmpty(pinCode)) {
                etPinCode.setError(getString(R.string.please_enter_pin));
                etPinCode.requestFocus();
                return;
            }
            String city = etCity.getText().toString().trim();
            if (TextUtils.isEmpty(city)) {
                etCity.setError(getString(R.string.please_enter_city));
                etCity.requestFocus();
                return;
            }

            String firstName = etFirstName.getText().toString().trim();
            if (TextUtils.isEmpty(firstName)) {
                etFirstName.setError(getString(R.string.please_enter_first_name));
                etFirstName.requestFocus();
                return;
            }

            String lastName = etLastName.getText().toString().trim();
            if (TextUtils.isEmpty(lastName)) {
                etLastName.setError(getString(R.string.please_enter_last_name));
                etLastName.requestFocus();
                return;
            }

            String email = etEmail.getText().toString().trim();
            if (TextUtils.isEmpty(email)) {
                etEmail.setError(getString(R.string.please_enter_valid_email));
                etEmail.requestFocus();
                return;
            }
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                etEmail.setError(getString(R.string.please_enter_valid_email));
                etEmail.requestFocus();
                return;
            }

            String mobile = etMobile.getText().toString().trim();
            if (TextUtils.isEmpty(mobile)) {
                etMobile.setError(getString(R.string.please_enter_mobile_number));
                etMobile.requestFocus();
                return;
            }

            if (mobile.length() != 10) {
                etMobile.setError(getString(R.string.please_enter_valid_mobile_number));
                etMobile.requestFocus();
                return;
            }

            if (TextUtils.isEmpty(relationshipManagerId)) {
                Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_relationship_manager));
                return;
            }

            try {
                JSONObject obj = new JSONObject();
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                JSONObject supplierInfo = new JSONObject();
                supplierInfo.put("organization_name", organizationName);
                supplierInfo.put("organization_type", organizationType);
                supplierInfo.put("org_address1", address1);
                supplierInfo.put("org_address2", address2);
                supplierInfo.put("org_country", "99");// todo India hard coded
                supplierInfo.put("org_state", selectedState);
                supplierInfo.put("org_city", city);
//                supplierInfo.put("org_site", "www.ebutor.com");//todo comment
                supplierInfo.put("org_pincode", pinCode);
                supplierInfo.put("supplier_rank", supplierRank);
                supplierInfo.put("supplier_type", supplierType);
                supplierInfo.put("rm_id", relationshipManagerId);
                obj.put("supplier_info", supplierInfo);
                JSONObject contactInfo = new JSONObject();
                contactInfo.put("org_firstname", firstName);
                contactInfo.put("org_lastname", lastName);
                contactInfo.put("org_email", email);
                contactInfo.put("org_mobile", mobile);
                obj.put("contact_info", contactInfo);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.createSupplierURL, map, CreateSupplierFragment.this, CreateSupplierFragment.this, PARSER_TYPE.CREATE_SUPPLIER);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.createSupplierURL);
                if (dialog != null)
                    dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //todo show network alert
        }
    }

    private void getSupplierMasterLookupData() {
        if (Networking.isNetworkAvailable(getActivity())) {

            try {
                JSONObject obj = new JSONObject();
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask deleteCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getSupplierMasterLookupURL, map, CreateSupplierFragment.this, CreateSupplierFragment.this, PARSER_TYPE.GET_SUPPLIER_MASTER_LOOKUP_DATA);
                deleteCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(deleteCartRequest, AppURL.getSupplierMasterLookupURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //todo show network alert
        }
    }

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     *
     * @param error
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();

            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (requestType == PARSER_TYPE.GET_SUPPLIER_MASTER_LOOKUP_DATA) {
                if (response instanceof SupplierMasterLookupModel) {
                    SupplierMasterLookupModel model = (SupplierMasterLookupModel) response;
                    organizationTypes = model.getOrganizationTypes();
                    supplierTypes = model.getSupplierTypes();
                    supplierRanks = model.getSupplierRanks();
                    relationshipManagers = model.getRelationshipManagers();

                    if (organizationTypes != null) {
                        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), organizationTypes);
                        spinOrganizationType.setAdapter(adapter);
                    }

                    if (supplierTypes != null) {
                        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), supplierTypes);
                        spinSupplierType.setAdapter(adapter);
                    }

                    if (supplierRanks != null) {
                        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), supplierRanks);
                        spinSupplierRank.setAdapter(adapter);
                    }
                    if (relationshipManagers != null) {
                        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), relationshipManagers);
                        spinManager.setAdapter(adapter);
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.oops), Toast.LENGTH_SHORT).show();
                }
            } else if (requestType == PARSER_TYPE.GET_PINCODE_DATA) {
                if (response instanceof PincodeDataModel) {
                    PincodeDataModel pincodeDataModel = (PincodeDataModel) response;
                    String state = pincodeDataModel.getStateName();
                    etState.setText(state);
                    etCountry.setText("India");
                    selectedState = pincodeDataModel.getStateId();

//                    spinState.setSelection(pos);
//                    spinState.setEnabled(false);

                }
            } else if (requestType == PARSER_TYPE.CREATE_SUPPLIER) {
                if (response instanceof SupplierModel) {

                    SupplierModel model = (SupplierModel) response;
//                    Toast.makeText(getActivity(), "Created Supplier with Supplier ID " + model.supplierId, Toast.LENGTH_SHORT).show();
                    /*Fragment frag = SubscribeProductsFragment.newInstance(model);
                    ((RegisterSupplierActivity)getActivity()).replaceFragment(frag);*/
                    Intent intent = getActivity().getIntent();
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();
                }
            }
        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.oops));
        }
    }

    @Override
    public void onSessionError(String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        Utils.logout(getActivity(), message);
    }
}
