package com.ebutor.srm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.activities.POApprovalDetailsActivity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.fragments.CancelRequestFragment;
import com.ebutor.fragments.ReturnRequestFragment;
import com.ebutor.models.ReasonsModel;
import com.ebutor.srm.adapters.POApprovalDetailAdapter;
import com.ebutor.srm.adapters.PODetailAdapter;
import com.ebutor.srm.models.PODetailModel;
import com.ebutor.srm.models.POModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.FeatureCodes;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class PODetailFragment extends Fragment implements CancelRequestFragment.OnDialogClosed, ReturnRequestFragment.OnDialogClosed, VolleyHandler<Object>, Response.ErrorListener {

    private Button btnAction;
    private boolean isCancelPOStatus = false, isrequestPOStatus = false;
    private TextView tvPONumber, tvPODate, tvStatus, tvApprovalStatus;
    private ListView lvList;
    private PODetailAdapter poDetailAdapter;
    private POApprovalDetailAdapter poApprovalDetailAdapter;
    private ArrayList<POModel> arrPOs;
    private SharedPreferences mSharedPreferences;
    private ArrayList<ReasonsModel> reasonArray;
    private String variantId = "", productId = "", poId = "", status, requestType = "", poDate;
    private TextView tvNumberOfItems, tvTotalAmount, tvDeliveryAddress, tvSupplierAddress, tvAlertMsg, tvDiscountOnBill;
    private RelativeLayout rlAlert;
    private Tracker mTracker;
    private Dialog dialog;
    private boolean isEdit = false, isFromApproval = false;
    private double totalAmount;
    private PODetailModel poDetailModel;
    private LinearLayout llMain;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_po_details, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments().containsKey("isFromApproval"))
            isFromApproval = getArguments().getBoolean("isFromApproval", false);

        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);

        tvPONumber = (TextView) view.findViewById(R.id.tvPONumber);
        tvNumberOfItems = (TextView) view.findViewById(R.id.tvNumberOfItems);
        tvTotalAmount = (TextView) view.findViewById(R.id.tvTotalAmount);
        tvDeliveryAddress = (TextView) view.findViewById(R.id.tv_delivery_address);
        tvSupplierAddress = (TextView) view.findViewById(R.id.tv_supplier_address);
        tvPODate = (TextView) view.findViewById(R.id.tv_po_date);
        tvStatus = (TextView) view.findViewById(R.id.tvStatus);
        tvApprovalStatus = (TextView) view.findViewById(R.id.tvApprovalStatus);
        tvAlertMsg = (TextView) view.findViewById(R.id.tv_alert_msg);
        tvDiscountOnBill = (TextView) view.findViewById(R.id.tv_discount_on_bill);
        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        lvList = (ListView) view.findViewById(R.id.list);

        arrPOs = new ArrayList<>();
        btnAction = (Button) view.findViewById(R.id.btn_po_cancel);

        MyApplication application = (MyApplication) getActivity().getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        /*final Bundle args = getArguments();
        if (args != null && args.getSerializable("poId") != null) {
            poId = args.getString("poId");
        }
        if (args != null && args.getSerializable("status") != null) {
            status = args.getString("status");
        }*/

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null && extras.containsKey("poId")) {
            poId = extras.getString("poId");
        }

        if (extras != null && extras.containsKey("status")) {
            status = extras.getString("status");
        }

        btnAction.setText(getActivity().getResources().getString(R.string.edit));

        btnAction.setTextColor(getResources().getColor(R.color.white));

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    updatePO();
                } else {
                    btnAction.setText(getActivity().getResources().getString(R.string.save));
                    poDetailAdapter = new PODetailAdapter(getActivity(), arrPOs, true);
                    lvList.setAdapter(poDetailAdapter);
                    Utils.setListViewHeightBasedOnItems(lvList);
                    isEdit = true;
                }
            }
        });
        if (isFromApproval) {
            poApprovalDetailAdapter = new POApprovalDetailAdapter(getActivity(), arrPOs, false);
            lvList.setAdapter(poApprovalDetailAdapter);
        } else {
            poDetailAdapter = new PODetailAdapter(getActivity(), arrPOs, false);
            lvList.setAdapter(poDetailAdapter);
        }
        Utils.setListViewHeightBasedOnItems(lvList);

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(getActivity())) {
                    switch (requestType) {
                        case "PODetail":
                            getPODetail(poId);
                            break;
                        case "ReturnReasons":
                            getReturnReasonsList();
                            break;
                        case "ReasonsList":
                            getCancelReasonsList();
                            break;
                        default:
                            getPODetail(poId);
                            break;

                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        getPODetail(poId);

    }

    public void updatePO() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "Update PO";

            HashMap<String, String> map = new HashMap<>();
            try {

                double totalAmount = 0.0;

                JSONObject detailsObj = new JSONObject();
                detailsObj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                detailsObj.put("platform_id", "5004");
                detailsObj.put("po_id", poId);
                detailsObj.put("po_date", poDate);
                detailsObj.put("validity", "7");
                detailsObj.put("delivery_before", poDate);
                detailsObj.put("indent_id", "Manual Indent");
                if (poDetailModel != null) {
                    detailsObj.put("supplier_id", poDetailModel.getSupplierId());
                    detailsObj.put("warehouse_id", poDetailModel.getWarehouseId());
                }
                detailsObj.put("po_type", "2");

                JSONArray productsArray = new JSONArray();
                for (int i = 0; i < arrPOs.size(); i++) {

                    /*int qty = 0, noOfEaches = 0, fbQty = 0;
                    try {
                        qty = Integer.parseInt(arrPOs.get(i).getQuantity());
                        noOfEaches = Integer.parseInt(arrPOs.get(i).getNoOfEaches());
                        fbQty = Integer.parseInt(arrPOs.get(i).getFreeQty());
                    } catch (Exception e) {
                        qty = 0;
                        noOfEaches = 0;
                    }
                    double price = 0.0;
                    try {
                        price = Double.parseDouble(arrPOs.get(i).getUnitPrice());
                    } catch (Exception e) {
                        price = 0.0;
                    }

                    totalAmount += price * (qty - fbQty) * noOfEaches;*/
                    if (TextUtils.isEmpty(arrPOs.get(i).getQuantity())) {
                        Toast.makeText(getActivity(), getString(R.string.please_enter_qty), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        totalAmount += Double.parseDouble(arrPOs.get(i).getProductTotal());
                    } catch (Exception e) {
                        e.printStackTrace();
                        totalAmount += 0;
                    }

                    JSONObject productObj = new JSONObject();
                    productObj.put("product_id", arrPOs.get(i).getProductId());
                    productObj.put("qty", arrPOs.get(i).getQuantity());
                    productObj.put("freeqty", arrPOs.get(i).getFreeQty());
                    productObj.put("unit_price", arrPOs.get(i).getUnitPrice());
                    productObj.put("total", arrPOs.get(i).getProductTotal());
                    productObj.put("packsize", arrPOs.get(i).getPackSizeValue());
                    productObj.put("freepacksize", arrPOs.get(i).getFreePackSizeValue());
                    productObj.put("pack_id", arrPOs.get(i).getPackId());
                    productObj.put("freebie_pack_id", arrPOs.get(i).getFreePackId());
                    productObj.put("parent_id", arrPOs.get(i).getParentId());

                    productsArray.put(productObj);

                }
                detailsObj.put("po_total", totalAmount);
                detailsObj.put("products", productsArray);
                detailsObj.put("remarks", "");

                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    detailsObj.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    detailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }

                map.put("data", detailsObj.toString());
            } catch (Exception e) {

            }

            VolleyBackgroundTask poListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.updatePOURL, map, PODetailFragment.this, PODetailFragment.this, PARSER_TYPE.UPDATE_PO);
            poListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(poListRequest, AppURL.updatePOURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "Update PO";
        }
    }

    private void getPODetail(String poId) {

        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "PODetail";
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject detailsObj = new JSONObject();
                detailsObj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                detailsObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                detailsObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                detailsObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                detailsObj.put("po_id", poId);
                if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_FF, false)) {
                    detailsObj.put("sales_rep_id", mSharedPreferences.getString(ConstantValues.KEY_FF_ID, ""));
                    detailsObj.put("sales_token", mSharedPreferences.getString(ConstantValues.KEY_FF_TOKEN, ""));
                }

                map.put("data", detailsObj.toString());
            } catch (Exception e) {

            }

            VolleyBackgroundTask poListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPODetailURL, map, PODetailFragment.this, PODetailFragment.this, PARSER_TYPE.PO_DETAIL);
            poListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(poListRequest, AppURL.getPODetailURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "PODetail";
        }
    }

    private void getReturnReasonsList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "ReturnReasons";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask returnReasonsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.returnReasonsURL, map, PODetailFragment.this, PODetailFragment.this, PARSER_TYPE.RETURN_REASONS);
                returnReasonsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyApplication.getInstance().addToRequestQueue(returnReasonsReq, AppURL.returnReasonsURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "ReturnReasons";
        }
    }

    private void getCancelReasonsList() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            requestType = "ReasonsList";
            try {
                JSONObject obj = new JSONObject();
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask cancelReasonsReq = new VolleyBackgroundTask(Request.Method.POST, AppURL.cancelReasonsURL, map, PODetailFragment.this, PODetailFragment.this, PARSER_TYPE.CANCEL_REASONS);
                cancelReasonsReq.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyApplication.getInstance().addToRequestQueue(cancelReasonsReq, AppURL.cancelReasonsURL);
                if (dialog != null)
                    dialog.show();


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            requestType = "ReasonsList";
        }
    }

    @Override
    public void dialogClose(String poId) {
        if (poId.equalsIgnoreCase(getActivity().getResources().getString(R.string.no_network))) {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        } else {
            getPODetail(poId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("PO Details Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            if (status.equalsIgnoreCase("success")) {

                if (requestType == PARSER_TYPE.PO_DETAIL) {
                    if (response instanceof PODetailModel) {
                        poDetailModel = (PODetailModel) response;
                        arrPOs = new ArrayList<>();
                        String poId = poDetailModel.getPoId();
                        poDate = poDetailModel.getPoDate();
                        String totalStatus = poDetailModel.getPoStatus();

                        String supplierName = poDetailModel.getSupplierName();
                        String supplierAddress1 = poDetailModel.getSupplierAddress1();
                        String supplierAddress2 = poDetailModel.getSupplierAddress2();
                        String supplierCity = poDetailModel.getSupplierCity();
                        String supplierPincode = poDetailModel.getSupplierPincode();
                        String supplierCountry = poDetailModel.getSupplierCountry();
                        String supplierMobile = poDetailModel.getSupplierMobile();
                        String shippingEmail = poDetailModel.getSupplierEmail();

                        String whName = poDetailModel.getWhName();
                        String contactName = poDetailModel.getContactName();
                        String deliveryAddress1 = poDetailModel.getDeliveryAddress1();
                        String deliveryAddress2 = poDetailModel.getDeliveryAddress2();
                        String deliveryCity = poDetailModel.getDeliveryCity();
                        String deliveryPincode = poDetailModel.getDeliveryPincode();
                        String deliveryCountry = poDetailModel.getDeliveryCountry();
                        String deliveryMobile = poDetailModel.getDeliveryMobile();
                        String deliveryEmail = poDetailModel.getDeliveryEmail();
                        boolean isApproval = poDetailModel.isApproval();

                        if (isFromApproval && getActivity() instanceof POApprovalDetailsActivity) {
                            MenuItem menuItem = ((POApprovalDetailsActivity) getActivity()).approvalItem;
                            if (menuItem != null && isApproval) {
                                menuItem.setVisible(true);
                            }
                        }

                        btnAction.setText(getActivity().getResources().getString(R.string.edit));
                        if (DBHelper.getInstance().checkIfFeatureAllowed(FeatureCodes.EDIT_PO_FEATURE_CODE) >= 0) {

                            if (!isFromApproval && (poDetailModel.getPoStatus().equalsIgnoreCase("OPEN") || poDetailModel.getPoStatus().equalsIgnoreCase("open"))) {
                                btnAction.setVisibility(View.VISIBLE);
                            } else {
                                btnAction.setVisibility(View.GONE);
                            }
                        } else {
                            btnAction.setVisibility(View.GONE);
                        }

                        if (TextUtils.isEmpty(supplierAddress2)) {
                            tvSupplierAddress.setText(String.format(getResources().getString(R.string.userAddress6), supplierName,
                                    supplierAddress1, supplierCity, supplierCountry, supplierPincode, supplierMobile));
                        } else {
                            tvSupplierAddress.setText(String.format(getResources().getString(R.string.userAddress5), supplierName,
                                    supplierAddress1, supplierAddress2, supplierCity, supplierCountry, supplierPincode, supplierMobile));
                        }
                        if (TextUtils.isEmpty(deliveryAddress2)) {
                            tvDeliveryAddress.setText(String.format(getResources().getString(R.string.userAddress6), contactName,
                                    deliveryAddress1, deliveryCity, deliveryCountry, deliveryPincode, deliveryMobile));
                        } else {
                            tvDeliveryAddress.setText(String.format(getResources().getString(R.string.userAddress5), contactName,
                                    deliveryAddress1, deliveryAddress2, deliveryCity, deliveryCountry, deliveryPincode, deliveryMobile));
                        }

                        String total = poDetailModel.getTotalAmount();
                        try {
                            totalAmount = Double.parseDouble(total);
                            tvTotalAmount.setText(String.format(getResources().getString(R.string.rupee)) + " " + String.format(Locale.CANADA, "%.2f", totalAmount));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        String poCode = poDetailModel.getPoNumber();

                        tvPONumber.setText(poCode == null ? poId : poCode);

                        tvPODate.setText(Utils.parseDate(Utils.DATE_TIME_STD_PATTERN, Utils.DATE_TIME_PATTERN, poDate));
                        tvStatus.setText(poDetailModel.getPoStatus());
                        tvApprovalStatus.setText(poDetailModel.getPoApprovalStatus());
                        tvDiscountOnBill.setText(getResources().getString(R.string.apply) + ": " + poDetailModel.getApplyDiscount() + ", " + getResources().getString(R.string.value) + ": " + poDetailModel.getDiscount() + " " + poDetailModel.getDiscountType());
                        arrPOs = poDetailModel.getArrPos();

                        if (arrPOs != null && arrPOs.size() > 0) {
                            if (isFromApproval) {
                                poApprovalDetailAdapter = new POApprovalDetailAdapter(getActivity(), arrPOs, false);
                                lvList.setAdapter(poApprovalDetailAdapter);
                            } else {
                                poDetailAdapter = new PODetailAdapter(getActivity(), arrPOs, false);
                                lvList.setAdapter(poDetailAdapter);
                            }
                            Utils.setListViewHeightBasedOnItems(lvList);
                            tvNumberOfItems.setText(String.valueOf(arrPOs.size()));
                        } else {
                            tvNumberOfItems.setText("0");
                        }

                    }
//                            orderDetailAdapter.notifyDataSetChanged();
                } else if (requestType == PARSER_TYPE.RETURN_REASONS) {
                    if (response instanceof ArrayList) {
                        reasonArray = (ArrayList<ReasonsModel>) response;

                        ReturnRequestFragment fragment = ReturnRequestFragment.newInstance(reasonArray, productId, variantId, poId, isrequestPOStatus);
                        fragment.setClickListener(PODetailFragment.this);
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        fragment.show(ft, "Return Request");
                    }
                } else if (requestType == PARSER_TYPE.CANCEL_REASONS) {
                    if (response instanceof ArrayList) {
                        reasonArray = (ArrayList<ReasonsModel>) response;

                        CancelRequestFragment fragment = CancelRequestFragment.newInstance(reasonArray, productId, variantId, poId, isCancelPOStatus);
                        fragment.setClickListener(PODetailFragment.this);
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        fragment.show(ft, "Cancel Request");

                    }
                } else if (requestType == PARSER_TYPE.UPDATE_PO) {
                    if (response instanceof String) {
                        String string = (String) response;
                        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setMessage(string);
                        dialog.setTitle(getString(R.string.app_name));
                        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                btnAction.setText(getActivity().getResources().getString(R.string.edit));
                                getPODetail(poId);
                                isEdit = false;
                            }
                        });
                        AlertDialog alert = dialog.create();
                        alert.setCanceledOnTouchOutside(false);
                        alert.setCancelable(false);
                        alert.show();

                    }
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), message);
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getActivity().getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

}
