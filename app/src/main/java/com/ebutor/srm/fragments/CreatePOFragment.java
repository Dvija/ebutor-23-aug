package com.ebutor.srm.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.HomeActivity;
import com.ebutor.MyApplication;
import com.ebutor.R;
import com.ebutor.backgroundtask.MultipartEntity;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.ImagePathData;
import com.ebutor.models.POMasterLookUpModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.RuntimePermissionUtils;
import com.ebutor.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CreatePOFragment extends Fragment implements VolleyHandler<Object>, Response.ErrorListener {

    private SharedPreferences mSharedPreferences;
    private RelativeLayout rlAlert;
    private LinearLayout llMain, llPaymentOptions, llPaymentDueDate;
    private Spinner spPaymentType, spPaymentMode, spPaidThrough;
    private EditText etReference, etPaymentDueDate, etLogisticsCost;
    private Button btnCreatePo;
    private Dialog dialog;
    private ArrayList<CustomerTyepModel> arrPaymentTypes, arrPaymentModes, arrPaidThrough;
    private String pocart = "", selPaymentType = "", selPaymentMode = "", selPaidThrough = "", selPaidThroughId = "", logisticsCount = "0";
    private int selYear = 0, selMonth = 0, selDay = 0;
    private String paymentDueDate, paymentDate, picturePath = "", imagePath = null;
    private ImageView ivUpload;
    private ArrayList<ImagePathData> imagePathDataArrayList;
    private int position = 0;

    private HorizontalScrollView imagesScrollView;
    private LinearLayout imageContainer;
    View.OnClickListener onDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getTag() != null) {
                ImagePathData path = (ImagePathData) view.getTag();
                if (imagePathDataArrayList != null && imagePathDataArrayList.contains(path)) {
                    imagePathDataArrayList.remove(path);
                }

                if (imagePathDataArrayList == null || imagePathDataArrayList.size() == 0) {
                    imagesScrollView.setVisibility(View.GONE);
                }
                imageContainer.removeAllViews();
                //
                for (ImagePathData s : imagePathDataArrayList) {
                    imageContainer.addView(setPic(s));
                }
            }
        }
    };

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_po, container, false);
        setHasOptionsMenu(true);
        if (getArguments().containsKey("pocart")) {
            pocart = getArguments().getString("pocart");
        }
        mSharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dialog = Utils.createLoader(getActivity(), ConstantValues.TOOLBAR_PROGRESS);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);

        rlAlert = (RelativeLayout) view.findViewById(R.id.rl_alert);
        llMain = (LinearLayout) view.findViewById(R.id.ll_main);
        llPaymentOptions = (LinearLayout) view.findViewById(R.id.ll_payment_options);
        llPaymentDueDate = (LinearLayout) view.findViewById(R.id.ll_payment_due_date);
        spPaymentType = (Spinner) view.findViewById(R.id.sp_payment_type);
        spPaymentMode = (Spinner) view.findViewById(R.id.sp_payment_mode);
        spPaidThrough = (Spinner) view.findViewById(R.id.sp_paid_through);
        etReference = (EditText) view.findViewById(R.id.et_reference);
        etPaymentDueDate = (EditText) view.findViewById(R.id.et_payment_due_date);
        etLogisticsCost = (EditText) view.findViewById(R.id.et_logistics_cost);
        ivUpload = (ImageView) view.findViewById(R.id.iv_upload);
//        ivImage = (ImageView) view.findViewById(R.id.iv_image);
        btnCreatePo = (Button) view.findViewById(R.id.btn_create_po);
        imagesScrollView = (HorizontalScrollView) view.findViewById(R.id.horizontal_scroll_view);
        imageContainer = (LinearLayout) view.findViewById(R.id.ll_images);

        etLogisticsCost.setSelection(etLogisticsCost.getText().length());

        int year, month, day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH) + 1;

        imagePathDataArrayList = new ArrayList<>();

        paymentDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, day + " " + (month + 1) + " " + year);
        paymentDueDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_TIME_STD_PATTERN, day + " " + (month + 1) + " " + year);
        etPaymentDueDate.setText(paymentDate);

        getData();

        ivUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.READ_EXTERNAL_STORAGE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                    return;
                }

                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                Uri photoURI = Uri.fromFile(photoFile); // create
                if (photoFile != null) {
                    try {
                        imagePath = getPath(photoURI);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI); // set the image file
                    startActivityForResult(intent, 2);
                } else {
                    Toast.makeText(getActivity(), "Unable to create URI", Toast.LENGTH_SHORT).show();
                }*/

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            RuntimePermissionUtils.showRationale(getActivity(),
                                    CreatePOFragment.this,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    9,
                                    "You need to allow access to Gallery");
                        } else {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    9);

                        }
                        return;
                    }

                    Intent intent = new Intent();
                    if (Build.VERSION.SDK_INT >= 19) {
                        // For Android versions of KitKat or later, we use a
                        // different intent to ensure
                        // we can get the file path from the returned intent URI
                        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    } else {
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                    }

                    intent.setType("image/*");
                    startActivityForResult(intent, 2);
                }

            }
        });

        spPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selPaymentType = arrPaymentTypes.get(position).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selPaymentMode = arrPaymentModes.get(position).getCustomerGrpId();
                /*if (selPaymentMode.equalsIgnoreCase("1")) {
                    llPaymentOptions.setVisibility(View.GONE);
                } else {
                    llPaymentOptions.setVisibility(View.VISIBLE);
                }*/
                logisticsCount = TextUtils.isEmpty(etLogisticsCost.getText().toString()) ? "0" : etLogisticsCost.getText().toString();
                if (selPaymentMode.equalsIgnoreCase("1")) {
                    llPaymentDueDate.setVisibility(View.VISIBLE);
                } else {
                    llPaymentDueDate.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spPaidThrough.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selPaidThrough = arrPaidThrough.get(position).getCustomerName() + " === " + arrPaidThrough.get(position).getCustomerGrpName();
                selPaidThroughId = arrPaidThrough.get(position).getCustomerGrpId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etPaymentDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year, month, day;
                final Calendar c = Calendar.getInstance();
                if (selYear == 0 || selMonth == 0 || selDay == 0) {
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH) + 1;
                } else {
                    year = selYear;
                    month = selMonth;
                    day = selDay;
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        selYear = year;
                        selMonth = monthOfYear;
                        selDay = dayOfMonth;

                        paymentDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_STD_PATTERN4, dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                        paymentDueDate = Utils.parseDate(Utils.DATE_STD_PATTERN3, Utils.DATE_TIME_STD_PATTERN, dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                        etPaymentDueDate.setText(paymentDate);
                    }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        btnCreatePo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if (!selPaymentMode.equalsIgnoreCase("1")) {
                    if (TextUtils.isEmpty(selPaidThroughId)) {
                        Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_paid_through));
                        return;
                    }
                    if (TextUtils.isEmpty(selPaymentType)) {
                        Utils.showAlertWithMessage(getActivity(), getString(R.string.please_select_payment_type));
                        return;
                    }
                }*/
                if (selPaymentMode.equalsIgnoreCase("1")) {
                    if (TextUtils.isEmpty(paymentDueDate)) {
                        Utils.showAlertDialog(getActivity(), getResources().getString(R.string.please_select_payment_due_date));
                        return;
                    }
                }

                if (imagePathDataArrayList != null && imagePathDataArrayList.size() > 0) {
                    //we have images to be uploaded, please upload
                    if (Networking.isNetworkAvailable(getActivity())) {
                       /* for (ImagePathData imagePath : imagePathDataArrayList) {
                            beginUpload(imagePath);
                        }*/
                        new UploadImageTask().execute(imagePathDataArrayList.get(position).getImagePath());
                    }

                } else {
                    //todo submit request to server/Local DB
                    createPO();

                }
            }
        });

    }

    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getActivity(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        imagePath = image.getAbsolutePath();
        return image;
    }

    private void createPO() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);

            HashMap<String, String> map = new HashMap<>();
            try {
                String[] arra = new String[]{};
                JSONArray imagesArray = new JSONArray();
                for (int i = 0; i < imagePathDataArrayList.size(); i++) {
                    imagesArray.put(imagePathDataArrayList.get(i).getImageKey());
                }

                JSONObject jsonObject = new JSONObject(pocart);
                jsonObject.put("payment_mode", selPaymentMode);
                jsonObject.put("logistics_cost", logisticsCount);
                jsonObject.put("po_proforma", imagesArray);
                if (selPaymentMode.equalsIgnoreCase("1")) {
                    jsonObject.put("payment_due_date", paymentDueDate);
                }
                /*if (!selPaymentMode.equalsIgnoreCase("1")) {
                    jsonObject.put("payment_type", selPaymentType);
                    jsonObject.put("paid_through", selPaidThrough);
                    jsonObject.put("payment_ref", etReference.getText().toString());
                }*/
                map.put("data", jsonObject.toString());
                Log.e("object", jsonObject.toString());

                VolleyBackgroundTask tabsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.createPOURL, map, CreatePOFragment.this, CreatePOFragment.this, PARSER_TYPE.CREATE_PO);
                tabsRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(tabsRequest, AppURL.createPOURL);
                if (dialog != null)
                    dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    private void getData() {
        if (Networking.isNetworkAvailable(getActivity())) {
            rlAlert.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                map.put("data", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask tabsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPOMasterlookupdataURL, map, CreatePOFragment.this, CreatePOFragment.this, PARSER_TYPE.PO_MASTER_LOOKUP);
            tabsRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(tabsRequest, AppURL.getPOMasterlookupdataURL);
            if (dialog != null)
                dialog.show();

        } else {
            rlAlert.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {

                Uri uri = data.getData();

                try {
                    String path = getPath(uri);

                    ImagePathData imagePathData = new ImagePathData();
                    imagePathData.setImagePath(path);
                    imagePathData.setImageKey(Utils.getAWSFilePath() + ".png");
                    imagePathDataArrayList.add(imagePathData);// Add picked image path to the list


                    imagesScrollView.setVisibility(View.VISIBLE);
                    imageContainer.addView(setPic(imagePathData));

//                beginUpload(path);
                } catch (URISyntaxException e) {
                    Toast.makeText(getActivity(),
                            "Unable to get the file from the given URI.  See error log for details",
                            Toast.LENGTH_LONG).show();
                    Log.e("onActivityResult", "Unable to upload file from the given uri", e);
                }
            }
        }
    }

    private View setPic(ImagePathData imagePath) {
        // Get the dimensions of the View
        int targetW = (int) (100 * Resources.getSystem().getDisplayMetrics().density);
        int targetH = (int) (75 * Resources.getSystem().getDisplayMetrics().density);

        View child = getActivity().getLayoutInflater().inflate(R.layout.image_container_layout, null);
        ImageView mImageView = (ImageView) child.findViewById(R.id.image_preview);
        ImageButton mClose = (ImageButton) child.findViewById(R.id.button_close);
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (bmOptions.outWidth / scale / 2 >= REQUIRED_SIZE
                && bmOptions.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        bmOptions.inSampleSize = scale;
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath.getImagePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath.getImagePath(), bmOptions);
        mImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
        mClose.setTag(imagePath);
        mClose.setOnClickListener(onDeleteClickListener);

        return child;

    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (response != null) {
            try {
                if (status.equalsIgnoreCase("success")) {
                    if (requestType == PARSER_TYPE.PO_MASTER_LOOKUP) {

                        if (response instanceof POMasterLookUpModel) {
                            POMasterLookUpModel poMasterLookUpModel = (POMasterLookUpModel) response;

                            arrPaymentTypes = poMasterLookUpModel.getArrPaymentType();
                            spPaymentType.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrPaymentTypes));
                            spPaymentType.setSelection(0);
                            arrPaymentModes = poMasterLookUpModel.getArrPaymentMode();
                            spPaymentMode.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrPaymentModes));
                            spPaymentMode.setSelection(0);
                            arrPaidThrough = poMasterLookUpModel.getArrPaidThrough();
                            spPaidThrough.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrPaidThrough));
                            spPaidThrough.setSelection(0);

                        }
                    } else if (requestType == PARSER_TYPE.CREATE_PO) {
                        if (response instanceof String) {

                            String string = (String) response;
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                            dialog.setMessage(string);
                            dialog.setTitle(getString(R.string.app_name));
                            dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    DBHelper.getInstance().deletePOCartRow(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""), "", true, "0");

                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            AlertDialog alert = dialog.create();
                            alert.setCanceledOnTouchOutside(false);
                            alert.setCancelable(false);
                            alert.show();

                        }

                    }
                } else {
                    Utils.showAlertWithMessage(getActivity(), message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(getActivity(), message);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(getActivity(), getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {
        }
    }

    public String uploadFile(String filePath) {

        String stringResponse = "";
        String charset = "UTF-8";

        try {

            MultipartEntity multipart = new MultipartEntity(AppURL.uploadImagesURL, charset);

            multipart.addFormField("_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
            multipart.addFormField("entity", "po_proforma");
            multipart.addFilePart("image", new File(filePath));


            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");
            StringBuilder sb = new StringBuilder();
            for (String line : response) {
                System.out.println(line);
                sb.append(line);
            }

            return sb.toString();
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private class UploadImageTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (position == 0)
                progressDialog = ProgressDialog.show(getActivity(), "", "Please wait..!", true);
        }

        @Override
        protected String doInBackground(String... strings) {
            return uploadFile(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s != null) {
                try {
                    JSONObject resultObj = new JSONObject(s);
                    String status = resultObj.optString("status");
                    String message = resultObj.optString("message");
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject dataObj = resultObj.optJSONObject("data");
                        String url = dataObj.optString("url");
                        imagePathDataArrayList.get(position).setImageKey(url);
                        position++;
                        if (position < imagePathDataArrayList.size()) {
                            new UploadImageTask().execute(imagePathDataArrayList.get(position).getImagePath());
                        } else {
                            //upload images are done.. Now upload the expense
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            createPO();

                        }
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
