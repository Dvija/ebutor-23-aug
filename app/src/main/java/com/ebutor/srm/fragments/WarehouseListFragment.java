package com.ebutor.srm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.srm.models.WarehouseModel;

import java.util.ArrayList;

/**
 * Created by santoshbaggam on 13/04/16.
 */
public class WarehouseListFragment extends DialogFragment {
    private OkListener confirmListener;
    private ListView mRecyclerView;

    private ItemsAdapter mAdapter;
    private ArrayList<WarehouseModel> arrWhs;

    public WarehouseListFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static WarehouseListFragment newInstance(ArrayList<WarehouseModel> list) {
        WarehouseListFragment frag = new WarehouseListFragment();
        Bundle args = new Bundle();
        args.putSerializable("WarehouseList", list);
        frag.setArguments(args);
        return frag;
    }

    public void setConfirmListener(OkListener listener) {
        this.confirmListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getActivity().getResources().getString(R.string.whlist));
        return inflater.inflate(R.layout.wish_list_dialog_fragment, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view


        mRecyclerView = (ListView) view.findViewById(R.id.wishListListView);

        arrWhs = (ArrayList<WarehouseModel>) getArguments().getSerializable("WarehouseList");

        mAdapter = new ItemsAdapter(getActivity(), arrWhs);
        mRecyclerView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Toast.makeText(getActivity(), (String) view.getTag(), Fse.LENGTH_LONG).show();
                view.setSelected(true);
                dismiss();
                Object obj = view.getTag();
                if (obj != null && obj instanceof WarehouseModel) {
                    WarehouseModel model = (WarehouseModel) obj;
//                    areasList.get(position).setSelected(true);
//                    MyApplication.getInstance().setCitiesList(areasList);
                    //toggle((CheckedTextView) view);
                    if (confirmListener != null) confirmListener.onOkClicked(model.getWhId());
                }

            }
        });
        /*mAdapter.SetOnItemClickListener(new AreasDialogAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Object obj) {
                Toast.makeText(getActivity(), ((AreaModel)obj).getAreaName(), Toast.LENGTH_LONG).show();
            }
        });*/

        /*btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmListener != null) {
                    confirmListener.onOkClicked();
                }
                dismiss();
            }
        });*/


    }

    public void toggle(CheckedTextView v) {
        if (v.isChecked()) {
            v.setChecked(false);
        } else {
            v.setChecked(true);
        }
    }

    public interface OkListener {
        void onOkClicked(String city);
    }

    private class ItemsAdapter extends BaseAdapter {
        ArrayList<WarehouseModel> items;
        String selected = "";
        private Context context;

        public ItemsAdapter(Context context, ArrayList<WarehouseModel> item) {
            this.context = context;
            this.items = item;
        }

        // @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_wh_list_dialog, null);
            }
            TextView whListName = (TextView) v.findViewById(R.id.tvWhListName);
            whListName.setText(items.get(position).getWhName());


            v.setTag(items.get(position));
            return v;
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }
    }
}
