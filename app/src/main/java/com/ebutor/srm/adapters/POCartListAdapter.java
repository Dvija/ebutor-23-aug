package com.ebutor.srm.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.CartModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class POCartListAdapter extends RecyclerView.Adapter<POCartListAdapter.MyViewHolder> {

    final LayoutInflater inflater;
    private Context context;
    private ArrayList<CartModel> arrCartList;
    private onCartItemDeleteListener listener;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    public POCartListAdapter(Context context, ArrayList<CartModel> arrCartList) {
        this.context = context;
        this.arrCartList = arrCartList;
        inflater = LayoutInflater.from(context);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new SimpleBitmapDisplayer())
                .build();
    }

    public void setListener(onCartItemDeleteListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View v = inflater.inflate(R.layout.row_po_cartlist_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        CartModel model = arrCartList.get(position);
        holder.llProduct.setTag(model);
        holder.llPoCart.setTag(model);

        holder.tvproduct.setText(model.getProductTitle());

        holder.ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v.getTag(), position, 0);
            }
        });
        holder.llPoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onItemClick(v.getTag(), position, 1);
            }
        });
        holder.llProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onItemClick(v.getTag(), position, 1);
            }
        });
        holder.ivCross.setTag(model);
//        holder.tvQty.setText(String.valueOf(model.getQuantity()));
//        holder.tvUOM.setText(model.getRemarks());
        holder.tvQty.setText(String.format(context.getString(R.string.po_cart_qty), model.getQuantity(), model.getRemarks()));
        holder.tvTotalPrice.setText(String.format(Locale.CANADA, "%.2f", model.getTotalPrice()));
        ImageLoader.getInstance().displayImage(model.getProductImage(), holder.ivProduct, options, animateFirstListener);

        if (model.getFreebieProductId() != null && model.getFreebieQty() != null && !model.getFreebieQty().equals("0")) {
            holder.freebieLayout.setVisibility(View.VISIBLE);
            holder.tvFreebieProduct.setText(model.getFreebieTitle());
            holder.tvFreebieQty.setText(model.getFreebieQty());
            holder.tvFreebieTotalPrice.setText("0");
        } else {
            holder.freebieLayout.setVisibility(View.GONE);
            holder.tvFreebieProduct.setText("");
            holder.tvFreebieQty.setText("");
            holder.tvFreebieTotalPrice.setText("0");
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (arrCartList != null)
            return arrCartList.size();
        else
            return 0;
    }

    public void setArrCart(ArrayList<CartModel> arrCartList) {
        this.arrCartList = arrCartList;
        this.notifyDataSetChanged();
    }

    public void clearData() {
        int size = this.arrCartList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.arrCartList.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void deleteItem(int pos) {
        int size = this.arrCartList.size();
        if (size > 0) {
//            for (int i = 0; i < size; i++) {

//            }
            try {

                this.arrCartList.remove(pos);
                this.notifyItemRangeRemoved(pos, size);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public interface onCartItemDeleteListener {
        void onItemClick(Object object, int position, int type);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProduct, ivCross;
        TextView tvproduct, tvTotalPrice, tvQty/*, tvUOM*/;
        TextView tvFreebieProduct, tvFreebieTotalPrice, tvFreebieQty;
        View llProduct, freebieLayout,llPoCart;

        public MyViewHolder(View view) {
            super(view);
            ivProduct = (ImageView) view.findViewById(R.id.ivProduct);
            ivCross = (ImageView) view.findViewById(R.id.ivCross);
            tvproduct = (TextView) view.findViewById(R.id.tvProduct);
            tvTotalPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
            tvQty = (TextView) view.findViewById(R.id.tvQty);
//            tvUOM = (TextView) view.findViewById(R.id.tvUOM);
            llProduct = view.findViewById(R.id.ll_product);
            llPoCart = view.findViewById(R.id.ll_po_cart);
            freebieLayout = view.findViewById(R.id.freebie_layout);

            tvFreebieProduct = (TextView) view.findViewById(R.id.tvFreebie);
            tvFreebieTotalPrice = (TextView) view.findViewById(R.id.tvFreebieTotalPrice);
            tvFreebieQty = (TextView) view.findViewById(R.id.tvFreebieQty);

        }
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
