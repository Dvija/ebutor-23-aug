package com.ebutor.srm.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.customview.SimpleTagImageView;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.interfaces.ProductSKUClickListener;
import com.ebutor.models.TestProductModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class PriceHistoryAdapter extends BaseAdapter {

    public int viewType = 1;
    public ArrayList<TestProductModel> mItemsList = null;
    LayoutInflater inflater;
    Context context;
    boolean isShoppingList = false;
    private ProductSKUClickListener productSKUClickListener;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    private String rating, name;
    private ProductImageClickListener productImageClickListener;
    private ActionSlideExpandableListView actionSlideExpandableListView;

    public PriceHistoryAdapter(Context context, ArrayList<TestProductModel> itemsList, ActionSlideExpandableListView actionSlideExpandableListView) {
        this.mItemsList = itemsList;
        this.context = context;
        this.actionSlideExpandableListView = actionSlideExpandableListView;
        inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();

    }

    @Override
    public int getCount() {
        return mItemsList.size();
    }

    public void setProductSKUClickListener(ProductSKUClickListener productSKUClickListener) {
        this.productSKUClickListener = productSKUClickListener;
    }

    public void setProductImageClickListener(ProductImageClickListener productImageClickListener) {
        this.productImageClickListener = productImageClickListener;
    }

    public void removeAllItems() {
        this.mItemsList.clear();
        notifyDataSetChanged();
    }

    public void addItem(TestProductModel model) {
        if (mItemsList != null && !mItemsList.contains(model)) {
            this.mItemsList.add(model);
        }
        notifyDataSetChanged();
    }

    public void removeItem(TestProductModel model) {
        if (mItemsList != null)
            this.mItemsList.remove(model);
        notifyDataSetChanged();
    }

    public void setIsShoppingList(boolean flag) {
        this.isShoppingList = flag;
    }

    public void setData(ArrayList<TestProductModel> productModelArrayList) {
        this.mItemsList = productModelArrayList;
        notifyDataSetInvalidated();
    }

    @Override
    public Object getItem(int position) {
        return mItemsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        int resId = R.layout.row_price_history_expandable;

//        if (convertView == null) {
        switch (viewType) {
            case 1:
                resId = R.layout.row_price_history_expandable;
                break;
            case 2:
                resId = R.layout.row_products_expandable_new;
                break;
        }
        view = inflater.inflate(resId, parent, false);
        holder = new ViewHolder();
        holder.ivOffer = (Button) view.findViewById(R.id.iv_offer);
        holder.tvProductName = (TextView) view.findViewById(R.id.tvProductName);
        holder.tvMrp = (TextView) view.findViewById(R.id.tvProductMRP);
        holder.tvESP = (TextView) view.findViewById(R.id.tvESP);
        holder.toggleButton = (LinearLayout) view.findViewById(R.id.expandable_toggle_button);
        holder.innerVariantsLayout = (LinearLayout) view.findViewById(R.id.inner_variants);
        holder.expandContentLayout = (LinearLayout) view.findViewById(R.id.ll_expand_content);
        holder.skuItemsLayout = (LinearLayout) view.findViewById(R.id.skuItemsLayout);
        holder.draweeView = (SimpleTagImageView) view.findViewById(R.id.productImage);
        holder.tvRating = (TextView) view.findViewById(R.id.tvRating);
        holder.ivWishlist = (ImageView) view.findViewById(R.id.ivWishlist);
        view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }

        final TestProductModel productModel = mItemsList.get(position);
        setOfferImageBackground(productModel.getPackType(), holder);
//        if(position%2 == 0)
//            setOfferImageBackground(productModel.getPackType(),holder);
//        else
//            setOfferImageBackground("CPOFFER", holder);
        holder.tvProductName.setText(productModel.getProductTitle().trim());
        ImageLoader.getInstance().displayImage(productModel.getPrimaryImage(), holder.draweeView, options, animateFirstListener);
        holder.draweeView.setTagBackgroundColor(Color.TRANSPARENT);
        holder.draweeView.setTagText("");
        double mrp = 0;
        try {
            mrp = Double.parseDouble(productModel.getMrp());
        } catch (Exception e) {
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(productModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
        setMRP(sb, holder.tvMrp);
        if (isShoppingList) {
            holder.ivWishlist.setImageResource(R.drawable.ic_delete);
            holder.ivWishlist.setVisibility(View.VISIBLE);
        } else {
            holder.ivWishlist.setVisibility(View.GONE);
            holder.tvRating.setVisibility(View.GONE);
//            holder.ivWishlist.setImageResource(R.drawable.ic_wishlist_gray);
        }
//        rating = productModel.getProductRating();
        rating = "0.0"; //temp
        if (rating == null || TextUtils.isEmpty(rating)) {
            rating = "0.0";
        }
        if (rating != null && !TextUtils.isEmpty(rating) && !rating.equalsIgnoreCase("null")) {
            DecimalFormat format = new DecimalFormat("0.0");
            double newmargin = Double.parseDouble(rating);
            rating = format.format(newmargin);
        }

        holder.tvRating.setText(rating.equalsIgnoreCase("null") ? "0.0" : rating);

        holder.ivWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productSKUClickListener != null) {
                    productSKUClickListener.OnItemOptionClicked(productModel, 1);
                }
            }
        });

        holder.draweeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productImageClickListener.OnImageClicked(position, rating);
            }
        });

        holder.tvProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productImageClickListener.OnImageClicked(position, rating);
            }
        });

//        holder.setTag();

        holder.skuItemsLayout.removeAllViews();
        if (productModel != null && productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
            for (int j = 0; j < productModel.getVariantModelArrayList().size(); j++) {
//            final NewVariantModel skuModel = productModel.getVariantModelArrayList().get(j);
                final TextView textView = new TextView(context);
                if (productModel.getVariantModelArrayList().get(j).split(":").length > 0)
                    name = productModel.getVariantModelArrayList().get(j).split(":")[0];
                DisplayMetrics displaymetrics = new DisplayMetrics();
                WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                windowManager.getDefaultDisplay().getMetrics(displaymetrics);
                int height = displaymetrics.heightPixels;
                textView.setTextSize(12);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
                textView.setBackground(ContextCompat.getDrawable(context, R.drawable.sku_button_selector));
                textView.setTextColor(ContextCompat.getColor(context, R.color.sku_button_color_selector));
                params.setMargins(0, 0, 20, 0);
                textView.setPadding(20, 5, 20, 5);
                textView.setGravity(Gravity.CENTER);
                textView.setLayoutParams(params);
                textView.setId(R.id.id_one);
                textView.setTag(name);
                textView.setText(name == null ? "" : name);
                if (productModel.getVariantValue1().equalsIgnoreCase(name)) {
                    textView.setSelected(true);
                    productModel.setSelectedVariant(name);
//                skuModel.setSelected(true);
//                mrp = 0;
//                try {
//                    mrp = Double.parseDouble(skuModel.getMrp());
//                } catch (Exception e) {
//                }
//                final SpannableStringBuilder sb = new SpannableStringBuilder(skuModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
//            final SpannableStringBuilder sb = new SpannableStringBuilder("MRP : " + String.format(Locale.CANADA, "%.2f", mrp)); // todo
//                setMRP(sb, holder);
                } else {
                    textView.setSelected(false);
                }
//
//
//            String availableQty = skuModel.getAvailableQuantity();
////            String availableQty = "100"; // todo
//            if (availableQty != null && !TextUtils.isEmpty(availableQty)) {
//                int qty = 0;
//                try {
//                    qty = Integer.parseInt(availableQty);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (qty > 0) {
//                    holder.draweeView.setTagBackgroundColor(Color.TRANSPARENT);
//                    holder.draweeView.setTagText("");
//                } else {
//                    holder.draweeView.setTagBackgroundColor(Color.parseColor("#afd3d3d3"));
//                    holder.draweeView.setTagText("Out of Stock");
//                }
//
//            }
////            }
                //
//            final int finalJ = j;
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int count = holder.skuItemsLayout.getChildCount();
                        for (int i = 0; i < count; i++) {
                            View btnView = holder.skuItemsLayout.getChildAt(i);
                            btnView.setSelected(false);
                        }
                        v.setSelected(true);
                        TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(productModel.getProductId(), name, "", "");
                        if (testProductModel != null) {
                            productModel.setSelectedVariant((String) textView.getTag());
                            setOfferImageBackground(testProductModel.getPackType(), holder);
                            double mrp = 0;
                            try {
                                mrp = Double.parseDouble(testProductModel.getMrp());
                            } catch (Exception e) {
                            }
                            final SpannableStringBuilder sb = new SpannableStringBuilder(productModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
                            setMRP(sb, holder.tvMrp);
                            ImageLoader.getInstance().displayImage(testProductModel.getPrimaryImage(), holder.draweeView, options, animateFirstListener);
                            holder.tvProductName.setText(testProductModel.getProductTitle().trim());
                        }
                        if (productSKUClickListener != null) {
                            productSKUClickListener.OnProductSKUClicked(textView.getTag(), getItemId(position), false);
                        }
//                    if (productModel.isClicked()) {
//                        skuModel.setSelected(skuModel.isSelected());
//                        if (productSKUClickListener != null) {
//                            productSKUClickListener.OnProductSKUClicked(skuModel, getItemId(position));
//                        }
//                    }
                    }
                });
//
////            if (productModel.isClicked()) {
////                skuModel.setSelected(skuModel.isSelected());
////                actionSlideExpandableListView.enableExpandOnItemClick();
////                if (productSKUClickListener != null) {
////                    productSKUClickListener.OnProductSKUClicked(skuModel, getItemId(position));
////                }
////            }
//
                holder.skuItemsLayout.addView(textView);
//
            }
        }
//        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {
//            holder.tvProductName.setText(productModel.getVariantModelArrayList().get(0).getProductName());
//            ImageLoader.getInstance().displayImage(productModel.getVariantModelArrayList().get(0).getImage(), holder.draweeView, options, animateFirstListener);
//        }


        /*if (model.getSkuModelArrayList() != null && model.getSkuModelArrayList().size() > 0) {
            final SKUModel skuModel = model.getSkuModelArrayList().get(0);
            if (skuModel.getSkuPacksModelArrayList() != null && skuModel.getSkuPacksModelArrayList().size() > 0) {
                SKUPacksModel skuPacksModel = skuModel.getSkuPacksModelArrayList().get(0);
                double mrp = 0;
                try {
                    mrp = Double.parseDouble(skuModel.getMrp());
                } catch (Exception e) {
                }
                final SpannableStringBuilder sb = new SpannableStringBuilder(skuModel.getMrp() == null ? "" : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));

                setMRP(sb, holder);
            }
        }*/

//        actionSlideExpandableListView.collapse();

        view.setTag(holder);
        return view;
    }

    public void setOfferImageBackground(String packType, ViewHolder holder) {
        switch (packType) {
            case "Regular":
            case "REGULAR":
                holder.ivOffer.setVisibility(View.GONE);
                break;
            case "Consumer Pack":
            case "CONSUMER PACK":
            case "CP Inside":
            case "CP Outside":
            case "Consumer Pack Inside":
            case "Consumer Pack Outside":
                holder.ivOffer.setVisibility(View.VISIBLE);
                holder.ivOffer.setBackgroundResource(R.drawable.ic_offer_green);
                holder.ivOffer.setText("CP Offer");
                break;
            case "Freebie":
            case "FREEBIE":
                holder.ivOffer.setVisibility(View.VISIBLE);
                holder.ivOffer.setBackgroundResource(R.drawable.ic_offer_red);
                holder.ivOffer.setText("Freebie");
                break;
            default:
                holder.ivOffer.setVisibility(View.GONE);
                break;
        }
    }

    public void notifyChangedItem(int pos, TestProductModel testProductModel) {
        mItemsList.set(pos, testProductModel);
        notifyDataSetChanged();
    }

    private void setMRP(SpannableStringBuilder sb, TextView textView) {
        /*// Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));*/
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        /*// Set the text color for first 4 characters
        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 0, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);*/
        // Create the Typeface you want to apply to certain text
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, 3, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(sb);
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return viewType;
    }

    public void changeViewType(int type) {
        viewType = type;
        notifyDataSetChanged();
    }

    public interface ProductImageClickListener {
        void OnImageClicked(int position, String rating);
    }

    private class ViewHolder {
        public TextView tvProductName, tvRating;
        public LinearLayout toggleButton, expandContentLayout, skuItemsLayout;
        private TextView tvMrp, tvESP;
        private ImageView ivWishlist;
        private Button ivOffer;
        private SimpleTagImageView draweeView;
        private LinearLayout innerVariantsLayout;
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}