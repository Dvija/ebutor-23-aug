package com.ebutor.srm.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ebutor.models.SearchModel;
import com.ebutor.utils.AppURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Based on https://developers.google.com/places/training/autocomplete-android
public class PlacesAdapter extends BaseAdapter implements Filterable {
    public static String whId = "", hubId = "0";
    private static String LOG_TAG = "PlacesAdapter";
    private final LayoutInflater mInflater;
    private final int mResource;
    private List<SearchModel> mResultList;

    public PlacesAdapter(Context context, int resource) {
        super();
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
        mResultList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mResultList.size();
    }

    @Override
    public SearchModel getItem(int index) {
        return mResultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(position, convertView, parent, mResource);
    }

    private View createViewFromResource(int position, View convertView, ViewGroup parent,
                                        int resource) {
        View view;

        if (convertView == null) {
            view = mInflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }

        String item = getItem(position).getName();
        ((TextView) view).setText(item);
        view.setTag(getItem(position).getProductId());
        return view;
    }


    private ArrayList<SearchModel> lookupSuggestions(String input) {
        ArrayList<SearchModel> resultList = new ArrayList<>();
        Uri.Builder builder = Uri.parse(AppURL.searchURL).buildUpon();

        HttpURLConnection conn = null;
        String response = "";
        try {
            URL url = new URL(AppURL.searchURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(9000000);
            conn.setConnectTimeout(9000000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(input);

            writer.flush();
            writer.close();
            os.close();
//            conn.setRequestProperty("Referer", "http://android-app.dancedeets.com/");
//            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }

        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(response);
//            if (jsonObj.optString("error_message", null) != null) {
//                Log.e(LOG_TAG, "Autocomplete Error (likely no results): " + jsonResults.toString());
//            }
            String status = jsonObj.optString("status");
            String message = jsonObj.optString("message");
            if (null != status && status.equalsIgnoreCase("success")) {
                JSONArray predsJsonArray = jsonObj.getJSONArray("data");

                // Extract the Place descriptions from the results
                resultList = new ArrayList<>(predsJsonArray.length());
                for (int i = 0; i < predsJsonArray.length(); i++) {
                    JSONObject object = predsJsonArray.optJSONObject(i);
                    String name = object.optString("name");
                    String product_id = object.optString("product_id");
                    SearchModel model = new SearchModel();
                    model.setName(name);
                    model.setProductId(product_id);
                    resultList.add(model);
                }
            }

        } catch (JSONException e) {

        }
        return resultList;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @Override
    public Filter getFilter() {
        // This Filter runs in another thread, which allows us to do long-running operations
        // to fetch the filtered results. (In this case, by loading from Google Places API).
        // But this background thread cannot touch mResultList, but must instead communicate
        // via returning from performFiltering to pass to publishResults.
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the Places suggestions.
                    // Don't modify mResultList, let publishResults do that in the UI thread.
                    List<SearchModel> resultList = new ArrayList<>();
                    try {

                        JSONObject object = new JSONObject();
                        object.put("keyword", constraint.toString());
                        object.put("le_wh_id", whId);
                        object.put("flag", "1");
                        object.put("hub_id", hubId);
                        HashMap<String, String> map = new HashMap<>();
                        map.put("data", object.toString());
                        resultList = lookupSuggestions(getPostDataString(map));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                } else {
                    // Don't allow null lists to leak out to publishResults when it sets mResultList,
                    // or it will cause a crash later when getCount is called.
                    filterResults.values = new ArrayList<>();
                    filterResults.count = 0;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                //noinspection unchecked
                mResultList = (List<SearchModel>) results.values;
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }
}