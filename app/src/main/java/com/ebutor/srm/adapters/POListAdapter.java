package com.ebutor.srm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.OrdersModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class POListAdapter extends RecyclerView.Adapter<POListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<OrdersModel> arrPOs = new ArrayList<>();
    private OnItemClickListener listener;

    public POListAdapter(Context context, ArrayList<OrdersModel> arrPOs) {
        this.context = context;
        this.arrPOs = arrPOs;
    }

    public void setClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.po_list_single_item, parent, false);
        v.setTag(new MyViewHolder(v));
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvSupplierName.setText(arrPOs.get(position).getSupplierName());
        holder.tvPOId.setText(arrPOs.get(position).getOrderCode());
        String date = parseDate(arrPOs.get(position).getDate(), "dd-MM-yyyy");
        holder.tvDate.setText(context.getResources().getString(R.string.date) + date);
        holder.tvStatus.setText(context.getResources().getString(R.string.status) + " : " + arrPOs.get(position).getOrderStatus());
        holder.tvApprovalStatus.setText(context.getResources().getString(R.string.approval_status) + " : " + arrPOs.get(position).getPoApprovalStatus());

        switch (arrPOs.get(position).getOrderStatus()) {
            case "OPEN ORDER":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.pending));
                break;
            case "PROCESSING":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "CONFIRMED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processed));
                break;
            case "PICKED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.cancel));
                break;
            case "PACKED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.shipping));
                break;
            case "HOLD":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.return_requested));
                break;
            case "READY TO DISPATCH":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.complete));
                break;
            case "SHIPPED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.complete));
                break;
            case "DELIVERED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.shipping));
                break;
            case "CANCELLED BY EBUTOR":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.cancel));
                break;
            case "CANCELLED BY CUSTOMER":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "RETURN INITIATED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "RETURNED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "REFUND INITIATED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "REFUNDED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            case "CLOSED":
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.processing));
                break;
            default:
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.pending));
                break;

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrPOs.size();
    }

    public String parseDate(String date, String format) {

        String newDate = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(format);
        try {
            newDate = output.format(input.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvSupplierName, tvPOId, tvDate, tvStatus, tvApprovalStatus;

        public MyViewHolder(View view) {
            super(view);
            tvSupplierName = (TextView) view.findViewById(R.id.tv_supplier_name);
            tvPOId = (TextView) view.findViewById(R.id.tv_poId);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvApprovalStatus = (TextView) view.findViewById(R.id.tv_approval_status);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(this.getPosition());
        }
    }
}
