package com.ebutor.srm.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ebutor.R;
import com.ebutor.models.NewPacksModel;
import com.ebutor.srm.models.POModel;
import com.ebutor.utils.EditTextWithDeleteButton;

import java.util.ArrayList;
import java.util.Locale;

public class PODetailAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<POModel> arrPOs = null;
    private boolean isEdit = false;
    private boolean isFlag = true; // to call the method in the text watcher

    public PODetailAdapter(Context context, ArrayList<POModel> arrPOs, boolean isEdit) {
        this.context = context;
        this.arrPOs = arrPOs;
        this.isEdit = isEdit;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return arrPOs.size();
    }

    @Override
    public Object getItem(int position) {
        return arrPOs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = inflater.inflate(R.layout.row_po_details, parent, false);
            holder = new ViewHolder();

            holder.tvSKU = (TextView) view.findViewById(R.id.tvSku);
            holder.tvMrp = (TextView) view.findViewById(R.id.tvMrp);
            holder.tvProduct = (TextView) view.findViewById(R.id.tvProduct);
            holder.etQty = (EditText) view.findViewById(R.id.etQty);
            holder.etFreebieQty = (EditText) view.findViewById(R.id.etFreebieQty);
//            holder.tvUOM = (TextView) view.findViewById(R.id.tvUOM);
//            holder.tvFreeUOM = (TextView) view.findViewById(R.id.tvFreeUOM);
            holder.tvUnitPrice = (TextView) view.findViewById(R.id.tvUnitPrice);
            holder.tvPrice = (TextView) view.findViewById(R.id.tvPrice);
            holder.tvTotalPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
            holder.tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            holder.tvDiscountOnBill = (TextView) view.findViewById(R.id.tv_discount_on_bill);
            holder.spUOM = (Spinner) view.findViewById(R.id.sp_uom);
            holder.spFreeUOM = (Spinner) view.findViewById(R.id.sp_free_uom);
            holder.tvSlp = (TextView) view.findViewById(R.id.tv_slp);
            holder.tvStd = (TextView) view.findViewById(R.id.tv_std);
            holder.tvThirtyD = (TextView) view.findViewById(R.id.tv_thirtyd);
            holder.tvAvailInv = (TextView) view.findViewById(R.id.tv_avail_inv);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final POModel model = arrPOs.get(position);
        int selUOM = 0, selFreeUOM = 0;
        holder.currentPos = 0;
        holder.prevPos = 0;
        holder.currentFreePos = 0;
        holder.prevFreePos = 0;
        holder.tvSKU.setText(context.getResources().getString(R.string.sku) + ":" + model.getSkuCode());
        holder.tvMrp.setText(context.getResources().getString(R.string.mrp) + ":" + model.getMrp());
        holder.tvProduct.setText(model.getProductName());
        holder.tvUnitPrice.setText(context.getResources().getString(R.string.unit_price) + ": " + model.getUnitPrice());
        holder.tvDiscountOnBill.setText(context.getResources().getString(R.string.apply) + ": " + model.getApplyDiscount() + ", " + context.getResources().getString(R.string.value) + ": " + model.getDiscount() + " " + model.getDiscountType());
        holder.tvPrice.setText(context.getResources().getString(R.string.price) + ": " + model.getPrice());

        holder.tvSlp.setText(context.getResources().getString(R.string.slp) + " : " + model.getSlp());
        holder.tvStd.setText(context.getResources().getString(R.string.std) + " : " + model.getStd());
        holder.tvThirtyD.setText(context.getResources().getString(R.string.thirtyd) + " : " + model.getThirtyd());
        holder.tvAvailInv.setText(context.getResources().getString(R.string.avail_inv) + " : " + model.getAvailableInventory());

        holder.etQty.setText(model.getQuantity());
        holder.etFreebieQty.setText(model.getFreeQty());
        holder.spUOM.setAdapter(new PackTypesSpinnerAdapter(context, model.getArrUOMs()));
        holder.spFreeUOM.setAdapter(new PackTypesSpinnerAdapter(context, model.getArrUOMs()));
        if (model.getArrUOMs() != null && model.getArrUOMs().size() > 0) {
            for (int i = 0; i < model.getArrUOMs().size(); i++) {
                if (model.getPackId().equalsIgnoreCase(model.getArrUOMs().get(i).getProductPackId())) {
                    selUOM = i;
                }
                if (model.getFreePackId().equalsIgnoreCase(model.getArrUOMs().get(i).getProductPackId())) {
                    selFreeUOM = i;
                }
            }
        }

        holder.spUOM.setSelection(selUOM);
        holder.spFreeUOM.setSelection(selFreeUOM);
//        holder.tvUOM.setText(" " + model.getUom());
//        holder.tvFreeUOM.setText(" " + model.getFreeUOM());
        holder.currentQty = model.getQuantity();
        holder.currentFreeQty = model.getFreeQty();

        if (isEdit) {
            holder.etQty.setInputType(InputType.TYPE_CLASS_NUMBER);
            holder.etQty.setEnabled(true);
            holder.etQty.setFocusable(true);
            if (position == 0) {
                holder.etQty.requestFocus();
            }
            holder.etQty.setClickable(true);
            holder.etQty.setFocusableInTouchMode(true);
            holder.etQty.setCursorVisible(true);
            holder.etQty.setSelection(holder.etQty.getText().length());

            holder.etFreebieQty.setInputType(InputType.TYPE_CLASS_NUMBER);
            holder.etFreebieQty.setEnabled(true);
            holder.etFreebieQty.setFocusable(true);
            holder.etFreebieQty.setClickable(true);
            holder.etFreebieQty.setFocusableInTouchMode(true);
            holder.etFreebieQty.setCursorVisible(true);
            holder.etFreebieQty.setSelection(holder.etFreebieQty.getText().length());

            holder.spUOM.setEnabled(true);
            holder.spFreeUOM.setEnabled(true);
        } else {
            holder.etQty.setEnabled(false);
            holder.etFreebieQty.setEnabled(false);
            holder.spUOM.setEnabled(false);
            holder.spFreeUOM.setEnabled(false);
        }

        holder.spUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (view.getTag() != null && view.getTag() instanceof NewPacksModel) {
                    holder.prevPos = holder.currentPos;
                    holder.currentPos = pos;
                    if (!isFlag) {
                        isFlag = true;
                        return;
                    }
                    holder.selectedPackModelPrev = holder.selectedPackModel;
                    holder.selectedPackModel = (NewPacksModel) view.getTag();

                    int selectedFreePos = holder.spFreeUOM.getSelectedItemPosition();

//                    int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, holder.etFreebieQty.getText().toString());
                    int size = model.getArrUOMs().get(selectedFreePos).getPackSize() == null ? 0 : Integer.parseInt(model.getArrUOMs().get(selectedFreePos).getPackSize());
                    int enteredQuantity = TextUtils.isEmpty(holder.etFreebieQty.getText().toString()) ? 0 : Integer.parseInt(holder.etFreebieQty.getText().toString());
                    int freeQuantityInEaches = size * enteredQuantity;
                    double unitPrice = 0.0;
                    int packSize = 0;
                    try {
                        unitPrice = Double.parseDouble(model.getUnitPrice());
                        packSize = Integer.parseInt(model.getArrUOMs().get(pos).getPackSize());
                    } catch (Exception e) {
                        unitPrice = 0.0;
                        packSize = 0;
                    }

                    int mainProductQtyInEaches = getProductsInEaches(holder.selectedPackModel, holder.etQty.getText().toString());

                    if (mainProductQtyInEaches == 0) {
                        model.setNoOfEaches(model.getArrUOMs().get(pos).getPackSize());
                        model.setPackId(model.getArrUOMs().get(pos).getProductPackId());
                        model.setPackSizeValue(model.getArrUOMs().get(pos).getLevel());
                        holder.tvPrice.setText(context.getResources().getString(R.string.price) + ": " + String.valueOf(unitPrice * packSize));
                        return;
                    }
                    if (freeQuantityInEaches > 0 && mainProductQtyInEaches <= freeQuantityInEaches) {
                        Toast.makeText(context, context.getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                        isFlag = false;
                        holder.spUOM.setSelection(holder.prevPos);
                        holder.selectedPackModel = holder.selectedPackModelPrev;
                        return;
                    }
                    int finalQty = mainProductQtyInEaches - freeQuantityInEaches;

                    model.setNoOfEaches(model.getArrUOMs().get(pos).getPackSize());
                    model.setPackId(model.getArrUOMs().get(pos).getProductPackId());
                    model.setPackSizeValue(model.getArrUOMs().get(pos).getLevel());
                    model.setProductTotal(String.valueOf(unitPrice * finalQty));
                    holder.tvPrice.setText(context.getResources().getString(R.string.price) + ": " + String.format(Locale.CANADA, "%.2f", (unitPrice * packSize)));
                    holder.tvTotalPrice.setText(context.getString(R.string.total_price) + ": " + String.format(Locale.CANADA, "%.2f", (unitPrice * finalQty)));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.spFreeUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                if (view.getTag() != null && view.getTag() instanceof NewPacksModel) {
                    holder.prevFreePos = holder.currentFreePos;
                    holder.currentFreePos = pos;
                    if (!isFlag) {
                        isFlag = true;
                        return;
                    }
                    holder.selectedPackModelPrev = holder.selectedFreePackModel;
                    holder.selectedFreePackModel = (NewPacksModel) view.getTag();

                    int freeQuantityInEaches = getProductsInEaches(holder.selectedFreePackModel, holder.etFreebieQty.getText().toString());

                    int selectedMainPos = holder.spUOM.getSelectedItemPosition();

//                    int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, holder.etFreebieQty.getText().toString());
                    int size = model.getArrUOMs().get(selectedMainPos).getPackSize() == null ? 0 : Integer.parseInt(model.getArrUOMs().get(selectedMainPos).getPackSize());
                    int enteredQuantity = TextUtils.isEmpty(holder.etQty.getText().toString()) ? 0 : Integer.parseInt(holder.etQty.getText().toString());
                    int mainProductQtyInEaches = size * enteredQuantity;
//                    int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, holder.etQty.getText().toString());
                    if (freeQuantityInEaches > 0 && mainProductQtyInEaches <= freeQuantityInEaches) {
                        Toast.makeText(context, context.getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                        isFlag = false;
                        holder.spFreeUOM.setSelection(holder.prevFreePos);
                        holder.selectedFreePackModel = holder.selectedFreePackModelPrev;
                        return;
                    }
                    double unitPrice = 0.0;
                    int finalQty = mainProductQtyInEaches - freeQuantityInEaches;

                    try {
                        unitPrice = Double.parseDouble(model.getUnitPrice());
                    } catch (Exception e) {
                        unitPrice = 0.0;
                    }
                    model.setFreeEaches(model.getArrUOMs().get(pos).getPackSize());
                    model.setFreePackId(model.getArrUOMs().get(pos).getProductPackId());
                    model.setFreePackSizeValue(model.getArrUOMs().get(pos).getLevel());
                    model.setProductTotal(String.valueOf(unitPrice * finalQty));
                    holder.tvTotalPrice.setText(context.getString(R.string.total_price) + ": " + String.valueOf(unitPrice * finalQty));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.etQty.addTextChangedListener(new EditTextWithDeleteButton.TextChangedListener() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredProductQty = s.toString();
                holder.prevQty = holder.currentQty;
                holder.currentQty = enteredProductQty;
                if (!isFlag) {
                    isFlag = true;
                    return;
                }
                model.setQuantity(enteredProductQty);
                if (!TextUtils.isEmpty(enteredProductQty)) {

                    String noOfEaches = model.getNoOfEaches();

                    String freeQty = model.getFreeQty();
                    String noOfFreeEaches = model.getFreeEaches();

                    int totalProductQty = 0;
                    int totalFreeQty = 0;
                    try {
                        totalProductQty = Integer.parseInt(enteredProductQty) * Integer.parseInt(noOfEaches);
                        totalFreeQty = Integer.parseInt(freeQty) * Integer.parseInt(noOfFreeEaches);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    double unitPrice = 0.0;
                    try {
                        unitPrice = Double.parseDouble(model.getUnitPrice());
                    } catch (Exception e) {
                        unitPrice = 0.0;
                    }

                    if (totalProductQty > totalFreeQty) {
                        isFlag = true;


                    } else {
                        isFlag = false;
//                        holder.etQty.setText(holder.prevQty);
//                        holder.etQty.setSelection(holder.etQty.getText().toString().length());
                        holder.etFreebieQty.setText("0");
                        model.setFreeQty("0");
                        totalFreeQty = 0;
                        Toast.makeText(context, context.getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                    }

                    int finalQty = totalProductQty - totalFreeQty;
                    model.setProductTotal(String.valueOf(unitPrice * finalQty));
                    holder.tvTotalPrice.setText(context.getString(R.string.total_price) + ": " + String.valueOf(unitPrice * finalQty));

                } else {
                    model.setProductTotal("0.00");
                    holder.tvTotalPrice.setText(context.getString(R.string.total_price) + ": 0.00");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.etFreebieQty.addTextChangedListener(new EditTextWithDeleteButton.TextChangedListener() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String enteredFreeQty = s.toString();
                holder.prevFreeQty = holder.currentFreeQty;
                holder.currentFreeQty = enteredFreeQty;
                if (!isFlag) {
                    isFlag = true;
                    return;
                }

                String productQty = model.getQuantity();
                String noOfEaches = model.getNoOfEaches();

                String noOfFreeEaches = model.getFreeEaches();

                int totalProductQty = 0;
                int totalFreeQty = 0;
                try {
                    totalProductQty = Integer.parseInt(productQty) * Integer.parseInt(noOfEaches);
                    totalFreeQty = Integer.parseInt(enteredFreeQty) * Integer.parseInt(noOfFreeEaches);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ((totalProductQty == 0 && totalFreeQty == 0) || totalProductQty > totalFreeQty) {
                    isFlag = true;
                    double unitPrice = 0.0;
                    try {
                        unitPrice = Double.parseDouble(model.getUnitPrice());
                    } catch (Exception e) {
                        unitPrice = 0.0;
                    }
                    model.setFreeQty(TextUtils.isEmpty(enteredFreeQty) ? "0" : enteredFreeQty);

                    int finalQty = totalProductQty - totalFreeQty;
                    model.setProductTotal(String.valueOf(unitPrice * finalQty));
                    holder.tvTotalPrice.setText(context.getString(R.string.total_price) + ": " + String.valueOf(unitPrice * finalQty));
                } else {
                    isFlag = false;
                    holder.etFreebieQty.setText(holder.prevFreeQty);
                    holder.etFreebieQty.setSelection(holder.etFreebieQty.getText().toString().length());
                    Toast.makeText(context, context.getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        try {
            String totalPrice = model.getSubTotal();
            double value = Double.parseDouble(totalPrice);
            model.setProductTotal(totalPrice);
            holder.tvTotalPrice.setText(context.getResources().getString(R.string.total_price) + ": " + String.format(Locale.CANADA, "%.2f", value));
        } catch (Exception e) {
            e.printStackTrace();
            model.setProductTotal("0.00");
            holder.tvTotalPrice.setText(context.getResources().getString(R.string.total_price) + ": " + "0.00");
        }

        return view;
    }

    private int getProductsInEaches(NewPacksModel selectedPackModel, String quantityEntered) {

        if (selectedPackModel != null) {
            int packSize = selectedPackModel.getPackSize() == null ? 0 : Integer.parseInt(selectedPackModel.getPackSize());
            int enteredQuantity = TextUtils.isEmpty(quantityEntered) ? 0 : Integer.parseInt(quantityEntered);

            return packSize * enteredQuantity;
        } else {
            return 0;
        }
    }

    private class ViewHolder {
        TextView tvSKU, tvMrp, tvProduct, tvUnitPrice, tvPrice, tvTotalPrice, tvStatus/*, tvUOM, tvFreeUOM*/, tvDiscountOnBill,tvSlp, tvStd, tvThirtyD, tvAvailInv;
        EditText etFreebieQty, etQty;
        Spinner spUOM, spFreeUOM;
        String currentQty = "", prevQty = "", currentFreeQty = "", prevFreeQty = "";
        int currentPos = 0, prevPos = 0, currentFreePos = 0, prevFreePos = 0;
        NewPacksModel selectedFreePackModelPrev, selectedFreePackModel, selectedPackModel, selectedPackModelPrev;
    }

}
