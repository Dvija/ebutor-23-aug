package com.ebutor.srm.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.NewPacksModel;
import com.ebutor.srm.models.POModel;

import java.util.ArrayList;
import java.util.Locale;

public class POApprovalDetailAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<POModel> arrPOs = null;

    public POApprovalDetailAdapter(Context context, ArrayList<POModel> arrPOs, boolean isEdit) {
        this.context = context;
        this.arrPOs = arrPOs;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return arrPOs.size();
    }

    @Override
    public Object getItem(int position) {
        return arrPOs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = inflater.inflate(R.layout.row_po_approval_details, parent, false);
            holder = new ViewHolder();

            holder.tvSKU = (TextView) view.findViewById(R.id.tvSku);
            holder.tvMrp = (TextView) view.findViewById(R.id.tvMrp);
            holder.tvProduct = (TextView) view.findViewById(R.id.tvProduct);
            holder.tvQty = (TextView) view.findViewById(R.id.tvQty);
            holder.tvFreebieQty = (TextView) view.findViewById(R.id.tvFreebieQty);
            holder.tvUnitPrice = (TextView) view.findViewById(R.id.tvUnitPrice);
            holder.tvPrice = (TextView) view.findViewById(R.id.tvPrice);
            holder.tvTotalPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
            holder.tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            holder.tvDiscountOnBill = (TextView) view.findViewById(R.id.tv_discount_on_bill);
            holder.tvSlp = (TextView) view.findViewById(R.id.tv_slp);
            holder.tvStd = (TextView) view.findViewById(R.id.tv_std);
            holder.tvThirtyD = (TextView) view.findViewById(R.id.tv_thirtyd);
            holder.tvAvailInv = (TextView) view.findViewById(R.id.tv_avail_inv);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final POModel model = arrPOs.get(position);
        holder.tvSKU.setText(context.getResources().getString(R.string.sku) + " : " + model.getSkuCode());
        holder.tvMrp.setText(context.getResources().getString(R.string.mrp) + " : " + model.getMrp());
        holder.tvProduct.setText(model.getProductName());
        holder.tvUnitPrice.setText(context.getResources().getString(R.string.unit_price) + " : " + model.getUnitPrice());
        holder.tvPrice.setText(context.getResources().getString(R.string.price) + " : " + model.getPrice());

        holder.tvDiscountOnBill.setText(context.getResources().getString(R.string.apply) + " : " + model.getApplyDiscount() + ", " + context.getResources().getString(R.string.value) + " : " + model.getDiscount() + " " + model.getDiscountType());
        holder.tvSlp.setText(context.getResources().getString(R.string.slp) + " : " + model.getSlp());
        holder.tvStd.setText(context.getResources().getString(R.string.std) + " : " + model.getStd());
        holder.tvThirtyD.setText(context.getResources().getString(R.string.thirtyd) + " : " + model.getThirtyd());
        holder.tvAvailInv.setText(context.getResources().getString(R.string.avail_inv) + " : " + model.getAvailableInventory());

        String packSize = "", freePackSize = "";
        if (model.getArrUOMs() != null && model.getArrUOMs().size() > 0) {
            for (int i = 0; i < model.getArrUOMs().size(); i++) {
                NewPacksModel UOMModel = model.getArrUOMs().get(i);
                if (model.getPackId().equalsIgnoreCase(UOMModel.getProductPackId())) {
                    packSize = UOMModel.getPackSize();
                }
                if (model.getFreePackId().equalsIgnoreCase(UOMModel.getProductPackId())) {
                    freePackSize = UOMModel.getPackSize();
                }
            }
        }

        holder.tvQty.setText(model.getQuantity() + " " + model.getUom() + "(" + packSize + ")");
        if (!TextUtils.isEmpty(freePackSize))
            holder.tvFreebieQty.setText(model.getFreeQty() + " " + model.getFreeUOM() + "(" + freePackSize + ")");
        else
            holder.tvFreebieQty.setText(model.getFreeQty() + " " + model.getFreeUOM());

        try {
            String totalPrice = model.getSubTotal();
            double value = Double.parseDouble(totalPrice);
            model.setProductTotal(totalPrice);
            holder.tvTotalPrice.setText(context.getResources().getString(R.string.total_price) + " : " + String.format(Locale.CANADA, "%.2f", value));
        } catch (Exception e) {
            e.printStackTrace();
            model.setProductTotal("0.00");
            holder.tvTotalPrice.setText(context.getResources().getString(R.string.total_price) + " : " + "0.00");
        }

        return view;
    }

    private class ViewHolder {
        TextView tvSKU, tvMrp, tvProduct, tvUnitPrice, tvPrice, tvTotalPrice, tvStatus, tvFreebieQty, tvQty/*, tvUOM, tvFreeUOM*/, tvDiscountOnBill, tvSlp, tvStd, tvThirtyD, tvAvailInv;
    }

}
