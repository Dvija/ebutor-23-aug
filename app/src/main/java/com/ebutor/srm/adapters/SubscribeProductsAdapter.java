package com.ebutor.srm.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.ebutor.R;
import com.ebutor.models.TestProductModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SubscribeProductsAdapter extends RecyclerView.Adapter<SubscribeProductsAdapter.MyViewHolder> implements Filterable {

    final LayoutInflater inflater;
    private Context context;
    private ArrayList<TestProductModel> arrCartList;
    private OnProductSubscribeChanged listener;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private ModelFilter filter;
    private List<TestProductModel> filteredModelItemsArray;

    public SubscribeProductsAdapter(Context context, ArrayList<TestProductModel> arrCartList) {
        this.context = context;
        this.arrCartList = arrCartList;
        inflater = LayoutInflater.from(context);
        this.filteredModelItemsArray = new ArrayList<TestProductModel>();
        filteredModelItemsArray.addAll(arrCartList);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new SimpleBitmapDisplayer())
                .build();
    }

    public void setListener(OnProductSubscribeChanged listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View v = inflater.inflate(R.layout.row_subscribe_product, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final TestProductModel model = filteredModelItemsArray.get(position);

        holder.tvproduct.setText(model.getProductTitle());

        holder.aSwitch.setChecked(model.isChecked());
        holder.aSwitch.setTag(model);

        holder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    model.setChecked(true);
                    // checked. Subscribe
                    if (listener != null)
                        listener.onSubscriptionChanges(buttonView.getTag(), position, 1);
                } else {
                    model.setChecked(false);
                    //unchecked.. Please do Un-subscribe
                    if (listener != null)
                        listener.onSubscriptionChanges(buttonView.getTag(), position, 0);
                }
            }
        });

        holder.tvproduct.setText(model.getProductTitle());
        holder.tvSKUCode.setText(model.getSKUCode());
        ImageLoader.getInstance().displayImage(model.getPrimaryImage(), holder.ivProduct, options, animateFirstListener);


    }

    public void addItem(TestProductModel testProductModel) {
        if (filteredModelItemsArray != null && !filteredModelItemsArray.contains(testProductModel)) {
            filteredModelItemsArray.add(testProductModel);
        }
    }

    public void removeAllItems() {
        if (filteredModelItemsArray != null) {
            filteredModelItemsArray.clear();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (filteredModelItemsArray != null)
            return filteredModelItemsArray.size();
        else
            return 0;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModelFilter();
        }
        return filter;
    }

    public interface OnProductSubscribeChanged {
        void onSubscriptionChanges(Object object, int position, int flag);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProduct;
        TextView tvproduct, tvSKUCode;
        Switch aSwitch;

        public MyViewHolder(View view) {
            super(view);
            ivProduct = (ImageView) view.findViewById(R.id.ivProduct);
            tvproduct = (TextView) view.findViewById(R.id.tvProduct);
            tvSKUCode = (TextView) view.findViewById(R.id.tvSKUCode);
            aSwitch = (Switch) view.findViewById(R.id.a_switch);
        }
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

    private class ModelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (!TextUtils.isEmpty(constraint)) {
                if (constraint != null && constraint.toString().length() > 0) {
                    constraint = constraint.toString().toLowerCase();
                    ArrayList<TestProductModel> filteredItems = new ArrayList<TestProductModel>();

                    for (int i = 0, l = arrCartList.size(); i < l; i++) {
                        TestProductModel m = arrCartList.get(i);
                        if (m.getProductTitle().toLowerCase().contains(constraint))
                            filteredItems.add(m);
                    }
                    result.count = filteredItems.size();
                    result.values = filteredItems;
                } else {
                    result.values = arrCartList;
                    result.count = arrCartList.size();

                }
            } else {
                result.values = arrCartList;
                result.count = arrCartList.size();
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredModelItemsArray.clear();
            if (filteredModelItemsArray != null && results != null && results.count > 0) {
                filteredModelItemsArray.addAll((ArrayList<TestProductModel>) results.values);
            }
            notifyDataSetChanged();
        }
    }
}
