package com.ebutor.srm.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.srm.fragments.PODetailFragment;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Utils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PODetailActivity extends ParentActivity {

    private String poId = "", status;
    private Dialog dialog;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.empty_fragment_layout_suppliers);

        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {

            getSupportActionBar().setTitle(getResources().getString(R.string.po_detail));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        dialog = Utils.createLoader(PODetailActivity.this, ConstantValues.TABS_PROGRESS);

        if (getIntent().hasExtra("poId"))
            poId = getIntent().getStringExtra("poId");
        if (getIntent().hasExtra("status")) {
            status = getIntent().getStringExtra("status");
        }

        if (savedInstanceState == null) {
            Fragment fragment = new PODetailFragment();
            Bundle args = new Bundle();
            args.putString("poId", poId);
            args.putString("status", status);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
