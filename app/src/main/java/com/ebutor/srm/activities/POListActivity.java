package com.ebutor.srm.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.srm.fragments.POListFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class POListActivity extends ParentActivity {

    private SharedPreferences mSharedPreferences;
    private boolean isAllPOs = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.empty_fragment_layout_suppliers);

        if (getIntent().hasExtra("AllPos")) {
            isAllPOs = getIntent().getBooleanExtra("AllPos", false);
        }
        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.po_list));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            Fragment fragment = new POListFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("AllPos", isAllPOs);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
