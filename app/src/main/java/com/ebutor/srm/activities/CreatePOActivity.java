package com.ebutor.srm.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.ebutor.MyApplication;
import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.srm.fragments.CreatePOFragment;
import com.google.android.gms.analytics.Tracker;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Srikanth Nama on 3/26/2016.
 */
public class CreatePOActivity extends ParentActivity {

    public LinearLayout llCart;
    private Tracker mTracker;
    private String pocart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.empty_fragment_layout_suppliers);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.po));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().hasExtra("pocart")) {
            pocart = getIntent().getStringExtra("pocart");
        }

        if (savedInstanceState == null) {

            Fragment fragment = new CreatePOFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pocart", pocart);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
        MyApplication application = (MyApplication) getApplication();
        //Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
