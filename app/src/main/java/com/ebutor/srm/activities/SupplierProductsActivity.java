package com.ebutor.srm.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.util.Predicate;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.FilterActivity;
import com.ebutor.MyApplication;
import com.ebutor.NotificationsActivity;
import com.ebutor.ParentActivity;
import com.ebutor.R;
import com.ebutor.adapters.ProductsExpandableAdapterNew;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.customview.SimpleTagImageView;
import com.ebutor.customview.expandable.library.AbstractSlideExpandableListAdapter;
import com.ebutor.customview.expandable.library.ActionSlideExpandableListView;
import com.ebutor.database.DBHelper;
import com.ebutor.fragments.SortFragment;
import com.ebutor.fragments.WishListFragment;
import com.ebutor.interfaces.ProductSKUClickListener;
import com.ebutor.models.CartModel;
import com.ebutor.models.NewPacksModel;
import com.ebutor.models.NewProductModel;
import com.ebutor.models.NewVariantModel;
import com.ebutor.models.PriceHistoryModel;
import com.ebutor.models.ProductsResponse;
import com.ebutor.models.TestProductModel;
import com.ebutor.models.WishListModel;
import com.ebutor.srm.adapters.PackTypesSpinnerAdapter;
import com.ebutor.srm.adapters.SupplierProductsExpandableAdapter;
import com.ebutor.srm.fragments.PriceHistoryFragment;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class SupplierProductsActivity extends ParentActivity implements ProductSKUClickListener,
        Response.ErrorListener, VolleyHandler<Object>,
        ProductsExpandableAdapterNew.ProductImageClickListener, SupplierProductsExpandableAdapter.ProductImageClickListener {

    private static final int TRANSLATE_DURATION_MILLIS = 2000;
    private final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();
    public SharedPreferences mSharedPreferences;
    public LayoutInflater inflater;
    SupplierProductsExpandableAdapter expandableAdapter;
    //    ArrayList<TestProductModel> productModelArrayList;
    ArrayList<TestProductModel> productsArrayList;
    ArrayList<WishListModel> wishListArray;
    ArrayList<NewPacksModel> arrSlabRates;
    JSONArray variantsJSONArray = new JSONArray();
    DisplayImageOptions options;
    private SwipeRefreshLayout swipeRefreshLayout;
    //    AlertDialogFragment newFragment;
    private ActionSlideExpandableListView actionSlideExpandableListView;
    private View expandedItemView;
    private int expandedPosition = -1;
    private int cartCount = 0;
    private int totalQty = 0, /*totalQtyToSend = 0, */
            fbQty = 0, fbQtyTotal = 0;
    private String listType = "list", unAvailableProducts, allProductIds;
    //    private RelativeLayout ivFilter;
//    private int currentOffset = 0;
    private int totalItemsCount = 0;
    private int offsetLimit = Utils.offsetLimit;// you can change this acg to requirement
    private String requestTypeString = "", requestTypeTemp = "", requestTypeAlert = "";
    private String inputId, inputName, keyId, marginToApply, unitPriceToApply;
    private double totalPrice, totalUnitPrice, totalMargin;
    private TestProductModel selectedProductModel;
    private int _requestedQuantity;
    private TextView tvSort, tvFilter;
    private View tvNoItems;
    //    private View footerView;
    private TextView tvTotalAmount/*,tvEsu*/;
    private Button btnProceed;
    private RelativeLayout flMain;
    private RelativeLayout rlMain;
    private String filterData, supplierId = "", whId = "";
    private String filterString, sortString = "";
    private JSONObject inputJson;
    private String selectedProductId = "", selWishListId = "", selProductId = "", selSortId = "", productId;
    private TestProductModel selectedWishListProductModel, expandedProductModel;
    private String buyerListingId = "";
    private boolean isShoppingList = false;
    private TextView tvBadge, tvTotalQty, tvTotalMargin, tvTotalPrice, tvFbQty;
    private TextView tvThumbnail;
    private RelativeLayout rlAlert;
    private LinearLayout llFooterView, slabsLayout, variantsLayout1, variantsLayout2, innerVariantsLayout1, innerVariantsLayout2;
    private TextView tvAlertMsg;
    private String type = "", prevClickedProductId = "";
    private String businessFilter = "", productFilter = "";
    private boolean isLoadMore, isFreebie = false, isFreebieChecked = false, isChild = false;
    private LinearLayout llFooter, llList;
    //    private FloatingActionButton fabFilter;
    private Tracker mTracker;
    private boolean isClear = false;
    private Dialog dialog;
    WishListFragment.OkListener confirmListener = new WishListFragment.OkListener() {
        @Override
        public void onOkClicked(String wishListId) {
            selWishListId = wishListId;
            addProductToWishList(wishListId);
        }
    };
    //    private double packPrice = 0.00, unitPrice = 0.00;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private LoadType loadType;
    SortFragment.OnSelListener okListener = new SortFragment.OnSelListener() {

        @Override
        public void onSelected(String sortId) {
            selSortId = sortId;
//            currentOffset = 0;
            getData(false);
        }
    };
    private ProgressBar progress;
    private TextView tvNoPacks;
    private TextView tvFreebieDesc;
    private NewPacksModel selectedPackModel, selectedFreePackModel, selectedFreePackModelPrev;
    private ProductsResponse productsResponse;
    private String variant_1_name, variant_2_name, variant_3_name;
    private ArrayList<String> arrVariant2;
    private int esu = 0, prevPos = 0, currentPos = 0;
    View.OnClickListener onVariantClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            Toast.makeText(getActivity(), "Variant Clicked", Toast.LENGTH_SHORT).show();
            marginToApply = "0";
            unitPriceToApply = "0";
            if (v != null && v.getId() == R.id.id_variant_one) {
                int count = variantsLayout1.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout1.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (tvFreebieDesc != null)
                    tvFreebieDesc.setVisibility(View.GONE);
                Object object = v.getTag();
                if (object != null && object instanceof String) {
                    variant_2_name = (String) object;
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    if (expandedItemView != null) {
                        final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                        ll.removeAllViews();

                        innerVariantsLayout2 = (LinearLayout) expandedItemView.findViewById(R.id.inner_variants_2);
                        innerVariantsLayout2.removeAllViews();

                        TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(selectedProductId, variant_1_name, variant_2_name, "");
                        if (testProductModel != null) {
//                        View childItemView = actionSlideExpandableListView.getChildAt(expandedPosition);
                            View childItemView = getChildItemView(expandedPosition);
                            if (childItemView == null) {
                                Toast.makeText(SupplierProductsActivity.this, R.string.oops_try_again, Toast.LENGTH_SHORT).show();
                                return;
                            }
                            TextView tvProduct = (TextView) childItemView.findViewById(R.id.tvProductName);
                            TextView tvMrp = (TextView) childItemView.findViewById(R.id.tvProductMRP);
                            TextView tvEsp = (TextView) childItemView.findViewById(R.id.tvESP);
                            SimpleTagImageView ivProduct = (SimpleTagImageView) childItemView.findViewById(R.id.productImage);
                            tvProduct.setText(testProductModel.getProductTitle());
                            setOfferImageBackground(testProductModel.getPackType(), childItemView);
                            double mrp = 0;
                            try {
                                mrp = Double.parseDouble(testProductModel.getMrp());
                            } catch (Exception e) {

                            }
                            final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
                            setMRP(sb, tvMrp);
                            ImageLoader.getInstance().displayImage(testProductModel.getPrimaryImage(), ivProduct, options, animateFirstListener);
                        }

                        String variant2list = DBHelper.getInstance().getVariant3List(selectedProductId, variant_1_name, variant_2_name);
                        if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                            if (variant2list != null && variant2list.length() > 0)
                                arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));
                            if (arrVariant2 != null && arrVariant2.size() > 0) {
                                View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                                variantsLayout2 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout2 != null) {
                                    variantsLayout2.removeAllViews();

                                    int selectedPos = 0;
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "";
                                        if (arrVariant2.get(i).split(":").length > 0)
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        else
                                            childVariant = arrVariant2.get(i);
                                        TextView childVariantLabel = createChildVariant(childVariant, R.id.id_variant_two);
                                        childVariantLabel.setTag(childVariant);
                                        childVariantLabel.setOnClickListener(onVariantClickListener);
                                        variantsLayout2.addView(childVariantLabel);

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout2.getChildCount() > 0) {
                                        variantsLayout2.getChildAt(selectedPos).performClick();
                                        variantsLayout2.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout2.addView(innerVariantsView);

                                }
                            } else {
                                Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //create packs
                            if (testProductModel != null) {
                                productId = testProductModel.getProductId();
                                SupplierProductsActivity.this.selectedProductModel = testProductModel;
                                LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                                slabRatesLayout.removeAllViews();
                                getSlabRates(productId);
                            }
                        }
                    }
                }
            } else if (v.getId() == R.id.id_variant_two) {
                //todo
                int count = variantsLayout2.getChildCount();
                for (int i = 0; i < count; i++) {
                    View btnView = variantsLayout2.getChildAt(i);
                    btnView.setSelected(false);
                }
                v.setSelected(true);
                if (tvFreebieDesc != null)
                    tvFreebieDesc.setVisibility(View.GONE);
                if (v.getTag() != null)
                    variant_3_name = (String) v.getTag();
                if (expandedItemView != null) {
                    final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                    ll.removeAllViews();
                    TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(selectedProductId, variant_1_name, variant_2_name, variant_3_name);
                    if (testProductModel != null) {
//                    View childItemView = actionSlideExpandableListView.getChildAt(expandedPosition);
                        View childItemView = getChildItemView(expandedPosition);
                        TextView tvProduct = (TextView) childItemView.findViewById(R.id.tvProductName);
                        TextView tvMrp = (TextView) childItemView.findViewById(R.id.tvProductMRP);
                        TextView tvEsp = (TextView) childItemView.findViewById(R.id.tvESP);
                        SimpleTagImageView ivProduct = (SimpleTagImageView) childItemView.findViewById(R.id.productImage);
                        tvProduct.setText(testProductModel.getProductTitle());
                        setOfferImageBackground(testProductModel.getPackType(), childItemView);
                        double mrp = 0;
                        try {
                            mrp = Double.parseDouble(testProductModel.getMrp());
                        } catch (Exception e) {

                        }
                        final SpannableStringBuilder sb = new SpannableStringBuilder(testProductModel.getMrp() == null ? "MRP : " : "MRP : " + String.format(Locale.CANADA, "%.2f", mrp));
                        setMRP(sb, tvMrp);
                        ImageLoader.getInstance().displayImage(testProductModel.getPrimaryImage(), ivProduct, options, animateFirstListener);
                        productId = testProductModel.getProductId();
                        SupplierProductsActivity.this.selectedProductModel = testProductModel;
                        LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                        slabRatesLayout.removeAllViews();
                        getSlabRates(productId);
                    }
                }
            } else if (v.getId() == R.id.id_slab_qty) {
                if (v.getTag() != null) {
                    int count = slabsLayout.getChildCount();
                    for (int i = 0; i < count; i++) {
                        arrSlabRates.get(i).setEsuQty(0);
                        arrSlabRates.get(i).setQty(0);
//                        arrSlabRates.get(i).setQtyToBeSent(0);
                        View btnView = slabsLayout.getChildAt(i);
                        btnView.setSelected(false);
                        selectedPackModel.setSelected(false);
                    }
                    v.setSelected(true);
                    selectedPackModel.setSelected(true);

                    NewPacksModel newPacksModel = (NewPacksModel) v.getTag();
                    if (newPacksModel != null) {
                        selectedPackModel = newPacksModel;
                        if (expandedItemView != null) {
                            final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                            ll.removeAllViews();
                            createPacksForVariant(newPacksModel, ll, expandedPosition);
                        }
                    }
                }
            }
        }
    };
    private EditText etSearch;

    public static Collection<NewProductModel> filter(Collection<NewProductModel> target, Predicate<NewProductModel> predicate) {
        Collection<NewProductModel> result = new ArrayList<NewProductModel>();
        for (NewProductModel element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    private View getChildItemView(int index) {
        View v = actionSlideExpandableListView.getChildAt(index -
                actionSlideExpandableListView.getFirstVisiblePosition());

        return v;
    }

    private void setMRP(SpannableStringBuilder sb, TextView textView) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
//        sb.setSpan(fcs, 5, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, 3, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.setText(sb);
    }

    private void getSlabRates(String productId) {
        try {
            if (!prevClickedProductId.equalsIgnoreCase(productId)) {
                prevClickedProductId = productId;
                JSONObject obj = new JSONObject();
                PARSER_TYPE parserType = PARSER_TYPE.PURCHASE_SLABS;
                obj.put("product_id", productId);
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
                obj.put("supplier_id", mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPurchaseSlabsURL, map,
                        SupplierProductsActivity.this, SupplierProductsActivity.this, parserType);
                productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getPurchaseSlabsURL);

                if (expandedItemView != null) {
                    final FrameLayout fl = (FrameLayout) expandedItemView.findViewById(R.id.fl_expand_content);
                    progress = (ProgressBar) fl.findViewById(R.id.loading);
                    tvNoPacks = (TextView) fl.findViewById(R.id.tv_no_packs);
                    tvNoPacks.setVisibility(View.GONE);
                    progress.setVisibility(View.VISIBLE);
                }
            }
//            if (dialog != null)
//                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_supplier_products);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mSharedPreferences.getString(ConstantValues.KEY_SUPPLIER_NAME, ""));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        inflater = this.getLayoutInflater();

        if (getIntent().hasExtra("SupplierId")) {
            supplierId = getIntent().getStringExtra("SupplierId");
        }
        if (getIntent().hasExtra("whId")) {
            whId = getIntent().getStringExtra("whId");
        }
        if (getIntent().hasExtra("Filter")) {
            filterData = getIntent().getStringExtra("Filter");
        }

        if (getIntent().hasExtra("Type")) {
            requestTypeString = getIntent().getStringExtra("Type");
        }

        if (getIntent().hasExtra("Id")) {
            inputId = getIntent().getStringExtra("Id");
        }

        if (getIntent().hasExtra("key_id")) {
            keyId = getIntent().getStringExtra("key_id");
        }

        if (getIntent().hasExtra("Name")) {
            inputName = getIntent().getStringExtra("Name");
        }
        if (getIntent().hasExtra("WishListName")) {
            isShoppingList = true;
            String wishListName = getIntent().getStringExtra("WishListName");
            buyerListingId = getIntent().getStringExtra("buyer_listing_id");
            TextView tvWishListName = (TextView) findViewById(R.id.tvWishListName);
            tvWishListName.setVisibility(View.VISIBLE);
            tvWishListName.setText(wishListName);
        }

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_placeholder_small)
                .showImageForEmptyUri(R.drawable.ic_no_image)
                .showImageOnFail(R.drawable.ic_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        dialog = Utils.createLoader(SupplierProductsActivity.this, ConstantValues.TOOLBAR_PROGRESS);

        llFooter = (LinearLayout) findViewById(R.id.llLoadMore);
        llList = (LinearLayout) findViewById(R.id.llList);
        llFooterView = (LinearLayout) findViewById(R.id.ll_footer_view);
        actionSlideExpandableListView = (ActionSlideExpandableListView) findViewById(R.id.list);
        tvNoItems = (View) findViewById(R.id.tvNoItems);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        flMain = (RelativeLayout) findViewById(R.id.fl_main);
        tvSort = (TextView) findViewById(R.id.tv_sort);
        tvFilter = (TextView) findViewById(R.id.tv_filter);
        tvThumbnail = (TextView) findViewById(R.id.tv_thumbnail);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        etSearch = (EditText) findViewById(R.id.et_search);

        wishListArray = new ArrayList<WishListModel>();

        productsArrayList = new ArrayList<>();

        expandableAdapter = new SupplierProductsExpandableAdapter(SupplierProductsActivity.this, productsArrayList, actionSlideExpandableListView);
        expandableAdapter.setProductSKUClickListener(SupplierProductsActivity.this);
        expandableAdapter.setProductImageClickListener(SupplierProductsActivity.this);
        expandableAdapter.setIsShoppingList(isShoppingList);
//        actionSlideExpandableListView.setOnScrollListener(onScrollListener());
        actionSlideExpandableListView.setAdapter(expandableAdapter);


        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();


        if (requestTypeString.equalsIgnoreCase("WishList")) {
//            fabFilter.setVisibility(View.GONE);
        }

        actionSlideExpandableListView.setExpandCollapseListener(new AbstractSlideExpandableListAdapter.OnItemExpandCollapseListener() {
            @Override
            public void onExpand(View itemView, int position) {
                expandedItemView = itemView;
                expandedPosition = position;
                Object object = itemView.getTag();
                if (object instanceof TestProductModel) {
                    TestProductModel productModel = (TestProductModel) object;
                    expandedProductModel = productModel;
                    selectedProductId = productModel.getProductId();
                    if (expandedItemView != null) {
                        LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                        if (ll != null)
                            ll.removeAllViews();

                        if (productModel.getVariantModelArrayList() != null && productModel.getVariantModelArrayList().size() > 0) {

                            if (productModel.getSelectedVariant() != null) {
                                OnProductSKUClicked(productModel.getSelectedVariant(), expandedPosition, productModel.isChild());
                            }
                        }
                    }
                }
            }

            @Override
            public void onCollapse(View itemView, int position) {


                expandedPosition = -1;
                expandedItemView = null;
            }
        });

        MyApplication.getInstance().setArrProductFilters(null);

        getData(false);
        getSortData();

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (expandableAdapter != null) {
                    String text = etSearch.getText().toString().toLowerCase(Locale.getDefault());
                    expandableAdapter.getFilter().filter(text);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
                    switch (requestTypeString) {
                        case "WishList":
                            getWishList();
                            break;
                        case "FilterData":
                            getFilterData();
                            break;
                        case "ApplyFilter":
                            if (inputJson != null)
                                applyFilter();
                            break;
                        case "AddWishList":
                            addProductToWishList(selWishListId);
                            break;
                        case "DeleteWishList":
                            deleteProductFromWishList(selProductId);
                            break;
                        default:
                            getFilterData();
                            getData(false);
                            break;
                    }
                } else {
                    Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestTypeTemp = "";
                isLoadMore = false;
                showSortDialog();
            }
        });

        tvThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSlideExpandableListView.collapse();

                switch (listType) {
                    case "list":
                        listType = "list1";
                        expandableAdapter.changeViewType(2);
                        expandableAdapter.notifyDataSetChanged();
                        break;
                    case "list1":
                        listType = "list";
                        expandableAdapter.changeViewType(1);
                        expandableAdapter.notifyDataSetChanged();
                        break;
                    /*case "grid":
                        listType = "list";
                        break;*/
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                selSortId = "";
                actionSlideExpandableListView.collapse();
                getData(true);
            }
        });

        tvFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (requestTypeString != null && requestTypeString.equalsIgnoreCase("Filter")) {
                    requestTypeString = type;
                }

                isLoadMore = false;

                getFilterData();

//                newFragment = AlertDialogFragment.newInstance();
//                newFragment.initializeInterface(MainActivity.this);
//                Bundle args = new Bundle();
//                args.putString("Type", requestType);
//                args.putString("Id", inputId);
//                args.putString("BusinessFilter", businessFilter);
//                args.putString("ProductFilter", productFilter);
//                newFragment.setArguments(args);
//                newFragment.show(getFragmentManager(), "dialog");
//                newFragment.setFilterData(filterString);

            }
        });

    }

    public void setOfferImageBackground(String packType, View view) {

        if (view == null) {
            return;
        }
        Button ivOffer = (Button) view.findViewById(R.id.iv_offer);

        switch (packType) {
            case "regular":
            case "REGULAR":
                ivOffer.setVisibility(View.GONE);
                break;
            case "cpoffer":
            case "CPOFFER":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_green);
                ivOffer.setText("CP Offer");
                break;
            case "freebie":
            case "FREEBIE":
                ivOffer.setVisibility(View.VISIBLE);
                ivOffer.setBackgroundResource(R.drawable.ic_offer_red);
                ivOffer.setText("Freebie");
                break;
        }
    }

    private void getProducts(String productIds, boolean isFreebie) {
        try {
            if (requestTypeTemp.equalsIgnoreCase("Filter")) {
                loadType = LoadType.APPLY_FILTER;
            } /*else {
                loadType = LoadType.LOAD_MORE;
            }*/
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.SRM_PRODUCTS;
            String appUrl = AppURL.srmProductsURL;

//            obj.put("offset", "0");
//            obj.put("offset_limit", "20");
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
            obj.put("product_ids", productIds);
            obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, appUrl, map,
                    SupplierProductsActivity.this, SupplierProductsActivity.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, appUrl);
            if (dialog != null && !isLoadMore && !isFreebie)
                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSortDialog() {
        FragmentManager fm = getSupportFragmentManager();
        SortFragment sortDialog = SortFragment.newInstance(sortString);
        sortDialog.setCancelable(true);
        sortDialog.setConfirmListener(okListener);
        sortDialog.show(fm, "sort_fragment");
    }

    private void getSortData() {
        if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
            requestTypeAlert = "SortData";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                map.put("data", obj.toString());

                VolleyBackgroundTask sortDataRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.sortingURL, map,
                        SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.GET_SORT_DATA);
                sortDataRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(sortDataRequest, AppURL.sortingURL);
//                if (dialog != null)
//                    dialog.show();

            } catch (Exception e) {

            }
        } else {
            requestTypeAlert = "SortData";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    private void getData(boolean isSwipeDown) {
        if (requestTypeString != null) {
            if (TextUtils.isEmpty(selSortId))
                loadType = LoadType.DEFAULT;
            else
                loadType = LoadType.SORT;
            if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
                requestTypeAlert = requestTypeString;
                rlAlert.setVisibility(View.GONE);
                flMain.setVisibility(View.VISIBLE);
                try {
                    JSONObject obj = new JSONObject();
                    PARSER_TYPE parserType = PARSER_TYPE.DEFAULT;
                    String appUrl = AppURL.getSupplierProductsURL;
//                    String appUrl = AppURL.categoriesURL;
                    parserType = PARSER_TYPE.SUPPLIER_PRODUCTS;

//                    obj.put("category_id", "16");//temp
                    obj.put("sort_id", selSortId);
                    obj.put("supplier_id", supplierId);
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    obj.put("le_wh_id", whId);
//                    obj.put("offset", String.valueOf(currentOffset));
                    obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));
//                    obj.put("offset_limit", String.valueOf(offsetLimit));

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, appUrl, map,
                            SupplierProductsActivity.this, SupplierProductsActivity.this, parserType);
                    productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                            AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(productsRequest, appUrl);
                    if (dialog != null && !isSwipeDown && !isLoadMore && loadType == LoadType.SORT)
                        dialog.show();

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

            } else {
                requestTypeAlert = requestTypeString;
                rlAlert.setVisibility(View.VISIBLE);
                flMain.setVisibility(View.GONE);
            }

        }

    }

    @Override
    public void OnProductSKUClicked(Object object, final long position, boolean isChild) {
        /*if (expandedItemView == null)
            return;
        if (expandedPosition == -1)
            return;*/
        if (object == null)
            return;

        if (position != expandedPosition) {
            View view = actionSlideExpandableListView.getViewByPosition((int) position, actionSlideExpandableListView);
            if (view != null) {
                View expandToggle = view.findViewById(R.id.expandable_toggle_button);
                if (expandToggle != null) {
                    expandToggle.performClick();
                    OnProductSKUClicked(object, position, isChild);
                }
            }
            return;
        }

        if (object instanceof String) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            variant_1_name = (String) object;
            marginToApply = "0";
            unitPriceToApply = "0";
            if ((int) position != expandedPosition)
                return;
            if (tvFreebieDesc != null)
                tvFreebieDesc.setVisibility(View.GONE);

            if (expandedItemView != null) {
                final LinearLayout ll = (LinearLayout) expandedItemView.findViewById(R.id.ll_expand_content);
                ll.removeAllViews();

                innerVariantsLayout1 = (LinearLayout) expandedItemView.findViewById(R.id.inner_variants);
                if (innerVariantsLayout1 != null) {
                    innerVariantsLayout1.removeAllViews();

                    if (isChild) {
                        this.isChild = isChild;
                        TestProductModel testProductModel = DBHelper.getInstance().getProductById(selectedProductId, true);
                        if (testProductModel != null) {
                            productId = testProductModel.getProductId();
                            selectedProductModel = testProductModel;
                            LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            getSlabRates(productId);
                        }
                    } else {

                        String variant2list = DBHelper.getInstance().getVariant2List(selectedProductId, variant_1_name);
                        if (variant2list != null && !TextUtils.isEmpty(variant2list) && !variant2list.equalsIgnoreCase("null")) {
                            if (variant2list != null && variant2list.length() > 0)
                                arrVariant2 = new ArrayList<String>(Arrays.asList(variant2list.split("\\s*,\\s*")));
                            if (arrVariant2 != null && arrVariant2.size() > 0) {
                                View innerVariantsView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                                variantsLayout1 = (LinearLayout) innerVariantsView.findViewById(R.id.variants_layout);
                                if (variantsLayout1 != null) {
                                    variantsLayout1.removeAllViews();

                                    int selectedPos = 0;
                                    for (int i = 0; i < arrVariant2.size(); i++) {
                                        String childVariant = "";
                                        if (arrVariant2.get(i).split(":").length > 0)
                                            childVariant = arrVariant2.get(i).split(":")[0];
                                        else
                                            childVariant = arrVariant2.get(i);
                                        TextView childVariantLabel = createChildVariant(childVariant, R.id.id_variant_one);
                                        childVariantLabel.setTag(childVariant);
                                        childVariantLabel.setOnClickListener(onVariantClickListener);
                                        variantsLayout1.addView(childVariantLabel);

//                            if (childVariant.isSelected())
//                                selectedPos = i;
                                    }

                                    if (variantsLayout1.getChildCount() > 0) {
                                        variantsLayout1.getChildAt(selectedPos).performClick();
                                        variantsLayout1.getChildAt(selectedPos).setSelected(true);
                                    }

                                    innerVariantsLayout1.addView(innerVariantsView);

                                }
                            } else {
                                Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.no_packs), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //create packs
                            TestProductModel testProductModel = DBHelper.getInstance().getParentProductDetails(selectedProductId, variant_1_name, "", "");
                            if (testProductModel != null) {
                                productId = testProductModel.getProductId();
                                SupplierProductsActivity.this.selectedProductModel = testProductModel;
                                LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                                slabRatesLayout.removeAllViews();
                                getSlabRates(productId);
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateFooter(View footerView, NewPacksModel newPacksModel) {
        if (footerView == null) {
            return;
        }
        if (newPacksModel == null)
            return;
       /* if(selectedFreePackModel == null){
            showToast("Free packs model is empty");
            return;
        }*/

        totalQty = 0;
//        totalQtyToSend = 0;
        fbQty = 0;
        fbQtyTotal = 0;
        marginToApply = "0";
        unitPriceToApply = "0";

        totalQty = newPacksModel.getQty();
//        totalQtyToSend = newPacksModel.getEsuQty();
        fbQty = newPacksModel.getFreeQty();
        fbQtyTotal = getProductsInEaches(selectedFreePackModel, fbQty + "");

        marginToApply = newPacksModel.getMargin();
        unitPriceToApply = newPacksModel.getUnitPrice();

        totalUnitPrice = 0;
        if (!TextUtils.isEmpty(unitPriceToApply)) {
            totalUnitPrice = Double.parseDouble(unitPriceToApply);
        }

        totalMargin = 0;
        if (!TextUtils.isEmpty(marginToApply)) {
            totalMargin = Double.parseDouble(marginToApply);
        }

//            skuModel.setAppliedMargin(marginToApply);
//            skuModel.setAppliedMRP(unitPriceToApply);

        tvTotalQty = (TextView) footerView.findViewById(R.id.tvTotalQty);
        tvTotalMargin = (TextView) footerView.findViewById(R.id.tvTotalMargin);
        tvTotalPrice = (TextView) footerView.findViewById(R.id.tvTotalPrice);
        if (tvTotalQty != null) {
//                tvTotalQty.setText("TOTAL QTY\n" + totalQty);
            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.qty) + "\n" + (newPacksModel.getEsuQty()));
            setSpannedText(sb, tvTotalQty, 3);
        }

        if (tvTotalMargin != null) {
//                tvMargin.setText("MARGIN\n" + String.format(Locale.CANADA, "%.2f", margin));
            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.markup_margin) + "\n" + String.format(Locale.CANADA, "%.2f", totalMargin) + "%");
            setSpannedText(sb, tvTotalMargin, 8);
        }
        totalPrice = totalUnitPrice * (totalQty - fbQtyTotal);
//            skuModel.setTotalPrice(String.format(Locale.CANADA, "%.2f", totalPrice));
        if (tvTotalPrice != null) {
//                tvTotalPrice.setText("TOTAL PRICE\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
            setSpannedText(sb, tvTotalPrice, 11);

        }

        if (expandedItemView != null) {
            tvFreebieDesc = (TextView) expandedItemView.findViewById(R.id.tvFreebieDesc);
            if (selectedPackModel != null && selectedPackModel.getFreebieDesc() != null &&
                    !TextUtils.isEmpty(selectedPackModel.getFreebieDesc()) && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("null") && !selectedPackModel.getFreebieDesc().equalsIgnoreCase("0")) {
                tvFreebieDesc.setText(selectedPackModel.getFreebieDesc());
                tvFreebieDesc.setVisibility(View.VISIBLE);
            } else
                tvFreebieDesc.setVisibility(View.GONE);
        }
    }

    private void setSpannedText(SpannableStringBuilder sb, TextView textView, int start) {
        // Span to set text color to some RGB value
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(255, 0, 0));
        // Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        // Set the text color for first 4 characters
        sb.setSpan(fcs, start, sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "font/OpenSans-Bold.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        sb.setSpan(typefaceSpan, start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new RelativeSizeSpan(1.2f), start, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(sb);
    }

    @Override
    public void OnCartItemAdded(Object object) {
        if (expandedItemView == null)
            return;
        if (expandedPosition == -1)
            return;
        if (object == null)
            return;

        if (object instanceof TestProductModel) {
            TestProductModel productModel = (TestProductModel) object;
            totalQty = 0;
            int totalQtyToSend = 0;

            ArrayList<NewPacksModel> packs = arrSlabRates;
            if (packs != null && packs.size() > 0) {
                for (NewPacksModel packModel : packs) {

                    int qty = packModel.getEsuQty();
                    totalQtyToSend += qty;
                }
            }

            double mrp = Double.parseDouble(selectedProductModel.getMrp());
            double unitPrice = Double.parseDouble(selectedPackModel.getUnitPrice());

            if (unitPrice > mrp) {
                Toast.makeText(SupplierProductsActivity.this, getString(R.string.enter_price_less_than_mrp), Toast.LENGTH_SHORT).show();
                return;
            }

            if (totalQtyToSend <= 0) {
                // show alert
                Utils.showAlertDialog(SupplierProductsActivity.this, getString(R.string.please_add_packs));
                return;
            }

          /*  if (totalPrice <= 0) {
                // show alert
                Utils.showAlertDialog(SupplierProductsActivity.this, getString(R.string.total_less_than_zero));
                return;
            }*/

            CartModel cartModel = new CartModel();
            cartModel.setProductId(productId);
            cartModel.setCustomerId(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
            cartModel.setQuantity(totalQtyToSend);
            cartModel.setUnitPrice(unitPriceToApply);
            cartModel.setMargin(marginToApply);
            cartModel.setTotalPrice(totalPrice);
            cartModel.setWarehouseId(mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
            cartModel.setDiscount("");//todo
            cartModel.setPackSize(selectedPackModel.getLevel());
            cartModel.setRemarks(selectedPackModel.getLevelName());
            cartModel.setProductPackId(selectedPackModel.getProductPackId());
            cartModel.setFreebiePackId(selectedFreePackModel.getProductPackId());
            int freeQty = 0;
            try {
                freeQty = Integer.parseInt(tvFbQty.getText().toString());
            } catch (Exception e) {
                freeQty = 0;
            }
            cartModel.setFreeqty(freeQty);
            cartModel.setIsChild(isChild == true ? 1 : 0);

            String _productEsu = selectedProductModel.getEsu();
            int esu = 1;
            if (_productEsu != null && !_productEsu.equals("0") && !TextUtils.isEmpty(_productEsu)) {
                try {
                    int tempEsu = Integer.parseInt(_productEsu);
                    esu = totalQtyToSend / tempEsu;
                } catch (Exception e) {
                    e.printStackTrace();
                    esu = 1;
                }
            }
            cartModel.setEsu(String.valueOf(esu));
            cartModel.setParentId(productId);
            if (isFreebieChecked) {
                cartModel.setIsFreebie("1");
            } else {
                cartModel.setIsFreebie("0");
            }

//            this.selectedProductModel = productModel;

            long rowId = Utils.insertOrUpdatePOCart(cartModel);
            if (rowId < 0) {
                showToast("Failed to insert in Cart");
            } else {
                showToast(getResources().getString(R.string.product_added_po_cart));
            }

            updateCartCount();
            MyApplication.getInstance().exportDatabase(DBHelper.DATABASE_NAME);

//            this.selectedProductModel = productModel;
//            updateToCart(productModel);
//            updateFooter(expandedItemView.findViewById(R.id.footer), model);
        }

    }

    private void showToast(String message) {
        Toast.makeText(SupplierProductsActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void OnItemOptionClicked(Object model, int type) {
        if (model != null && model instanceof TestProductModel) {
            final TestProductModel productModel = (TestProductModel) model;
            selectedWishListProductModel = productModel;
            if (!isShoppingList) {
                // Wish list
                this.selectedProductId = productModel.getProductId();
                if (wishListArray.size() == 0) {
                    getWishList();
                } else {
                    showWishListDialog();
                }
            } else {
                // Delete product from wish list
                new AlertDialog.Builder(SupplierProductsActivity.this)
                        .setTitle(getString(R.string.app_name))
                        .setMessage(getResources().getString(R.string.are_you_sure_delete_from_wishlist))
                        .setPositiveButton(getResources().getString(R.string.delete), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                selProductId = productModel.getProductId();
                                deleteProductFromWishList(selProductId);
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        }
        return;

    }

    @Override
    public void onSKUClicked(String productId) {

    }

    private void getWishList() {
        if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
            requestTypeAlert = "GetWishList";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "1");
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask getShoppingListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.shoppinglistURL, map, SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.WISHLIST);
                getShoppingListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(getShoppingListRequest, AppURL.shoppinglistURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestTypeAlert = "GetWishList";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    public JSONArray getInnerVariantsArray(ArrayList<NewVariantModel> variants) {
        try {
            for (NewVariantModel skuModel : variants) {
                if (skuModel.getHasInnerVariants() == 1) {
                    ArrayList<NewVariantModel> arrInner = skuModel.getVariantModelArrayList();
                    getInnerVariantsArray(arrInner);
                } else {
                    if (variants != null && variants.size() > 0) {
                        for (NewVariantModel newVariantModel : variants) {
                            JSONObject variantObj = new JSONObject();
                            variantObj.put("variant_id", newVariantModel.getSkuId());

                            ArrayList<NewPacksModel> packs = newVariantModel.getPacksModelArrayList();
                            JSONArray packsArray = new JSONArray();
                            int totalQtyToSend = 0;
                            if (packs != null && packs.size() > 0) {
                                for (NewPacksModel packModel : packs) {
                                    int qty = packModel.getEsuQty();
//                                    if (qty > 0) {
                                    JSONObject packObj = new JSONObject();
                                    packObj.put("pack_qty", qty);
//                                    packObj.put("packprice_id", packModel.getProductSlabId());
                                    packsArray.put(packObj);
//                                    }
                                    totalQtyToSend += qty;
                                }
                            }
                            variantObj.put("total_qty", totalQtyToSend);
                            variantObj.put("total_price", newVariantModel.getTotalPrice());
                            variantObj.put("applied_margin", newVariantModel.getAppliedMargin());
                            variantObj.put("unit_price", newVariantModel.getAppliedMRP());
                            variantObj.put("packs", packsArray);
                            if (totalQtyToSend > 0) {
                                variantsJSONArray.put(variantObj);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return variantsJSONArray;
    }

    private void updateToCart(TestProductModel productModel) {
        variantsJSONArray = new JSONArray();
        if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
            requestTypeAlert = "AddCart";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            try {
                JSONArray productsArray = new JSONArray();
                JSONObject productObj = new JSONObject();

                JSONObject dataObj = new JSONObject();
                JSONObject variantObj = new JSONObject();
                JSONArray variantArr = new JSONArray();

                ArrayList<NewPacksModel> packs = arrSlabRates;
                JSONArray packsArray = new JSONArray();
                int totalQtyToSend = 0;
                if (packs != null && packs.size() > 0) {
                    for (NewPacksModel packModel : packs) {
                        int qty = packModel.getEsuQty();
//                                    if (qty > 0) {
                        JSONObject packObj = new JSONObject();
                        packObj.put("pack_qty", qty);
                        packsArray.put(packObj);
//                                    }
                        totalQtyToSend += qty;
                    }
                }
                if (totalQtyToSend > 0) {
                    variantObj.put("variant_id", productId);
                    variantObj.put("total_qty", totalQtyToSend);
                    variantObj.put("total_price", totalPrice);
                    variantObj.put("applied_margin", marginToApply);
                    variantObj.put("unit_price", unitPriceToApply);
                    variantObj.put("packs", packsArray);
                    variantArr.put(variantObj);
                    productObj.put("product_id", productId);
                    productObj.put("variants", variantArr);
                    productsArray.put(productObj);
                }

                if (productsArray.length() > 0) {
                    dataObj.put("products", productsArray);
                    dataObj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    dataObj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                    dataObj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    dataObj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                    Log.e("Cart Obj", dataObj.toString());

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", dataObj.toString());

                    VolleyBackgroundTask viewCartRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.addCartURL, map, SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.ADD_TO_CART);
                    viewCartRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(viewCartRequest, AppURL.addCartURL);
                    if (dialog != null)
                        dialog.show();

                } else {
                    Utils.showAlertDialog(SupplierProductsActivity.this, getResources().getString(R.string.please_add_packs));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            requestTypeAlert = "AddCart";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    private void updateCartCount() {
        this.cartCount = Utils.getPOCartCount(mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""),
                mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
        invalidateOptionsMenu();
        if (tvBadge != null) {
            tvBadge.setText(String.valueOf(this.cartCount));
        }
    }

    private void setSKUListData(ArrayList<TestProductModel> arrProducts) {

        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }

        if (arrProducts != null && arrProducts.size() > 0) {
            switch (loadType) {
                case APPLY_FILTER:
                case SORT:
                case DEFAULT:
                case SWIPEDOWN:
                    productsArrayList.clear();
                    expandableAdapter.removeAllItems();
                    break;
                case LOAD_MORE:
                    break;
            }

            for (int i = 0; i < arrProducts.size(); i++) {
                expandableAdapter.addItem(arrProducts.get(i));
                if (!productsArrayList.contains(arrProducts.get(i)))
                    productsArrayList.add(arrProducts.get(i));
            }

            expandableAdapter.notifyDataSetChanged();

        } else {
            // temp list is null
            if (productsArrayList.size() < 1) {
                actionSlideExpandableListView.setVisibility(View.INVISIBLE);
                tvNoItems.setVisibility(View.VISIBLE);
            }
        }

    }

    private TextView createChildVariant(final String text, int id) {

        TextView textView = new TextView(SupplierProductsActivity.this);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, height / 25);
        textView.setBackground(ContextCompat.getDrawable(SupplierProductsActivity.this, R.drawable.sku_button_selector));
        textView.setTextColor(ContextCompat.getColor(SupplierProductsActivity.this, R.color.sku_button_color_selector));
        params.setMargins(0, 0, 20, 0);
        textView.setPadding(20, 5, 20, 5);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        textView.setId(id);
        textView.setTag(text);
        textView.setText(text == null ? "" : text);

        return textView;
    }

    private TextView createSlabRates(final String text, int id) {

        TextView textView = new TextView(SupplierProductsActivity.this);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        textView.setTextSize(12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setBackground(ContextCompat.getDrawable(SupplierProductsActivity.this, R.drawable.slab_selector));
        textView.setTextColor(ContextCompat.getColor(SupplierProductsActivity.this, R.color.sku_button_color_selector));
        params.setMargins(0, 0, 20, 0);
        textView.setPadding(20, 5, 20, 5);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(params);
        textView.setId(id);
        textView.setTag(text);
        textView.setText(text == null ? "" : text);

        return textView;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isDayChanged = MyApplication.getInstance().isDayChanged();
        if (isDayChanged) {
            MyApplication.getInstance().setDayChanged(false);
            selSortId = "";
            productsArrayList = new ArrayList<>();
            actionSlideExpandableListView.collapse();
            getData(false);
        }
        updateCartCount();
        if (requestTypeString.equalsIgnoreCase("BrandProducts")) {
            mTracker.setScreenName(inputName + " products");
        } else if (requestTypeString.equalsIgnoreCase("SubCategories")) {
            mTracker.setScreenName("Subcategories of" + " " + inputName);
        } else if (requestTypeString.equalsIgnoreCase("FeaturedOffers")) {
            mTracker.setScreenName("FeaturedOffers of" + " " + inputName);
        }
        mTracker.setScreenName(requestTypeString + " of " + inputName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("View")
                .setAction("View of different products")
                .build());
    }

//    public void refresh() {
//        //ivCrossFilter.setVisibility(View.GONE);
//        if (MyApplication.getInstance().getFilterData() != null) {
//            String filterData = MyApplication.getInstance().getFilterData();
//            if (filterData != null && filterData.length() > 0) {
//                try {
//                    productModelArrayList = new ArrayList<ProductModel>();
//                    JSONObject filterDataObj = new JSONObject(filterData);
//                    setSKUListData(filterDataObj);
//                } catch (Exception e) {
//
//                }
//            }
//        }
//    }

    public void getFilterData() {
        if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
            requestTypeAlert = "FilterData";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject jsonObject = new JSONObject();

                String segmentId = mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, "");

                jsonObject.put("segment_id", segmentId);
                jsonObject.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                switch (requestTypeString) {
                    case "SubCategories":
                        jsonObject.put("category_id", inputId);
                        jsonObject.put("manufacturer_id", "");
                        break;
                    case "FeaturedOffers":
                        jsonObject.put("category_id", inputId);
                        jsonObject.put("manufacturer_id", "");
                        break;
                    case "Manufacturers":
                        jsonObject.put("category_id", "");
                        jsonObject.put("manufacturer_id", inputId);
                        break;
                    case "BrandProducts":
                        jsonObject.put("category_id", "");
                        jsonObject.put("brand_id", inputId);
                        break;
                    case "Products":
                        jsonObject.put(keyId, inputId);
                        break;
                    default:
                        jsonObject.put("category_id", "");
                        jsonObject.put("manufacturer_id", inputId);
                        break;
                }

                if (requestTypeString.equalsIgnoreCase("Search")) {
                    jsonObject.put("search", getIntent().getStringExtra("Key"));
                } else {
                    jsonObject.put("search", "");
                }
                jsonObject.put("flag", "2");
                jsonObject.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                jsonObject.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                jsonObject.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                jsonObject.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                map.put("data", jsonObject.toString());

                VolleyBackgroundTask productDetailsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.filterDataURL, map,
                        SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.GET_FILTER_DATA_NEW);
                productDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(productDetailsRequest, AppURL.filterDataURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {

            }
        } else {
            requestTypeAlert = "FilterData";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    private void applyFilter() {
        isLoadMore = false;
//        currentOffset = 0;
        loadType = LoadType.APPLY_FILTER;
        if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                inputJson.put("offset", "20");
                inputJson.put("offset_limit", String.valueOf(offsetLimit));
                inputJson.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                inputJson.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                inputJson.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                inputJson.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                inputJson.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));

                map.put("data", inputJson.toString());

                VolleyBackgroundTask applyFilterRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.applyFilterURL, map, SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.APPLY_FILTER);
                applyFilterRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyApplication.getInstance().addToRequestQueue(applyFilterRequest, AppURL.applyFilterURL);
                if (dialog != null)
                    dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

//    @Override
//    public void onFilterClear() {
//        requestType = requestTypeTemp;
//        currentOffset = 0;
//        fabFilter.setVisibility(View.VISIBLE);
//        getData();
//    }
//
//    @Override
//    public void onFilterClose(String response, String responseType, JSONObject inputJSON, String type, String businessFilter, String productFilter) {
//        if (responseType != null) {
//            this.requestType = responseType;
//        } else {
//            this.requestType = "";
//        }
//        this.inputJson = inputJSON;
//        this.type = type;
//        this.businessFilter = businessFilter;
//        this.productFilter = productFilter;
//        fabFilter.setVisibility(View.VISIBLE);
//        if(inputJson!=null)
//            applyFilter();
////        refresh(response);
//    }

    public void filterLoadMore() {
        loadType = LoadType.LOAD_MORE;
        if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            try {
//                inputJson.put("offset", String.valueOf(currentOffset));
//                inputJson.put("offset_limit", String.valueOf(offsetLimit));
                inputJson.put("customer_type", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""));
                inputJson.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                inputJson.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                inputJson.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                inputJson.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                HashMap<String, String> map = new HashMap<>();
                if (inputJson != null && inputJson.length() > 0) {
                    map.put("data", inputJson.toString());
                }
                VolleyBackgroundTask applyFilterRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.applyFilterURL, map, SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.APPLY_FILTER);
                applyFilterRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyApplication.getInstance().addToRequestQueue(applyFilterRequest, AppURL.applyFilterURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestTypeAlert = "ApplyFilter";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

//    public void refresh(String response) {
//        //ivCrossFilter.setVisibility(View.GONE);
//
//        if (response != null && response.length() > 0) {
//            try {
//                //actionSlideExpandableListView.removeAllViews();
//                productModelArrayList = new ArrayList<ProductModel>();
//                expandableAdapter.notifyDataSetInvalidated();
//                expandableAdapter.removeAllItems();
//                JSONObject filterDataObj = new JSONObject(response);
//
//                setSKUListData(filterDataObj);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void addProductToWishList(String wishListId) {
        if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
            if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
                requestTypeAlert = "AddWishList";
                rlAlert.setVisibility(View.GONE);
                flMain.setVisibility(View.VISIBLE);
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("flag", "1");
                    obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                    obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                    obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                    obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                    obj.put("buyer_listing_id", wishListId);
                    obj.put("product_id", this.selectedProductId);

                    HashMap<String, String> map = new HashMap<>();
                    map.put("data", obj.toString());

                    VolleyBackgroundTask addWishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productListOperationsURL, map,
                            SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.ADDTO_WISHLIST);
                    addWishListRequest.setRetryPolicy(new DefaultRetryPolicy(
                            AppURL.initial_time_out,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    MyApplication.getInstance().addToRequestQueue(addWishListRequest, AppURL.productListOperationsURL);
                    if (dialog != null)
                        dialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                requestTypeAlert = "AddWishList";
                rlAlert.setVisibility(View.VISIBLE);
                flMain.setVisibility(View.GONE);
            }

        } else {
            Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.please_login_addto_wishlist), Toast.LENGTH_SHORT).show();
        }
    }

    private int getProductsInEaches(NewPacksModel selectedPackModel, String quantityEntered) {

        int packSize = selectedPackModel.getPackSize() == null ? 0 : Integer.parseInt(selectedPackModel.getPackSize());
        int enteredQuantity = TextUtils.isEmpty(quantityEntered) ? 0 : Integer.parseInt(quantityEntered);

        return packSize * enteredQuantity;
    }

    private void createPacksForVariant(final NewPacksModel skuPacksModel, final LinearLayout linearLayout, final long position) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (skuPacksModel != null) {
            View packView = inflater.inflate(R.layout.row_packs_header_supplier, null);

//            tvEsu = (TextView) packView.findViewById(R.id.tvEsu);
            final EditText etPackPrice = (EditText) packView.findViewById(R.id.etPackPrice);
            final TextView tvUnitPrice = (TextView) packView.findViewById(R.id.tvUnitPrice);
            final TextView tvMargin = (TextView) packView.findViewById(R.id.tvMargin);

            int packSize = 0;

            try {
                packSize = Integer.parseInt(skuPacksModel.getPackSize());
            } catch (Exception e) {
                packSize = 0;
            }

            esu = packSize;
//            tvEsu.setText(""+packSize);

            double packPrice = 0;
            try {
                packPrice = (Double.parseDouble(skuPacksModel.getPackSize()) * Double.parseDouble(skuPacksModel.getUnitPrice()));
            } catch (Exception e) {
                packPrice = 0;
            }

//            SupplierProductsActivity.this.packPrice = packPrice;
            etPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

            double unitPrice = 0;
            try {
                unitPrice = Double.parseDouble(skuPacksModel.getUnitPrice());
            } catch (Exception e) {
                unitPrice = 0;
            }

//            SupplierProductsActivity.this.unitPrice = unitPrice;
            tvUnitPrice.setText(skuPacksModel.getUnitPrice() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", unitPrice));

            double margin = 0;
            try {
                margin = Double.parseDouble(skuPacksModel.getMargin());
            } catch (Exception e) {
            }
            tvMargin.setText(skuPacksModel.getMargin() == null ? "--/-" : String.format(Locale.CANADA, "%.2f", margin) + "%");

            final TextView tvQty = (TextView) packView.findViewById(R.id.tvQuantity);
            tvQty.setText(skuPacksModel.getEsuQty() + "");
            ImageButton ibMinus = (ImageButton) packView.findViewById(R.id.ibMinus);
            ImageButton ibPlus = (ImageButton) packView.findViewById(R.id.ibPlus);

//            cbFreebie.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        isFreebieChecked = true;
//                        etPackPrice.setText("0.00");
//                        tvUnitPrice.setText("0.00");
//                        etPackPrice.setEnabled(false);
//                        SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n0.00");
//                        setSpannedText(sb, tvTotalPrice, 11);
//                        SpannableStringBuilder sb1 = new SpannableStringBuilder(getResources().getString(R.string.markup_margin) + "\n0.00%");
//                        setSpannedText(sb1, tvTotalMargin, 8);
//                    } else {
//                        isFreebieChecked = false;
//                        etPackPrice.setText(String.format(Locale.CANADA, "%.2f", SupplierProductsActivity.this.packPrice));
//                        tvUnitPrice.setText(String.format(Locale.CANADA, "%.2f", SupplierProductsActivity.this.unitPrice));
//                        if (tvTotalMargin != null) {
//                            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.markup_margin) + "\n" + String.format(Locale.CANADA, "%.2f", totalMargin) + "%");
//                            setSpannedText(sb, tvTotalMargin, 8);
//                        }
//                        if (tvTotalPrice != null) {
//                            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
//                            setSpannedText(sb, tvTotalPrice, 11);
//
//                        }
//
//                        etPackPrice.setEnabled(true);
//                        etPackPrice.setSelection(etPackPrice.getText().toString().length());
//                    }
//                }
//            });

            etPackPrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s != null && !isFreebieChecked) {
                        double unitPrice = 0, packPrice = 0, packSize = 0, mrp = 0;
                        try {
                            mrp = Double.parseDouble(selectedProductModel.getMrp());
                            packPrice = Double.parseDouble(s.toString());
                            unitPrice = Double.parseDouble(selectedPackModel.getUnitPrice());

//                            if (unitPrice > mrp) {
//
//                                Toast.makeText(SupplierProductsActivity.this, "enter price less than mrp", Toast.LENGTH_SHORT).show();
//                                etPackPrice.setText(String.valueOf(unitPrice * esu));
//                                etPackPrice.setSelection(etPackPrice.getText().toString().length());
//                                packPrice = unitPrice * esu;
//                                return;
//                            }
//                            packSize = Double.parseDouble(skuPacksModel.getPackSize());
                            if (esu != 0)
                                unitPrice = Double.parseDouble(s.toString()) / esu;
                            else
                                unitPrice = 0;
                        } catch (Exception e) {
                            packPrice = 0;
                            unitPrice = 0;
                        }
                        selectedPackModel.setUnitPrice(unitPrice + "");
//                        SupplierProductsActivity.this.packPrice = packPrice;
//                        SupplierProductsActivity.this.unitPrice = unitPrice;
                        tvUnitPrice.setText(String.format(Locale.CANADA, "%.2f", unitPrice));
                        updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                        /*totalPrice = unitPrice * totalQtyToSend;
                        if (tvTotalPrice != null) {
                            SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.total_price) + "\n" + String.format(Locale.CANADA, "%.2f", totalPrice));
                            setSpannedText(sb, tvTotalPrice, 11);

                        }*/
                    } else {
                        tvUnitPrice.setText("");
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            ibPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                        Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.please_login_add_packs), Toast.LENGTH_LONG).show();
                        return;
                    }
                    int qty = skuPacksModel.getQty();
                    int esuQty = skuPacksModel.getEsuQty();
//                            int qtyToSent = skuPacksModel.getQtyToBeSent();

                    int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                            0 : Integer.parseInt(skuPacksModel.getPackSize());
//                            int packSize = esuQty;
//                            try {
//                                packSize = Integer.parseInt(tvEsu.getText().toString());
//                            } catch (Exception e) {
//                                packSize = 0;
//                            }
                    int updatedQty = qty + packSize;
//                            int qtyToBeSent = qtyToSent + 1;
                    esuQty = esuQty + 1;
                    tvQty.setText("" + esuQty);
                    skuPacksModel.setQty(updatedQty);
                    skuPacksModel.setEsuQty(esuQty);
//                            skuPacksModel.setQtyToBeSent(qtyToBeSent);
                    double packPrice = 0;
                    try {
                        packPrice = esu * Double.parseDouble(skuPacksModel.getUnitPrice());
                    } catch (Exception e) {
                        packPrice = 0;
                    }

//                    SupplierProductsActivity.this.packPrice = packPrice;
//                    String _packPrice = String.format(Locale.CANADA, "%.2f", packPrice);
//                    etPackPrice.setText(_packPrice);

                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
//                        } else {
//                            Utils.showAlertDialog(SupplierProductsActivity.this, getResources().getString(R.string.quantity_not_available));
//                        }
//                    }


                }
            });

            ibMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                        Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.please_login_add_packs), Toast.LENGTH_LONG).show();
                        return;
                    }
//                    String strQty = tvQty.getText().toString().trim();
                    int qty = skuPacksModel.getQty();
                    int esuQty = skuPacksModel.getEsuQty();
//                    int qtyToSent = skuPacksModel.getQtyToBeSent();

                    if (esuQty <= 0) {
                        tvQty.setText("0");
                    } else {

                        int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                0 : Integer.parseInt(skuPacksModel.getPackSize());
//                        int packSize = esuQty;
//                        try {
//                            packSize = Integer.parseInt(tvEsu.getText().toString());
//                        } catch (Exception e) {
//                            packSize = 0;
//                        }
                        int updatedQty = qty - packSize;
//                        int qtyToBeSent = qtyToSent - 1;
                        esuQty = esuQty - 1;
                        int quantityInEaches = getProductsInEaches(selectedPackModel, esuQty + "");
                        int freeQuantityInEachesPrev = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

                        if (quantityInEaches <= freeQuantityInEachesPrev) {
                            Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (esuQty < 0) {
                            tvQty.setText("0");
                        } else {
                            tvQty.setText("" + esuQty);
                        }
                        skuPacksModel.setQty(updatedQty);
                        skuPacksModel.setEsuQty(esuQty);
//                        skuPacksModel.setQtyToBeSent(qtyToBeSent);

                        double packPrice = 0;
                        try {
                            packPrice = esu * Double.parseDouble(skuPacksModel.getUnitPrice());
                        } catch (Exception e) {
                            packPrice = 0;
                        }

//                        SupplierProductsActivity.this.packPrice = packPrice;
//                        etPackPrice.setText(String.format(Locale.CANADA, "%.2f", packPrice));

                    }
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                }
            });

            linearLayout.addView(packView);

            View freebieView = inflater.inflate(R.layout.row_freebie, null);
            ImageButton ibFbPlus = (ImageButton) freebieView.findViewById(R.id.ibFbPlus);
            ImageButton ibFbMinus = (ImageButton) freebieView.findViewById(R.id.ibFbMinus);
            final Spinner packTypeSpinner = (Spinner) freebieView.findViewById(R.id.packs_spinner);
            packTypeSpinner.setAdapter(new PackTypesSpinnerAdapter(SupplierProductsActivity.this, arrSlabRates));
            packTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (view.getTag() != null && view.getTag() instanceof NewPacksModel) {
                        prevPos = currentPos;
                        currentPos = position;
                        selectedFreePackModelPrev = selectedFreePackModel;
                        selectedFreePackModel = (NewPacksModel) view.getTag();

//                        int freeQtyInESU = skuPacksModel.getFreeQty();
                        int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

                        int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, tvQty.getText().toString());

                        if (freeQuantityInEaches > 0 && mainProductQtyInEaches <= freeQuantityInEaches) {
                            Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                            packTypeSpinner.setSelection(prevPos);
                            selectedFreePackModel = selectedFreePackModelPrev;
                            return;
                        }


                        updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            tvFbQty = (TextView) freebieView.findViewById(R.id.tvFbQuantity);
            tvFbQty.setText("0");

            ibFbPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int freeQtyInESU = skuPacksModel.getFreeQty();
                    int freeQuantityInEachesPrev = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

                    int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, tvQty.getText().toString());

                    if (mainProductQtyInEaches <= freeQuantityInEachesPrev) {
                        Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int packSize = TextUtils.isEmpty(selectedFreePackModel.getPackSize()) ?
                            0 : Integer.parseInt(selectedFreePackModel.getPackSize());

                    int fbQty = freeQtyInESU + 1;
                    int fbQtyTotal = freeQuantityInEachesPrev + packSize;

                    if (mainProductQtyInEaches <= fbQtyTotal) {
                        Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    skuPacksModel.setFreeQty(fbQty);
                    skuPacksModel.setFreeQtyTotal(fbQtyTotal);

                    tvFbQty.setText("" + fbQty);
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);

                    /*int qty = skuPacksModel.getFreeQty();
                    int qtyTotal = skuPacksModel.getFreeQtyTotal();

                    if (qty < selectedPackModel.getQtyToBeSent()) {

                        int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                0 : Integer.parseInt(skuPacksModel.getPackSize());

                        int fbQty = qty + 1;
                        int fbQtyTotal = qtyTotal + packSize;
                        skuPacksModel.setFreeQty(fbQty);
                        skuPacksModel.setFreeQtyTotal(fbQtyTotal);
                        tvFbQty.setText("" + fbQty);
                        updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                    } else {
                        Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
                    }*/

                }
            });

            ibFbMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    int freeQtyInESU = skuPacksModel.getFreeQty();

                    if (freeQtyInESU <= 0) {
                        tvFbQty.setText("0");
                        return;
                    }

                    int freeQuantityInEaches = getProductsInEaches(selectedFreePackModel, tvFbQty.getText().toString());

//                    int mainProductQtyInEaches = getProductsInEaches(selectedPackModel, tvQty.getText().toString());
//                    if(mainProductQtyInEaches <= freeQuantityInEaches){
//                        Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.freeqty_grt_qty), Toast.LENGTH_SHORT).show();
//                        return;
//                    }

                    int packSize = TextUtils.isEmpty(selectedFreePackModel.getPackSize()) ?
                            0 : Integer.parseInt(selectedFreePackModel.getPackSize());

                    int fbQty = freeQtyInESU - 1;
                    int fbQtyTotal = freeQuantityInEaches - packSize;

                    skuPacksModel.setFreeQty(fbQty);
                    skuPacksModel.setFreeQtyTotal(fbQtyTotal);

                    tvFbQty.setText("" + fbQty);
                    updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);

                    /*int qty = skuPacksModel.getFreeQty();
                    int qtyTotal = skuPacksModel.getFreeQtyTotal();
                    if (qty <= 0) {
                        tvFbQty.setText("0");
                    } else {

                        int packSize = TextUtils.isEmpty(skuPacksModel.getPackSize()) ?
                                0 : Integer.parseInt(skuPacksModel.getPackSize());

                        int fbQty = qty - 1;
                        int fbQtyTotal = qtyTotal - packSize;
                        skuPacksModel.setFreeQty(fbQty);
                        skuPacksModel.setFreeQtyTotal(fbQtyTotal);
                        tvFbQty.setText("" + fbQty);
                        updateFooter(linearLayout.findViewById(R.id.footer), skuPacksModel);
                    }*/

                }
            });
            linearLayout.addView(freebieView);

        }

        View footerView = inflater.inflate(R.layout.row_packs_footer, null);
        linearLayout.addView(footerView);

        footerView.findViewById(R.id.tvAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OnCartItemAdded(expandedProductModel);

            }
        });
        updateFooter(footerView, skuPacksModel);
    }

    @Override
    public void OnImageClicked(int position, String rating) {
//        Intent intent = new Intent(SupplierProductsActivity.this, ProductDetailsActivity.class);
//        intent.putExtra("ProductId", TextUtils.isEmpty(productId) ? productsArrayList.get(position).getProductId() : productId);
//        intent.putExtra("ParentId", productsArrayList.get(position).getProductId());
//        intent.putExtra("position", position + "");
//        startActivityForResult(intent, 12);
    }

    @Override
    public void OnPriceHistoryClicked(int position, Object object) {

        if (object instanceof TestProductModel) {
            TestProductModel testProductModel = (TestProductModel) object;
            String parentId = testProductModel.getProductId();
            String productId = this.productId;
            if (!parentId.equalsIgnoreCase(DBHelper.getInstance().getParentId(productId))) {
                productId = testProductModel.getProductId();
            }
            getPriceHistory(productId);
        }
    }

    private void getPriceHistory(String productId) {
        try {
            JSONObject obj = new JSONObject();
            PARSER_TYPE parserType = PARSER_TYPE.PRICE_HISTORY;
            obj.put("product_id", productId);
            obj.put("supplier_id", mSharedPreferences.getString(ConstantValues.KEY_SEL_SUPPLIER_ID, ""));
            obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
            obj.put("srm_token", mSharedPreferences.getString(ConstantValues.KEY_SRM_TOKEN, ""));

            HashMap<String, String> map = new HashMap<>();
            map.put("data", obj.toString());

            VolleyBackgroundTask productsRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.getPurchasePriceHistoryURL, map,
                    SupplierProductsActivity.this, SupplierProductsActivity.this, parserType);
            productsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(productsRequest, AppURL.getPurchasePriceHistoryURL);

            if (dialog != null)
                dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int position = 0;
        if (requestCode == 12 && resultCode == RESULT_OK) {
            if (data != null) {
                if (data.hasExtra("position")) {
                    position = Integer.parseInt(data.getStringExtra("position"));
                }
                if (data.hasExtra("rating")) {
                    String rating = data.getStringExtra("rating");
//                    productModelArrayList.get(position).setProductRating(rating);
                    expandableAdapter.notifyDataSetInvalidated();
                }
            }
        } else if (requestCode == 22 && resultCode == RESULT_OK) {
            if (data != null) {
                String responseType = "";
                if (data.hasExtra("clear")) {
                    isClear = data.getBooleanExtra("clear", false);
                }
                if (data.hasExtra("requestType")) {
                    responseType = data.getStringExtra("requestType");
                }
                if (data.hasExtra("inputJson")) {
                    String inputString = data.getStringExtra("inputJson");
                    try {
                        inputJson = new JSONObject(inputString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (data.hasExtra("type")) {
                    type = data.getStringExtra("type");
                }
                if (isClear) {
                    getData(false);
                } else {
                    if (responseType != null) {
                        this.requestTypeTemp = responseType;
                    } else {
                        this.requestTypeTemp = "";
                    }
                    if (inputJson != null)
                        applyFilter();
                }
            }
        }
    }

    public void showWishListDialog() {
        FragmentManager fm = getSupportFragmentManager();
        WishListFragment editNameDialog = WishListFragment.newInstance(wishListArray);
        editNameDialog.setCancelable(true);
        editNameDialog.setConfirmListener(confirmListener);
        editNameDialog.show(fm, "areas_fragment_dialog");
    }

    public void deleteProductFromWishList(String selectedProductId) {
        if (Networking.isNetworkAvailable(SupplierProductsActivity.this)) {
            requestTypeAlert = "DeleteWishList";
            rlAlert.setVisibility(View.GONE);
            flMain.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject();
                obj.put("flag", "3");
                obj.put("customer_token", mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_TOKEN, ""));
                obj.put("buyer_listing_id", this.buyerListingId);
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_SRM_WH_ID, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
                obj.put("product_id", selectedProductId);

                HashMap<String, String> map = new HashMap<>();
                map.put("data", obj.toString());

                VolleyBackgroundTask delWishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.productListOperationsURL, map,
                        SupplierProductsActivity.this, SupplierProductsActivity.this, PARSER_TYPE.DELETE_PRODUCTFROM_WISHLIST);
                delWishListRequest.setRetryPolicy(new DefaultRetryPolicy(
                        AppURL.initial_time_out,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MyApplication.getInstance().addToRequestQueue(delWishListRequest, AppURL.productListOperationsURL);
                if (dialog != null)
                    dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestTypeAlert = "DeleteWishList";
            rlAlert.setVisibility(View.VISIBLE);
            flMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void hideAndShowProductsListViewBasedOnProductsCount() {
        if (productsArrayList.size() > 0) {
            actionSlideExpandableListView.setVisibility(View.VISIBLE);
            tvNoItems.setVisibility(View.GONE);
            expandableAdapter.notifyDataSetChanged();
        } else {
            actionSlideExpandableListView.setVisibility(View.INVISIBLE);
            tvNoItems.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if (error != null)
//            Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object results, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing() && requestType != PARSER_TYPE.GET_SORT_DATA)
            dialog.dismiss();
        if (llFooter.getVisibility() == View.VISIBLE) {
            llFooter.setVisibility(View.GONE);
        }
        if (results != null) {
            if (status.equalsIgnoreCase("success")) {
                if (requestType == PARSER_TYPE.GET_FILTER_DATA_NEW) {

                    if (results instanceof JSONObject) {
                        JSONObject dataObj = (JSONObject) results;
                        filterString = dataObj.toString();

                        Intent intent = new Intent(SupplierProductsActivity.this, FilterActivity.class);
                        intent.putExtra("FilterString", filterString);
                        intent.putExtra("Type", requestTypeString);
                        intent.putExtra("key_id", keyId);
                        intent.putExtra("Id", inputId);
                        intent.putExtra("displayStar", false);
                        startActivityForResult(intent, 22);
                    }
                } else if (requestType == PARSER_TYPE.GET_SORT_DATA) {
                    if (results instanceof JSONArray) {
                        JSONArray dataArray = (JSONArray) results;
                        sortString = dataArray.toString();
                    }
                } else if (requestType == PARSER_TYPE.WISHLIST) {

                    if (results instanceof ArrayList) {
                        wishListArray = (ArrayList<WishListModel>) results;
                        if (wishListArray != null && wishListArray.size() > 0) {
                            showWishListDialog();
                        } else {
                            Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.please_create_list), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (requestType == PARSER_TYPE.ADDTO_WISHLIST) {
                    Utils.showAlertDialog(SupplierProductsActivity.this, message);
                } else if (requestType == PARSER_TYPE.DELETE_PRODUCTFROM_WISHLIST) {

//                    if (productModelArrayList.contains(selectedWishListProductModel)) {
//                        productModelArrayList.remove(selectedWishListProductModel);
//                        expandableAdapter.removeItem(selectedWishListProductModel);
//                        hideAndShowProductsListViewBasedOnProductsCount();
//                    }
                } else if (requestType == PARSER_TYPE.PRICE_HISTORY) {
                    if (results instanceof PriceHistoryModel) {
                        PriceHistoryModel priceHistoryModel = (PriceHistoryModel) results;
                        ArrayList<NewPacksModel> arrSlabRates = priceHistoryModel.getArrPrices();
                        if (arrSlabRates != null && arrSlabRates.size() > 0) {
                            FragmentManager fm = getSupportFragmentManager();
                            PriceHistoryFragment editNameDialog = PriceHistoryFragment.newInstance(priceHistoryModel);
                            editNameDialog.setCancelable(true);
                            editNameDialog.show(fm, "price_fragment_dialog");
                        } else {
                            Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.no_price_history), Toast.LENGTH_SHORT).show();
                        }

                    }
                } else if (requestType == PARSER_TYPE.PURCHASE_SLABS) {

                    if (results instanceof ArrayList) {
                        if (progress != null)
                            progress.setVisibility(View.GONE);
                        prevClickedProductId = "";
                        arrSlabRates = (ArrayList<NewPacksModel>) results;

                        if (expandedItemView != null) {
                            LinearLayout slabRatesLayout = (LinearLayout) expandedItemView.findViewById(R.id.slab_rates);
                            slabRatesLayout.removeAllViews();
                            if (arrSlabRates != null && arrSlabRates.size() > 0) {
                                tvNoPacks.setVisibility(View.GONE);
                                View slabRatesView = inflater.inflate(R.layout.layout_horizontal_scrollview, null);
                                slabsLayout = (LinearLayout) slabRatesView.findViewById(R.id.variants_layout);
                                if (slabsLayout != null) {
                                    slabsLayout.removeAllViews();

                                    int selectedPos = 0;
                                    for (int i = 0; i < arrSlabRates.size(); i++) {
                                        selectedPackModel = arrSlabRates.get(i);
                                        String slab = arrSlabRates.get(i).getLevelName();
                                        TextView childVariantLabel = createSlabRates(slab + "(" + arrSlabRates.get(i).getNoOfEaches() + ")", R.id.id_slab_qty);
                                        childVariantLabel.setTag(arrSlabRates.get(i));
                                        childVariantLabel.setOnClickListener(onVariantClickListener);
                                        slabsLayout.addView(childVariantLabel);

                                        if (selectedPackModel.isSelected())
                                            selectedPos = i;

                                    }

                                    slabRatesLayout.addView(slabRatesView);

                                    if (slabsLayout.getChildCount() > 0) {
                                        slabsLayout.getChildAt(selectedPos).performClick();
                                        slabsLayout.getChildAt(selectedPos).setSelected(true);
                                    }


                                }
                            } else {
                                if (tvNoPacks != null)
                                    tvNoPacks.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                } else if (requestType == PARSER_TYPE.SRM_PRODUCTS) {
                    if (results instanceof ArrayList) {

                        ArrayList<TestProductModel> tempList = new ArrayList<>();
                        if (!isFreebie) {
                            ArrayList<String> missedParentList = new ArrayList<>();
                            if (loadType == LoadType.SORT) {
                                tempList = DBHelper.getInstance().getProductsByProductIds(allProductIds, false, false, "", "");
                                missedParentList = DBHelper.getInstance().getParentNotAvailableProductIds(allProductIds);
                            } else {
                                tempList = DBHelper.getInstance().getProductsByProductIds(unAvailableProducts, false, false, "", "");
                                missedParentList = DBHelper.getInstance().getParentNotAvailableProductIds(unAvailableProducts);
                            }
//                        productModelArrayList.addAll(tempList);
                            String ids = TextUtils.join(",", missedParentList);
                            tempList.addAll(DBHelper.getInstance().getProductsByProductIds(ids, true, false, "", ""));
                            if (tempList.size() > 0)
                                setSKUListData(tempList);
                        }
                    }
                } else {
                    if (results instanceof ProductsResponse) {
                        if (swipeRefreshLayout != null)
                            swipeRefreshLayout.setRefreshing(false);
//                        currentOffset += offsetLimit;

                        productsResponse = (ProductsResponse) results;
//                        if (loadType != LoadType.LOAD_MORE)
                        totalItemsCount = productsResponse.getTotalItemsCount();
                        allProductIds = productsResponse.getProductIds();
                        DBHelper.getInstance().insertProductInTemp(allProductIds);
                        ArrayList<TestProductModel> productTempList = DBHelper.getInstance().getProductsByProductIds(productsResponse.getProductIds(), false, false, "", "");
                        unAvailableProducts = DBHelper.getInstance().getNotAvailableProductIds();

                        ArrayList<String> missedParentList = DBHelper.getInstance().getParentNotAvailableProductIds(allProductIds);
//                        ArrayList<TestProductModel> parentUnavailableProducts = new ArrayList<>();
                        String ids = TextUtils.join(",", missedParentList);
                        productTempList.addAll(DBHelper.getInstance().getProductsByProductIds(ids, true, false, "", ""));

                        if (productTempList.size() > 0 /*&& TextUtils.isEmpty(unAvailableProducts)*/) {
                            setSKUListData(productTempList);
                        }

                        if (!TextUtils.isEmpty(unAvailableProducts)) {
                            isFreebie = false;
                            getProducts(unAvailableProducts, isFreebie);
                        }

                        if (totalItemsCount == 0) {
                            actionSlideExpandableListView.setVisibility(View.INVISIBLE);
                            tvNoItems.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } else {
                Utils.showAlertWithMessage(SupplierProductsActivity.this, message);
            }
        } else {
            Utils.showAlertWithMessage(SupplierProductsActivity.this, getResources().getString(R.string.something_wrong));
        }
    }

    @Override
    public void onSessionError(String message) {
        Utils.logout(SupplierProductsActivity.this, message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_supplier, menu);
        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        Utils.setBadgeCount(SupplierProductsActivity.this, icon, cartCount);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cart) {

            if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                startActivity(new Intent(SupplierProductsActivity.this, POCartActivity.class));
            } else {
                Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.please_login_view_cart), Toast.LENGTH_LONG).show();
            }

        } else if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.notification) {
            if (mSharedPreferences.getBoolean(ConstantValues.KEY_IS_LOGGED_IN, false)) {
                startActivity(new Intent(SupplierProductsActivity.this, NotificationsActivity.class));
            } else {
                Toast.makeText(SupplierProductsActivity.this, getResources().getString(R.string.please_login_view_notifications), Toast.LENGTH_LONG).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private enum LoadType {DEFAULT, SORT, APPLY_FILTER, LOAD_MORE, SWIPEDOWN}

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
