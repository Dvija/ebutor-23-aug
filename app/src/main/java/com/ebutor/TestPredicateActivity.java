package com.ebutor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.internal.util.Predicate;
import com.ebutor.models.BrandModel;
import com.ebutor.models.CategoryModel;
import com.ebutor.models.ManufacturerModel;
import com.ebutor.models.TestModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Created by Srikanth Nama on 12-Aug-16.
 */
public class TestPredicateActivity extends AppCompatActivity {

    List<ManufacturerModel> manufacturerModelList;
    List<BrandModel> brandModelList;
    List<CategoryModel> categoryModelList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manufacturerModelList = new ArrayList<>();
        brandModelList = new ArrayList<>();
        categoryModelList = new ArrayList<>();


        String response = readFromFile("filter.json", TestPredicateActivity.this);
        parseResponse(response);



    }

    private void parseResponse(String response) {
        try{
            JSONObject object = new JSONObject(response);
            JSONObject dataObj = object.optJSONObject("data");
            JSONArray manufacturesArray = dataObj.optJSONArray("manufacturers");
            for (int i=0; i<manufacturesArray.length(); i++){
                JSONObject obj = manufacturesArray.optJSONObject(i);
                ManufacturerModel model = new ManufacturerModel();
                model.setManufacturerId(obj.optString("manufacturer_id"));
                model.setManufacturerName(obj.optString("manufacturer_name"));

                manufacturerModelList.add(model);
            }

            JSONArray brandArray = dataObj.optJSONArray("brand");
            for (int i=0; i<brandArray.length(); i++){
                JSONObject obj = brandArray.optJSONObject(i);
                BrandModel model = new BrandModel();
                model.setManufacturerId(obj.optString("manufacturer_id"));
                model.setBrandName(obj.optString("brand_name"));
                model.setBrandId(obj.optString("brand_id"));
                model.setCategoryId(obj.optString("category_id"));

                brandModelList.add(model);
            }

            JSONArray categoryArray = dataObj.optJSONArray("categories");
            for (int i=0; i<categoryArray.length(); i++){
                JSONObject obj = categoryArray.optJSONObject(i);
                CategoryModel model = new CategoryModel();
                model.setManufacturerId(obj.optString("manf_id"));
                model.setCategoryName(obj.optString("category_name"));
                model.setBrandId(obj.optString("brand_id"));
                model.setCategoryId(obj.optString("category_id"));

                categoryModelList.add(model);
            }


            /*Predicate<BrandModel> isAuthorized = user -> {
                // binds a boolean method in User to a reference
                return user.getCategoryId().contains("175");
            };
            Predicate<TestModel> isCategory = user -> {
                // binds a boolean method in User to a reference

                return user.getIds()!=null && user.getIds().contains("20");
            };*/


            Predicate<BrandModel> isAuthorized = new Predicate<BrandModel>() {
                @Override
                public boolean apply(BrandModel brandModel) {
                    return brandModel.getCategoryId().contains("175");
                }
            };

            Predicate<TestModel> isCategory = new Predicate<TestModel>() {
                @Override
                public boolean apply(TestModel testModel) {
                    return testModel.getIds()!=null && testModel.getIds().contains("20");
                }
            };

//             allUsers is a Collection<User>
            Collection<BrandModel> authorizedUsers = filter(brandModelList, isAuthorized);
            if(authorizedUsers!=null){
                for (BrandModel model :
                        authorizedUsers) {
                    Log.e("Brand ", model.getCategoryId() + "  " + model.getBrandId());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
    public static Collection<BrandModel> filter(Collection<BrandModel> target, Predicate<BrandModel> predicate) {
        Collection<BrandModel> result = new ArrayList<BrandModel>();
        for (BrandModel element: target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }
    public interface IPredicate<T> { boolean apply(T type); }


    public String readFromFile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }
}
