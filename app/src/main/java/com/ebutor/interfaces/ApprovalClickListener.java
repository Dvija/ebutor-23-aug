package com.ebutor.interfaces;

/**
 * Created by Srikanth Nama on 09-Jan-17.
 */


public interface ApprovalClickListener {
    void onApprove(String currentStatus, String amount, String status, String comments, String isFinalStep, String tallyLedgerName);
    void onCancel();
}
