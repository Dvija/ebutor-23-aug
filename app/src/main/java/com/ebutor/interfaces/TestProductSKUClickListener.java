package com.ebutor.interfaces;

/**
 * Created by Srikanth Nama on 3/18/2016.
 */
public interface TestProductSKUClickListener {
    void OnProductSKUClicked(Object object, long position);
    void OnCartItemAdded(Object model);
    void OnItemOptionClicked(Object model, int type);
}
