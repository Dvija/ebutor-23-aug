package com.ebutor.interfaces;

/**
 * Created by Srikanth Nama on 3/18/2016.
 */
public interface ProductSKUClickListener {
    void OnProductSKUClicked(Object object, long position,boolean isChild);
    void OnCartItemAdded(Object model);
    void OnItemOptionClicked(Object model, int type);
    void onSKUClicked(String productId);
}
