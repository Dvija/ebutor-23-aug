package com.ebutor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ebutor.backgroundtask.VolleyBackgroundTask;
import com.ebutor.callbacks.VolleyHandler;
import com.ebutor.database.DBHelper;
import com.ebutor.models.BusinessTypeModel;
import com.ebutor.models.CategoryModel;
import com.ebutor.models.CustomerTyepModel;
import com.ebutor.models.MasterLookUpModel;
import com.ebutor.utils.AppURL;
import com.ebutor.utils.ConstantValues;
import com.ebutor.utils.Networking;
import com.ebutor.utils.PARSER_TYPE;
import com.ebutor.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by 300024 on 3/4/2016.
 */
public class SkipSignupActivity extends ParentActivity implements Response.ErrorListener, VolleyHandler<Object> {
    private static final int TIME_INTERVAL = 2000;
    public static Activity instnace;
    TextView tvSkipExplore, tvNext, tvAlertMsg;
    LinearLayout llSignup;
    DBHelper dataBaseObj;
    Spinner customerTypeSpinner, businessTypeSpinner;
    ArrayList<BusinessTypeModel> businessTyepArr;
    private ArrayList<CustomerTyepModel> customerTypesArr, volumesArr, licenseArr;
    private ArrayList<CategoryModel> arrCategories;
    private RelativeLayout rlAlert, rlMain;
    private SharedPreferences mSharedPreferences;
    private long mBackPressed;
    private Tracker mTracker;
    private String requestType = "", customerGrpId = "", segmentId = "";
    private Dialog dialog;
    private boolean isHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skip_signup);
        instnace = this;

        dialog = Utils.createLoader(SkipSignupActivity.this, ConstantValues.PROGRESS);

        tvSkipExplore = (TextView) findViewById(R.id.tv_skip_explore);
        tvNext = (TextView) findViewById(R.id.tv_next);

        llSignup = (LinearLayout) findViewById(R.id.ll_signup);
        rlMain = (RelativeLayout) findViewById(R.id.rl_main);
        tvAlertMsg = (TextView) findViewById(R.id.tv_alert_msg);
        rlAlert = (RelativeLayout) findViewById(R.id.rl_alert);
        customerTypeSpinner = (Spinner) findViewById(R.id.spinner_buyer_type);
        businessTypeSpinner = (Spinner) findViewById(R.id.spinner_business_type);

        MyApplication application = (MyApplication) getApplication();
//Calling getDefaultTracker method.It returns tracker
        mTracker = application.getDefaultTracker();

        dataBaseObj = new DBHelper(SkipSignupActivity.this);
//        Spanned string = Html.fromHtml("Skip & Explore>>");
//        tvSkipExplore.setText(string);

        mSharedPreferences = getSharedPreferences(getResources().getString(R.string.app_name), MODE_PRIVATE);

        getMasterLookUp();

        customerTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                customerGrpId = customerTypesArr.get(i).getCustomerGrpId();
                if (customerGrpId != null && customerGrpId.length() > 0) {
                    editor.putString(ConstantValues.KEY_CUSTOMER_GRP_ID, customerGrpId);
                    editor.apply();
                    mSharedPreferences.edit().putString(ConstantValues.CUSTOMER_GRP_ID, customerGrpId).apply();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        businessTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                segmentId = businessTyepArr.get(i).getSegmentId();
                if (segmentId != null && segmentId.length() > 0) {
                    editor.putInt("position", i);
                    editor.putString(ConstantValues.KEY_SEGMENT_ID, segmentId);
                    editor.apply();
                    mSharedPreferences.edit().putString(ConstantValues.SEGMENT_ID, segmentId).apply();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tvAlertMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Networking.isNetworkAvailable(SkipSignupActivity.this)) {
                    switch (requestType) {
                        case "getCategories":
                            loadCategories();
                            break;
                        case "GetSegments":
                            getMasterLookUp();
                            break;
                        default:
                            getMasterLookUp();
                            break;
                    }
                } else {
                    Toast.makeText(SkipSignupActivity.this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

            }
        });
        tvSkipExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isHome = true;
                loadCategories();
            }
        });
        llSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isHome = false;
                loadCategories();

            }
        });

    }

    private void loadCategories() {
        if (TextUtils.isEmpty(customerGrpId) || customerGrpId.equalsIgnoreCase(getResources().getString(R.string.select_customer_type))) {
            Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.please_select_customer_type));
        } else if (TextUtils.isEmpty(segmentId) || segmentId.equalsIgnoreCase(getResources().getString(R.string.select_segment))) {
            Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.please_select_segment));
        } else {
            getCategories();
        }
    }

    private void getCategories() {

        if (Networking.isNetworkAvailable(SkipSignupActivity.this)) {
            requestType = "getCategories";
            rlAlert.setVisibility(View.GONE);
            rlMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                obj.put("hub_id", mSharedPreferences.getString(ConstantValues.KEY_HUB_ID, "0"));
                obj.put("pincode", mSharedPreferences.getString(ConstantValues.KEY_PINCODE, ""));
                obj.put("le_wh_id", mSharedPreferences.getString(ConstantValues.KEY_WH_IDS, ""));
                obj.put("segment_id", mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""));
//                obj.put("segment_id", "48001");
                map.put("data", obj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.categoriesURL, map, SkipSignupActivity.this, SkipSignupActivity.this, PARSER_TYPE.GET_MAIN_CATEGORIES);
            wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.categoriesURL);
            if (dialog != null)
                dialog.show();
        } else {
            requestType = "getCategories";
            rlAlert.setVisibility(View.VISIBLE);
            rlMain.setVisibility(View.GONE);
        }

    }

    private void getMasterLookUp() {

        if (Networking.isNetworkAvailable(SkipSignupActivity.this)) {
            requestType = "GetSegments";
            rlAlert.setVisibility(View.GONE);
            rlMain.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<>();
            try {
                JSONObject obj = new JSONObject();
                map.put("data", obj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            VolleyBackgroundTask wishListRequest = new VolleyBackgroundTask(Request.Method.POST, AppURL.masterLookUpURL, map, SkipSignupActivity.this, SkipSignupActivity.this, PARSER_TYPE.GET_MASTER_LOOKUP);
            wishListRequest.setRetryPolicy(new DefaultRetryPolicy(AppURL.initial_time_out,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApplication.getInstance().addToRequestQueue(wishListRequest, AppURL.masterLookUpURL);
            if (dialog != null)
                dialog.show();
        } else {
            requestType = "GetSegments";
            rlAlert.setVisibility(View.VISIBLE);
            rlMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(SkipSignupActivity.this, getResources().getString(R.string.press_once_again_exit), Toast.LENGTH_SHORT).show();

        }

        mBackPressed = System.currentTimeMillis();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Skip Signup Page");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (error != null) {
                if (error instanceof NetworkError) {
                    Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.network_error));
                } else if (error instanceof ServerError) {
                    Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.server_error));
                } else if (error instanceof AuthFailureError) {
                    Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.auth_error));
                } else if (error instanceof NoConnectionError) {
                    Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.no_network_connection));
                } else if (error instanceof TimeoutError) {
                    Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.time_out_error));
                } else {
                    Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.server_error));
                }
            } else {
                Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.server_error));
            }
        } catch (Exception e) {

        }
//        if (error != null)
//            Utils.showAlertWithMessage(SkipSignupActivity.this, getResources().getString(R.string.unexpected_error));
    }

    @Override
    public void onResponse(Object response, PARSER_TYPE requestType, String status, String message) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        if (response != null) {
            try {
                if (response != null) {

                    if (requestType == PARSER_TYPE.GET_MASTER_LOOKUP) {
                        if (status.equalsIgnoreCase("success")) {

                            if (response instanceof MasterLookUpModel) {
                                MasterLookUpModel masterLookUpModel = (MasterLookUpModel) response;
                                businessTyepArr = masterLookUpModel.getBusinessTypeArr();
                                customerTypesArr = masterLookUpModel.getArrCustomers();
                            }

                            if (customerTypesArr != null && customerTypesArr.size() > 0) {
                                for (int i = 0; i < customerTypesArr.size(); i++) {
                                    CustomerTyepModel customerTyepModel = customerTypesArr.get(i);
                                    if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase("3013")) {
                                        customerTypesArr.remove(customerTyepModel);
                                    }
                                }
                                ArrayAdapter<CustomerTyepModel> adapter = new ArrayAdapter<>(SkipSignupActivity.this, android.R.layout.simple_spinner_dropdown_item, customerTypesArr);
                                customerTypeSpinner.setAdapter(adapter);
                                int pos = 0;
                                for (int i = 0; i < customerTypesArr.size(); i++) {
                                    CustomerTyepModel customerTyepModel = customerTypesArr.get(i);
                                    if (customerTyepModel.getCustomerGrpId().equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_CUSTOMER_GRP_ID, ""))) {
                                        pos = i;
                                        break;
                                    }
                                }

                                customerTypeSpinner.setSelection(pos);
                            }

                            if (businessTyepArr != null && businessTyepArr.size() > 0) {
                                ArrayAdapter<BusinessTypeModel> adapter = new ArrayAdapter<>(SkipSignupActivity.this, android.R.layout.simple_spinner_dropdown_item, businessTyepArr);
                                businessTypeSpinner.setAdapter(adapter);
                                int pos = 0;
                                for (int i = 0; i < businessTyepArr.size(); i++) {
                                    BusinessTypeModel businessTypeModel = businessTyepArr.get(i);
                                    if (businessTypeModel.getSegmentId().equalsIgnoreCase(mSharedPreferences.getString(ConstantValues.KEY_SEGMENT_ID, ""))) {
                                        pos = i;
                                        break;
                                    }
                                }
                                businessTypeSpinner.setSelection(pos);
                            }

                        } else {
                            Utils.showAlertWithMessage(SkipSignupActivity.this, message);
                        }
                    } else if (requestType == PARSER_TYPE.GET_MAIN_CATEGORIES) {

                        if (status.equalsIgnoreCase("success")) {

                            if (isHome) {
                                startActivity(new Intent(SkipSignupActivity.this, HomeActivity.class));
                                finish();
                            } else {
                                startActivity(new Intent(SkipSignupActivity.this, FirstSignupActivity.class));
                            }

                        } else {
                            Utils.showAlertWithMessage(SkipSignupActivity.this, message);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSessionError(String message) {

    }
}
